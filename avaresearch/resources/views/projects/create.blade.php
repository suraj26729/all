@extends('layouts.app')
@section('title', 'Create Project')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <!-- <h4 class="">Fill up the details</h4> -->
    <a href="{{ route('projects.index') }}" class="btn btn-secondary btn-xs"><i class="icon-arrow_back"></i> Back to
      List</a>
    <hr />
    @if(session('success'))
    <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
    @endif @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif @if (session('errors'))
    {{ session('errors')->first('message') }}
    @endif
    <form class="form-horizontal" action="{{ route('projects.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="job_number" class="control-label">Job Number <sup class="mandatory">*</sup></label>
            <input class="form-control" id="job_number" type="text" name="job_number" value="{{ old('job_number') }}"
              required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="job_name" class="control-label">Job Name <sup class="mandatory">*</sup></label>
            <input class="form-control" id="job_name" type="text" name="job_name" value="{{ old('job_name') }}"
              required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="subject" class="control-label">Subject <sup class="mandatory">*</sup></label>
            <input class="form-control" id="subject" type="text" name="subject" value="{{ old('subject') }}" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="description" class="control-label">Description <sup class="mandatory">*</sup></label>
            <textarea name="description" id="description" rows="5" class="form-control"
              required>{{ old('description') }}</textarea>
          </div>
        </div>
      </div>
      <div class="row">


        <!-- <div class="col-md-6">
          <div class="form-group">
            <label for="client_id" class="control-label">Client <sup class="mandatory">*</sup></label>
            <select name="client_id" id="client_id" class="form-control" required>
              <option value="">Select Client</option>
              @foreach($clients as $client)
              <option value="{{ $client->id }}" data-company="{{ $client->profile->company_name }}">
                {{ $client->profile->first_name.' '.$client->profile->last_name }}
              </option>
              @endforeach
            </select>
          </div>
        </div> -->
        <div class="col-md-6">
          <div class="form-group">
            <label for="company_name" class="control-label">Company Name <sup class="mandatory">*</sup></label>
            <select name="client_id" id="company_name" class="form-control" required>
              <option value="">Select Company</option>
              @foreach($clients as $client)
              <option value="{{ $client->id }}" data-client="{{ $client->profile->first_name.' '.$client->profile->last_name }}">
                {{ $client->profile->company_name}}
              </option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="client_id" class="control-label">Client</label>
            <input class="form-control" type="text"  id="client_id" placeholder="Please select company first"
              disabled />
          </div>
        </div>


        
        <!-- <div class="col-md-6">
          <div class="form-group">
            <label for="company_name" class="control-label">Company Name</label>
            <input class="form-control" type="text" id="company_name" placeholder="Please select client first"
              disabled />
          </div>
        </div> -->



      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="location" class="control-label">Location <sup class="mandatory">*</sup></label>
            <input class="form-control" id="location" type="text" name="location" value="{{ old('location') }}"
              required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="contact_phone" class="control-label">Contact Phone <sup class="mandatory">*</sup></label>
            <input class="form-control mobile" id="contact_phone" type="text" name="contact_phone"
              value="{{ old('contact_phone') }}" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="contact_email" class="control-label">Contact Email <sup class="mandatory">*</sup></label>
            <input class="form-control" id="contact_email" type="email" name="contact_email"
              value="{{ old('contact_email') }}" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="venue_email" class="control-label">Venue Email <sup class="mandatory">*</sup></label>
            <input class="form-control" id="venue_email" type="email" name="venue_email"
              value="{{ old('venue_email') }}" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="received_date" class="control-label">Job Received <sup class="mandatory">*</sup></label>
            <input class="form-control date" id="received_date" type="text" name="received_date"
              value="{{ old('received_date') }}" autocomplete="off" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="start_date" class="control-label">Start Date <sup class="mandatory">*</sup></label>
            <input class="form-control date" id="start_date" type="text" name="start_date"
              value="{{ old('start_date') }}" autocomplete="off" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="finish_date" class="control-label">Finish Date <sup class="mandatory">*</sup></label>
            <input class="form-control date" id="finish_date" type="text" name="finish_date"
              value="{{ old('finish_date') }}" autocomplete="off" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="signed_sheets" class="control-label">Signed Sheets <sup class="mandatory">*</sup></label>
            <select name="signed_sheets" id="signed_sheets" class="form-control" required>
              <option value="">Select</option>
              <option value="1">Yes</option>
              <option value="0">No</option>
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Created By</label>
            <input type="text" disabled class="form-control"
              value="{{ auth()->user()->profile->first_name.' '.auth()->user()->profile->last_name }}" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <button class="btn btn-primary" id="submit_btn"><i class="fa fa-save mr-2"></i>Submit</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('footer')
<script>
  $('#location').change(function(){
        var location=$('#location').val();
            if(location == 'London' || location == 'london'){
            $('.mobile').mask('0000-0000-000');
          }else{
            $('.mobile').mask('0000-000-000');
          }
        });
   
  $('#company_name').change(function(){
            $('#client_id').val($('#company_name option:selected').data('client'));
        });
        $('#job_number').change(function(){
            var job_number = $(this).val();
            $.ajax({
                url: "{{ route('checkJobNum') }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}',job_number: job_number },
                beforeSend: function(){
                    $('small.checknum').remove();
                    $('#submit_btn').prop('disabled', true);
                },
                success: function(res){
                    if(res){
                        $('#job_number').after('<small class="text-danger checknum">Already exists in other project!</small>');
                    } else {
                        $('#job_number').after('<small class="text-success checknum">Available!</small>');
                        $('#submit_btn').removeAttr('disabled');
                    }
                },
                error: function(err){ console.log(err); }
            })
        });
</script>
@endsection