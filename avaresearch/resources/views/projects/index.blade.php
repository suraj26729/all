@extends('layouts.app') @section('title', 'Projects') @section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <div class="row">
      <div class="col-md-6">
        <a href="{{ route('projects.create') }}" class="btn btn-primary btn-xs"><i class="icon-plus"></i> Add New</a>
      </div>
      @if(auth()->user()->type != 'client')
      <div class="col-md-6 text-right">
        <button class="btn btn-primary btn-xs create_job_summary_annual_report">
          <i class="icon-document-download"></i> Job Summary Annual Report
        </button>
      </div>
      @endif
    </div>
    <hr />
    @if(session('success'))
    <div class="alert alert-success">{{ session("success") }}</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="list_table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Job Number</th>
            <th visible="false">Job Name</th>
            <th>Job Name</th>
            <th>Subject</th>
            <th>Description</th>
            <th>Client</th>
            <th visible="false">Client First Name</th>
            <th visible="false">Client Last Name</th>
            <th>Location</th>
            <th>Created By</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
@section('footer')
<script>
  $(document).ready(function() {

    $("#list_table").DataTable({
      sDom: "Rlfrtip",
      processing: true,
      serverSide: true,
      pageLength: 10,
      ajax: "{{ route('DTProjects') }}",
      columns: [
        { data: 'jobnumber', name: 'projects.jobnumber'/* ,"render": function(data, type, full, meta){
            var jobid =parseInt(data);
            return jobid;
        }*/
      },
        { data: "job_number", name: "job_number" },
        { data: "job_name", name: "job_name", visible: false },
        { data: "project_name", name: "projects.job_name", searchable: false },
        { data: "subject", name: "subject" },
        { data: "excerpt_desc", name: "description", width: "15%" },
        {
          data: "client_name",
          name: "user_details.first_name",
          searchable: false
        },
        {
          data: "first_name",
          name: "user_details.first_name",
          visible: false
        },
        {
          data: "last_name",
          name: "user_details.last_name",
          visible: false
        },
        { data: "location", name: "location" },
        { data: "created_name", name: "created_by" },
        { data: "created_at", name: "created_at" },
        {
          data: "actions",
          name: "actions",
          orderable: false,
          searchable: false
        }
      ],
      columnDefs: [
       { targets: 1, orderData: 0 },
       { targets: 0, visible: false }    
   ],
      order: [[11, "desc"]]
    });
  });
  $(document).on("click", ".delete_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");
    swal(
      {
        title: "Are you sure to delete",
        text: "Job Name: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('projects.index') }}/" + id,
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}" },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted", "You deleted the project: " + name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
  //Create job annual report
  $('.create_job_summary_annual_report').click(function(){
      var btn = $(this);
      var btn_html = btn.html();
      $.ajax({
          url: "{{ route('generateJobAnnualReport') }}",
          type: 'POST',
          data: { _token: '{{ csrf_token() }}' },
          beforeSend: function(){
              btn.html('<i class="fa fa-spinner fa-spin"></i> Generating Report...');
          },
          success: function(url){
              //swal("Report Generated", "", "success");
              document.location = url;
          },
          error: function(err){ console.log(err); },
          complete: function(){ btn.html(btn_html); }
      });
  });
</script>
@endsection