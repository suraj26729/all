@extends('layouts.app')
@section('title', 'Queries :: '.$project->job_name.' (#'.$project->job_number.')')
@section('content')

@include('projects.tab-nav', ['project' => $project, 'current'=>'queries'])

<div class="container-fluid animatedParent my-3 animateOnce white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <div class="row pb-2">
      <div class="col-md-12 text-right pl-4">
        <a href="javascript:void(0);" class="btn btn-danger btn-xs btn-delete-selected" style="display:none;"><i
            class="icon-trash"></i>
          Delete Selected
        </a>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="list_table">
        <thead>
          <tr>
            <th><input type="checkbox" id="check_all" /></th>
            <th>Title</th>
            <th>Created By</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection

@section('footer')
<script>
  $(document).ready(function() {
        $("#list_table").DataTable({
          sDom: "Rlfrtip",
          processing: true,
          serverSide: true,
          pageLength: 10,
          ajax: "{{ route('DTProjectQueries', $project->uuid) }}",
          columns: [
              { data: "check_row", name: "check_row", orderable: false, searchable: false, width: '20px' },
              { data: "title", name: "title", width: '600px' },
              { data: "creator_name", name: "created_by" },
              { data: "created_at", name: "created_at" },
              { data: "actions", name: "actions", orderable: false, searchable: false }
          ]
        });
    });
    $(document).on("click", ".delete_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");
    swal(
      {
        title: "Are you sure to delete",
        text: "Query Title: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('indexQuery', $project->uuid) }}/" + id,
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}" },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted", "You deleted the query: " + name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
  $(document).on('change', '#check_all', function(){
    if(this.checked){
      $('#list_table input[name=check_row]').each(function(){
        $(this).prop('checked', true);
      });
      $('.btn-delete-selected').show();
    } else {
      $('#list_table input[name=check_row]').each(function(){
        $(this).prop('checked', false);
      });
      $('.btn-delete-selected').hide();
    }
  });
  $(document).on('change', 'input[name=check_row]', function(){
    var checked = $('input[name=check_row]:checked').length;
    if(checked) $('.btn-delete-selected').show();
    else $('.btn-delete-selected').hide();
  });
  $(document).on("click", ".btn-delete-selected", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");

    var ids = [];
    var els = [];
    $('input[name=check_row]:checked').each(function(i, el){
      els.push(el);
      ids.push($(this).val());
    });

    swal(
      {
        title: "Are you sure to delete the selected queries",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('indexQuery', $project->uuid) }}/bulk/delete",
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}", ids: ids },
            success: function(res) {
              if (res) {
                $.each(els, (index, item) => {
                  item.closest('tr').remove();
                });
                swal("Deleted", "You deleted the selected queries", "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
</script>
@endsection