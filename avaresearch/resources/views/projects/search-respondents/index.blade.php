@extends('layouts.app')
@section('title', 'Search Repondents :: '.$project->job_name.' (#'.$project->job_number.')')
@section('header')
<link rel="stylesheet" href="{{ asset('public/assets/css/jsgrid.min.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('public/assets/css/jsgrid-theme.min.css') }}" type="text/css" />
@endsection

@section('content')
@include('projects.tab-nav', ['project' => $project,'current'=>'search-respondents'])

      <div class="scrollto" style="display:none;">
            <div class="scroll_to_b">
                    <a href="javascript:;" id="scrollToBottom">&#x25BC;</a>
                </div>   
            <div class="scroll_to_top">
                 <a href="javascript:;" id="scrollToTop">&#x25B2;</a>
            </div>
            <div class="scroll_left">
                <a href="javascript:;" id="scrollToRight">&#9664;</a>
            </div>
            <div class="scroll_right">
                <a href="javascript:;" id="scrollToLeft">&#9654;</a>
            </div>

                    
        </div>
<div class="row mt-3">
    <div class="col-md-12 text-center">
        <button class="btn btn-outline-primary btn-xs toggle_column"><i class="icon-edit"></i> Headers and
            Filters</button>
    </div>
</div>

<div class="container-fluid animatedParent my-3 animateOnce white relative shadow p-0 columns_filters_wrap">
    <div class="animated fadeInUpShort go">
        @include('projects.search-respondents.parts.select-tab')
    </div>
</div>

<div class="container-fluid animatedParent my-3 animateOnce white relative shadow p-0">
    <div class="animated fadeInUpShort go">
        @include('projects.search-respondents.parts.search-table')
    </div>
</div>

@endsection

@section('footer')
<script src="{{ asset('public/assets/js/jsgrid.min.js') }}" type="text/javascript"></script>
@endsection

@push('scripts')
<script>
    $('.toggle_column').click(function(){
        if($('.columns_filters_wrap').is(':visible'))
            $('.columns_filters_wrap').slideUp();
        if(!$('.columns_filters_wrap').is(':visible'))
            $('.columns_filters_wrap').slideDown();
    });
</script>
@endpush