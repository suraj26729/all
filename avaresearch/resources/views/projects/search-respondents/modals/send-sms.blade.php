<div class="modal fade" id="send-sms-modal" tabindex="-1" role="dialog" aria-labelledby="send-sms-modal"
    aria-hidden="true">  

    <div class="modal-dialog modal-dialog-centered modal-scrollable modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('sendProjectSMS', $project->uuid) }}" method="post" id="send-sms-form">
                @csrf
                <input type="hidden" name="project_id" value="<?php echo $project->uuid ?>" />
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Send SMS</h5>
                    <div class="maximize">
                        <button type="button" class="max-mize">
                            <i class="fa fa-window-maximize" aria-hidden="true"></i>
                        </button>  
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>                    
                   
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="to" class="control-label">To <sup class="mandatory">*</sup></label>
                        <input type="text" name="to" id="sms_to" class="form-control" readonly required />
                        <small>Number of total Respondents: <span class="text-primary total_res">0</span></small>
                    </div>
                    <div class="form-group">
                        <label for="template_type" class="control-label">Template Type <sup
                                class="mandatory">*</sup></label>
                        <select name="template_type" id="sms_template_type" class="form-control" required>
                            <option value="">Select</option>
                            <option value="project">Project</option>
                            <option value="universal">Universal</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="template_id" class="control-label">Body Template <sup
                                class="mandatory">*</sup></label>
                        <select name="template_id" id="sms_template_id" class="form-control" required>
                            <option value="">Select</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message" class="control-label">Message <sup class="mandatory">*</sup></label>
                        <div class="clear-fix"></div>
                        <blockquote class="text-primary">Maximum Number of characters for this text box is 152.
                        </blockquote>
                        <textarea name="message" id="sms_message" rows="5" class="form-control" required
                            onkeyup="char_count_message();"></textarea>
                        <small>You have <b id="remain_char">144</b> characters remaining for your
                            description...</small>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                    <button type="submit" class="btn btn-primary"><i class="icon-send-o"></i> Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('scripts')
<script>
    var maxLen = 152;
    function char_count_message(){
        var Length = $("#sms_message").val().length;
        var AmountLeft = maxLen - Length;
        $('#remain_char').html(AmountLeft);
        if(Length >= maxLen){
        if (event.which != 8) {
            return false;
        }
        }
    }
    $(document).on('change', '#sms_template_type', function(){
        var selected = $(this).val();
        $.ajax({
            url: "{{ route('indexProjectSearchRespondents', $project->uuid) }}" + '/send-sms/'+selected+'/templates',
            dataType: 'json',
            beforeSend: function(){
                $('#sms_template_id').prop('disabled', true);
            },
            success: function(res){
                var options = `<option value="">Select</option>`;
                $.each(res, (i, item) => {
                    options += `<option value="${item.id}">${item.title}</option>`;
                });
                $('#sms_template_id').html(options);
            },
            error: function(err){
                console.log(err);
                alert(err.responseText);
            },
            complete: function(){
                $('#sms_template_id').prop('disabled', false);
            }
        })
    });
    $(document).on('change', '#sms_template_id', function(){
        var selected = $('#sms_template_type').val();
        var id = $(this).val();
        $.ajax({
            url: "{{ route('indexProjectSearchRespondents', $project->uuid) }}" + '/send-sms/'+selected+'/templates/'+id,
            dataType: 'json',
            success: function(res){
                $('#sms_message').html(res.message);
            },
            error: function(err){
                console.log(err);
                alert(err.responseText);
            },
        })
    });
    $(document).on('submit', '#send-sms-form', function(){
        var form = $(this);
        var btn = form.find('button[type=submit]');
        var btn_text = btn.html();
        var formData = new FormData(form[0]);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                form.find('button[type=submit]').html('...').prop('disabled', true);
            },
            success: function(res){
                if(res){
                    $('#send-sms-modal').modal('hide');
                    swal("SMS Sent", "", "success");
                    form[0].reset();
                }
            },
            error: function(err){
                console.log(err);
                //alert(err.responseText);
            },
            complete: function(){
                form.find('button[type=submit]').html(btn_text).prop('disabled', false);
            }
        });
        return false;
    });

</script>
@endpush