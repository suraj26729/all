<div class="modal fade" id="save-query-modal" tabindex="-1" role="dialog" aria-labelledby="save-query-modal"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="" method="POST" id="save-query-form">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Save Query</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="query_title" class="control-label">Query Title</label>
                        <input type="text" class="form-control" name="query_title" required />
                    </div>
                    <input type="hidden" name="project_id" id="project_id" value="{{ $project->id }}" />
                    <input type="hidden" name="respondent_ids" id="respondent_ids" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Query</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(document).on('submit', '#save-query-form', function(){
            var checked = $('#searchGrid input[name=row_check]:checked').length;
            if(checked){
                var project_id = $('#btn-save-query').data('project');
                var respondent_ids = [];
                $('#searchGrid input[name=row_check]:checked').each(function(){
                    respondent_ids.push($(this).val());
                });
                if(respondent_ids.length)
                    $('#save-query-form').find('#respondent_ids').val(respondent_ids.join(','));

                $.ajax({
                    url: "{{ route('saveQuery', $project->uuid) }}",
                    type: 'POST',
                    data: $('#save-query-form').serialize(),
                    dataType: 'json',
                    beforeSend: function(){
                        $('#save-query-modal button[type=submit]').html('...').prop('disabled', false);
                    },
                    success: function(res){
                        $('#save-query-modal').modal('hide');
                        swal("Query is saved", "Selected query is saved successully", "success");
                    },
                    error: function(err){
                        console.log(err);
                        alert(err.responseText);
                    },
                    complete: function(){
                        $('#save-query-modal button[type=submit]').html('Save Query').removeAttr('disabled');
                    }
                });
            } else {
                swal("", "Please check any one respondent", "warning");
            }
            return false;
        });
</script>
@endpush