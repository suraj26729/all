<div class="modal fade" id="send-email-modal" tabindex="-1" role="dialog" aria-labelledby="send-email-modal"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-scrollable modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('sendProjectEmails', $project->uuid) }}" method="POST" id="send-email-form"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="project_id" value="<?php echo $project->uuid ?>" />
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Send Email</h5>
                    <div class="maximize">
                        <button type="button" class="max-mize">
                            <i class="fa fa-window-maximize" aria-hidden="true"></i>
                        </button>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="to" class="control-label">To <sup class="mandatory">*</sup></label>
                        <input type="text" name="to" id="to" class="form-control" readonly required />
                        <small>Number of total Respondents: <span class="text-primary total_res">0</span></small>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="control-label">Subject <sup class="mandatory">*</sup></label>
                        <input type="text" name="subject" id="subject" class="form-control" required />
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="template_type" class="control-label">Template Type <sup
                                        class="mandatory">*</sup></label>
                                <select name="template_type" id="template_type" class="form-control" required>
                                    <option value="">Select</option>
                                    <option value="project">Project</option>
                                    <option value="universal">Universal</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="group_id" class="control-label">Group <sup class="mandatory">*</sup></label>
                                <select name="group_id" id="group_id" class="form-control">
                                    <option value="">Select</option>
                                    @if($project->groups)
                                    @foreach ($project->groups as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="template_id" class="control-label">Body Template <sup
                                class="mandatory">*</sup></label>
                        <select name="template_id" id="template_id" class="form-control" required>
                            <option value="">Select</option>
                        </select>
                    </div>
                    <p class="text-danger">Don't mention " Dear Respondent/Participant " in Message Area</p>
                    <div class="form-group">
                        <label for="message" class="control-label">Message <sup class="mandatory">*</sup></label>
                        <textarea name="message" id="message" rows="10" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="attachments" class="control-label">Attachments</label>
                        <div class="attach_wrap"></div>
                    </div>
                    <div class="form-group">
                        <label for="new_attachments" class="control-label">New Attachments</label><br />
                        <input type="file" name="new_attachment1" id="new_attachment1" /><br />
                        <input type="file" name="new_attachment2" id="new_attachment2" /><br />
                        <input type="file" name="new_attachment3" id="new_attachment3" />
                        <br />
                        <br />
                        <input type="checkbox" name="include_privacy" id="include_privacy" value="1" /><span
                            class="text-danger">Include Privacy Policy</span>
                    </div>
                    <div class="form-group">
                        <label for="from" class="control-label">From <sup class="mandatory">*</sup></label>
                        <select name="from" id="from" class="form-control" required>
                            <option value="">Select</option>
                            <option value="office@avaresearch.com.au">office@avaresearch.com.au</option>
                            <option value="Project01@avaresearch.com.au">Project01@avaresearch.com.au</option>
                            <option value="Project02@avaresearch.com.au">Project02@avaresearch.com.au</option>
                            <option value="Project03@avaresearch.com.au">Project03@avaresearch.com.au</option>
                            <option value="Project04@avaresearch.com.au">Project04@avaresearch.com.au</option>
                            <option value="Project05@avaresearch.com.au">Project05@avaresearch.com.au</option>
                            <option value="info@avaresearch.com.au">info@avaresearch.com.au</option>
                            <option value="dhun@avaresearch.com.au">dhun@avaresearch.com.au</option>
                            <option value="mahtab@avaresearch.com.au">mahtab@avaresearch.com.au</option>
                            <option value="dhunm@hotmail.com">dhunm@hotmail.com</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                    <button type="submit" class="btn btn-primary"><i class="icon-send-o"></i> Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'message' );

    $(document).on('change', '#template_type', function(){
        var selected = $(this).val();
        $.ajax({
            url: "{{ route('indexProjectSearchRespondents', $project->uuid) }}" + '/send-email/'+selected+'/templates',
            dataType: 'json',
            beforeSend: function(){
                $('#template_id').prop('disabled', true);
            },
            success: function(res){
                var options = `<option value="">Select</option>`;
                $.each(res, (i, item) => {
                    options += `<option value="${item.id}">${item.title}</option>`;
                });
                $('#template_id').html(options);
            },
            error: function(err){
                console.log(err);
                alert(err.responseText);
            },
            complete: function(){
                $('#template_id').prop('disabled', false);
            }
        })
    });
    $(document).on('change', '#template_id', function(){
        var selected = $('#template_type').val();
        var id = $(this).val();
        $.ajax({
            url: "{{ route('indexProjectSearchRespondents', $project->uuid) }}" + '/send-email/'+selected+'/templates/'+id,
            dataType: 'json',
            success: function(res){
                CKEDITOR.instances.message.setData(res.message);
                if(res.attachments != null && res.attachments != undefined){
                    var html = '';
                    $.each(res.attachments, (i, item) => {
                        html = `<a href="{{ asset('storage/app/public') }}/${item.url}" target="_blank">${item.url.substr(item.url.lastIndexOf('/') + 1)}</a>`;
                    });
                    $('.attach_wrap').html(html);
                }
            },
            error: function(err){
                console.log(err);
                alert(err.responseText);
            },
        })
    });

    $(document).on('submit', '#send-email-form', function(){
        var form = $(this);
        var btn = form.find('button[type=submit]');
        var btn_text = btn.html();
        var formData = new FormData(form[0]);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                form.find('button[type=submit]').html('...').prop('disabled', true);
            },
            success: function(res){
                if(res){
                    $('#send-email-modal').modal('hide');
                    swal("Mail Sent", "", "success");
                    form[0].reset();
                    CKEDITOR.instances.message.setData('');
                    $('.attach_wrap').html('');
                }
            },
            error: function(err){
                console.log(err);
                alert(err.responseText);
            },
            complete: function(){
                form.find('button[type=submit]').html(btn_text).prop('disabled', false);
            }
        });
        return false;
    });

</script>
@endpush