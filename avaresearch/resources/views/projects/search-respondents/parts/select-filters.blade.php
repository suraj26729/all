<div class="row mb-2">
    <div class="col-md-12">
        <button type="reset" class="btn btn-danger btn-sm btn-reset"><i class="fa fa-times"></i> Clear</button>
    </div>
</div>
<div class="row">
    @if(isset($filters_json->column1))
    <div class="col-md-3">
        <div class="form-group">
            <ul id="filters_ul" class="m-0">
                @foreach ($filters_json->column1 as $item)
                <li class="filter_li" style="width: 240px; display: inline-block; float: left;">
                    <input type="checkbox" class="filter" name="filter[]" value="{{ $item->key }}"
                        {{ isset($query->filter) && in_array($item->key, $query->filter) ? 'checked' : '' }}>&nbsp;
                    {{ $item->label }}
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    @if(isset($filters_json->column2))
    <div class="col-md-3">
        <div class="form-group">
            <ul id="filters_ul" class="m-0">
                @foreach ($filters_json->column2 as $item)
                <li class="filter_li" style="width: 240px; display: inline-block; float: left;">
                    <input type="checkbox" class="filter" name="filter[]" value="{{ $item->key }}"
                        {{ isset($query->filter) && in_array($item->key, $query->filter) ? 'checked' : '' }}>&nbsp;
                    {{ $item->label }}
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    @if(isset($filters_json->column3))
    <div class="col-md-3">
        <div class="form-group">
            <ul id="filters_ul" class="m-0">
                @foreach ($filters_json->column3 as $item)
                <li class="filter_li" style="width: 240px; display: inline-block; float: left;">
                    <input type="checkbox" class="filter" name="filter[]" value="{{ $item->key }}"
                        {{ isset($query->filter) && in_array($item->key, $query->filter) ? 'checked' : '' }}>&nbsp;
                    {{ $item->label }}
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
    @if(isset($filters_json->column4))
    <div class="col-md-3">
        <div class="form-group">
            <ul id="filters_ul" class="m-0">
                @foreach ($filters_json->column4 as $item)
                <li class="filter_li" style="width: 240px; display: inline-block; float: left;">
                    <input type="checkbox" class="filter" name="filter[]" value="{{ $item->key }}"
                        {{ isset($query->filter) && in_array($item->key, $query->filter) ? 'checked' : '' }}>&nbsp;
                    @if(in_array($item->key,['3_month_rule', '6_month_rule', '12_month_rule']))
                    <b style="font-weight: 700;">{{ $item->label }}</b>
                    @else
                    {{ $item->label }}
                    @endif
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif
</div>

@include('projects.search-respondents.parts.filters')