<div class="row">
    <div class="col-md-12">
        <div class="card no-b">
            <div class="card-header white pb-0">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center">
                        <ul class="nav nav-pills mb-3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link r-20" id="w3--tab1" data-toggle="tab" href="#w3-tab1" role="tab"
                                    aria-controls="tab1" aria-expanded="true" aria-selected="true">Select
                                    Headers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link r-20 active show" id="w3--tab2" data-toggle="tab" href="#w3-tab2"
                                    role="tab" aria-controls="tab2" aria-selected="false">Select Filters</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card-body no-p">
                <form action="" id="search_respond" method="POST">
                    @csrf
                    <div class="tab-content">
                        <div class="tab-pane p-10 fade show" id="w3-tab1" role="tabpanel" aria-labelledby="w3-tab1">
                            @include('projects.search-respondents.parts.select-columns')
                        </div>
                        <div class="tab-pane p-10 fade show active" id="w3-tab2" role="tabpanel"
                            aria-labelledby="w3-tab2">
                            @include('projects.search-respondents.parts.select-filters')
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 text-center">
                            <button type="submit" class="btn btn-primary"><i
                                    class="fa fa-search mr-2"></i>Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>