{{-- <div class="toggle_form">
        <a href="#" class="open_frm">Test goeing</a>
      </div> --}}



<div class="search_table_wrap">
    <div class="row search-summary text-center" style="display: none;">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h5>Total Number of Respondents: <span id="total_resp">0</span></h5>
        </div>
        <div class="col-md-4">
            <h5 class="text-right pr-4">Page: <span id="pagenumber"> </span> </h5>
        </div>
        <div class="col-md-12 flag-summary text-left">
            <div class="row">
                <div class="col-md-3">
                    <span class="colorbox"><b style="background: #f97b2e;"></b> DO NOT CALL(Bad candidate or
                        Groupie)</span>
                </div>
                <div class="col-md-3">
                    <span class="colorbox"><b style="background: #f3dc32;"></b> ONE OFF RESPONDENT(FROM LISTS)</span>
                </div>
                <div class="col-md-3">
                    <span class="colorbox"><b style="background: #3e9003;"></b> HAS ACCENT</span>
                </div>
                <div class="col-md-3">
                    <span class="colorbox"><b style="background: #9a139a;"></b> EXCELLENT</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <span class="colorbox"><b style="background: #fb0202;"></b> NO SHOW</span>
                </div>
                <div class="col-md-3">
                    <span class="colorbox"><b style="background: #2b58bf;"></b> DO NOT SEND INVITATION EMAIL
                        OFTEN</span>
                </div>
                {{-- <div class="col-md-3">
                    <span class="colorbox"><b style="background: #C2B7FF;"></b> HAS RECOMMENDED FRIENDS OR
                        RELATIVES</span>
                </div> --}}
                <div class="col-md-3">
                    <span class="colorbox"><b style="background: #eb66f3;"></b> KEEN CANDIDATES</span>
                </div>
               <div class="col-md-3 text-right">
                
                </div>
            </div>
            
        </div>

        {{-- <div class="wrapper1">
                <div class="div1">
                </div>
            </div>
            <div class="wrapper2">
                <div class="div2">
                        
                </div>
            </div>  --}}
        <div class="col-md-12">
            <div id="searchGrid"></div>
        </div>




        <div class="well text-center p-3 search-action-btns" style="display: none;">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" id="btn-save-query" data-project="{{ $project->id }}">
                    <i class="icon-save mr-2"></i> Save Query
                </button>
                <button type="button" class="btn btn-primary" id="btn-send-email" data-project="{{ $project->id }}"><i
                        class="icon-email mr-2"></i> Send E-mail</button>
                <button type="button" class="btn btn-primary" id="btn-send-sms" data-project="{{ $project->id }}"
                    {{ (auth('web')->check() && auth('web')->user()->type=='staff') ? 'disabled' : '' }}><i
                        class="icon-textsms mr-2"></i> Send
                    SMS</button>
                <button type="button" class="btn btn-primary" id="btn-export-query" data-project="{{ $project->id }}"><i
                        class="icon-file-excel-o mr-2"></i> Export Query</button>
                <button type="button" class="btn btn-primary" data-project="{{ $project->id }}"><i
                        class="icon-check mr-2"></i> Allocate Point</button>
                <button type="button" class="btn btn-primary" id="btn-save-job-event"
                    data-project="{{ $project->id }}"><i class="icon-event_available mr-2"></i> Save Job
                    Event</button>
                <button type="button" class="btn btn-primary" data-project="{{ $project->id }}"><i
                        class="icon-check mr-2"></i> Respondent
                    Screening</button>
            </div>
            @if(auth('web')->check() && auth('web')->user()->type=='staff')
            <div class="col-md-12 mt-3">
                <strong class="text-danger">* Staff cannot send SMS</strong>
            </div>
            @endif
        </div>
    </div>

    @push('scripts')

    @include('projects.search-respondents.modals.save-query')

    @include('projects.search-respondents.modals.send-email')

    @include('projects.search-respondents.modals.send-sms')

    <script>
        $(document).on('click', '#btn-save-query', function(){
        var checked = $('#searchGrid input[name=row_check]:checked').length;
        if(checked){
            $('#save-query-modal').modal('show');
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    $(document).on('click', '#btn-send-email', function(){
        var checked = $('#searchGrid input[name=row_check]:checked').length;
        if(checked){
            let emails = [];
            $('#searchGrid input[name=row_check]:checked').each(function(i, el){
                var email = $(this).data('email');
                if(email!='')
                    emails.push(email);
            });
            $('#send-email-form #to').val(emails.join(','));
            $('#send-email-form .total_res').html(emails.length);
            $('#send-email-modal').modal('show');
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    $(document).on('click', '#btn-send-sms', function(){
        var checked = $('#searchGrid input[name=row_check]:checked').length;
        if(checked){
            let phones = [];
            $('#searchGrid input[name=row_check]:checked').each(function(i, el){
                var phone = $(this).data('mobile');
                if(phone != '')
                    phones.push(phone);
            });
            $('#send-sms-form #sms_to').val(phones.join(','));
            $('#send-sms-form .total_res').html(phones.length);
            $('#send-sms-modal').modal('show');
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    $(document).on('click', '#btn-save-job-event', function(){
        var btn = $(this);
        var btn_text = $(this).html();
        var checked = $('#searchGrid input[name=row_check]:checked').length;
        if(checked){
            let data = [];
            let flag = 0;
            $('#searchGrid input[name=row_check]:checked').each(function(i, el){
                var respondent_id = $(this).val();
                var name = $(this).closest('tr').find('select.job_event').val();
                var group_id = $(this).closest('tr').find('select.job_group').val();
                var group_time = $(this).closest('tr').find('select.job_group_time').val();

                if(name == '') {
                    flag++;
                    swal("Data Validation", `You missed to select a job event for a respondent #${respondent_id}. Please select it`, "error");
                    return false;
                } else if(group_id == ''){
                    flag++;
                    swal("Data Validation", `You missed to select a job group for a respondent #${respondent_id}. Please select it`, "error");
                    return false;
                } else if(group_time == ''){
                    flag++;
                    swal("Data Validation", `You missed to select a job group time for a respondent #${respondent_id}. Please select it`, "error");
                    return false;
                } else {
                    data.push({ respondent_id, name, group_id, group_time });
                }
            });
            if(flag == 0){
                $.ajax({
                    url: "{{ route('saveJobEvent', $project->uuid) }}",
                    type: 'POST',
                    data: { _token: "{{ csrf_token() }}", data: data },
                    dataType: 'json',
                    beforeSend: function(){
                        btn.html('<i class="icon-spinner"></i>');
                    },
                    success: function(res){
                        if(res.success){
                            swal("Job Events Saved", `Job events are created for the selected respondents`, "success");
                        }
                    },
                    error: function(err){
                        console.log(err);
                        alert(err.responseText);
                    },
                    complete: function(){
                        btn.html(btn_text);
                    }
                });
            }
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });

    $('#btn-export-query').click(function(){
        var btn = $(this);
        var btn_text = $(this).html();
        var checked = $('#searchGrid input[name=row_check]:checked').length;
        if(checked){
            var ids = [];
            $('#searchGrid input[name=row_check]:checked').each(function(i, el){
                ids.push($(this).val());
            });
            swal(
                {
                    title: "Are you sure to export ?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "{{ route('exportQuery', $project->uuid) }}",
                                type: 'POST',
                                data: { _token: "{{ csrf_token() }}", ids: ids },
                                //dataType: 'json',
                                beforeSend: function(){
                                    btn.html('<i class="icon-spinner"></i>');
                                },
                                success: function(url){
                                    swal("Query Exported", ``, "success");
                                    window.open(url, '_blank');
                                },
                                error: function(err){
                                    console.log(err);
                                    alert(err.responseText);
                                },
                                complete: function(){
                                    btn.html(btn_text);
                                }
                            });
                        }
                    }
            );
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    </script>

    <script>
        var columns = [];
    var edit_columns = [
        'first_name',
        'last_name',
        'occupation', 
        'partner_occupation', 
        'job_title', 
        'birth_country', 
        'ethnic_background', 
        'income_level',
        //'children_name_and_age',
        'email',
        'mobile_phone',
        'mobile_phone2',
        'home_phone',
        'work_phone',
        'home_postcode',
        'work_postcode',
        'highest_education',
        'profession',
        'cigarette_brands',
        //'brands',
        'mobile1_type',
        'alcohol_consumed',
        'health_problems',
        'state',
        'additional_notes',
        'closest_large_city'
    ];
    $('#search_respond').submit(function(){
        columns = [
            {
                name: "check",
                title: `<input type="checkbox" class="row_check_all" />`,
                width: 50,
                editing: false,
                sorting: false
            },
            @if(auth()->check() && auth()->user()->type == 'admin')
            {
                name: "delete",
                title: '',
                width: 50,
                editing: false,
                sorting: false
            },
            @endif
            {
                type: "control",deleteButton: false,
                sorting: false
            },
            {
                name: "flags",
                "title": "",
                width: 10,
                editing: false,
                sorting: false
            },
            {
                name: "id",
                title: "ID",
                type: "text",
                editing:false
            },
            {
                name: "first_name", 
                title: "First Name",
                type: "text", 
                width: 130,
                validate: "required",
                editing: edit_columns.indexOf('first_name') != -1 ? true : false
            },
            {
                name: "last_name", 
                title: "Last Name",
                type: "text",
                width: 130,
                validate: "required",
                editing: edit_columns.indexOf('last_name') != -1 ? true : false
            },
            {
                name: "age", 
                title: "Age",
                type: "text",
                width: 80,
                editing: edit_columns.indexOf('age') != -1 ? true : false
            },
            {
                name: "email", 
                title: "Email",
                type: "text",
                width: 250,
                editing: edit_columns.indexOf('email') != -1 ? true : false
            },
            {
                name: "mobile_phone", 
                title: "Mobile Phone 1",
                type: "text",
                width: 130,
                validate: "required",
                editing: edit_columns.indexOf('mobile_phone') != -1 ? true : false
            },
            {
                name: "mobile_phone2", 
                title: "Mobile Phone 2",
                type: "text",
                width: 130,
                //validate: "required",
                editing: edit_columns.indexOf('mobile_phone2') != -1 ? true : false
            },
            {
                name: "additional_notes", 
                title: "Additional Notes",
                type: "textarea",
                width: 130,
                //validate: "required",
                editing: edit_columns.indexOf('additional_notes') != -1 ? true : false
            },
        ];

        var editData = JSON.parse(`{!! $editData !!}`);

       

        var job_events_json = JSON.parse(`{!! $job_events_json !!}`);
        var job_event_options = '';
        $.each(job_events_json, (key, item) => {
            job_event_options += `<optgroup label="${item.title}">`;
                $.each(item.events, (key2, event) => {
                    job_event_options += `<option value="${event.value}">${event.label}</option>`;
                });
            job_event_options += `</optgroup>`;
        });
        var job_event_html = `<select class="form-control job_event">
                                <option value="">Select</option>
                                ${job_event_options}
                            </select>`;
        var groups_json = JSON.parse(`{!! $groups_json !!}`);
        var groups_options = '';
        $.each(groups_json, (key, item) => {
            groups_options += `<option value="${item.value}">${item.label}</option>`;
        });
        job_event_html += `<select class="form-control job_group">
                                <option value="">Select</option>
                                ${groups_options}
                            </select>`;

        job_event_html += `<select class="form-control job_group_time" style="display:none;"><option value="">Select</option></select>`;

        $.each($(".column:checked"), function(){  
            var name = $(this).val();
            if(name != 'respondents.id' && name != 'first_name' && name != 'last_name' && name != 'age' && name != 'email' && name != 'mobile_phone' && name != 'mobile_phone2' && name != 'additional_notes'){
                if(name == 'occupation' || name == 'partner_occupation'){
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.occupations,
                        valueField: 'id',
                        textField: 'name'
                       
                    });
                } else if(name == 'birth_country'){
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.birth_country,
                        valueField: 'id',
                        textField: 'name',
                        validate: "required"
                    });
                } else if(name == 'ethnic_background'){ 
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.ethnic_background,
                        valueField: 'id',
                        textField: 'name'
                        
                    });
                } else if(name == 'highest_education'){ 
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.highest_education,
                        valueField: 'name',
                        textField: 'name',
                        validate: "required"
                    });
                } else if(name == 'profession'){ 
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.profession,
                        valueField: 'name',
                        textField: 'name'
                    });
                } else if(name == 'income_level'){ 
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.income_level,
                        valueField: 'name',
                        textField: 'name'
                    });
                } else if(name == 'state'){ 
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.state,
                        valueField: 'id',
                        textField: 'name'
                    });
                } 
                else if(name == 'cigarette_brands'){ 
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.cigarette_brands,
                        valueField: 'name',
                        textField: 'name'
                    });
                } 
                else if(name == 'mobile1_type'){ 
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.mobile1_type,
                        valueField: 'name',
                        textField: 'name'
                    });
                } else if(name == 'alcohol_consumed'){ 
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "select",
                        width: 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false,
                        items: editData.alcohol_consumed,
                        valueField: 'id',
                        textField: 'name'
                    });
                } else if(name == 'job_event'){
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        width: 150,
                        editing: false,
                        sorting: false,
                        itemTemplate: function(value, item){
                            return job_event_html;
                        }
                    });
                } else if(name == "email_template" || name == "previous_events") {
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "text",
                        editing: false,
                        sorting: false,
                        width: name == 'email' ? 250 : 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false
                    });
                } else if(name == "home_postcode") {
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "text",
                        width: 80,
                        editing: edit_columns.indexOf(name) != -1 ? true : false
                    });
                }else if(name == "Closest Large City") {
                    columns.push({
                        name: name, 
                        title: name.replace('_', ' '),
                        type: "text",
                        width: 80,
                        editing: edit_columns.indexOf(name) != -1 ? true : false
                    });
                } else {
                    var title = name.replace('_', ' ');
                    if(name == 'home_postsuburb') title = 'Home Suburb';
                    if(name == 'work_postsuburb') title = 'Work Suburb';
                    columns.push({
                        name: name, 
                        title: title,
                        type: "text",
                        width: name == 'email' ? 250 : 150,
                        editing: edit_columns.indexOf(name) != -1 ? true : false
                    });
                }
            }
        });
        draw_grid();
        return false;
    });
    $('.btn-reset').click(function(){
        if($('#w3-tab1').hasClass('active')){
            $.each($(".column:checked").not(':disabled'), function(){  
                $(this).prop('checked', false)
                $(this).removeAttr('checked');
            });
        }
        if($('#w3-tab2').hasClass('active')){
            $.each($(".filter:checked").not(':disabled'), function(){  
                $(this).prop('checked', false)
                $(this).removeAttr('checked');
                var value = $(this).val();

                $('#filter_'+value+'_wrap').find('input').not('[type=checkbox]').val('').removeAttr('value');
                $('#filter_'+value+'_wrap').find('select[multiple]').val([]).trigger('change');
                $('#filter_'+value+'_wrap').find('select').not('[multiple]').val('');
                $('#filter_'+value+'_wrap').find('r').val('').html('');
                $('#filter_'+value+'_wrap').find('input[type=checkbox]').prop('checked', false).removeAttr('checked');
                $('#filter_'+value+'_wrap').hide();
                if(value=='cigarette'){
                    $('#filter_brands_wrap').find('select[multiple]').val([]).trigger('change');
                    $('#filter_brands_wrap').hide();
                } 
            });
        }
    });
    function draw_grid(){
        $('.search-summary, .search-action-btns').hide();
        var btn = $('#search_respond').find('button[type=submit]');
        var btn_text = btn.html();

        $("#searchGrid").jsGrid("reset");//Reset The Grid date while click search

        $("#searchGrid").jsGrid({
            width: "100%",
            //height: "600px",

            editing: true,
            sorting: true,
            paging: true,
            autoload: true,

            pageSize: {{ $pageSize }},
            pageButtonCount: 5,
            pageLoading : true,
            deleteConfirm: "Do you really want to delete?",

            heading: true,

            controller: {
                loadData: function(filter){
                    $('#pagenumber').html(filter.pageIndex);
                    //console.log(filter);
                    // var formArray = $('#search_respond').serializeArray();
                    // var _formArray = [];
                    // for(let item of formArray){
                    //     if(item.name.indexOf('filter_') != -1 && !$('[name='+item.name+']').is(':visible')) _formArray.push({ name: item.name, value: "" });
                    //     else _formArray.push(item);
                    // }
                    // var serializeData = [];
                    // for(let item of _formArray) serializeData.push(`${item.name}=${item.value}`);
                    // let serializeStr = serializeData.join('&');

                    return $.ajax({
                        type: "POST",
                        url: "{{ route('getProjectSearchRespondents', $project->uuid) }}",
                        data: $('#search_respond').serialize()+"&pageIndex="+filter.pageIndex+"&pageSize="+filter.pageSize+
                        "&sortField="+(filter.sortField || '')+"&sortOrder="+(filter.sortOrder || ''),
                        // data: serializeStr+"&pageIndex="+filter.pageIndex+"&pageSize="+filter.pageSize+
                        // "&sortField="+(filter.sortField || '')+"&sortOrder="+(filter.sortOrder || ''),
                        beforeSend: function(){ $('.scrollto').hide(); btn.html('<i class="fa fa-circle-o-notch fa-spin"></i> Searching...').prop('disabled', true);
                         },
                        success: function(res){
                            $('.search-summary, .search-action-btns').show();
                            $('#total_resp').html(res.itemsCount);
                            
                            setTimeout(function(){
                                $('.jsgrid-grid-body').doubleScroll();
                                $('html, body').animate({
                                    scrollTop: $(".search_table_wrap").offset().top - $('.sticky').outerHeight() - 10
                                }, 1000);
                                $('.scrollto').show();
                            }, 2000);
                        },
                        error: function(err){ console.log(err); alert(err.responseText); },
                        complete: function(){ btn.html(btn_text).prop('disabled', false); }
                    });
                },
                updateItem: function(item){
                    item._token = "{{ csrf_token() }}";
                    item._method = "PATCH";

                   //anchor to get id
                   var res_id=$(item.id).text();
                   var id =parseInt(res_id);
                    item.id= id;

                    return $.ajax({
                        type: "POST",
                        url: "{{ route('inlineUpdateRespondent') }}",
                        data: item,
                    });
                }
            },
            
            fields: columns,
            rowClick: function (args) {
                //selectShare(args.item.job_event);
                return false;
            },
            // rowClass: function(item, itemIndex) {
            //     return (item.isQualified != undefined && item.isQualified) ? 'bg-green' : '';
            // },
            pagerFormat: "Pages: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} of {pageCount} Total rows: {itemCount}",
        });
    }
    $('.filter').change(function(){
        var value = $(this).val();
        if($(this).is(':checked'))
            $('#filter_'+value+'_wrap').show();
        else{
            $('#filter_'+value+'_wrap').find('input').not('[type=checkbox]').val('').removeAttr('value');
            $('#filter_'+value+'_wrap').find('select[multiple]').val([]).trigger('change');
            $('#filter_'+value+'_wrap').find('select').not('[multiple]').val('').trigger('change');
            $('#filter_'+value+'_wrap').find('textarea').val('');
            $('#filter_'+value+'_wrap').find('input[type=checkbox]').prop('checked', false);
            $('#filter_'+value+'_wrap').hide();
            if(value=='cigarette'){
                $('#filter_brands_wrap').find('select[multiple]').val([]).trigger('change');
                $('#filter_brands_wrap').hide();
            }
        }
    });
    $('#filter_cigarette_wrap input[type="checkbox"]').change(function(){
        var cigarette = $(this).val();
        console.log(cigarette);
        if(cigarette != "Doesn't Smoke"){
            $('#filter_brands_wrap').show();
        } else {
            $('#filter_brands_wrap').find('select[multiple]').val([]).trigger('change');
            $('#filter_brands_wrap').hide();
        }
    });
    $(document).on('change', '.chs', function(){
        var name = $(this).attr('name');
        $(this).closest('.checkboxes').find('input[type="checkbox"]').prop('checked', false).removeAttr('checked');
        $(this).prop('checked', true);
    });
    $('#filter_phone_companies_wrap [type="checkbox"]').change(function(){
        if($(this).is(':checked')){
            if($(this).val() == 'Home')
                $('#filter_phone_companies_wrap #filter_home_phone_provider_wrap').show();
            if($(this).val() == 'Work')
                $('#filter_phone_companies_wrap #filter_work_phone_provider_wrap').show();
            if($(this).val() == 'Mobile')
                $('#filter_phone_companies_wrap #filter_mobile1_provider_wrap').show();
        } else {
            if($(this).val() == 'Home')
                $('#filter_phone_companies_wrap #filter_home_phone_provider_wrap').hide();
            if($(this).val() == 'Work')
                $('#filter_phone_companies_wrap #filter_work_phone_provider_wrap').hide();
            if($(this).val() == 'Mobile')
                $('#filter_phone_companies_wrap #filter_mobile1_provider_wrap').hide();
        }
    });

    $(document).on('change','.search_table_wrap select.job_group', function(){
        var group_id = $(this).val();
        var el = $(this);
        if(group_id){
            $.ajax({
                url: "{{ url('/projects/groups/timings') }}/"+group_id,
                dataType: 'json',
                success: function(res){
                    if(res.length){
                        el.closest('.jsgrid-cell').find('select.job_group_time').html(`<option value="">Select</option>`);
                        $.each(res, (i, item) => {
                            el.closest('.jsgrid-cell').find('select.job_group_time').append(`<option value="${item}">${item}</option>`);
                        });
                        el.closest('.jsgrid-cell').find('select.job_group_time').show();
                    }
                },
                error: function(err){
                    console.log(err);
                    alert(err.responseText);
                }
            });
        }
    });

    @if($project->searched_data)
        $(document).ready(function(){
            $.each($(".filter:checked").not(':disabled'), function(){  
                $(this).trigger('change');
            });
        });
    @endif

    $(document).on('change', '.row_check_all', function(){
        if($(this).is(':checked')){
            $('.row_check').each(function(){
                $(this).prop('checked', true);
            });
        } else {
            $('.row_check').each(function(){
                $(this).prop('checked', false);
            });
        }
    });

    $(document).on("click", ".delete_res", function() {
        var id = $(this).data("id");
        var name = $(this).data("name");
        var row = $(this).closest("tr");
        swal({
  title: "Delete",
  text: "Please Enter The Reason",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  confirmButtonText: "Delete",
  cancelButtonText: "Cancel",
  confirmButtonClass: "btn-danger",
  
  },
  function(reason){
    if (reason === false){
      swal.showInputError("Please Fill The Reason!");
    }else if (reason === "") {
      swal.showInputError("Please Fill The Reason!");
     return false
    } else{
       $.ajax({
            url: "{{ route('duplicateDeleteReason') }}",
            type: "POST",
            data: { _token: "{{ csrf_token() }}",reason: reason, id :id },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted!", "You deleted the respondent: "+name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
       }
    });
    });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#scrollToBottom').bind("click", function () {
                $('html, body').animate({ scrollTop: $(document).height() }, 1200);
                return false;
            });
            $('#scrollToTop').bind("click", function () {
                $('html, body').animate({ scrollTop: 0 }, 1200);
                return false;
            });

            $('#scrollToLeft').bind("click", function () {
                //event.preventDefault();
                $('html, body').animate({
                  scrollLeft: "+=200px"
                }, "slow");
                });

        $('#scrollToRight').bind("click", function () {
             //event.preventDefault();
            $('html, body').animate({
              scrollLeft: "-=200px"
             }, "slow");
            });
        });


    </script>

    <script type="text/javascript">
        $(window).on('load', function(){ 
            $(function(){
                $(".wrapper1").scroll(function(){
                    $(".wrapper2")
                        .scrollLeft($(".wrapper1").scrollLeft());
                });
                $(".wrapper2").scroll(function(){
                    $(".wrapper1")
                        .scrollLeft($(".wrapper2").scrollLeft());
                });
            });
        });
    </script>

    <script>
        $('#filter_hear_about').change(function(){
            if($(this).val() == 'Others'){
                $('#filter_source_name').show();
            } else {
                $('#filter_source_name').hide();
            }
        });
    </script>
    
    @endpush