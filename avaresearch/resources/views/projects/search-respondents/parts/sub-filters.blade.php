<div class="form-group sub-fields" id="filter_{{ $sub->key }}_wrap" style="display: none;">
    @if(isset($sub->type))
    <label class="control-label" for="filter_{{ $sub->key }}"> {{ $sub->label }}</label>
    @endif

    @if(isset($sub->type) && $sub->type == 'text')
    <input type="text" name="filter_{{ $sub->key }}"
        class="form-control {{ isset($sub->phone) && $sub->phone ? 'phone' : '' }}"
        value="{{ isset($filter_values['filter_'.$sub->key]) ? $filter_values['filter_'.$sub->key] : '' }}" />

    @elseif(isset($sub->type) && $sub->type == 'number')
    <input type="number" name="filter_{{ $sub->key }}"
        class="form-control {{ isset($sub->phone) && $sub->phone ? 'phone' : '' }}"
        value="{{ isset($filter_values['filter_'.$sub->key]) ? $filter_values['filter_'.$sub->key] : '' }}" />

    @elseif(isset($sub->type) && $sub->type == 'email')
    <input type="email" name="filter_{{ $sub->key }}" class="form-control"
        value="{{ isset($filter_values['filter_'.$sub->key]) ? $filter_values['filter_'.$sub->key] : '' }}" />

    @elseif(isset($sub->type) && $sub->type == 'url')
    <input type="url" name="filter_{{ $sub->key }}" class="form-control"
        value="{{ isset($filter_values['filter_'.$sub->key]) ? $filter_values['filter_'.$sub->key] : '' }}" />

    @elseif(isset($sub->type) && $sub->type == 'textarea')
    <textarea name="filter_{{ $sub->key }}" id="filter_{{ $sub->key }}" rows="3"
        class="form-control">{{ isset($filter_values['filter_'.$sub->key]) ? $filter_values['filter_'.$sub->key] : '' }}</textarea>

    @elseif(isset($sub->type) && $sub->type == 'select')
    <select name="filter_{{ $sub->key }}{{ isset($sub->multiple) && $sub->multiple==true ? '[]' : '' }}"
        id="filter_{{ $sub->key }}"
        class="form-control {{ isset($sub->multiple) && $sub->multiple==true ? 'select2' : '' }}"
        {{ isset($sub->multiple) && $sub->multiple==true ? 'multiple="multiple"' : '' }}>
        <option value="">Select</option>
        @if(isset($sub->options))
        @foreach ($sub->options as $option)
        <option value="{{ $option }}" {{ q_item_select($sub, $option, $filter_values) }}>{{ $option }}</option>
        @endforeach
        @endif
    </select>

    @elseif(isset($sub->type) && $sub->type == 'checkbox')
    <div class="checkboxes">
        @foreach ($sub->options as $option)
        <input class="{{ isset($sub->single) && $sub->single ? 'chs' : '' }}" type="checkbox"
            name="filter_{{ $sub->key }}[]" value="{{ $option }}" {{ q_item_check($sub, $option, $filter_values) }} />
        {{ $option }}
        @endforeach
    </div>

    @elseif(isset($sub->type) && $sub->type == 'number_range')
    <div class="input-group">
        <input name="filter_{{ $sub->key }}_from" class="form-control text-center" placeholder="From" type="number"
            value="{{ isset($filter_values['filter_'.$sub->key.'_from']) ? $filter_values['filter_'.$sub->key.'_from'] : '' }}" />
        <span class="input-group-addon"><i class="icon-angle-right"></i></span>
        <input name="filter_{{ $sub->key }}_to" class="form-control text-center" placeholder="To" type="number"
            value="{{ isset($filter_values['filter_'.$sub->key.'_to']) ? $filter_values['filter_'.$sub->key.'_to'] : '' }}" />
    </div>

    @elseif(isset($sub->type) && $sub->type == 'date_range')
    <div class="input-group">
        <input name="filter_{{ $sub->key }}_from" class="form-control text-center date" placeholder="From" type="text"
            value="{{ isset($filter_values['filter_'.$sub->key.'_from']) ? $filter_values['filter_'.$sub->key.'_from'] : '' }}" />
        <span class="input-group-addon"><i class="icon-angle-right"></i></span>
        <input name="filter_{{ $sub->key }}_to" class="form-control text-center date" placeholder="To" type="text"
            value="{{ isset($filter_values['filter_'.$sub->key.'_to']) ? $filter_values['filter_'.$sub->key.'_to'] : '' }}" />
    </div>

    @elseif(isset($sub->type) && $sub->type == 'master_select')
    <select name="filter_{{ $sub->key }}{{ isset($sub->multiple) && $sub->multiple==true ? '[]' : '' }}"
        id="filter_{{ $sub->key }}"
        class="form-control {{ isset($sub->multiple) && $sub->multiple==true ? 'select2' : '' }}"
        {{ isset($sub->multiple) && $sub->multiple==true ? 'multiple="multiple"' : '' }}>
        <option value="">Select</option>
        @foreach ($masters[$sub->key] as $option)
        @if(isset($sub->value_field) && $sub->value_field == 'name')
        <option value="{{ $option->name }}" {{ q_item_select($sub, $option->name, $filter_values) }}>{{ $option->name }}
        </option>
        @else
        <option value="{{ $option->id }}" {{ q_item_select($sub, $option->id, $filter_values) }}>{{ $option->name }}
        </option>
        @endif
        @endforeach
    </select>

    @elseif(isset($sub->type) && $sub->type == 'client_select')
    <select name="filter_{{ $sub->key }}{{ isset($sub->multiple) && $sub->multiple==true ? '[]' : '' }}"
        id="filter_{{ $sub->key }}" class="form-control select2"
        {{ isset($sub->multiple) && $sub->multiple==true ? 'multiple="multiple"' : '' }}>
        <option value="">Select</option>
        @foreach ($clients as $client)
        <option value="{{ $client->id }}" {{ q_item_select($sub, $client->id, $filter_values) }}>
            {{ $client->profile->first_name.' '.$client->profile->last_name  }}</option>
        @endforeach
    </select>

    @endif
</div>