<div class="row mt-3 mb-4 filter-controls">
    <div class="col-md-3">
        @if(isset($filters_json->column1))
        @foreach ($filters_json->column1 as $item)
        <div class="form-group" id="filter_{{ $item->key }}_wrap" style="display: none;">
            @if(isset($item->type) && $item->key != 'name')
            <label class="control-label" for="filter_{{ $item->key }}"> {{ $item->label }}</label>
            @endif

            @if($item->key == 'name')

            <label class="control-label" for="filter_first_name">First Name</label>
            <input type="text" name="filter_first_name" class="form-control"
                value="{{ isset($filter_values['filter_first_name']) ? $filter_values['filter_first_name'] : '' }}" />

            <label class="control-label" for="filter_last_name">Last Name</label>
            <input type="text" name="filter_last_name" class="form-control"
                value="{{ isset($filter_values['filter_last_name']) ? $filter_values['filter_last_name'] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'text')
            <input type="text" name="filter_{{ $item->key }}"
                class="form-control {{ isset($item->phone) && $item->phone ? 'phone' : '' }}"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'number')
            <input type="number" name="filter_{{ $item->key }}"
                class="form-control {{ isset($item->phone) && $item->phone ? 'phone' : '' }}"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'email')
            <input type="email" name="filter_{{ $item->key }}" class="form-control"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'url')
            <input type="url" name="filter_{{ $item->key }}" class="form-control"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'textarea')
            <textarea name="filter_{{ $item->key }}" id="filter_{{ $item->key }}" rows="3"
                class="form-control">{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}</textarea>

            @elseif(isset($item->type) && $item->type == 'select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @if(isset($item->options))
                @foreach ($item->options as $option)
                <option value="{{ $option }}" {{ q_item_select($item, $option, $filter_values) }}>{{ $option }}</option>
                @endforeach
                @endif
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'checkbox')
            <div class="checkboxes">
                @foreach ($item->options as $option)
                <div class="form-check pl-0">
                    <input class="{{ isset($item->single) && $item->single ? 'chs' : '' }}" type="checkbox"
                        name="filter_{{ $item->key }}[]" value="{{ $option }}"
                        {{ q_item_check($item, $option, $filter_values) }} /> {{ $option }}
                </div>
                @endforeach
            </div>

            @elseif(isset($item->type) && $item->type == 'number_range')
            <div class="input-group">
                <input name="filter_{{ $item->key }}_from" class="form-control text-center" placeholder="From"
                    type="number"
                    value="{{ isset($filter_values['filter_'.$item->key.'_from']) ? $filter_values['filter_'.$item->key.'_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_{{ $item->key }}_to" class="form-control text-center" placeholder="To" type="number"
                    value="{{ isset($filter_values['filter_'.$item->key.'_to']) ? $filter_values['filter_'.$item->key.'_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'date_range')
            <div class="input-group">
                <input name="filter_{{ $item->key }}_from" class="form-control text-center date" placeholder="From"
                    type="text"
                    value="{{ isset($filter_values['filter_'.$item->key.'_from']) ? $filter_values['filter_'.$item->key.'_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_{{ $item->key }}_to" class="form-control text-center date" placeholder="To"
                    type="text"
                    value="{{ isset($filter_values['filter_'.$item->key.'_to']) ? $filter_values['filter_'.$item->key.'_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'master_select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($masters[$item->key] as $option)
                @if(isset($item->value_field) && $item->value_field == 'name')
                <option value="{{ $option->name }}" {{ q_item_select($item, $option->name, $filter_values) }}>
                    {{ $option->name }}</option>
                @else
                <option value="{{ $option->id }}" {{ q_item_select($item, $option->id, $filter_values) }}>
                    {{ $option->name }}</option>
                @endif
                @endforeach
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'client_select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}" class="form-control select2"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($clients as $client)
                <option value="{{ $client->id }}" {{ q_item_select($item, $client->id, $filter_values) }}>
                    {{ $client->userdetail->company_name.' - '.$client->userdetail->first_name.' '.$client->userdetail->last_name  }}
                </option>
                @endforeach
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>
            @endif

            @if(isset($item->description))
            <small>{!! $item->description !!}</small>
            @endif

            @if(isset($item->other_option) && $item->other_option)
            <input type="text" name="other_{{ str_replace("_id","",$item->key) }}" class="form-control"
                placeholder="Please specify" id="other_{{ $item->key }}"
                value="{{ isset($filter_values['other_'.str_replace("_id","",$item->key)]) ? $filter_values['other_'.str_replace("_id","",$item->key)] : '' }}"
                style="{{ isset($filter_values['filter_'.$item->key]) && $filter_values['filter_'.$item->key] == -1 ? '' : 'display:none;' }}" />
            @endif

        </div>
        @endforeach

        <div class="form-group" id="filter_brands_wrap"
            style="display: {{ isset($filter_values['filter_brands']) ? 'block' : 'none'  }};">
            <label class="control-label" for="filter_brands">Brands</label>
            <select name="filter_brands[]" id="filter_brands" class="form-control select2" multiple="multiple">
                <option value="">Select</option>
                @foreach ($masters['brands'] as $option)
                <option value="{{ $option->name }}"
                    {{ q_item_select((object)['key' => 'brands', 'multiple' => true], $option->name , $filter_values) }}>
                    {{ $option->name }}</option>
                @endforeach
            </select>
        </div>

        @endif
    </div>
    <div class="col-md-3">
        @if(isset($filters_json->column2))
        @foreach ($filters_json->column2 as $item)
        <div class="form-group" id="filter_{{ $item->key }}_wrap" style="display: none;">
            @if(isset($item->type))
            <label class="control-label" for="filter_{{ $item->key }}"> {{ $item->label }}</label>
            @endif

            @if(isset($item->type) && $item->type == 'text')
            <input type="text" name="filter_{{ $item->key }}"
                class="form-control {{ isset($item->phone) && $item->phone ? 'phone' : '' }}"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'number')
            <input type="number" name="filter_{{ $item->key }}"
                class="form-control {{ isset($item->phone) && $item->phone ? 'phone' : '' }}"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'email')
            <input type="email" name="filter_{{ $item->key }}" class="form-control"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'url')
            <input type="url" name="filter_{{ $item->key }}" class="form-control"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'textarea')
            <textarea name="filter_{{ $item->key }}" id="filter_{{ $item->key }}" rows="3"
                class="form-control">{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}</textarea>

            @elseif(isset($item->type) && $item->type == 'select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @if(isset($item->options))
                @foreach ($item->options as $option)
                <option value="{{ $option }}" {{ q_item_select($item, $option, $filter_values) }}>{{ $option }}</option>
                @endforeach
                @endif
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'checkbox')
            <div class="checkboxes">
                @foreach ($item->options as $option)
                <div class="form-check pl-0">
                    <input class="{{ isset($item->single) && $item->single ? 'chs' : '' }}" type="checkbox"
                        name="filter_{{ $item->key }}[]" value="{{ $option }}"
                        {{ q_item_check($item, $option, $filter_values) }} /> {{ $option }}
                </div>
                @endforeach
            </div>

            @elseif(isset($item->type) && $item->type == 'number_range')
            <div class="input-group">
                <input name="filter_{{ $item->key }}_from" class="form-control text-center" placeholder="From"
                    type="number"
                    value="{{ isset($filter_values['filter_'.$item->key.'_from']) ? $filter_values['filter_'.$item->key.'_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_{{ $item->key }}_to" class="form-control text-center" placeholder="To" type="number"
                    value="{{ isset($filter_values['filter_'.$item->key.'_to']) ? $filter_values['filter_'.$item->key.'_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'date_range')
            <div class="input-group">
                <input name="filter_{{ $item->key }}_from" class="form-control text-center date" placeholder="From"
                    type="text"
                    value="{{ isset($filter_values['filter_'.$item->key.'_from']) ? $filter_values['filter_'.$item->key.'_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_{{ $item->key }}_to" class="form-control text-center date" placeholder="To"
                    type="text"
                    value="{{ isset($filter_values['filter_'.$item->key.'_to']) ? $filter_values['filter_'.$item->key.'_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'master_select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($masters[$item->key] as $option)
                @if(isset($item->value_field) && $item->value_field == 'name')
                <option value="{{ $option->name }}" {{ q_item_select($item, $option->name, $filter_values) }}>
                    {{ $option->name }}</option>
                @else
                <option value="{{ $option->id }}" {{ q_item_select($item, $option->id, $filter_values) }}>
                    {{ $option->name }}</option>
                @endif
                @endforeach
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'client_select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}" class="form-control select2"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($clients as $client)
                <option value="{{ $client->id }}" {{ q_item_select($item, $client->id, $filter_values) }}>
                    {{ $client->profile->company_name.' - '.$client->profile->first_name.' '.$client->profile->last_name  }}
                </option>
                @endforeach
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @endif

            @if(isset($item->description))
            <small>{!! $item->description !!}</small>
            @endif

            @if(isset($item->other_option) && $item->other_option)
            <input type="text" name="other_{{ str_replace("_id","",$item->key) }}" class="form-control"
                placeholder="Please specify" id="other_{{ $item->key }}"
                value="{{ isset($filter_values['other_'.str_replace("_id","",$item->key)]) ? $filter_values['other_'.str_replace("_id","",$item->key)] : '' }}"
                style="{{ isset($filter_values['filter_'.$item->key]) && $filter_values['filter_'.$item->key] == -1 ? '' : 'display:none;' }}" />
            @endif

        </div>
        @endforeach
        @endif
    </div>
    <div class="col-md-3">
        @if(isset($filters_json->column3))
        @foreach ($filters_json->column3 as $item)
        <div class="form-group" id="filter_{{ $item->key }}_wrap" style="display: none;">
            @if(isset($item->type))
            <label class="control-label" for="filter_{{ $item->key }}"> {{ $item->label }}</label>
            @endif

            @if(isset($item->type) && $item->type == 'text')
            <input type="text" name="filter_{{ $item->key }}"
                class="form-control {{ isset($item->phone) && $item->phone ? 'phone' : '' }}"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'number')
            <input type="number" name="filter_{{ $item->key }}"
                class="form-control {{ isset($item->phone) && $item->phone ? 'phone' : '' }}"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'email')
            <input type="email" name="filter_{{ $item->key }}" class="form-control"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'url')
            <input type="url" name="filter_{{ $item->key }}" class="form-control"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'textarea')
            <textarea name="filter_{{ $item->key }}" id="filter_{{ $item->key }}" rows="3"
                class="form-control">{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}</textarea>

            @elseif(isset($item->type) && $item->type == 'select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @if(isset($item->options))
                @foreach ($item->options as $option)
                <option value="{{ $option }}" {{ q_item_select($item, $option, $filter_values) }}>{{ $option }}</option>
                @endforeach
                @endif
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'checkbox')
            <div class="checkboxes">
                @foreach ($item->options as $option)
                <div class="form-check pl-0">
                    <input class="{{ isset($item->single) && $item->single ? 'chs' : '' }}" type="checkbox"
                        name="filter_{{ $item->key }}[]" value="{{ $option }}"
                        {{ q_item_check($item, $option, $filter_values) }} /> {{ $option }}
                </div>
                @endforeach
            </div>

            @elseif(isset($item->type) && $item->type == 'number_range')
            <div class="input-group">
                <input name="filter_{{ $item->key }}_from" class="form-control text-center" placeholder="From"
                    type="number"
                    value="{{ isset($filter_values['filter_'.$item->key.'_from']) ? $filter_values['filter_'.$item->key.'_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_{{ $item->key }}_to" class="form-control text-center" placeholder="To" type="number"
                    value="{{ isset($filter_values['filter_'.$item->key.'_to']) ? $filter_values['filter_'.$item->key.'_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'date_range')
            <div class="input-group">
                <input name="filter_{{ $item->key }}_from" class="form-control text-center date" placeholder="From"
                    type="text"
                    value="{{ isset($filter_values['filter_'.$item->key.'_from']) ? $filter_values['filter_'.$item->key.'_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_{{ $item->key }}_to" class="form-control text-center date" placeholder="To"
                    type="text"
                    value="{{ isset($filter_values['filter_'.$item->key.'_to']) ? $filter_values['filter_'.$item->key.'_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'master_select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($masters[$item->key] as $option)
                @if(isset($item->value_field) && $item->value_field == 'name')
                <option value="{{ $option->name }}" {{ q_item_select($item, $option->name, $filter_values) }}>
                    {{ $option->name }}</option>
                @else
                <option value="{{ $option->id }}"
                    {{-- {{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}
                    --}} {{ q_item_select($item, $option->id, $filter_values) }}>
                    {{ $option->name }}</option>
                @endif
                @endforeach
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'client_select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}" class="form-control select2"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($clients as $client)
                <option value="{{ $client->id }}" {{ q_item_select($item, $client->id, $filter_values) }}>
                    {{ $client->profile->company_name.' - '.$client->profile->first_name.' '.$client->profile->last_name  }}
                </option>
                @endforeach
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @endif

            @if(isset($item->description))
            <small>{!! $item->description !!}</small>
            @endif

            @if(isset($item->other_option) && $item->other_option)
            <input type="text" name="other_{{ str_replace("_id","",$item->key) }}" class="form-control"
                placeholder="Please specify" id="other_{{ $item->key }}"
                value="{{ isset($filter_values['other_'.str_replace("_id","",$item->key)]) ? $filter_values['other_'.str_replace("_id","",$item->key)] : '' }}"
                style="{{ isset($filter_values['filter_'.$item->key]) && $filter_values['filter_'.$item->key] == -1 ? '' : 'display:none;' }}" />
            @endif

            @if(isset($item->sub_fields) && count($item->sub_fields))
            @foreach($item->sub_fields as $sub)
            @include('projects.search-respondents.parts.sub-filters')
            @endforeach
            @endif

            @if($item->key == 'hear_about')
            <input type="text" name="filter_source_name" class="form-control" placeholder="Please specify"
                id="filter_source_name"
                value="{{ isset($filter_values['filter_source_name']) ? $filter_values['filter_source_name'] : '' }}"
                style="{{ isset($filter_values['filter_'.$item->key]) && $filter_values['filter_'.$item->key] == 'Others' ? '' : 'display:none;' }}" />
            @endif
        </div>
        @endforeach
        @endif
    </div>
    <div class="col-md-3">
        @if(isset($filters_json->column4))
        @foreach ($filters_json->column4 as $item)
        <div class="form-group" id="filter_{{ $item->key }}_wrap" style="display: none;">
            @if(isset($item->type))
            <label class="control-label" for="filter_{{ $item->key }}"> {{ $item->label }}</label>
            @endif

            @if(isset($item->type) && $item->type == 'text')
            <input type="text" name="filter_{{ $item->key }}"
                class="form-control {{ isset($item->phone) && $item->phone ? 'phone' : '' }}"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'number')
            <input type="number" name="filter_{{ $item->key }}"
                class="form-control {{ isset($item->phone) && $item->phone ? 'phone' : '' }}"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'email')
            <input type="email" name="filter_{{ $item->key }}" class="form-control"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'url')
            <input type="url" name="filter_{{ $item->key }}" class="form-control"
                value="{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}" />

            @elseif(isset($item->type) && $item->type == 'textarea')
            <textarea name="filter_{{ $item->key }}" id="filter_{{ $item->key }}" rows="3"
                class="form-control">{{ isset($filter_values['filter_'.$item->key]) ? $filter_values['filter_'.$item->key] : '' }}</textarea>

            @elseif(isset($item->type) && $item->type == 'select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @if(isset($item->options))
                @foreach ($item->options as $option)
                <option value="{{ $option }}" {{ q_item_select($item, $option, $filter_values) }}>{{ $option }}</option>
                @endforeach
                @endif
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'checkbox')
            <div class="checkboxes">
                @foreach ($item->options as $option)
                <div class="form-check pl-0">
                    <input class="{{ isset($item->single) && $item->single ? 'chs' : '' }}" type="checkbox"
                        name="filter_{{ $item->key }}[]" value="{{ $option }}"
                        {{ q_item_check($item, $option, $filter_values) }} /> {{ $option }}
                </div>
                @endforeach
            </div>

            @elseif(isset($item->type) && $item->type == 'number_range')
            <div class="input-group">
                <input name="filter_{{ $item->key }}_from" class="form-control text-center" placeholder="From"
                    type="number"
                    value="{{ isset($filter_values['filter_'.$item->key.'_from']) ? $filter_values['filter_'.$item->key.'_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_{{ $item->key }}_to" class="form-control text-center" placeholder="To" type="number"
                    value="{{ isset($filter_values['filter_'.$item->key.'_to']) ? $filter_values['filter_'.$item->key.'_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'date_range')
            <div class="input-group">
                <input name="filter_{{ $item->key }}_from" class="form-control text-center date" placeholder="From"
                    type="text"
                    value="{{ isset($filter_values['filter_'.$item->key.'_from']) ? $filter_values['filter_'.$item->key.'_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_{{ $item->key }}_to" class="form-control text-center date" placeholder="To"
                    type="text"
                    value="{{ isset($filter_values['filter_'.$item->key.'_to']) ? $filter_values['filter_'.$item->key.'_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'master_select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($masters[$item->key] as $option)
                @if(isset($item->value_field) && $item->value_field == 'name')
                <option value="{{ $option->name }}" {{ q_item_select($item, $option->name, $filter_values) }}>
                    {{ $option->name }}</option>
                @else
                <option value="{{ $option->id }}" {{ q_item_select($item, $option->id, $filter_values) }}>
                    {{ $option->name }}</option>
                @endif
                @endforeach
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'client_select')
            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}" class="form-control select2"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($clients as $client)
                <option value="{{ $client->id }}" {{ q_item_select($item, $client->id, $filter_values) }}>
                    {{ $client->profile->company_name.' - '.$client->profile->first_name.' '.$client->profile->last_name  }}
                </option>
                @endforeach
                @if(isset($item->other_option) && $item->other_option)
                <option value="{{ $item->other_option_value ? $item->other_option_value : -1 }}">Other</option>
                @endif
            </select>

            @elseif(isset($item->type) && $item->type == 'children_age_range')
            <label for="any_kids">Any Kids:</label>
            <div class="input-group">
                <input name="filter_child_any_from" class="form-control text-center" placeholder="From" type="number"
                    value="{{ isset($filter_values['filter_child_any_from']) ? $filter_values['filter_child_any_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_child_any_to" class="form-control text-center" placeholder="To" type="number"
                    value="{{ isset($filter_values['filter_child_any_to']) ? $filter_values['filter_child_any_to'] : '' }}" />
            </div>
            <div class="clearfix"></div>
            <label for="all_kids">All Kids:</label>
            <div class="input-group">
                <input name="filter_child_all_from" class="form-control text-center" placeholder="From" type="number"
                    value="{{ isset($filter_values['filter_child_all_from']) ? $filter_values['filter_child_all_from'] : '' }}" />
                <span class="input-group-addon"><i class="icon-angle-right"></i></span>
                <input name="filter_child_all_to" class="form-control text-center" placeholder="To" type="number"
                    value="{{ isset($filter_values['filter_child_all_to']) ? $filter_values['filter_child_all_to'] : '' }}" />
            </div>

            @elseif(isset($item->type) && $item->type == 'job_event' && $item->key=='job_event')

            <select name="filter_{{ $item->key }}{{ isset($item->multiple) && $item->multiple==true ? '[]' : '' }}"
                id="filter_{{ $item->key }}"
                class="form-control {{ isset($item->multiple) && $item->multiple==true ? 'select2' : '' }}"
                {{ isset($item->multiple) && $item->multiple==true ? 'multiple="multiple"' : '' }}>
                <option value="">Select</option>
                @foreach ($masters[$item->key] as $optgroup)
                <optgroup label="{{ $optgroup->title }}">
                    @foreach ($optgroup->events as $option)
                    @if(isset($item->value_field) && $item->value_field == 'name')
                    <option value="{{ $option->label }}" {{ q_item_select($item, $option->label, $filter_values) }}>
                        {{ $option->label }}</option>
                    @else
                    <option value="{{ $option->value }}" {{ q_item_select($item, $option->value, $filter_values) }}>
                        {{ $option->label }}</option>
                    @endif
                    @endforeach
                </optgroup>
                @endforeach
            </select>

            @endif

            @if(isset($item->description))
            <small>{!! $item->description !!}</small>
            @endif

            @if(isset($item->other_option) && $item->other_option)
            <input type="text" name="other_{{ str_replace("_id","",$item->key) }}" class="form-control"
                placeholder="Please specify" id="other_{{ $item->key }}"
                value="{{ isset($filter_values['other_'.str_replace("_id","",$item->key)]) ? $filter_values['other_'.str_replace("_id","",$item->key)] : '' }}"
                style="{{ isset($filter_values['filter_'.$item->key]) && $filter_values['filter_'.$item->key] == -1 ? '' : 'display:none;' }}" />
            @endif

        </div>
        @endforeach
        @endif
    </div>
</div>

@push('scripts')
<script>
    $('#original_country').change(function(){
            var country_id = $(this).val() || 0;
            $.ajax({
                url: "{{ route('ethnicity') }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}',country_id: country_id},
                beforeSend: function(){
                   
                },
                success: function(res){
                    //console.log(res);
                    if(res){
                        $("#ethnic_background").empty();
                        $("#ethnic_background").append('<option value="">Select</option>');
                        $.each(res,function(key,value){
                            $("#ethnic_background").append('<option value="'+value+'">'+value+'</option>');
                        });
                        
                    } else {
                        $("#ethnic_background").append('<option>No Data Available</option>');
                    }
                },
                error: function(err){ console.log(err); }
            });
        });
</script>
@endpush