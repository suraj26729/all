<div class="row mb-4">
  <div class="col-md-3">
    <div class="form-group">
      <ul id="columns_ul" class="m-0">
        <li class="column_li" style="width: 240px; display: inline-block; float: left;">
          <input type="checkbox" class="column" name="column[]" value="respondents.id" checked disabled>&nbsp; ID
        </li>
        @for ($i = 0; $i < 30; $i++) <li class="column_li" style="width: 240px; display: inline-block; float: left;">

          @if(in_array($columns[$i], ['age', 'email', 'all_events']))
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}" checked disabled>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }}

          {{-- @elseif(in_array($columns[$i], ['email_template']) && $query==null)
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}" checked>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }} --}}

          @else
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}"
            {{ isset($query->column) && is_array($query->column) && in_array($columns[$i], $query->column) ? 'checked' : '' }}>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }}
          @endif

          </li>
          @endfor
      </ul>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <ul id="columns_ul" class="m-0">
        @for ($i = 30; $i < 60; $i++) <li class="column_li" style="width: 240px; display: inline-block; float: left;">

          @if(in_array($columns[$i], ['first_name', 'last_name' , 'job_event',
          'home_postcode','home_postsuburb','ethnic_background', 'gender' ]))
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}" checked disabled>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }}

          {{-- @elseif(in_array($columns[$i], ['gender', 'home_phone' ]) && $query==null)
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}" checked>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }} --}}

          @else
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}"
            {{ isset($query->column) && is_array($query->column) && in_array($columns[$i], $query->column) ? 'checked' : '' }}>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }}
          @endif

          </li>
          @endfor
      </ul>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <ul id="columns_ul" class="m-0">
        @for ($i = 60; $i < 90; $i++) <li class="column_li" style="width: 240px; display: inline-block; float: left;">

          @if(in_array($columns[$i], ['mobile_phone', 'occupation']))
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}" checked disabled>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }}

          {{-- @elseif(in_array($columns[$i], ['previous_events']) && $query==null)
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}" checked>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }} --}}

          @else
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}"
            {{ isset($query->column) && is_array($query->column) && in_array($columns[$i], $query->column) ? 'checked' : '' }}>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }}
          @endif

          </li>
          @endfor
      </ul>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <ul id="columns_ul" class="m-0">
        @for ($i = 90; $i < count($columns); $i++) <li class="column_li"
          style="width: 240px; display: inline-block; float: left;">
          @if(in_array($columns[$i], ['state']))
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}" checked disabled>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }}

          {{-- @elseif(in_array($columns[$i], ['source_name' ]) && $query==null)
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}" checked>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }} --}}

          @else
          <input type="checkbox" class="column" name="column[]" value="{{ $columns[$i] }}"
            {{ isset($query->column) && is_array($query->column) && in_array($columns[$i], $query->column) ? 'checked' : '' }}>&nbsp;
          {{ preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", studly_case($columns[$i])) }}
          @endif

          </li>
          @endfor
      </ul>
    </div>
  </div>
</div>