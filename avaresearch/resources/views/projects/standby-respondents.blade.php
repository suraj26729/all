@extends('layouts.app')
@section('title', 'Stand By Respondents :: '.$project->job_name.' (#'.$project->job_number.')')
@section('content')

@include('projects.tab-nav', ['project' => $project, 'current'=>'standby-respondents'])

<div class="container-fluid animatedParent my-3 animateOnce white relative shadow pt-3 pb-3">
    <div class="animated fadeInUpShort go">
        <div class="row pb-2">
            <div class="col-md-12 text-right pl-4">
                <a href="javascript:void(0);" class="btn btn-primary btn-xs btn-move-all-respondents"
                    style="display:none;"><i class="icon-move"></i>
                    Move to All Respondents
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="list_table">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="check_all" /></th>
                        <th>Respondent Id</th>
                        <th>Respondent Name</th>
                        <th>Respondent Last Name</th>
                        <th>Respondent Group Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Data will be placed here -->
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('footer')
<script>
    $(document).ready(function() {
        $("#list_table").DataTable({
            sDom: "Rlfrtip",
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: "{{ route('DTStandByRespondents', $project->uuid) }}",
            columns: [
                { data: "check_row", name: "check_row", orderable: false, searchable: false, width: '20px' },
                { data: "respondent_id", name: "job_events.respondent_id" },
                { data: "name", name: "respondents.first_name" },
                { data: "last_name", name: "respondents.last_name", visible: false },
                { data: "group_name", name: "project_groups.name" },
                { data: "actions", name: "actions", orderable: false, searchable: false }
            ]
        });
    });
    $(document).on('change', '#check_all', function(){
        if(this.checked){
        $('#list_table input[name=check_row]').each(function(){
            $(this).prop('checked', true);
        });
        $('.btn-move-all-respondents').show();
        } else {
        $('#list_table input[name=check_row]').each(function(){
            $(this).prop('checked', false);
        });
        $('.btn-move-all-respondents').hide();
        }
    });
    $(document).on('change', 'input[name=check_row]', function(){
        var checked = $('input[name=check_row]:checked').length;
        if(checked) $('.btn-move-all-respondents').show();
        else $('.btn-move-all-respondents').hide();
    });
    $(document).on("click", ".reverse_btn", function() {
        var id = $(this).data("id");
        var name = $(this).data("name");
        var row = $(this).closest("tr");
        swal(
        {
            title: "Are you sure to reverse job qualified",
            text: "Respondent Name: " + name,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
            $.ajax({
                url: "{{ route('reverseProjectStandByRespondents', $project->uuid) }}",
                type: "DELETE",
                data: { _token: "{{ csrf_token() }}", id: id },
                success: function(res) {
                if (res) {
                    row.remove();
                    swal("Deleted", "You reverse the respondent: " + name, "success");
                }
                },
                error: function(err) {
                console.log(err);
                }
            });
            }
        }
        );
    });
    $(document).on("click", ".btn-move-all-respondents", function() {
        var id = $(this).data("id");
        var name = $(this).data("name");
        var row = $(this).closest("tr");

        var ids = [];
        var els = [];
        $('input[name=check_row]:checked').each(function(i, el){
            els.push(el);
            ids.push($(this).val());
        });

        swal(
        {
            title: "Are you sure to move the selected respondents",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
            $.ajax({
                url: "{{ route('moveProjectStandByRespondents', $project->uuid) }}",
                type: "POST",
                data: { _token: "{{ csrf_token() }}", _method: "PATCH", ids: ids },
                success: function(res) {
                if (res) {
                    $.each(els, (index, item) => {
                    item.closest('tr').remove();
                    });
                    swal("Moved", "You moved the selected respondents", "success");
                }
                },
                error: function(err) {
                console.log(err);
                }
            });
            }
        }
        );
    });
</script>
@endsection