@extends('layouts.app')
@section('title', 'Edit Email Template :: '.$template->title. ' | ' .$group->name.' | '.$project->job_name.'
(#'.$project->job_number.')')
@section('content')

@include('projects.tab-nav', ['project' => $project,'current'=>'email_templates', 'group' => $group])

<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">

    @if(session('success'))
    <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
      <ul class="mb-0">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <form class="form-horizontal"
      action="{{ route('updateProjectGroupEmailTemp', [$project->uuid,$group->uuid,$template->id]) }}" method="POST"
      enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="title" class="control-label">Title <sup class="mandatory">*</sup></label>
            <input class="form-control" id="title" type="text" name="title" value="{{ old('title',$template->title) }}"
              required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="message" class="control-label">Message <sup class="mandatory">*</sup></label>
            <textarea name="message" id="message" rows="10" class="form-control"
              required>{{ old('message',$template->message) }}</textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="attachments" class="control-label">Attachments</label><br />
            <input type="file" name="attachment[]" /><br />
            @if($template->attachments != null && isset($template->attachments[0]))
            <div class="attach_wrap">
              <br />
              <a href="{{ asset('storage/app/public/'.$template->attachments[0]->url) }}"
                target="_blank">{{ basename($template->attachments[0]->url) }}</a>
              <a href="javascript:void(0);" class="remove_attach" data-attach="{{ $template->attachments[0]->id }}"
                data-name="{{basename($template->attachments[0]->url)  }}"><i class="icon-times"></i></a>
              <input type="hidden" name="attachment[]" value="{{ $template->attachments[0]->url }}" />
              <br />
            </div>
            @endif
          </div>
          <div class="form-group">
            <input type="file" name="attachment[]" /><br />
            @if($template->attachments != null && isset($template->attachments[1]))
            <div class="attach_wrap">
              <br />
              <a href="{{ asset('storage/app/public/'.$template->attachments[1]->url) }}"
                target="_blank">{{ basename($template->attachments[1]->url) }}</a>
              <a href="javascript:void(0);" class="remove_attach" data-attach="{{ $template->attachments[1]->id }}"
                data-name="{{basename($template->attachments[1]->url)  }}"><i class="icon-times"></i></a>
              <input type="hidden" name="attachment[]" value="{{ $template->attachments[1]->url }}" />
              <br />
            </div>
            @endif
          </div>
          <div class="form-group">
            <input type="file" name="attachment[]" />
            @if($template->attachments != null && isset($template->attachments[2]))
            <div class="attach_wrap">
              <br />
              <a href="{{ asset('storage/app/public/'.$template->attachments[2]->url) }}"
                target="_blank">{{ basename($template->attachments[2]->url) }}</a>
              <a href="javascript:void(0);" class="remove_attach" data-attach="{{ $template->attachments[2]->id }}"
                data-name="{{basename($template->attachments[2]->url)  }}"><i class="icon-times"></i></a>
              <input type="hidden" name="attachment[]" value="{{ $template->attachments[2]->url }}" />
              <br />
            </div>
            @endif
          </div>
        </div>
      </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <button class="btn btn-primary"><i class="fa fa-save mr-2"></i>Save Changes</button>
      </div>
    </div>
  </div>
  </form>
</div>
</div>
@endsection

@section('footer')
<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'message' );

  jQuery('.remove_attach').click(function(){
    var btn = jQuery(this);
    var id = btn.data('attach');
    var name = btn.data('name');
    swal(
      {
        title: "Are you sure to delete",
        text: "Name: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('deleteEmailTempAttach') }}",
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}", id: id },
            success: function(res) {
              if (res) {
                btn.closest('.attach_wrap').remove();
                swal("Deleted", "You deleted the attachment", "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
</script>
@endsection