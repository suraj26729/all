@extends('layouts.app')
@section('title', 'Create Email Template :: '.$group->name.' | '.$project->job_name.' (#'.$project->job_number.')')
@section('content')

@include('projects.tab-nav', ['project' => $project,'current'=>'create_group_email_template', 'group' => $group])

<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    @if(session('success'))
    <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
      <ul class="mb-0">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <form class="form-horizontal" action="{{ route('storeProjectGroupEmailTemp', [$project->uuid, $group->uuid]) }}"
      method="POST" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="master_template" class="control-label">Email Template</label>
            <select name="master_template" id="master_template" class="form-control select2">
              <option value="">Select Template</option>
              @foreach ($templates as $template)
              <option value="{{ $template->id }}">{{ $template->title }}</option>
              @endforeach
            </select>
          </div>
          <i class="fa fa-spinner fa-spin loading" style="display:none;"></i>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="title" class="control-label">Title <sup class="mandatory">*</sup></label>
            <input class="form-control" id="title" type="text" name="title" value="{{ old('title') }}" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="message" class="control-label">Message <sup class="mandatory">*</sup></label>
            <textarea name="message" id="message" rows="10" class="form-control"
              required>{{ old('message') }}</textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="attachments" class="control-label">Attachments</label><br />
            <input type="file" class="attach_file" name="attachment[]" data-id="1" /><br />
            <div class="attach_data attach_data_1"><br /></div>
            <input type="file" class="attach_file" name="attachment[]" data-id="2" /><br />
            <div class="attach_data attach_data_2"><br /></div>
            <input type="file" class="attach_file" name="attachment[]" data-id="3" />
            <div class="attach_data attach_data_3"><br /></div>

            <input type="hidden" id="attachment_1" name="t_attachment[]" value="" />
            <input type="hidden" id="attachment_2" name="t_attachment[]" value="" />
            <input type="hidden" id="attachment_3" name="t_attachment[]" value="" />

          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <button class="btn btn-primary"><i class="fa fa-save mr-2"></i>Submit</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('footer')
<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'message' );
  $('#master_template').change(function(){
    var id = $(this).val();
    $.ajax({
      url: "{{ route('email-templates.index') }}"+'/'+id+'/json',
      dataType: 'json',
      beforeSend: function(){
        $('.loading').show();
      },
      success: function(res){
        $('#title').val(res.title);
        CKEDITOR.instances.message.setData(res.message);
        if(res.attachments.length){
          for(var i = 0; i < res.attachments.length; i++){
            $('.attach_data_'+(i+1)).html('<a href="{{ asset("storage/app/public") }}/'+res.attachments[i].url+'">'+res.attachments[i].url.split('/').reverse()[0]+'</a>&nbsp;&nbsp;<a href="javascript:void(0);" class="_remove_attach"><i class="icon-times"></i></a><br />');
            $('#attachment_'+(i+1)).val(res.attachments[i].url);
          }
        } else {
          for(var i = 0; i < 3; i++){
            $('.attach_data_'+(i+1)).html('');
            $('#attachment_'+(i+1)).val('');
          }
        }
      },
      error: function(err){
        console.log(err);
        alert(err.responseText);
      },
      complete: function(){
        $('.loading').hide();
      }
    });
  });
  $(document).on('click', '._remove_attach', function(){
    $(this).closest('.attach_data').html('<br/>');
  });
  $('.attach_file').change(function(){
    var id = $(this).data('id');
    $('.attach_data_'+id).html('<br/>');
    $('#attachment_'+id).val('');
  });
</script>
@endsection