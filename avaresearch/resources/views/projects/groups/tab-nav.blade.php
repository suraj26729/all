<header class="white relative shadow">
    <div class="container-fluid">
        <div class="row">
            <ul class="nav nav-material" role="tablist">
                <li>
                    <a class="nav-link {{ $current=='group_detail' ? 'active':'' }}"
                        href="{{ route('editGroup', [$project->uuid, $group->uuid]) }}" role="tab"><i
                            class="icon icon-cog"></i>Details</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='document_category' ? 'active':'' }}" href="#" role="tab"><i
                            class="icon-document-text2"></i>Document Category</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='quota' ? 'active':'' }}" href="#" role="tab"><i
                            class="icon icon-question"></i>Quota</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='create_group_email_template' ? 'active':'' }}"
                        href="{{ route('createProjectGroupEmailTemp', [$project->uuid, $group->uuid]) }}" role="tab"><i
                            class="icon-mail-envelope"></i> Create
                        Template</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='create_group_sms_template' ? 'active':'' }}"
                        href="{{ route('createProjectGroupSMSTemp', [$project->uuid, $group->uuid]) }}" role="tab"><i
                            class="icon-textsms"></i> Create SMS
                        Template</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='group_email_templates' ? 'active':'' }}"
                        href="{{ route('indexProjectGroupEmailTemp', [$project->uuid, $group->uuid]) }}" role="tab">
                        <i class="icon-mail-envelope-open"></i> Manage Template
                    </a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='group_sms_templates' ? 'active':'' }}"
                        href="{{ route('indexProjectGroupSMSTemp', [$project->uuid, $group->uuid]) }}" role="tab"><i
                            class="icon-message"></i> Manage SMS
                        Template</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='group_all_respondents' ? 'active':'' }}"
                        href="{{ route('indexProjectGroupAllRespondents', [$project->uuid, $group->uuid]) }}"
                        role="tab"><i class="icon icon-check"></i>All
                        Respondents</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='group_standby_respondents' ? 'active':'' }}"
                        href="{{ route('indexProjectGroupStandByRespondents', [$project->uuid, $group->uuid]) }}"
                        role="tab"><i class="icon icon-stop-watch"></i>Stand By
                        Respondents</a>
                </li>
            </ul>
        </div>
    </div>
</header>