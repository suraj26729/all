@extends('layouts.app')
@section('title', 'All Respondents :: '.$group->name.' | '.$group->project->job_name.'
(#'.$group->project->job_number.')')
@section('content')

@include('projects.tab-nav', ['project' => $project,'current'=>'group_all_respondents', 'group' => $group])

<div class="container-fluid animatedParent my-3 animateOnce white relative shadow pt-3 pb-3">
    <div class="animated fadeInUpShort go">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="list_table">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="check_all" /></th>
                        <th>Respondent Id</th>
                        <th>Respondent Name</th>
                        <th>Respondent Last Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Data will be placed here -->
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 pt-2 action-wrap" style="display: none;">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="checkbox" name="include_standby" id="include_standby" value="Yes"> Include Standby
                        Respondents As Well
                    </div>
                </div>
                <div class="col-md-9" style="line-height: 3em;">
                    <button type="button" class="btn btn-primary" id="btn-send-email"
                        data-project="{{ $project->id }}"><i class="icon-email mr-2"></i> Send E-mail</button>
                    <button type="button" class="btn btn-primary" id="btn-send-sms" data-project="{{ $project->id }}"><i
                            class="icon-textsms mr-2"></i> Send SMS</button>
                    <button type="button" class="btn btn-primary" id="btn-create-attendee-doc"
                        data-project="{{ $project->id }}"><i class="icon-file-word-o mr-2"></i> Generate Attendee
                        Document</button>
                    <button type="button" class="btn btn-primary" id="btn-create-bank-details"
                        data-project="{{ $project->id }}"><i class="icon-bank mr-2"></i> Generate Bank Details</button>
                    <br />
                    <button type="button" class="btn btn-primary" id="btn-no-show" data-project="{{ $project->id }}"><i
                            class="icon-bank mr-2"></i> No Show</button>
                    <button type="button" class="btn btn-primary" id="btn-excellent"
                        data-project="{{ $project->id }}"><i class="icon-bank mr-2"></i> Excellent</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')

@include('projects.search-respondents.modals.send-email')

@include('projects.search-respondents.modals.send-sms')

<script>
    $(document).ready(function() {
        $("#list_table").DataTable({
            sDom: "Rlfrtip",
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: "{{ route('DTGroupAllRespondents', [$group->project->uuid, $group->uuid]) }}",
            columns: [
                { data: "check_row", name: "check_row", orderable: false, searchable: false, width: '20px' },
                { data: "respondent_id", name: "job_events.respondent_id" },
                { data: "name", name: "respondents.first_name" },
                { data: "last_name", name: "respondents.last_name", visible: false },
                { data: "actions", name: "actions", orderable: false, searchable: false }
            ]
        });
    });
    $(document).on("click", ".reverse_btn", function() {
        var id = $(this).data("id");
        var name = $(this).data("name");
        var row = $(this).closest("tr");
        swal(
        {
            title: "Are you sure to reverse job qualified",
            text: "Respondent Name: " + name,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
            $.ajax({
                url: "{{ route('reverseProjectGroupAllRespondents', [$group->project->uuid, $group->uuid]) }}",
                type: "DELETE",
                data: { _token: "{{ csrf_token() }}", id: id },
                success: function(res) {
                    if (res) {
                        row.remove();
                        swal("Deleted", "You reverse the respondent: " + name, "success");
                    }
                },
                error: function(err) {
                console.log(err);
                }
            });
            }
        }
        );
    });
    $(document).on('change', '#check_all', function(){
        if(this.checked){
        $('#list_table input[name=row_check]').each(function(){
            $(this).prop('checked', true);
        });
        $('.action-wrap').show();
        } else {
        $('#list_table input[name=row_check]').each(function(){
            $(this).prop('checked', false);
        });
        $('.action-wrap').hide();
        }
    });
    $(document).on('change', 'input[name=row_check]', function(){
        var checked = $('input[name=row_check]:checked').length;
        if(checked) $('.action-wrap').show();
        else $('.action-wrap').hide();
    });

    $(document).on('click', '#btn-send-email', function(){
        var checked = $('#list_table input[name=row_check]:checked').length;
        if(checked){
            let emails = [];
            $('#list_table input[name=row_check]:checked').each(function(i, el){
                var email = $(this).data('email');
                if(email!='')
                    emails.push(email);
            });
            $('#send-email-form #to').val(emails.join(','));
            $('#send-email-form .total_res').html(emails.length);
            $('#send-email-modal').modal('show');
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    $(document).on('click', '#btn-send-sms', function(){
        var checked = $('#list_table input[name=row_check]:checked').length;
        if(checked){
            let phones = [];
            $('#list_table input[name=row_check]:checked').each(function(i, el){
                var phone = $(this).data('mobile');
                if(phone != '')
                    phones.push(phone);
            });
            $('#send-sms-form #sms_to').val(phones.join(','));
            $('#send-sms-form .total_res').html(phones.length);
            $('#send-sms-modal').modal('show');
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    $(document).on('click', '#btn-create-attendee-doc', function(){
        var btn = $(this);
        var btntext = btn.html();
        var checked = $('#list_table input[name=row_check]:checked').length;
        if(checked){
            var ids = [];
            $('#list_table input[name=row_check]:checked').each(function(i, el){
                var id = $(this).val();
                if(id != '')
                    ids.push(id);
            });
            var include_standby = 0;
            include_standby = $('#include_standby').is(':checked') ? 1 : 0; 
            $.ajax({
                url: "{{ route('createAttendeeDocument', $group->project->uuid) }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}', ids: ids, include_standby: include_standby },
                beforeSend: function(){
                    btn.html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
                },
                success: function(url){
                    if(url) {
                        //document.location = url;
                        swal("Document Generated", "Attendee document has been generated successfully", "success");
                    }
                },
                error: function(err){
                    console.log(err);
                    alert(err.responseText);
                },
                complete: function(){
                    btn.html(btntext).prop('disabled', false);
                }
            });
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    $(document).on('click', '#btn-create-bank-details', function(){
        var btn = $(this);
        var btntext = btn.html();
        var checked = $('#list_table input[name=row_check]:checked').length;
        if(checked){
            var ids = [];
            $('#list_table input[name=row_check]:checked').each(function(i, el){
                var id = $(this).val();
                if(id != '')
                    ids.push(id);
            });
            var include_standby = 0;
            include_standby = $('#include_standby').is(':checked') ? 1 : 0; 
            $.ajax({
                url: "{{ route('createBankDocument', $group->project->uuid) }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}', ids: ids, include_standby: include_standby },
                beforeSend: function(){
                    btn.html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
                },
                success: function(url){
                    if(url) {
                        //document.location = url;
                        swal("Document Generated", "Bank document has been generated successfully", "success");
                    }
                },
                error: function(err){
                    console.log(err);
                    alert(err.responseText);
                },
                complete: function(){
                    btn.html(btntext).prop('disabled', false);
                }
            });
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    $(document).on('click', '#btn-no-show', function(){
        var btn = $(this);
        var btntext = btn.html();
        var checked = $('#list_table input[name=row_check]:checked').length;
        if(checked){
            var ids = [];
            $('#list_table input[name=row_check]:checked').each(function(i, el){
                var id = $(this).val();
                if(id != '')
                    ids.push(id);
            });
            $.ajax({
                url: "{{ route('respondentFlagUpdate', 'do_no_show') }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}', _method: 'PATCH', ids: ids },
                beforeSend: function(){
                    btn.html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
                },
                success: function(res){
                    if(res) {
                        swal("Updated", "Respondents are marked as 'No Show'", "success");
                    }
                },
                error: function(err){
                    console.log(err);
                    alert(err.responseText);
                },
                complete: function(){
                    btn.html(btntext).prop('disabled', false);
                }
            });
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
    $(document).on('click', '#btn-excellent', function(){
        var btn = $(this);
        var btntext = btn.html();
        var checked = $('#list_table input[name=row_check]:checked').length;
        if(checked){
            var ids = [];
            $('#list_table input[name=row_check]:checked').each(function(i, el){
                var id = $(this).val();
                if(id != '')
                    ids.push(id);
            });
            $.ajax({
                url: "{{ route('respondentFlagUpdate', 'excellent_candidate') }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}', _method: 'PATCH', ids: ids },
                beforeSend: function(){
                    btn.html('<i class="fa fa-spinner fa-spin"></i>').prop('disabled', true);
                },
                success: function(res){
                    if(res) {
                        swal("Updated", "Respondents are marked as 'Excellent'", "success");
                    }
                },
                error: function(err){
                    console.log(err);
                    alert(err.responseText);
                },
                complete: function(){
                    btn.html(btntext).prop('disabled', false);
                }
            });
        } else {
            swal("", "Please check any one respondent", "warning");
        }
    });
</script>
@endsection