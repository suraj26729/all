@extends('layouts.app')
@section('title', 'Create Group :: '.$project->job_name.' (#'.$project->job_number.')')
@section('content')
<div class="row pb-2">
    <div class="col-md-6">
        <a href="{{ route('indexGroups', $project->uuid) }}" class="btn btn-info btn-xs"><i class="icon-arrow_back"></i>
            Back to
            Groups</a>
        <a href="{{ route('projects.index') }}" class="btn btn-secondary btn-xs"><i class="icon-arrow_back"></i> Back to
            Projects</a>
    </div>
</div>
<div class="container-fluid animatedParent my-3 animateOnce white relative shadow pt-3 pb-3">
    <div class="animated fadeInUpShort go">
        @if(session('success'))
        <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
        @endif

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (session('errors'))
        {{ session('errors')->first('message') }}
        @endif

        <form class="form-horizontal" action="{{ route('storeGroup', $project->uuid) }}" method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name" class="control-label">Name <sup class="mandatory">*</sup></label>
                        <input class="form-control" id="name" type="text" name="name" value="{{ old('name') }}"
                            required />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="location" class="control-label">Location</label>
                        <select name="location" id="location" class="form-control">
                            <option value="">Select</option>
                            @foreach($locations as $location)
                            <option value="{{ $location->id }}">{{ $location->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="venue_address" class="control-label">Venue Address <sup
                                class="mandatory">*</sup></label>
                        <input class="form-control" id="venue_address" type="text" name="venue_address"
                            value="{{ old('venue_address') }}" required />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="venue_phone" class="control-label">Venue Phone <sup
                                class="mandatory">*</sup></label>
                        <input class="form-control" id="venue_phone" type="text" name="venue_phone"
                            value="{{ old('venue_phone') }}" onkeypress="return isNumber(event);" required />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="client_phone" class="control-label">Client Phone</label>
                        <input class="form-control" id="client_phone" type="text" name="client_phone"
                            value="{{ old('client_phone') }}" onkeypress="return isNumber(event);" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="research_phone" class="control-label">Researcher Phone</label>
                        <input class="form-control" id="research_phone" type="text" name="research_phone"
                            value="{{ old('research_phone') }}" onkeypress="return isNumber(event);" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="client_email" class="control-label">Client email</label>
                        <input class="form-control" id="client_email" type="email" name="client_email"
                            value="{{ old('client_email') }}" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="research_email" class="control-label">Researcher Email</label>
                        <input class="form-control" id="research_email" type="email" name="research_email"
                            value="{{ old('research_email') }}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="incentive" class="control-label">Incentive <sup class="mandatory">*</sup></label>
                        <input class="form-control" id="incentive" type="text" name="incentive"
                            value="{{ old('incentive') }}" required />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="other" class="control-label">Remarks</label>
                        <textarea name="other" id="other" rows="5" class="form-control">{{ old('other') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="group_date" class="control-label">Date</label>
                        <input class="form-control date" id="group_date" type="text" name="group_date"
                            value="{{ old('group_date') }}" autocomplete="off" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="duration" class="control-label">Duration <sup class="mandatory">*</sup></label>
                        <input class="form-control" id="duration" type="text" name="duration"
                            value="{{ old('duration') }}" required />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="group_time" class="control-label">Time</label>
                        <input class="form-control time" id="group_time" type="text" name="group_time"
                            value="{{ old('group_time') }}" autocomplete="off" />
                    </div>
                </div>
                <div class="col diff_time_wrap">
                    <div class="form-group">
                        <label for="different_time" class="control-label">Time 1</label>
                        <div class="input-group">
                            <input class="form-control time" type="text" name="time[]" autocomplete="off" />
                            <div class="input-group-appened">
                                <button type="button" class="btn btn-secondary addTime"><i class="icon-plus"></i>
                                    Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-primary" id="submit_btn"><i class="fa fa-save mr-2"></i>Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('footer')
<script>
    var i = 2;
    $('.addTime').click(function(){
        var html = `<div class="form-group">
                        <label class="control-label">Time ${i}</label>
                        <div class="input-group">
                            <input class="form-control time" type="text" name="time[]" autocomplete="off" />
                            <div class="input-group-appened">
                                <button type="button" class="btn btn-danger removeTime"><i class="icon-times"></i>
                                    Remove</button>
                            </div>
                        </div>
                    </div>`;
        $('.diff_time_wrap').append(html);
        init_time();
        i++;
    });
    $(document).on('click','.removeTime', function(){
        $(this).closest('.form-group').remove();
        i--; 
    });

    function init_time(){
        jQuery(".time").datetimepicker({
            format: "H:i",
            datepicker: false
        });
    }
</script>
@endsection