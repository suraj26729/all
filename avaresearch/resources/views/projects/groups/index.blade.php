@extends('layouts.app')
@section('title', 'Groups :: '.$project->job_name.' (#'.$project->job_number.')')
@section('content')

@include('projects.tab-nav', ['project' => $project,'current'=>'group'])

<div class="container-fluid animatedParent my-3 animateOnce white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    @if(session('success'))
    <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
    @endif
    @if(auth()->user()->type != 'client')
    <div class="row pb-2">
      <div class="col-md-12 text-right pl-4">
        <a href="{{ route('createGroup', $project->uuid) }}" class="btn btn-primary btn-xs"><i class="icon-plus"></i>
          Create New Group</a>
      </div>
    </div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="list_table">
        <thead>
          <tr>
            <th visible="false">Name</th>
            <th>Name</th>
            <th>Location</th>
            <th>Incentive</th>
            <th>Remark</th>
            <th>Created By</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection

@section('footer')
<script>
  $(document).ready(function() {
        $("#list_table").DataTable({
        sDom: "Rlfrtip",
        processing: true,
        serverSide: true,
        pageLength: 10,
        ajax: "{{ route('DTProjectGroups', $project->uuid) }}",
        columns: [
            { data: "name", name: "name", visible: false },
            { data: "group_name", name: "name", searchable: false },
            { data: "mlocation", name: "master_states.name" },
            { data: "incentive", name: "incentive" },
            { data: "other", name: "other" },
            { data: "creator_name", name: "created_by" },
            { data: "created_at", name: "created_at" },
            { data: "actions", name: "actions", orderable: false, searchable: false }
        ],
        order: [[0, "asc"]]
        });
    });
    $(document).on("click", ".delete_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");
    swal(
      {
        title: "Are you sure to delete",
        text: "Job Name: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('indexGroups', $project->uuid) }}/" + id,
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}" },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted", "You deleted the group: " + name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
</script>
@endsection