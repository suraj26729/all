@extends('layouts.app')
@section('title', 'Time Allocation :: '.$project->job_name.' (#'.$project->job_number.')')
@section('content')

@include('projects.tab-nav', ['project' => $project, 'current'=>'Time Allocation'])

<div class="container-fluid animatedParent my-3 animateOnce white relative shadow pt-3 pb-3">
    <div class="animated fadeInUpShort go">
        <div class="row pb-2">
            <div class="col-md-12 text-right pl-4">
                <a href="javascript:void(0);" class="btn btn-primary btn-xs btn-move-all-respondents"
                    style="display:none;"><i class="icon-move"></i>
                    Time Allocation
                </a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="list_table">
                <thead>
                    <tr>
                        
                        <th>Project ID</th>
                        <th>Staff Name</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Deveoted Time</th>
                        <th>Created Date</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Data will be placed here -->
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@section('footer')
<script>
    $(document).ready(function() {
        $("#list_table").DataTable({
            sDom: "Rlfrtip",
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: "{{ route('DTTimeAllocation', $project->uuid) }}",
            columns: [
                
                { data: "job_number", name: "projects.job_number" },
                { data: "staff_name", name: "staff_name" ,orderable : false},
                { data: "start_time", name: "start_time" },
                { data: "end_time", name: "end_time" },
                { data: "deveoted_time", name: "deveoted_time" },
                { data: "created_at", name: "tracking_times.created_at" }
            ],
          order: [[5,'DESC']]
        });
    });

                 $(function(){  
                        $('#countdown').countup();  
                    });
                    (function($){  
                      // Number of seconds in every time division
                      var days  = 24*60*60,
                        hours = 60*60,
                        minutes = 60;
                      
                      // Creating the plugin
                      $.fn.countup = function(prop){
                        
                        var options = $.extend({
                          callback  : function(){},
                          start   : new Date('00:00:00')
                        },prop);                
                        var passed = 0, d, h, m, s, 
                          positions;

                        // Initialize the plugin
                        init(this, options);
                        
                        positions = this.find('.position');
                        
                        (function tick(){   
                          passed = Math.floor((new Date() - options.start) / 1000);
                            
                          // Number of days passed
                          d = Math.floor(passed / days);
                          updateDuo(0, 1, d);
                          passed -= d*days;
                          
                          // Number of hours left
                          h = Math.floor(passed / hours);
                          updateDuo(2, 3, h);
                          passed -= h*hours;
                          
                          // Number of minutes left
                          m = Math.floor(passed / minutes);
                          updateDuo(4, 5, m);
                          passed -= m*minutes;
                          
                          // Number of seconds left
                          s = passed;
                          updateDuo(6, 7, s);
                          
                          // Calling an optional user supplied callback
                          options.callback(d, h, m, s);
                          
                          // Scheduling another call of this function in 1s
                          setTimeout(tick, 1000);
                        })();
                        
                        // This function updates two digit positions at once
                        function updateDuo(minor,major,value){
                          switchDigit(positions.eq(minor),Math.floor(value/10)%10);
                          switchDigit(positions.eq(major),value%10);
                        }
                        
                        return this;
                      };


                      function init(elem, options){
                        elem.addClass('countdownHolder');

                        // Creating the markup inside the container
                        $.each(['Days','Hours','Minutes','Seconds'],function(i){
                          $('<span class="count'+this+'">').html(
                            '<span class="position">\
                              <span class="digit static">0</span>\
                            </span>\
                            <span class="position">\
                              <span class="digit static">0</span>\
                            </span>'
                          ).appendTo(elem);
                          
                          if(this!="Seconds"){
                            elem.append('<span class="countDiv countDiv'+i+'"></span>');
                          }
                        });

                      }

                      // Creates an animated transition between the two numbers
                      function switchDigit(position,number){
                        
                        var digit = position.find('.digit')
                        
                        if(digit.is(':animated')){
                          return false;
                        }
                        
                        if(position.data('digit') == number){
                          // We are already showing this number
                          return false;
                        }
                        
                        position.data('digit', number);
                        
                        var replacement = $('<span>',{
                          'class':'digit',
                          css:{
                            top:'-2.1em',
                            opacity:0
                          },
                          html:number
                        });
                        
                        // The .static class is added when the animation
                        // completes. This makes it run smoother.
                        
                        digit
                          .before(replacement)
                          .removeClass('static')
                          .animate({top:'2.5em',opacity:0},'fast',function(){
                            digit.remove();
                          })

                        replacement
                          .delay(100)
                          .animate({top:0,opacity:1},'fast',function(){
                            replacement.addClass('static');
                          });
                      }
                    })(jQuery);


    /*
    $(document).on('change', '#check_all', function(){
        if(this.checked){
        $('#list_table input[name=check_row]').each(function(){
            $(this).prop('checked', true);
        });
        $('.btn-move-all-respondents').show();
        } else {
        $('#list_table input[name=check_row]').each(function(){
            $(this).prop('checked', false);
        });
        $('.btn-move-all-respondents').hide();
        }
    });
    $(document).on('change', 'input[name=check_row]', function(){
        var checked = $('input[name=check_row]:checked').length;
        if(checked) $('.btn-move-all-respondents').show();
        else $('.btn-move-all-respondents').hide();
    });
    $(document).on("click", ".reverse_btn", function() {
        var id = $(this).data("id");
        var name = $(this).data("name");
        var row = $(this).closest("tr");
        swal(
        {
            title: "Are you sure to reverse job qualified",
            text: "Respondent Name: " + name,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
            $.ajax({
                url: "{{ route('reverseProjectStandByRespondents', $project->uuid) }}",
                type: "DELETE",
                data: { _token: "{{ csrf_token() }}", id: id },
                success: function(res) {
                if (res) {
                    row.remove();
                    swal("Deleted", "You reverse the respondent: " + name, "success");
                }
                },
                error: function(err) {
                console.log(err);
                }
            });
            }
        }
        );
    });
    $(document).on("click", ".btn-move-all-respondents", function() {
        var id = $(this).data("id");
        var name = $(this).data("name");
        var row = $(this).closest("tr");

        var ids = [];
        var els = [];
        $('input[name=check_row]:checked').each(function(i, el){
            els.push(el);
            ids.push($(this).val());
        });

        swal(
        {
            title: "Are you sure to move the selected respondents",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
            $.ajax({
                url: "{{ route('moveProjectStandByRespondents', $project->uuid) }}",
                type: "POST",
                data: { _token: "{{ csrf_token() }}", _method: "PATCH", ids: ids },
                success: function(res) {
                if (res) {
                    $.each(els, (index, item) => {
                    item.closest('tr').remove();
                    });
                    swal("Moved", "You moved the selected respondents", "success");
                }
                },
                error: function(err) {
                console.log(err);
                }
            });
            }
        }
        );
    });*/
</script>
@endsection