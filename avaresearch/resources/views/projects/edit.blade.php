@extends('layouts.app')
@section('title', 'Edit Project :: '.$project->job_name.' (#'.$project->job_number.')')
@section('content')

@include('projects.tab-nav', ['project' => $project, 'current' => 'detail'])

<div class="container-fluid animatedParent my-3 animateOnce white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">

    @if(session('success'))
    <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    @if (session('errors'))
    {{ session('errors')->first('message') }}
    @endif

    <form class="form-horizontal" action="{{ route('projects.update', $project->id) }}" method="POST"
      enctype="multipart/form-data">
      @csrf @method('PUT')
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="job_number" class="control-label">Job Number <sup class="mandatory">*</sup></label>
            <input class="form-control" id="job_number" type="text" name="job_number"
              value="{{ old('job_number', $project->job_number) }}" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="job_name" class="control-label">Job Name <sup class="mandatory">*</sup></label>
            <input class="form-control" id="job_name" type="text" name="job_name"
              value="{{ old('job_name', $project->job_name) }}" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="subject" class="control-label">Subject <sup class="mandatory">*</sup></label>
            <input class="form-control" id="subject" type="text" name="subject"
              value="{{ old('subject', $project->subject) }}" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="description" class="control-label">Description <sup class="mandatory">*</sup></label>
            <textarea name="description" id="description" rows="5" class="form-control"
              required>{{ old('description', $project->description) }}</textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- <div class="col-md-6">
          <div class="form-group">
            <label for="client_id" class="control-label">Client <sup class="mandatory">*</sup></label>
            <select name="client_id" id="client_id" class="form-control" required>
              <option value="">Select Client</option>
              @foreach($clients as $client)
              @if($client->id == $project->client_id)
              <option value="{{ $client->id }}" data-company="{{ $client->profile->company_name }}" selected>
                {{ $client->profile->first_name.' '.$client->profile->last_name }}
              </option>
              @else
              <option value="{{ $client->id }}" data-company="{{ $client->profile->company_name }}">
                {{ $client->profile->first_name.' '.$client->profile->last_name }}
              </option>
              @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="company_name" class="control-label">Company Name</label>
            <input class="form-control" type="text" id="company_name" placeholder="Please select client first"
              disabled />
          </div>
        </div> -->

        <div class="col-md-6">
          <div class="form-group">
            <label for="company_name" class="control-label">Company <sup class="mandatory">*</sup></label>
            <select name="client_id" id="company_name" class="form-control" required>
              <option value="">Select Company</option>
              @foreach($clients as $client)
              @if($client->id == $project->client_id)
              <option value="{{ $client->id }}" data-client="{{ $client->profile->first_name.' '.$client->profile->last_name }}" selected>
                {{ $client->profile->company_name}}
              </option>
              @else
              <option value="{{ $client->id }}" data-client="{{ $client->profile->first_name.' '.$client->profile->last_name }}">
                {{ $client->profile->company_name}}
              </option>
              @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="client_id" class="control-label">Client</label>
            <input class="form-control" type="text"  id="client_id" placeholder="Please select company first"
              disabled />
          </div>
        </div>



      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="location" class="control-label">Location <sup class="mandatory">*</sup></label>
            <input class="form-control" id="location" type="text" name="location"
              value="{{ old('location', $project->location) }}" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="contact_phone" class="control-label">Contact Phone <sup class="mandatory">*</sup></label>
            <input class="form-control mobile" id="contact_phone" type="text" name="contact_phone"
              value="{{ old('contact_phone', $project->contact_phone) }}" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="contact_email" class="control-label">Contact Email <sup class="mandatory">*</sup></label>
            <input class="form-control" id="contact_email" type="email" name="contact_email"
              value="{{ old('contact_email', $project->contact_email) }}" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="venue_email" class="control-label">Venue Email <sup class="mandatory">*</sup></label>
            <input class="form-control" id="venue_email" type="email" name="venue_email"
              value="{{ old('venue_email', $project->venue_email) }}" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="received_date" class="control-label">Job Received <sup class="mandatory">*</sup></label>
            <input class="form-control date" id="received_date" type="text" name="received_date"
              value="{{ old('received_date', date('Y-m-d', strtotime($project->received_date))) }}" autocomplete="off"
              required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="start_date" class="control-label">Start Date <sup class="mandatory">*</sup></label>
            <input class="form-control date" id="start_date" type="text" name="start_date"
              value="{{ old('start_date', date('Y-m-d', strtotime($project->start_date))) }}" autocomplete="off"
              required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="finish_date" class="control-label">Finish Date <sup class="mandatory">*</sup></label>
            <input class="form-control date" id="finish_date" type="text" name="finish_date"
              value="{{ old('finish_date', date('Y-m-d', strtotime($project->finish_date))) }}" autocomplete="off"
              required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="signed_sheets" class="control-label">Signed Sheets <sup class="mandatory">*</sup></label>
            <select name="signed_sheets" id="signed_sheets" class="form-control" required>
              <option value="">Select</option>
              <option value="1" {{ $project->signed_sheets == 1 ? 'selected' : '' }}>Yes</option>
              <option value="0" {{ $project->signed_sheets == 0 ? 'selected' : '' }}>No</option>
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label">Created By</label>
            <input type="text" disabled class="form-control"
              value="{{ $project->createdBy ? ($project->createdBy->profile->first_name.' '.$project->createdBy->profile->last_name) : '--' }}" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-check mb-2 mr-sm-2">
            <input class="form-check-input" type="checkbox" id="validation_report" name="validation_report"
              {{ $project->validation_report=='Yes' ? 'checked' : '' }} value="Yes" />
            <label class="form-check-label" for="validation_report">
              Validation Report Received
            </label>
          </div>
        </div>
      </div>
      <div class="row mt-3">
        <div class="col-md-6">
          <div class="form-group">
            <button class="btn btn-primary" id="submit_btn"><i class="fa fa-save mr-2"></i>Save Changes</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('footer')
<script>
  $(function(){
          $('#company_name').trigger('change');
        });
        $('#company_name').change(function(){
            $('#client_id').val($('#company_name option:selected').data('client'));
        });
        $('#job_number').change(function(){
            var job_number = $(this).val();
            $.ajax({
                url: "{{ route('checkJobNum') }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}',job_number: job_number, action:'update', current: '{{ $project->id }}' },
                beforeSend: function(){
                    $('small.checknum').remove();
                    $('#submit_btn').prop('disabled', true);
                },
                success: function(res){
                    if(res){
                        $('#job_number').after('<small class="text-danger checknum">Already exists in other project!</small>');
                    } else {
                        $('#job_number').after('<small class="text-success checknum">Available!</small>');
                        $('#submit_btn').removeAttr('disabled');
                    }
                },
                error: function(err){ console.log(err); }
            })
        });

 /* $(function() {
 
  var select = $('select');
  select.html(select.find('option').sort(function(x, y) {
    // to change to descending order switch "<" for ">"
    return $(x).text() > $(y).text() ? 1 : -1;
  }));

  // select default item after sorting (first item)
  // $('select').get(0).selectedIndex = 0;
});*/
</script>
@endsection