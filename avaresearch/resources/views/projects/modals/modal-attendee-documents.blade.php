<div class="modal fade" id="all-attendee-documents" tabindex="-1" role="dialog" aria-labelledby="all-attendee-documents"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">All Group Attendee Documents</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    @forelse(list_all_attendee_documents($project->id) as $item)
                    <li class="mb-3"><a href="{{ asset('public/storage/'.$item->document_name) }}" target="_blank"><i
                                class="fa fa-download mr-2"></i> {{ basename($item->document_name) }}</a>
                    </li>
                    @empty
                    <li>
                        <h3 class="mt-3 text-center mb-0">No documents generated yet.</h3>
                    </li>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div>