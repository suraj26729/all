

<div class="row pb-3">
    <div class="col">
        @if(request()->is('*/groups/*'))
        <a href="{{ route('indexGroups', $project->uuid) }}" class="btn btn-info btn-xs"><i class="icon-arrow_back"></i>
            Back to
            Groups</a>
        @endif
        <a href="{{ route('projects.index') }}" class="btn btn-secondary btn-xs"><i class="icon-arrow_back"></i> Back to
            Projects</a>
    </div>

    @if(!request()->is('*/groups/*') && auth()->user()->type != 'client')
    <div class="col text-right">
        <div class="timer text-right" id="timer"><h3><strong>
        @if(auth()->user()->type == 'staff')
        @if(!session()->has('timetracking_id'))
            <button class="btn btn-success btn-xs " id='start-cronometer'><i class="icon-document-download"></i> Start Job</button>
        @else 
        <button class="btn btn-danger btn-xs " id='reset-timer'><i class="icon-document-download"></i>End Job</button>
        @endif
        @endif

        <button class="btn btn-primary btn-xs create_job_summary_inv_report"><i class="icon-document-download"></i> Job
            Summary
            Individual Report</button>
    </div>
    @endif
</div>

@if(!request()->is('*/groups/*'))
@if(auth()->user()->type != 'client')
<header class="white  mb-2 relative shadow">
    <div class="container-fluid">
        <div class="row">
            <ul class="nav nav-material" role="tablist">
                <li>
                    <a class="nav-link" href="javascript:void(0);" role="tab" data-toggle="modal"
                        data-target="#all-attendee-documents"><i class="icon-document-text3"></i>
                        Preview All
                        Group Attendee Document</a>
                </li>
                {{-- <li>
                    <a class="nav-link" href="javascript:void(0);" role="tab"><i class="icon-download"></i> Job
                        Summary Document</a>
                </li> --}}
                <li>
                    <a class="nav-link {{ $current=='create_email_template' ? 'active':'' }}"
                        href="{{ route('createProjectEmailTemp', $project->uuid) }}" role="tab"><i
                            class="icon-mail-envelope"></i> Create
                        Template</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='create_sms_template' ? 'active':'' }}"
                        href="{{ route('createProjectSMSTemp', $project->uuid) }}" role="tab"><i
                            class="icon-textsms"></i> Create SMS
                        Template</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='email_templates' ? 'active':'' }}"
                        href="{{ route('indexProjectEmailTemp', $project->uuid) }}" role="tab">
                        <i class="icon-mail-envelope-open"></i> Manage Template
                    </a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='sms_templates' ? 'active':'' }}"
                        href="{{ route('indexProjectSMSTemp', $project->uuid) }}" role="tab"><i
                            class="icon-message"></i> Manage SMS
                        Template</a>
                </li>
                <li>
                    <a class="nav-link" href="javascript:void(0);" role="tab" data-toggle="modal"
                        data-target="#all-bank-documents"><i class="icon-bank"></i> Preview Bank
                        Details Document</a>
                </li>
                <li>
                    <a class="nav-link" href="javascript:void(0);" role="tab"><i class="icon-document2"></i> Send
                        Attendee Documents</a>
                </li>
                <li>
                    <a class="nav-link" href="javascript:void(0);" role="tab"><i class="icon-mail-envelope"></i>
                        Confirmation Email (All respondents)</a>
                </li>
                <li>
                    <a class="nav-link" href="javascript:void(0);" role="tab"><i class="icon-mail-envelope"></i>
                        Confirmation Email (only new
                        respondents)</a>
                </li>
                <li>
                    <a class="nav-link" href="javascript:void(0);" role="tab"><i class="icon-mail-envelope"></i>
                        Confirmation SMS (All respondents)</a>
                </li>
                <li>
                    <a class="nav-link" href="javascript:void(0);" role="tab"><i class="icon-mail-envelope"></i>
                        Confirmation SMS (only new
                        respondents)</a>
                </li>
            </ul>
        </div>
    </div>
</header>
@endif
<header class="white pt-2 relative shadow">
    <div class="container-fluid">
        <div class="row">
            <ul class="nav nav-material responsive-tab" role="tablist">
                <li>
                    <a class="nav-link {{ $current=='detail' ? 'active':'' }}"
                        href="{{ route('projects.edit', $project->uuid) }}" role="tab"><i
                            class="icon icon-cog"></i>Details</a>
                </li>
                <li>
                    <a class="nav-link {{ $current=='group' ? 'active':'' }}"
                        href="{{ route('indexGroups', $project->uuid) }}" role="tab"><i
                            class="icon icon-group"></i>Groups</a>
                </li>
                @if(auth()->user()->type != 'client')
                <li>
                    <a class="nav-link {{ $current=='queries' ? 'active':'' }}"
                        href="{{ route('indexQuery', $project->uuid) }}" role="tab"><i
                            class="icon icon-save"></i>Queries</a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('timeallocation', $project->uuid) }}" role="tab"><i class="icon icon-time-is-money-1"></i>Time Allocation</a>
                </li>
                <li>
                    <a class="nav-link {{ $current == 'search-respondents' ? 'active' : '' }}"
                        href="{{ route('indexProjectSearchRespondents', $project->uuid) }}" role="tab"><i
                            class="icon icon-search"></i>Search
                        Respondents</a>
                </li>
                <li>
                    <a class="nav-link" href="" role="tab"><i class="icon icon-question_answer"></i>Survey</a>
                </li>
                <li>
                    <a class="nav-link" href="" role="tab"><i class="icon icon-question"></i>Screener</a>
                </li>
                <li>
                    <a class="nav-link {{ $current == 'standby-respondents' ? 'active' : '' }}"
                        href="{{ route('indexProjectStandByRespondents', $project->uuid) }}" role="tab"><i
                            class="icon icon-stop-watch"></i>Stand By
                        Respondents</a>
                </li>
                <li>
                    <a class="nav-link {{ $current == 'all-respondents' ? 'active' : '' }}"
                        href="{{ route('indexProjectAllRespondents', $project->uuid) }}" role="tab"><i
                            class="icon icon-check"></i>All
                        Respondents</a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</header>
@elseif(auth()->user()->type != 'client')
@include('projects.groups.tab-nav', ['project' => $project,'current'=>$current])
@endif

@push('scripts')
@include('projects.modals.modal-attendee-documents')
@include('projects.modals.modal-bank-documents')
<script>
    $('.create_job_summary_inv_report').click(function(){
        var btn = $(this);
        var btn_html = btn.html();
        $.ajax({
            url: "{{ route('generateJobReport') }}",
            type: 'POST',
            data: { _token: '{{ csrf_token() }}', job: '{{ $project->id }}' },
            beforeSend: function(){
                btn.html('<i class="icon-spinner"></i> Generating Report...');
            },
            success: function(url){
                //swal("Report Generated", "", "success");
                document.location = url;
            },
            error: function(err){ console.log(err); },
            complete: function(){ btn.html(btn_html); }
        });
    });

   // $('#timer').hide();
    $('#start-cronometer').click(function(){
        $('#timer').show();
        //var dt = new Date();
        //var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        $.ajax({
            url: "{{ route('StartTimeTracking') }}",
            type: 'POST',
            data: { _token: '{{ csrf_token() }}', job: '{{ $project->id }}'},
            success: function(data){
                //sessionStorage.getItem("intervalID")
                
               swal({title: "Job Started",  type: "success"},
                      function(){ 
                       location.reload();
                        }
                    );
            },
            error: function(err){ console.log(err); }
            
        });

    });

        $('#reset-timer').click(function(){
            $('#timer').hide();
            console.log()
        //var dt = new Date();
       // var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        $.ajax({
            url: "{{ route('EndTimeTracking') }}",
            type: 'POST',
            data: { _token: '{{ csrf_token() }}'},
            success: function(data){
               
               swal({title: "Job Ended",  type: "warning"},
                      function(){ 
                       location.reload();
                        }
                    );
               
            },
            error: function(err){ console.log(err); }
            
        });

    });
</script>
@endpush