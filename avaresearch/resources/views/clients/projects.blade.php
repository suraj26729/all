@extends('layouts.app')
@section('title', 'Projects :: '.$user->profile->first_name.' '.$user->profile->last_name)
@section('content')

<div class="row pb-3">
  <div class="col">
    <a href="{{ route('clients.index') }}" class="btn btn-secondary btn-xs"><i class="icon-arrow_back"></i> Back to
      Clients</a>
  </div>
</div>

<header class="white pt-2 relative shadow">
  <div class="container-fluid">
    <div class="row">
      <ul class="nav nav-material responsive-tab" id="v-pills-tab" role="tablist">
        <li>
          <a class="nav-link" href="{{ route('clients.edit', $user->uuid) }}" role="tab"><i
              class="icon icon-cog"></i>Details</a>
        </li>
        <li>
          <a class="nav-link active show" href="javascript:;" role="tab"><i class="icon icon-briefcase"></i>Jobs</a>
        </li>
      </ul>
    </div>
  </div>
</header>

<div class="container-fluid animatedParent animateOnce white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="list_table">
        <thead>
          <tr>
            <th>Job Number</th>
            <th visible="false">Job Name</th>
            <th>Job Name</th>
            <th>Subject</th>
            <th>Description</th>
            <th>Location</th>
            <th>Phone</th>
            <th>Created By</th>
            <th>Date</th>
            <th visible="false">User First Name</th>
            <th visible="false">User Last Name</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="13">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="13">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('footer')
<script>
  $(document).ready(function() {
    $("#list_table").DataTable({
      sDom: "Rlfrtip",
      processing: true,
      serverSide: true,
      pageLength: 10,
      ajax: "{{ route('DTClientProjects', $user->uuid) }}",
      columns: [
        { data: "job_number", name: "job_number" },
        { data: "job_name", name: "job_name", visible: false },
        { data: "project_name", name: "projects.job_name", searchable: false },
        { data: "subject", name: "subject" },
        { data: "excerpt_desc", name: "description" },
        { data: "location", name: "location" },
        { data: "contact_phone", name: "contact_phone" },
        {
          data: "creator_name",
          name: "user_details.first_name",
          searchable: false
        },
        { data: "created_at", name: "created_at" },
        {
          data: "first_name",
          name: "user_details.first_name",
          visible: false
        },
        {
          data: "last_name",
          name: "user_details.last_name",
          visible: false
        }
      ],
      order: [[0, "asc"]]
    });
  });
</script>
@endsection