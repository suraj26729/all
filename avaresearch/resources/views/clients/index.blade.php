@extends('layouts.app')
@section('title', 'Clients')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <!-- <h4 class="">Registered Staffs</h4> -->
    <a href="{{ route('clients.create') }}" class="btn btn-primary btn-xs"><i class="icon-plus"></i> Add New</a>
    <hr />
    @if(session('success'))
    <div class="alert alert-success">{{ session("success") }}</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="clients_table">
        <thead>
          <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Contact No</th>
            <th>Address</th>
            <th>Created By</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="6">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="6">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
@section('footer')
<script>
  $(document).ready(function() {
    $("#clients_table").DataTable({
      sDom: "Rlfrtip",
      processing: true,
      serverSide: true,
      pageLength: 10,
      ajax: "{{ route('DTClients') }}",
      columns: [
        { data: "image", name: "user_details.first_name", searchable: false },
        { data: "first_name", name: "user_details.first_name", visible: false },
        { data: "last_name", name: "user_details.last_name", visible: false },
        { data: "contact_no", name: "user_details.contact_no", width: "100px" },
        { data: "address", name: "user_details.address", width: "200px" },
        { data: "created_user_name", name: "users.created_by" },
        { data: "created_at", name: "users.created_at" },
        {
          data: "actions",
          name: "actions",
          orderable: false,
          searchable: false
        }
      ],
      order: [[0, "asc"]]
    });
  });
  $(document).on("click", ".delete_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");
    swal(
      {
        title: "Are you sure to delete",
        text: "Name: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('clients.index') }}/" + id,
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}" },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted", "You deleted the client: " + name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
</script>
@endsection