@extends('layouts.app') @section('title', 'Create Client') @section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <!-- <h4 class="">Fill up the details</h4> -->
    <a href="{{ route('clients.index') }}" class="btn btn-secondary btn-xs"><i class="icon-arrow_back"></i> Back to
      List</a>
    <hr />
    @if(session('success'))
    <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
    @endif @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif @if (session('errors'))
    {{ session('errors')->first('message') }}
    @endif
    <form class="form-horizontal" action="{{ route('clients.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="first_name" class="control-label">First Name <sup class="mandatory">*</sup> </label>
            <input class="form-control" id="first_name" type="text" name="first_name" value="{{ old('first_name') }}"
              required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="last_name" class="control-label">Last Name <sup class="mandatory">*</sup></label>
            <input class="form-control" id="first_name" type="text" name="last_name" value="{{ old('last_name') }}"
              required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="company_name" class="control-label">Company Name <sup class="mandatory">*</sup></label>
            <input class="form-control" id="company_name" type="text" name="company_name"
              value="{{ old('company_name') }}" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="website" class="control-label">Website</label>
            <input title="Ex: http://example.com" class="form-control" id="website" type="url" name="website"
              value="{{ old('website') }}" />
            <small class="text-primary">URL must have a protocol http:// or https://</small>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="contact_no" class="control-label">Contact Number <sup class="mandatory">*</sup></label>
            <input class="form-control" id="contact_no" type="text" name="contact_no" value="{{ old('contact_no') }}"
              minlength="10" maxlength="10" onkeypress="return isNumber(event);" required />
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="fax_no" class="control-label">Fax Number</label>
            <input class="form-control" id="fax_no" type="text" name="fax_no" value="{{ old('fax_no') }}" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label for="email" class="control-label">Email Address <sup class="mandatory">*</sup></label>
            <input class="form-control" id="email" type="email" name="email" value="{{ old('email') }}" required />
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="password" class="control-label">Password </label>
            <input class="form-control" id="password" type="password" name="password">
             <!-- <small class="text-primary">
              Your password must be between 5 and 10 characters long
            </small>  -->
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="password_confirmation" class="control-label">Confirm Password </label>
            <input class="form-control" id="password" type="password" name="password_confirmation" >
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="address" class="control-label">Address <sup class="mandatory">*</sup></label>
            <textarea name="address" id="address" cols="30" rows="10" class="form-control"
              required>{{ old("address") }}</textarea>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="image" class="control-label">Image</label><br />
            <div class="preview-wrap mb-3 hidden"></div>
            <input type="file" name="image" id="image" accept="image/*" />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <button class="btn btn-primary"><i class="fa fa-save mr-2"></i>Submit</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

@endsection @section('footer')
<script>
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $(".preview-wrap")
          .html(`<img src="${e.target.result}" alt="Avatar" class="w-150px" />`)
          .removeClass("hidden");
      };

      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#image").change(function() {
    $(".preview-wrap").addClass("hidden");
    readURL(this);
  });
</script>
@endsection