@extends('layouts.app')
@section('title', 'Staff Qualified Report')
@section('content')
<div class="animatedParent animateOnce relative">
    <div class="animated fadeInUpShort go">
        <div class="d-flex row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" id="search-form">
                            <div class="form-group">
                                <label class="control-label">Date</label>
                                <input type="text" name="date" class="date-time-picker form-control form-control-lg"
                                    data-options='{"inline":true, "format":"d.m.Y H:i", "timepicker":false}' />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Staff</label>
                                <select class="form-control select2" name="staff_id" required>
                                    <option value="">Select</option>
                                    @foreach($staffs as $staff)
                                    <option value="{{ $staff->id }}">
                                        {{ $staff->profile->first_name.' '.$staff->profile->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group text-center">
                                <button class="btn btn-primary"><i class="icon-search mr-2"></i>Search</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-9 search-result-wrap"></div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script>
    $('#search-form').submit(function(){
            var data = { _token: "{{ csrf_token() }}",date: $('[name="date"]').val() || moment(new Date()).format('DD.MM.YYYY HH:mm'), staff: $('[name="staff_id"]').val() };
            $.ajax({
                url: "{{ url('/staff-qualified-report/getData') }}",
                type: 'POST',
                data: data,
                beforeSend: function(){
                    $('.search-result-wrap').html(`<i class="fa fa-circle-o-notch fa-2x fa-spin"></i>`);
                },
                success: function(res){
                    $('.search-result-wrap').html(res);
                },
                error: function(err){
                    console.log(err);
                    alert(err.responseText);
                }
            });
            return false;
        });
</script>
@endpush