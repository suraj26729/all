<div class="card">
    <div class="card-body">
        <div class="row text-white no-gutters no-m shadow">
            <div class="col-lg-4">
                <div class="green counter-box p-40">
                    <div class="float-right">
                        <span class="icon icon-briefcase s-48"></span>
                    </div>
                    <div class="sc-counter s-36 counter-animated">{{ $total_jobs }}</div>
                    <h6 class="counter-title">Projects Worked</h6>
                </div>
            </div>
            <div class="col-lg-4 offset-md-4">
                <div class="sunfollower counter-box p-40">
                    <div class="float-right">
                        <span class="icon icon-graduation-cap s-48"></span>
                    </div>
                    <div class="sc-counter s-36 counter-animated">{{ $total_respondents }}</div>
                    <h6 class="counter-title">Respondents Qualified</h6>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <th>Job Number</th>
                            <th>Job Name</th>
                            <th>Respondents</th>
                        </thead>
                        <tbody>
                            @forelse ($result as $item)
                            <tr>
                                <td>{{ $item['project_number'] }}</td>
                                <td>
                                    <a href="{{ route('projects.edit', $item['project_uuid']) }}">
                                        {{ $item['project_name'] }}
                                    </a>
                                </td>
                                <td>
                                    @foreach($item['respondents'] as $resp)
                                    <p class="mb-1">
                                        <a href="{{ route('respondents.edit', $resp['respondent_uuid']) }}">
                                            {{ $resp['respondent_name'] }}
                                        </a>
                                    </p>
                                    @endforeach
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="3">Not worked in any of the jobs</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>