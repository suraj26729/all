@extends('layouts.app')
@section('title', 'Leads')
@section('content')

<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
    <div class="animated fadeInUpShort go">
    <strong><h4>Import Respondents Form Facebook Leads</h4></strong>
    <hr />
    @if(session('respondents'))
    <div>
        <strong>Total No Of Respondents Imported:  {{ session("respondents") }}</strong> &emsp; &emsp;
    </div>
    @endif

     @if(session('exist'))
    <div>
        <strong>Total No Of Existing Respondents:  {{ session("exist") }}</strong>
    </div>
    @endif

   <!--  @if(session('error'))
    <div>{{ session("error") }}</div>
    @endif -->
         @if(session('error'))
        <div class="toast" data-title="Error" data-message="{{ session('error') }}" data-type="error"></div>       
        @endif

        @if(session('success'))
        <div class="toast" data-title="Success" data-message="{{ session('success') }}" data-type="success"></div>       
        @endif

         @if(session('warning'))
        <div class="toast" data-title="Message" data-message="{{ session('warning') }}" data-type="warning"></div>       
        @endif
        <br>

        <form class="form-horizontal" action="{{ route('leads.store') }}" method="POST" >
        @csrf
            <div class="row">
                <div class="col-md-5">
                    <label for="lead_form_id" class="control-label">Lead Form ID <sup class="mandatory">*</sup></label>
                    <input class="form-control" id="lead_form_id" type="text" name="form_id" required />
                </div>
                <div class="col-md-5">
                    <label for="lead_form_name" class="control-label">Lead Form Name <sup class="mandatory">*</sup></label>
                    <input class="form-control" id="lead_form_name" type="text" name="form_name" required />
                </div>
                <div class="col-md-2">
                <label>&nbsp;</label>
                    <div class="form-group">                       
                        <button class="btn btn-primary" id="submit_btn"><i class="fa fa-save mr-2"></i>Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection