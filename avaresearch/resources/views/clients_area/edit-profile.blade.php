@extends('layouts.app')
@section('title', 'Edit Profile')
@section('content')

<div class="container-fluid animatedParent animateOnce white relative shadow pt-3 pb-3">
    <div class="animated fadeInUpShort go">

        @if(session('success'))
        <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
        @endif

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (session('errors'))
        {{ session('errors')->first('message') }}
        @endif

        <form class="form-horizontal" action="{{ route('clients.update', $user->id) }}" method="POST"
            enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name" class="control-label">First Name <sup class="mandatory">*</sup></label>
                        <input class="form-control" id="first_name" type="text" name="first_name"
                            value="{{ old('first_name', $user->profile->first_name) }}" required />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="last_name" class="control-label">Last Name <sup class="mandatory">*</sup></label>
                        <input class="form-control" id="first_name" type="text" name="last_name"
                            value="{{ old('last_name', $user->profile->last_name) }}" required />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="company_name" class="control-label">Company Name <sup
                                class="mandatory">*</sup></label>
                        <input class="form-control" id="company_name" type="text" name="company_name"
                            value="{{ old('company_name', $user->profile->company_name) }}" required />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="website" class="control-label">Website</label>
                        <input class="form-control" id="website" type="url" name="website"
                            value="{{ old('website', $user->profile->website) }}" />
                        <small class="text-primary">URL must have a protocol http:// or https://</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact_no" class="control-label">Contact Number <sup
                                class="mandatory">*</sup></label>
                        <input class="form-control" id="contact_no" type="text" name="contact_no"
                            value="{{ old('contact_no', $user->profile->contact_no) }}" required />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="fax_no" class="control-label">Fax Number</label>
                        <input class="form-control" id="fax_no" type="text" name="fax_no"
                            value="{{ old('fax_no', $user->profile->fax_no) }}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email" class="control-label">Email Address <sup class="mandatory">*</sup></label>
                        <input class="form-control" id="email" type="email" name="email"
                            value="{{ old('email', $user->email) }}" required />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address" class="control-label">Address <sup class="mandatory">*</sup></label>
                        <textarea name="address" id="address" cols="30" rows="10" class="form-control"
                            required>{{ old("address", $user->profile->address) }}</textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="image" class="control-label">Image</label><br />
                        @if($user->profile->image)
                        <div class="preview-wrap mb-3">
                            <img src="{{ asset('storage/app/public/'.$user->profile->image) }}" class="w-150px"
                                alt="Avatar" />
                        </div>
                        @else
                        <div class="preview-wrap mb-3 hidden"></div>
                        @endif
                        <input type="file" name="image" id="image" accept="image/*" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <button class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $(".preview-wrap")
          .html(`<img src="${e.target.result}" alt="Avatar" class="w-150px" />`)
          .removeClass("hidden");
      };

      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#image").change(function() {
    $(".preview-wrap").addClass("hidden");
    readURL(this);
  });
</script>
@endsection