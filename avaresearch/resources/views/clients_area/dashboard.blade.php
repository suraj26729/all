@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<div class="card">
  <div class="card-body">
    <h1>Welcome {{ auth('web')->user()->profile->first_name }} !</h1>
    <h3>Have a nice time with your Client dashboard!</h3>
  </div>
</div>
@endsection