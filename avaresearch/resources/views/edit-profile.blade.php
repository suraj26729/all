@extends('layouts.app') @section('title', 'Edit Profile') @section('content')
<header class="white pt-3 relative shadow">
  <div class="container-fluid">
    <div class="row p-t-b-10 ">
      <div class="col">
        <div class="pb-3">
          <div class="image mr-3  float-left">
            <img class="user_avatar no-b no-p" src="{{ asset('public/assets/img/dummy/u1.png') }}"
              alt="{{ $user->profile->first_name }}" />
          </div>
          <div>
            <h6 class="p-t-10">
              {{ $user->profile->first_name.' '.$user->profile->last_name }}
            </h6>
            {{ $user->email }}
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <ul class="nav nav-material responsive-tab" id="v-pills-tab" role="tablist">
        <li>
          <a class="nav-link active show" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings"
            role="tab" aria-controls="v-pills-settings" aria-selected="true"><i class="icon icon-cog"></i>Edit
            Profile</a>
        </li>
      </ul>
    </div>
  </div>
</header>
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade active show" id="v-pills-settings" role="tabpanel"
        aria-labelledby="v-pills-settings-tab">
        @if(session('message'))
        <div class="alert alert-success">{{ session("message") }}</div>
        @endif
        <form class="form-horizontal" action="{{ route('save-profile') }}" method="POST">
          @csrf
          <div class="row">
            <div class="col">
              <div class="form-group">
                <label for="first_name" class="control-label">First Name</label>
                <input class="form-control" id="first_name" placeholder="First Name" type="text" name="first_name"
                  value="{{ $user->profile->first_name }}" required />
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label for="last_name" class="control-label">Last Name</label>
                <input class="form-control" id="first_name" placeholder="Last Name" type="text" name="last_name"
                  value="{{ $user->profile->last_name }}" required />
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Address</label>
                <textarea name="address" id="address" rows="10" placeholder="Address"
                  class="form-control">{{ $user->profile->address }}</textarea>
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label for="contact_no" class="col-sm-2 control-label">Contact No</label>
                <input class="form-control" id="contact_no" placeholder="Contact No" type="text" name="contact_no"
                  value="{{ $user->profile->contact_no }}" />
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group">
                <button type="submit" class="btn btn-primary"><i class="fa fa-save mr-2"></i>Save Changes</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection