<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('public/assets/img/favicon.png') }}" type="image/x-icon">
    <title>@yield('title')</title>
    <!-- CSS -->
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="{{ asset('public/assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('public/assets/css/custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('public/assets/css/front.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700|Open+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="light sidebar-mini sidebar-collapse">
    @include('parts.preloader')

    <header id="header" class="header header-v1 header-v1-only clearfix">
        <div class="container-fluid">
            <div id="logo-navigation" class="sixteen columns">
                <div id="logo" class="logo">
                    <a href="https://avaresearch.com.au/">
                        <img src="{{ asset('public/assets/img/logo.png') }}" alt="Ava Research" class="logo_standard">
                    </a>

                </div>
                <div id="navigation" class="clearfix">
                    <div class="header-icons">

                    </div>
                    <ul id="nav" class="menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
                        <li><a href="https://avaresearch.com.au/about-ava-market-research/">Who We Are</a></li>
                        <li class="sub-menu-hover">
                            <a href="https://avaresearch.com.au/what-we-do/">What We Do</a>
                            <ul class="sub-menu">
                                <li><a href="https://avaresearch.com.au/what-we-do/market-research-recruitment/">Market
                                        Research Recruitment</a></li>
                                <li><a href="https://avaresearch.com.au/what-we-do/room-hire-sydney/">Room Hire</a></li>
                                <li><a href="https://avaresearch.com.au/what-we-do/paid-research-for-participants/">Paid
                                        Research For Participants</a></li>
                            </ul>
                        </li>
                        <li><a href="https://avaresearch.com.au/articles/">Articles</a></li>
                        <li><a href="https://avaresearch.com.au/request-a-quote/">Request Quote</a></li>
                        <li><a href="https://avaresearch.com.au/contact/">Contact</a></li>
                        <li class="header-menu-btn">
                            <a href="{{ route('register_respondents') }}"><span
                                    class="menu-image-title">Register</span></a>
                        </li>
                        <li class="header-menu-btn">
                            <a href="{{ route('pageRespondentLogin') }}"><span class="menu-image-title">Login</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div class="container-fluid relative animatedParent animateOnce">
        @yield('content')
    </div>

    <footer id="footer">
        <div class="container-fluid">
            <div class="col-4">
                <div class="footer-widget-1">
                    <h3>Ava Research</h3>
                    <ul class="footer-address">
                        <li class="map">703/60-62 York St Sydney NSW 2000 Australia</li>
                        <li class="phone">(+61) 2 9719 2990</li>
                        <li class="email"><a href="mailto:info@avaresearch.com.au">info@avaresearch.com.au</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-4">
                <div class="footer-widget-2">
                    <h3>PAGES</h3>
                    <ul class="footer-link">
                        <li><a href="https://avaresearch.com.au/what-we-do/market-research-recruitment/">Market Research
                                Recruitment</a></li>
                        <li><a href="https://avaresearch.com.au/what-we-do/paid-research-for-participants/">Paid
                                Research For Participants</a></li>
                        <li><a href="https://avaresearch.com.au/request-a-quote/">Request a Quote</a></li>
                        <li><a href="https://avaresearch.com.au/what-we-do/room-hire-sydney/">Group Room Hire</a></li>
                        <li><a href="https://avaresearch.com.au/contact/">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-4">
                <div class="footer-widget-3">
                    <h3>LINKS</h3>
                    <ul class="footer-link">
                        <li><a href="https://devavaresearch.com.au/respondents/login">Login</a></li>
                        <li><a href="https://devavaresearch.com.au/respondents/register">Register</a></li>
                        <li><a href="https://avaresearch.com.au/privacy/">Privacy</a></li>
                        <li><a href="https://avaresearch.com.au/articles/">articles</a></li>
                        <li><a href="https://avaresearch.com.au/careers/">careers</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="clear"></div>

    </footer>
    <script src="{{ asset('public/assets/js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/app.js') }}"></script>
    <script src="{{ asset('public/assets/js/jquery.maskedinput.min.js') }}"></script>
    @yield('footer')
    <script>
        jQuery(".date").datetimepicker({
            format: "Y-m-d",
            timepicker: false,
            scrollMonth : false,
            scrollInput : false
        });
        function isNumber(e){
            e = e || window.event;
            var charCode = e.which ? e.which : e.keyCode;
            return /\d/.test(String.fromCharCode(charCode));
        }
        $(document).ready(function () {
            $('input[name=mobile_phone]').mask('0000-000-000');
            $('input[name=mobile_phone2]').mask('0000-000-000');
            $('input[name=home_phone]').mask('00-0000-0000');
            $('input[name=work_phone]').mask('00-0000-0000');
            $('.phone').mask('0000-000-000');
            $('.phone').keypress(function (e) {
                return isNumber(e);
                // var length = jQuery(this).val().length;
                // if(length > 11) {
                //       return false;
                // } else if(e.which != 8 && e.which != 0 && e.which != 43 && (e.which < 48 || e.which > 57 )) {
                //       return false;
                // } else if((length == 0) && (e.which == 48)) {
                //       return false;
                // }
            });
        });
    </script>
</body>


</html>