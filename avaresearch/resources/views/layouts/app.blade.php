<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <link rel="icon" href="{{ asset('public/assets/img/favicon.png') }}" type="image/x-icon" />
  <title>@yield('title')</title>
  <!-- CSS -->
  <link rel="stylesheet" href="{{ asset('public/assets/css/sweetalert.css') }}" />
  <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  @yield('header')
  <link rel="stylesheet" href="{{ asset('public/assets/css/app.css') }}" />
  <link rel="stylesheet" href="{{ asset('public/assets/css/custom.css') }}" />
 
</head>

<body class="light sidebar-mini sidebar-collapse">
  @include('parts.preloader')
  <div id="app">
    <aside class="main-sidebar fixed offcanvas b-r sidebar-tabs" data-toggle="offcanvas">
      @include('parts.sidebar')
    </aside>
    <a href="#" data-toggle="push-menu" class="paper-nav-toggle left ml-2 fixed">
      <i></i>
    </a>
    <div class="has-sidebar-left has-sidebar-tabs">
      @include('parts.topnav')
      <div class="container-fluid relative animatedParent animateOnce my-3">
        @yield('content')
      </div>
    </div>
    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg shadow white fixed"></div>
  </div>
  <script src="{{ asset('public/assets/js/sweetalert.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/app.js') }}"></script>
  <script src="{{ asset('public/assets/js/jquery.maskedinput.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/colresize.min.js') }}"></script>
  <script src="{{ asset('public/assets/js/jquery.doubleScroll.js') }}"></script>
  <script src="{{ asset('public/assets/js/timer_script.js') }}"></script>
  @yield('footer')
  @stack('scripts')
  <script>
    jQuery(".date").datetimepicker({
        format: "Y-m-d",
        timepicker: false,
        scrollMonth : false,
	      scrollInput : false
    });

    jQuery(".time").datetimepicker({
      format: "H:i",
      datepicker: false
    });

    function isNumber(e){
      e = e || window.event;
      var charCode = e.which ? e.which : e.keyCode;
      return /\d/.test(String.fromCharCode(charCode));
    }
    $(document).ready(function () {
      $('.mobile').mask('0000-000-000');
      $('.telephone').mask('0000-000-000');
      $('input[name=filter_mobile_phone], input[name=mobile_phone]').mask('0000-000-000');
      $('input[name=filter_mobile_phone2], input[name=mobile_phone2]').mask('0000-000-000');
      $('input[name=filter_home_phone], input[name=home_phone]').mask('00-0000-0000');
      $('input[name=filter_work_phone], input[name=work_phone]').mask('00-0000-0000');
      $('.phone').keypress(function (e) {
          return isNumber(e);
          // var length = jQuery(this).val().length;
          // if(length > 11) {
          //       return false;
          // } else if(e.which != 8 && e.which != 0 && e.which != 43 && (e.which < 48 || e.which > 57 )) {
          //       return false;
          // } else if((length == 0) && (e.which == 48)) {
          //       return false;
          // }
      });
      $(document).on('click', '.max-mize', function(){
        $(this).closest('.modal-dialog').toggleClass('maxi-mize');
      });
    });
  </script>
</body>

</html>