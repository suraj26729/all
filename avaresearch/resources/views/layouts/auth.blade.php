<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('public/assets/img/favicon.png') }}" type="image/x-icon">
    <title>@yield('title')</title>
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('public/assets/css/app.css') }}">

    <style>
        body {
            color: #000;
        }

        .login-bg {
            background: url('{{ asset("public/assets/img/login-bg.jpg") }}');
            background-size: cover;
            background-position: center center !important;
        }
    </style>

    <link rel="stylesheet" href="{{ asset('public/assets/css/custom.css') }}" />

</head>

<body class="light sidebar-mini sidebar-collapse">
    <!-- Pre loader -->
    @include('parts.preloader')
    <div id="app">
        @yield('content')
        <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
        <div class="control-sidebar-bg shadow white fixed"></div>
    </div>
    <script src="{{ asset('public/assets/js/app.js') }}"></script>
</body>

</html>