@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<div class="card">
  <div class="card-body">
    <h1>Welcome {{ auth('respondent')->user()->first_name }}</h1>
    <h3>Have a nice time with your Respondent dashboard!</h3>
  </div>
</div>
@endsection