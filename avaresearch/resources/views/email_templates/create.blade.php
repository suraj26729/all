@extends('layouts.app')
@section('title', 'Create Email Template')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <!-- <h4 class="">Fill up the details</h4> -->
    <a href="{{ route('email-templates.index') }}" class="btn btn-secondary btn-xs"><i class="icon-arrow_back"></i> Back
      to
      List</a>
    <hr />
    @if(session('success'))
    <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
      <ul class="mb-0">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <form class="form-horizontal" action="{{ route('email-templates.store') }}" method="POST"
      enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="title" class="control-label">Title <sup class="mandatory">*</sup></label>
            <input class="form-control" id="title" type="text" name="title" value="{{ old('title') }}" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="message" class="control-label">Message <sup class="mandatory">*</sup></label>
            <textarea name="message" id="message" rows="10" class="form-control"
              required>{{ old('message') }}</textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="attachments" class="control-label">Attachments</label><br />
            <input type="file" name="attachment[]" /><br /><br />
            <input type="file" name="attachment[]" /><br /><br />
            <input type="file" name="attachment[]" />
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <button class="btn btn-primary"><i class="fa fa-save mr-2"></i>Submit</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('footer')
<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'message' );
</script>
@endsection