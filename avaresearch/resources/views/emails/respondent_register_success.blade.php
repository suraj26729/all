<html>
<head>
<title>Visitor Mail</title>
</head>
<body>
    <table width='' height='' border='0' align='' cellpadding='4' cellspacing='4'>				 
        <tr>
        <td align=''>Welcome To Ava Research</td>
        </tr>
        <tr>
        <td align=''>Thank you for registering your details with us for paid market research.</td>
        </tr>
        <tr>
        <td align=''>We will contact you when there is a project that is suitable for you.</td>
        </tr>
        <tr>
        <td align=''>As our clients are looking for specific demographics, purchase behaviours and service usage etc we need to screen you through a series of questions to determine if you are suitable for the particular project (as each project requires different types of respondents)</td>
        </tr>
        <tr>
        <td align=''>These questions are either asked over the phone or in an online survey screening method.</td>
        </tr>
        <tr>
        <td align=''>The more projects you express interest in, the more chances you have to qualify!<br>
        Please remember that if any of your details change, you can update your details by visiting our website</td>
        </tr>
        <tr>
        <td align=''>Be sure to let your friends and family know about us too!</td>
        </tr>
        <tr>
        <td align=''>We hope to book you into a research study very soon.</td>
        </tr>
        <tr>
        <td align=''><h3>Login Details </h3></td>
        </tr>
        <tr>
        <td align=''><strong>Email:</strong> {{ $respondent->email }}</td>
        </tr>
        <tr>
        <td align=''><strong>Password:</strong> {{ $password }} </td>
        </tr>
    <tr>
    <td><a href='http://panel.avaresearch.com.au/index.php/home/login'>
    Please login here :http://panel.avaresearch.com.au/index.php/home/login</a></td>	
    </tr>
        <tr>
        <td align=''><strong><a href='http://panel.avaresearch.com.au/index.php/home/login'></a></strong></td>
        </tr>
        <tr>
        <td align=''><strong>Kind regards</strong></td>
        </tr>
        <tr>
        <td align=''><strong>Team at Ava Research )))</strong></td>
        </tr>
    </table>

        
    <div class='form-group fb-bg1'>
    <div class='col-md-12 text-center'>
    <h2 style='color:#fff;'>Do not forget to register the rest of your family members and recommend a friend!</h2>
    <h5 style='color:#fff;'>Please visit facebook  regularly  to view our current and future projects. Also have you registered/recommended the rest of your family members in anystate of Australia  yet?</h5>
    <a class='clickhere-cls' href='https://www.facebook.com/avaresearch/'>Click Here </a>
    </div>
    </div>							
        
</body>
</html>