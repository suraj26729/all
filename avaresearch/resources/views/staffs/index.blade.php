@extends('layouts.app') @section('title', 'Staffs') @section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <!-- <h4 class="">Registered Staffs</h4> -->
    <a href="{{ route('staffs.create') }}" class="btn btn-primary btn-xs"><i class="icon-plus"></i> Add New</a>
    <hr />
    @if(session('success'))
    <div class="alert alert-success">{{ session("success") }}</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="list_table">
        <thead>
          <tr>
            <th>User</th>
            <th>Contact No</th>
            <th>Location</th>
            <th>Created By</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          @foreach($staffs as $staff)
          <tr>
            <td>
              <div class="d-flex">
                <div class="avatar avatar-md mr-3 mb-2 mt-1">
                  <span
                    class="avatar-letter avatar-letter-{{ strtolower(substr($staff->profile->first_name, 0, 1)) }}  avatar-md circle"></span>
                </div>
                <div>
                  <div>
                    <a href="{{ route('staffs.edit', $staff->uuid) }}">
                      <strong>{{ $staff->profile->first_name.' '.$staff->profile->last_name }}</strong>
                    </a>
                  </div>
                  <small>{{ $staff->email }}</small>
                </div>
              </div>
            </td>
            <td>{{ $staff->profile->contact_no }}</td>
            <td>{{ $staff->profile->address }}</td>
            <td>
              <a href="{{ route('staffs.edit', $staff->createdBy->uuid) }}">
                {{ $staff->createdBy->profile->first_name.' '.$staff->createdBy->profile->last_name }}
              </a>
            </td>
            <td>{{ $staff->created_at->format('d-m-Y') }}</td>
            <td>
              <a href="{{ route('staffs.edit', $staff->uuid) }}"><i class="icon-pencil mr-3"></i></a>
              <a href="javascript:void(0);" class="delete_btn" data-id="{{ $staff->id }}"
                data-name="{{ $staff->profile->first_name.' '.$staff->profile->last_name }}"><i
                  class="icon-trash"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection @section('footer')
<script>
  $(document).ready(function() {
      $("#list_table").DataTable({
        sDom: "Rlfrtip"
      });
  });
  //$(".delete_btn").click(function() {
    $(document).on("click",".delete_btn",function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");
    
    swal(
      {
        title: "Are you sure to delete",
        text: "Name: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('staffs.index') }}/" + id,
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}" },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted", "You delete the staff: " + name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
</script>
@endsection