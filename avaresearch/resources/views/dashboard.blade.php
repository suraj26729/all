@extends('layouts.app') @section('title', 'Dashboard') @section('content')
<div class="row text-white no-gutters no-m shadow">
  <div class="col-lg-3">
    <div class="green counter-box p-40">
      <div class="float-right">
        <span class="icon icon-briefcase s-48"></span>
      </div>
      <div class="sc-counter s-36">{{ $projects_count }}</div>
      <h6 class="counter-title">Projects</h6>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="blue1 counter-box p-40">
      <div class="float-right">
        <span class="icon icon-tie s-48"></span>
      </div>
      <div class="sc-counter s-36">{{ $clients_count }}</div>
      <h6 class="counter-title">Clients</h6>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="sunfollower counter-box p-40">
      <div class="float-right">
        <span class="icon icon-graduation-cap s-48"></span>
      </div>
      <div class="sc-counter s-36">{{ $respondents_count }}</div>
      <h6 class="counter-title">Respondents</h6>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="strawberry counter-box p-40">
      <div class="float-right">
        <span class="icon icon-group s-48"></span>
      </div>
      <div class="sc-counter s-36">{{ $staffs_count }}</div>
      <h6 class="counter-title">Staffs</h6>
    </div>
  </div>
</div>

<div class="row my-3">
  <div class="col-md-6">
    <div class=" card no-b card-block shadow">
      <div class="card-body">
        <h3>Running Projects</h3>
        @if($running_projects)
        <table class="table table-hover earning-box">
          <tbody>
            @foreach($running_projects as $project)
            <tr class="no-b">
              <td>{{ $project->job_number }}</td>
              <td>
                <h6><a href="{{ route('projects.edit', $project->uuid) }}">{{ $project->job_name }}</a></h6>
                <small class="text-muted">{{ $project->subject }}</small><br />
                <small
                  class="text-muted">{{ date('d-m-Y', strtotime($project->start_date)).' - '.date('d-m-Y', strtotime($project->finish_date)) }}</small>
              </td>
              <td>
                {{ $project->client ? $project->client->profile->first_name.' '.$project->client->profile->last_name : '' }}<br />
                <small class="text-muted">{{ $project->client ? $project->client->profile->company_name : '' }}</small>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        @else
        <h5 class="mt-3 text-muted">No projects available</h5>
        @endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class=" card no-b card-block shadow">
      <div class="card-body">
        <h3>Upcoming Projects</h3>
        <table class="table table-hover earning-box">
          <tbody>
            @forelse($upcoming_projects as $project)
            <tr class="no-b">
              <td>{{ $project->job_number }}</td>
              <td>
                <h6><a href="{{ route('projects.edit', $project->uuid) }}">{{ $project->job_name }}</a></h6>
                <small class="text-muted">{{ $project->subject }}</small><br />
                <small
                  class="text-muted">{{ date('d-m-Y', strtotime($project->start_date)).' - '.date('d-m-Y', strtotime($project->finish_date)) }}</small>
              </td>
              <td>
                {{ $project->client ? $project->client->profile->first_name.' '.$project->client->profile->last_name : '' }}<br />
                <small class="text-muted">{{ $project->client ? $project->client->profile->company_name : '' }}</small>
              </td>
            </tr>
            @empty
            <tr>
              <td colspan="5">
                <h5 class="mt-3 text-muted">No projects available</h5>
              </td>
            </tr>
            @endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection