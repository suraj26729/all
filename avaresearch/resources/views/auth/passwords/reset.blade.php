@extends('layouts.auth')
@section('title', 'Reset Password')
@section('content')
<div class="page parallel">
    <div class="d-flex row">
        <div class="col-md-8 height-full blue text-center d-none d-md-block login-bg" data-bg-repeat="false"
            data-bg-possition="center"></div>
        <div class="col-md-4 white">
            <div class="p-5">
                <img src="{{ asset('public/assets/img/logo.png') }}" alt="Logo" />
            </div>
            <div class="p-5">
                <h3>{{ __('Reset Password') }}</h3>
                <p>
                    {{ __('Please enter your new password to reset') }}
                </p>
                @if (session('status'))
                <div class="toast" data-title="Success" data-message="{{ session("status") }}" data-type="success">
                </div>
                @endif
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group has-icon">
                        <i class="icon-envelope-o"></i>
                        <input type="email" class="form-control form-control-lg @error('email') is-invalid @enderror"
                            placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required
                            autocomplete="email" autofocus />
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group has-icon">
                        <i class="icon-user-secret"></i>
                        <input type="password"
                            class="form-control form-control-lg @error('password') is-invalid @enderror"
                            placeholder="{{ __('Password') }}" id="password" name="password" required
                            autocomplete="new-password" />
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group has-icon">
                        <i class="icon-user-secret"></i>
                        <input type="password" class="form-control form-control-lg"
                            placeholder="{{ __('Confirm Password') }}" id="password_confirmation"
                            name="password_confirmation" required autocomplete="new-password" />
                    </div>

                    <button class="btn btn-primary btn-lg btn-block"><i
                            class="icon-save mr-2"></i>{{ __('Reset Password') }}</button>

                    <div class="form-group mt-3">
                        <a href="{{ route('login') }}">
                            {{ __("Back to Login") }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection