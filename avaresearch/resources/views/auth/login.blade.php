@extends('layouts.auth')
@section('title', 'Login')
@section('content')
@if($errors->has('office_ip'))
<div class="toast" data-title="{{ $errors->first('office_ip') }}" data-message="Your access is restricted"
  data-type="error"></div>
@endif
<div class="page parallel">
  <div class="d-flex row">
    <div class="col-md-8 height-full blue d-none d-md-block p login-bg" data-bg-repeat="false"
      data-bg-possition="center">

      {{-- <div class="d-flex text-white login-list center-block">
        <ul>
          <li>Manage Existing Clients/Staffs</li>
          <li>Create New Clients/Staffs</li>
          <li>Manage Testimonials</li>
          <li>Manage FAQs</li>
          <li>Manage Blogs</li>
          <li>.. and many more developed features!</li>
        </ul>
      </div> --}}

    </div>
    <div class="col-md-4 white">
      <div class="p-5">
        <img src="{{ asset('public/assets/img/logo.png') }}" alt="Logo" />
      </div>
      <div class="p-5">
        <h3>Login</h3>
        <p>
          Hey welcome back!
        </p>
        @if (session('status'))
        <div class="toast" data-title="Success" data-message="{{ session("status") }}" data-type="success">
        </div>
        @endif
        <form method="POST" action="{{ route('login') }}">
          @csrf
          <div class="form-group has-icon">
            <i class="icon-envelope-o"></i>
            <input type="email" class="form-control form-control-lg @error('email') is-invalid @enderror"
              placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required
              autocomplete="email" autofocus />
            @error('email')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
          <div class="form-group has-icon">
            <i class="icon-user-secret"></i>
            <input type="password" class="form-control form-control-lg @error('password') is-invalid @enderror"
              placeholder="{{ __('Password') }}" id="password" name="password" required
              autocomplete="current-password" />
            @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>

          <div class="form-group">
            <div class="material-switch float-left">
              <input id="remember" name="remember" type="checkbox" {{ old("remember") ? "checked" : "" }} checked />
              <label for="remember" class="bg-primary"></label>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;{{ __("Remember Me") }}
          </div>

          <button class="btn btn-primary btn-lg btn-block"><i class="icon-lock mr-2"></i>{{ __('Login') }}</button>

          @if (Route::has('password.request'))
          <div class="form-group mt-3">
            <a href="{{ route('password.request') }}">
              {{ __("Forgot Your Password?") }}
            </a>
          </div>
          @endif
        </form>
      </div>
    </div>
  </div>
</div>
@endsection