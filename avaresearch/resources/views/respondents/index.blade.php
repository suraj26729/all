@extends('layouts.app')
@section('title', 'Respondents')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <a href="{{ route('respondents.create') }}" class="btn btn-primary btn-xs"><i class="icon-plus"></i> Add New</a>
    <hr />
    @if(session('success'))
    <div class="alert alert-success">{{ session("success") }}</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="respondents_table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th visible="false">Email</th>
            <th>Mobile Phone</th>
            <th>State</th>
            <th>Country of Origin</th>
            <th>Created By</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('footer')
<script>
  $(document).ready(function() {
        $("#respondents_table").DataTable({
            sDom: "Rlfrtip",
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: "{{ route('DTRespondents') }}",
            columns: [
                { data: "id", name: "respondents.id" },
                { data: "respondent_name", name: "respondents.first_name" },
                { data: "first_name", name: "respondents.first_name", visible: false },
                { data: "last_name", name: "respondents.last_name", visible: false },
                { data: "email", name: "respondents.email", visible: false },
                { data: "mobile_phone", name: "respondent_details.mobile_phone",  },
                { data: "state", name: "master_states.name" },
                { data: "country", name: "master_birth_countries.name" },
                { data: "created_user_name", name: "respondents.created_by" },
                { data: "created_at", name: "respondents.created_at" },
                {
                data: "actions",
                name: "actions",
                orderable: false,
                searchable: false
                }
            ],
            order: [[9, "desc"]]
        });
    });
  $(document).on("click", ".delete_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");

    swal({
  title: "Delete",
  text: "Please Enter The Reason",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  confirmButtonText: "Delete",
  cancelButtonText: "Cancel",
  confirmButtonClass: "btn-danger",
  
  },
  function(reason){
    if (reason === false){
      swal.showInputError("Please Fill The Reason!");
    }else if (reason === "") {
      swal.showInputError("Please Fill The Reason!");
     return false
    } else{
       $.ajax({
            url: "{{ route('duplicateDeleteReason') }}",
            type: "POST",
            data: { _token: "{{ csrf_token() }}",reason: reason, id :id },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted!", "You deleted the respondent: "+name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
       }
    });  
});
  
  /*$(document).on("click", ".delete_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");
    swal(
      {
        title: "Are you sure to delete",
        text: "Name: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('respondents.index') }}/" + id,
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}" },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted", "You deleted the respondent: " + name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });*/
</script>
@endsection