@extends('layouts.app')
@section('title', 'Duplicate Respondents')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    @if(session('success'))
    <div class="alert alert-success">{{ session("success") }}</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="respondents_table">
        <thead>
          <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Date Of Birth</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('footer')
<script>
  $(document).ready(function() {
        $("#respondents_table").DataTable({
            sDom: "Rlfrtip",
            processing: true,
            serverSide: true,
            pageLength: 10,
            order: [[1, "asc"]],
            ajax: "{{ route('DTDuplicateRespondents') }}",
            columns: [
                { data: "id", name: "id" },
                { data: "first_name", name: "first_name" },
                { data: "last_name", name: "last_name" },
                { data: "dob", name: "dob" },
                { data: "email", name: "email" },
                { data: "actions", name: "actions", orderable: false, searchable: false }
                        
            ]
        });
    });

  $(document).on("click", ".delete_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");

    swal({
  title: "Delete",
  text: "Please Enter The Reason",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  confirmButtonText: "Delete",
  cancelButtonText: "Cancel",
  confirmButtonClass: "btn-danger",
  
  
},
function(reason){
  if (reason === false){
    swal.showInputError("Please Fill The Reason!");
  }else if (reason === "") {
    swal.showInputError("Please Fill The Reason!");
    return false
  } else{
    $.ajax({
            url: "{{ route('duplicateDeleteReason') }}",
            type: "POST",
            data: { _token: "{{ csrf_token() }}",reason: reason, id :id },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted!", "You deleted the respondent: "+name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
    
  }

  
});
    
  });

</script>
@endsection