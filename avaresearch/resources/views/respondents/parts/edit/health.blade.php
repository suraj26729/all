<div class="row mb-4">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="health_problems">Health problems</label>
            <div class="health_pro_list">
                <ul class="health_problem_list">
                    @php
                    $selected = [];
                    if($respondent->health && $respondent->health->health_problems)
                    $selected = explode(',',$respondent->health->health_problems);
                    @endphp
                    @foreach ($health_problems as $item)
                    <li class="health_problem_li" style="width: 300px; display: inline-block; float: left;">
                        <input type="checkbox" class="health_problems" id="health_problem_{{ $item->id }}"
                            name="health_problems[]" value="{{ $item->name }}"
                            {{ in_array($item->name, $selected) ? 'checked' : '' }}>&nbsp;{{ $item->name }}
                        @if($item->name == 'Other')
                        <div class="form-group other_health_problem_wrap"
                            style="{{ in_array($item->name, $selected) ? '' : 'display:none;' }}">
                            <input type="text" class="form-control" name="other_health_problem"
                                id="other_health_problem" value="{{ $respondent->health->other_health_problem }}" />
                        </div>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="dietary_requirements">Dietary Requirments</label>
            <div class="dietaryname">
                @php
                $collection = [
                'Dairy Free',
                'Gluten Free',
                'Lactose Free',
                'Vegan',
                'Vegetarian',
                'None'
                ];
                $selected = [];
                if($respondent->health && $respondent->health->dietary_requirements)
                $selected = explode(',',$respondent->health->dietary_requirements);
                @endphp
                @foreach ($collection as $item)
                <input type="checkbox" class="check_dietary" name="dietary_requirements[]" value="{{ $item }}"
                    {{ in_array($item, $selected) ? 'checked' : '' }}>
                {{ $item }} &nbsp;
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="health_funds">Health Funds</label>
            <div id="health_problem_list">
                <ul id="health_problem_ul">
                    @php
                    $selected = [];
                    if($respondent->health && $respondent->health->health_funds)
                    $selected = explode(',',$respondent->health->health_funds);
                    @endphp
                    @foreach ($health_funds as $item)
                    <li style="width: 300px; display: inline-block; float: left;">
                        <input type="checkbox" class="health_funds" name="health_funds[]"
                            id="health_funds_{{ $item->id }}" value="{{ $item->id }}"
                            {{ in_array($item->id, $selected) ? 'checked' : '' }}>
                        &nbsp; {{ $item->name }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-3 offset-md-3">
        <div class="form-group other_health_fund_wrap" style="display:none;">
            <input type="text" name="other_health_fund" id="other_health_fund" class="form-control"
                placeholder="Please Specify"
                value="{{ $respondent->health ? $respondent->health->other_health_fund : '' }}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="cigarette_brands">Cigratte Brands</label>
            <div>
                @php
                $collection = [
                "Smoke_None" => "Doesn't Smoke",
                "Smoke_Regular" => "Regular Smoker",
                "Smoke_Social" => "Social Smoker",
                "Smoke_Quit" => "Quit Smoking",
                "Smoke_Vaping" => "E-cigarettes or Vaping"
                ];

                @endphp
                @foreach ($collection as $key => $item)
                <input class="chs" type="checkbox" id="{{ $key }}" name="cigarette_brands" value="{{ $item }}"
                    {{ $respondent->health && $respondent->health->cigarette_brands==$item ? 'checked' : '' }}>
                {{ $item }} &nbsp;
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="partner_cigarette_brands">Partner Cigratte Brands</label>
            <div>
                @php
                $collection = [
                "Smoke_None1" => "Doesn't Smoke",
                "Smoke_Regular1" => "Regular Smoker",
                "Smoke_Social1" => "Social Smoker",
                "Smoke_Quit1" => "Quit Smoking",
                "Smoke_Vaping1" => "E-cigarettes or Vaping"
                ];

                @endphp
                @foreach ($collection as $key => $item)
                <input class="chs" type="checkbox" id="{{ $key }}" name="partner_cigarette_brands" value="{{ $item }}"
                    {{ $respondent->health && $respondent->health->partner_cigarette_brands==$item ? 'checked' : '' }}>
                {{ $item }} &nbsp;
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="organ_donate">Are you in favour of organ donation?</label>
            <div>
                <input class="chs" type="checkbox" id="organ_yes" name="organ_donate" value="1"
                    {{ $respondent->health && $respondent->health->organ_donate==1 ? 'checked' : '' }}>Yes
                &nbsp;
                <input class="chs" type="checkbox" id="organ_no" name="organ_donate" value="0"
                    {{ $respondent->health && $respondent->health->organ_donate==0 ? 'checked' : '' }}>No &nbsp;
            </div>
        </div>
    </div>
</div>

<div class="row" hidden id="organ_wrap">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="donated_organ">Which organs have you registered for
                donation?</label>
            <select id="donated_organ" name="donated_organ" class="form-control">
                <option value="">Select</option>
                @php
                $collection = [
                'eyes' => 'Eyes',
                'liver' => 'Liver',
                'heart' => 'Heart',
                'lungs' => 'Lungs',
                'other' => 'Other',
                'none' => 'None'
                ];
                @endphp
                @foreach ($collection as $key => $item)
                <option value="{{ $key }}"
                    {{ $respondent->health && $respondent->health->donated_organ==$key ? 'selected' : '' }}>{{ $item }}
                </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row" id="cig_brands_wrap" style="display:none;">
    <div class="col-md-12">
        <div class="form-group">
            <label for="quite_smoking_methods" class="control-label">Quit Smoking Methods</label>
            <div>
                @php
                $selected = [];
                if($respondent->health && $respondent->health->quite_smoking_methods)
                $selected = explode(',',$respondent->health->quite_smoking_methods);
                @endphp
                <input type="checkbox" name="quite_smoking_methods[]" value="Nicotine Replacement Technology (NRT)"
                    {{ in_array('Nicotine Replacement Technology (NRT)', $selected) ? 'checked' : '' }}>
                Nicotine Replacement Technology (NRT) &nbsp;
                <input type="checkbox" name="quite_smoking_methods[]" value="Therapy"
                    {{ in_array('Therapy', $selected) ? 'checked' : '' }}> Therapy &nbsp;
            </div>
        </div>
        <div class="form-group">
            <label for="nicotine_replacement" class="control-label">Nicotine Replacement Technologies
                Used</label>
            <div>
                @php
                $collection = [
                'Combination therapy',
                'Gum',
                'Inhaler',
                'Lozenges',
                'Mouth Spray',
                'Patches'
                ];
                $selected = [];
                if($respondent->health && $respondent->health->nicotine_replacement)
                $selected = explode(',',$respondent->health->nicotine_replacement);
                @endphp
                @foreach ($collection as $item)
                <input type="checkbox" name="nicotine_replacement[]" value="{{ $item }}"
                    {{ in_array($item, $selected) ? 'checked' : '' }}> {{ $item }} &nbsp;
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group" id="cig_my_brands_wrap" style="display:none;">
            <label for="brands" class="control-label">Your Brands</label>
            <div>
                <ul style="list-style:none;">
                    @php
                    $selected = [];
                    if($respondent->health && $respondent->health->brands)
                    $selected = explode(',',$respondent->health->brands);
                    @endphp
                    @foreach($brands as $brand)
                    <li>
                        <input type="checkbox" id="check_brand-{{ $brand->id }}" name="brands[]"
                            value="{{ $brand->name }}"
                            {{ in_array($brand->name, $selected) ? 'checked' : '' }}>&nbsp;{{ $brand->name }}
                    </li>
                    @endforeach
                    <li style="display:none;" id="other_cig_brands_wrap">
                        <input type="text" name="orther_brands" placeholder="Others Brands Name" class="form-control"
                            value="{{ $respondent->health ? $respondent->health->orther_brands : '' }}">
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group" id="cig_partner_brands_wrap" style="display:none;">
            <label for="" class="control-label">Partner's Brands</label>
            <div>
                <ul style="list-style:none;">
                    @php
                    $selected = [];
                    if($respondent->health && $respondent->health->partner_brands)
                    $selected = explode(',',$respondent->health->partner_brands);
                    @endphp
                    @foreach($brands as $brand)
                    <li>
                        <input type="checkbox" id="check_pbrand-{{ $brand->id }}" name="partner_brands[]"
                            value="{{ $brand->name }}"
                            {{ in_array($brand->name, $selected) ? 'checked' : '' }}>&nbsp;{{ $brand->name }}
                    </li>
                    @endforeach
                    <li style="display:none;" id="other_cigp_brands_wrap">
                        <input type="text" name="other_partner_brands" placeholder="Others Brands Name"
                            class="form-control"
                            value="{{ $respondent->health ? $respondent->health->other_partner_brands : '' }}" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(function(){
        $('input[name=organ_donate]:checked').trigger('click');
        $('input[name=cigarette_brands]:checked').trigger('click');
        $('input[name=partner_cigarette_brands]:checked').trigger('click');
        $('input#check_brand-131').trigger('change');
        $('input#check_pbrand-131').trigger('change');
        $('#health_funds_21').trigger('change');
    });
    $('input[name=organ_donate]').change(function(){
        var val = $(this).val();
        if(val == 1)
            $('#organ_wrap').removeAttr('hidden');
        else
            $('#organ_wrap').prop('hidden', true);
    });
    $('input[name=cigarette_brands]').change(function(){
        if($(this).val() != "Doesn't Smoke"){
            if(!$('#cig_brands_wrap').is(':visible')) $('#cig_brands_wrap').show();
            if(!$('#cig_my_brands_wrap').is(':visible')) $('#cig_my_brands_wrap').show();
        } else {
            if($('#cig_brands_wrap').is(':visible') && $('input[name=cigarette_brands]:checked').val() == "Doesn't Smoke" ) $('#cig_brands_wrap').hide();
            if($('#cig_my_brands_wrap').is(':visible')) $('#cig_my_brands_wrap').hide();
        }
    });
    $('input[name=partner_cigarette_brands]').change(function(){
        if($(this).val() != "Doesn't Smoke"){
            if(!$('#cig_brands_wrap').is(':visible')) $('#cig_brands_wrap').show();
            if(!$('#cig_partner_brands_wrap').is(':visible')) $('#cig_partner_brands_wrap').show();
        } else {
            if($('#cig_brands_wrap').is(':visible') && $('input[name=partner_cigarette_brands]:checked').val() == "Doesn't Smoke") $('#cig_brands_wrap').hide();
            if($('#cig_partner_brands_wrap').is(':visible')) $('#cig_partner_brands_wrap').hide();
        }
    });
    $('input#check_brand-131').change(function(){
        if($(this).is(':checked'))
            $('#other_cig_brands_wrap').show();
        else{
            $('[name=other_cig_brands]').val('');
            $('#other_cig_brands_wrap').hide();
        }
    });
    $('input#check_pbrand-131').change(function(){
        if($(this).is(':checked'))
            $('#other_cigp_brands_wrap').show();
        else{
            $('[name=other_cigp_brands]').val('');
            $('#other_cigp_brands_wrap').hide();
        }
    });
    $('#health_funds_21').change(function(){
        if($(this).is(':checked'))
            $('.other_health_fund_wrap').show();
        else{
            $('#other_health_fund').val('');
            $('.other_health_fund_wrap').hide();
        }
    });
    $('#health_problem_36').change(function(){
        $('#other_health_problem').val('');
        if($(this).is(':checked'))
            $('.other_health_problem_wrap').show();
        else
            $('.other_health_problem_wrap').hide();
    });
</script>
@endpush