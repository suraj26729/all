<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="home_ownership" class="control-label">Home Ownership</label>
            <select name="home_ownership" class="form-control">
                <option value="">Select</option>
                <option value="I own a commercial property"
                    {{ $respondent->houseHold && $respondent->houseHold->home_ownership=='I own a commercial property' ? 'selected' : '' }}>
                    I own a commercial property</option>
                <option value="I own one or more personal investment properties"
                    {{ $respondent->houseHold && $respondent->houseHold->home_ownership=='I own one or more personal investment properties' ? 'selected' : '' }}>
                    I own one or more personal investment
                    properties</option>
                <option value="I own the house I live in with a mortgage"
                    {{ $respondent->houseHold && $respondent->houseHold->home_ownership=='I own the house I live in with a mortgage' ? 'selected' : '' }}>
                    I own the house I live in with a mortgage
                </option>
                <option value="I own the house I live in without a mortgage"
                    {{ $respondent->houseHold && $respondent->houseHold->home_ownership=='I own the house I live in without a mortgage' ? 'selected' : '' }}>
                    I own the house I live in without a
                    mortgage</option>
                <option value="Own"
                    {{ $respondent->houseHold && $respondent->houseHold->home_ownership=='Own' ? 'selected' : '' }}>
                    Own</option>
                <option value="Rent"
                    {{ $respondent->houseHold && $respondent->houseHold->home_ownership=='Rent' ? 'selected' : '' }}>
                    Rent</option>
                <option value="Rent and Own"
                    {{ $respondent->houseHold && $respondent->houseHold->home_ownership=='Rent and Own' ? 'selected' : '' }}>
                    Rent and Own</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="residence_type" class="control-label">Residence Type</label>
            <select name="residence_type" class="form-control">
                <option value="">Select</option>
                <option value="House"
                    {{ $respondent->houseHold && $respondent->houseHold->residence_type=='House' ? 'selected' : '' }}>
                    House</option>
                <option value="Townhouse"
                    {{ $respondent->houseHold && $respondent->houseHold->residence_type=='Townhouse' ? 'selected' : '' }}>
                    Townhouse</option>
                <option value="Unit"
                    {{ $respondent->houseHold && $respondent->houseHold->residence_type=='Unit' ? 'selected' : '' }}>
                    Unit</option>
                <option value="Villa"
                    {{ $respondent->houseHold && $respondent->houseHold->residence_type=='Villa' ? 'selected' : '' }}>
                    Villa</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">       
            <label for="household_type" class="control-label">Household Type</label>
            <select name="household_type" class="form-control">
                
                <option value="">Select</option>
                @php
                $array = [
                'All my children have left home',
                'Double Income with Kids',
                'Double Income without Kids',
                'I live with my children',
                'Living with Parents',
                'Single Income with Kids',
                'Single Income without Kids',
                'Single Parent',
                'Widow',
                'With another family',
                'With Flat Mate(s)',
                'With Grandparents',
                'With Partner'
                ];
                @endphp
                @foreach ($array as $item)
                <option value="{{ $item }}"
                    {{ $respondent->houseHold && $respondent->houseHold->household_type==$item ? 'selected' : '' }}>
                    {{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="marital_status" class="control-label">Marital Status</label>
            <select name="marital_status" class="form-control">
                <option value="">Select</option>
                @php
                $collection = [
                'Defacto',
                'Divorced',
                'Engaged',
                'Married',
                'Seperated',
                'Single',
                'Widow'
                ];
                @endphp
                @foreach ($collection as $item)
                <option value="{{ $item }}"
                    {{ $respondent->houseHold && $respondent->houseHold->marital_status==$item ? 'selected' : '' }}>
                    {{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="income_level" class="control-label">Household Income Level</label>
            <select name="income_level" class="form-control">
                <option value="">Select</option>
                @php
                $collection = [
                'under $40,000',
                '$40,000 to $49,999',
                '$50,000 to $59,999',
                '$60,000 to $69,999',
                '$70,000 to $79,999',
                '$80,000 to $89,999',
                '$90,000 to $99,999',
                '$100,000 to $149,999',
                '$150,000 to $199,999',
                '$200,000 or greater'
                ];
                @endphp
                @foreach ($collection as $item)
                <option value="{{ $item }}"
                    {{ $respondent->houseHold && $respondent->houseHold->income_level==$item ? 'selected' : '' }}>
                    {{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="pets" class="control-label">What type of pets you have?</label><br />
            @php
            $collection = [
            'Bird',
            'Cat',
            'Dog',
            'Fish',
            'None'
            ];
            $selected = [];
            if($respondent->houseHold && $respondent->houseHold->pets)
            $selected = explode(',',$respondent->houseHold->pets);
            @endphp
            @foreach ($collection as $item)
            <input type="checkbox" class="check_class" name="pets[]" value="{{ $item }}"
                {{ in_array($item, $selected) ? 'checked' : '' }}> {{ $item }} &nbsp;
            @endforeach
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="residency_status" class="control-label">Residency Status</label>
            <select name="residency_status" class="form-control">
                <option value="">Select</option>
                @foreach ($residency_status as $item)
                <option value="{{ $item->name }}"
                    {{ $respondent->houseHold && $respondent->houseHold->residency_status==$item->name ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="birth_country" class="control-label">Birth Country</label>
            <select name="birth_country" class="form-control" id="birth_country">
                <option value="">Select</option>
                @foreach ($countries as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->houseHold && $respondent->houseHold->birth_country==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group other_country" style="display:none;">
            <label for="other_country" class="control-label">Other Birth Country</label>
            <input type="text" class="form-control" name="other_country"
                value="{{ $respondent->houseHold ? $respondent->houseHold->other_country : '' }}" />
        </div>
        <div class="form-group">
            <label for="original_country" class="control-label">Origin Country</label>
            <select name="original_country" class="form-control">
                <option value="">Select</option>
                @foreach ($countries as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->houseHold && $respondent->houseHold->original_country==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>        
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="children" class="control-label">Children</label><br />
            <input class="chs" type="checkbox" id="haveChildren" name="children" value="1"
                {{ $respondent->houseHold && $respondent->houseHold->children==1 ? 'checked' : '' }}><b> I have
                Children</b>
            <br>
            <input class="chs" type="checkbox" id="HasNoChildren" name="children" value="0"
                {{ $respondent->houseHold && $respondent->houseHold->children==0 ? 'checked' : '' }}><b> I have
                no Children</b>
        </div>
        <div id="childrenDetailsDiv"
            {{ $respondent->houseHold && $respondent->houseHold->children==1 ? '' : 'hidden' }}>
            <label for="ChildrenAllowed" class="control-label">
                <input id="ChildrenAllowed" name="children_allowed" type="checkbox" value="1"
                    {{ $respondent->houseHold && $respondent->houseHold->children_allowed==1 ? 'checked' : '' }} />
                Please indicate if you are interested in allowing your
                children to participate
                in paid market research.</label><br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name <span style="color:red;">*</span></th>
                            <th>Month and Year of Birth <span style="color:red;">*</span></th>
                            <th>Gender <span style="color:red;">*</span></th>
                            <th>Left Home</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 0; $i <= 6; $i++) <tr>
                            <td>
                                <input id="child_{{ $i }}_name" name="child_name[]" class="form-control" type="text"
                                    value="{{ $respondent->childrens && count($respondent->childrens) && isset($respondent->childrens[$i]) ? $respondent->childrens[$i]->child_name : ''}}" />
                            </td>
                            <td>
                                {{-- <input id="child_{{ $i }}_dob" name="child_dob[]" class="form-control date"
                                type="text"
                                value="{{ $respondent->childrens && count($respondent->childrens) && isset($respondent->childrens[$i]) ? $respondent->childrens[$i]->dob : ''}}"
                                /> --}}
                                <div class="input-group">
                                    <select name="birth_month[]" id="birth_month_{{ $i }}" class="form-control">
                                        <option value="">Month</option>
                                        @for($m = 1; $m <= 12; $m++) <option value="{{ $m }}"
                                            {{ $respondent->childrens && count($respondent->childrens) && isset($respondent->childrens[$i]) && $respondent->childrens[$i]->birth_month==$m ? 'selected' : '' }}>
                                            {{ $m }}</option> @endfor
                                    </select>
                                    <select name="birth_year[]" id="birth_year_{{ $i }}" class="form-control">
                                        <option value="">Year</option>
                                        @for($y = date('Y'); $y >= date('Y') - 100; $y--) <option value="{{ $y }}"
                                            {{ $respondent->childrens && count($respondent->childrens) && isset($respondent->childrens[$i]) && $respondent->childrens[$i]->birth_year==$y ? 'selected' : '' }}>
                                            {{ $y }}
                                        </option> @endfor
                                    </select>
                                </div>
                            </td>
                            <td>
                                <select id="child_{{ $i }}_gender" name="child_gender[]" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Male"
                                        {{ $respondent->childrens && count($respondent->childrens) && isset($respondent->childrens[$i]) &&  $respondent->childrens[$i]->gender=='Male' ? 'selected' : '' }}>
                                        Male</option>
                                    <option value="Female"
                                        {{ $respondent->childrens && count($respondent->childrens) && isset($respondent->childrens[$i]) &&  $respondent->childrens[$i]->gender=='Female' ? 'selected' : '' }}>
                                        Female</option>
                                </select>
                            </td>
                            <td>
                                <input id="child_{{ $i }}_left_home" name="left_home[]" type="checkbox" value="1"
                                    {{ $respondent->childrens && count($respondent->childrens) && isset($respondent->childrens[$i]) &&  $respondent->childrens[$i]->left_home==1 ? 'checked' : '' }} />
                            </td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(function () { 
        $('#birth_country').trigger('change');
    });
    $('input[name=children]').change(function(){
        var val = $(this).val();
        if(val == 1){
            $('#childrenDetailsDiv').removeAttr('hidden');
        } else {
            $('#childrenDetailsDiv').prop('hidden', true);
        }
    });
    $('#birth_country').change(function(){
        var val = $(this).val();
        if(val == 249){
            $('.other_country').show();
        } else {
            $('.other_country').hide();
        }
    });
    // $('[name=original_country]').change(function(){
    //     var country_id = $(this).val() || 0;
    //     $.ajax({
    //         url: "{{ route('ethnicity') }}",
    //         type: 'POST',
    //         data: { _token: '{{ csrf_token() }}',country_id: country_id},
    //         beforeSend: function(){
                
    //         },
    //         success: function(res){
    //             //console.log(res);
    //             if(res){
    //                 $("[name=ethnic_background]").empty();
    //                 $("[name=ethnic_background]").append('<option value="">Select</option>');
    //                 $.each(res,function(key,value){
    //                     $("[name=ethnic_background]").append('<option value="'+value+'">'+value+'</option>');
    //                 });
                    
    //             } else {
    //                 $("[name=ethnic_background]").append('<option>No Data Available</option>');
    //             }
    //         },
    //         error: function(err){ console.log(err); }
    //     });
    // });
</script>
@endpush