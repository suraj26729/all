<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="vehicle_make_id" class="control-label">Vehicle1-Make</label>
            <select name="vehicle_make_id" class="form-control">
                <option value="">Select</option>
                @foreach ($vehicle_makes as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->product && $respondent->product->vehicle_make_id==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="vehicle2_make_id" class="control-label">Vehicle2-Make</label>
            <select name="vehicle2_make_id" class="form-control">
                <option value="">Select</option>
                @foreach ($vehicle_makes as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->product && $respondent->product->vehicle2_make_id==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="vehicle_type_id" class="control-label">Vehicle1-Type</label>
            <select name="vehicle_type_id" class="form-control has_other_option">
                <option value="">Select</option>
                @foreach ($vehicle_types as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->product && $respondent->product->vehicle_type_id==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
                <option value="-1"
                    {{ $respondent->product && $respondent->product->vehicle_type_id==-1 ? 'selected' : '' }}>Other
                </option>
            </select>
        </div>
        <div class="form-group other_vehicle_type_id"
            style="{{ $respondent->product && $respondent->product->vehicle_type_id==-1 ? '' : 'display:none' }}">
            <input type="text" class="form-control" name="other_vehicle_type"
                value="{{ $respondent->product ? $respondent->product->other_vehicle_type : '' }}"
                placeholder="Please enter it">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="vehicle2_type_id" class="control-label">Vehicle2-Type</label>
            <select name="vehicle2_type_id" class="form-control has_other_option">
                <option value="">Select</option>
                @foreach ($vehicle_types as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->product && $respondent->product->vehicle2_type_id==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
                <option value="-1"
                    {{ $respondent->product && $respondent->product->vehicle2_type_id==-1 ? 'selected' : '' }}>Other
                </option>
            </select>
        </div>
        <div class="form-group other_vehicle2_type_id"
            style="{{ $respondent->product && $respondent->product->vehicle2_type_id==-1 ? '' : 'display:none' }}">
            <input type="text" class="form-control" name="other_vehicle2_type"
                value="{{ $respondent->product ? $respondent->product->other_vehicle2_type : '' }}"
                placeholder="Please enter it">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="vehicle_model" class="control-label">Vehicle1-Model</label>
            <input type="text" class="form-control" name="vehicle_model"
                value="{{ $respondent->product ? $respondent->product->vehicle_model : '' }}" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="vehicle2_model" class="control-label">Vehicle2-Model</label>
            <input type="text" class="form-control" name="vehicle2_model"
                value="{{ $respondent->product ? $respondent->product->vehicle2_model : '' }}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="vehicle_year" class="control-label">Vehicle1-Year</label>
            <input type="text" class="form-control" name="vehicle_year"
                value="{{ $respondent->product ? $respondent->product->vehicle_year : '' }}" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="vehicle2_year" class="control-label">Vehicle2-Year</label>
            <input type="text" class="form-control" name="vehicle2_year"
                value="{{ $respondent->product ? $respondent->product->vehicle2_year : '' }}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="travel_location" class="control-label">Travel Location</label>
            <select name="travel_location" class="form-control">
                <option value="">Select</option>
                <option value="Both"
                    {{ $respondent->product && $respondent->product->travel_location=='Both' ? 'selected' : '' }}>
                    Both</option>
                <option value="Interstate"
                    {{ $respondent->product && $respondent->product->travel_location=='Interstate' ? 'selected' : '' }}>
                    Interstate</option>
                <option value="Overseas"
                    {{ $respondent->product && $respondent->product->travel_location=='Overseas' ? 'selected' : '' }}>
                    Overseas</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="travel_reason" class="control-label">Travel Reason</label>
            <select name="travel_reason" class="form-control">
                <option value="">Select</option>
                <option value="Both"
                    {{ $respondent->product && $respondent->product->travel_reason=='Both' ? 'selected' : '' }}>Both
                </option>
                <option value="Business"
                    {{ $respondent->product && $respondent->product->travel_reason=='Business' ? 'selected' : '' }}>
                    Business</option>
                <option value="Personal"
                    {{ $respondent->product && $respondent->product->travel_reason=='Personal' ? 'selected' : '' }}>
                    Personal</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="travel_frequency" class="control-label">Travel Frequency</label>
            <select name="travel_frequency" class="form-control">
                <option value="">Select</option>
                <option value="Never"
                    {{ $respondent->product && $respondent->product->travel_frequency=='Never' ? 'selected' : '' }}>
                    Never</option>
                <option value="Sometimes"
                    {{ $respondent->product && $respondent->product->travel_frequency=='Sometimes' ? 'selected' : '' }}>
                    Sometimes</option>
                <option value="Once every few years"
                    {{ $respondent->product && $respondent->product->travel_frequency=='Once every few years' ? 'selected' : '' }}>
                    Once
                    every few years</option>
                <option value="Atleast once a year"
                    {{ $respondent->product && $respondent->product->travel_frequency=='Atleast once a year' ? 'selected' : '' }}>
                    Atleast once a year</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="rewards_progammes" class="control-label">Rewards Programmes
            </label>
            <div class="row">
                <div id="reward_programs_list" class="col-md-12">
                    <ul id="reward_programs_ul">
                        @php
                        $selected = [];
                        if($respondent->product && $respondent->product->rewards_progammes)
                        $selected = explode(',',$respondent->product->rewards_progammes);
                        @endphp
                        @foreach ($reward_programs as $item)
                        <li class="reward_programs_li" style="width: 400px; display: inline-block; float: left;">
                            <input type="checkbox" class="rewards_programmes" name="rewards_progammes[]"
                                value="{{ $item->id }}"
                                {{ in_array($item->id, $selected) ? 'checked' : '' }}>&nbsp;{{ $item->name }}
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="technology_use" class="control-label">Technology Use</label>
            <div class="row">
                <div id="technology_use_list" class="col-md-12">
                    <ul id="technology_use_ul">
                        @php
                        $selected = [];
                        if($respondent->product && $respondent->product->technology_use)
                        $selected = explode(',',$respondent->product->technology_use);
                        @endphp
                        @foreach ($technology_uses as $item)
                        <li class="technology_use_li" style="width: 300px; display: inline-block; float: left;">
                            <input type="checkbox" class="technology_use" name="technology_use[]"
                                value="{{ $item->id }}"
                                {{ in_array($item->id, $selected) ? 'checked' : '' }}>&nbsp;{{ $item->name }}
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="general_products" class="control-label">General Products</label>
            <div>
                @php
                $selected = [];
                if($respondent->product && $respondent->product->general_products)
                $selected = explode(',',$respondent->product->general_products);
                @endphp
                <input type="checkbox" name="general_products[]" value="Cereal"
                    {{ in_array('Cereal', $selected) ? 'checked' : '' }}>&nbsp;Cereal
                <input type="checkbox" name="general_products[]" value="Coffee"
                    {{ in_array('Coffee', $selected) ? 'checked' : '' }}>&nbsp;Coffee
                <input type="checkbox" name="general_products[]" value="Confectionary"
                    {{ in_array('Confectionary', $selected) ? 'checked' : '' }}>&nbsp;Confectionary
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="internet_provider" class="control-label">Internet Provider</label>
            <select name="internet_provider" id="internet_provider" class="form-control">
                <option value="">Select</option>
                @foreach ($internet_providers as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->product && $respondent->product->internet_provider==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="alcohol_consumed" class="control-label">Alcohol Consumed</label>
            <select name="alcohol_consumed" id="alcohol_consumed" class="form-control">
                <option value="">Select</option>
                @foreach ($alcohol_consumeds as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->product && $respondent->product->alcohol_consumed==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="optical" class="control-label">Optical</label>
            <div class="opticalglass">
                @php
                $selected = [];
                if($respondent->product && $respondent->product->optical)
                $selected = explode(',',$respondent->product->optical);
                @endphp
                <input type="checkbox" class="optical" name="optical[]" value="Contact Lenses"
                    {{ in_array('Contact Lenses', $selected) ? 'checked' : '' }}> Contact Lenses &nbsp;
                <input type="checkbox" class="optical" name="optical[]" value="Glasses Prescription"
                    {{ in_array('Glasses Prescription', $selected) ? 'checked' : '' }}> Glasses
                Prescription &nbsp;
                <input type="checkbox" class="optical" name="optical[]" value="Visually Impaired"
                    {{ in_array('Visually Impaired', $selected) ? 'checked' : '' }}> Visually Impaired
                &nbsp;
                <input type="checkbox" class="optical1" name="optical[]" value="None"
                    {{ in_array('None', $selected) ? 'checked' : '' }}> None &nbsp;
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="lens_brand" class="control-label">Lens Brand</label>
            <input type="text" class="form-control" name="lens_brand"
                value="{{ $respondent->product ? $respondent->product->lens_brand : '' }}" />
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(document).on('change', '.has_other_option', function(){
        var value = $(this).val();
        var name = $(this).attr('name');
        if(value == -1 || value.toLowerCase() == 'other'){
            $('.other_'+name+' input').val('');
            $('.other_'+name).show();
        } else {
            $('.other_'+name+' input').val('');
            $('.other_'+name).hide();
        }
    });
</script>
@endpush