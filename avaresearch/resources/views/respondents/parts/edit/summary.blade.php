<div class="row mb-4">
  <div class="col-md-12">
    <div class="form-group">
      <label for="preferred_research_location" class="control-label">Preferred Research Locations</label>
      <div id="pref_location_list">
        <ul id="loc_ul">
          @php
          $selected = [];
          if($respondent->summary && $respondent->summary->preferred_research_location)
          $selected = explode(',',$respondent->summary->preferred_research_location);
          @endphp
          @foreach($preferred_locations as $key => $item)
          <li style="width: 300px; display: inline-block; float: left;">
            <input type="checkbox" class="res_location" name="preferred_research_location[]" value="{{ $item->id }}"
              {{ in_array($item->id, $selected) ? 'checked' : '' }}>&nbsp;{{ $item->name }}
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="availability_times" class="control-label">Availability</label>
      <div class="availname">
        @php
        $selected = [];
        if($respondent->summary && $respondent->summary->availability_times)
        $selected = explode(',',$respondent->summary->availability_times);

        $collection = [
        'After Hours',
        'Office Hours',
        'Weekdays',
        'Weekends',
        'Any Time'
        ];
        @endphp
        @foreach ($collection as $item)
        <input type="checkbox" class="check_avail" name="availability_times[]" value="{{ $item }}"
          {{ in_array($item, $selected) ? 'checked' : '' }}> {{ $item }}
        &nbsp;
        @endforeach
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="session_type" class="control-label">Session Type</label>
      <div>
        @php
        $selected = [];
        if($respondent->summary && $respondent->summary->session_type)
        $selected = explode(',',$respondent->summary->session_type);

        $collection = [
        'Group Discussion',
        'Face To Face Interview',
        'Online Surveys',
        'Taste Testing',
        'Telephone Interview'
        ];
        @endphp
        @foreach ($collection as $item)
        <input type="checkbox" name="session_type[]" value="{{ $item }}"
          {{ in_array($item, $selected) ? 'checked' : '' }}> {{ $item }}
        &nbsp;
        @endforeach
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="preferred_contact_method" class="control-label">Preferred Contact Method</label>
      <div>
        @php
        $selected = [];
        if($respondent->summary && $respondent->summary->preferred_contact_method)
        $selected = explode(',',$respondent->summary->preferred_contact_method);

        $collection = [
        'Email',
        'Mobile Phone',
        'SMS/Text Message',
        'Home Phone',
        'Work Phone'
        ];
        @endphp
        @foreach ($collection as $item)
        <input type="checkbox" name="preferred_contact_method[]" value="{{ $item }}"
          {{ in_array($item, $selected) ? 'checked' : '' }}> {{ $item }} &nbsp;
        @endforeach
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="additional_notes" class="control-label">Additional Notes</label>
      <textarea name="additional_notes" rows="5"
        class="form-control">{{ $respondent->summary ? $respondent->summary->additional_notes : '' }}</textarea>
    </div>
  </div>
</div>