<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">I am :</label><br />
            @php
            $collection = [
            'Student' => 'a student',
            'Business Owner' => 'a business Owner',
            'Retired' => 'retired',
            'Currenty Unemployed' => 'currently unemployed',
            'Stay at home Parent' => 'a stay at home Parent',
            'employed' => 'employed',
            'Sole Trader' => 'Sole Trader',
            'Contractual' => 'Contractual'
            ];
            @endphp
            @foreach ($collection as $key => $item)
            <input class="chs" type="checkbox" name="profession" value="{{ $key }}"
                {{ $respondent->occupation && $respondent->occupation->profession==$key ? 'checked' : '' }}> {{ $item }}
            &nbsp;
            @endforeach
        </div>
    </div>
</div>

<div class="row" hidden id="company_wrap">
    <div class="col-md-6">
        <div class="form-group">
            <label for="company_name" class="control-label">Company Name</label>
            <input type="text" class="form-control" name="company_name"
                value="{{ $respondent->occupation ? $respondent->occupation->company_name : '' }}" />
        </div>
        <div class="form-group">
            <label for="company_website" class="control-label">Website</label>
            <input type="text" class="form-control" name="company_website"
                value="{{ $respondent->occupation ? $respondent->occupation->company_website : '' }}" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="company_email" class="control-label">Company Email</label>
            <input type="text" class="form-control" name="company_email"
                value="{{ $respondent->occupation ? $respondent->occupation->company_email : '' }}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="occupation_id" class="control-label">Occupation</label>
            <select name="occupation_id" class="form-control">
                <option value="">Select</option>
                @foreach ($occupations as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->occupation && $item->id==$respondent->occupation->occupation_id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_occupation_id" class="control-label">Partner's Occupation</label>
            <select name="partner_occupation_id" class="form-control">
                <option value="">Select</option>
                @foreach ($occupations as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->occupation && $item->id==$respondent->occupation->partner_occupation_id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="occupation_type" class="control-label">Type</label>
            <select name="occupation_type" class="form-control">
                <option value="">Select</option>
                @php
                $collection = [
                'Part Time',
                'Full Time',
                'Casual',
                'Shift',
                'Contractual'
                ]
                @endphp
                @foreach ($collection as $item)
                <option value="{{ $item }}"
                    {{ $respondent->occupation && $respondent->occupation->type==$item ? 'selected' : '' }}>
                    {{ $item }}
                </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="hours_per_week" class="control-label">Hours Per Week</label>
            <input type="text" class="form-control" name="hours_per_week"
                value="{{ $respondent->occupation ? $respondent->occupation->hours_per_week : '' }}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="job_title" class="control-label">Job Title</label>
            <input type="text" class="form-control" name="job_title"
                value="{{ $respondent->occupation ? $respondent->occupation->job_title : '' }}" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_job_title" class="control-label">Partner's Job Title</label>
            <input type="text" class="form-control" name="partner_job_title"
                value="{{ $respondent->occupation ? $respondent->occupation->partner_job_title : '' }}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="employer_name" class="control-label">Employer Name</label>
            <input type="text" class="form-control" name="employer_name"
                value="{{ $respondent->occupation ? $respondent->occupation->employer_name : '' }}" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_employer_name" class="control-label">Partner's Employer Name</label>
            <input type="text" class="form-control" name="partner_employer_name"
                value="{{ $respondent->occupation ? $respondent->occupation->partner_employer_name : '' }}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="employer_size_id" class="control-label">Employer Size</label>
            <select name="employer_size_id" class="form-control">
                <option value="">Select</option>
                @foreach ($employer_sizes as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->occupation && $respondent->occupation->employer_size_id==$item->id ? 'selected': '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_employer_size_id" class="control-label">Partner's Employer Size</label>
            <select name="partner_employer_size_id" class="form-control">
                <option value="">Select</option>
                @foreach ($employer_sizes as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->occupation && $respondent->occupation->partner_employer_size_id==$item->id ? 'selected': '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="employer_turnover" class="control-label">Employer Turnover</label>
            <select name="employer_turnover" class="form-control">
                <option value="">Select</option>
                @php
                $collection = [
                'less than $100k',
                '$100k to $250k',
                '$250k to $500k',
                '$501k to $1m',
                '$1m to $5m',
                'over $5m'
                ]
                @endphp
                @foreach ($collection as $item)
                <option value="{{ $item }}"
                    {{ $respondent->occupation && $respondent->occupation->employer_turnover==$item ? 'selected' : '' }}>
                    {{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_employer_turnover" class="control-label">Partner's Employer Turnover</label>
            <select name="partner_employer_turnover" class="form-control">
                <option value="">Select</option>
                @foreach ($collection as $item)
                <option value="{{ $item }}"
                    {{ $respondent->occupation && $respondent->occupation->partner_employer_turnover==$item ? 'selected' : '' }}>
                    {{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="industry_id" class="control-label">Industry</label>
            <select name="industry_id" class="form-control">
                <option value="">Select</option>
                @foreach ($industries as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->occupation && $respondent->occupation->industry_id==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_industry" class="control-label">Partner Industry</label>
            <select name="partner_industry" class="form-control">
                <option value="">Select</option>
                @foreach ($industries as $item)
                <option value="{{ $item->id }}"
                    {{ $respondent->occupation && $respondent->occupation->partner_industry==$item->id ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="highest_education" class="control-label">Highest Education Level Achieved</label>
            <select name="highest_education" class="form-control">
                <option value="">Select</option>
                @php
                $collection = [
                'School Certificate (year 10)',
                'Higher School Certificate (Year 12)',
                'Cert 1-3 qualification',
                'Cert 4 qualification',
                'Diploma/Advanced Dimploma',
                'Undergraduate Degree',
                'Graduate',
                'Post-graduation Degree'
                ]
                @endphp
                @foreach ($collection as $item)
                <option value="{{ $item }}"
                    {{ $respondent->occupation && $respondent->occupation->highest_education==$item ? 'selected' : '' }}>
                    {{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="education_institute_type" class="control-label">Education Institute Type</label>
            <select name="education_institute_type" class="form-control">
                <option value="">Select</option>
                @php
                $collection = ['Private', 'Public', 'Catholic', 'Goverment'];
                @endphp
                @foreach ($collection as $item)
                <option value="{{ $item }}"
                    {{ $respondent->occupation && $respondent->occupation->education_institute_type==$item ? 'selected' : '' }}>
                    {{ $item }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="education_institute_suburb" class="control-label">Education Institute Suburb</label>
            <input type="text" class="form-control" name="education_institute_suburb"
                value="{{ $respondent->occupation ? $respondent->occupation->education_institute_suburb : '' }}" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="education_year" class="control-label">Education Year</label>
            <input type="text" class="form-control" name="education_year"
                value="{{ $respondent->occupation ? $respondent->occupation->education_year : '' }}" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="education_study_field" class="control-label">Education Study Field</label>
            <input type="text" class="form-control" name="education_study_field"
                value="{{ $respondent->occupation ? $respondent->occupation->education_study_field : '' }}" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_education_study_field" class="control-label">Partner's Education Study Field</label>
            <input type="text" class="form-control" name="partner_education_study_field"
                value="{{ $respondent->occupation ? $respondent->occupation->partner_education_study_field : '' }}" />
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(function(){
        $('input[name=profession]:checked').trigger('click');
    });
    $('input[name=profession]').change(function(){
        if($(this).val() == 'Business Owner')
            $('#company_wrap').removeAttr('hidden');
        else
            $('#company_wrap').prop('hidden', true);
    });
</script>
@endpush