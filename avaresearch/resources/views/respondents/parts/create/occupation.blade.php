<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">I am :</label><br />
            <input class="chs" type="checkbox" name="profession" value="Student"> a student
            &nbsp;
            <input class="chs" type="checkbox" name="profession" value="Business Owner"> a
            business Owner &nbsp;
            <input class="chs" type="checkbox" name="profession" value="Retired"> retired &nbsp;
            <input class="chs" type="checkbox" name="profession" value="Currenty Unemployed"> currently unemployed
            &nbsp;
            <input class="chs" type="checkbox" name="profession" value="Stay at home Parent">
            a stay at home Parent &nbsp;
            <input class="chs" type="checkbox" name="profession" value="employed"> employed
            &nbsp;
            <input class="chs" type="checkbox" name="profession" value="Sole Trader"> Sole
            Trader &nbsp;
            <input class="chs" type="checkbox" name="profession" value="Contractual"> Contractual &nbsp;
        </div>
    </div>
</div>

<div class="row" hidden id="company_wrap">
    <div class="col-md-6">
        <div class="form-group">
            <label for="company_name" class="control-label">Company Name</label>
            <input type="text" class="form-control" name="company_name" />
        </div>
        <div class="form-group">
            <label for="company_website" class="control-label">Website</label>
            <input type="text" class="form-control" name="company_website" />
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group">
            <label for="company_email" class="control-label">Company Email</label>
            <input type="text" class="form-control" name="company_email" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="occupation_id" class="control-label">Occupation</label>
            <select name="occupation_id" class="form-control">
                <option value="">Select</option>
                @foreach ($occupations as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_occupation_id" class="control-label">Partner's Occupation</label>
            <select name="partner_occupation_id" class="form-control">
                <option value="">Select</option>
                @foreach ($occupations as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="occupation_type" class="control-label">Type</label>
            <select name="occupation_type" class="form-control">
                <option value="">Select</option>
                <option value="Part Time">Part Time</option>
                <option value="Full Time">Full Time</option>
                <option value="Casual">Casual</option>
                <option value="Shift">Shift</option>
                <option value="Contractual">Contractual</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="hours_per_week" class="control-label">Hours Per Week</label>
            <input type="text" class="form-control" name="hours_per_week" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="job_title" class="control-label">Job Title</label>
            <input type="text" class="form-control" name="job_title" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_job_title" class="control-label">Partner's Job Title</label>
            <input type="text" class="form-control" name="partner_job_title" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="employer_name" class="control-label">Employer Name</label>
            <input type="text" class="form-control" name="employer_name" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_employer_name" class="control-label">Partner's Employer Name</label>
            <input type="text" class="form-control" name="partner_employer_name" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="employer_size_id" class="control-label">Employer Size</label>
            <select name="employer_size_id" class="form-control">
                <option value="">Select</option>
                @foreach ($employer_sizes as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_employer_size_id" class="control-label">Partner's Employer Size</label>
            <select name="partner_employer_size_id" class="form-control">
                <option value="">Select</option>
                @foreach ($employer_sizes as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="employer_turnover" class="control-label">Employer Turnover</label>
            <select name="employer_turnover" class="form-control">
                <option value="">Select</option>
                <option value="less than $100k">less than $100k</option>
                <option value="$100k to $250k">$100k to $250k</option>
                <option value="$250k to $500k">$250k to $500k</option>
                <option value="$501k to $1m">$501k to $1m</option>
                <option value="$1m to $5m">$1m to $5m</option>
                <option value="over $5m ">over $5m</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_employer_turnover" class="control-label">Partner's Employer Turnover</label>
            <select name="partner_employer_turnover" class="form-control">
                <option value="">Select</option>
                <option value="less than $100k">less than $100k</option>
                <option value="$100k to $250k">$100k to $250k</option>
                <option value="$250k to $500k">$250k to $500k</option>
                <option value="$501k to $1m">$501k to $1m</option>
                <option value="$1m to $5m">$1m to $5m</option>
                <option value="over $5m ">over $5m</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="industry_id" class="control-label">Industry</label>
            <select name="industry_id" class="form-control">
                <option value="">Select</option>
                @foreach ($industries as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_industry" class="control-label">Partner Industry</label>
            <select name="partner_industry" class="form-control">
                <option value="">Select</option>
                @foreach ($industries as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="highest_education" class="control-label">Highest Education Level Achieved</label>
            <select name="highest_education" class="form-control">
                <option value="">Select</option>
                <option value="School Certificate (year 10)">School Certificate (year 10)</option>
                <option value="Higher School Certificate (Year 12)">Higher School Certificate (Year 12)</option>
                <option value="Cert 1-3 qualification">Cert 1-3 qualification</option>
                <option value="Cert 4 qualification">Cert 4 qualification</option>
                <option value="Diploma/Advanced Dimploma">Diploma/Advanced Dimploma</option>
                <option value="Undergraduate Degree">Undergraduate Degree</option>
                <option value="Graduate">Graduate</option>
                <option value="Post-graduation Degree">Post-graduation Degree</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="education_institute_type" class="control-label">Education Institute Type</label>
            <select name="education_institute_type" class="form-control">
                <option value="">Select</option>
                <option value="Private">Private</option>
                <option value="Public">Public</option>
                <option value="Catholic">Catholic</option>
                <option value="Goverment">Government</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="education_institute_suburb" class="control-label">Education Institute Suburb</label>
            <input type="text" class="form-control" name="education_institute_suburb" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="education_year" class="control-label">Education Year</label>
            <input type="text" class="form-control" name="education_year" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="education_study_field" class="control-label">Education Study Field</label>
            <input type="text" class="form-control" name="education_study_field" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="partner_education_study_field" class="control-label">Partner's Education Study Field</label>
            <input type="text" class="form-control" name="partner_education_study_field" />
        </div>
    </div>
</div>

@push('scripts')
<script>
    $('input[name=profession]').change(function(){
        if($(this).val() == 'Business Owner')
            $('#company_wrap').removeAttr('hidden');
        else
            $('#company_wrap').prop('hidden', true);
    });
</script>
@endpush