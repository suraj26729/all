<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="home_ownership" class="control-label">Home Ownership</label>
            <select name="home_ownership" class="form-control">
                <option value="">Select</option>
                <option value="I own a commercial property">I own a commercial property</option>
                <option value="I own one or more personal investment properties">I own one or more personal investment
                    properties</option>
                <option value="I own the house I live in with a mortgage">I own the house I live in with a mortgage
                </option>
                <option value="I own the house I live in without a mortgage">I own the house I live in without a
                    mortgage</option>
                <option value="Own">Own</option>
                <option value="Rent">Rent</option>
                <option value="Rent and Own">Rent and Own</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="residence_type" class="control-label">Residence Type</label>
            <select name="residence_type" class="form-control">
                <option value="">Select</option>
                <option value="House">House</option>
                <option value="Townhouse">Townhouse</option>
                <option value="Unit">Unit</option>
                <option value="Villa">Villa</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="household_type" class="control-label">Household Type</label>
            <select name="household_type" class="form-control">
                <option value="">Select</option>
                <option value="All my children have left home">All my children have left home</option>
                <option value="Double Income with Kids">Double Income with Kids</option>
                <option value="Double Income without Kids">Double Income without Kids</option>
                <option value="I live with my children">I live with my children</option>
                <option value="Living with Parents">Living with Parents</option>
                <option value="Single Income with Kids">Single Income with Kids</option>
                <option value="Single Income without Kids">Single Income without Kids</option>
                <option value="Single Parent">Single Parent</option>
                <option value="Widow">Widow</option>
                <option value="With another family">With another family</option>
                <option value="With Flat Mate(s)">With Flat Mate(s)</option>
                <option value="With Grandparents">With Grandparents</option>
                <option value="With Partner">With Partner</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="marital_status" class="control-label">Marital Status</label>
            <select name="marital_status" class="form-control">
                <option value="">Select</option>
                <option value="Defacto">Defacto</option>
                <option value="Divorced">Divorced</option>
                <option value="Engaged">Engaged</option>
                <option value="Married">Married</option>
                <option value="Seperated">Seperated</option>
                <option value="Single">Single</option>
                <option value="Widow">Widow</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="income_level" class="control-label">Household Income Level</label>
            <select name="income_level" class="form-control">
                <option value="">Select</option>
                <option value="under $40,000">under $40,000</option>
                <option value="$40,000 to $49,999">$40,000 to $49,999</option>
                <option value="$50,000 to $59,999">$50,000 to $59,999</option>
                <option value="$60,000 to $69,999">$60,000 to $69,999</option>
                <option value="$70,000 to $79,999">$70,000 to $79,999</option>
                <option value="$80,000 to $89,999">$80,000 to $89,999</option>
                <option value="$90,000 to $99,999">$90,000 to $99,999</option>
                <option value="$100,000 to $149,999">$100,000 to $149,999</option>
                <option value="$150,000 to $199,999">$150,000 to $199,999</option>
                <option value="$200,000 or greater">$200,000 or greater</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="pets" class="control-label">What type of pets you have?</label><br />
            <input type="checkbox" class="check_class" name="pets[]" value="Bird"> Bird &nbsp;
            <input type="checkbox" class="check_class" name="pets[]" value="Cat"> Cat &nbsp;
            <input type="checkbox" class="check_class" name="pets[]" value="Dog"> Dog &nbsp;
            <input type="checkbox" class="check_class" name="pets[]" value="Fish"> Fish &nbsp;
            <input type="checkbox" class="check_class1" name="pets[]" value="None"> None &nbsp;
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="residency_status" class="control-label">Residency Status</label>
            <select name="residency_status" class="form-control">
                <option value="">Select</option>
                @foreach ($residency_status as $item)
                <option value="{{ $item->name }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="birth_country" class="control-label">Birth Country</label>
            <select name="birth_country" class="form-control" id="birth_country">
                <option value="">Select</option>
                @foreach ($countries as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group other_country" style="display:none;">
            <label for="other_country" class="control-label">Other Birth Country</label>
            <input type="text" class="form-control" name="other_country" />
        </div>
        <div class="form-group">
            <label for="original_country" class="control-label">Origin Country</label>
            <select name="original_country" class="form-control">
                <option value="">Select</option>
                @foreach ($countries as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>        
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="children" class="control-label">Children</label><br />
            <input class="chs" type="checkbox" id="haveChildren" name="children" value="1"><b> I have Children</b>
            <br>
            <input class="chs" type="checkbox" id="HasNoChildren" checked="" name="children" value="0"><b> I have
                no Children</b>
        </div>
        <div id="childrenDetailsDiv" hidden>
            <label for="ChildrenAllowed" class="control-label">
                <input id="ChildrenAllowed" name="children_allowed" type="checkbox" value="1" />
                Please indicate if you are interested in allowing your
                children to participate
                in paid market research.</label><br>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name <span style="color:red;">*</span></th>
                            <th>Date of Birth <span style="color:red;">*</span></th>
                            <th>Gender <span style="color:red;">*</span></th>
                            <th>Left Home</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 0; $i <= 6; $i++) <tr>
                            <td>
                                <input id="child_{{ $i }}_name" name="child_name[]" class="form-control" type="text" />
                            </td>
                            <td>
                                <div class="input-group">
                                    <select name="birth_month[]" id="birth_month_{{ $i }}" class="form-control">
                                        <option value="">Month</option>
                                        @for($m = 1; $m <= 12; $m++) <option value="{{ $m }}">
                                            {{ $m }}</option> @endfor
                                    </select>
                                    <select name="birth_year[]" id="birth_year_{{ $i }}" class="form-control">
                                        <option value="">Year</option>
                                        @for($y = date('Y'); $y >= date('Y') - 100; $y--) <option value="{{ $y }}">
                                            {{ $y }}
                                        </option> @endfor
                                    </select>
                                </div>
                            </td>
                            <td>
                                <select id="child_{{ $i }}_gender" name="child_gender[]" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </td>
                            <td>
                                <input id="child_{{ $i }}_left_home" name="left_home[]" type="checkbox" value="1" />
                            </td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $('input[name=children]').change(function(){
        var val = $(this).val();
        if(val == 1){
            $('#childrenDetailsDiv').removeAttr('hidden');
        } else {
            $('#childrenDetailsDiv').prop('hidden', true);
        }
    });
    $('#birth_country').change(function(){
        var val = $(this).val();
        if(val == 249){
            $('.other_country').show();
        } else {
            $('.other_country').hide();
        }
    });
    // $('[name=original_country]').change(function(){
    //     var country_id = $(this).val() || 0;
    //     $.ajax({
    //         url: "{{ route('ethnicity') }}",
    //         type: 'POST',
    //         data: { _token: '{{ csrf_token() }}',country_id: country_id},
    //         beforeSend: function(){
                
    //         },
    //         success: function(res){
    //             //console.log(res);
    //             if(res){
    //                 $("[name=ethnic_background]").empty();
    //                 $("[name=ethnic_background]").append('<option value="">Select</option>');
    //                 $.each(res,function(key,value){
    //                     $("[name=ethnic_background]").append('<option value="'+value+'">'+value+'</option>');
    //                 });
                    
    //             } else {
    //                 $("[name=ethnic_background]").append('<option>No Data Available</option>');
    //             }
    //         },
    //         error: function(err){ console.log(err); }
    //     });
    // });
</script>
@endpush