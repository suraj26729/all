<div class="row mb-4">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="bank">Banks</label>
            <div id="banks_list">
                <ul id="banks_ul">
                    @foreach ($banks as $item)
                    <li class="bank_li" style="width: 300px; display: inline-block; float: left;">
                        <input type="checkbox" class="bank" name="bank_id[]"
                            value="{{ $item->id }}">&nbsp;{{ $item->name }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="superannuation">Superannuation</label>
            <div id="banks_list">
                <ul id="banks_ul">
                    @foreach ($super_annutions as $item)
                    <li class="bank_li" style="width: 400px; display: inline-block; float: left;">
                        <input type="checkbox" class="superannuation" name="superannuation_id[]"
                            value="{{ $item->name }}">&nbsp;{{ $item->name }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="_non_super_investments">Non-Super Investments</label><br />
            <input type="checkbox" name="_non_super_investments[]" value="Investment Properties">
            Investment Properties &nbsp;
            <input type="checkbox" name="_non_super_investments[]" value="Managed Funds"> Managed
            Funds &nbsp;
            <input type="checkbox" name="_non_super_investments[]" value="Options"> Options &nbsp;
            <input type="checkbox" name="_non_super_investments[]" value="Shares"> Shares &nbsp;
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="insurance_providers">Insurance Providers</label>
            <div id="insurance_providers_list">
                <ul id="insurance_providers_ul">
                    @foreach ($insurance_providers as $item)
                    <li class="insurance_providers_li" style="width: 200px; display: inline-block; float: left;">
                        <input type="checkbox" class="insurance_providers" name="insurance_providers[]"
                            value="{{ $item->name }}">&nbsp;{{ $item->name }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="insurance_policies">Insurance Policies</label>
            <div class="row">
                <div class="col-md-12" id="insurance_policies_list">
                    <ul id="insurance_policies_ul">
                        @foreach ($insurance_policies as $item)
                        <li class="insurance_policies_li" style="width: 500px; display: inline-block; float: left;">
                            <input type="checkbox" class="insurance_policies" name="insurance_policies[]"
                                value="{{ $item->name }}">&nbsp;{{ $item->name }}
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Phone Providers</label>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="home_phone_provider">Home Phone</label>
                        <select name="home_phone_provider" class="form-control">
                            <option value="">Select</option>
                            @foreach ($phone_providers as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <input type="text" class="form-control" name="other_home_phone_provider"
                            id="other_home_phone_provider" placeholder="Please specify" style="display:none;" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="mobile1_provider">Mobile 1</label>
                        <select name="mobile1_provider" class="form-control" id="mobile1_provider">
                            <option value="">Select</option>
                            @foreach($mobile_providers as $item)
                            <option value="{{ $item->name }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" id="mobile1_name" style="display:none;">
                        <label class="control-label" for="mobile1_provider">Provider's Name</label>
                        <input type="text" class="form-control" name="mobile1_provider_other" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="mobile1_type">Mobile 1 Type</label>
                        <select name="mobile1_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Pre-paid">Pre-paid</option>
                            <option value="Post-paid/Contract">Post-paid/Contract</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="work_phone_provider">Work Phone</label>
                        <select name="work_phone_provider" class="form-control">
                            <option value="">Select</option>
                            @foreach ($phone_providers as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <input type="text" class="form-control" name="other_work_phone_provider"
                            id="other_work_phone_provider" placeholder="Please specify" style="display:none;" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="mobile2_provider">Mobile 2</label>
                        <select name="mobile2_provider" class="form-control" id="mobile2_provider">
                            <option value="">Select</option>
                            @foreach($mobile_providers as $item)
                            <option value="{{ $item->name }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" id="mobile2_name" style="display:none;">
                        <label class="control-label" for="mobile1_provider">Provider's Name</label>
                        <input type="text" class="form-control" name="mobile2_provider_other" />
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="mobile2_type">Mobile 2 Type</label>
                        <select name="mobile2_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Pre-paid">Pre-paid</option>
                            <option value="Post-paid/Contract">Post-paid/Contract</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Tv Provides :</label><br />
            <input type="checkbox" name="tv_providers[]" value="Austar"> Austar &nbsp;
            <input type="checkbox" name="tv_providers[]" value="Foxtel"> Foxtel &nbsp;
            <input type="checkbox" name="tv_providers[]" value="Foxtel IQ"> Foxtel IQ &nbsp;
            <input type="checkbox" name="tv_providers[]" value="OptusVision"> OptusVision &nbsp;
            <input type="checkbox" name="tv_providers[]" value="Tivo"> Tivo &nbsp;
            <input type="checkbox" name="tv_providers[]" value="Netflix"> Netflix &nbsp;
            <input type="checkbox" name="tv_providers[]" value="Set top box"> Set top box &nbsp;
        </div>
    </div>
</div>

@push('scripts')
<script>
    $('#mobile1_provider').change(function(){
        $('[name=mobile1_provider_other]').val('');
        if($(this).val() == 'Other')
            $('#mobile1_name').show();
        else
            $('#mobile1_name').hide();
    });
    $('#mobile2_provider').change(function(){
        $('[name=mobile2_provider_other]').val('');
        if($(this).val() == 'Other')
            $('#mobile2_name').show();
        else
            $('#mobile2_name').hide();
    });
    $('[name=home_phone_provider]').change(function(){
        $('#other_home_phone_provider').val('');
        if($(this).val() == 21){
            $('#other_home_phone_provider').show();
        } else {
            $('#other_home_phone_provider').hide();
        }
    });
    $('[name=work_phone_provider]').change(function(){
        $('#other_work_phone_provider').val('');
        if($(this).val() == 21){
            $('#other_work_phone_provider').show();
        } else {
            $('#other_work_phone_provider').hide();
        }
    });
</script>
@endpush