<div class="row mb-4">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="health_problems">Health problems</label>
            <div class="health_pro_list">
                <ul class="health_problem_list">
                    @foreach ($health_problems as $item)
                    <li class="health_problem_li" style="width: 300px; display: inline-block; float: left;">
                        <input type="checkbox" class="health_problems" name="health_problems[]"
                            id="health_problem_{{ $item->id }}" value="{{ $item->name }}">&nbsp;{{ $item->name }}
                        @if($item->name == 'Other')
                        <div class="form-group other_health_problem_wrap" style="display: none;">
                            <input type="text" class="form-control" name="other_health_problem"
                                id="other_health_problem" />
                        </div>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="dietary_requirements">Dietary Requirments</label>
            <div class="dietaryname">
                <input type="checkbox" class="check_dietary" name="dietary_requirements[]" value="Dairy Free"> Dairy
                Free
                &nbsp;
                <input type="checkbox" class="check_dietary" name="dietary_requirements[]" value="Gluten Free"> Gluten
                Free &nbsp;
                <input type="checkbox" class="check_dietary" name="dietary_requirements[]" value="Lactose Free"> Lactose
                Free &nbsp;
                <input type="checkbox" class="check_dietary" name="dietary_requirements[]" value="Vegan">
                Vegan &nbsp;
                <input type="checkbox" class="check_dietary" name="dietary_requirements[]" value="Vegetarian">
                Vegetarian
                &nbsp;
                <input type="checkbox" class="check_dietary1" name="dietary_requirements[]" value="None">
                None &nbsp;
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="health_funds">Health Funds</label>
            <div id="health_problem_list">
                <ul id="health_problem_ul">
                    @foreach ($health_funds as $item)
                    <li style="width: 300px; display: inline-block; float: left;">
                        <input type="checkbox" class="health_funds" name="health_funds[]"
                            id="health_funds_{{ $item->id }}" value="{{ $item->id }}">
                        &nbsp; {{ $item->name }}
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-3 offset-md-3">
        <div class="form-group other_health_fund_wrap" style="display:none;">
            <input type="text" name="other_health_fund" id="other_health_fund" class="form-control"
                placeholder="Please Specify" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="cigarette_brands">Cigratte Brands</label>
            <div>
                <input class="chs" type="checkbox" checked="checked" id="Smoke_None" name="cigarette_brands"
                    value="Doesn't Smoke">
                Doesn't Smoke &nbsp;
                <input class="chs" type="checkbox" id="Smoke_Regular" name="cigarette_brands" value="Regular Smoker">
                Regular
                Smoker &nbsp;
                <input class="chs" type="checkbox" id="Smoke_Social" name="cigarette_brands" value="Social Smoker">
                Social Smoker &nbsp;
                <input class="chs" type="checkbox" id="Smoke_Quit" name="cigarette_brands" value="Quit Smoking">
                Quit Smoking &nbsp;
                <input class="chs" type="checkbox" id="Smoke_Vaping" name="cigarette_brands"
                    value="E-cigarettes or Vaping">
                E-cigarettes or Vaping &nbsp;
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="partner_cigarette_brands">Partner Cigratte Brands</label>
            <div>
                <input class="chs" type="checkbox" checked="checked" id="Smoke_None1" name="partner_cigarette_brands"
                    value="Doesn't Smoke"> Doesn't Smoke &nbsp;
                <input class="chs" type="checkbox" id="Smoke_Regular1" name="partner_cigarette_brands"
                    value="Regular Smoker">
                Regular Smoker &nbsp;
                <input class="chs" type="checkbox" id="Smoke_Social1" name="partner_cigarette_brands"
                    value="Social Smoker">
                Social
                Smoker &nbsp;
                <input class="chs" type="checkbox" id="Smoke_Quit1" name="partner_cigarette_brands"
                    value="Quit Smoking">
                Quit
                Smoking &nbsp;
                <input class="chs" type="checkbox" id="Smoke_Vaping1" name="partner_cigarette_brands"
                    value="E-cigarettes or Vaping"> E-cigarettes or Vaping &nbsp;
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="organ_donate">Are you in favour of organ donation?</label>
            <div>
                <input class="chs" type="checkbox" id="organ_yes" name="organ_donate" value="1">Yes
                &nbsp;
                <input class="chs" type="checkbox" id="organ_no" name="organ_donate" value="0">No &nbsp;
            </div>
        </div>
    </div>
</div>

<div class="row" hidden id="organ_wrap">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="donated_organ">Which organs have you registered for
                donation?</label>
            <select id="donated_organ" name="donated_organ" class="form-control">
                <option value="">Select</option>
                <option value="eyes">Eyes</option>
                <option value="liver">Liver</option>
                <option value="heart">Heart</option>
                <option value="lungs">Lungs</option>
                <option value="other">Other</option>
                <option value="none">None</option>
            </select>
        </div>
    </div>
</div>

<div class="row" id="cig_brands_wrap" style="display:none;">
    <div class="col-md-12">
        <div class="form-group">
            <label for="quite_smoking_methods" class="control-label">Quit Smoking Methods</label>
            <div>
                <input type="checkbox" name="quite_smoking_methods[]" value="Nicotine Replacement Technology (NRT)">
                Nicotine Replacement Technology (NRT) &nbsp;
                <input type="checkbox" name="quite_smoking_methods[]" value="Therapy"> Therapy &nbsp;
            </div>
        </div>
        <div class="form-group">
            <label for="nicotine_replacement" class="control-label">Nicotine Replacement Technologies
                Used</label>
            <div>
                <input type="checkbox" name="nicotine_replacement[]" value="Combination therapy"> Combination
                therapy &nbsp;
                <input type="checkbox" name="nicotine_replacement[]" value="Gum"> Gum &nbsp;
                <input type="checkbox" name="nicotine_replacement[]" value="Inhaler"> Inhaler &nbsp;
                <input type="checkbox" name="nicotine_replacement[]" value="Lozenges"> Lozenges &nbsp;
                <input type="checkbox" name="nicotine_replacement[]" value="Mouth Spray"> Mouth Spray &nbsp;
                <input type="checkbox" name="nicotine_replacement[]" value="Patches"> Patches &nbsp;
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group" id="cig_my_brands_wrap" style="display:none;">
            <label for="brands" class="control-label">Your Brands</label>
            <div>
                <ul style="list-style:none;">
                    @foreach($brands as $brand)
                    <li>
                        <input type="checkbox" id="check_brand-{{ $brand->id }}" name="brands[]"
                            value="{{ $brand->name }}">&nbsp;{{ $brand->name }}
                    </li>
                    @endforeach
                    <li style="display:none;" id="other_cig_brands_wrap">
                        <input type="text" name="orther_brands" placeholder="Others Brands Name" class="form-control">
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group" id="cig_partner_brands_wrap" style="display:none;">
            <label for="" class="control-label">Partner's Brands</label>
            <div>
                <ul style="list-style:none;">
                    @foreach($brands as $brand)
                    <li>
                        <input type="checkbox" id="check_pbrand-{{ $brand->id }}" name="partner_brands[]"
                            value="{{ $brand->name }}">&nbsp;{{ $brand->name }}
                    </li>
                    @endforeach
                    <li style="display:none;" id="other_cigp_brands_wrap">
                        <input type="text" name="other_partner_brands" placeholder="Others Brands Name"
                            class="form-control" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $('input[name=organ_donar]').change(function(){
        var val = $(this).val();
        if(val == 'Yes')
            $('#organ_wrap').removeAttr('hidden');
        else
            $('#organ_wrap').prop('hidden', true);
    });
    $('input[name=cigarette_brands]').change(function(){
        if($(this).val() != "Doesn't Smoke"){
            if(!$('#cig_brands_wrap').is(':visible')) $('#cig_brands_wrap').show();
            if(!$('#cig_my_brands_wrap').is(':visible')) $('#cig_my_brands_wrap').show();
        } else {
            if($('#cig_brands_wrap').is(':visible') && $('input[name=cigarette_brands]:checked').val() == "Doesn't Smoke" ) $('#cig_brands_wrap').hide();
            if($('#cig_my_brands_wrap').is(':visible')) $('#cig_my_brands_wrap').hide();
        }
    });
    $('input[name=partner_cigarette_brands]').change(function(){
        if($(this).val() != "Doesn't Smoke"){
            if(!$('#cig_brands_wrap').is(':visible')) $('#cig_brands_wrap').show();
            if(!$('#cig_partner_brands_wrap').is(':visible')) $('#cig_partner_brands_wrap').show();
        } else {
            if($('#cig_brands_wrap').is(':visible') && $('input[name=partner_cigarette_brands]:checked').val() == "Doesn't Smoke") $('#cig_brands_wrap').hide();
            if($('#cig_partner_brands_wrap').is(':visible')) $('#cig_partner_brands_wrap').hide();
        }
    });
    $('input#check_brand-131').change(function(){
        $('[name=other_cig_brands]').val('');
        if($(this).is(':checked'))
            $('#other_cig_brands_wrap').show();
        else
            $('#other_cig_brands_wrap').hide();
    });
    $('input#check_pbrand-131').change(function(){
        $('[name=other_cigp_brands]').val('');
        if($(this).is(':checked'))
            $('#other_cigp_brands_wrap').show();
        else
            $('#other_cigp_brands_wrap').hide();
    });
    $('#health_funds_21').change(function(){
        $('#other_health_fund').val('');
        if($(this).is(':checked'))
            $('.other_health_fund_wrap').show();
        else
            $('.other_health_fund_wrap').hide();
    });
    $('#health_problem_36').change(function(){
        $('#other_health_problem').val('');
        if($(this).is(':checked'))
            $('.other_health_problem_wrap').show();
        else
            $('.other_health_problem_wrap').hide();
    });
</script>
@endpush