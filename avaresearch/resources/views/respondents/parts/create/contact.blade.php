<div class="row">
    <div class="col-md-12 text-center">
        <div class="form-group">
            <h3>I would like to participate in online surveys/forums as well for cash or gift pay vouchers?</h3>
            <div class="form-check">
                <input class="chs" type="checkbox" name="online_survey" value="1" /> Yes
                <input class="chs" type="checkbox" name="online_survey" value="0" /> No
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="hear_about" class="control-label">Where did you know about us? <sup class="text-danger">*</sup>
            </label>
            <select name="hear_about" id="hear_about" class="form-control" required>
                <option value="">Select</option>
                <option value="Facebook">Facebook</option>
                <option value="Internet Website">Internet Website</option>
                <option value="Magazine">Magazine</option>
                <option value="Newspaper">Newspaper</option>
                <!--<option value="Radio">Radio</option>-->
                <option value="Word of Mouth">Word of Mouth</option>
                <option value="Flyer">Flyer</option>
                <option value="Gum Tree">Gum Tree</option>
                <option value="Survey Monkey">Survey Monkey</option>
                <option value="Others" id="otherhow">Others</option>
            </select>
            <small id="hear_about_error" class="text-danger form_error"></small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group source_name_wrap" hidden>
            <label for="source_name" class="control-label">Facebook Name</label>
            <input type="text" class="form-control" name="source_name" />
            <small id="source_name_error" class="text-danger form_error"></small>
        </div>
        <div class="form-group source_email_wrap" hidden>
            <label for="source_email" class="control-label">Source Email/Website</label>
            <input type="text" class="form-control" name="source_email" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="first_name" class="control-label">First Name <sup class="text-danger">*</sup></label>
            <input type="text" class="form-control" name="first_name" required />
            <small id="first_name_error" class="text-danger form_error"></small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="last_name" class="control-label">Last Name <sup class="text-danger">*</sup></label>
            <input type="text" class="form-control" name="last_name" required />
            <small id="last_name_error" class="text-danger form_error"></small>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="dob" class="control-label">Date of Birth <sup class="text-danger">*</sup></label>
            {{-- <input type="text" class="form-control date" name="dob" required autocomplete="off" /> --}}
            <div class="input-group">
                <select name="dob_day" id="dob_day" class="form-control custom-select" required>
                    <option value="">Day</option>
                    @for($i=1;$i<=31;$i++)<option value="{{ $i }}">{{ $i }}</option>@endfor
                </select>
                <select name="dob_mon" id="dob_mon" class="form-control custom-select" required>
                    <option value="">Month</option>
                    @for($i=1;$i<=12;$i++)<option value="{{ sprintf("%02d", $i) }}">
                        {{ date("M", mktime(0, 0, 0, $i, 10)) }}</option>@endfor
                </select>
                <select name="dob_year" id="dob_year" class="form-control custom-select" required>
                    <option value="">Year</option>
                    @for($i=date('Y')-99;$i<=date('Y')-8;$i++)<option value="{{ $i }}">{{ $i }}
                        </option>@endfor
                </select>
            </div>
            <small id="dob_error" class="text-danger form_error"></small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="gender" class="control-label">Gender <sup class="text-danger">*</sup></label>
            <select name="gender" id="gender" class="form-control" required>
                <option value="">Select</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
            </select>
            <small id="gender_error" class="text-danger form_error"></small>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="state" class="control-label">State <sup class="text-danger">*</sup></label>
            <select name="state" id="state" class="form-control" required>
                <option value="">Select</option>
                @foreach ($states as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
            <small id="state_error" class="text-danger form_error"></small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="closest_large_city" class="control-label">Closest Large City</label>
            <input type="text" id="closest_large_city" name="closest_large_city" class="form-control" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="home_postcode" class="control-label">Home Postcode</label>
            <input type="text" class="form-control" name="home_postcode" id="home_postcode" />
            <small class="text-primary">Press TAB after entering the PostCode and choose your suburb in Home Suburb
                Field from the DropDown
                Menu</small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="home_postsuburb" class="control-label">Home Suburb <sup class="text-danger">*</sup></label>
            <div class="input-group">
                <select name="home_postsuburb" id="home_postsuburb" class="form-control" required>
                    <option value="">Select</option>
                </select>

                <select name="home_subdirection" id="home_subdirection" class="form-control custom-select">
                    <option value="">Select Direction</option>
                    <option value="Inner city">Inner city</option>
                    <option value="Outer City">Outer City</option>
                    <option value="West">West</option>
                    <option value="North">North</option>
                    <option value="East">East</option>
                    <option value="South">South</option>
                </select>
            </div>
            <small class="text-primary clearfix">Now please choose your Home Suburb from the dropdown menu</small>
            <small id="home_postsuburb_error" class="text-danger form_error clearfix"></small>
            <small class="text-danger home-suburb-not-found"></small>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="home_postcode" class="control-label">Work Postcode</label>
            <input type="text" class="form-control" name="work_postcode" id="work_postcode" />
            <small class="text-primary">Press TAB after entering the PostCode and choose your suburb in Work Suburb
                Field from the DropDown Menu</small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="work_postsuburb" class="control-label">Current Work Suburb</label>
            <div class="input-group">
                <select name="work_postsuburb" id="work_postsuburb" class="form-control">
                    <option value="">Select</option>
                </select>

                <select name="work_subdirection" id="work_subdirection" class="form-control custom-select">
                    <option value="">Select Direction</option>
                    <option value="West">West</option>
                    <option value="North">North</option>
                    <option value="East">East</option>
                    <option value="South">South</option>
                </select>
            </div>

            <small class="text-primary clearfix">Now please choose your Work Suburb from the dropdown menu</small>
            <small class="text-danger work-suburb-not-found"></small>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="mobile_phone" class="control-label">Mobile Phone</label>
            <input type="text" class="form-control" name="mobile_phone" />
            <small class="text-primary clearfix">if you do not provide your mobile no. you will not get any sms
                notification of
                jobs from us</small>
            <small id="mobile_phone_error" class="text-danger form_error"></small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="mobile_phone2" class="control-label">Mobile Phone 2</label>
            <input type="text" class="form-control" name="mobile_phone2" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="home_phone" class="control-label">Home Landline (If available)</label>
            <input type="text" class="form-control" name="home_phone" />
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="work_phone" class="control-label">Work Landline (If available)</label>
            <input type="text" class="form-control" name="work_phone" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="email" class="control-label">Email <sup class="text-danger">*</sup></label>
            <input type="email" class="form-control" name="email" id="email" required />
            <small id="email_error" class="text-danger form_error"></small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="additional_email" class="control-label">Additional Email</label>
            <input type="email" class="form-control" name="additional_email" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="password" class="control-label">Password <sup class="text-danger">*</sup></label>
            <input type="password" class="form-control" name="password" required />
            <small class="text-primary">
                Your password must be between 5 and 10 characters long
            </small><br />
            <small class="text-muted clearfix">Only for updating your details</small>
            <small id="password_error" class="text-danger form_error"></small>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="ethnic_background" class="control-label">Ethnic Background</label>
            <select name="ethnic_background" class="form-control">
                <option value="">Select</option>
                @foreach ($ethnic_backgrounds as $item)
                <option value="{{ $item->name }}">{{ $item->name }}</option>
                @endforeach
            </select>
            <small id="ethnic_background_error" class="text-danger form_error"></small>
        </div>
    </div>
</div>

<div class="alert alert-danger validation-error" style="display: none;">
    <b>Validation Error: Please check the above fields to know the errors</b>
</div>

@push('scripts')
<script>
    $('#hear_about').change(function(){
        var data = $(this).val();
        $('.source_name_wrap, .source_email_wrap').prop('hidden', true);
        if(data != ''){
            $('.source_name_wrap label').text(data+' Name');
            $('.source_name_wrap').removeAttr('hidden');
            if(data == 'Others'){
                $('.source_email_wrap').removeAttr('hidden');
            }
        }
    });
    $('#home_postcode, #state').change(function(){
        var postcode = $('#home_postcode').val();
        var state_id = $('#state').val();
        var html = `<option value="">Select</option>`;
        $('.home-suburb-not-found').html('');
        if(postcode != ''  && state_id != ''){
            $.get(`{{ route('respondents.index') }}/suburbs/${postcode}/${state_id}`, function(data){
                if(data.length){
                    for (var i = 0; i < data.length; ++i) {
                        html += `<option value="${data[i].suburb}">${data[i].suburb}</option>`;
                    }
                } else {
                    $('.home-suburb-not-found').html(`No Suburb for postcode ${postcode}`);
                }
                $('#home_postsuburb').html(html);
            });
        }
    });
    $('#work_postcode, #state').change(function(){
        var postcode = $('#work_postcode').val();
        var state_id = $('#state').val();
        var html = `<option value="">Select</option>`;
        $('.work-suburb-not-found').html('');
        if(postcode != ''  && state_id != ''){
            $.get(`{{ route('respondents.index') }}/suburbs/${postcode}/${state_id}`, function(data){
                if(data.length){
                    for (var i = 0; i < data.length; ++i) {
                        html += `<option value="${data[i].suburb}">${data[i].suburb}</option>`;
                    }
                } else {
                    $('.work-suburb-not-found').html(`No Suburb for postcode ${postcode}`);
                }
                $('#work_postsuburb').html(html);
            });
        }
    });
    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
           return false;
        }else{
           return true;
        }
      }
    $('[name=first_name], [name=dob]').change(function(){
        $('#first_name_error').html('');
        $('.validation-error').hide();
        var first_name = $('[name=first_name]').val();
        var dob = $('[name=dob]').val();
        if(first_name != '' && dob != ''){
            $.ajax({
                url: "{{ route('respondentNameDOBCheck') }}",
                type: 'POST',
                data: { _token: "{{ csrf_token() }}", first_name: first_name, dob: dob },
                dataType: 'json',
                beforeSend: function(){
                    $('.sw-btn-next').prop('disabled', true);
                },
                success: function(res){
                    if(res.exists==true){
                        $('.validation-error').show();
                        $('#first_name_error').html('Respondent is already registered with Simliar First Name and Date of Birth');
                    } else {
                        $('.sw-btn-next').removeAttr('disabled');
                    }
                },
                error: function(err){
                    console.log(err);
                }
            });
        }
    });
    $('[name=mobile_phone]').change(function(){
        $('#mobile_phone_error').html('');
        $('.validation-error').hide();
        var mobile_phone = $(this).val();
        if(mobile_phone != ''){
            $.ajax({
                url: "{{ route('respondentMobileCheck') }}",
                type: 'POST',
                data: { _token: "{{ csrf_token() }}", mobile_phone: mobile_phone },
                dataType: 'json',
                beforeSend: function(){
                    $('.sw-btn-next').prop('disabled', true);
                },
                success: function(res){
                    if(res.exists==true){
                        $('.validation-error').show();
                        $('#mobile_phone_error').html('Mobile Phone is already registered!');
                    } else {
                        $('.sw-btn-next').removeAttr('disabled');
                    }
                },
                error: function(err){
                    console.log(err);
                }
            });
        }
    });
    $('#email').change(function(){
        $('#email_error').html('');
        $('.validation-error').hide();
        var email = $(this).val();
        if(email != ''){
            if(!IsEmail(email)) $('#email_error').html('Email is invalid');
            else {
                $.ajax({
                    url: "{{ route('respondentEmailCheck') }}",
                    type: 'POST',
                    data: { _token: "{{ csrf_token() }}", email: email },
                    dataType: 'json',
                    beforeSend: function(){
                        $('.sw-btn-next').prop('disabled', true);
                    },
                    success: function(res){
                        if(res.exists==true){
                            $('.validation-error').show();
                            $('#email_error').html('Email is already registered!');
                        } else {
                            $('.sw-btn-next').removeAttr('disabled');
                        }
                    },
                    error: function(err){
                        console.log(err);
                    }
                });
            }
        }
    });
    $('.respondent-stepper').on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
        if(stepNumber == 0){
            $('#contact .form_error').html('');
            $('.validation-error').hide();
            var hear_about = $('[name=hear_about]').val();
            var source_name = $('[name=source_name]').val();
            var first_name = $('[name=first_name]').val();
            var last_name = $('[name=last_name]').val();
            //var dob = $('[name=dob]').val();

            var dob_day = $('[name=dob_day]').val();
            var dob_mon = $('[name=dob_mon]').val();
            var dob_year = $('[name=dob_year]').val();

            var gender = $('[name=gender]').val();
            var state = $('[name=state]').val();
            var home_postsuburb = $('[name=home_postsuburb]').val();            
            var email = $('[name=email]').val();
            var password = $('[name=password]').val();
            var ethnic_background = $('[name=ethnic_background]').val();
            var flag = 0;

            if(hear_about==''){
                $('#hear_about_error').html('This field is required!');
                flag++;
            } else {
                if(source_name == ''){
                    $('#source_name_error').html('This field is required!');
                    flag++;
                }
            }

            if(first_name==''){
                $('#first_name_error').html('This field is required!');
                flag++;
            }

            if(last_name==''){
                $('#last_name_error').html('This field is required!');
                flag++;
            }

            // if(dob==''){
            //     $('#dob_error').html('This field is required!');
            //     flag++;
            // }

            if(dob_day == '' || dob_mon == '' || dob_year == ''){
                $('#dob_error').html('This field is required!');
                flag++;
            }

            if(gender==''){
                $('#gender_error').html('This field is required!');
                flag++;
            }

            if(state==''){
                $('#state_error').html('This field is required!');
                flag++;
            }

            if(home_postsuburb==''){
                $('#home_postsuburb_error').html('This field is required!');
                flag++;
            }
           
            if(email==''){
                $('#email_error').html('This field is required!');
                flag++;
            }

            if(password==''){
                $('#password_error').html('This field is required!');
                flag++;
            }
            // if(ethnic_background==''){
            //     $('#ethnic_background_error').html('This field is required!');
            //     flag++;
            // }
            if(flag) {
                $('.validation-error').show();
                return false;
            }
        }
    }); 
</script>
@endpush