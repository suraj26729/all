<div class="row mb-4">
  <div class="col-md-12">
    <div class="form-group">
      <label for="preferred_research_location" class="control-label">Preferred Research Locations</label>
      <div id="pref_location_list">
        <ul id="loc_ul">
          @foreach($preferred_locations as $key => $item)
          <li style="width: 300px; display: inline-block; float: left;">
            <input type="checkbox" class="res_location" name="preferred_research_location[]"
              value="{{ $item->id }}">&nbsp;{{ $item->name }}
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="availability_times" class="control-label">Availability</label>
      <div class="availname">
        <input type="checkbox" class="check_avail" name="availability_times[]" value="After Hours"> After Hours
        &nbsp;
        <input type="checkbox" class="check_avail" name="availability_times[]" value="Office Hours"> Office
        Hours &nbsp;
        <input type="checkbox" class="check_avail" name="availability_times[]" value="Weekdays">
        Weekdays &nbsp;
        <input type="checkbox" class="check_avail" name="availability_times[]" value="Weekends">
        Weekends &nbsp;
        <input type="checkbox" class="check_avail1" name="availability_times[]" value="Any Time"> Any Time
        &nbsp;
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="session_type" class="control-label">Session Type</label>
      <div>
        <input type="checkbox" name="session_type[]" value="Group Discussion"> Group Discussion
        &nbsp;
        <input type="checkbox" name="session_type[]" value="Face To Face Interview"> Face To
        Face Interview &nbsp;
        <input type="checkbox" name="session_type[]" value="Online Surveys"> Online Surveys
        &nbsp;
        <input type="checkbox" name="session_type[]" value="Taste Testing"> Taste Testing &nbsp;
        <input type="checkbox" name="session_type[]" value="Telephone Interview"> Telephone
        Interview &nbsp;
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="preferred_contact_method" class="control-label">Preferred Contact Method</label>
      <div>
        <input type="checkbox" name="preferred_contact_method[]" value="Email"> Email &nbsp;
        <input type="checkbox" name="preferred_contact_method[]" value="Mobile Phone"> Mobile Phone &nbsp;
        <input type="checkbox" name="preferred_contact_method[]" value="SMS/Text Message"> SMS/Text
        Message &nbsp;
        <input type="checkbox" name="preferred_contact_method[]" value="Home Phone"> Home Phone &nbsp;
        <input type="checkbox" name="preferred_contact_method[]" value="Work Phone"> Work Phone &nbsp;
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="additional_notes" class="control-label">Additional Notes</label>
      <textarea name="additional_notes" rows="5" class="form-control"></textarea>
    </div>
  </div>
</div>