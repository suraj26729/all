@extends('layouts.app')
@section('title', 'Create Respondent')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow p-0">
    <div class="animated fadeInUpShort go">
        @if(session('success'))
        <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
        @endif
        <form action="{{ route('respondents.store') }}" method="POST" id="new_respondent_form">
            @csrf
            <div class="respondent-stepper sw-main" data-options=''>
                <ul class="nav step-anchor">
                    <li>
                        <a href="#contact" class="text-center">
                            <i class="icon-phone2 s-24"></i><br />
                            Contact Details
                        </a>
                    </li>
                    <li>
                        <a href="#household" class="text-center">
                            <i class="icon-home s-24"></i><br />
                            Household Details
                        </a>
                    </li>
                    <li>
                        <a href="#occupation" class="text-center">
                            <i class="icon-briefcase2 s-24"></i><br />
                            Occupation Details
                        </a>
                    </li>
                    <li>
                        <a href="#health" class="text-center">
                            <i class="icon-local_hospital s-24"></i><br />
                            Health Details
                        </a>
                    </li>
                    <li>
                        <a href="#service" class="text-center">
                            <i class="icon-gear s-24"></i><br />
                            Services you use
                        </a>
                    </li>
                    <li>
                        <a href="#product" class="text-center">
                            <i class="icon-package s-24"></i><br />
                            Products you use
                        </a>
                    </li>
                    <li>
                        <a href="#summary" class="text-center">
                            <i class="icon-th-list s-24"></i><br />
                            Summary
                        </a>
                    </li>
                </ul>
                <div class="card no-b shadow">
                    <div id="contact" class="card-body p-3">
                        @include('respondents.parts.create.contact')
                    </div>
                    <div id="household" class="card-body p-3">
                        @include('respondents.parts.create.household')
                    </div>
                    <div id="occupation" class="card-body p-3">
                        @include('respondents.parts.create.occupation')
                    </div>
                    <div id="health" class="card-body p-3">
                        @include('respondents.parts.create.health')
                    </div>
                    <div id="service" class="card-body p-3">
                        @include('respondents.parts.create.services')
                    </div>
                    <div id="product" class="card-body p-3">
                        @include('respondents.parts.create.product')
                    </div>
                    <div id="summary" class="card-body p-3">
                        @include('respondents.parts.create.summary')
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script>
    $(document).on('change', '.chs', function(){
        var name = $(this).attr('name');
        $('input[name='+name+']').prop('checked', false);
        $(this).prop('checked', true);
    });
    $(document).ready(function(){
        $('.respondent-stepper').smartWizard({
            transitionEffect: "fade",
            showStepURLhash: false,
            anchorSettings: { enableAllAnchors: true },
            toolbarSettings: {
                toolbarPosition: 'bottom', // none, top, bottom, both
                toolbarButtonPosition: 'right', // left, right
                showNextButton: true, // show/hide a Next button
                showPreviousButton: true, // show/hide a Previous button
                toolbarExtraButtons: [
                    $('<button type="button"></button>').text('Submit')
                    .addClass('btn btn-success btn-finish')
                    .prop('hidden', true)
                    .on('click', function(){ 
                        //alert('Success button click');  
                        console.log($('#new_respondent_form').serialize());  

                        $.ajax({
                            url: "{{ route('respondents.store') }}",
                            type: 'POST',
                            data: $('#new_respondent_form').serialize(),
                            dataType: 'json',
                            beforeSend: function(){
                                $('.btn-finish').html('<i class="icon-spinner"></i>').prop('disabled', true);
                            },
                            success: function(res){
                                location.reload(true);
                            },
                            error: function(err){ 
                                console.log(err); 
                                alert(err.responseText); 
                            },
                            complete: function(){
                                $('.btn-finish').html('Submit').removeAttr('disabled');
                            }
                        })
                    })
                ]
            }, 
        }).on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            if(stepNumber == 6){
                $('.btn-finish').removeAttr('hidden');
                $('.sw-btn-next').prop('hidden', true);
            } else {
                $('.btn-finish').prop('hidden', true);
                $('.sw-btn-next').removeAttr('hidden');
            }
        });
    });
</script>
@endsection