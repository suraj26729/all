@extends('layouts.app')
@section('title', 'Deleted Respondents')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    @if(session('success'))
    <div class="alert alert-success">{{ session("success") }}</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="respondents_table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th >Email</th>    
            <th >Delete Reason</th>   
            <th>Deleted Date</th>
            <th>Deleted By</th>
          </tr>
          </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('footer')
<script>
  $(document).ready(function() {
        $("#respondents_table").DataTable({
            sDom: "Rlfrtip",
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: "{{ route('DTDeleteRespondents') }}",
            columns: [
                { data: "id", name: "respondents.id" },
                { data: "respondent_name", name: "respondents.first_name" },
                { data: "email", name: "respondents.email" },
                { data: "delete_reson", name: "respondents.delete_reson" },
                { data: "deleted_at", name: "respondents.deleted_at" },
                { data: "deleted_user_name", name: "respondents.deleted_by" },
                
            ],
            order: [[4, "desc"]]
        });
    });
  
</script>
@endsection