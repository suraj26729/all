@extends('layouts.app')
@section('title', 'Inactive Respondents')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    @if(session('success'))
    <div class="alert alert-success">{{ session("success") }}</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="respondents_table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th visible="false">Email</th>
            <th>Mobile Phone</th>
            <th>State</th>
            <th>Ethnic Background</th>
            <th>Created By</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="9">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection

@section('footer')
<script>
  $(document).ready(function() {
        $("#respondents_table").DataTable({
            sDom: "Rlfrtip",
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: "{{ route('DTInactiveRespondents') }}",
            columns: [
                { data: "id", name: "respondents.id" },
                { data: "respondent_name", name: "respondents.first_name" },
                { data: "first_name", name: "respondents.first_name", visible: false },
                { data: "last_name", name: "respondents.last_name", visible: false },
                { data: "email", name: "respondents.email", visible: false },
                { data: "mobile_phone", name: "respondent_details.mobile_phone",  },
                { data: "state", name: "master_states.name" },
                { data: "ethnic_background", name: "respondent_households.ethnic_background" },
                { data: "created_user_name", name: "respondents.created_by" },
                { data: "created_at", name: "respondents.created_at" },
                {
                data: "actions",
                name: "actions",
                orderable: false,
                searchable: false
                }
            ],
            order: [[0, "desc"]]
        });
    });
  $(document).on("click", ".delete_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");
    swal(
      {
        title: "Are you sure to delete",
        text: "Name: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('respondents.index') }}/" + id,
            type: "DELETE",
            data: { _token: "{{ csrf_token() }}" },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Deleted", "You deleted the respondent: " + name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
  $(document).on("click", ".activate_btn", function() {
    var id = $(this).data("id");
    var name = $(this).data("name");
    var row = $(this).closest("tr");
    swal(
      {
        title: "Are you sure to active",
        text: "Name: " + name,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-success",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: "{{ route('respondents.index') }}/inactive/activate/" + id,
            type: "PATCH",
            data: { _token: "{{ csrf_token() }}" },
            success: function(res) {
              if (res) {
                row.remove();
                swal("Activated", "You activated the respondent: " + name, "success");
              }
            },
            error: function(err) {
              console.log(err);
            }
          });
        }
      }
    );
  });
</script>
@endsection