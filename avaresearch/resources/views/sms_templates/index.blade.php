@extends('layouts.app')
@section('title', 'SMS Templates')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <!-- <h4 class="">Registered Staffs</h4> -->
    <a href="{{ route('sms-templates.create') }}" class="btn btn-primary btn-xs"><i class="icon-plus"></i> Add New</a>
    <hr />
    @if(session('success'))
    <div class="alert alert-success">{{ session("success") }}</div>
    @endif
    <div class="table-responsive">
      <table class="table table-bordered table-hover" id="list_table">
        <thead>
          <tr>
            <th>Serial No</th>
            <th visible="hide">Title</th>
            <th>Title</th>
            <th>Created By</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <!-- Data will be placed here -->
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
@section('footer')
<script>
  $(document).ready(function() {
      $("#list_table").DataTable({
        sDom: "Rlfrtip",
        processing: true,
        serverSide: true,
        pageLength: 10,
        ajax: "{{ route('DTSMSTemplates') }}",
        columns: [
          {
            "title": "Serial",
            render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            },
            width: '2%'
          },
          { data: 'title', name: 'title', visible: false },
          { data: 'template_name', name: 'title'},
          { data: 'created_user_name', name: 'created_by'},
          { data: 'created_at', name: 'created_at'},
          {
            data: "actions",
            name: "actions",
            orderable: false,
            searchable: false
          }
        ],
        order: [[0, "asc"]]
      });
    });
    $(document).on("click", ".delete_btn", function() {
      var id = $(this).data("id");
      var name = $(this).data("name");
      var row = $(this).closest("tr");
      swal(
        {
          title: "Are you sure to delete",
          text: "Name: " + name,
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          showLoaderOnConfirm: true
        },
        function(isConfirm) {
          if (isConfirm) {
            $.ajax({
              url: "{{ route('sms-templates.index') }}/" + id,
              type: "DELETE",
              data: { _token: "{{ csrf_token() }}" },
              success: function(res) {
                if (res) {
                  row.remove();
                  swal("Deleted", "You deleted the template: " + name, "success");
                }
              },
              error: function(err) {
                console.log(err);
              }
            });
          }
        }
      );
    });
</script>
@endsection