@extends('layouts.app')
@section('title', 'Create SMS Template')
@section('content')
<div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
  <div class="animated fadeInUpShort go">
    <!-- <h4 class="">Fill up the details</h4> -->
    <a href="{{ route('sms-templates.index') }}" class="btn btn-secondary btn-xs"><i class="icon-arrow_back"></i> Back
      to
      List</a>
    <hr />
    @if(session('success'))
    <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
      <ul class="mb-0">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <form class="form-horizontal" action="{{ route('sms-templates.store') }}" method="POST"
      enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="title" class="control-label">Title <sup class="mandatory">*</sup></label>
            <input class="form-control" id="title" type="text" name="title" value="{{ old('title') }}" required />
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label for="message" class="control-label">Message <sup class="mandatory">*</sup></label>
            <div class="clear-fix"></div>
            <blockquote class="text-primary">Maximum Number of characters for this text box is 152.</blockquote>
            <textarea name="message" id="message" rows="10" class="form-control" maxlength="152"
              required>{{ old('message') }}</textarea>
            <small>You have <b id="remain_char">144</b> characters remaining for your
              description...</small>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <button class="btn btn-primary"><i class="fa fa-save mr-2"></i>Submit</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('footer')
<script>
  var maxLen = 152;
  function char_count_message(){
    var Length = $("#message").val().length;
    var AmountLeft = maxLen - Length;
    $('#remain_char').html(AmountLeft);
    if(Length >= maxLen){
      if (event.which != 8) {
          return false;
      }
    }
  }
  $('#message').keyup(function(){
    char_count_message();
  });
</script>
@endsection