<div class="sticky">
  <div class="navbar navbar-expand d-flex justify-content-between bd-navbar white shadow">
    <div class="relative">
      <div class="d-flex">
        <div class="d-none d-md-block">
          <h1 class="nav-title">@yield('title')</h1>

        </div>

      </div>
      
    </div>

    <!-- Timer -->
    <div class="text-right">
      <div class="row">
                <div class="col-md-8 input-wrapper">
                    <div class="input">
                        <input type="hidden" id="num" class="form-control" min="0">
                        
                    </div>
                    <div class="buttons-wrapper">
                        <!--  <button class="btn" id="start-cronometer">Start Stopwatch</button>  -->
                    </div>
                </div>
                <div id="timer" class="col-12">
                  <div class="clock-wrapper">
                      <span class="hours">00</span>
                      <span class="dots">:</span>
                      <span class="minutes">00</span>
                      <span class="dots">:</span>
                      <span class="seconds">00</span>
                     <!--  <button class="btn" id="reset-timer">Reset Timer</button> -->
                  </div>
                </div>
                <div class="buttons-wrapper">
                  <!-- <button class="btn" id="stop-timer">Stop Timer</button> -->
                  
                </div>
            </div>
      <!-- <label id="minutes">00</label>:<label id="seconds">00</label> -->
    </div> 
    <!--Top Menu Start -->
   

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account-->
        <li class="dropdown custom-dropdown user user-menu ">
          <a href="#" class="nav-link" data-toggle="dropdown">
            <!-- <img
              src="{{ asset('public/assets/img/dummy/u1.png') }}"
              class="user-image"
              alt="User Image"
            />
             -->
            <figure class="avatar">
              <img src="{{ asset('public/assets/img/dummy/u1.png') }}" alt="" />
              <span class="avatar-badge online"></span>
            </figure>
            <!-- <i class="icon-more_vert "></i> -->
          </a>
          <div class="dropdown-menu p-4 dropdown-menu-right">
            <div class="row box justify-content-between my-4">
              <div class="col">
                <a href="{{ route('edit-profile') }}">
                  <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                  <div class="pt-1">Profile</div>
                </a>
              </div>
              <div class="col">
                @if(auth()->user()->type == 'staff' && session()->has('timetracking_id'))
                <a href="#" lass="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter"> 
                  <i class="icon-perm_data_setting indigo lighten-2 avatar r-5"></i>
                  <div class="pt-1">Logout</div>
                  
                </a>
                @else
                <a href="{{ route('logout') }}">
                  <i class="icon-perm_data_setting indigo lighten-2 avatar r-5"></i>
                  <div class="pt-1">Logout</div>
                </a>
                @endif
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Project Job Alert !!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Please Stop the Job Timer.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="{{ route('endtimelogout') }}"><button type="button" class="btn btn-danger">End Job/Logout</button></a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);


  })();

</script>