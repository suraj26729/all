<div class="sidebar">
  <div class="d-flex hv-100 align-items-stretch">
    <div class="blue text-white">
      <div class="nav mt-2 pt-5 flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

        <a class="nav-link" href="{{ route('home') }}" title="Home"><i class="icon-home"></i></a>
        @if(auth('web')->check() && auth('web')->user()->type != 'client')
        <a class="nav-link" href="{{ route('calendar.index') }}" title="Calendar"><i class="icon-calendar"></i></a>
        <a class="nav-link" href="{{ route('staff-qualified-report.index') }}" title="Qulified Report"><i
            class="icon-assignment_turned_in"></i></a>
        <a class="nav-link" href="{{ route('clients.index') }}" title="Clients"><i class="icon-tie"></i></a>
        <a class="nav-link" href="{{ route('projects.index') }}" title="Projects"><i class="icon-briefcase"></i></a>
        <a class="nav-link" href="{{ route('projects.create') }}" title="Create Project"><i class="icon-add"></i></a>
        <a class="nav-link" href="{{ route('respondents.index') }}" title="Respondents"><i
            class="icon-graduation-cap"></i></a>
        <!-- <a class="nav-link blink skin_handle" href="#"
          ><i class="icon-lightbulb_outline"></i
        ></a> -->
        @if(auth('web')->check() && auth('web')->user()->type == 'admin')
        <a class="nav-link" href="{{ route('staffs.index') }}" title="Staffs"><i class="icon-group"></i></a>
        @endif
        <a class="nav-link" href="{{ route('email-templates.index') }}" title="Email Templates"><i
            class="icon-mail-envelope-open"></i></a>
        <a class="nav-link" href="{{ route('sms-templates.index') }}" title="SMS Templates"><i
            class="icon-textsms"></i></a>
        @else
        <a class="nav-link" href="{{ route('projects.index') }}" title="Projects"><i class="icon-briefcase"></i></a>
        @endif

        @if(auth('web')->check() && auth('web')->user()->type == 'admin')
        <a class="nav-link" href="{{ route('leads.index') }}" title="Leads"><i class="icon-compass"></i></a>
        @endif

        <a href="{{ route('edit-profile') }}">
          <figure class="avatar">
            <img src="{{ asset('public/assets/img/dummy/u1.png') }}" alt="" />
            <span class="avatar-badge online"></span>
          </figure>
        </a>
        {{-- <a class="nav-link blink skin_handle" href="#"><i class="icon-lightbulb_outline"></i></a> --}}
      </div>
    </div>
    <div class="tab-content flex-grow-1" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
        <div class="relative brand-wrapper sticky b-b">
          <div class="d-flex justify-content-between align-items-center p-3">
            <div class="text-xs-center">
              <span class="font-weight-lighter s-18">Menu</span>
            </div>
            <!-- <div class="badge badge-danger r-0">New Panel</div> -->
          </div>
        </div>
        <ul class="sidebar-menu">
          <li class="treeview">
            <a href="{{ route('home') }}">
              <i class="icon icon-home s-24"></i>
              <span>Dashboard</span>
            </a>
          </li>
          @if(auth('web')->check() && auth('web')->user()->type != 'client')
          <li class="treeview">
            <a href="{{ route('calendar.index') }}">
              <i class="icon icon-calendar s-24"></i>
              <span>Calendar</span>
            </a>
          </li>
          <li class="treeview">
            <a href="{{ route('staff-qualified-report.index') }}">
              <i class="icon icon-assignment_turned_in s-24"></i>
              <span>Staff Qualified Report</span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="icon icon icon-tie s-24"></i>
              <span>Clients</span>
              <i class=" icon-angle-left  pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ route('clients.create') }}"><i class="icon icon-add"></i>Add New
                </a>
              </li>
              <li>
                <a href="{{ route('clients.index') }}"><i class="icon icon-circle-o"></i>All Clients</a>
              </li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#"><i class="icon icon-graduation-cap s-24"></i>Respondents<i
                class=" icon-angle-left  pull-right"></i></a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ route('respondents.create') }}"><i class="icon icon-add"></i>Add New</a>
              </li>
              <li>
                <a href="{{ route('respondents.index') }}"><i class="icon icon-circle-o"></i>All Respondents</a>
              </li>
              {{-- <li>
                <a href="#"><i class="icon icon-user"></i>Search Respondents
                </a>
              </li>
              <li>
                <a href="#"><i class="icon icon-circle-o"></i>Global Query</a>
              </li> --}}
              <li>
                <a href="{{ route('listInactive') }}"><i class="icon icon-circle-o"></i>Inactive Respondents</a>
              </li>
              <li>
                <a href="{{ route('duplicate') }}"><i class="icon icon-circle-o"></i>Duplicate Respondents</a>
              </li>
              <li>
                <a href="{{ route('listDeleted') }}"><i class="icon icon-circle-o"></i>Deleted Respondents</a>
              </li>
            </ul>
          </li>
          @if(auth('web')->check() && auth('web')->user()->type == 'admin')
          <li class="treeview">
            <a href="#">
              <i class="icon icon icon-group s-24"></i>
              <span>Staffs</span>
              <i class=" icon-angle-left  pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ url('/staffs/create') }}"><i class="icon icon-add"></i>Add New
                </a>
              </li>
              <li>
                <a href="{{ url('/staffs') }}"><i class="icon icon-circle-o"></i>All Staffs</a>
              </li>
            </ul>
          </li>
          @endif
          <li class="treeview">
            <a href="#">
              <i class="icon icon icon-briefcase s-24"></i>
              <span>Projects</span>
              <i class=" icon-angle-left  pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ route('projects.create') }}"><i class="icon icon-add"></i>Add New </a>
              </li>
              <li>
                <a href="{{ route('projects.index') }}"><i class="icon icon-circle-o"></i>All Projects</a>
              </li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="icon icon icon-mail-envelope-open s-24"></i>
              <span>Email Templates</span>
              <i class=" icon-angle-left  pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ route('email-templates.create') }}"><i class="icon icon-add"></i>Add New </a>
              </li>
              <li>
                <a href="{{ route('email-templates.index') }}"><i class="icon icon-circle-o"></i>All Templates</a>
              </li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="icon icon icon-textsms s-24"></i>
              <span>SMS Templates</span>
              <i class=" icon-angle-left  pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ route('sms-templates.create') }}"><i class="icon icon-add"></i>Add New </a>
              </li>
              <li>
                <a href="{{ route('sms-templates.index') }}"><i class="icon icon-circle-o"></i>All Templates</a>
              </li>
            </ul>
          </li>
          @else
          <li class="treeview">
            <a href="#">
              <i class="icon icon icon-briefcase s-24"></i>
              <span>Projects</span>
              <i class=" icon-angle-left  pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ route('projects.create') }}"><i class="icon icon-add"></i>Add New </a>
              </li>
              <li>
                <a href="{{ route('projects.index') }}"><i class="icon icon-circle-o"></i>All Projects</a>
              </li>
            </ul>
          </li>
          @endif
           @if(auth('web')->check() && auth('web')->user()->type == 'admin')
          <li class="treeview">
            <a href="{{ route('leads.index') }}">
              <i class="icon icon-compass s-24"></i>
              <span>Leads</span>
            </a>
          </li>
          @endif
        </ul>
      </div>
    </div>
  </div>
</div>