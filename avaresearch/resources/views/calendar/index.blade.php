@extends('layouts.app')
@section('title', 'Calendar')
@section('content')
<div class="animatedParent animateOnce relative">
    <div class="animated fadeInUpShort go">
        <div class="d-flex row">
            <div class="col-md-3">
                <div class="card shadow">
                    <div class="card-body">
                        <div class="card-title">Date:</div>
                        <div class="form-group">
                            <input id="cal_date" type="text" class="date-time-picker form-control form-control-lg" />
                        </div>
                        <div class="form-group mb-0 text-center loading-spinner" style="display:none;">
                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <h3 class="text-center">Date: <span class="cal_date">{{ date('d-m-Y') }}</span></h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover mb-0" id="calendar_table">
                                <thead>
                                    <tr>
                                        <th visible="false">ID</th>
                                        <th>Job Name</th>
                                        <th visible="false">Job Name</th>
                                        <th>Group Name</th>
                                        <th>Location</th>
                                        <th>Incentive</th>
                                        <th>Remarks</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Data will be placed here -->
                                    <tr>
                                        <td colspan="8">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script>
    $(document).ready(function() {
        var cal_date = '{{ date("Y-m-d H:i:s") }}';
        var calendarTable = $("#calendar_table").DataTable({
            sDom: "Rlfrtip",
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "{{ route('DTCalendar') }}",
                beforeSend: function(){ $('.loading-spinner').show() },
                data: function (d) {
                    d.date = cal_date
                },
                complete: function(){ $('.loading-spinner').hide() }
            },
            columns: [
                { data: 'id', name: 'id', visible: false },
                { data: "job_name", name: "projects.job_name", visible: false },
                { data: "project_name", name: "projects.job_name", searchable: false },
                { data: "name", name: "project_groups.name" },
                { data: "mlocation", name: "master_states.name" },
                { data: "incentive", name: "incentive" },
                { data: "other", name: "other" },
                {
                    data: "actions",
                    name: "actions",
                    orderable: false,
                    searchable: false
                }
            ],
            order: [[2, "asc"]],
            language: {
                emptyTable: "No Groups Available in this Date"
            }
        });
        $('#cal_date').datetimepicker({
            inline:true,
            format: "Y-m-d H:i:s",
            timepicker:false,
            onShow:function(ct){
                alert(ct);
            },
            onChangeDateTime:function(dp,$input){
                cal_date = $input.val();
                $('span.cal_date').html(moment(cal_date).format('DD-MM-YYYY'));
                calendarTable.draw();
            }
        });
    });
</script>
@endsection