@extends('layouts.app')
@section('title', 'Calendar')
@section('content')
<div class="animatedParent animateOnce relative">
    <div class="animated fadeInUpShort go">
        <div class="d-flex row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">Date:</div>
                        <div class="form-group">
                            <input type="text" class="date-time-picker form-control form-control-lg"
                                data-options='{"inline":true, "format":"d.m.Y H:i", "timepicker":false}' />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover" id="clients_table">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Contact No</th>
                                        <th>Address</th>
                                        <th>Created By</th>
                                        <th>Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Data will be placed here -->
                                    <tr>
                                        <td colspan="6">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection