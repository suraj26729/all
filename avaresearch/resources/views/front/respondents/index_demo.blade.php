@extends('layouts.respondent')
@section('title', 'Respondents')
@section('content')
<style>
    sup {
        color: #FF0000;
    }
</style>
<div class="container-fluid relative animatedParent animateOnce my-3">
    <div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
        <div class="animated fadeInUpShort go">
            <h4 class="text-center">Register with Ava Research DEMO</h4>
            @if(session('success'))
            <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
            @endif @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif @if (session('errors'))
            {{ session('errors')->first('message') }}
            @endif
            <form class="form-horizontal" action="{{ route('add-respondent') }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email" class="control-label">Email <sup>*</sup></label>
                            <input class="form-control" id="email" type="email" name="email" required
                                value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password" class="control-label">Password <sup>*</sup></label>
                            <input class="form-control" id="password" type="password" name="password" minlength="5"
                                maxlength="10" required>
                            <small class="text-primary">Password must between 5 to 10 chars</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group text-primary">
                            “Please visit our Facebook page to view our current jobs and click on the survey link to
                            enter. This will give you a better chance to participate in our busy period.”
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="online_survey" class="control-label">Would you like to participate in online
                                surveys as well for some cash or Gift Pay Vouchers (to purchase anything of your choice
                                from some big stores like Coles, Aldi and others)?</label>
                            <!-- <input class="form-control" id="online_survey" type="text" name="online_survey" required> -->
                            <input type="radio" name="online_survey" value="1"
                                {{ old('online_survey')==1 ? 'checked' :'' }}>Yes
                            <input type="radio" name="online_survey" value="0"
                                {{ old('online_survey')!=1 ? 'checked' :'' }}>No
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="first_name" class="control-label">First Name <sup>*</sup></label>
                            <input class="form-control" id="first_name" type="text" name="first_name" required
                                value="{{ old('first_name') }}" maxlength="255">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="last_name" class="control-label">Last Name <sup>*</sup></label>
                            <input class="form-control" id="last_name" type="text" name="last_name" required
                                value="{{ old('last_name') }}" maxlength="255">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="hear_about" class="control-label">Where did you hear about us?
                                <sup>*</sup></label>

                            <select class="form-control custom-select" id="hear_about" type="text" name="hear_about"
                                required>
                                <option value="">Select</option>
                                <option value="Facebook" {{ old('hear_about')=='Facebook' ? 'selected' : '' }}>Facebook
                                </option>
                                <option value="Internet Website"
                                    {{ old('hear_about')=='Internet Website' ? 'selected' : '' }}>Internet Website
                                </option>
                                <option value="Magazine" {{ old('hear_about')=='Magazine' ? 'selected' : '' }}>Magazine
                                </option>
                                <option value="Newspaper" {{ old('hear_about')=='Newspaper' ? 'selected' : '' }}>
                                    Newspaper</option>
                                <option value="Word of Mouth"
                                    {{ old('hear_about')=='Word of Mouth' ? 'selected' : '' }}>Word of Mouth</option>
                                <option value="Flyer" {{ old('hear_about')=='Flyer' ? 'selected' : '' }}>Flyer</option>
                                <option value="Gum Tree" {{ old('hear_about')=='Gum Tree' ? 'selected' : '' }}>Gum Tree
                                </option>
                                <option value="Survey Monkey"
                                    {{ old('hear_about')=='Survey Monkey' ? 'selected' : '' }}>Survey Monkey</option>
                                <option value="Others" {{ old('hear_about')=='Others' ? 'selected' : '' }}>Others
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- <div class="form-group">
                            <label for="source_name" class="control-label"><span id="sourcename">Source </span> Name <sup>*</sup></label>
                            <input class="form-control" id="source_name" type="text" name="source_name" required>
                        </div> -->

                        <div class="form-group source_name_wrap" {{ empty(old('hear_about')) ? 'hidden' : '' }}>
                            <label for="source_name" class="control-label"><span id="sourcename">Source </span>
                                Name <sup>*</sup></label>
                            <input type="text" class="form-control" name="source_name" required
                                value="{{ old('source_name') }}" />
                        </div>
                        <div class="form-group source_email_wrap" {{ old('hear_about')!='Others' ? 'hidden' : '' }}>
                            <label for="source_email" class="control-label">Source Email/Website</label>
                            <input type="text" class="form-control" name="source_email"
                                value="{{ old('source_email') }}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dob" class="control-label">Date of Birth <sup>*</sup></label>
                            {{-- <input class="form-control date" id="dob" type="text" name="dob" autocomplete="off" required
                                value="{{ old('dob') }}"> --}}
                            <div class="input-group">
                                <select name="dob_day" id="dob_day" class="form-control custom-select" required>
                                    <option value="">Day</option>
                                    @for($i=1;$i<=31;$i++)<option value="{{ $i }}"
                                        {{ old('dob_day')==$i ? 'selected' : '' }}>{{ $i }}</option>@endfor
                                </select>
                                <select name="dob_mon" id="dob_mon" class="form-control custom-select" required>
                                    <option value="">Month</option>
                                    @for($i=1;$i<=12;$i++)<option value="{{ sprintf("%02d", $i) }}"
                                        {{ old('dob_mon')==sprintf("%02d", $i) ? 'selected' : '' }}>
                                        {{ date("M", mktime(0, 0, 0, $i, 10)) }}</option>@endfor
                                </select>
                                <select name="dob_year" id="dob_year" class="form-control custom-select" required>
                                    <option value="">Year</option>
                                    @for($i=date('Y')-99;$i<=date('Y')-8;$i++)<option value="{{ $i }}"
                                        {{ old('dob_year')==$i ? 'selected' : '' }}>{{ $i }}
                                        </option>@endfor
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="gender" class="control-label">Gender <sup>*</sup></label>
                            <select class="form-control custom-select" name="gender" id="gender" required>
                                <option value="">Select</option>
                                <option value="Male" {{ old('gender') == 'Male' ? 'selected' : '' }}>Male</option>
                                <option value="Female" {{ old('gender') == 'Female' ? 'selected' : '' }}>Female</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="state" class="control-label">State <sup>*</sup></label>
                            <select name="state" id="state" class="form-control custom-select" required>
                                <option value="">Select State</option>
                                @foreach($states as $state)
                                <option value="{{ $state->id }}" {{ old('state')==$state->id ? 'selected' : '' }}>
                                    {{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="closest_city" class="control-label">Closest Largest City <sup>*</sup></label>
                            
                            <select name="closest_city" id="closest_city" class="form-control custom-select" required>
                                    <option value="">Select City</option>
                            </select>
                            <small class="text-primary">please choose the closest largest city to where you live</small>
                            <input type="text" name="other_closest_city" value="" id="cityothers" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="original_country" class="control-label">Country of Origin <sup>*</sup></label>
                            <select class="form-control select2" name="original_country" id="original_country" required>
                                <option value="">Select</option>
                                @foreach($countries as $item)
                                <option value="{{ $item->id }}"
                                    {{ old('original_country')==$item->id ? 'selected' : '' }}>{{ $item->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="ethnic_background" class="control-label">Ethnic Background <sup>*</sup></label>
                            <select class="form-control select2" name="ethnic_background" id="ethnic_background" required>
                                <option value="">Select</option>
                                @foreach($ethnic_backgrounds as $item)
                                <option value="{{ $item->name }}"
                                    {{ old('ethnic_background')==$item->name ? 'selected' : '' }}>{{ $item->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="mobile_phone" class="control-label">Mobile</label>
                            <input class="form-control" id="mobile_phone" type="text" name="mobile_phone"
                                onkeypress="return isNumber(event);" value="{{ old('mobile_phone') }}">
                            <small class="text-primary">if you do not provide your mobile no. you will not get any sms
                                notification of jobs from us</small>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="mobile_phone" class="control-label">Mobile 2</label>
                            <input class="form-control" id="mobile_phone2" type="text" name="mobile_phone2"
                                onkeypress="return isNumber(event);" value="{{ old('mobile_phone2') }}">
                            <small class="text-primary">DO NOT FILL THIS COLUMN IF IT IS SAME AS MOBILE NO. 1</small>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="home_phone" class="control-label">Home Landline (If available)</label>
                            <input class="form-control" type="text" name="home_phone" id="home_phone"
                                onkeypress="return isNumber(event);" value="{{ old('home_phone') }}">
                            <small class="text-primary">Please enter landline only and AREA code first</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="home_postcode" class="control-label">Home Postcode <sup>*</sup></label>
                            <input class="form-control" type="text" name="home_postcode" id="home_postcode" required
                                value="{{-- old('home_postcode') --}}">
                            <small class="text-primary">Press TAB after entering the PostCode and choose your suburb in
                                Home Suburb Field from the DropDown Menu</small>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="home_postsuburb" class="control-label">Home Suburb <sup>*</sup></label>
                            <div class="input-group">

                                <select name="home_postsuburb" id="home_postsuburb" class="form-control custom-select"
                                    required>
                                    <option value="">Select Suburb</option>
                                </select>

                                <select name="home_subdirection" id="home_subdirection"
                                    class="form-control custom-select">
                                    <option value="">Select Direction</option>
                                    <option value="Inner city">Inner city</option>
                                    <option value="Outer City">Outer City</option>
                                    <option value="West">West</option>
                                    <option value="North">North</option>
                                    <option value="East">East</option>
                                    <option value="South">South</option>
                                </select>

                            </div>
                            <small class="text-primary clearfix">Now please choose your Home Suburb from the dropdown
                                menu</small>
                            <small class="text-danger home-suburb-not-found"></small>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="work_postcode" class="control-label">Work Postcode </label>
                            <input class="form-control" type="text" name="work_postcode" id="work_postcode"
                                value="{{-- old('work_postcode') --}}">
                            <small class="text-primary">Press TAB after entering the PostCode and choose your suburb in
                                Work Suburb Field from the DropDown Menu</small>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="work_postsuburb" class="control-label">Work Suburb</label>
                            <div class="input-group">

                                <select name="work_postsuburb" id="work_postsuburb" class="form-control custom-select">
                                    <option value="">Select Suburb</option>
                                </select>

                                <select name="work_subdirection" id="work_subdirection"
                                    class="form-control custom-select">
                                    <option value="">Select Direction</option>
                                    <option value="Inner city">Inner city</option>
                                    <option value="Outer City">Outer City</option>
                                    <option value="West">West</option>
                                    <option value="North">North</option>
                                    <option value="East">East</option>
                                    <option value="South">South</option>
                                </select>
                            </div>

                            <small class="text-primary clearfix">Now please choose your Work Suburb from the dropdown
                                menu</small>
                            <small class="text-danger work-suburb-not-found"></small>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">

                    <div class="col">
                        <div class="form-group">
                            <label for="work_phone" class="control-label">Work Phone </label>
                            <input type="text" name="work_phone" id="work_phone" class="form-control"
                                onkeypress="return isNumber(event);" value="{{ old('work_phone') }}" />
                <small class="text-primary">Please enter landline only and AREA code first</small>
        </div>

    </div>
</div>--}}
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="residency_status" class="control-label">Residency Status <sup>*</sup></label>
            <select name="residency_status" id="residency_status" class="form-control" required>
                <option value="">Select</option>
                @foreach ($residency_status as $item)
                <option value="{{ $item->name }}" {{ old('residency_status')==$item->name ? 'selected' : '' }}>
                    {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="occupation" class="control-label">Occupation <sup>*</sup></label>
            <select name="occupation_id" id="occupation" class="form-control custom-select" required>
                <option value="">Select Occupation</option>
                @foreach($occupations as $occupation)
                <option value="{{ $occupation->id }}" {{ old('occupation_id')==$occupation->id ? 'selected' : '' }}>
                    {{ $occupation->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="industry" class="control-label">Industry </label>
            <select name="industry_id" id="industry" class="form-control custom-select">
                <option value="">Select Industry</option>
                @foreach($industries as $industry)
                <option value="{{ $industry->id }}" {{ old('industry_id')==$industry->id ? 'selected' : '' }}>
                    {{ $industry->name }}
                </option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="industry" class="control-label">Cigratte Brands </label><br />
            <input type="radio" name="cigarette_brands" id="non-smoker" value="Doesn't Smoke" checked>Non Smoker
            <input type="radio" name="cigarette_brands" id="smoker" value="Regular Smoker">Smoker
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="brands" class="control-label">Brands</label>
            <select name="brands" id="brands" class="form-control custom-select" disabled="true" required>
                <option value="">Select Brands</option>
                @foreach($brands as $brand)
                <option value="{{ $brand->name }}">{{ $brand->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="occupation" class="control-label">Any Health Condition</label>
            <select name="health_problems" id="health_problems" class="form-control custom-select">
                <option value="">Select Health Condition</option>
                @foreach($healthproblems as $healthproblem)
                <option value="{{ $healthproblem->name }}">{{ $healthproblem->name }}</option>
                @endforeach
            </select>
            <input type="text" class="form-control" name="other_health_problem" id="other_health_problem"
                placeholder="Please Specify" style="display:none;" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div>
            Any info collected here will be kept strictly confidential and will only be used for
            Marketing research Purpose. Click here to read Ava Research <a class="text-primary"
                href="https://avaresearch.com.au/privacy/">Privacy Policy</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <span><img src="https://panel.avaresearch.com.au/assets/img/icon-contact.gif" alt="icon-contact"></span> 02 8072
        9959 Or 0433 153 473
        <br />
        <span>Email:</span>office@avaresearch.com.au
    </div>
</div>


<div class="row">
    <div class="col">
        <div class="form-group text-center">
            <button class="btn btn-primary btn-submit"><i class="icon-save mr-2"></i>Submit</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col text-center">
        <h2> Do not forget to register the rest of your family members and recommend a friend! </h2>

    </div>
</div>
<div class="row">
    <div class="col text-center">
        Please visit facebook regularly to view our current and future projects. Also have you
        registered/recommended the rest of your family members in anystate of Australia yet?
    </div>
</div>
<div class="row">
    <div class="col text-center">
        <a href="https://www.facebook.com/avaresearchpty/">CLICK HERE</a>
    </div>
</div>
</form>
</div>
</div>
</div>
@endsection

@section('footer')

<script>
    var mobile_phone = document.getElementById("mobile_phone")
    , mobile_phone2 = document.getElementById("mobile_phone2")
    , home_phone = document.getElementById("home_phone");

    function validatePhone(){
        if(mobile_phone.value == '' && mobile_phone2.value == '' && home_phone.value == ''){
            mobile_phone.setCustomValidity("Please specify atleast one contact number");
        } else {
            mobile_phone.setCustomValidity('');
            if(mobile_phone.value != "" && mobile_phone2.value != "" && mobile_phone.value == mobile_phone2.value) {
                mobile_phone2.setCustomValidity("Both mobile numbers should not be same");
            } else {
                mobile_phone2.setCustomValidity('');
            }
        }
    }

    mobile_phone.onchange = validatePhone;
    mobile_phone2.onkeyup = validatePhone;
    home_phone.onkeyup = validatePhone;
    $(function(){
        $("#hear_about").change(function(){
            var source = $(this).val();
            $('.source_name_wrap, .source_email_wrap').prop('hidden', true);
            $("#sourcename").html(source);
            $('.source_name_wrap').removeAttr('hidden');
            if(data == 'Others'){
                $('.source_email_wrap').removeAttr('hidden');
            }
        });

        $('input[name=cigarette_brands]').change(function(){
            if($(this).val() == 'Regular Smoker')
            {
                $("#brands").prop("disabled", false);
            }
            else{
                $("#brands").prop("disabled", true);
            }
        });

        /*$("#home_postcode").change(function(){
            var postcode = $(this).val();
            $.ajax({
                url: "{{ route('suburb') }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}',postcode: postcode},
                beforeSend: function(){
                   
                },
                success: function(res){
                    //console.log(res);
                    if(res){
                        $("#home_postsuburb").empty();
                        $("#home_postsuburb").append('<option value="">Select Suburb</option>');
                        $.each(res,function(key,value){
                            $("#home_postsuburb").append('<option value="'+value+'">'+value+'</option>');
                        });
                        
                    } else {
                        $("#home_postsuburb").append('<option>No Data Available</option>');
                    }
                },
                error: function(err){ console.log(err); }
            });
        });
        $("#work_postcode").change(function(){
            var postcode = $(this).val();
            $.ajax({
                url: "{{ route('suburb') }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}',postcode: postcode},
                beforeSend: function(){
                   
                },
                success: function(res){
                    //console.log(res);
                    if(res){
                        $("#work_postsuburb").empty();
                        $("#work_postsuburb").append('<option value="">Select Suburb</option>');
                        $.each(res,function(key,value){
                            $("#work_postsuburb").append('<option value="'+value+'">'+value+'</option>');
                        });
                        
                    } else {
                        $("#work_postsuburb").append('<option>No Data Available</option>');
                    }
                },
                error: function(err){ console.log(err); }
            });
        });*/
        /*$('#original_country').change(function(){
            var country_id = $(this).val() || 0;
            $.ajax({
                url: "{{ route('ethnicity') }}",
                type: 'POST',
                data: { _token: '{{ csrf_token() }}',country_id: country_id},
                beforeSend: function(){
                   
                },
                success: function(res){
                    //console.log(res);
                    if(res){
                        $("#ethnic_background").empty();
                        $("#ethnic_background").append('<option value="">Select</option>');
                        $.each(res,function(key,value){
                            $("#ethnic_background").append('<option value="'+value+'">'+value+'</option>');
                        });
                        
                    } else {
                        $("#ethnic_background").append('<option>No Data Available</option>');
                    }
                },
                error: function(err){ console.log(err); }
            });
        });*/

        $('#home_postcode, #state').change(function(){
            var postcode = $('#home_postcode').val();
            var state_id = $('#state').val();
            var html = `<option value="">Select</option>`;
            $('.home-suburb-not-found').html('');
            $('.btn-submit').prop('disabled', false);
            if(postcode != ''  && state_id != ''){
                $.get(`{{ route('respondents.index') }}/suburbs/${postcode}/${state_id}`, function(data){
                    if(data.length){
                        for (var i = 0; i < data.length; ++i) {
                            html += `<option value="${data[i].suburb}">${data[i].suburb}</option>`;
                        }
                    } else {
                        $('.home-suburb-not-found').html(`No Suburb for postcode ${postcode} in <b class="text-uppercase">${$('#state option:selected').text()}</b>`);
                        $('.btn-submit').prop('disabled', true);
                    }
                    $('#home_postsuburb').html(html);
                });
            }
        });
        $('#work_postcode, #state').change(function(){
            var postcode = $('#work_postcode').val();
            var state_id = $('#state').val();
            var html = `<option value="">Select</option>`;
            $('.work-suburb-not-found').html('');
            $('.btn-submit').prop('disabled', false);
            if(postcode != ''  && state_id != ''){
                $.get(`{{ route('respondents.index') }}/suburbs/${postcode}/${state_id}`, function(data){
                    if(data.length){
                        for (var i = 0; i < data.length; ++i) {
                            html += `<option value="${data[i].suburb}">${data[i].suburb}</option>`;
                        }
                    } else {
                        $('.btn-submit').prop('disabled', true);
                        $('.work-suburb-not-found').html(`No Suburb for postcode ${postcode} in <b class="text-uppercase">${$('#state option:selected').text()}</b>`);
                    }
                    $('#work_postsuburb').html(html);
                });
            }
        });

        $('#health_problems').change(function(){
            $('#other_health_problem').val('');
            if($(this).val() == 'Other'){
                $('#other_health_problem').show();
            } else {
                $('#other_health_problem').hide();
            }
        });

        $('#state').on('change', function(){
            var state_id = $('#state').val();
             $('#cityothers').hide();
            var html = `<option value="">Select</option>`;
            if(state_id != ''){
                $.get(`{{ route('respondents.index') }}/closestcity/${state_id}`, function(data){
                    if(data.length){
                        for (var i = 0; i < data.length; ++i) {
                            html += `<option value="${data[i].name}">${data[i].name}</option>`;
                        }
                        html +=`<option value="other" id="cityotheroption">Other</option>`;
                    } 
                    $('#closest_city').html(html);
                });
            }
        });

        $('#cityothers').hide();
        $('#closest_city').on('change', function() {
            var city = $('#closest_city').val();
            if(city =="other"){
                $('#cityothers').show();
            }else{
                $('#cityothers').hide();
              
        
                
            }
        });
    });
</script>

@endsection