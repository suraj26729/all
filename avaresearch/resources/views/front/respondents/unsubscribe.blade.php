@extends('layouts.respondent')
@section('title', 'Unsubscribe Respondent')
@section('content')
<div class="container-fluid relative animatedParent animateOnce my-3">
    <div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
        <h4 class="text-center">Unsubscribe with Ava Research</h4><br>
        @if(session('success'))
        <div class="toast" data-title="Success" data-message="{{ session("success") }}" data-type="success"></div>
        @endif
        <div class="row">
            <div class="col-md-4 offset-md-4">
                @if (session('errors'))
                <div class="alert alert-danger">
                    {!! session('errors')->first('message') !!}
                </div>
                @endif
                <form class="form-horizontal" action="{{ route('unsubscribe-respondent') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="email" class="control-label">Password <sup style="color: #FF0000;">*</sup></label>
                        <input type="hidden" name="uuid"  value="{{ $uuid }}">
                        <input class="form-control" id="password" type="password" name="password" required >
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">Unsubscribe Reason <sup style="color: #FF0000;">*</sup></label>
                        <textarea class="form-control" id="reason" type="text" name="reason" required>

                        </textarea>
                        
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-danger btn-submit"><i class="icon-sign-out mr-2"></i>Unsubscribe</button>

                    </div>
                    @if (Route::has('password.request'))
                        <div class="form-group mt-3 text-right">
                            <a  href="{{ route('respondent.password.request') }}"><b>
                                {{ __("Forgot Your Password?") }}
                            </b></a>
                        </div>
                        @endif

                </form>
            </div>

        </div>
    </div>
</div>
@endsection