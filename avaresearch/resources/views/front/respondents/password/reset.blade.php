@extends('layouts.respondent')
@section('title', 'Respondents Reset Password')
@section('content')
<div class="container-fluid relative animatedParent animateOnce my-3">
    <div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
        <div class="animated fadeInUpShort go">
            <h4 class="text-center">Respondents Reset Password</h4>
            <div class="row">
                <div class="col-4 offset-4">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form method="POST" action='{{ route('respondent.password.request') }}'>
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group has-icon">
                            <i class="icon-envelope-o"></i>
                            <input type="email"
                                class="form-control form-control-lg @error('email') is-invalid @enderror"
                                placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email', $email) }}"
                                required autocomplete="email" autofocus />
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group has-icon">
                            <i class="icon-user-secret"></i>
                            <input type="password"
                                class="form-control form-control-lg @error('password') is-invalid @enderror"
                                placeholder="{{ __('Password') }}" id="password" name="password" required
                                autocomplete="new-password" />
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group has-icon">
                            <i class="icon-user-secret"></i>
                            <input type="password" class="form-control form-control-lg"
                                placeholder="{{ __('Confirm Password') }}" id="password_confirmation"
                                name="password_confirmation" required autocomplete="new-password" />
                        </div>

                        <button class="btn btn-primary btn-lg btn-block"><i
                                class="fa fa-save mr-2"></i>{{ __('Reset Password') }}</button>

                        <div class="form-group mt-3">
                            <a href="{{ route('pageRespondentLogin') }}">
                                {{ __("Back to Login") }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection