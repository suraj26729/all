@extends('layouts.respondent')
@section('title', 'Unsubscribe Respondent')
@section('content')
<div class="container-fluid relative animatedParent animateOnce my-3">
    <div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
        <div class="animated fadeInUpShort go">
            <div class="text-center p-5">
                <i class="icon-trash text-danger" style="font-size: 5em;"></i>
                <h2 class="display-5 pt-3">You have been Unsubscribed</h2>
            </div>
        </div>
    </div>
</div>
@endsection