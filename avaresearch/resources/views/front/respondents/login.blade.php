@extends('layouts.respondent')
@section('title', 'Respondents Login')
@section('content')
<div class="container-fluid relative animatedParent animateOnce my-3">
    <div class="container-fluid animatedParent animateOnce my-3 white relative shadow pt-3 pb-3">
        <div class="animated fadeInUpShort go">
            <h4 class="text-center">Respondents Login</h4>
            <div class="row">
                <div class="col-4 offset-4">
                    <form method="POST" action='{{ route('respondentLogin') }}'>
                        @csrf
                        <div class="form-group has-icon">
                            <i class="icon-envelope-o"></i>
                            <input type="email"
                                class="form-control form-control-lg @error('email') is-invalid @enderror"
                                placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}"
                                required autocomplete="email" autofocus />
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group has-icon">
                            <i class="icon-user-secret"></i>
                            <input type="password"
                                class="form-control form-control-lg @error('password') is-invalid @enderror"
                                placeholder="{{ __('Password') }}" id="password" name="password" required
                                autocomplete="current-password" />
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="material-switch float-left">
                                <input id="remember" name="remember" type="checkbox"
                                    {{ old("remember") ? "checked" : "" }} checked />
                                <label for="remember" class="bg-primary"></label>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;{{ __("Remember Me") }}
                        </div>

                        <button class="btn btn-primary btn-lg btn-block"><i
                                class="icon-lock mr-2"></i>{{ __('Login') }}</button>

                        @if (Route::has('password.request'))
                        <div class="form-group mt-3">
                            <a href="{{ route('respondent.password.request') }}">
                                {{ __("Forgot Your Password?") }}
                            </a>
                        </div>
                        @endif

                        <div class="form-group mt-3">
                            <a href="{{ route('clientLogin') }}">
                                {{ __("Client Login") }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection