<?php

Route::get('/respondents/DTData', 'Respondent\RespondentController@DTData')->name('DTRespondents');
Route::get('/respondents/suburbs/{postcode}/{state_id}', 'Respondent\RespondentController@getSuburbs')->name('getSuburbs');
//closest city
Route::get('/respondents/closestcity/{state_id}', 'Respondent\RespondentController@getclosescity')->name('getclosescity');

Route::post('/respondents/register/name-dob/check', 'Respondent\RespondentController@nameDOBCheck')->name('respondentNameDOBCheck');
Route::post('/respondents/register/mobile/check', 'Respondent\RespondentController@mobileCheck')->name('respondentMobileCheck');
Route::post('/respondents/register/email/check', 'Respondent\RespondentController@emailCheck')->name('respondentEmailCheck');
Route::patch('/respondents/inline-update', 'Respondent\RespondentController@inlineUpdate')->name('inlineUpdateRespondent');

//Flag update
Route::patch('/respondents/flag/{flag}', 'Respondent\RespondentController@flagUpdate')->name('respondentFlagUpdate');

//Inactive Respondents
Route::get('/respondents/inactive/DTInactiveData', 'Respondent\RespondentController@DTInactiveData')->name('DTInactiveRespondents');
Route::get('/respondents/inactive', 'Respondent\RespondentController@listInactive')->name('listInactive');
Route::patch('/respondents/inactive/activate/{respondent}', 'Respondent\RespondentController@activate')->name('activateRespondent');
//Duplicate Respondents
Route::get('/respondents/duplicate', 'Respondent\RespondentController@duplicate')->name('duplicate');
Route::get('/respondents/duplicate/DTDuplicateData', 'Respondent\RespondentController@DTDuplicateData')->name('DTDuplicateRespondents');
Route::post('/respondents/duplicate/deletereason', 'Respondent\RespondentController@deletereason')->name('duplicateDeleteReason');
//deleted Responends
Route::get('/respondents/deleted', 'Respondent\RespondentController@listDeleted')->name('listDeleted');
Route::get('/respondents/deleted/DTDeleteData', 'Respondent\RespondentController@DTDeleteData')->name('DTDeleteRespondents');

Route::resource('respondents', 'Respondent\RespondentController');
