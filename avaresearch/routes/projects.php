<?php

Route::group(['middleware' => 'auth'], function () {
    //Projects
    Route::get('/projects/DTData', 'Project\ProjectController@DTData')->name('DTProjects');
    Route::post('/projects/checkJobNum', 'Project\ProjectController@checkJobNum')->name('checkJobNum');
    Route::post('/projects/job-report', 'Project\ProjectController@generateJobReport')->name('generateJobReport');
    Route::post('/projects/job-annual-report', 'Project\ProjectController@generateJobAnnualReport')->name('generateJobAnnualReport');

    //Groups
    Route::get('/projects/{projectUuid}/DTGroups', 'Project\ProjectGroupController@DTProjectGroups')->name('DTProjectGroups');
    Route::get('/projects/{projectUuid}/groups', 'Project\ProjectGroupController@index')->name('indexGroups');
    Route::get('/projects/{projectUuid}/groups/create', 'Project\ProjectGroupController@create')->name('createGroup');
    Route::post('/projects/{projectUuid}/groups', 'Project\ProjectGroupController@store')->name('storeGroup');
    Route::get('/projects/{projectUuid}/groups/{uuid}', 'Project\ProjectGroupController@edit')->name('editGroup');
    Route::put('/projects/{projectUuid}/groups/{group}', 'Project\ProjectGroupController@update')->name('updateGroup');
    Route::delete('/projects/{projectUuid}/groups/{group}', 'Project\ProjectGroupController@destory')->name('deleteGroup');
    Route::get('/projects/groups/timings/{group}', 'Project\ProjectGroupController@getTimings')->name('getTimings');

    //Email Templates
    Route::get('/projects/{projectUuid}/DTEmailTemps', 'Project\ProjectEmailTemplateController@DTEmailTemps')->name('DTEmailTemps');
    Route::get('/projects/{projectUuid}/email-templates', 'Project\ProjectEmailTemplateController@index')->name('indexProjectEmailTemp');
    Route::get('/projects/{projectUuid}/email-templates/create', 'Project\ProjectEmailTemplateController@create')->name('createProjectEmailTemp');
    Route::post('/projects/{projectUuid}/email-templates', 'Project\ProjectEmailTemplateController@store')->name('storeProjectEmailTemp');
    Route::get('/projects/{projectUuid}/email-templates/{uuid}/edit', 'Project\ProjectEmailTemplateController@edit')->name('editProjectEmailTemp');
    Route::put('/projects/{projectUuid}/email-templates/{template}', 'Project\ProjectEmailTemplateController@update')->name('updateProjectEmailTemp');
    Route::delete('/projects/{projectUuid}/email-templates/{template}', 'Project\ProjectEmailTemplateController@destory')->name('deleteProjectEmailTemp');
    //Email Attachment delete
    Route::delete('/projects/{projectUuid}/email-templates/{template}', 'Project\ProjectEmailTemplateController@attachmentDestory')->name('deleteProjectEmailAttTemp');

    //SMS Templates
    Route::get('/projects/{projectUuid}/DTSMSTemps', 'Project\ProjectSMSTemplateController@DTSMSTemps')->name('DTSMSTemps');
    Route::get('/projects/{projectUuid}/sms-templates', 'Project\ProjectSMSTemplateController@index')->name('indexProjectSMSTemp');
    Route::get('/projects/{projectUuid}/sms-templates/create', 'Project\ProjectSMSTemplateController@create')->name('createProjectSMSTemp');
    Route::post('/projects/{projectUuid}/sms-templates', 'Project\ProjectSMSTemplateController@store')->name('storeProjectSMSTemp');
    Route::get('/projects/{projectUuid}/sms-templates/{uuid}/edit', 'Project\ProjectSMSTemplateController@edit')->name('editProjectSMSTemp');
    Route::put('/projects/{projectUuid}/sms-templates/{template}', 'Project\ProjectSMSTemplateController@update')->name('updateProjectSMSTemp');
    Route::delete('/projects/{projectUuid}/sms-templates/{template}', 'Project\ProjectSMSTemplateController@destory')->name('deleteProjectSMSTemp');

    //Project Group Email Templates
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/DTEmailTemps', 'Project\Group\ProjectGroupEmailTemplateController@DTEmailTemps')->name('DTPGEmailTemps');
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/email-templates', 'Project\Group\ProjectGroupEmailTemplateController@index')->name('indexProjectGroupEmailTemp');
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/email-templates/create', 'Project\Group\ProjectGroupEmailTemplateController@create')->name('createProjectGroupEmailTemp');
    Route::post('/projects/{projectUuid}/groups/{groupUuid}/email-templates', 'Project\Group\ProjectGroupEmailTemplateController@store')->name('storeProjectGroupEmailTemp');
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/email-templates/{uuid}/edit', 'Project\Group\ProjectGroupEmailTemplateController@edit')->name('editProjectGroupEmailTemp');
    Route::put('/projects/{projectUuid}/groups/{groupUuid}/email-templates/{template}', 'Project\Group\ProjectGroupEmailTemplateController@update')->name('updateProjectGroupEmailTemp');
    Route::delete('/projects/{projectUuid}/groups/{groupUuid}/email-templates/{template}', 'Project\Group\ProjectGroupEmailTemplateController@destory')->name('deleteProjectGroupEmailTemp');

    //Project Group SMS Templates
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/DTSMSTemps', 'Project\Group\ProjectGroupSMSTemplateController@DTSMSTemps')->name('DTPGSMSTemps');
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/sms-templates', 'Project\Group\ProjectGroupSMSTemplateController@index')->name('indexProjectGroupSMSTemp');
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/sms-templates/create', 'Project\Group\ProjectGroupSMSTemplateController@create')->name('createProjectGroupSMSTemp');
    Route::post('/projects/{projectUuid}/groups/{groupUuid}/sms-templates', 'Project\Group\ProjectGroupSMSTemplateController@store')->name('storeProjectGroupSMSTemp');
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/sms-templates/{uuid}/edit', 'Project\Group\ProjectGroupSMSTemplateController@edit')->name('editProjectGroupSMSTemp');
    Route::put('/projects/{projectUuid}/groups/{groupUuid}/sms-templates/{template}', 'Project\Group\ProjectGroupSMSTemplateController@update')->name('updateProjectGroupSMSTemp');
    Route::delete('/projects/{projectUuid}/groups/{groupUuid}/sms-templates/{template}', 'Project\Group\ProjectGroupSMSTemplateController@destory')->name('deleteProjectGroupSMSTemp');

    //Project Queries
    Route::get('/projects/{projectUuid}/DTQueries', 'Project\ProjectQueryController@DTProjectQueries')->name('DTProjectQueries');
    Route::get('/projects/{projectUuid}/queries', 'Project\ProjectQueryController@index')->name('indexQuery');
    Route::post('/projects/{projectUuid}/queries', 'Project\ProjectQueryController@store')->name('saveQuery');
    Route::delete('/projects/{projectUuid}/queries/{query}', 'Project\ProjectQueryController@destory')->name('deleteQuery');
    Route::delete('/projects/{projectUuid}/queries/bulk/delete', 'Project\ProjectQueryController@destoryBulk')->name('deleteBulkQuery');
    Route::get('/projects/{projectUuid}/search-respondents/query/{queryUUID}', 'Project\ProjectSearchRespondentController@indexByQuery')->name('indexByQueryProjectSearchRespondents');

    //Time Allocation

     Route::get('/projects/{projectUuid}/timeallocation', 'Project\ProjectTimeAllocation@index')->name('timeallocation');
     Route::get('/projects/{projectUuid}/DTTimeAllocation', 'Project\ProjectTimeAllocation@DTTimeAllocation')->name('DTTimeAllocation');
     Route::post('/projects/TimeAllocationStart', 'Project\ProjectTimeAllocation@store')->name('StartTimeTracking');
     Route::post('/projects/TimeAllocationEnd', 'Project\ProjectTimeAllocation@update')->name('EndTimeTracking');
     Route::get('/projects/TimeAllocationEndlogout', 'Project\ProjectTimeAllocation@endtimelogout')->name('endtimelogout');



    //Project Search respondents
    Route::get('/projects/{projectUuid}/search-respondents', 'Project\ProjectSearchRespondentController@index')->name('indexProjectSearchRespondents');
    Route::post('/projects/{projectUuid}/search-respondents', 'Project\ProjectSearchRespondentController@getRespondents')->name('getProjectSearchRespondents');

    //Respondent Send Email
    Route::post('/projects/{projectUuid}/search-respondents/send-email', 'Project\ProjectSendEmailController@send')->name('sendProjectEmails');
    Route::get('/projects/{projectUuid}/search-respondents/send-email/{type}/templates', 'Project\ProjectSendEmailController@getTemplates')->name('getProjectEmailsTemplates');
    Route::get('/projects/{projectUuid}/search-respondents/send-email/{type}/templates/{id}', 'Project\ProjectSendEmailController@getTemplate')->name('getProjectEmailTemplate');

    //Respondent Send SMS
    Route::post('/projects/{projectUuid}/search-respondents/send-sms', 'Project\ProjectSendSMSController@send')->name('sendProjectSMS');
    Route::get('/projects/{projectUuid}/search-respondents/send-sms/{type}/templates', 'Project\ProjectSendSMSController@getTemplates')->name('getProjectSMSTemplates');
    Route::get('/projects/{projectUuid}/search-respondents/send-sms/{type}/templates/{id}', 'Project\ProjectSendSMSController@getTemplate')->name('getProjectSMSTemplate');

    //Standby respondents
    Route::get('/projects/{projectUuid}/DTStandByRespondents', 'Project\ProjectStandByRespondentController@DTStandByRespondents')->name('DTStandByRespondents');
    Route::get('/projects/{projectUuid}/standby-respondents', 'Project\ProjectStandByRespondentController@index')->name('indexProjectStandByRespondents');
    Route::patch('/projects/{projectUuid}/standby-respondents/move', 'Project\ProjectStandByRespondentController@move')->name('moveProjectStandByRespondents');
    Route::delete('/projects/{projectUuid}/standby-respondents/reverse', 'Project\ProjectStandByRespondentController@reverse')->name('reverseProjectStandByRespondents');

    //All respondents
    Route::get('/projects/{projectUuid}/DTAllRespondents', 'Project\ProjectAllRespondentController@DTAllRespondents')->name('DTAllRespondents');
    Route::get('/projects/{projectUuid}/all-respondents', 'Project\ProjectAllRespondentController@index')->name('indexProjectAllRespondents');
    Route::delete('/projects/{projectUuid}/all-respondents/reverse', 'Project\ProjectAllRespondentController@reverse')->name('reverseProjectAllRespondents');

    //Job Events
    Route::post('/projects/{projectUuid}/search-respondents/save-job-event', 'Project\ProjectJobEventController@store')->name('saveJobEvent');

    //Export Query
    Route::post('/projects/{projectUuid}/search-respondents/export-query', 'Project\ProjectSearchRespondentController@exportQuery')->name('exportQuery');

    //Attendee Documents
    Route::post('/projects/{projectUuid}/attendee-document/create', 'Project\ProjectAttendeeDocumentController@generate')->name('createAttendeeDocument');

    //Bank Documents
    Route::post('/projects/{projectUuid}/bank-document/create', 'Project\ProjectBankDocumentController@generate')->name('createBankDocument');

    //Group Standby respondents
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/DTStandByRespondents', 'Project\Group\ProjectGroupStandByRespondentController@DTStandByRespondents')->name('DTGroupStandByRespondents');
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/standby-respondents', 'Project\Group\ProjectGroupStandByRespondentController@index')->name('indexProjectGroupStandByRespondents');
    Route::patch('/projects/{projectUuid}/groups/{groupUuid}/standby-respondents/move', 'Project\Group\ProjectGroupStandByRespondentController@move')->name('moveProjectGroupStandByRespondents');
    Route::delete('/projects/{projectUuid}/groups/{groupUuid}/standby-respondents/reverse', 'Project\Group\ProjectGroupStandByRespondentController@reverse')->name('reverseProjectGroupStandByRespondents');

    //Group All respondents
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/DTAllRespondents', 'Project\Group\ProjectGroupAllRespondentController@DTAllRespondents')->name('DTGroupAllRespondents');
    Route::get('/projects/{projectUuid}/groups/{groupUuid}/all-respondents', 'Project\Group\ProjectGroupAllRespondentController@index')->name('indexProjectGroupAllRespondents');
    Route::delete('/projects/{projectUuid}/groups/{groupUuid}/all-respondents/reverse', 'Project\Group\ProjectGroupAllRespondentController@reverse')->name('reverseProjectGroupAllRespondents');

    //-------
    Route::resource('projects', 'Project\ProjectController');
    //-------
});
