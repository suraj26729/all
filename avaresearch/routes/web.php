<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/respondents/login', 'Front\RespondentController@respondentLoginForm')->name('pageRespondentLogin');
Route::post('/respondents/login', 'Front\RespondentController@respondentLogin')->name('respondentLogin');
Route::get('/respondents/register', 'Front\RespondentController@index')->name('register_respondents');
//Demo Responends
Route::get('/respondents/registerdemo', 'Front\RespondentController@indexdemo')->name('register_respondents_demo');

Route::post('/suburb', 'Front\RespondentController@suburb')->name('suburb');
Route::post('/ethnicity', 'Front\RespondentController@ethnicity')->name('ethnicity');
Route::post('/add-respondent', 'Front\RespondentController@store')->name('add-respondent');

Route::get('/respondents/unsubscribe/{uuid}', 'Front\RespondentController@unsubscribe')->name('unsubscribeNow');

Route::post('/respondents/unsubscribe', 'Front\RespondentController@unsubscribeWithReason')->name('unsubscribe-respondent');

Route::get('/respondents/unsubscribeEmail/{uuid}', 'Front\RespondentController@emailUnsubscribe')->name('unsubscribeEmail');


//Route::post('/respondents/unsubscribe', 'Front\RespondentController@postUnsubscribe')->name('unsubscribe-respondent');

Route::post('/respondents/password/email', 'Auth\RespondentForgotPasswordController@sendResetLinkEmail')->name('respondent.password.email');
Route::get('/respondents/password/reset', 'Auth\RespondentForgotPasswordController@showLinkRequestForm')->name('respondent.password.request');
Route::post('/respondents/password/reset', 'Auth\RespondentResetPasswordController@reset')->name('respondent.password.update');
Route::get('/respondents/password/reset/{token}', 'Auth\RespondentResetPasswordController@showResetForm')->name('respondent.password.reset');

Route::get('/clients/login', 'Front\ClientController@login')->name('clientLogin');
Route::post('/clients/login', 'Front\ClientController@postLogin')->name('clientPostLogin');


Route::get('/', function () {
    return redirect(route('home'));
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile/edit', 'HomeController@editProfile')->name('edit-profile');
Route::post('/profile/edit', 'HomeController@saveProfile')->name('save-profile');

Route::get('/logout', function () {
    if (auth()->guard('respondent')->check()) {
        //auth('respondent')->user()->update(['login_status' => 0]);
        auth('respondent')->logout();
        return redirect(route('pageRespondentLogin'));
    } else if (auth()->check() && auth()->user()->type == 'client') {
        auth()->user()->update(['login_status' => 0]);
        auth()->logout();
        return redirect(route('clientLogin'));
    } else {
        if (auth()->check()) auth()->user()->update(['login_status' => 0]);
        auth()->logout();
        Session::flush();
        return redirect(route('login'));
    }
})->name('logout');

Route::group(['middleware' => 'auth'], function () {

    Route::resource('staffs', 'StaffController')->middleware('App\Http\Middleware\AdminMiddleware');

    //Clients
    Route::get('/clients/DTData', 'ClientController@DTData')->name('DTClients');
    Route::get('/clients/{uuid}/projects', 'ClientController@getProjects')->name('getProjects');
    Route::get('/clients/{uuid}/DTProjectData', 'ClientController@DTProjectData')->name('DTClientProjects');
    Route::resource('clients', 'ClientController');
    //----------

    //Email Templates
    Route::get('/email-templates/DTData', 'EmailTemplateController@DTData')->name('DTEmailTemplates');
    Route::delete('/email-templates/deleteEmailTempAttach', 'EmailTemplateController@deleteEmailTempAttach')->name('deleteEmailTempAttach');
    Route::get('/email-templates/{template}/json', 'EmailTemplateController@singleJson')->name('EmailTemplateSingleJson');
    Route::resource('email-templates', 'EmailTemplateController');
    //--------------------

    //SMS Templates
    Route::get('/sms-templates/DTData', 'SMSTemplateController@DTData')->name('DTSMSTemplates');
    Route::resource('sms-templates', 'SMSTemplateController');
    //--------------------

    //Calendar
    Route::get('/calendar/DTData', 'CalendarController@DTData')->name('DTCalendar');
    Route::resource('calendar', 'CalendarController');

    //Staff Qualified Report
    Route::post('/staff-qualified-report/getData', 'StaffReportController@getData')->name('getDataDTQualifiedReport');
    Route::resource('staff-qualified-report', 'StaffReportController');

    //Qulified Report
    //Route::resource('/reports', 'QulifiedReportController');

    //Leads
    Route::resource('leads', 'LeadController');

    
});
