<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Respondents\RespondentService;
use App\Models\Masters\MasterBank;
use App\Models\Masters\MasterPhoneProvider;
use DB;

class RespondentServiceSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondentservice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Respondent Service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondent_service.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                // DB::statement("TRUNCATE respondent_services");
                // DB::statement("ALTER TABLE respondent_services AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                foreach ($importData_arr as $key => $item) {
                    $exists = RespondentService::where('respondent_id', $item[0])->withTrashed()->count();
                    if($exists==0){
                    //if ($item[0] > 35889) break;
                    $this->info($key);

                    $bankIds = isset($item[1]) && $item[1] != 'NULL' && !empty($item[1]) ? $item[1] : null;
                    if ($bankIds != null) {
                        $bankIds = explode(',', $bankIds);
                        $_banks = [];
                        foreach ($bankIds as $_item) {
                            $data = MasterBank::where('name', $_item)->first();
                            if ($data != null) $_banks[] = $data->id;
                        }
                        $bankIds = count($_banks) ? implode(',', $_banks) : null;
                    }

                    $home_phone_provider = !empty($item[6]) && $item[6] != 'NULL' ? $item[6] : null;
                    if ($home_phone_provider != null) {
                        $data = MasterPhoneProvider::where('name', $home_phone_provider)->first();
                        $home_phone_provider = $data != null ? $data->id : null;
                    }

                    $work_phone_provider = !empty($item[7]) && $item[7] != 'NULL' ? $item[7] : null;
                    if ($work_phone_provider != null) {
                        $data = MasterPhoneProvider::where('name', $work_phone_provider)->first();
                        $work_phone_provider = $data != null ? $data->id : null;
                    }

                    $created_at = isset($item[15]) && $item[15] != 'NULL' && $item[15] != '' && $item[15] != '0000-00-00 00:00:00' ? $item[15] : date('Y-m-d H:i:s');
                    $updated_at = isset($item[16]) && $item[16] != 'NULL' && $item[16] != '' && $item[16] != '0000-00-00 00:00:00' ? $item[16] : date('Y-m-d H:i:s');

                    $data = [
                        'respondent_id' => $item[0],

                        'bank_id' => $bankIds,

                        'superannuation_id' => !empty($item[2]) && $item[2] != 'NULL' ? $item[2] : null,
                        'non_super_investments' => !empty($item[3]) && $item[3] != 'NULL' ? $item[3] : null,
                        'insurance_providers' => !empty($item[4]) && $item[4] != 'NULL' ? $item[4] : null,
                        'insurance_policies' => !empty($item[5]) && $item[5] != 'NULL' ? $item[5] : null,

                        'home_phone_provider' => $home_phone_provider,
                        'work_phone_provider' => $work_phone_provider,
                        'mobile1_provider' => !empty($item[8]) && $item[8] != 'NULL' ? $item[8] : null,
                        'mobile2_provider' => !empty($item[9]) && $item[9] != 'NULL' ? $item[9] : null,
                        'mobile1_provider_other' => !empty($item[10]) && $item[10] != 'NULL' ? $item[10] : null,
                        'mobile2_provider_other' => !empty($item[11]) && $item[11] != 'NULL' ? $item[11] : null,
                        'mobile1_type' => !empty($item[12]) && $item[12] != 'NULL' ? $item[13] : null,
                        'mobile2_type' => !empty($item[13]) && $item[13] != 'NULL' ? $item[13] : null,

                        'tv_providers' => !empty($item[14]) && $item[14] != 'NULL' ? $item[14] : null,

                        'created_at' => $created_at,
                        'updated_at' => $updated_at
                    ];

                    $respondent = RespondentService::create($data);
                    }
                }
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
