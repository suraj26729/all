<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User, App\UserDetail;
use DB;

class ClientsSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:clients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/clients.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');
                // User::where('type', 'staff')->delete();
                DB::statement("DELETE from user_details WHERE user_id IN (SELECT id from users WHERE type = 'client')");
                DB::statement("DELETE from users WHERE type = 'client'");

                DB::statement("ALTER TABLE users AUTO_INCREMENT =  1");
                DB::statement("ALTER TABLE user_details AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    // Skip first row (Remove below comment if you want to skip the first row)
                    // if ($i == 0) {
                    //     $i++;
                    //     continue;
                    // }
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                //$this->info(json_encode($importData_arr[1][8]));

                // Insert to MySQL database
                $data = [];
                $detail_data = [];
                foreach ($importData_arr as $key => $item) {
                    if (!isset($item[8])) $this->info(json_encode($item) . '\n\n');
                    $data[$key] = [
                        'id' => $item[0],
                        'type' => 'client',
                        'email' => $item[8],
                        'username' => $item[10],
                        'password' => !empty($item[11]) ? bcrypt($item[11]) : bcrypt('soft@123'),
                        'last_login_date' => isset($item[14]) && $item[14] != '0000-00-00' && $item[14] != 'NULL' ? date('Y-m-d H:i:s', strtotime($item[14])) : null,
                        'login_status' => isset($item[15]) && $item[15] != 'NULL' ? $item[15] : 0
                    ];
                    $detail_data[$key] = [
                        'user_id' => 0,
                        'first_name' => $item[2],
                        'last_name' => $item[3],
                        'company_name' => $item[4],
                        'additional_email' => $item[9],
                        'website' => $item[5],
                        'contact_no' => $item[7],
                        'fax_no' => $item[6],
                        'image' => $item[12],
                        'address' => $item[13],
                        //'searched_data' => $item[14],
                        //'project_searched_data' => $item[15],
                    ];
                }

                if (count($data)) {
                    foreach ($data as $key => $item) {
                        $user = User::create($item);
                        if ($user) {
                            $detail_data[$key]['user_id'] = $user->id;
                            $detail = UserDetail::create($detail_data[$key]);
                        }
                    }
                }

                $this->info(User::count());
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
