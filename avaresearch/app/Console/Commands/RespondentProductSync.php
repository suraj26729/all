<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Respondents\RespondentProduct;
use App\Models\Masters\MasterVehicleMake;
use App\Models\Masters\MasterVehicleType;
use App\Models\Masters\MasterRewardProgram;
use App\Models\Masters\MasterTechnologyUse;
use App\Models\Masters\MasterInternetProvider;
use App\Models\Masters\MasterAlcoholConsumed;
use DB;

class RespondentProductSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondentproduct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Respondent Products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondent_product.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                // DB::statement("TRUNCATE respondent_products");
                // DB::statement("ALTER TABLE respondent_products AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                foreach ($importData_arr as $key => $item) {
                    $exists = RespondentProduct::where('respondent_id', $item[0])->withTrashed()->count();
                    if($exists==0){
                    //if ($item[0] > 35889) break;
                    $this->info($key);

                    $vehicle_make_id = !empty($item[1]) && $item[1] != 'NULL' ? $item[1] : null;
                    $vehicle_make_id = MasterVehicleMake::where('name', $vehicle_make_id)->first();
                    $vehicle_make_id = $vehicle_make_id != null ? $vehicle_make_id->id : null;

                    $vehicle2_make_id = !empty($item[2]) && $item[2] != 'NULL' ? $item[2] : null;
                    $vehicle2_make_id = MasterVehicleMake::where('name', $vehicle2_make_id)->first();
                    $vehicle2_make_id = $vehicle2_make_id != null ? $vehicle2_make_id->id : null;

                    $vehicle_type_id = !empty($item[3]) && $item[3] != 'NULL' ? $item[3] : null;
                    $vehicle_type_id = MasterVehicleType::where('name', $vehicle_type_id)->first();
                    $vehicle_type_id = $vehicle_type_id != null ? $vehicle_type_id->id : null;

                    $vehicle2_type_id = !empty($item[4]) && $item[4] != 'NULL' ? $item[4] : null;
                    $vehicle2_type_id = MasterVehicleType::where('name', $vehicle2_type_id)->first();
                    $vehicle2_type_id = $vehicle2_type_id != null ? $vehicle2_type_id->id : null;

                    $rewards_progammes = isset($item[12]) && $item[12] != 'NULL' && !empty($item[12]) ? $item[12] : null;
                    if ($rewards_progammes != null) {
                        $rewards_progammes = explode(',', $rewards_progammes);
                        $_rewards_progammes = [];
                        foreach ($rewards_progammes as $_item) {
                            $data = MasterRewardProgram::where('name', $_item)->first();
                            if ($data != null) $_rewards_progammes[] = $data->id;
                        }
                        $rewards_progammes = count($_rewards_progammes) ? implode(',', $_rewards_progammes) : null;
                    }

                    $technology_use = isset($item[13]) && $item[13] != 'NULL' && !empty($item[13]) ? $item[13] : null;
                    if ($technology_use != null) {
                        $technology_use = explode(',', $technology_use);
                        $_technology_use = [];
                        foreach ($technology_use as $_item) {
                            $data = MasterTechnologyUse::where('name', $_item)->first();
                            if ($data != null) $_technology_use[] = $data->id;
                        }
                        $technology_use = count($_technology_use) ? implode(',', $_technology_use) : null;
                    }

                    $internet_provider = !empty($item[15]) && $item[15] != 'NULL' ? $item[15] : null;
                    $internet_provider = MasterInternetProvider::where('name', $internet_provider)->first();
                    $internet_provider = $internet_provider != null ? $internet_provider->id : null;

                    $alcohol_consumed = !empty($item[16]) && $item[16] != 'NULL' ? $item[16] : null;
                    $alcohol_consumed = MasterAlcoholConsumed::where('name', $alcohol_consumed)->first();
                    $alcohol_consumed = $alcohol_consumed != null ? $alcohol_consumed->id : null;


                    $created_at = isset($item[19]) && $item[19] != 'NULL' && $item[19] != '' && $item[19] != '0000-00-00 00:00:00' ? $item[19] : date('Y-m-d H:i:s');
                    $updated_at = isset($item[20]) && $item[20] != 'NULL' && $item[20] != '' && $item[20] != '0000-00-00 00:00:00' ? $item[20] : date('Y-m-d H:i:s');

                    $data = [
                        'respondent_id' => $item[0],

                        'vehicle_make_id' => $vehicle_make_id,
                        'vehicle2_make_id' => $vehicle2_make_id,
                        'vehicle_type_id' => $vehicle_type_id,
                        'vehicle2_type_id' => $vehicle2_type_id,

                        'vehicle_model' => !empty($item[5]) && $item[5] != 'NULL' ? $item[5] : null,
                        'vehicle2_model' => !empty($item[6]) && $item[6] != 'NULL' ? $item[6] : null,
                        'vehicle_year' => !empty($item[7]) && $item[7] != 'NULL' ? $item[7] : null,
                        'vehicle2_year' => !empty($item[8]) && $item[8] != 'NULL' ? $item[8] : null,

                        'travel_location' => !empty($item[9]) && $item[9] != 'NULL' ? $item[9] : null,
                        'travel_reason' => !empty($item[10]) && $item[10] != 'NULL' ? $item[10] : null,
                        'travel_frequency' => !empty($item[11]) && $item[11] != 'NULL' ? $item[11] : null,

                        'rewards_progammes' => $rewards_progammes,
                        'technology_use' => $technology_use,

                        'general_products' => !empty($item[14]) && $item[14] != 'NULL' ? $item[14] : null,

                        'internet_provider' => $internet_provider,
                        'alcohol_consumed' => $alcohol_consumed,

                        'optical' => !empty($item[17]) && $item[17] != 'NULL' ? $item[17] : null,
                        'lens_brand' => !empty($item[18]) && $item[18] != 'NULL' ? $item[18] : null,

                        'created_at' => $created_at,
                        'updated_at' => $updated_at
                    ];

                    $respondent = RespondentProduct::create($data);
                    }
                }
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
