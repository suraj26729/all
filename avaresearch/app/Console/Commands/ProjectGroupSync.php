<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UserDetail;
use App\Models\Project;
use App\Models\ProjectGroup;
use App\Models\Masters\MasterState;
use DB;

class ProjectGroupSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:projectgroups';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Projectgroups';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/groups.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');
                DB::statement("TRUNCATE project_groups");
                DB::statement("ALTER TABLE project_groups AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    // Skip first row (Remove below comment if you want to skip the first row)
                    // if ($i == 0) {
                    //     $i++;
                    //     continue;
                    // }
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                //$this->info(json_encode($importData_arr[1][8]));

                // Insert to MySQL database
                $data = [];
                $detail_data = [];
                foreach ($importData_arr as $key => $item) {
                    $location = MasterState::where('name', $item[3])->first();
                    $data[$key] = [
                        'id' => $item[0],
                        'job_id' => $item[1],
                        'name' => $item[2],
                        'location' => $location != null ? $location->id : null,
                        'venue_address' => $item[4],
                        'venue_phone' => $item[5],
                        'client_phone' => $item[6],
                        'research_phone' => $item[7],
                        'client_email' => $item[8],
                        'research_email' => $item[9],
                        'incentive' => $item[10],
                        'other' => $item[11],
                        'group_date' => $item[13],
                        'group_time' => $item[14],
                        'different_time' => $item[15],
                        'duration' => $item[16] != 'NULL' ? $item[16] : null,
                        'created_by' => 1
                    ];
                    if ($item[13] != '0000-00-00') $data[$key]['group_date'] = $item[13];
                    else $data[$key]['group_date'] = null;
                    if ($item[14] != '00:00:00' && $item[14] != 'NULL') $data[$key]['group_time'] = $item[14];
                    else $data[$key]['group_time'] = null;
                    if ($item[15] != 'NULL' && $item[15] != '0') $data[$key]['different_time'] = $item[15];
                    else $data[$key]['different_time'] = null;
                }

                if (count($data)) {
                    foreach ($data as $key => $item) {
                        $group = ProjectGroup::create($item);
                    }
                }

                $this->info(Project::count());
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
