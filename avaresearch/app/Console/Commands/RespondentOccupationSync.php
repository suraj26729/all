<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Respondents\RespondentOccupation;
use App\Models\Masters\MasterOccupation;
use App\Models\Masters\MasterEmployerSize;
use App\Models\Masters\MasterIndustry;
use DB;

class RespondentOccupationSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondentoccupation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Respondent Occupation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondent_occupation.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                // DB::statement("TRUNCATE respondent_occupations");
                // DB::statement("ALTER TABLE respondent_occupations AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                foreach ($importData_arr as $key => $item) {
                    $exists = RespondentOccupation::where('respondent_id', $item[0])->withTrashed()->count();
                    if($exists==0){
                    //if ($item[0] > 35889) break;
                    $this->info($key);

                    $occupation_id = isset($item[2]) && !empty($item[2]) ? $item[2] : null;
                    $occupation_id = MasterOccupation::where('name', $occupation_id)->first();
                    $occupation_id = $occupation_id != null ? $occupation_id->id : null;

                    $partner_occupation_id = isset($item[3]) && !empty($item[3]) ? $item[3] : null;
                    $partner_occupation_id = MasterOccupation::where('name', $partner_occupation_id)->first();
                    $partner_occupation_id = $partner_occupation_id != null ? $partner_occupation_id->id : null;

                    $employer_size_id = isset($item[10]) && !empty($item[10]) ? $item[10] : null;
                    $employer_size_id = MasterEmployerSize::where('name', $employer_size_id)->first();
                    $employer_size_id = $employer_size_id != null ? $employer_size_id->id : null;

                    $partner_employer_size_id = isset($item[11]) && !empty($item[11]) ? $item[11] : null;
                    $partner_employer_size_id = MasterEmployerSize::where('name', $partner_employer_size_id)->first();
                    $partner_employer_size_id = $partner_employer_size_id != null ? $partner_employer_size_id->id : null;

                    $industry_id = isset($item[14]) && !empty($item[14]) ? $item[14] : null;
                    $industry_id = MasterIndustry::where('name', $industry_id)->first();
                    $industry_id = $industry_id != null ? $industry_id->id : null;

                    $partner_industry = isset($item[15]) && !empty($item[15]) ? $item[15] : null;
                    $partner_industry = MasterIndustry::where('name', $partner_industry)->first();
                    $partner_industry = $partner_industry != null ? $partner_industry->id : null;

                    $created_at = isset($item[26]) && $item[26] != 'NULL' && $item[26] != '' ? $item[26] : date('Y-m-d H:i:s');
                    $updated_at = isset($item[27]) && $item[27] != 'NULL' && $item[27] != '' ? $item[27] : date('Y-m-d H:i:s');

                    $data = [
                        'respondent_id' => $item[0],
                        'profession' => !empty($item[1]) && $item[1] != 'NULL'  ? $item[1] : null,

                        'occupation_id' => $occupation_id,
                        'partner_occupation_id' => $partner_occupation_id,

                        'type' => !empty($item[4]) && $item[4] != 'NULL' ? $item[4] : null,
                        'hours_per_week' => !empty($item[5]) && $item[5] != 'NULL' ? $item[5] : null,
                        'job_title' => !empty($item[6]) && $item[6] != 'NULL' ? $item[6] : null,
                        'partner_job_title' => !empty($item[7]) && $item[7] != 'NULL' ? $item[7] : null,
                        'employer_name' => !empty($item[8]) && $item[8] != 'NULL' ? $item[8] : null,
                        'partner_employer_name' => !empty($item[9]) && $item[9] != 'NULL' ? $item[9] : null,

                        'employer_size_id' => $employer_size_id,
                        'partner_employer_size_id' => $partner_employer_size_id,

                        'employer_turnover' => !empty($item[12]) && $item[12] != 'NULL' ? $item[12] : null,
                        'partner_employer_turnover' => !empty($item[13]) && $item[13] != 'NULL' ? $item[13] : null,

                        'industry_id' => $industry_id,
                        'partner_industry' => $partner_industry,

                        'highest_education' => !empty($item[16]) && $item[16] != 'NULL' ? $item[16] : null,
                        'education_institute_type' => !empty($item[17]) && $item[17] != 'NULL' ? $item[17] : null,
                        'education_institute_suburb' => !empty($item[18]) && $item[18] != 'NULL' ? $item[18] : null,
                        'education_year' => !empty($item[19]) && $item[19] != 'NULL' ? $item[19] : null,
                        'education_study_field' => !empty($item[20]) && $item[20] != 'NULL' ? $item[20] : null,
                        'partner_education_study_field' => !empty($item[21]) && $item[21] != 'NULL' ? $item[21] : null,
                        'company_name' => !empty($item[22]) && $item[22] != 'NULL' ? $item[22] : null,
                        'company_postcode' => !empty($item[23]) && $item[23] != 'NULL' ? $item[23] : null,
                        'company_website' => !empty($item[24]) && $item[24] != 'NULL' ? $item[24] : null,
                        'company_email' => !empty($item[25]) && $item[25] != 'NULL' ? $item[25] : null,

                        'created_at' => $created_at,
                        'updated_at' => $updated_at
                    ];

                    $respondent = RespondentOccupation::create($data);
                    }
                }
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
