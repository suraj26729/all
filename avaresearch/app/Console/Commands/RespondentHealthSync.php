<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Respondents\RespondentHealth;
use App\Models\Masters\MasterHealthFund;
use DB;

class RespondentHealthSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondenthealth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Respondent Health';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondent_health.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                //DB::statement("TRUNCATE respondent_healths");
                //DB::statement("ALTER TABLE respondent_healths AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                foreach ($importData_arr as $key => $item) {
                    $exists = RespondentHealth::where('respondent_id', $item[0])->withTrashed()->count();
                    if($exists==0){
                    //if ($item[0] > 35889) break;
                    $this->info($key);

                    $health_funds = isset($item[3]) && $item[3] != 'NULL' && !empty($item[3]) ? $item[3] : null;
                    if ($health_funds != null) {
                        $health_funds = explode(',', $health_funds);
                        $_health_funds = [];
                        foreach ($health_funds as $_item) {
                            $data = MasterHealthFund::where('name', $_item)->first();
                            if ($data != null) $_health_funds[] = $data->id;
                        }
                        $health_funds = count($_health_funds) ? implode(',', $_health_funds) : null;
                    }

                    $organ_donate = !empty($item[13]) && $item[13] != 'NULL' ? $item[13] : null;
                    $organ_donate = $organ_donate == 'Yes' ? 1 : 0;

                    $created_at = isset($item[15]) && $item[15] != 'NULL' && $item[15] != '' && $item[15] != '0000-00-00 00:00:00' ? $item[15] : date('Y-m-d H:i:s');
                    $updated_at = isset($item[16]) && $item[16] != 'NULL' && $item[16] != '' && $item[16] != '0000-00-00 00:00:00' ? $item[16] : date('Y-m-d H:i:s');

                    //$this->info(json_encode($item));

                    $data = [
                        'respondent_id' => $item[0],

                        'health_problems' => !empty($item[1]) && $item[1] != 'NULL' ? $item[1] : null,
                        'dietary_requirements' => !empty($item[2]) && $item[2] != 'NULL' ? $item[2] : null,
                        'health_funds' => $health_funds,

                        'other_health_fund' => !empty($item[4]) && $item[4] != 'NULL' ? $item[4] : null,

                        'cigarette_brands' => !empty($item[5]) && $item[5] != 'NULL' ? $item[5] : null,
                        'partner_cigarette_brands' => !empty($item[6]) && $item[6] != 'NULL' ? $item[6] : null,

                        'quite_smoking_methods' => !empty($item[7]) && $item[7] != 'NULL' ? $item[7] : null,
                        'nicotine_replacement' => !empty($item[8]) && $item[8] != 'NULL' ? $item[8] : null,

                        'brands' => !empty($item[9]) && $item[9] != 'NULL' ? $item[9] : null,
                        'orther_brands' => !empty($item[10]) && $item[10] != 'NULL' ? $item[10] : null,

                        'partner_brands' => !empty($item[11]) && $item[11] != 'NULL' ? $item[11] : null,
                        'other_partner_brands' => !empty($item[12]) && $item[12] != 'NULL' ? $item[12] : null,

                        'organ_donate' => $organ_donate,
                        'donated_organ' => !empty($item[14]) && $item[14] != 'NULL' ? $item[14] : null,

                        'created_at' => $created_at,
                        'updated_at' => $updated_at
                    ];

                    $respondent = RespondentHealth::create($data);
                    }
                }
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
