<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Masters\MasterEthnicBackground;
use App\Models\Masters\MasterResidencyStatus;
use App\Models\Masters\MasterHealthProblem;
use App\Models\Masters\MasterBrand;
use App\Models\Respondents\RespondentHealth;

class EthnicBackgroundImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:ethnicity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Ethnic Backgrounds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $ethnic_arr = [
            1 => 'Pashtuns,Tajik,Hazara,Uzbek, Aymāq, Turkmen, Baloch,Pashayi',
            3 => 'Albanians',
            5 => 'Samoan, Tongan',
            6 => 'Andorran,Spanish, Portuguese, French',
            7 => 'Ovimbundu',
            11 => 'European',
            12 => 'Armenian, Yezidi ( Kurd)',
            14 => 'English, Scottish, Italian, Greek, Aboriginee, Indian',
            4 => 'Arab, Berber',
            19 => 'Bengali',
            37 => 'Khmer',
            39 => 'Canadian, English, Scottish French, Irish Native Indian, East Indian',
            55 => 'Croat, Serb',
            57 => 'Greek',
            65 => 'Egyptian',
            74 => 'Finn',
            99 => 'Mandarine speaking Chinese, Cantonese speaking Chinese, Chinese others',
            102 => 'Indo-Aryan',
            104 => 'Persian, Arab',
            105 => 'Arab, Kurd',
            107 => 'Jewish, Palestanian, Arab',
            108 => 'Italian',
            110 => 'Japanese',
            115 => 'Koreans',
            124 => 'Arab, Armenian',
            136 => 'Bhumiputera, Indians',
            158 => 'Nepalese',
            171 => 'Punjabi, Pashtun, Sindhi',
            185 => 'Russian',
            193 => 'Arab, Afro-Asian',
            197 => 'Malay, Indian',
            202 => 'Black African, Colourd, White, Indian',
            204 => 'Spanish',
            205 => 'Sinhalese, Tamil',
            215 => 'Mandarine speaking Chinese, Cantonese speaking Chinese, Chinese others',
            220 => 'Thai',
            226 => 'Turkish, Kurdish',
            233 => 'White',
            240 => 'Viet',
        ];

        foreach($ethnic_arr as $country_id => $value){
            $ethnicity = explode(',', $value);
            foreach ($ethnicity as $item) {
                $item = trim($item);

                $exists = MasterEthnicBackground::where('name', $item)->where('country_id', $country_id)->exists();
                if (!$exists) {
                    MasterEthnicBackground::create(['country_id' => $country_id, 'name' => $item, 'isactive' => 1]);
                }
            }
        }

        $residency_statuses = ['Australian Citizen', 'Newzealanders', 'Permanent Resident', 'Student Visa', 'Business Visa', 'Temporary Visa'];
        foreach($residency_statuses as $value){
            MasterResidencyStatus::create(['name' => $value, 'isactive' => 1]);
        }

        // $healths = RespondentHealth::select('id', 'health_problems', 'cigarette_brands', 'brands')->get();
        // $count = 0;
        // foreach ($healths as $health) {
        //     if (is_numeric($health->health_problems)) {
        //         $problem = MasterHealthProblem::find($health->health_problems);
        //         if ($problem) {
        //             $count += 1;
        //             $health->update(['health_problems' => $problem->name]);
        //         }
        //     }
        //     if ($health->cigarette_brands == 'Smoke') {
        //         $health->update(['cigarette_brands' => 'Regular Smoker']);
        //     }
        //     if ($health->cigarette_brands == 'Does not Smoke') {
        //         $health->update(['cigarette_brands' => "Doesn't Smoke"]);
        //     }
        //     if (is_numeric($health->brands)) {
        //         $brand = MasterBrand::find($health->brands);
        //         if ($brand) $health->update(['brands' => $brand->name]);
        //     }
        // }

        // $this->info('Total: ' . $count);
    }
}
