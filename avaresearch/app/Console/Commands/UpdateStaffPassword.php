<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class UpdateStaffPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:staff-passwords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Generate Staff Passwords';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $i = 0;
            $staffs = User::with('profile')->where('type', 'staff')->get();
            foreach($staffs as $staff){
                $i+=1;
                $staff->update(['password' => bcrypt('JQhvFz\Mw3S73vXV')]);
            }
            $this->info($i);
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
