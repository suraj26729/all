<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Respondents\RespondentDetail;
use App\Models\Masters\MasterState;
use DB;

class RespondentDetailSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondentdetails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Respondent Details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondent_details.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                // DB::statement("TRUNCATE respondent_details");
                // DB::statement("ALTER TABLE respondent_details AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                foreach ($importData_arr as $key => $item) {
                    $exists = RespondentDetail::where('respondent_id', $item[0])->withTrashed()->count();
                    if($exists==0){
                        //if ($item[0] > 35889) break;
                        $this->info($key);
                        //$this->info(json_encode($item));

                        $state_name = isset($item[1]) && !empty($item[1]) ? $item[1] : null;
                        $state = MasterState::where('name', $state_name)->first();
                        $state = $state != null ? $state->id : 1;

                        $online_survey = isset($item[14]) && $item[14] != 'NULL' && $item[14] != '' ? $item[14] : null;
                        $online_survey = $online_survey == 'Yes' ? 1 : 0;

                        $unsub_at = isset($item[17]) && $item[17] != 'NULL' && $item[17] != '' && $item[17] != '0000-00-00 00:00:00' ? $item[17] : null;

                        $created_date = isset($item[18]) && $item[18] != 'NULL' && $item[18] != '' ? $item[18] : date('Y-m-d H:i:s');

                        $data = [
                            'respondent_id' => $item[0],
                            'state' => $state,
                            'closest_large_city' =>  isset($item[2]) && !empty($item[2]) ? $item[2] : null,
                            'home_postcode' =>  isset($item[3]) && !empty($item[3]) ? $item[3] : null,
                            'home_postsuburb' => isset($item[4]) && !empty($item[4]) ? $item[4] : null,
                            'work_postcode' => isset($item[5]) && !empty($item[5]) ? $item[5] : null,
                            'work_postsuburb' => isset($item[6]) && !empty($item[6]) ? $item[6] : null,
                            'home_phone' => isset($item[7]) && !empty($item[7]) ? $item[7] : null,
                            'work_phone' => isset($item[8]) && !empty($item[8]) ? $item[8] : null,
                            'mobile_phone' => isset($item[9]) && !empty($item[9]) ? $item[9] : null,
                            'mobile_phone2' => isset($item[10]) && !empty($item[10]) ? $item[10] : null,
                            'hear_about' => isset($item[11]) && !empty($item[11]) ? $item[11] : null,
                            'source_name' => isset($item[12]) && !empty($item[12]) ? $item[12] : null,
                            'source_email' => isset($item[13]) && !empty($item[13]) ? $item[13] : null,
                            'online_survey' => $online_survey,
                            'articulation' => isset($item[15]) && !empty($item[15]) ? $item[15] : 0,
                            'last_modified_by' => 1,
                            'unsub_reason' => isset($item[16]) && !empty($item[16]) ? $item[16] : null,
                            'unsub_at' => $unsub_at,
                            'created_at' => $created_date,
                            'updated_at' => $unsub_at
                        ];
                        $respondent = RespondentDetail::create($data);
                    }
                }
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
