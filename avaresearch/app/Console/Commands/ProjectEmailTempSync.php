<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Project\ProjectEmailTemplate;
use DB;

class ProjectEmailTempSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:projectemailtemplates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Project Email Templates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/project_email_template.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                DB::statement("TRUNCATE project_email_template_attachments");
                DB::statement("ALTER TABLE project_email_template_attachments AUTO_INCREMENT =  1");

                DB::statement("TRUNCATE project_email_templates");
                DB::statement("ALTER TABLE project_email_templates AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    // Skip first row (Remove below comment if you want to skip the first row)
                    // if ($i == 0) {
                    //     $i++;
                    //     continue;
                    // }
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                //$this->info(json_encode($importData_arr[1][8]));

                // Insert to MySQL database
                $data = [];
                $attach_data = [];
                foreach ($importData_arr as $key => $item) {
                    $data[$key] = [
                        'id' => $item[0],
                        'job_id' => $item[1],
                        'title' => $item[2],
                        'message' => $item[3],
                        'group_id' => is_numeric($item[5]) ? $item[5] : null,
                        'created_by' => 1
                    ];

                    if ($item[4] != '') {
                        $attach_data[$item[0]][] = [
                            'template_id' => $item[0],
                            'url' => 'docs/attachments/' . $item[4]
                        ];
                    }

                    if ($item[6] != '') {
                        $attach_data[$item[0]][] = [
                            'template_id' => $item[0],
                            'url' => 'docs/attachments/' . $item[6]
                        ];
                    }

                    if ($item[7] != '') {
                        $attach_data[$item[0]][] = [
                            'template_id' => $item[0],
                            'url' => 'docs/attachments/' . $item[7]
                        ];
                    }
                }

                if (count($data)) {
                    foreach ($data as $key => $item) {
                        $template = ProjectEmailTemplate::create($item);
                        if (isset($attach_data[$template->id])) {
                            foreach ($attach_data[$template->id] as $item) {
                                $template->attachments()->create($item);
                            }
                        }
                    }
                }

                $this->info(ProjectEmailTemplate::count());
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
