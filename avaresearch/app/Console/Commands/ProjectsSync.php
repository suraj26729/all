<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UserDetail;
use App\Models\Project;
use DB;

class ProjectsSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:projects';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Projects';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/jobs.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');
                DB::statement("TRUNCATE projects");
                DB::statement("ALTER TABLE projects AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    // Skip first row (Remove below comment if you want to skip the first row)
                    // if ($i == 0) {
                    //     $i++;
                    //     continue;
                    // }
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                //$this->info(json_encode($importData_arr[1][8]));

                // Insert to MySQL database
                $data = [];
                $detail_data = [];
                foreach ($importData_arr as $key => $item) {
                    if ($item[11] == 'Administrator')
                        $staff = UserDetail::find(1);
                    else
                        $staff = UserDetail::where('first_name', 'LIKE', '%' . $item[11] . '%')->first();

                    $data[$key] = [
                        'id' => $item[0],
                        'job_number' => $item[1],
                        'job_name' => $item[2],
                        'subject' => $item[3],
                        'description' => strip_tags($item[4]),
                        'client_id' => $item[5] = 0 ? null : $item[5],
                        'location' => $item[7],
                        'contact_phone' => $item[8],
                        'contact_email' => $item[9],
                        'venue_email' => $item[10],
                        'created_by' => $staff != null ? $staff->user_id : 0,
                        'received_date' => $item[14] != 'NULL' && $item[14] != '' ? $item[14] : null,
                        'start_date' => $item[15] != 'NULL' && $item[15] != '' ? $item[15] : null,
                        'finish_date' => $item[16] != 'NULL' && $item[16] != '' ? $item[16] : null,
                        'validation_report' => $item[13],
                        'signed_sheets' => $item[17] == 'yes' ? 1 : 0
                    ];
                    if ($item[12] != '0000-00-00') $data[$key]['created_at'] = $item[12];
                }

                if (count($data)) {
                    foreach ($data as $key => $item) {
                        $project = Project::create($item);
                    }
                }

                $this->info(Project::count());
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
