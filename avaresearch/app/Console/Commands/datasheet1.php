<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\datasheet;

class datasheet1 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'datasheet:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'datasheet Upload';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file =storage_path("app/public/csv/sheettesting01.csv");
        if ($handle = fopen ($file, 'r' )) {
        while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {
            $data1['id'] =$data[0];
            $data1['name'] =$data[1];
            $data1['address'] =$data[2];
            $update =Datasheet::create($data1);
        }
        fclose ( $handle );
        }
/*
        $LegalIssue = new Datasheet ();
        $LegalIssue->id = $data [0];
        $LegalIssue->name = $data [1];
        $LegalIssue->address = $data [2];
        $LegalIssue->save();*/
    }
}
