<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Respondents\Respondent;
use App\Models\Respondents\RespondentChildren;
use App\Models\Respondents\RespondentDetail;
use App\Models\Respondents\RespondentHealth;
use App\Models\Respondents\RespondentHousehold;
use App\Models\Respondents\RespondentOccupation;
use App\Models\Respondents\RespondentProduct;
use App\Models\Respondents\RespondentService;
use App\Models\Respondents\RespondentSummary;

class UpdateRepondentIds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:respondentids';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Repondent IDS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ids = [];
        for($i = 37229; $i <= 37247; $i++){
            $ids[] = $i;
        }
        $fromId = 40450;
        $respondents = Respondent::with(['detail', 'houseHold', 'occupation', 'health', 'service', 'product', 'summary', 'childrens'])->withTrashed()->whereIn('id', $ids)->get();
        if($respondents){
            foreach($respondents as $item){
                $item->detail->update(['respondent_id' => $fromId]);
                $item->houseHold->update(['respondent_id' => $fromId]);
                $item->occupation->update(['respondent_id' => $fromId]);
                $item->health->update(['respondent_id' => $fromId]);
                $item->service->update(['respondent_id' => $fromId]);
                $item->product->update(['respondent_id' => $fromId]);
                $item->summary->update(['respondent_id' => $fromId]);
                if($item->childrens()->exists()){
                    $item->childrens()->update(['respondent_id' => $fromId]);
                }
                $item->update(['id' => $fromId]);
                $fromId += 1;
            }
        }
        $this->info($fromId);
    }
}
