<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Respondents\RespondentChildren;
use DB;

class RespondentChildrenSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondentchildren';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Respondent Children';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondent_children.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                // DB::statement("TRUNCATE respondent_childrens");
                // DB::statement("ALTER TABLE respondent_childrens AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                foreach ($importData_arr as $key => $item) {
                    $exists = RespondentChildren::where('id', $item[0])->withTrashed()->count();
                    if($exists==0){
                        $this->info($key);

                        $data = [
                            'id' => $item[0],
                            'respondent_id' => $item[1],
                            'child_name' => $item[2],
                            'gender' => $item[3],
                            'birth_month' => !empty($item[4]) ? $item[4] : null,
                            'birth_year' => !empty($item[5]) ? $item[5] : null,
                            'left_home' => isset($item[6]) && strtolower($item[6]) == 'yes' ? 1 : 0
                        ];

                        $respondent = RespondentChildren::create($data);
                    }
                }
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
