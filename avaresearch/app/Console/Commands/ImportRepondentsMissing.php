<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Respondents\Respondent;

class ImportRepondentsMissing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:missing-respondents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Missing Respondents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/Datasheet_10_03_2020.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    //$this->info( $num );
                    // Skip first row (Remove below comment if you want to skip the first row)
                   /* if ($i == 0 || $i==1) {
                        $i++;
                        continue;
                    }*/
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                        
                    }
                    $i++;
                }
                fclose($file);

                $data = [];
                $exts = 0;
                $migs = 0;
                foreach ($importData_arr as $key => $item) {
                    // $id = filter_var(trim($item[0]), FILTER_SANITIZE_NUMBER_INT);
                    $name = explode(' ', trim($item[0]));
                    //$this->info($item[1]);
                    $email = filter_var(trim($item[3]), FILTER_VALIDATE_EMAIL);
                    $email = $email ? filter_var(trim($item[3]),FILTER_SANITIZE_EMAIL) : "";
                     $mobile_phone = filter_var(trim($item[4]), FILTER_SANITIZE_NUMBER_INT);
                    $mobile_phone ='0'.$mobile_phone;
                    
                    // //$postcode = strtolower(trim($item[4]));
                    //$home_postcode = null;
                     $suburb_rec = "";
                    // if((int) $item[5] > 0) $home_postcode = (int) $item[5];
                    // else{
                     //$suburb01=$item[1];

                     $suburb = strtolower(trim($item[1]));
                     if($suburb != ''){
                          $suburb_rec = DB::select('select * from master_postcodes where suburb = ?', [$suburb]);

                        $suburb01 = $suburb_rec[0]->suburb;
                        $homepost = $suburb_rec[0]->postcode;
                     }else{
                        $suburb01 =null;
                        $homepost =null;
                     }
                    
                       // $this->info($suburb01);
                   
                    
                     $state = strtolower(trim($item[2]));
                     $state_rec = DB::select('select id from master_states where lower(name) = ?', [$state]);
                  
                     $exact_age = $item[6];
                     $avg_age = "";
                     if((int) $exact_age > 0){
                         $avg_age = (int) $exact_age;
                     } else {
                         $age_range = explode('-', $item[6]);
                         $avg_age = isset($age_range[0]) && is_numeric($age_range[0]) && isset($age_range[1]) && is_numeric($age_range[1]) ? round(($age_range[0]+$age_range[1])/2) : "";
                     }
                     $dob = $avg_age != "" ? date('Y-m-01', strtotime("-".$avg_age." years")) : null;
                     //$dob = null;
                     //$this->info($dob);
                    
                    // $occupation = strtolower(trim($item[14]));
                    // $occupation_rec = DB::select('select * from master_occupations where lower(name) = ?', [$occupation]);
                    // $industry = strtolower(trim($item[15]));
                    // $industry_rec = DB::select('select * from master_industries where lower(name) = ?', [$industry]);

                    // //$income_level = (strtolower(trim($item[21])) && strtolower(trim($item[21])) == '$150,000 - $199,000')?'$150,000 to $199,999' : null;
                    
                    // $cigarette_brands =  trim($item[17]) == "Tobacco" ? 'Regular smoker' : null;
                    // $brands_arr = [];
                    // if($item[15] == "Tailor made cigarettes [TMC]") $brands_arr[] = 'Tailor made cigarettes';
                    // if($item[16] == "Roll your own tobacco") $brands_arr[] = 'Roll Your Own';
                   
                    // $brands = count($brands_arr) ? implode(',', $brands_arr) : null; 

                    // // if(isset($industry_rec[0]))
                    // // $this->info(json_encode($industry_rec[0]->name));
                     $this->info($name[0]);
                    $data = [
                        // 'id' => is_numeric($id) ? (int) $id : null,
                         'first_name' => isset($name[0]) ? $name[0] : "",
                         'last_name' => isset($name[1]) ? $name[1] : "",
                        'dob' => $dob,
                        'gender' => !empty($item[5]) ? $item[5] : 'Male',
                         'email' => $email,
                         'password' => bcrypt('{AVA@2020!}'),
                         'isactive' => 1,
                         'created_by' => 1,
                         'created_at' => '2020-03-10 00:00:00',
                         'updated_at' => '2020-03-10 00:00:00'
                     ];
                    // $this->info($data);

                    // if(empty($email)) continue;

                    if(empty($email)) {
                        // $this->info($key);
                        continue;
                    }

                    $exists = Respondent::where('email', $email)->withTrashed()->count();
                    //$exists = Respondent::where('email', $email)->whereNull('dob')->where('created_at','2020-01-12')->withTrashed()->count();
                    $cnt=0;
                    
                   
                    //$this->info($suburb01);
                    if($exists == 0){
                        $this->info('new='.$email);
                        $migs += 1;
                         $respondent = Respondent::create($data);
                        if($respondent){
                            $details = [
                        //         'respondent_id' => $respondent->id,
                                'online_survey' => 0,
                                'state' => isset($state_rec[0]) ? $state_rec[0]->id : null,
                                'mobile_phone' => !empty($mobile_phone) ? $mobile_phone : null,
                                'home_postcode' => isset($suburb_rec[0]) ? $homepost : null,
                                'home_postsuburb' => isset($suburb_rec[0]) ? $suburb01 : null,
                             ];
                             $detail = $respondent->detail()->create($details);
        
                             $occupation = [
                                'respondent_id' => $respondent->id,
                        //         'occupation_id' => isset($occupation_rec[0]) ? $occupation_rec[0]->id : null,
                        //         'job_title' => $occupation,
                        //         'industry_id' => isset($industry_rec[0]) ? $industry_rec[0]->id : null,
                             ];
                             $occupation = $respondent->occupation()->create($occupation);
        
                             $health = [
                                'respondent_id' => $respondent->id,
                        //         'brands' => $brands,
                        //         'cigarette_brands' => $cigarette_brands
                            ];
                            $health = $respondent->health()->create($health);
        
                             $households = [
                               'respondent_id' => $respondent->id,
                        //         //'income_level' => $income_level,
                             ];
                            $_houseHold = $respondent->houseHold()->create($households);
        
                            $products = [
                                'respondent_id' => $respondent->id
                            ];
                            $product = $respondent->product()->create($products);
        
                            $services = [
                                'respondent_id' => $respondent->id
                            ];
                            $service = $respondent->service()->create($services);
        
                            $summaries = [
                                'respondent_id' => $respondent->id
                            ];

                            $summary = $respondent->summary()->create($summaries);
                        }
                    } else {
                        $exts += $exists ? 1 : 0;
                        // $respondent = Respondent::where('email', $email)->whereNull('dob')->where('created_at','2020-01-12')->withTrashed()->first();

                        // $exact_age = $item[10];
                        // $avg_age = "";
                        // if((int) $exact_age > 0){
                        //     $avg_age = (int) $exact_age;
                        // } else {
                        //     $age_range = explode('-', $item[8]);
                        //     $avg_age = isset($age_range[0]) && is_numeric($age_range[0]) && isset($age_range[1]) && is_numeric($age_range[1]) ? round(($age_range[0]+$age_range[1])/2) : "";
                        // }
                        // $dob = $avg_age != "" ? date('Y-m-01', strtotime("-".$avg_age." years")) : null;

                        // $this->info($respondent->id.'-'.$dob);
                       
                        // if($respondent){
                        //     $dob_updt = $respondent->update(['dob' => $dob]);
                        // }
                    }
                    //$this->info($cnt);
                    // $this->info($brands);
                    // $this->info(json_encode($details));
                }
                $this->info('Exts'.$exts);
                $this->info('New'.$migs);
                //$this->info(json_encode($data));
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
