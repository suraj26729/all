<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Respondents\RespondentSummary;
use App\Models\Masters\MasterPreferredResearchLocation;
use DB;

class RespondentSummarySync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondentsummary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import respondent summary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondent_summary.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                // DB::statement("TRUNCATE respondent_summaries");
                // DB::statement("ALTER TABLE respondent_summaries AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                foreach ($importData_arr as $key => $item) {
                    $exists = RespondentSummary::where('respondent_id', $item[0])->withTrashed()->count();
                    if($exists==0){
                    //if ($item[0] > 35889) break;
                    $this->info($key);

                    $preferred_research_location = isset($item[1]) && $item[1] != 'NULL' && !empty($item[1]) ? $item[1] : null;
                    if ($preferred_research_location != null) {
                        $preferred_research_location = explode(',', $preferred_research_location);
                        $_preferred_research_location = [];
                        foreach ($preferred_research_location as $_item) {
                            $data = MasterPreferredResearchLocation::where('name', $_item)->first();
                            if ($data != null) $_preferred_research_location[] = $data->id;
                        }
                        $preferred_research_location = count($_preferred_research_location) ? implode(',', $_preferred_research_location) : null;
                    }

                    $created_at = isset($item[6]) && $item[6] != 'NULL' && $item[6] != '' && $item[6] != '0000-00-00 00:00:00' ? $item[6] : date('Y-m-d H:i:s');
                    $updated_at = isset($item[7]) && $item[7] != 'NULL' && $item[7] != '' && $item[7] != '0000-00-00 00:00:00' ? $item[7] : date('Y-m-d H:i:s');

                    $data = [
                        'respondent_id' => $item[0],

                        'preferred_research_location' => $preferred_research_location,

                        'availability_times' => !empty($item[2]) && $item[2] != 'NULL' ? $item[2] : null,
                        'session_type' => !empty($item[3]) && $item[3] != 'NULL' ? $item[3] : null,
                        'preferred_contact_method' => !empty($item[4]) && $item[4] != 'NULL' ? $item[4] : null,
                        'additional_notes' => !empty($item[5]) && $item[5] != 'NULL' ? $item[5] : null,

                        'created_at' => $created_at,
                        'updated_at' => $updated_at
                    ];

                    $respondent = RespondentSummary::create($data);
                    }
                }
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
