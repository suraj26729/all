<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Masters\MasterBirthCountry;
use App\Models\Respondents\RespondentHousehold;
use DB;

class RespondentHouseHoldSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondenthousehold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import House Hold';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondent_household.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                //DB::statement("TRUNCATE respondent_households");
                //DB::statement("ALTER TABLE respondent_households AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                foreach ($importData_arr as $key => $item) {
                    $exists = RespondentHousehold::where('respondent_id', $item[0])->withTrashed()->count();
                    if($exists==0){
                    //if ($item[0] > 35889) break;
                    $this->info($key);

                    $birth_country = isset($item[7]) && !empty($item[7]) ? $item[7] : null;
                    $birth_country = MasterBirthCountry::where('name', $birth_country)->first();
                    $birth_country = $birth_country != null ? $birth_country->id : null;

                    $original_country = isset($item[9]) && !empty($item[9]) ? $item[9] : null;
                    $original_country = MasterBirthCountry::where('name', $original_country)->first();
                    $original_country = $original_country != null ? $original_country->id : null;

                    $children = isset($item[12]) && $item[12] != 'NULL' && !empty($item[12]) ? $item[12] : 'No';
                    $children = $children == 'Yes' ? 1 : 0;

                    $children_allowed = isset($item[13]) && $item[13] != 'NULL' && !empty($item[13]) ? $item[13] : 'No';
                    $children_allowed = $children_allowed == 'Yes' ? 1 : 0;

                    $children_seperate = isset($item[14]) && $item[14] != 'NULL' && !empty($item[14]) ? $item[14] : 'No';
                    $children_seperate = $children_seperate == 'Yes' ? 1 : 0;

                    $created_date = isset($item[15]) && $item[15] != 'NULL' && $item[15] != '' ? $item[15] : date('Y-m-d H:i:s');

                    $updated_at = isset($item[16]) && $item[16] != 'NULL' && $item[16] != '' ? $item[16] : date('Y-m-d H:i:s');

                    $data = [
                        'respondent_id' => $item[0],
                        'marital_status' => $item[1],
                        'home_ownership' => $item[2],
                        'household_type' => $item[3],
                        'income_level' => $item[4],
                        'residence_type' => $item[5],
                        'residency_status' => $item[6],
                        'birth_country' => $birth_country,
                        'other_country' => $item[8],
                        'original_country' => $original_country,
                        'ethnic_background' => $item[10],
                        'pets' => $item[11],
                        'children' => $children,
                        'children_allowed' => $children_allowed,
                        'children_seperate' => $children_seperate,
                        'created_at' => $created_date,
                        'updated_at' => $updated_at
                    ];

                    $respondent = RespondentHousehold::create($data);
                    }
                }
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
