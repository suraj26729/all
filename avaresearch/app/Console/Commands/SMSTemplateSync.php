<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\SMSTemplate;
use DB;

class SMSTemplateSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:smstemplates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import SMS Templates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/sms_template.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                DB::statement("DELETE from s_m_s_templates");
                DB::statement("ALTER TABLE s_m_s_templates AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    // Skip first row (Remove below comment if you want to skip the first row)
                    // if ($i == 0) {
                    //     $i++;
                    //     continue;
                    // }
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                //$this->info(json_encode($importData_arr[1][8]));

                // Insert to MySQL database
                $data = [];
                $attach_data = [];
                foreach ($importData_arr as $key => $item) {
                    $data[$key] = [
                        'id' => $item[0],
                        'title' => $item[1],
                        'message' => $item[2],
                        'created_by' => 1
                    ];
                }

                if (count($data)) {
                    foreach ($data as $key => $item) {
                        $template = SMSTemplate::create($item);
                    }
                }

                $this->info(SMSTemplate::count());
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
