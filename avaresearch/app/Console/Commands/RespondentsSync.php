<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Respondents\Respondent;
use DB;

class RespondentsSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:respondents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Respondents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $filename = storage_path('app/public/csv/respondents.csv');
            if (!file_exists($filename) || !is_readable($filename)) {
                $this->info('File not found' . $filename);
                return false;
            } else {
                $this->info('File is found');

                //DB::statement("TRUNCATE respondents");
                //DB::statement("ALTER TABLE respondents AUTO_INCREMENT =  1");

                $file = fopen($filename, "r");
                $importData_arr = [];
                $i = 0;
                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);
                    // Skip first row (Remove below comment if you want to skip the first row)
                    // if ($i == 0) {
                    //     $i++;
                    //     continue;
                    // }
                    for ($c = 0; $c < $num; $c++) {
                        $importData_arr[$i][] = $filedata[$c];
                    }
                    $i++;
                }
                fclose($file);

                // $this->info(json_encode($importData_arr[1]));

                // Insert to MySQL database
                $data = [];
                $attach_data = [];
                foreach ($importData_arr as $key => $item) {
                    $exists = Respondent::where('id', $item[0])->withTrashed()->count();
                    if($exists==0){
                    $this->info($key);
                    $dob = !empty($item[3]) && $item[3] != '0000-00-00' ? $item[3] : null;

                    $do_not_call = isset($item[8]) && $item[8] != 'NULL' && $item[8] != '' ? $item[8] : null;
                    $do_not_call = $do_not_call == 'Yes' ? 1 : 0;

                    $one_off_resp = isset($item[9]) && $item[9] != 'NULL' && $item[9] != '' ? $item[9] : null;
                    $one_off_resp = $one_off_resp == 'Yes' ? 1 : 0;

                    $has_accent = isset($item[10]) && $item[10] != 'NULL' && $item[10] != '' ? $item[10] : null;
                    $has_accent = $has_accent == 'Yes' ? 1 : 0;

                    $do_no_show = isset($item[11]) && $item[11] != 'NULL' && $item[11] != '' ? $item[11] : null;
                    $do_no_show = $do_no_show == 'Yes' ? 1 : 0;

                    $stop_often_email = isset($item[12]) && $item[12] != 'NULL' && $item[12] != '' ? $item[12] : null;
                    $stop_often_email = $stop_often_email == 'Yes' ? 1 : 0;

                    $excellent_candidate = isset($item[13]) && $item[13] != 'NULL' && $item[13] != '' ? $item[13] : null;
                    $excellent_candidate = $excellent_candidate == 'Yes' ? 1 : 0;

                    $wrong_number = isset($item[14]) && $item[14] != 'NULL' && $item[14] != '' ? $item[14] : null;
                    $wrong_number = $wrong_number == 'Yes' ? 1 : 0;

                    $keen_candidate = isset($item[15]) && $item[15] != 'NULL' && $item[15] != '' ? $item[15] : null;
                    $keen_candidate = $keen_candidate == 'Yes' ? 1 : 0;

                    $isactive = isset($item[16]) && $item[16] != 'NULL' && $item[16] != '' ? $item[16] : 0;
                    $isactive = $isactive == 'Yes' ? 1 : 0;

                    $delete_reason = isset($item[17]) && $item[17] != 'NULL' && $item[17] != '' ? $item[17] : null;

                    $created_date = isset($item[18]) && $item[18] != '0000-00-00' && $item[18] != 'NULL' && $item[18] != '' ? $item[18] : date('Y-m-d H:i:s');
                    $updated_date = isset($item[19]) && $item[19] != '0000-00-00' && $item[19] != 'NULL' && $item[19] != '' ? $item[19] : date('Y-m-d H:i:s');

                    $data = [
                        'id' => $item[0],
                        'first_name' => $item[1],
                        'last_name' => $item[2],
                        'dob' => $dob,
                        'gender' => $item[4],
                        'email' => $item[5],
                        'additional_email' => $item[6],
                        'password' => bcrypt($item[7]),
                        'do_not_call' => $do_not_call,
                        'one_off_resp' => $one_off_resp,
                        'has_accent' => $has_accent,
                        'do_no_show' => $do_no_show,
                        'stop_often_email' => $stop_often_email,
                        'excellent_candidate' => $excellent_candidate,
                        'wrong_number' => $wrong_number,
                        'keen_candidate' => $keen_candidate,
                        'isactive' => $isactive,
                        'delete_reson' => $delete_reason,
                        'created_at' => $created_date,
                        'updated_at' => $updated_date,
                        'created_by' => 1
                    ];
                    $respondent = Respondent::create($data);
                    }
                }

                // if (count($data)) {
                //     foreach ($data as $key => $item) {
                //         $respondent = Respondent::create($item);
                //     }
                // }

                $this->info(Respondent::count());
            }
        } catch (Exception $e) {
            $this->info($e->getMessage());
        }
    }
}
