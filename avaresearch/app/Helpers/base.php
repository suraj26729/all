<?php

use App\Models\Project\ProjectAttendeeDocument;
use App\Models\Project\ProjectBankDocument;

if (!function_exists('item_select')) {
    function q_item_select($item, $option, $filterValues)
    {
        if (isset($filterValues['filter_' . $item->key])) {
            if (is_array($filterValues['filter_' . $item->key]) && isset($item->multiple) && $item->multiple == true && in_array($option, $filterValues['filter_' . $item->key])) return 'selected';
            else if ($option == $filterValues['filter_' . $item->key]) return 'selected';
        }
    }
}

if (!function_exists('item_check')) {
    function q_item_check($item, $option, $filterValues)
    {
        if (isset($filterValues['filter_' . $item->key]) && is_array($filterValues['filter_' . $item->key])) {
            if (in_array($option, $filterValues['filter_' . $item->key])) return 'checked';
        }
    }
}

if (!function_exists('list_all_attendee_documents')) {
    function list_all_attendee_documents($project_id)
    {
        return ProjectAttendeeDocument::where('project_id', $project_id)->select('document_name')->get();
    }
}

if (!function_exists('list_all_bank_documents')) {
    function list_all_bank_documents($project_id)
    {
        return ProjectBankDocument::where('project_id', $project_id)->select('document_name')->get();
    }
}
