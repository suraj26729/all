<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Datasheet extends Model
{
    protected $fillable = ['id','name','address'];
}
