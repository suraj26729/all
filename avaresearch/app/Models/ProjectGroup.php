<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ProjectGroup extends Model
{
    use SoftDeletes;
    protected $guarded = ['uuid'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'job_id', 'id');
    }

    public function location()
    {
        return $this->belongsTo('App\Models\Masters\MasterState', 'location', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function job_events()
    {
        return $this->hasMany('App\Models\Project\JobEvent', 'group_id', 'id');
    }
}
