<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class ProjectSMSTemplate extends Model
{
    use SoftDeletes;
    protected $guarded = ['uuid'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'job_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\ProjectGroup', 'group_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
