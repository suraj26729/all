<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectAttendeeDocument extends Model
{
    protected $guarded = ['id'];

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'job_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
