<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectPoint extends Model
{
    protected $guarded = ['uuid'];

    public function respondent()
    {
        return $this->belongsTo('App\Models\Respondents\Respondent', 'respondent_id', 'id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }
}
