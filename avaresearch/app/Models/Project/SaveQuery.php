<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class SaveQuery extends Model
{
    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }


    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
