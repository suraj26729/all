<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectEmailTemplateAttachment extends Model
{
    protected $guarded = ['id'];
}
