<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class JobEvent extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id', 'id');
    }

    public function template()
    {
        return $this->belongsTo('App\Models\EmailTemplate', 'template_id', 'id');
    }

    public function projectTemplate()
    {
        return $this->belongsTo('App\Models\Project\ProjectEmailTemplate', 'template_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'assigned_user', 'id')->with('profile');
    }

    public function respondent()
    {
        return $this->belongsTo('App\Models\Respondents\Respondent', 'respondent_id', 'id');
    }
}
