<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class TrackingTime extends Model
{
    protected $fillable = ['staff_id','project_id','start_time'];
}
