<?php

namespace App\Models\Respondents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RespondentHousehold extends Model
{
    //
    use SoftDeletes;

    protected $guarded = ['id'];
}
