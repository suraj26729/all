<?php

namespace App\Models\Respondents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RespondentDetail extends Model
{
    //
    use SoftDeletes;

    protected $guarded = ['id'];
}
