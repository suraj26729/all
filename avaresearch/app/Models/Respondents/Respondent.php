<?php

namespace App\Models\Respondents;


use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;
use App\Models\Respondents\RespondentDetail;
use App\Models\Respondents\RespondentHousehold;
use App\Models\Respondents\RespondentChildren;
use App\Models\Respondents\RespondentOccupation;

use App\Notifications\RespondentResetPasswordNotification;

class Respondent extends Authenticatable
{
    //
    use Notifiable, SoftDeletes;
    protected $guarded = ['uuid'];

    protected $guard = 'respondent';

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    public function detail()
    {
        return $this->hasOne(RespondentDetail::class, 'respondent_id', 'id');
    }

    public function houseHold()
    {
        return $this->hasOne(RespondentHousehold::class, 'respondent_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany(RespondentChildren::class, 'respondent_id', 'id');
    }

    public function occupation()
    {
        return $this->hasOne(RespondentOccupation::class, 'respondent_id', 'id');
    }

    public function health()
    {
        return $this->hasOne(RespondentHealth::class, 'respondent_id', 'id');
    }

    public function service()
    {
        return $this->hasOne(RespondentService::class, 'respondent_id', 'id');
    }

    public function product()
    {
        return $this->hasOne(RespondentProduct::class, 'respondent_id', 'id');
    }

    public function summary()
    {
        return $this->hasOne(RespondentSummary::class, 'respondent_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
    public function deletedBy()
    {
        return $this->belongsTo('App\User', 'deleted_by', 'id');
    }

    public function job_events()
    {
        return $this->hasMany('App\Models\Project\JobEvent', 'respondent_id', 'id')->orderBy('created_date', 'DESC');
    }

    public function qualified_job_events()
    {
        return $this->hasMany('App\Models\Project\JobEvent', 'respondent_id', 'id')->whereIn('name', ['Screened OK', 'Qualified', 'Screened OK – Quotas Full'])->orderBy('created_date', 'DESC');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new RespondentResetPasswordNotification($token));
    }
}
