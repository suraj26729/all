<?php

namespace App\Models\Respondents;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RespondentSummary extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];
}
