<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Project extends Model
{
    use SoftDeletes;
    protected $guarded = ['uuid'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id', 'id')->with('profile');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id')->with('profile');
    }

    public function groups()
    {
        return $this->hasMany('App\Models\ProjectGroup', 'job_id', 'id');
    }

    public function job_events()
    {
        return $this->hasMany('App\Models\Project\JobEvent', 'project_id', 'id');
    }
}
