<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;

class MasterEthnicBackground extends Model
{
    protected $guarded = ['id'];
}
