<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;

class MasterPreferredResearchLocation extends Model
{
    protected $guarded = ['id'];
}
