<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;

class MasterClosestCity extends Model
{
    protected $guarded = ['id'];
}
