<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;

class MasterResidencyStatus extends Model
{
    protected $guarded = ['id'];
}
