<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplateAttachment extends Model
{
    protected $guarded = ['id'];
}
