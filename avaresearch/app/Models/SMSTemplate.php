<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class SMSTemplate extends Model
{
    use SoftDeletes;
    protected $guarded = ['uuid'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Str::uuid();
        });
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
}
