<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class SearchRespondents implements FromCollection, WithHeadings
{
    use Exportable;

    protected $data, $args;

    public function __construct($args)
    {
        $data = session('table_data');
        $this->data = $data;
        $this->args = $args;
    }

    public function collection()
    {
        $result = [];
        foreach ($this->data as $row_i => $row) {
            if (in_array($row->id, $this->args['ids'])) {
                foreach ($row as $key => $item) {
                    $result[$row_i][$key] = strip_tags($item);
                }
            }
        }
        return collect($result);
    }

    public function headings(): array
    {
        $row = $this->data[0];
        $keys = array_keys((array) $row);
        return $keys;
    }
}
