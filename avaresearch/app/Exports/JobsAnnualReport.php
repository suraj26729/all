<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use App\Models\Project;

class JobsAnnualReport implements FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {
        $result = [];
        $projects = Project::orderBy('job_number', 'ASC')->get();

        foreach ($projects as $key => $row) {
            if ($row->job_number) {
                $result[$key]['job_number'] = $row->job_number;
                $result[$key]['job_received_date'] = $row->received_date;
                $result[$key]['job_name'] = $row->received_date;
                $result[$key]['job_description'] = $row->description;
                $result[$key]['number_of_groups'] = count($row->groups);
                $result[$key]['number_of_in_depths'] = count($row->groups);
                $result[$key]['company_name'] = $row->client ? $row->client->profile->company_name : '';
                $result[$key]['client_name'] = $row->client ? $row->client->profile->first_name . ' ' . $row->client->profile->last_name : '';
                $result[$key]['start_date'] = date('Y-m-d', strtotime($row->start_date));
                $result[$key]['finish_date'] =  date('Y-m-d', strtotime($row->finish_date));
                $result[$key]['signed_sheets'] = $row->signed_sheets == 1 ? 'Yes' : 'No';
            }
        }

        return collect($result);
    }

    public function headings(): array
    {
        return [
            'Job Number', 'Job received date', 'Job Name', 'Job Description', 'Number of Groups',
            'Number of in-depths', 'Company Name', 'Client Name', 'Start Date', 'Finish Date', 'Signed sheets received'
        ];
    }
}
