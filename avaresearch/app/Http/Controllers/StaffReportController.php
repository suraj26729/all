<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project\JobEvent;
use App\Models\Project;
use App\Models\Respondents\Respondent;
use App\User;

class StaffReportController extends Controller
{
    public function index(){
        $staffs = User::where('type', 'staff')->where('isactive', 1)->get();
        return view('staff_report.index', compact('staffs'));
    }

    public function getData(Request $request){
        $date = date('Y-m-d', strtotime($request->date));
        $staff = $request->staff;

        $job_events = JobEvent::where('name', 'Screened OK')->where('created_date', $date)->where('assigned_user', $staff)->get();

        $job_ids = [];
        $respondent_ids = [];
        $job_respondents = [];
        $result = [];

        foreach($job_events as $event){ 
            $job_ids[] = $event->project_id; $respondent_ids[] = $event->respondent_id;
            $job_respondents[$event->project_id][] = $event->respondent_id;
        }

        $job_ids = array_unique($job_ids);
        $respondent_ids = array_unique($respondent_ids);

        $total_jobs = count($job_ids);
        $total_respondents = count($respondent_ids);

        if(count($job_respondents)){
            foreach($job_respondents as $project_id => $respondent_ids){
                $project = Project::find($project_id);
                if($project){
                    $respondents = [];
                    foreach($respondent_ids as $respondent_id){
                        $respondent = Respondent::find($respondent_id);
                        if($respondent){
                            $respondents[] = ['respondent_name' => $respondent->first_name.' '.$respondent->last_name, 'respondent_uuid' => $respondent->uuid];
                        }
                    }
                    $result[] = ['project_number' => $project->job_number, 'project_name' => $project->job_name, 'project_uuid' => $project->uuid, 'respondents' => $respondents];
                }
            }
        }

        // dd($result);

        return response()->json(view('staff_report.report', compact('total_jobs', 'total_respondents', 'result'))->render());
    }
}
