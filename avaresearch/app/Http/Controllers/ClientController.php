<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\UserDetail;
use Auth;
use Yajra\Datatables\Datatables;
use App\Models\Project;
use App\Rules\DomainOnly;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('clients.index');
    }

    public function DTData()
    {
        $clients = User::leftjoin('user_details', 'user_details.user_id', '=', 'users.id')
            ->select(
                'users.id',
                'users.uuid',
                'users.email',
                'users.isactive',
                'user_details.first_name',
                'user_details.last_name',
                'user_details.address',
                'user_details.contact_no',
                'users.created_by',
                'users.created_at'
            )->where('users.type', 'client');
        return Datatables::of($clients)
            ->addColumn('image', function ($client) {
                return '<div class="d-flex">
                <div class="avatar avatar-md mr-3 mb-2 mt-1">
                  <span
                    class="avatar-letter avatar-letter-' . strtolower(substr($client->profile->first_name, 0, 1)) . ' avatar-md circle"
                  ></span>
                </div>
                <div>
                    <div>
                    <a href="' . route('clients.edit', $client->uuid) . '">
                        <strong>' . $client->profile->first_name . ' ' . $client->profile->last_name . '</strong>
                    </a>
                    </div>
                    <small>' . $client->email . '</small>
                </div>
              </div>';
            })
            ->addColumn('created_user_name', function ($client) {
                if ($client->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $client->createdBy->uuid) . '">
                    ' . $client->createdBy->profile->first_name . ' ' . $client->createdBy->profile->last_name . '
                </a>';
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($client) {
                return $client->created_at->format('d-m-Y');
            })
            ->addColumn('actions', function ($client) {
                return '<td>
                    <a href="' . route('clients.edit', $client->uuid) . '"><i class="icon-pencil mr-3"></i></a>
                    <a href="javascript:void(0);" class="delete_btn" data-id="' . $client->id . '" data-name="' . $client->profile->first_name . '"><i class="icon-trash"></i></a>
                </td>';
            })->rawColumns(['image', 'actions', 'created_user_name'])->make(true);
    }

    public function create()
    {
        return view('clients.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'company_name' => 'required|max:255',
            'contact_no' => 'required|numeric',
            'email' => 'required|unique:users|email|max:255',
            //'password' => 'required|min:5|max:10|confirmed',
            //'password_confirmation' => 'required|min:5|max:10',
            'address' => 'required'
        ];
        $this->validate($request, $rules, ['password.confirmed' => 'Password and Confirm Password not matched']);

        $args = $request->only(['email', 'password']);
        $args['type'] = 'client';
        if($args['password'] == null){
            $args['password'] = bcrypt("AVA@2020!");  
        }else{
            $args['password'] = bcrypt($request->password);
        }
        
        $username = explode('@', $request->email);
        $username = $username[0];
        $user = User::where('username', $username)->exists();
        if ($user) $username = $username . rand(1, 99);
        $args['username'] = $username;
        $args['created_by'] = auth()->id();

        $user = User::create($args);
        if ($user) {
            $args = $request->except(['email', 'password', 'password_confirmation']);
            $path = 'avatar-images/' . date('Y') . '/' . date('m');
            if ($request->file('image')) {
                $uploadedFile = $request->file('image');
                $filename = time() . $uploadedFile->getClientOriginalName();
                Storage::disk('public')->putFileAs($path, $uploadedFile, $filename);
                $args['image'] = $path . '/' . $filename;
            }


            $args['user_id'] = $user->id;
            $args['created_by'] = auth()->id();

            $detail = UserDetail::create($args);
            if ($detail)
                return redirect()->back()->withSuccess('Client created successfully!');
            else
                return redirect()->back()->withErrors(['message' => 'Problem while creating client profile. Please try again']);
        } else {
            return redirect()->back()->withErrors(['message' => 'Problem while creating client. Please try again']);
        }
    }

    public function edit($uuid)
    {
        $user = User::with(['profile'])->where('uuid', $uuid)->first();
        return view('clients.edit', compact('user'));
    }

    public function update(Request $request, User $client)
    {
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'company_name' => 'required|max:255',
            'contact_no' => 'required|numeric',
            'email' => 'required|email|max:255|unique:users,email,' . $client->id,
            'address' => 'required'
        ];
        $this->validate($request, $rules, []);

        $client->update(['email' => $request->email]);

        $data = $request->except(['email', 'image']);

        $path = 'avatar-images/' . date('Y') . '/' . date('m');
        $url = '';
        if ($request->file('image')) {
            $uploadedFile = $request->file('image');
            $filename = time() . $uploadedFile->getClientOriginalName();
            Storage::disk('public')->putFileAs($path, $uploadedFile, $filename);
            $url = $path . '/' . $filename;
            $data['image'] = $url;
        }

        $client->profile->update($data);

        return redirect()->back()->withSuccess('Client details updated successfully!');
    }

    public function destroy(User $client)
    {
        $client->profile->delete();
        $client->delete();
        return response()->json(1);
    }

    public function getProjects($uuid)
    {
        $user = User::where('uuid', $uuid)->first();
        return view('clients.projects', compact('user'));
    }

    public function DTProjectData($uuid)
    {
        $client = User::where('uuid', $uuid)->first();
        $projects = Project::where('client_id', $client->id)->leftjoin('users', 'users.id', '=', 'projects.created_by')
            ->leftjoin('user_details', 'user_details.user_id', '=', 'users.id')
            ->select(
                'projects.id',
                'projects.uuid',
                'projects.job_number',
                'projects.job_name',
                'projects.subject',
                'projects.description',
                'projects.location',
                'projects.contact_phone',
                'projects.contact_email',
                'projects.venue_email',
                'projects.created_by',
                'projects.created_at',
                'user_details.first_name',
                'user_details.last_name'
            )->orderby('projects.job_name','ASC');
        return Datatables::of($projects)
            ->addColumn('project_name', function ($project) {
                return '<a href="' . route('projects.edit', $project->uuid) . '">
                    ' . $project->job_name  . '
                </a>';
            })
            ->addColumn('excerpt_desc', function ($project) {
                return str_limit($project->description, 40, '...');
            })
            ->addColumn('creator_name', function ($project) {
                if ($project->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $project->createdBy->uuid) . '">
                    ' . $project->createdBy->profile->first_name . ' ' . $project->createdBy->profile->last_name . '
                </a>';
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($project) {
                return $project->created_at->format('d-m-Y');
            })
            ->rawColumns(['project_name', 'creator_name'])->make(true);
    }
}
