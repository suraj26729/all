<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Respondents\Respondent;
use App\Models\Respondents\RespondentDetail;
use App\Models\Respondents\RespondentHousehold;
use App\Models\Respondents\RespondentOccupation;
use App\Models\Respondents\RespondentHealth;
use App\Models\Respondents\RespondentService;
use App\Models\Respondents\RespondentProduct;
use App\Models\Respondents\RespondentSummary;
use App\Models\Masters\MasterPostcode;
use DB;
use Exception;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('leads.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            //
            // $rules = ['lead_form_id' => 'required'];
            // $this->validate($request, $rules, []);

            //dd($request->all());
            $json = json_decode(file_get_contents("https://graph.facebook.com/v2.10/522061471472551/accounts?access_token=EAAJ39JXmQxgBALBeGdXoHh2zQr0jIoYAT3EmgCJm8abet8jgIUFUAyobS5yU7AknvX7coc2uDA99N9iBxZAI27xJhWNuVpZBfbp690rZCg9OxhcvEz8j6ZC3htSkZBZC5ggVeIZAiXuSn8QyJGZAVdjQIfL8lRKQ8jwZD"), true); 
                
            $access_token = $json['data'][0]['access_token'];
        

            $form_id = $request->form_id;
            $form_name = $request->form_name;


           // $leads = json_decode(, true);

            $content = @file_get_contents("https://graph.facebook.com/v2.10/".$form_id."/leads?access_token=".$access_token."&fields=created_time%2Cid%2Cad_id%2Cform_id%2Cfield_data");

            if (strpos($http_response_header[0], "200")) 
            { 
                $leads = json_decode($content, true);
            }
            else { 
                  return redirect()->back()->with(['error' => 'Enter The Valid Leads Form ID']);
            } 
            //dd($leads);
            //$leads = json_decode($form_leads,true);


            foreach ($leads['data'] as $key => $value) {              
                foreach ($value['field_data'] as $key1 => $value1) {
                    $fileds_arr[$key][$value1['name']] = $value1['values'][0];
                }
            }
            
            //dd($fileds_arr);

            $existing_emails = [];  $respondents = [];
            foreach ($fileds_arr as $key => $value) 
            { 
                $exits = Respondent::where('email',$value['email'])->first();
              
                if($exits)
                { 
                   
                    $existing_emails[] = $value['email'];
                    
                }
                else{             
                     
                    $data = [];$detail=[];
                    if(isset($value['full_name']))
                    {
                        $full_name = trim($value['full_name']);
                        $res_name =  explode(" ",$full_name,2);

                        $data['first_name'] = $res_name['0'];
                        $data['last_name'] = $res_name['1'];
                    }else{
                        $data['first_name'] = isset($value['first_name'])?$value['first_name'] : '';
                         $data['last_name'] = isset($value['last_name'])?$value['last_name']:'';
                    }

                    $data['dob'] = isset($value['date_of_birth'])?date('Y-m-d',strtotime($value['date_of_birth'])):'';
                    $data['password'] = bcrypt('ava@123');
                    $data['email'] = isset($value['email'])?$value['email']:'';            
                    $data['gender'] = isset($value['gender'])?ucfirst($value['gender']):'';                 
                    $data['created_by'] = auth()->id();
                    

                    
                    /*$citystate =MasterPostcode::where('suburb', $value['city'])
                                ->select('state_id','postcode')->first();
                            
                    $detail['state'] = isset($citystate->state_id)?$citystate->state_id:'';*/
                    if(isset($value['state']))
                    {
                        $state_name = strtolower($value['state']);

                        if($value['state'] == 'NSW' || $state_name == 'new south wales')
                        {
                        $detail['state'] = 1;
                        }
                        elseif($value['state'] == 'QLD' || $state_name == 'queensland')
                        {
                        $detail['state'] = 2;
                        }
                        elseif($value['state'] == 'SA' || $state_name == 'south australia')
                        {
                        $detail['state'] = 3;
                        }
                        elseif($value['state'] == 'TAS' || $state_name == 'tasmania')
                        {
                        $detail['state'] = 4;
                        }
                        elseif($value['state'] == 'VIC' || $state_name == 'victoria')
                        {
                        $detail['state'] = 5;
                        }
                        elseif($value['state'] == 'WA' || $state_name == 'western australia')
                        {
                        $detail['state'] = 6;
                        }
                        elseif($value['state'] == 'ACT' || $state_name == 'australian capital Territory')
                        {
                        $detail['state'] = 7;
                        }
                        elseif($value['state'] == 'NT' || $state_name == 'northern territory')
                        {
                        $detail['state'] = 8;
                        }
                        else
                        {
                            $detail['state'] = '';
                        }
                    }

                    if(isset($value['phone_number'])){
                     $detail['mobile_phone']=   preg_replace('/[+]61/', '0', $value['phone_number'], 1);
                    }else{
                        $detail['mobile_phone']= '';
                    }

                     
                    $detail['closest_large_city'] = isset($value['city'])?$value['city']:'';         
                    $detail['home_postcode'] = isset($value['post_code'])?$value['post_code']:'';               
                    $detail['home_postsuburb'] = isset($value['home_postsuburb'])?$value['home_postsuburb']:'';               
                    $detail['source_name'] = 'Facebook Leads';
                    $detail['hear_about'] = $form_name;
                    $detail['online_survey'] = 0;
                    
                    $household = [];
                    $household['residency_status'] = isset($value['residency_status'])?$value['residency_status']:null;
                    $household['ethnic_background'] = isset($value['ethnic_background'])?$value['ethnic_background']:null;
                    $occupation = [];
                    $occupation['job_title'] = isset($value['job_title'])?$value['job_title']:'';
                    $health = [];
                    $service = [];
                    $product = [];
                    $summary = [];                    
                    $respondents[] = $value['email'];
                     $respondent = Respondent::create($data);
                     if ($respondent) {
                        $respondents[] = $value['email'];
                        $detail = $respondent->detail()->create($detail);
                        $_houseHold = $respondent->houseHold()->create($household);
                        $occupation = $respondent->occupation()->create($occupation);
                        $health = $respondent->health()->create($health);
                        $service = $respondent->service()->create($service);
                        $product = $respondent->product()->create($product);
                         $summary = $respondent->summary()->create($summary);
                         DB::commit();
                       
                     } 
                          
                }
            }
       
            if(count($existing_emails) == count($fileds_arr) ){

                return redirect()->back()->with(['warning' => 'Respondent Already Exist in Database', 'exist' => count($existing_emails)]);
            }
                return redirect()->back()->with(['success' => 'Leads imported sucessfully','respondents'=>count($respondents)/2 , 'exist' => count($existing_emails)]);
            
            
        }catch (Exception $ex) {
            DB::rollBack();
            //return response()->json($ex, 500);
            return redirect()->back()->with(['error' => $ex]);
        }         

        

                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
