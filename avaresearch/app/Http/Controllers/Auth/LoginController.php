<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:respondent')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        if (env('OFFICE_IP_LOCK') && env('OFFICE_IP') != request()->ip() && $user->type == 'staff') {
            Auth::logout();
            return redirect('login')->withErrors(['office_ip' => 'Access is Restricted']);
        }
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->guard('web')->validate($this->credentials($request))) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'isactive' => 1, 'type' => 'admin']) || Auth::attempt(['email' => $request->email, 'password' => $request->password, 'isactive' => 1, 'type' => 'staff'])) {
                auth('web')->user()->update(['login_status' => 1]);
                return redirect()->intended('home');
            } else {
                $this->incrementLoginAttempts($request);
                return redirect()->back()->withErrors(['email' => 'This account is not activated']);
            }
        } else {
            // dd('ok');
            $this->incrementLoginAttempts($request);
            return redirect()->back()->withErrors(['email' => 'Credentials do not match our database.']);
        }
    }
}
