<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Password;

class RespondentForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest:respondent');
    }

    public function showLinkRequestForm()
    {
        return view('front.respondents.password.email');
    }

    protected function broker()
    {
        return Password::broker('respondents');
    }
}
