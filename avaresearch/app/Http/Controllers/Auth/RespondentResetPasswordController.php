<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Password;
use Auth;


class RespondentResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest:respondent');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('front.respondents.password.reset')
            ->with(
                ['token' => $token, 'email' => $request->email]
            );
    }

    protected function guard()
    {
        return Auth::guard('respondent');
    }

    protected function broker()
    {
        return Password::broker('respondents');
    }
}
