<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\ProjectGroup;
use App\Models\Project\ProjectSMSTemplate;
use App\Models\SMSTemplate;
use App\Models\Project\JobEvent;
use App\Models\Respondents\Respondent;

class ProjectSendSMSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTemplates($projectUUID, $type)
    {
        $templates = '';
        if ($type == 'project') {
            $project = Project::where('uuid', $projectUUID)->first();
            $templates = ProjectSMSTemplate::select('id', 'title')->where('job_id', $project->id)->where('isactive', 1)->get()->toArray();
        } else {
            $templates = SMSTemplate::select('id', 'title')->where('isactive', 1)->get()->toArray();
        }
        return response()->json($templates, 200);
    }

    public function getTemplate($projectUUID, $type, $id)
    {
        if ($type == 'project') {
            $template = ProjectSMSTemplate::find($id);
            return response()->json($template, 200);
        } else {
            $template = SMSTemplate::find($id);
            return response()->json($template, 200);
        }
    }

    public function send(Request $request)
    {
        $to = $request->to;
        $to = explode(',', $to);

        $template_type = $request->template_type;
        $template_id = $request->template_id;
        $message = urlencode($request->message);

        $project = Project::where('uuid', $request->project_id)->first();

        $exetel_username = env('EXETEL_USERNAME');
        $exetel_password = env('EXETEL_PASSWORD');
        $exetel_senderid = env('EXETEL_SENDERID');

        foreach ($to as $mobile) {

            $respondent = Respondent::whereHas('detail', function ($q) use ($mobile) {
                $q->where('mobile_phone', $mobile);
            })->first();

            //$mobile = 919842640172;

            if ($respondent && $respondent->wrong_number != 1  && !empty($mobile)) {

                $mobile = str_replace(' ', '', str_replace('-', ' ', $mobile));
                $url = "https://smsgw.exetel.com.au/sendsms/api_sms.php?username={$exetel_username}&password={$exetel_password}&mobilenumber={$mobile}&message={$message}&sender={$exetel_senderid}&messagetype=Text&referencenumber=1234";
                try {
                    $client = new \GuzzleHttp\Client();
                    $response = $client->request('GET', $url);
                    $statusCode = $response->getStatusCode();
                    if ($statusCode == 200) {
                        $response = $response->getBody();
                        JobEvent::create([
                            'project_id' => $project->id,
                            'respondent_id' => $respondent->id,
                            'name' => 'SMS Sent',
                            'client_id' => $project->client_id,
                            'group_id' => 0,
                            'attende_doc_com' => '',
                            'created_date' => date('Y-m-d'),
                            'template_id' => $template_id,
                            'qualifiying_date' => date('Y-m-d H:i:s'),
                            'note' => $message,
                            'email' => 'No',
                            'sms' => 'Yes',
                            'assigned_user' => auth()->id()
                        ]);
                    }
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    $response = json_encode((string) $e->getResponse()->getBody());
                }

                // $sms_result = file_get_contents($url);
                // $result = explode('|', $sms_result);

                // if ($result[0]) {
                //     JobEvent::create([
                //         'project_id' => $project->id,
                //         'respondent_id' => $respondent->id,
                //         'name' => 'SMS Sent',
                //         'client_id' => $project->client_id,
                //         'group_id' => 0,
                //         'attende_doc_com' => '',
                //         'created_date' => date('Y-m-d'),
                //         'template_id' => $template_id,
                //         'qualifiying_date' => date('Y-m-d H:i:s'),
                //         'note' => $message,
                //         'email' => 'No',
                //         'sms' => 'Yes',
                //         'assigned_user' => auth()->id()
                //     ]);
                // }
            }
        }
        return response()->json($url);
    }
}
