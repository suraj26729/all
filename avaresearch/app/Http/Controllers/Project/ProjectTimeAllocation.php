<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\Project;
use App\Models\Project\TrackingTime;
use Carbon\Carbon;
use Auth;

class ProjectTimeAllocation extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   

    public function index($projectUUID)
    {
         $project = Project::where('uuid', $projectUUID)->first();
        return view('projects.time_allocation', compact('project'));
    }
    
    public function DTTimeAllocation($projectUUID)
    {
        $timeallocation =TrackingTime::join('projects', 'projects.id', '=', 'tracking_times.project_id')
        ->join('user_details', 'user_details.user_id', '=', 'tracking_times.staff_id')
        ->where('projects.uuid',$projectUUID)
        ->select(
            'tracking_times.start_time',
            'tracking_times.end_time',
            'tracking_times.deveoted_time',
            'tracking_times.created_at',
            'projects.job_number',
            'user_details.first_name',
            'user_details.last_name'

        );

        return Datatables::of($timeallocation)
        ->editColumn('staff_name', function($staff){
            return $staff->first_name. ' '.$staff->last_name;
        })
        ->rawColumns(['staff_name'])
        ->make(true);
    }

    public function store(Request $request)
    {
       
        $staff_id =auth()->id();
        $start_time =date("H:i:s");

        $data = [
            'staff_id' => $staff_id,
            'project_id' => $request->job,
            'start_time' => $start_time
        ];
       $timetracking =TrackingTime::create($data);

       $request->session()->put([
                     'timetracking_id' => $timetracking->id,
                     'start_time' => $start_time
            ]);
       
    }

    public function update(Request $request)
    {
        $staff_id =auth()->id();
        $timetracking_id =  $request->session()->get('timetracking_id');
        $start_time =  $request->session()->get('start_time');

        $end_time =date("H:i:s");

        $start  = new Carbon($start_time);
        $end    = new Carbon($end_time);
        $deveoted_time = $start->diff($end)->format('%H:%I:%S');
        
        $endTracking = TrackingTime::where(['id' => $timetracking_id, 'staff_id' =>$staff_id])
        ->update([
            'end_time' => $end_time,
            'deveoted_time' => $deveoted_time
        ]);
        
        $request->session()->forget('timetracking_id','start_time');

    }

    public function endtimelogout(Request $request)
    {

        $staff_id =auth()->id();

        $timetracking_id =  $request->session()->get('timetracking_id');
        $start_time =  $request->session()->get('start_time');
        $end_time =date("H:i:s");

        $start  = new Carbon($start_time);
        $end    = new Carbon($end_time);
        $deveoted_time = $start->diff($end)->format('%H:%I:%S');
        
        $endTracking = TrackingTime::where(['id' => $timetracking_id, 'staff_id' =>$staff_id])
        ->update([
            'end_time' => $end_time,
            'deveoted_time' => $deveoted_time
        ]);
        
        $request->session()->forget('timetracking_id','start_time');
         
        Auth::logout();
         return redirect(route('login'));

    }
    

    public function destory($projectUuid, SaveQuery $query)
    {
        
    }



}
