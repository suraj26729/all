<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Models\Project;
use App\Models\Project\JobEvent;

class ProjectStandByRespondentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($projectUUID)
    {
        $project = Project::where('uuid', $projectUUID)->first();
        return view('projects.standby-respondents', compact('project'));
    }

    public function DTStandByRespondents($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $respondents = JobEvent::where('project_id', $project->id)->where('job_events.name', 'Standby')
            ->leftjoin('respondents', 'respondents.id', '=', 'job_events.respondent_id')
            ->leftjoin('project_groups', 'project_groups.id', '=', 'job_events.group_id')
            ->selectRaw(
                'job_events.id, job_events.respondent_id, CONCAT(respondents.first_name," ",respondents.last_name) as name, 
                respondents.last_name, project_groups.name as group_name'
            );
        return Datatables::of($respondents)
            ->addColumn('check_row', function ($query) {
                return '<td><input type="checkbox" name="check_row" value="' . $query->id . '" /></td>';
            })
            ->addColumn('actions', function ($query) {
                return '<td>
                            <a href="javascript:void(0);" class="btn btn-outline-primary btn-xs reverse_btn" data-id="' . $query->id . '" data-name="' . $query->name . '">
                                <i class="icon-trash"></i> Reverse Job qualified 
                            </a>
                        </td>';
            })->rawColumns(['check_row', 'actions'])->make(true);
    }

    public function move(Request $request, $projectUuid)
    {
        $ids = $request->ids;
        $res = JobEvent::whereIn('id', $ids)->update(['name' => 'Screened OK']);
        return response()->json($res, 200);
    }

    public function reverse(Request $request, $projectUuid)
    {
        $id = $request->id;
        $job_event = JobEvent::find($id);
        if ($job_event != null) $job_event->delete();
        return response()->json(1);
    }
}
