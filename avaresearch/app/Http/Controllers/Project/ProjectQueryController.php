<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\Project;
use App\Models\Project\SaveQuery;

class ProjectQueryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($projectUUID)
    {
        $project = Project::where('uuid', $projectUUID)->first();
        return view('projects.queries.index', compact('project'));
    }

    public function DTProjectQueries($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $groups = SaveQuery::where('project_id', $project->id)
            ->select(
                'save_queries.id',
                'save_queries.uuid',
                'save_queries.title',
                'save_queries.created_by',
                'save_queries.created_at'
            );
        return Datatables::of($groups)
            ->addColumn('check_row', function ($query) {
                return '<td><input type="checkbox" name="check_row" value="' . $query->id . '" /></td>';
            })
            ->addColumn('creator_name', function ($query) {
                if ($query->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $query->createdBy->uuid) . '">
                    ' . $query->createdBy->profile->first_name . ' ' . $query->createdBy->profile->last_name . '
                </a>';
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($query) {
                return $query->created_at->format('d-m-Y');
            })
            ->addColumn('actions', function ($query) use ($project) {
                return '<td>
                <a href="' . route('indexByQueryProjectSearchRespondents', [$project->uuid, $query->uuid]) . '" class="btn btn-primary btn-sm">Run Query</a>
                <a href="javascript:void(0);" class="btn btn-danger btn-sm delete_btn" data-id="' . $query->id . '" data-name="' . $query->title . '"><i class="icon-trash"></i></a>
            </td>';
            })->rawColumns(['check_row', 'actions', 'creator_name'])->make(true);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['searched_data'] = session('searched_data');
        $query = session('query');
        $data['query'] = $query[1]['query'];

        $save_query = SaveQuery::create([
            'title' => $data['query_title'], 'project_id' => $data['project_id'], 'respondent_ids' => $data['respondent_ids'],
            'query' => $data['query'], 'project_searched_data' => $data['searched_data'], 'created_by' => auth()->id()
        ]);

        return response()->json($save_query, 201);
    }

    public function destory($projectUuid, SaveQuery $query)
    {
        $query->delete();
        return response()->json(1);
    }

    public function destoryBulk($projectUUID, Request $request)
    {
        if (is_array($request->ids)) {
            SaveQuery::whereIn('id', $request->ids)->delete();
            return response()->json(1);
        }
        return response()->json(0);
    }
}
