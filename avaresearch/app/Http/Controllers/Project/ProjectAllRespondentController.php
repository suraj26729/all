<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\User;
use App\Models\Project;
use App\Models\Respondents\Respondent;
use App\Models\Project\JobEvent;

class ProjectAllRespondentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($projectUUID)
    {
        $project = Project::where('uuid', $projectUUID)->first();
        return view('projects.all-respondents', compact('project'));
    }

    public function DTAllRespondents($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $respondents = JobEvent::where('project_id', $project->id)->whereIn('job_events.name', ['Screened OK', 'Qualified', 'Screened OK – Quotas Full'])
            ->leftjoin('respondents', 'respondents.id', '=', 'job_events.respondent_id')
            ->leftjoin('project_groups', 'project_groups.id', '=', 'job_events.group_id')
            ->selectRaw(
                'job_events.id, job_events.respondent_id, CONCAT(respondents.first_name," ",respondents.last_name) as name, 
                respondents.last_name, project_groups.name as group_name, job_events.created_date, job_events.assigned_user'
            );
            //->whereNull('respondents.deleted_at');
        return Datatables::of($respondents)
            ->addColumn('check_row', function ($query) {
                $respondent = Respondent::find($query->respondent_id);
                $email = '';
                $mobile = '';
                if ($respondent != null) {
                    $email = ($respondent->email && $respondent->stop_often_email != 1) ? $respondent->email : '';
                    $mobile = ($respondent->detail->mobile_phone && $respondent->wrong_number != 1) ? $respondent->detail->mobile_phone : '';
                }
                return '<td><input type="checkbox" name="row_check" class="row_check" value="' . $query->id . '" data-email="' . $email . '" 
                data-mobile="' . $mobile . '" /></td>';
            })
            ->addColumn('assigned_by', function ($query) {
                $assigned_by = User::find($query->assigned_user);
                return $assigned_by ? $assigned_by->profile->first_name . ' ' . $assigned_by->profile->last_name : '';
            })
            ->addColumn('actions', function ($query) {
                return '<td>
                            <a href="javascript:void(0);" class="btn btn-outline-primary btn-xs reverse_btn" data-id="' . $query->id . '" data-name="' . $query->name . '">
                                <i class="icon-trash"></i> Reverse Job qualified 
                            </a>
                        </td>';
            })->rawColumns(['check_row', 'actions'])->make(true);
    }

    public function reverse(Request $request, $projectUuid)
    {
        $id = $request->id;
        $job_event = JobEvent::find($id);
        if ($job_event != null) $job_event->delete();
        return response()->json(1);
    }
}
