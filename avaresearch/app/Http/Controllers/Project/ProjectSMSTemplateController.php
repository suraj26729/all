<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\SMSTemplate;
use App\Models\Project\ProjectSMSTemplate;
use Yajra\Datatables\Datatables;

class ProjectSMSTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        return view('projects.sms_templates.index', compact('project'));
    }

    public function DTSMSTemps($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $templates = ProjectSMSTemplate::select(
            'id',
            'uuid',
            'title',
            'isactive',
            'created_by',
            'created_at'
        )->where('job_id', $project->id);
        return Datatables::of($templates)
            ->addColumn('template_name', function ($template) use ($projectUuid) {
                return '<a href="' . route('editProjectSMSTemp', [$projectUuid, $template->uuid]) . '">
                    ' . $template->title  . '
                </a>';
            })
            ->addColumn('actions', function ($template) use ($projectUuid) {
                return '<td>
                <a href="' . route('editProjectSMSTemp', [$projectUuid, $template->uuid]) . '"><i class="icon-pencil mr-3"></i></a>
                <a href="javascript:void(0);" class="delete_btn" data-id="' . $template->id . '" data-name="' . $template->title . '"><i class="icon-trash"></i></a>
            </td>';
            })
            ->addColumn('creator_name', function ($template) {
                if ($template->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $template->createdBy->uuid) . '">
                    ' . $template->createdBy->profile->first_name . ' ' . $template->createdBy->profile->last_name . '
                </a>';
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($template) {
                return $template->created_at->format('d-m-Y');
            })
            ->rawColumns(['template_name', 'actions', 'creator_name'])->make(true);
    }

    public function create($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        return view('projects.sms_templates.create', compact('project'));
    }

    public function store(Request $request, $projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];
        $this->validate($request, $rules, []);

        //dd($request->all());

        $data = $request->all();
        $data['job_id'] = $project->id;
        $data['created_by'] = auth()->id();

        $template = ProjectSMSTemplate::create($data);
        if ($template) {
            return redirect()->back()->withSuccess('SMS Template created successfully!');
        } else
            return redirect()->back()->withErrors(['message' => 'Problem while creating sms template. Please try again']);
    }

    public function edit($projectUuid, $uuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $template = ProjectSMSTemplate::where('uuid', $uuid)->first();
        return view('projects.sms_templates.edit', compact('template', 'project'));
    }

    public function update(Request $request, $projectUuid, ProjectSMSTemplate $template)
    {
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];

        $this->validate($request, $rules, []);

        $data = $request->all();
        $data['modified_by'] = auth()->id();

        $template->update($data);

        return redirect()->back()->withSuccess('SMS Template updated successfully!');
    }

    public function destory($projectUuid, ProjectSMSTemplate $template)
    {
        $template->delete();
        return response()->json(1);
    }
}
