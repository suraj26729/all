<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectGroup;
use App\Models\Project\ProjectEmailTemplate;
use App\Models\EmailTemplate;
use App\Models\Project\JobEvent;
use App\Models\Respondents\Respondent;

use Illuminate\Support\Facades\Storage;

use Mail;

class ProjectSendEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTemplates($projectUUID, $type)
    {
        $templates = '';
        if ($type == 'project') {
            $project = Project::where('uuid', $projectUUID)->first();
            $templates = ProjectEmailTemplate::select('id', 'title')->where('job_id', $project->id);

            if (auth('web')->check() && auth('web')->user()->type == 'staff')
                $templates = $templates->where('title', 'NOT LIKE', '%invitation%');

            $templates = $templates->where('isactive', 1)->get()->toArray();
        } else {
            $templates = EmailTemplate::select('id', 'title')->where('isactive', 1)->where('title', 'NOT LIKE', '%invitation%')
                ->get()->toArray();
        }
        return response()->json($templates, 200);
    }

    public function getTemplate($projectUUID, $type, $id)
    {
        if ($type == 'project') {
            $template = ProjectEmailTemplate::with('attachments')->find($id);
            return response()->json($template, 200);
        } else {
            $template = EmailTemplate::with('attachments')->find($id);
            return response()->json($template, 200);
        }
    }

    public function send(Request $request)
    {
        $to = $request->to;
        $to = explode(',', $to);
        $subject = $request->subject;
        $template_type = $request->template_type;
        $template_id = $request->template_id;
        $group_id = $request->group_id;
        $message = $request->message;

        $new_attachment1 = $request->file('new_attachment1');
        $new_attachment2 = $request->file('new_attachment2');
        $new_attachment3 = $request->file('new_attachment3');

        $include_privacy = $request->include_privacy;
        $from = $request->from;

        $project = Project::where('uuid', $request->project_id)->first();
        $group = '';
        if ($group_id) {
            $group = ProjectGroup::with(['location'])->find($group_id);
        }
        $template = '';
        if ($template_type == 'project') $template = ProjectEmailTemplate::with('attachments')->find($template_id);
        else $template = EmailTemplate::with('attachments')->find($template_id);

        $attachments = [];
        if ($template->attachments != null) {
            foreach ($template->attachments as $attachment) {
                //$attachments[] = asset('storage/app/public/' . $attachment->url);
                $attachments[] = storage_path('app/public/' . $attachment->url);
            }
        }

        if (!empty($new_attachment1)) {
            $path = 'docs/temp-attachments';
            $filename = time() . $new_attachment1->getClientOriginalName();
            Storage::disk('public')->putFileAs($path, $new_attachment1, $filename);
            //$attachments[] = asset('storage/app/public/' . $path . '/' . $filename);
            $attachments[] = storage_path('app/public/' . $path . '/' . $filename);
        }

        //dd($attachments);

        $job_name = $project->job_name;
        if ($group) {
            $group_name = $group->name;
            $venue = $group->venue_address . ',' . $group->location;
            $incentive = $group->incentive;
            $date = $group->group_date;
            $duration = $group->duration;
            $time = $group->group_time;

            $replacement_strings = array('%Jobname%', '%GroupName%', '%Venue%', '%Incentive%', '%Date%', '%Duration%', '%Time%');
            $actual_strings      = array($job_name, $group_name, $venue, $incentive, $date, $duration, $time);

            $message = str_replace($replacement_strings, $actual_strings, $message);
        }

        // $to = ['satheesh123raj@gmail.com'];

        foreach ($to as $email) {

            $respondent = Respondent::where('email', $email)->first();

            /*$replacement_strings1 = array('{firstname}');
            $actual_strings1      = array($respondent->first_name);
            $message = str_replace($replacement_strings1, $actual_strings1, $message);*/

           // if ($respondent && $respondent->stop_often_email != 1 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            
           //$photo = asset('public/assets/img/logo.png');
           $photo = 'https://devavaresearch.com.au/public/assets/img/logo.png';
            $logo="<p><img src='".$photo." ' style='height:50px; width:180px' /></p>";
            

             if ($respondent && filter_var($email, FILTER_VALIDATE_EMAIL)) {

                $body = $logo."<br/>"."<p style='font-family:arial,sans-serif; font-size:14px'> Dear  " . $respondent->first_name .",</p> <br/>" . $message;

                if ($include_privacy) $body = $body . '<br/>' . env('PRIVACY_POLICY_URL');

                // $unsubscribe_link = '<br/><br/>To stop receiving these emails please click here : <a href="'.env('APP_URL').'/respondents/unsubscribe?type=' . urlencode(base64_encode($template_type)) . '&template=' . urlencode(base64_encode($template_id)) . '">Unsubscribe from Ava Research Database</a> OR <a href="'.env('APP_URL').'/respondents/unsubscribe?type=' . urlencode(base64_encode($template_type)) . '&template=' . urlencode(base64_encode($template_id)) . '">Unsubscribe from Emails only</a>';

                $unsubscribe_link = '<br/><br/>To stop receiving these emails please click here : <a href="http://localhost/Avaresearch/respondents/unsubscribe/'.$respondent->uuid.'">Unsubscribe from Ava Research Database</a> OR <a href="http://localhost/Avaresearch/respondents/unsubscribeEmail/'.$respondent->uuid.'">Unsubscribe from Emails only</a>';

                $body = $body . $unsubscribe_link;

                //$email = 'satheesh123raj@gmail.com';

                Mail::send([], [], function ($mail) use ($from, $body, $email, $subject, $attachments) {
                    $mail->from($from)->to($email)->subject($subject)->setBody($body, 'text/html');
                    if (count($attachments)) {
                        foreach ($attachments as $url) $mail->attach($url);
                    }
                });
                if (!Mail::failures()) {
                    JobEvent::create([
                        'project_id' => $project->id,
                        'respondent_id' => $respondent->id,
                        'name' => 'Email Sent',
                        'client_id' => $project->client_id,
                        'group_id' => $group_id,
                        'attende_doc_com' => '',
                        'created_date' => date('Y-m-d'),
                        'template_id' => $template_id,
                        'qualifiying_date' => date('Y-m-d H:i:s'),
                        'note' => $body,
                        'email' => 'Yes',
                        'sms' => 'No',
                        'assigned_user' => auth()->id()
                    ]);
                }else{
                  echo "Failed"; //dd($respondent->id);
                }
            }
        }
        return response()->json(1);
    }
}
