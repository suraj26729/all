<?php

namespace App\Http\Controllers\Project\Group;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectGroup;
use App\Models\EmailTemplate;
use App\Models\Project\ProjectEmailTemplate;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

class ProjectGroupEmailTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($projectUuid, $groupUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        return view('projects.groups.email_templates.index', compact('project', 'group'));
    }

    public function DTEmailTemps($projectUuid, $groupUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        $templates = ProjectEmailTemplate::select(
            'project_email_templates.id',
            'project_email_templates.uuid',
            'project_email_templates.title',
            'project_email_templates.isactive',
            'project_email_templates.created_by',
            'project_email_templates.created_at'
        )->where('group_id', $group->id);
        return Datatables::of($templates)
            ->addColumn('template_name', function ($template) use ($projectUuid, $groupUuid) {
                return '<a href="' . route('editProjectGroupEmailTemp', [$projectUuid, $groupUuid, $template->uuid]) . '">
                    ' . $template->title  . '
                </a>';
            })
            ->addColumn('actions', function ($template) use ($projectUuid, $groupUuid) {
                return '<td>
                <a href="' . route('editProjectGroupEmailTemp', [$projectUuid, $groupUuid, $template->uuid]) . '"><i class="icon-pencil mr-3"></i></a>
                <a href="javascript:void(0);" class="delete_btn" data-id="' . $template->id . '" data-name="' . $template->title . '"><i class="icon-trash"></i></a>
            </td>';
            })
            ->addColumn('creator_name', function ($template) {
                if ($template->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $template->createdBy->uuid) . '">
                    ' . $template->createdBy->profile->first_name . ' ' . $template->createdBy->profile->last_name . '
                </a>';
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($template) {
                return $template->created_at->format('d-m-Y');
            })
            ->rawColumns(['template_name', 'actions', 'creator_name'])->make(true);
    }

    public function create($projectUuid, $groupUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        $templates = EmailTemplate::where('isactive', 1)->whereIn('id', [83, 94, 95, 96, 97, 98, 100])->orderBy('title', 'ASC')->get();
        return view('projects.groups.email_templates.create', compact('project', 'group', 'templates'));
    }

    public function store(Request $request, $projectUuid, $groupUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];
        $this->validate($request, $rules, []);

        //dd($request->all());

        $data = $request->except('attachment', 't_attachment', 'master_template');
        $data['job_id'] = $project->id;
        $data['group_id'] = $group->id;
        $data['created_by'] = auth()->id();

        $template = ProjectEmailTemplate::create($data);
        if ($template) {
            if ($request->has('attachment') && count($request->attachment)) {
                $args = [];
                $files = $request->file('attachment');
                $path = 'docs/attachments';
                foreach ($files as $file) {
                    $filename = time() . $file->getClientOriginalName();
                    Storage::disk('public')->putFileAs($path, $file, $filename);
                    $template->attachments()->create(['template_id' => $template->id, 'url' => $path . '/' . $filename]);
                }
            }
            if ($request->has('t_attachment') && count($request->t_attachment)) {
                $files = $request->t_attachment;
                foreach ($files as $file) {
                    if ($file != null && $file != '')
                        $template->attachments()->create(['template_id' => $template->id, 'url' => $file]);
                }
            }
            return redirect()->back()->withSuccess('Email Template created successfully!');
        } else
            return redirect()->back()->withErrors(['message' => 'Problem while creating email template. Please try again']);
    }

    public function edit($projectUuid, $groupUuid, $uuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        $template = ProjectEmailTemplate::where('uuid', $uuid)->first();
        return view('projects.groups.email_templates.edit', compact('template', 'project', 'group'));
    }

    public function update(Request $request, $projectUuid, $groupUuid, ProjectEmailTemplate $template)
    {
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];

        $this->validate($request, $rules, []);

        $data = $request->except('attachment');
        $data['modified_by'] = auth()->id();

        $template->update($data);

        if ($request->has('attachment') && count($request->attachment)) {
            $template->attachments()->delete();
            $args = [];
            $files = $request->file('attachment');
            if (!empty($files) && count($files)) {
                $path = 'docs/attachments';
                foreach ($files as $file) {
                    $filename = time() . str_replace(' ', '-', $file->getClientOriginalName());
                    Storage::disk('public')->putFileAs($path, $file, $filename);
                    $template->attachments()->create(['template_id' => $template->id, 'url' => $path . '/' . $filename]);
                }
            }
            foreach ($request->attachment as $url) {
                if (is_string($url)) {
                    $template->attachments()->create(['template_id' => $template->id, 'url' => $url]);
                }
            }
        }

        return redirect()->back()->withSuccess('Email Template updated successfully!');
    }

    public function destory($projectUuid, $groupUuid, ProjectEmailTemplate $template)
    {
        $template->delete();
        return response()->json(1);
    }
}
