<?php

namespace App\Http\Controllers\Project\Group;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Models\Project;
use App\Models\ProjectGroup;
use App\Models\Project\JobEvent;

class ProjectGroupStandByRespondentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($projectUuid, $groupUuid)
    {
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        return view('projects.groups.standby-respondents', compact('group'));
    }

    public function DTStandByRespondents($projectUuid, $groupUuid)
    {
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        $respondents = JobEvent::where('group_id', $group->id)->where('job_events.name', 'Standby')
            ->leftjoin('respondents', 'respondents.id', '=', 'job_events.respondent_id')
            ->selectRaw(
                'job_events.id, job_events.respondent_id, CONCAT(respondents.first_name," ",respondents.last_name) as name, 
                respondents.last_name'
            );
        return Datatables::of($respondents)
            ->addColumn('check_row', function ($query) {
                return '<td><input type="checkbox" name="check_row" value="' . $query->id . '" /></td>';
            })
            ->addColumn('actions', function ($query) {
                return '<td>
                            <a href="javascript:void(0);" class="btn btn-outline-primary btn-xs reverse_btn" data-id="' . $query->id . '" data-name="' . $query->name . '">
                                <i class="icon-trash"></i> Reverse Job qualified 
                            </a>
                        </td>';
            })->rawColumns(['check_row', 'actions'])->make(true);
    }

    public function move(Request $request, $projectUuid, $groupUuid)
    {
        $ids = $request->ids;
        $res = JobEvent::whereIn('id', $ids)->update(['name' => 'Screened OK']);
        return response()->json($res, 200);
    }

    public function reverse(Request $request, $projectUuid, $groupUuid)
    {
        $id = $request->id;
        $job_event = JobEvent::find($id);
        if ($job_event != null) $job_event->delete();
        return response()->json(1);
    }
}
