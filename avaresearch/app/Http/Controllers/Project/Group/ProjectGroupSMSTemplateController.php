<?php

namespace App\Http\Controllers\Project\Group;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectGroup;
use App\Models\Project\ProjectSMSTemplate;
use Yajra\Datatables\Datatables;

class ProjectGroupSMSTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($projectUuid, $groupUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        return view('projects.groups.sms_templates.index', compact('project', 'group'));
    }

    public function DTSMSTemps($projectUuid, $groupUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        $templates = ProjectSMSTemplate::select(
            'id',
            'uuid',
            'title',
            'isactive',
            'created_by',
            'created_at'
        )->where('group_id', $group->id);
        return Datatables::of($templates)
            ->addColumn('template_name', function ($template) use ($projectUuid, $groupUuid) {
                return '<a href="' . route('editProjectGroupSMSTemp', [$projectUuid, $groupUuid, $template->uuid]) . '">
                    ' . $template->title  . '
                </a>';
            })
            ->addColumn('actions', function ($template) use ($projectUuid, $groupUuid) {
                return '<td>
                <a href="' . route('editProjectGroupSMSTemp', [$projectUuid, $groupUuid, $template->uuid]) . '"><i class="icon-pencil mr-3"></i></a>
                <a href="javascript:void(0);" class="delete_btn" data-id="' . $template->id . '" data-name="' . $template->title . '"><i class="icon-trash"></i></a>
            </td>';
            })
            ->addColumn('creator_name', function ($template) {
                if ($template->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $template->createdBy->uuid) . '">
                    ' . $template->createdBy->profile->first_name . ' ' . $template->createdBy->profile->last_name . '
                </a>';
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($template) {
                return $template->created_at->format('d-m-Y');
            })
            ->rawColumns(['template_name', 'actions', 'creator_name'])->make(true);
    }

    public function create($projectUuid, $groupUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        return view('projects.groups.sms_templates.create', compact('project', 'group'));
    }

    public function store(Request $request, $projectUuid, $groupUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];
        $this->validate($request, $rules, []);

        //dd($request->all());

        $data = $request->all();
        $data['job_id'] = $project->id;
        $data['group_id'] = $group->id;
        $data['created_by'] = auth()->id();

        $template = ProjectSMSTemplate::create($data);
        if ($template) {
            return redirect()->back()->withSuccess('SMS Template created successfully!');
        } else
            return redirect()->back()->withErrors(['message' => 'Problem while creating sms template. Please try again']);
    }

    public function edit($projectUuid, $groupUuid, $uuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $groupUuid)->first();
        $template = ProjectSMSTemplate::where('uuid', $uuid)->first();
        return view('projects.groups.sms_templates.edit', compact('template', 'project', 'group'));
    }

    public function update(Request $request, $projectUuid, $groupUuid, ProjectSMSTemplate $template)
    {
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];

        $this->validate($request, $rules, []);

        $data = $request->all();
        $data['modified_by'] = auth()->id();

        $template->update($data);

        return redirect()->back()->withSuccess('Email Template updated successfully!');
    }

    public function destory($projectUuid, $groupUuid, ProjectSMSTemplate $template)
    {
        $template->delete();
        return response()->json(1);
    }
}
