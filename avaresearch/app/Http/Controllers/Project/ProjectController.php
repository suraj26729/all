<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\ProjectGroup;
use Yajra\Datatables\Datatables;

use Excel;
use App\Exports\JobsAnnualReport;

use App\User;


class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('projects.index');
        /*$i=1;
        $project =Project::select('id')->get();
        

       for($i=0;$i<600;$i++){
            $date=date("Y-m-d h:i:s");
           echo $date.'<br>';

            $project1=Project::where('jobnumber',$i)
            ->update(["created_at" => $date]);
            sleep(1);
            echo $date=date("Y-m-d h:i:s").'<br><br>';
           // echo $i.'<br>';
            }
            echo "Completed";*/
    }

    public function DTData()
    {
        $projects = Project::with(['client'])->leftjoin('users', 'users.id', '=', 'projects.client_id')
            ->leftjoin('user_details', 'user_details.user_id', '=', 'users.id')
            ->select(
                'projects.jobnumber',
                'projects.uuid',
                'projects.job_number',
                'projects.job_name',
                'projects.subject',
                'projects.description',
                'projects.client_id',
                'user_details.first_name',
                'user_details.last_name',
                'projects.location',
                'projects.created_by',
                'projects.created_at'
            );
        if (auth()->user()->type == 'client') $projects = $projects->where('client_id', auth()->id());

        return Datatables::of($projects)
            
            ->addColumn('project_name', function ($project) {
                return '<a href="' . route('projects.edit', $project->uuid) . '">
                    ' . $project->job_name  . '
                </a>';
            })
            ->addColumn('excerpt_desc', function ($project) {
                return str_limit($project->description, 40, '...');
            })
            ->addColumn('client_name', function ($project) {
                if ($project->client != null) {
                    if (auth()->user()->type == 'client') {
                        return '<a href="' . route('edit-profile') . '">
                        ' . $project->client->profile->first_name . ' ' . $project->client->profile->last_name . '
                    </a>';
                    } else {
                        return '<a href="' . route('clients.edit', $project->client->uuid) . '">
                    ' . $project->client->profile->first_name . ' ' . $project->client->profile->last_name . '
                </a>';
                    }
                } else {
                    return '';
                }
            })
            ->addColumn('created_name', function ($project) {
                if ($project->createdBy != null) {
                    if (auth()->user()->type == 'client') {
                        return $project->createdBy->profile->first_name . ' ' . $project->createdBy->profile->last_name;
                    } else {
                        return '<a href="' . route('staffs.edit', $project->createdBy->uuid) . '">
                    ' . $project->createdBy->profile->first_name . ' ' . $project->createdBy->profile->last_name . '
                </a>';
                    }
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($project) {
                return $project->created_at->format('d-m-Y');
            })
            ->addColumn('actions', function ($project) {
                return '<td>
                <a href="' . route('projects.edit', $project->uuid) . '"><i class="icon-pencil mr-3"></i></a>
                <a href="javascript:void(0);" class="delete_btn" data-id="' . $project->id . '" data-name="' . $project->job_name . '"><i class="icon-trash"></i></a>
            </td>';
            })->rawColumns(['project_name', 'client_name', 'actions', 'description', 'location', 'created_name'])->make(true);
    }

    public function create()
    {
        $clients = User::with(['profile','createdBy'])->where('isactive', 1)->where('type', 'client')->get()->sortBy('profile.company_name');
        return view('projects.create', compact('clients'));
    }

    public function store(Request $request)
    {
        $rules = [
            'job_number' => 'required|max:255',
            'job_name' => 'required|max:255',
            'subject' => 'required|max:255',
            'description' => 'required',
            'client_id' => 'required|numeric',
            'location' => 'required|max:255',
            'contact_phone' => 'required|max:255',
            'contact_email' => 'required|email',
            'venue_email' => 'required|email',
            'received_date' => 'required|date',
            'start_date' => 'required|date',
            'finish_date' => 'required|date'
        ];
        $this->validate($request, $rules, []);

        $data = $request->all();
        $data['jobnumber'] = (int)$request->job_number;
        $data['created_by'] = auth()->id();

        $project = Project::create($data);
        if ($project)
            return redirect()->back()->withSuccess('Project created successfully!');
        else
            return redirect()->back()->withErrors(['message' => 'Problem while creating project. Please try again']);
    }

    public function checkJobNum(Request $request)
    {
        $result = Project::where('job_number', $request->job_number);
        if ($request->has('action') && $request->action == 'update') {
            $result = $result->whereNotIn('id', [$request->current]);
        }
        $result = $result->exists();
        return response()->json($result);
    }

    public function edit($uuid)
    {
        $project = Project::with(['client', 'createdBy'])->where('uuid', $uuid)->first();
        $clients = User::with(['profile'])->where('isactive', 1)->where('type', 'client')->get()->sortBy('profile.company_name');
        return view('projects.edit', compact('project', 'clients'));
    }

    public function update(Request $request, Project $project)
    {
        $rules = [
            'job_number' => 'required|max:255',
            'job_name' => 'required|max:255',
            'subject' => 'required|max:255',
            'description' => 'required',
            'client_id' => 'required|numeric',
            'location' => 'required|max:255',
            'contact_phone' => 'required|max:255',
            'contact_email' => 'required|email',
            'venue_email' => 'required|email',
            'received_date' => 'required|date',
            'start_date' => 'required|date',
            'finish_date' => 'required|date'
        ];

        $this->validate($request, $rules, []);

        $data = $request->all();
        $data['modified_by'] = auth()->id();
        if (!$request->has('validation_report')) $data['validation_report'] = 'No';

        $project->update($data);

        return redirect()->back()->withSuccess('Project updated successfully!');
    }

    public function destroy(Project $project)
    {
        $project->delete();
        return response()->json(1);
    }

    public function generateJobReport(Request $request)
    {
        $job_id = $request->job;
        if ($job_id) {
            $project = Project::find($job_id);
            $phpWord = new \PhpOffice\PhpWord\PhpWord();

            $sectionStyle = ['orientation' => 'landscape'];
            $styleTable = ['borderSize' => 6, 'borderColor' => '006699', 'cellMargin' => 80];
            $styleFirstRow = ['borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF'];

            $phpWord->addTitleStyle(1, ['bold' => true], ['spaceAfter' => 240]);
            $section = $phpWord->addSection($sectionStyle);

            $source = file_get_contents(asset('public/assets/img/logo.png'), false, stream_context_create(["ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )]));

            $section->addImage($source, array('width' => 180, 'height' => 50, 'align' => 'center'));
            $paragraphStyle = array('lineHeight' => '0.9');
            $section->addTitle('JOB SUMMARY REPORT:', 1, $paragraphStyle);
            $phpWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

            $table = $section->addTable('myOwnTableStyle');
            $noSpace = array('spaceAfter' => 0);
            $table->addRow();
            $table->addCell(7000)->addText('Job Number:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText(htmlspecialchars($project->job_number), array('bold' => true), $noSpace);
            $table->addRow();
            $table->addCell(7000)->addText('Job received Date:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText($project->received_date, array('bold' => true), $noSpace);
            $table->addRow();
            $table->addCell(7000)->addText('Job Name:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText(htmlspecialchars($project->job_name), array('bold' => true), $noSpace);
            $job_desc = htmlspecialchars(str_replace('<br>', ' ', $project->description));
            $table->addRow();
            $table->addCell(7000)->addText('Job Description:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText($job_desc, array('bold' => true), $noSpace);


            $groups_count = ProjectGroup::where('job_id', $project->id)->where('name', 'like', 'G%')->count();
            $indepth_groups_count = ProjectGroup::where('job_id', $project->id)->where('name', 'not like', 'G%')->count();

            $table->addRow();
            $table->addCell(7000)->addText('Number of Groups:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText($groups_count, array('bold' => true), $noSpace);
            $table->addRow();
            $table->addCell(7000)->addText('Number of in-depths:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText($indepth_groups_count, array('bold' => true), $noSpace);

            $table->addRow();
            $company_name = $project->client != null ? $project->client->profile->company_name : '-';
            $client_name = $project->client != null ? ($project->client->profile->first_name . ' ' . $project->client->profile->last_name) : '-';
            $table->addCell(7000)->addText('Company Name:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText(htmlspecialchars($company_name), array('bold' => true), $noSpace);
            $table->addRow();
            $table->addCell(7000)->addText('Client Name:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText(htmlspecialchars($client_name), array('bold' => true), $noSpace);
            $table->addRow();

            $start_date = $project->start_date != null ? date('Y-m-d', strtotime($project->start_date)) : '';
            $finish_date = $project->finish_date != null ? date('Y-m-d', strtotime($project->finish_date)) : '';
            $table->addCell(7000)->addText('Start Date:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText($start_date, array('bold' => true), $noSpace);
            $table->addRow();
            $table->addCell(7000)->addText('Finish Date:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText($finish_date, array('bold' => true), $noSpace);
            $table->addRow();
            $table->addCell(7000)->addText('Signed Sheets Received:', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText($project->signed_sheets, array('bold' => true), $noSpace);

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $url = storage_path('app/public/docs/job-summary/' . $project->job_name . '.docx');
            $objWriter->save($url, true);
            //return response()->download(public_path($project->job_name . '.docx'));
            return response()->json(asset('public/storage/docs/job-summary/' . $project->job_name . '.docx'));
        }
    }

    public function generateJobAnnualReport(Request $request)
    {
        $filename = 'Job summary report(Annual with all project listed).xls';
        Excel::store(new JobsAnnualReport(), 'public/' . $filename);
        return response()->json(asset('public/storage/' . $filename));
    }
}
