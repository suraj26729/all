<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\Project\ProjectPoint;

class ProjectPointController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
    }
}
