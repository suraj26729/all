<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\Project\JobEvent;

class ProjectJobEventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $data = $request->data;
        $job_events = [];
        foreach ($data as $item) {
            $flag = 0;
            if (in_array($item['name'], ['No Show', 'Do not call', 'Inactive', 'Has accent', 'One off respondent', 'Excellent candidate'])) {
                $respondent = Respondent::find($item['respondent_id']);
                if ($respondent) {
                    if ($item['name'] == 'No Show') $respondent->update(['do_no_show' => 1]);
                    if ($item['name'] == 'Do not call') $respondent->update(['do_not_call' => 1]);
                    if ($item['name'] == 'Inactive') $respondent->update(['isactive' => 1]);
                    if ($item['name'] == 'Has accent') $respondent->update(['has_accent' => 1]);
                    if ($item['name'] == 'One off respondent') $respondent->update(['one_off_resp' => 1]);
                    if ($item['name'] == 'Excellent candidate') $respondent->update(['excellent_candidate' => 1]);
                }
            } else {
                if ($item['name'] == 'Screened OK' || $item['name'] == 'Standby') {
                    $flag = JobEvent::where('project_id', $project->id)->where('respondent_id', $item['respondent_id'])
                        ->where('name', '=', $item['name'])->where('group_id', '=', $item['group_id'])->exists();
                }
                if ($flag == 0) {
                    $job_events[] = JobEvent::create([
                        'project_id' => $project->id,
                        'respondent_id' => $item['respondent_id'],
                        'name' => $item['name'],
                        'client_id' => $project->client_id,
                        'group_id' => $item['group_id'],
                        'attende_doc_com' => '',
                        'created_date' => date('Y-m-d'),
                        'qualifiying_date' => date('Y-m-d H:i:s'),
                        'template_id' => 0,
                        'group_time' => $item['group_time'],
                        'assigned_user' => auth()->id()
                    ]);
                }
            }
        }
        return response()->json(['success' => 1, 'job_events' => $job_events], 201);
    }
}
