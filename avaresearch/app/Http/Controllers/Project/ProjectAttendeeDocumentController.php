<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\Project\JobEvent;
use App\Models\ProjectGroup;
use App\Models\Project\ProjectAttendeeDocument;

class ProjectAttendeeDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function tns_document($project, $group_ids, $ids, $include_standby)
    {
       /* join('respondents', 'respondents.id', '=', 'job_events.respondent_id')*/
            
        foreach ($group_ids as $id) {
            $all_respondents = JobEvent::join('respondents', 'respondents.id', '=', 'job_events.respondent_id')
                ->whereNull('respondents.deleted_at')
                ->where('project_id', $project->id)->where('group_id', $id)
                ->whereIn('job_events.id', $ids)->whereIn('name', ['Screened OK', 'Qualified', 'Screened OK – Quotas Full'])->get();
            $standby_respondents = [];
            if ($include_standby) {
                $standby_respondents = JobEvent::join('respondents', 'respondents.id', '=', 'job_events.respondent_id')
                ->whereNull('respondents.deleted_at')
                ->where('project_id', $project->id)->where('group_id', $id)
                    ->where('name', 'Standby')->get();
            }
            $group = ProjectGroup::find($id);

            $phpWord = new \PhpOffice\PhpWord\PhpWord();

            $sectionStyle = array(
                'orientation' => 'portrait',
                'marginTop' => 400,
                'marginLeft' => 400,
                'marginRight' => 400
            );

            $styleTable = array('borderSize' => 6, 'borderColor' => 'ec008b', 'cellMargin' => 50);
            $styleFirstRow = array('borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '');
            $phpWord->addTitleStyle(1, array('bold' => true), array('spaceAfter' => 240));
            $section = $phpWord->addSection($sectionStyle);

            $header = $section->addHeader();
            $table = $header->addTable();
            $table->addRow();

            $source = file_get_contents(asset('public/assets/img/tns.png'), false, stream_context_create(["ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )]));


            $table->addCell(7000, array('vMerge' => 'restart'))->addImage(
                $source,
                array(
                    'width' => 100,
                    'height' => 80,
                    'wrappingStyle' => 'square',
                    'positioning' => 'absolute',
                )
            );

            $table->addCell(4500, array('align' => 'right'))->addText('Level 6, 30 Hickson Rd,', array('bold' => true, 'size' => 9, 'align' => 'right'));
            $table->addRow();
            $table->addCell(7000, array('vMerge' => 'continue'));
            $table->addCell(4500, array('align' => 'right'))->addText('Millers Point, NSW', array('bold' => true, 'size' => 9, 'align' => 'right'));
            $table->addRow();
            $table->addCell(7000, array('vMerge' => 'continue'));
            $table->addCell(4500, array('align' => 'right'))->addText('Australia', array('bold' => true, 'size' => 9, 'align' => 'right'));
            $table->addRow();
            $table->addCell(7000, array('vMerge' => 'continue'));
            $table->addCell(4500, array('align' => 'right'))->addText('t  +61 2 9563 4200', array('bold' => true, 'size' => 9, 'align' => 'right'));
            $table->addRow();
            $table->addCell(7000, array('vMerge' => 'continue'));
            $table->addCell(4500, array('align' => 'right'))->addText('f +61 2 9563 4202', array('bold' => true, 'size' => 9, 'align' => 'right'));

            $paragraphStyle = array('lineHeight' => '0.9');
            $section->addTitle('Respondent Validation Report ', 1, array('bold' => true, 'align' => 'center', 'size' => 16));

            $phpWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

            // Add table
            $table = $section->addTable('myOwnTableStyle');
            $noSpace = array('spaceAfter' => 0);
            $table->addRow();
            $table->addCell(7000)->addText('Job Name: ' . $project->job_name, array('bold' => true), $noSpace);
            $table->addCell(7000)->addText('Job Number: ' . $project->job_number, array('bold' => true), $noSpace);

            $table->addRow();
            $table->addCell(7000)->addText('Date: ' . date('Y-m-d', strtotime($group->group_date)), '', $noSpace);
            $table->addCell(7000)->addText('Time: ' . $group->group_time, '', $noSpace);

            $table->addRow();
            $table->addCell(7000, array('gridSpan' => 2))->addText('Venue/Location:' . $group->venue_address, '', $noSpace);

            $table->addRow();
            $table->addCell(7000)->addText('Recruiter: Ava Research Pty Ltd', '', $noSpace);
            $table->addCell(7000)->addText('Recruiter Phone No: 0433 190 233 or 02 9719 2990', '', $noSpace);

            $table->addRow();
            $table->addCell(7000)->addText('Group Name: ' . $group->name, '', $noSpace);
            $table->addCell(7000)->addText('# of Respondents: ' . count($all_respondents), '', $noSpace);

            $section->addTitle('', 1, 0);
            $section->addListItem(htmlspecialchars('I agree that I am the person whose signature appears below.'), 0, 0);
            $section->addListItem(htmlspecialchars('I agree that the questions answered by me at the time of recruitment were answered honestly and to the best of my knowledge.'), 0, 0);
            $section->addListItem(htmlspecialchars('I give consent to the research study being recorded/transcripted/video recorded and/or photographed.  A copy of the transcript/audio/video tape/photographs may be read/heard/seen by people who are members of the national/international client company (ies) working on this project.  Excerpts from the transcript/audio or video tape/photographs may be used to illustrate the research findings in reporting to the client.  The recordings will not be used for public broadcast, and respondents will not be identified by name.'), 0, 0);
            $section->addListItem(htmlspecialchars('I agree that all information discussed at this research session is private and confidential and at no time may this information be disclosed to a third party without the written permission from the researcher.'), 0, 0);
            $section->addListItem(htmlspecialchars('I agree that any monies or gratuities received by me regarding the research study are accepted as compensation for time and expenses incurred and are accepted wholly for a private and domestic nature. Please contact your recruiter direct with any payment queries.'), 0, 0);
            $section->addListItem(htmlspecialchars('Accordingly please ensure no withholding tax is deducted from the payment in accordance with section 12-190 in schedule 1 of the Taxation Administration Act 1953.'), 0, 0);

            $phpWord->addTableStyle('Table1', $styleTable, $styleFirstRow);
            $table1 = $section->addTable('Table1');

            $table1->addRow(null, array('tblHeader' => true, 'cantSplit' => true));

            $table1->addCell(500)->addText('S.No.', array('bold' => true), $noSpace);
            $table1->addCell(500)->addText('Name', array('bold' => true), $noSpace);
            $table1->addCell(500)->addText('Signature', array('bold' => true), $noSpace);
            $table1->addCell(3000)->addText('Mobile #', array('bold' => true), $noSpace);


            foreach ($all_respondents as $key => $item) {
                $table1->addRow();
                $table1->addCell(500)->addText($key + 1, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(500)->addText(htmlspecialchars($item->respondent->first_name . ' ' . $item->respondent->last_name), '', array('spaceAfter' => 0));
                $table1->addCell(4000)->addText('', '', array('spaceAfter' => 0));
                $table1->addCell(4000)->addText(htmlspecialchars($item->respondent->detail->mobile_phone), '', array('spaceAfter' => 0));
            }

            if (count($standby_respondents)) {
                $section->addTextBreak();
                $section->addText('Stand by Respondent', array('bold' => true), $paragraphStyle);
                $phpWord->addTableStyle('Table2', $styleTable, $styleFirstRow);
                $table2 = $section->addTable('Table2');

                $table2->addRow(null, array('tblHeader' => true, 'cantSplit' => true));
                $table2->addCell(500)->addText('S.No.', array('bold' => true), $noSpace);
                $table2->addCell(500)->addText('Time', array('bold' => true), $noSpace);

                $table2->addCell(3000)->addText('ID', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('First Name', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('Last Name', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('Mobile Phone', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('E-Mail', array('bold' => true), $noSpace);

                $table2->addCell(3000)->addText('Signature', array('bold' => true), $noSpace);

                foreach ($standby_respondents as $key => $item) {
                    $table2->addRow();
                    $table2->addCell(500)->addText($key + 1, array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(500)->addText(htmlspecialchars($group->group_time), array('bold' => true), array('spaceAfter' => 0));

                    $table2->addCell(500)->addText($item->respondent->id, array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(500)->addText(htmlspecialchars($item->respondent->first_name), array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(500)->addText(htmlspecialchars($item->respondent->last_name), array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(500)->addText(htmlspecialchars($item->respondent->detail->mobile_phone), array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(500)->addText($item->respondent->email, array('bold' => true), array('spaceAfter' => 0));

                    $table2->addCell(3000)->addText('', '', array('spaceAfter' => 0));
                }

                $section->addText("", '', array('lineHeight' => '1'));
                $section->addText("Moderator to complete for ISO 20252:2012", array('bold' => true), array('lineHeight' => '1'));
                $section->addText("", '', array('lineHeight' => '1'));
                $section->addText("A: # attended ______________ B: # attended ______________ C: # attended ______________ Moderator Signature ______________", '', $paragraphStyle);
            } else {
                $section->addText("", '', array('lineHeight' => '1'));
                $section->addText("Moderator to complete for ISO 20252:2012", array('bold' => true), array('lineHeight' => '1'));
                $section->addText("", '', array('lineHeight' => '1'));
                $section->addText("A: # attended ______________ B: # attended ______________ C: # attended ______________", '', $paragraphStyle);
                $section->addText("Moderator Signature ______________", '', array('lineHeight' => '1'));
            }

            $footer = $section->createFooter();
            $footer->addPreserveText('Qualitative_Respondent Participation Signature Validation Report_v20150909', '', '');

            $filename = $project->job_name . "_" . preg_replace("/[^a-zA-Z0-9\s]/ ", "_", $group->name) . '.docx';
            date_default_timezone_set("Australia/Sydney");
            $date = date('Y-m-d H:i:s');

            if (file_exists(storage_path('app/public/docs/rvr/' . $filename))) unlink(storage_path('app/public/docs/rvr/' . $filename));

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $url = storage_path('app/public/docs/rvr/' . $filename);
            $objWriter->save($url, true);

            $attendees = implode(',', $ids);
            $document_name = 'docs/rvr/' . $filename;

            $attendee_document = ProjectAttendeeDocument::where('project_id', $project->id)
                ->where('document_name', $document_name)->first();
            if ($attendee_document) {
                $attendee_document->update(['created_by' => auth()->id(), 'attendees' => $attendees]);
            } else {
                ProjectAttendeeDocument::create([
                    'project_id' => $project->id, 'group_id' => $id, 'attendees' => $attendees, 'document_name' => $document_name, 'created_by' => auth()->id()
                ]);
            }
        }
    }

    public function regular_document($project, $group_ids, $ids, $include_standby)
    {
        foreach ($group_ids as $id) {
            $all_respondents = JobEvent::join('respondents', 'respondents.id', '=', 'job_events.respondent_id')
                ->whereNull('respondents.deleted_at')
                ->where('project_id', $project->id)->where('group_id', $id)
                ->whereIn('job_events.id', $ids)->whereIn('name', ['Screened OK', 'Qualified', 'Screened OK – Quotas Full'])->get();

            $standby_respondents = [];
            if ($include_standby) {
                $standby_respondents = JobEvent::join('respondents', 'respondents.id', '=', 'job_events.respondent_id')
                ->whereNull('respondents.deleted_at')
                ->where('project_id', $project->id)->where('group_id', $id)
                    ->where('name', 'Standby')->get();
            }
            $group = ProjectGroup::find($id);

            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            $sectionStyle = array(
                'orientation' => 'portrait',
                'marginTop' => 400,
                'marginLeft' => 400,
                'marginRight' => 400
            );
            $docgrouptime = ($group->group_time == '00:00:00' || $group->group_time == null) ? 'Various' : $group->group_time;
            $styleTable = array('borderSize' => 6, 'borderColor' => '006699', 'cellMargin' => 50);
            $styleFirstRow = array('borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF');
            $phpWord->addTitleStyle(1, array('bold' => true), array('spaceAfter' => 240));
            $section = $phpWord->addSection($sectionStyle);
            //dd(asset('public/assets/img/logo.png'));
            $source = file_get_contents(asset('public/assets/img/logo.png'), false, stream_context_create(["ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )]));
            $section->addImage($source, array('width' => 180, 'height' => 50, 'align' => 'center'));
            $paragraphStyle = array('lineHeight' => '0.9');
            $section->addTitle('Respondent Validation Report:       Project No: ' . $project->job_number . '        Date: ' . date('d-m-Y', strtotime($group->group_date)) . '       Time: ' . $docgrouptime . '', 1, 0);

            $phpWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

            // Add table
            $table = $section->addTable('myOwnTableStyle');
            $noSpace = array('spaceAfter' => 0);
            $table->addRow();
            $table->addCell(7000)->addText('Recruiter: Ava Research Pty. Ltd.', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText('Attn: ' . htmlspecialchars($project->client->profile->first_name) . ' ' . htmlspecialchars($project->client->profile->last_name), array('bold' => true), $noSpace);

            $table->addRow();
            $table->addCell(7000)->addText('Ph: 02 8072 9959 or 0433 153 473', array('bold' => true), $noSpace);

            $table->addCell(7000)->addText('Client Ph: ' . htmlspecialchars($group->client_phone), array('bold' => true), $noSpace);


            $table->addRow();
            $table->addCell(7000)->addText('Duration: ' . htmlspecialchars($group->duration), array('bold' => true), $noSpace);

            $table->addCell(7000)->addText('Venue: ' . htmlspecialchars($group->venue_address), array('bold' => true), $noSpace);

            $table->addRow();
            $table->addCell(7000)->addText('Group Name: ' . htmlspecialchars($group->name), array('bold' => true), $noSpace);

            $table->addCell(7000)->addText('Respondents: ' . count($all_respondents), array('bold' => true), $noSpace);
            $table->addRow();
            $table->addCell(7000)->addText('Incentive : ' . htmlspecialchars($group->incentive), array('bold' => true), $noSpace);
            $table->addCell(7000)->addText('Email : ' . htmlspecialchars($group->client_email), array('bold' => true), $noSpace);

            $section->addTextBreak();
            $section->addTitle('Respondents:', 1, 0);
            $section->addListItem(htmlspecialchars('I agree that I am the person directly recruited and whose signature appears below.'), 0, 0);
            $section->addListItem(htmlspecialchars('I acknowledge that any monies or gratuities received in return for my participation in this research are accepted as compensation for the time and expenses incurred by me and are wholly of a private and non-business nature.'), 0, 0);
            $section->addListItem(htmlspecialchars('I agree that the questions answered by me at the time of recruitment were answered honestly and to the best of my knowledge.'), 0, 0);
            $section->addListItem(htmlspecialchars('I give consent to the research study being viewed and video/audio recorded in any format provided it is not to be used for commercial purpose, and will not be used for any purpose other than this research study.'), 0, 0);
            $section->addListItem(htmlspecialchars('I agree not to disclose to the public any information about the matters discussed or the people attending. '), 0, 0);

            $phpWord->addTableStyle('Table1', $styleTable, $styleFirstRow);
            $table1 = $section->addTable('Table1');

            $table1->addRow(null, array('tblHeader' => true, 'cantSplit' => true));
            $table1->addCell(500)->addText('S.No.', array('bold' => true), $noSpace);
            $table1->addCell(500)->addText('Time', array('bold' => true), $noSpace);

            $table1->addCell(3000)->addText('ID', array('bold' => true), $noSpace);
            $table1->addCell(3000)->addText('First Name', array('bold' => true), $noSpace);
            $table1->addCell(3000)->addText('Last Name', array('bold' => true), $noSpace);
            $table1->addCell(3000)->addText('Mobile Phone', array('bold' => true), $noSpace);
            $table1->addCell(3000)->addText('E-Mail', array('bold' => true), $noSpace);

            $table1->addCell(3000)->addText('Signature', array('bold' => true), $noSpace);


            foreach ($all_respondents as $key => $item) {
                $table1->addRow();
                $table1->addCell(500)->addText($key + 1, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(500)->addText($item->group_time, array('bold' => true), array('spaceAfter' => 0));

                $table1->addCell(3000)->addText($item->respondent->id, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(3000)->addText($item->respondent->first_name, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(3000)->addText($item->respondent->last_name, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(3000)->addText($item->respondent->detail->mobile_phone, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(3000)->addText($item->respondent->email, array('bold' => true), array('spaceAfter' => 0));

                $table1->addCell(3000)->addText('', '', array('spaceAfter' => 0));
            }

            if (count($standby_respondents)) {
                $section->addTextBreak();
                $section->addText('Stand by Respondent', array('bold' => true), $paragraphStyle);
                $phpWord->addTableStyle('Table2', $styleTable, $styleFirstRow);
                $table2 = $section->addTable('Table2');

                $table2->addRow(null, array('tblHeader' => true, 'cantSplit' => true));
                $table2->addCell(3000)->addText('S.No.', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('Time', array('bold' => true), $noSpace);

                $table2->addCell(3000)->addText('ID', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('First Name', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('Last Name', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('Mobile Phone', array('bold' => true), $noSpace);
                $table2->addCell(3000)->addText('E-Mail', array('bold' => true), $noSpace);

                $table2->addCell(3000)->addText('Signature', array('bold' => true), $noSpace);

                foreach ($standby_respondents as $key => $item) {
                    $table2->addRow();
                    $table2->addCell(500)->addText($key + 1, array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(500)->addText($item->group_time, array('bold' => true), array('spaceAfter' => 0));

                    $table2->addCell(3000)->addText($item->respondent->id, array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(3000)->addText($item->respondent->first_name, array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(3000)->addText($item->respondent->last_name, array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(3000)->addText($item->respondent->detail->mobile_phone, array('bold' => true), array('spaceAfter' => 0));
                    $table2->addCell(3000)->addText($item->respondent->email, array('bold' => true), array('spaceAfter' => 0));

                    $table2->addCell(3000)->addText('', '', array('spaceAfter' => 0));
                }
            }

            $section->addTextBreak();
            $section->addText("Researcher's Comments: ______________________", array('bold' => true), $paragraphStyle);
            $section->addText("Please email signed sheets to dhun@avaresearch.com.au", array('bold' => true), $paragraphStyle);

            $filename = $project->job_name . "_" . preg_replace("/[^a-zA-Z0-9\s]/ ", "_", htmlspecialchars($group->name)) . '.docx';
            date_default_timezone_set("Australia/Sydney");
            $date = date('Y-m-d H:i:s');

            if (file_exists(storage_path('app/public/docs/rvr/' . $filename))) unlink(storage_path('app/public/docs/rvr/' . $filename));

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $url = storage_path('app/public/docs/rvr/' . $filename);
            $objWriter->save($url, true);

            $attendees = implode(',', $ids);
            $document_name = 'docs/rvr/' . $filename;

            $attendee_document = ProjectAttendeeDocument::where('project_id', $project->id)
                ->where('document_name', $document_name)->first();
            if ($attendee_document) {
                $attendee_document->update(['created_by' => auth()->id(), 'attendees' => $attendees]);
            } else {
                ProjectAttendeeDocument::create([
                    'project_id' => $project->id, 'group_id' => $id, 'attendees' => $attendees, 'document_name' => $document_name, 'created_by' => auth()->id()
                ]);
            }
        }
    }

    public function generate(Request $request, $projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $ids = $request->ids;

        $include_standby = $request->include_standby;

        $job_events = JobEvent::join('respondents', 'respondents.id', '=', 'job_events.respondent_id')
                ->whereIn('job_events.id', $ids)
                ->whereNull('respondents.deleted_at')
                ->get();
                
        $group_ids = [];
        foreach ($job_events as $event) {
            $group_ids[] = $event->group_id;
        }
        $group_ids = array_unique($group_ids);

        if (isset($project->client) && $project->client->company_name == 'TNS')
            $this->tns_document($project, $group_ids, $ids, $include_standby);
        else {
            $this->regular_document($project, $group_ids, $ids, $include_standby);
        }

        return response()->json(1);
    }
}
