<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Project;
use App\Models\Project\JobEvent;
use App\Models\ProjectGroup;
use App\Models\Project\ProjectBankDocument;

class ProjectBankDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function regular_document($project, $group_ids, $ids, $include_standby)
    {
        foreach ($group_ids as $id) {
            $all_respondents = JobEvent::join('respondents', 'respondents.id', '=', 'job_events.respondent_id')
                ->whereNull('respondents.deleted_at')
                ->where('project_id', $project->id)->where('group_id', $id)
                ->whereIn('job_events.id', $ids)->whereIn('name', ['Screened OK', 'Qualified', 'Screened OK – Quotas Full'])->get();
            $standby_respondents = [];
            if ($include_standby) {
                $standby_respondents = JobEvent::join('respondents', 'respondents.id', '=', 'job_events.respondent_id')
                ->whereNull('respondents.deleted_at')
                ->where('project_id', $project->id)->where('group_id', $id)
                    ->where('name', 'Standby')->get();
            }
            $group = ProjectGroup::find($id);

            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            $sectionStyle = array(
                'orientation' => 'portrait',
                'marginTop' => 400,
                'marginLeft' => 400,
                'marginRight' => 400
            );

            $docgrouptime = ($group->group_time == '00:00:00' || $group->group_time == null) ? 'Various' : $group->group_time;
            $styleTable = array('borderSize' => 6, 'borderColor' => '006699', 'cellMargin' => 80);
            $styleFirstRow = array('borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF');
            $phpWord->addTitleStyle(1, array('bold' => true), array('spaceAfter' => 240));
            $section = $phpWord->addSection($sectionStyle);

            $source = file_get_contents(asset('public/assets/img/logo.png'), false, stream_context_create(["ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            )]));
            $section->addImage($source, array('width' => 180, 'height' => 50, 'align' => 'center'));
            
            $paragraphStyle = array('lineHeight' => '0.9');
            $section->addTitle('Respondent Validation Report:       Project No: ' . $project->job_number . '        Date: ' . date('d-m-Y', strtotime($group->group_date)) . '       Time: ' . $docgrouptime . '', 1, 0);

            $phpWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);

            // Add table
            $table = $section->addTable('myOwnTableStyle');
            $noSpace = array('spaceAfter' => 0);
            $table->addRow();
            $table->addCell(7000)->addText('Recruiter: Ava Research Pty. Ltd.', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText('Attn: ' . htmlspecialchars($project->client->profile->first_name) . ' ' . htmlspecialchars($project->client->profile->last_name) . '', array('bold' => true), $noSpace);

            $table->addRow();
            $table->addCell(7000)->addText('Ph: 02 8072 9988', array('bold' => true), $noSpace);
            $table->addCell(7000)->addText('Client Ph: ' . htmlspecialchars($group->client_phone), array('bold' => true), $noSpace);

            $table->addRow();
            $table->addCell(7000)->addText('Duration: ' . htmlspecialchars($group->duration), array('bold' => true), $noSpace);
            $table->addCell(7000)->addText('Venue: ' . htmlspecialchars($group->venue_address), array('bold' => true), $noSpace);

            $table->addRow();
            $table->addCell(7000)->addText(htmlspecialchars($group->name), array('bold' => true), $noSpace);
            $table->addCell(7000)->addText('Respondents: ' . count($all_respondents), array('bold' => true), $noSpace);
            $table->addRow();
            $table->addCell(7000)->addText('Incentive : ' .  htmlspecialchars($group->incentive), array('bold' => true), $noSpace);
            $table->addCell(7000)->addText('Email : ' . htmlspecialchars($group->client_email), array('bold' => true), $noSpace);

            $section->addTitle('Respondents:', 1, 0);
            $section->addListItem(htmlspecialchars('I agree that I am the person directly recruited and whose signature appears below.'), 0, 0);
            $section->addListItem(htmlspecialchars('I acknowledge that any monies or gratuities received in return for my participation in this research are accepted as compensation for the time and expenses incurred by me and are wholly of a private and non-business nature.'), 0, 0);
            $section->addListItem(htmlspecialchars('I agree that the questions answered by me at the time of recruitment were answered honestly and to the best of my knowledge.'), 0, 0);
            $section->addListItem(htmlspecialchars('I give consent to the research study being viewed and video/audio recorded in any format provided it is not to be used for commercial purpose, and will not be used for any purpose other than this research study.'), 0, 0);
            $section->addListItem(htmlspecialchars('I agree not to disclose to the public any information about the matters discussed or the people attending. '), 0, 0);

            $phpWord->addTableStyle('Table1', $styleTable, $styleFirstRow);
            $table1 = $section->addTable('Table1');

            $table1->addRow(null, array('tblHeader' => true, 'cantSplit' => true));
            $table1->addCell(500)->addText('S.No.', array('bold' => true), $noSpace);

            $tab_cell = $table1->addCell(3000);
            $tab_cell->addText('Id. No.', array('bold' => true), $noSpace);
            $tab_cell = $table1->addCell(3000);
            $tab_cell->addText('Name', array('bold' => true), $noSpace);
            $tab_cell = $table1->addCell(3000);
            $tab_cell->addText('Mobile Phone', array('bold' => true), $noSpace);
            $tab_cell = $table1->addCell(3000);
            $tab_cell->addText('BSB', array('bold' => true), $noSpace);
            $tab_cell = $table1->addCell(3000);
            $tab_cell->addText('Account Number', array('bold' => true), $noSpace);
            $tab_cell = $table1->addCell(3000);
            $tab_cell->addText('Email', array('bold' => true), $noSpace);
            $tab_cell = $table1->addCell(3000);
            $tab_cell->addText('Date of Study', array('bold' => true), $noSpace);
            $tab_cell = $table1->addCell(3000);
            $tab_cell->addText('Paid On', array('bold' => true), $noSpace);

            foreach ($all_respondents as $key => $item) {
                $table1->addRow();

                $table1->addCell(500)->addText($key + 1, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(500)->addText($item->id, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(500)->addText($item->respondent->first_name . ' ' . $item->respondent->last_name, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(500)->addText($item->respondent->detail->mobile_phone, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(500)->addText('', '', array('spaceAfter' => 0));
                $table1->addCell(500)->addText('', '', array('spaceAfter' => 0));
                $table1->addCell(500)->addText($item->respondent->email, array('bold' => true), array('spaceAfter' => 0));
                $table1->addCell(500)->addText('', '', array('spaceAfter' => 0));
                $table1->addCell(500)->addText('', '', array('spaceAfter' => 0));
            }

            $filename = $project->job_name . "_" . preg_replace("/[^a-zA-Z0-9\s]/ ", "_", htmlspecialchars($group->name)) . '.docx';
            date_default_timezone_set("Australia/Sydney");
            $date = date('Y-m-d H:i:s');

            if (file_exists(storage_path('docs/bank/' . $filename))) unlink(storage_path('docs/bank/' . $filename));

            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            $url = storage_path('app/public/docs/bank/' . $filename);
            $objWriter->save($url, true);

            $attendees = implode(',', $ids);
            $document_name = 'docs/bank/' . $filename;

            $attendee_document = ProjectBankDocument::where('project_id', $project->id)
                ->where('document_name', $document_name)->first();
            if ($attendee_document) {
                $attendee_document->update(['created_by' => auth()->id(), 'attendees' => $attendees]);
            } else {
                ProjectBankDocument::create([
                    'project_id' => $project->id, 'group_id' => $id, 'attendees' => $attendees, 'document_name' => $document_name, 'created_by' => auth()->id()
                ]);
            }
        }
    }

    public function generate(Request $request, $projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $ids = $request->ids;
        $include_standby = $request->include_standby;

        $job_events = JobEvent::join('respondents', 'respondents.id', '=', 'job_events.respondent_id')
                ->whereNull('respondents.deleted_at')
                ->whereIn('job_events.id', $ids)->get();

        $group_ids = [];
        foreach ($job_events as $event) {
            $group_ids[] = $event->group_id;
        }
        $group_ids = array_unique($group_ids);

        $this->regular_document($project, $group_ids, $ids, $include_standby);

        return response()->json(1);
    }
}
