<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Project\SaveQuery;
use App\Models\Respondents\Respondent;
use App\Models\Respondents\RespondentChildren;

use App\Models\Masters\MasterBrand;
use App\Models\Masters\MasterMobileProvider;
use App\Models\Masters\MasterState;
use App\Models\Masters\MasterPostcode;
use App\Models\Masters\MasterBirthCountry;
use App\Models\Masters\MasterEthnicBackground;
use App\Models\Masters\MasterOccupation;
use App\Models\Masters\MasterEmployerSize;
use App\Models\Masters\MasterIndustry;
use App\Models\Masters\MasterHealthProblem;
use App\Models\Masters\MasterHealthFund;
use App\Models\Masters\MasterBank;
use App\Models\Masters\MasterSuperannuation;
use App\Models\Masters\MasterInsuranceProvider;
use App\Models\Masters\MasterInsurancePolicy;
use App\Models\Masters\MasterPhoneProvider;
use App\Models\Masters\MasterVehicleMake;
use App\Models\Masters\MasterVehicleType;
use App\Models\Masters\MasterRewardProgram;
use App\Models\Masters\MasterTechnologyUse;
use App\Models\Masters\MasterInternetProvider;
use App\Models\Masters\MasterAlcoholConsumed;
use App\Models\Masters\MasterPreferredResearchLocation;
use App\Models\Masters\MasterResidencyStatus;

use App\User;
use App\Models\EmailTemplate;
use App\Models\Project\ProjectEmailTemplate;
use DB;
use Excel;
use App\Exports\SearchRespondents;

class ProjectSearchRespondentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search_init($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $respondent_columns = DB::getSchemaBuilder()->getColumnListing('respondents');
        $respondent_details_columns = DB::getSchemaBuilder()->getColumnListing('respondent_details');
        $respondent_households_columns = DB::getSchemaBuilder()->getColumnListing('respondent_households');
        $respondent_occupations_columns = DB::getSchemaBuilder()->getColumnListing('respondent_occupations');
        $respondent_healths_columns = DB::getSchemaBuilder()->getColumnListing('respondent_healths');
        $respondent_services_columns = DB::getSchemaBuilder()->getColumnListing('respondent_services');
        $respondent_products_columns = DB::getSchemaBuilder()->getColumnListing('respondent_products');
        $respondent_summaries_columns = DB::getSchemaBuilder()->getColumnListing('respondent_summaries');

        $columns = array_merge(
            $respondent_columns,
            $respondent_details_columns,
            $respondent_healths_columns,
            $respondent_households_columns,
            $respondent_occupations_columns,
            $respondent_services_columns,
            $respondent_products_columns,
            $respondent_summaries_columns
        );

        $removed_columns = [
            'id', 'uuid', 'deleted_at', 'created_by','deleted_by', 'respondent_id', 'password',
            'brands', 'children_allowed', 'children_seperate', 'delete_reson', 'donated_organ', 'excellent_candidate', 'isactive',
            'keen_candidate', 'last_modified_by', 'login_status', 'online_survey', 'organ_donate', 'orther_brands', 'other_country',
            'other_health_fund', 'other_partner_brands', 'other_vehicle2_type', 'other_vehicle_type', 'partner_cigarette_brands',
            'remember_token', 'stop_often_email', 'type', 'unsub_at', 'unsub_reason', 'wrong_number', 'other_health_problem',
            'other_home_phone_provider', 'other_work_phone_provider'
        ];

        // $columns = array_filter($columns, function ($value) {
        //     return $value != 'id' && $value != 'uuid' && $value != 'deleted_at'
        //         && $value != 'created_by' && $value != 'respondent_id' && $value != 'password';
        // });

        $columns = array_filter($columns, function ($value) use ($removed_columns) {
            if (!in_array($value, $removed_columns)) return $value;
        });


        $columns = array_unique($columns);
        sort($columns);

        //dd($columns);

        // unset($columns[7]);
        // unset($columns[9]);
        // unset($columns[10]);
        // unset($columns[18]);
        // unset($columns[23]);
        // unset($columns[33]);
        // unset($columns[55]);
        // unset($columns[57]);
        // unset($columns[58]);
        // unset($columns[61]);
        // //unset($columns[73]);
        // unset($columns[75]);
        // //unset($columns[76]);
        // unset($columns[77]);
        // //unset($columns[78]);
        // unset($columns[79]);
        // unset($columns[80]);
        // unset($columns[81]);
        // unset($columns[82]);
        // unset($columns[83]);
        // unset($columns[84]);
        // unset($columns[86]);
        // //unset($columns[90]);
        // //unset($columns[103]);
        // unset($columns[99]);
        // unset($columns[107]);
        // unset($columns[114]);
        // unset($columns[115]);
        // unset($columns[116]);
        // // unset($columns[112]);
        // unset($columns[131]);

        $columns[] = 'age';
        $columns[] = 'children_name_and_age';
        $columns[] = 'job_event';
        $columns[] = 'occupation_type';
        //$columns[] = 'partner_smoke_type';
        $columns[] = 'email_template';
        $columns[] = 'all_events';
        $columns[] = 'previous_events';

        $columns = array_map(function ($value) {
            $value = strpos($value, '_id') !== false ? str_replace('_id', '', $value) : $value;
            if ($value == 'created_at') return 'date_created';
            else if ($value == 'updated_at') return 'last_updated';
            else return $value;
        }, $columns);


        sort($columns);

        //dd($columns);

        $filters_json = json_decode(file_get_contents(storage_path('app/public/json/respondent-filters.json')));
        $pageSize = 100;

        $master_alcohol_consumed = MasterAlcoholConsumed::where('isactive', 1)->get();
        $masters['alcohol_consumed'] = $master_alcohol_consumed;

        $master_banks = MasterBank::where('isactive', 1)->get();
        $masters['banks'] = $master_banks;

        $master_birth_country = MasterBirthCountry::where('isactive', 1)->get();
        $masters['birth_country'] = $master_birth_country;

        $master_origin_country = MasterBirthCountry::where('isactive', 1)->get();
        $masters['original_country'] = $master_origin_country;

        $ethnic_background = MasterEthnicBackground::select('name')->distinct()->where('isactive', 1)->orderBy('name', 'ASC')->get();
        $masters['ethnic_background'] = $ethnic_background;

        $health_funds = MasterHealthFund::where('isactive', 1)->get();
        $masters['health_funds'] = $health_funds;

        $health_problems = MasterHealthProblem::where('isactive', 1)->get();
        $masters['health_problems'] = $health_problems;

        $industry_id = MasterIndustry::where('isactive', 1)->get();
        $masters['industry_id'] = $industry_id;

        $insurance_providers = MasterInsuranceProvider::where('isactive', 1)->get();
        $masters['insurance_providers'] = $insurance_providers;

        $insurance_policies = MasterInsurancePolicy::where('isactive', 1)->get();
        $masters['insurance_policies'] = $insurance_policies;

        $occupation_id = MasterOccupation::where('isactive', 1)->get();
        $masters['occupation_id'] = $occupation_id;

        $clients = User::where('type', 'client')->get();

        $masters['partner_occupation_id'] = $occupation_id;

        $states = MasterState::where('isactive', 1)->get();
        $masters['state'] = $states;

        $superannuation_funds = MasterSuperannuation::where('isactive', 1)->get();
        $masters['superannuation_id'] = $superannuation_funds;

        $vehicle_make = MasterVehicleMake::where('isactive', 1)->get();
        $masters['vehicle_make_id'] = $vehicle_make;

        $employer_size_id = MasterEmployerSize::where('isactive', 1)->get();
        $masters['employer_size_id'] = $employer_size_id;
        $masters['partner_employer_size_id'] = $employer_size_id;

        $phone_providers = MasterPhoneProvider::where('isactive', 1)->get();
        $masters['home_phone_provider'] = $phone_providers;
        $masters['work_phone_provider'] = $phone_providers;
        $masters['mobile1_provider'] = $phone_providers;

        $job_events_json = json_decode(file_get_contents(storage_path('app/public/json/job-events.json')));
        $masters['job_event'] = $job_events_json;
        $job_events_json = (string) json_encode($job_events_json);

        $brands = MasterBrand::where('isactive', 1)->get();
        $masters['brands'] = $brands;

        $countries = MasterBirthCountry::where('id', '!=', 238)->where('isactive', 1)->orderBy('name')->get();
        $masters['countries'] = $countries;

        $residency_status = MasterResidencyStatus::where('isactive', 1)->get();
        $masters['residency_status'] = $residency_status;

        $vehicle_type = MasterVehicleType::where('isactive', 1)->orderBy('name')->get();
        $masters['vehicle_type_id'] = $vehicle_type;

        //editData
        $editData = [];
        $editData['occupations'] = [];
        $editData['occupations'][] = ['id' => '', 'name' => 'Select'];
        foreach ($occupation_id as $item) {
            $editData['occupations'][] = ['id' => (string) $item->id, 'name' => $item->name];
        }

        $editData['birth_country'] = [];
        $editData['birth_country'][] = ['id' => '', 'name' => 'Select'];
        foreach ($master_birth_country as $item) {
            $editData['birth_country'][] = ['id' => (string) $item->id, 'name' => $item->name];
        }

        $editData['ethnic_background'] = [];
        $editData['ethnic_background'][] = ['id' => '', 'name' => 'Select'];
        foreach ($ethnic_background as $item) {
            $editData['ethnic_background'][] = ['id' => $item->name, 'name' => $item->name];
        }

        $editData['highest_education'] = [];
        $editData['highest_education'][] = ['id' => '', 'name' => 'Select'];
        $editData['highest_education'][] = ['id' => 'School Certificate (year 10)', 'name' => 'School Certificate (year 10)'];
        $editData['highest_education'][] = ['id' => 'Higher School Certificate (Year 12', 'name' => 'Higher School Certificate (Year 12'];
        $editData['highest_education'][] = ['id' => 'Cert 1-3 qualification', 'name' => 'Cert 1-3 qualification'];
        $editData['highest_education'][] = ['id' => 'Cert 4 qualification', 'name' => 'Cert 4 qualification'];
        $editData['highest_education'][] = ['id' => 'Diploma/Advanced Dimploma', 'name' => 'Diploma/Advanced Dimploma'];
        $editData['highest_education'][] = ['id' => 'Undergraduate Degree', 'name' => 'Undergraduate Degree'];
        $editData['highest_education'][] = ['id' => 'Graduate', 'name' => 'Graduate'];
        $editData['highest_education'][] = ['id' => 'Post-graduation Degree', 'name' => 'Post-graduation Degree'];

        $editData['profession'] = [];
        $editData['profession'][] = ['id' => '', 'name' => 'Select'];
        $editData['profession'][] = ['id' => 'Student', 'name' => 'Student'];
        $editData['profession'][] = ['id' => 'Business Owner', 'name' => 'Business Owner'];
        $editData['profession'][] = ['id' => 'Retired', 'name' => 'Retired'];
        $editData['profession'][] = ['id' => 'Currenty Unemployed', 'name' => 'Currenty Unemployed'];
        $editData['profession'][] = ['id' => 'Stay at home Parent', 'name' => 'Stay at home Parent'];
        $editData['profession'][] = ['id' => 'employed', 'name' => 'employed'];
        $editData['profession'][] = ['id' => 'Sole Trader', 'name' => 'Sole Trader'];

        $editData['cigarette_brands'] = [];
        $editData['cigarette_brands'][] = ['id' => '', 'name' => 'Select'];
        $cigarette_brands = MasterBrand::where('isactive', 1)->get();
        foreach ($cigarette_brands as $item) {
            $editData['cigarette_brands'][] = ['id' => $item->name, 'name' => $item->name];
        }

        // $editData['cigarette_brands'][] = ['id' => "Doesn't Smoke", 'name' => "Doesn't Smoke"];
        // $editData['cigarette_brands'][] = ['id' => "Regular Smoker", 'name' => "Regular Smoker"];
        // $editData['cigarette_brands'][] = ['id' => "Social Smoker", 'name' => "Social Smoker"];
        // $editData['cigarette_brands'][] = ['id' => "Quit Smoking", 'name' => "Quit Smoking"];

        $editData['mobile1_type'] = [];
        $editData['mobile1_type'][] = ['id' => '', 'name' => 'Select'];
        $editData['mobile1_type'][] = ['id' => "Pre-paid", 'name' => "Pre-paid"];
        $editData['mobile1_type'][] = ['id' => "Post-paid/Contract", 'name' => "Post-paid/Contract"];

        $editData['alcohol_consumed'] = [];
        $editData['alcohol_consumed'][] = ['id' => '', 'name' => 'Select'];
        foreach ($master_alcohol_consumed as $item) {
            $editData['alcohol_consumed'][] = ['id' => (string) $item->id, 'name' => $item->name];
        }

        $editData['health_problems'] = [];
        $editData['health_problems'][] = ['id' => '', 'name' => 'Select'];
        foreach ($health_problems as $item) {
            $editData['health_problems'][] = ['id' => (string) $item->id, 'name' => $item->name];
        }

        $editData['income_level'] = [];
        $editData['income_level'][] = ['id' => '', 'name' => 'Select'];
        $income_levels = [
            'under $40,000',
            '$40,000 to $49,999',
            '$50,000 to $59,999',
            '$60,000 to $69,999',
            '$70,000 to $79,999',
            '$80,000 to $89,999',
            '$90,000 to $99,999',
            '$100,000 to $149,999',
            '$150,000 to $199,999',
            '$200,000 or greater'
        ];
        foreach ($income_levels as $item) {
            $editData['income_level'][] = ['id' => $item, 'name' => $item];
        }

        $editData['state'] = [];
        $editData['state'][] = ['id' => '', 'name' => 'Select'];
        foreach ($states as $item) {
            $editData['state'][] = ['id' => $item->id, 'name' => $item->name];
        }

        $editData['residency_status'] = [];
        $editData['residency_status'][] = ['id' => '', 'name' => 'Select'];
        foreach ($residency_status as $item) {
            $editData['residency_status'][] = ['id' => (string) $item->id, 'name' => $item->name];
        }

        $editData = (string) json_encode($editData);

        $groups = [];
        if (isset($project->groups)) {
            foreach ($project->groups as $group) {
                $group_date = date('Y-m-d', strtotime($group->group_date));
                $groups[] = ['label' => $group->name . ' (' . $group_date . ' -- ' . $group->group_time . ')', 'value' => $group->id];
            }
        }
        $groups_json = (string) json_encode($groups);

        return [
            'project' => $project, 'columns' => $columns, 'filters_json' => $filters_json, 'pageSize' => $pageSize, 'masters' => $masters,
            'clients' => $clients, 'editData' => $editData, 'job_events_json' => $job_events_json, 'groups_json' => $groups_json
        ];
    }

    public function index($projectUuid)
    {

        $output = $this->search_init($projectUuid);

        extract($output);

        $query = $project->searched_data != null ? json_decode($project->searched_data) : '';

        if ($query) {
            $query->column = (array) $query->column;
        }

        $filter_values = isset($query->values) ? (array) $query->values : [];

        $ajax_data = [];
        if (isset($filter_values['filter_e_original_country']) && is_numeric($filter_values['filter_e_original_country'])) {
            $ethnic_backgrounds = MasterEthnicBackground::whereIn('country_id', [0, (int) $filter_values['filter_e_original_country']])
                ->where('isactive', 1)->orderBy('name', 'ASC')->get();
            $ajax_data['ethnic_backgrounds'] = $ethnic_backgrounds;
        }

        return view('projects.search-respondents.index', compact(
            'project',
            'columns',
            'filters_json',
            'pageSize',
            'masters',
            'clients',
            'query',
            'filter_values',
            'editData',
            'job_events_json',
            'groups_json',
            'ajax_data'
        ));
    }

    public function indexByQuery($projectUuid, $queryUUID)
    {

        $output = $this->search_init($projectUuid);

        extract($output);

        $save_query = SaveQuery::where('uuid', $queryUUID)->first();

        $query = $save_query->project_searched_data != null ? json_decode($save_query->project_searched_data) : '';

        $query->column = (array) $query->column;

        $filter_values = isset($query->values) ? (array) $query->values : [];

        //dd($filter_values);

        $ajax_data = [];
        if (isset($filter_values['filter_e_original_country']) && is_numeric($filter_values['filter_e_original_country'])) {
            $ethnic_backgrounds = MasterEthnicBackground::whereIn('country_id', [0, (int) $filter_values['filter_e_original_country']])
                ->where('isactive', 1)->get();
            $ajax_data['ethnic_backgrounds'] = $ethnic_backgrounds;
        }

        return view('projects.search-respondents.index', compact(
            'project',
            'columns',
            'filters_json',
            'pageSize',
            'masters',
            'clients',
            'query',
            'filter_values',
            'editData',
            'job_events_json',
            'groups_json',
            'save_query',
            'ajax_data'
        ));
    }

    public function getRespondents($projectUuid, Request $request)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $limit = $request->pageSize;
        $offset = ($request->pageIndex * $request->pageSize) - $request->pageSize;
        $_req_columns = isset($request->column) ? $request->column : [];
        $default_columns = [
            'respondents.id', 'respondents.first_name', 'respondents.last_name', 'age', 'all_events',
            'respondents.email', 'state', 'mobile_phone','mobile_phone2','additional_notes', 'home_postcode', 'home_postsuburb', 'occupation_id', 'ethnic_background', 'gender'
        ];
        if (isset($request->column) && count($request->column))
            $request->column = array_merge($default_columns, $request->column);
        else
            $request->column = $default_columns;

        $selected_columns = array_values(array_unique($request->column));
        $selected_columns = array_map(function ($item) {
            if ($item == 'respondents.id') return ['name' => $item, 'label' => 'ID'];
            return ['name' => $item, 'label' => preg_replace('/(?<=\\w)(?=[A-Z])/', " $1", studly_case($item))];
        }, $selected_columns);

        //change age column
        $extra_columns = ['all_events', 'children_name_and_age', 'email_template', 'previous_events'];
        $request->column = array_map(function ($item) {
            if ($item == 'age')
                return 'YEAR(CURDATE()) - YEAR(respondents.dob) as age';
            else if ($item == 'occupation') return 'occupation_id';
            else if ($item == 'bank') return 'bank_id';
            else if ($item == 'employer_size') return 'employer_size_id';
            else if ($item == 'date_created') return 'respondents.created_at';
            else if ($item == 'email') return 'respondents.email';
            else if ($item == 'industry') return 'industry_id';
            else if ($item == 'last_updated') return 'respondents.updated_at';
            else if ($item == 'occupation_type') return 'respondent_occupations.type';
            else if ($item == 'partner_employer_size') return 'partner_employer_size_id';
            else if ($item == 'partner_occupation') return 'partner_occupation_id';
            else if ($item == 'superannuation') return 'superannuation_id';
            else if ($item == 'vehicle2_make') return 'vehicle2_make_id';
            else if ($item == 'vehicle_make') return 'vehicle_make_id';
            else if ($item == 'vehicle_type') return 'vehicle_type_id';
            else if ($item == 'vehicle2_type') return 'vehicle2_type_id';
            else if ($item == 'gender') return 'respondents.gender';
            else return $item;
        }, $request->column);

        //remove extra columns
        $query_columns = $request->column;
        $request->column = array_filter($request->column, function ($item) use ($extra_columns) {
            if (!in_array($item, $extra_columns)) return $item;
        });

        $columns = implode(',', $request->column);
        DB::enableQueryLog();

        $respondents = DB::table('respondents')->select(DB::raw($columns))
            ->join('respondent_details', 'respondent_details.respondent_id', '=', 'respondents.id')
            ->join('respondent_households', 'respondent_households.respondent_id', '=', 'respondents.id')
            ->join('respondent_occupations', 'respondent_occupations.respondent_id', '=', 'respondents.id')
            ->join('respondent_healths', 'respondent_healths.respondent_id', '=', 'respondents.id')
            ->join('respondent_services', 'respondent_services.respondent_id', '=', 'respondents.id')
            ->join('respondent_products', 'respondent_products.respondent_id', '=', 'respondents.id')
            ->join('respondent_summaries', 'respondent_summaries.respondent_id', '=', 'respondents.id');

        $filter_args = $request->has('filter') ? $request->filter : [];
        $job_event = '';

        $job_event_filter_detect = false;
        foreach ($filter_args as $filter) {
            if (in_array($filter, [
                'job_event', 'not_received_email','not_received_invitation_email', 'not_received_confirmation_email',
                'not_received_sms', '3_month_rule', '6_month_rule', '12_month_rule'
            ])) {
                $job_event_filter_detect = true;
            }
        }

        $extra_filters = ['job_specific_client', 'no_job_specific_client', 'no_job_any_client'];

        if ($job_event_filter_detect) {
            $respondents->leftjoin('job_events', 'job_events.respondent_id', '=', 'respondents.id');
        }

        foreach ($filter_args as $filter) {
            if ($request->has('filter_' . $filter) && !empty($request->get('filter_' . $filter)) && !in_array($filter, $extra_filters)) {
                $_filter = $filter == 'id' ? 'respondents.id' : $filter;

                if ($filter == 'name') {
                    $respondents->whereRaw(
                        "(respondents.first_name LIKE '%" . $request->get('filter_' . $filter) . "%' 
                        OR respondents.last_name LIKE '%" . $request->get('filter_' . $filter) . "%')"
                    );
                } else if($filter == 'hear_about'){ 
                    if($request->get('filter_' . $filter)=='Others' && !empty($request->get('filter_source_name'))){
                        $respondents->whereRaw(
                            "(hear_about LIKE '%" . $request->get('filter_' . $filter) . "%' AND source_name LIKE '%" . $request->get('filter_source_name') . "%')"
                        );
                    } else {
                        $respondents->whereRaw("hear_about LIKE '%" . $request->get('filter_' . $filter) . "%'");
                    }
                } else if ($filter == 'email') {
                    $respondents->where('respondents.email', $request->get('filter_' . $filter));
                } else if ($filter == 'additional_notes') {
                    $respondents->where($_filter, 'LIKE', '%' . $request->get('filter_' . $filter) . '%');
                } else if ($filter == 'state' && is_array($request->get('filter_' . $filter))) {
                    $respondents->whereIn('state', $request->get('filter_' . $filter));
                } else if ($filter == 'closest_large_city') {
                    $cities = explode(',', $request->get('filter_' . $filter));
                    $cities = array_map(function($item){ return trim($item); }, $cities);
                    $cities = implode('|', $cities);
                    $respondents->where($filter, 'RLIKE', $cities);
                } else if ($filter == 'banks') {
                    if (is_array($request->get('filter_' . $filter))) {
                        $query = [];
                        foreach ($request->get('filter_' . $filter) as $row) {
                            $query[] = "bank_id LIKE '" . $row . ",%'";
                            $query[] = "bank_id LIKE '%," . $row . ",%'";
                            $query[] = "bank_id LIKE '%," . $row . "'";
                        }
                        $query = count($query) ? implode(' OR ', $query) : $query;
                        $respondents->whereRaw("(" . $query . ")");
                    }
                } else if ($filter == 'cigarette') {
                    if (is_array($request->get('filter_' . $filter))) {
                        $query = [];
                        foreach ($request->get('filter_' . $filter) as $row) {
                            $query[] = 'cigarette_brands LIKE "%' . $row . '%"';
                        }
                        $query = count($query) ? implode(' OR ', $query) : $query;
                        $respondents->whereRaw("(" . $query . ")");
                    }
                    if (is_array($request->filter_brands) and count($request->filter_brands) && $request->cigarette != "Doesn't Smoke") {
                        $query = [];
                        foreach ($request->filter_brands as $row) {
                            $query[] = "brands LIKE '%" . $row . "%'";
                        }
                        $query = count($query) ? implode(' OR ', $query) : $query;
                        $respondents->whereRaw("(" . $query . ")");
                    }
                } else if ($filter == 'phone_companies') {
                    if (is_array($request->get('filter_' . $filter))) {
                        $query = [];
                        foreach ($request->get('filter_' . $filter) as $row) {
                            if ($row == 'Home') $query[] = "home_phone_provider=" . $request->filter_home_phone_provider;
                            if ($row == 'Work') $query[] = "work_phone_provider=" . $request->filter_work_phone_provider;
                            if ($row == 'Mobile') $query[] = "mobile1_provider=" . $request->filter_mobile1_provider;
                        }
                        if (count($query)) {
                            $query = count($query) ? implode(' OR ', $query) : $query;
                            $respondents->whereRaw("(" . $query . ")");
                        }
                    }
                } else if ($filter == 'organ_donate') {
                    $value = $request->filter_organ_donate == 'Yes' ? 1 : 0;
                    $respondents->where('organ_donate', $value);
                } else if ($filter == 'updated_at') {
                    $respondents->whereRaw("(respondents.updated_at >= NOW() - INTERVAL $request->filter_updated_at)");
                } else if ($filter == 'job_event') {
                    //$job_event = $request->get('filter_' . $filter);
                    $respondents->whereRaw("(job_events.name = '$request->filter_job_event' )");
                } else {
                    if (is_array($request->get('filter_' . $filter))) {
                        $query = [];
                        foreach ($request->get('filter_' . $filter) as $row) {
                            if (is_numeric($row)) {
                                $query[] = "{$filter} LIKE '" . $row . ",%'";
                                $query[] = "{$filter} LIKE '%," . $row . ",%'";
                                $query[] = "{$filter} LIKE '%," . $row . "'";
                            } else
                                $query[] = $filter . " LIKE '%" . $row . "%'";
                        }
                        $query = count($query) ? implode(' OR ', $query) : $query;
                        $respondents->whereRaw("(" . $query . ")");
                    } else {
                        $op = '=';
                        $value = $request->get('filter_' . $filter);
                        $op = $_filter == 'closest_large_city' ? 'LIKE' : $op;
                        $op = $_filter == 'home_phone' ? 'LIKE' : $op;
                        $op = $_filter == 'home_postsuburb' ? 'LIKE' : $op;
                        $op = $_filter == 'mobile_phone' ? 'LIKE' : $op;
                        $op = $_filter == 'mobile_phone2' ? 'LIKE' : $op;
                        $op = $_filter == 'education_study_field' ? 'LIKE' : $op;
                        $op = $_filter == 'education_institute_suburb' ? 'LIKE' : $op;
                        $op = $_filter == 'vehicle_model' ? 'LIKE' : $op;
                        $op = $_filter == 'work_phone' ? 'LIKE' : $op;
                        $op = $_filter == 'work_postsuburb' ? 'LIKE' : $op;

                        if ($_filter == 'home_phone' || $_filter == 'mobile_phone' || $_filter == 'work_phone') {
                            $num_value = (int) preg_replace('/\D/', '', $value);
                            $respondents->whereRaw("($_filter = '$value' OR $_filter = '$num_value')");
                        } else {
                            if ($op == 'LIKE')
                                $value = '%' . $value . '%';
                            $respondents->where($_filter, $op, $value);
                        }
                    }
                }
            } else if ($filter == 'name') {
                $_sql = [];
                if (!empty($request->filter_first_name))
                    $_sql[] = "respondents.first_name LIKE '%" . $request->get('filter_first_name') . "%'";
                if (!empty($request->filter_last_name))
                    $_sql[] = "respondents.last_name LIKE '%" . $request->get('filter_last_name') . "%'";

                $_sql = implode(' AND ', $_sql);
                $respondents->whereRaw("($_sql)");
            } else if ($filter == 'id_range') {
                $sql = [];
                if (!empty($request->filter_id_range_from)) $sql[] = 'respondents.id >= ' . $request->filter_id_range_from;
                if (!empty($request->filter_id_range_to)) $sql[] = 'respondents.id <= ' . $request->filter_id_range_to;

                $sql = implode(' AND ', $sql);

                $respondents->whereRaw(
                    "(" . $sql . ")"
                );
            } else if ($filter == 'age') {
                $sql = [];
                if (!empty($request->filter_age_from)) $sql[] = '(YEAR(CURDATE()) - YEAR(respondents.dob)) >= "' . $request->filter_age_from . '"';
                if (!empty($request->filter_age_to)) $sql[] = '(YEAR(CURDATE()) - YEAR(respondents.dob)) <= "' . $request->filter_age_to . '"';

                $sql = implode(' AND ', $sql);

                $respondents->whereRaw(
                    "(" . $sql . ")"
                );
            } else if ($filter == 'created_at') {
                $sql = [];
                if (!empty($request->filter_created_at_from)) $sql[] = 'respondents.created_at >= "' . $request->filter_created_at_from . '"';
                if (!empty($request->filter_created_at_to)) $sql[] = 'respondents.created_at <= "' . $request->filter_created_at_to . '"';

                $sql = implode(' AND ', $sql);

                $respondents->whereRaw(
                    "(" . $sql . ")"
                );
            } else if ($filter == 'dob') {
                $sql = [];
                if (!empty($request->filter_dob_from)) $sql[] = 'respondents.dob >= "' . $request->filter_dob_from . '"';
                if (!empty($request->filter_dob_to)) $sql[] = 'respondents.dob <= "' . $request->filter_dob_to . '"';

                $sql = implode(' AND ', $sql);

                $respondents->whereRaw(
                    "(" . $sql . ")"
                );
            } else if ($filter == 'vehicle_year') {
                $sql = [];
                if (!empty($request->filter_vehicle_year_from)) $sql[] = 'vehicle_year >= ' . $request->filter_vehicle_year_from;
                if (!empty($request->filter_vehicle_year_to)) $sql[] = 'vehicle_year <= ' . $request->filter_vehicle_year_to;

                $sql = implode(' AND ', $sql);

                $respondents->whereRaw(
                    "(" . $sql . ")"
                );
            } else if ($filter == 'home_postcode_range') {
                $sql = [];
                if (!empty($request->filter_home_postcode_range_from)) $sql[] = 'home_postcode >= ' . $request->filter_home_postcode_range_from;
                if (!empty($request->filter_home_postcode_range_to)) $sql[] = 'home_postcode <= ' . $request->filter_home_postcode_range_to;

                $sql = implode(' AND ', $sql);

                $respondents->whereRaw(
                    "(" . $sql . ")"
                );
            } else if ($filter == 'work_postcode_range') {
                $sql = [];
                if (!empty($request->filter_work_postcode_range_from)) $sql[] = 'work_postcode >= ' . $request->filter_work_postcode_range_from;
                if (!empty($request->filter_work_postcode_range_to)) $sql[] = 'work_postcode <= ' . $request->filter_work_postcode_range_to;

                $sql = implode(' AND ', $sql);

                $respondents->whereRaw(
                    "(" . $sql . ")"
                );
            } else if (in_array($filter, ['do_no_show', 'do_not_call', 'one_off_resp', 'stop_often_email'])) {
                $respondents->where($filter, 1);
            } else if ($filter == 'no_one_off_res') {
                $respondents->where('one_off_resp', 0);
            } else if ($filter == 'exclude_accent_people') {
                $respondents->where('has_accent', 0);
            } else if ($filter == 'no_email_address') {
                $respondents->whereRaw('respondents.email IS NULL OR respondents.email = ""');
            } else if ($filter == 'exclude_people_no_email') {
                $respondents->whereRaw('respondents.email IS NOT NULL AND respondents.email != ""');
            } else if ($filter == 'online_survey') {
                $respondents->where('online_survey', 1);
            } else if ($filter == 'no_online_survey') {
                $respondents->where('online_survey', 0);
            } else if ($filter == 'no_landline') {
                $respondents->whereRaw('(home_phone IS NULL OR home_phone = "")');
            } else if ($filter == 'no_mobile') {
                $respondents->whereRaw('(mobile_phone IS NULL and  mobile_phone2 is null )');
            } else if ($filter == 'exclude_no_mobile') {
                $respondents->whereRaw('(mobile_phone IS NOT NULL AND mobile_phone != "")');
            } else if ($filter == 'inactive') {
                $respondents->where('isactive', 0);
            } else if ($filter == 'excellent_candidate') {
                $respondents->where('excellent_candidate', 1);
            } else if ($filter == 'keen_candidate') {
                $respondents->where('keen_candidate', 1);
            } else if ($filter == 'has_accent') {
                $respondents->where('has_accent', 1);
            } else if ($filter == 'has_children') {
                $any_from = $request->filter_child_any_from;
                $any_to = $request->filter_child_any_to;
                $all_from = $request->filter_child_all_from;
                $all_to = $request->filter_child_all_to;

                $query = '';
                if (is_numeric($any_from) && is_numeric($any_to)) {
                    $query = '(YEAR(CURDATE()) - birth_year) >= ' . $any_from . ' AND ' . ' (YEAR(CURDATE()) - birth_year) <= ' . $any_to;
                } else if (is_numeric($all_from) && is_numeric($all_to)) {
                    $query = '(YEAR(CURDATE()) - birth_year) >= ' . $all_from . ' AND ' . ' (YEAR(CURDATE()) - birth_year) <= ' . $all_to;
                }
                if (!empty($query)) {
                    $childrens = RespondentChildren::select('respondent_id')->whereRaw($query)->pluck('respondent_id')->toArray();
                    if (count($childrens)) {
                        $respondents->whereIn('respondents.id', array_unique($childrens));
                    }
                }
            } else if ($filter == 'has_no_children') {
                $resIDS = RespondentChildren::selectRaw('DISTINCT respondent_id')->pluck('respondent_id')->toArray();
                $respondents->whereNotIn('respondents.id', $resIDS);
            } else if ($filter == 'allow_children') {
                $respondents->where('children_allowed', 1);
            } else if ($filter == 'not_received_invitation_email') {
                $emailTemplate = EmailTemplate::select('id')->where('title', 'LIKE', '%invitation%')->pluck('id')->toArray();
                $emailTemplate2 = ProjectEmailTemplate::select('id')->where('job_id', $project->id)->where('title', 'LIKE', '%invitation%')->pluck('id')->toArray();
                $template_ids = array_merge($emailTemplate, $emailTemplate2);
                $template_ids = array_unique($template_ids);
                if (count($template_ids)) {
                    $template_ids = implode(',', $template_ids);
                    $respondents->whereRaw("( respondents.id NOT IN(select job_events.respondent_id from job_events where job_events.template_id IN ($template_ids) AND job_events.project_id=$project->id) )");
                }
            } else if ($filter == 'not_received_confirmation_email') {
                $emailTemplate = EmailTemplate::select('id')->where('title', 'LIKE', '%confirmation%')->pluck('id')->toArray();
                $emailTemplate2 = ProjectEmailTemplate::select('id')->where('job_id', $project->id)->where('title', 'LIKE', '%confirmation%')->pluck('id')->toArray();
                $template_ids = array_merge($emailTemplate, $emailTemplate2);
                $template_ids = array_unique($template_ids);
                if (count($template_ids)) {
                    $template_ids = implode(',', $template_ids);
                    $respondents->whereRaw("( respondents.id NOT IN(select job_events.respondent_id from job_events where job_events.template_id IN ($template_ids) AND job_events.project_id=$project->id) )");
                    //$respondents->whereRaw("(job_events.template_id NOT IN ($template_ids))");
                }
            } else if ($filter == 'not_received_email') {
                $respondents->whereRaw("(respondents.id NOT IN (select job_events.respondent_id from job_events where job_events.project_id=$project->id AND job_events.name = 'Email Sent'))");
            } else if ($filter == 'not_received_sms') {
                $respondents->whereRaw("(respondents.id NOT IN (select job_events.respondent_id from job_events where job_events.project_id=$project->id AND job_events.name = 'SMS Sent'))");
            } else if ($filter == '3_month_rule') {
                $three_mon_before_pdate = date("Y-m-d", strtotime("-3 months"));
                $respondents->whereRaw("(respondents.id NOT IN ( select respondent_id from job_events where job_events.created_date >= '$three_mon_before_pdate' AND job_events.name IN('Payment Sent', 'Paid Off at Session', 'Screened OK', 'Screened OK – Quotas Full') ))");
            } else if ($filter == '6_month_rule') {
                $six_mon_before_pdate = date("Y-m-d", strtotime("-6 months"));
                $respondents->whereRaw("(respondents.id NOT IN ( select respondent_id from job_events where job_events.created_date >= '$six_mon_before_pdate' AND job_events.name IN('Payment Sent', 'Paid Off at Session', 'Screened OK', 'Screened OK – Quotas Full') ))");
            } else if ($filter == '12_month_rule') {
                $twelve_mon_before_pdate = date("Y-m-d", strtotime("-12 months"));
                $respondents->whereRaw("(respondents.id NOT IN ( select respondent_id from job_events where job_events.created_date >= '$twelve_mon_before_pdate' AND job_events.name IN('Payment Sent', 'Paid Off at Session', 'Screened OK', 'Screened OK – Quotas Full') ))");
            } else if ($filter == 'job_specific_client') {
                $respondents->whereRaw("(respondents.id IN ( select respondent_id from job_events where job_events.client_id = " . $request->filter_job_specific_client . " ))");
            } else if ($filter == 'no_job_specific_client') {
                $respondents->whereRaw("(respondents.id NOT IN ( select respondent_id from job_events where job_events.client_id = " . $request->filter_no_job_specific_client . " ))");
            } else if ($filter == 'no_job_any_client') {
                $respondents->whereRaw("(respondents.id NOT IN ( select respondent_id from job_events ))");
            } else if($filter == 'not_screened_ok') {
                $respondents->whereRaw("(respondents.id NOT IN (select job_events.respondent_id from job_events where job_events.project_id=$project->id AND job_events.name IN ('Payment Sent', 'Paid Off at Session', 'Screened OK', 'Screened OK – Quotas Full', 'Did not Qualify')))");
            }
        }

        if (!empty($request->sortField) && !empty($request->sortOrder)) {
            if ($request->sortField == 'age') $request->sortField = 'age';
            if ($request->sortField == 'bank') $request->sortField = 'bank_id';
            if ($request->sortField == 'date_created') $request->sortField = 'respondents.created_at';
            if ($request->sortField == 'last_updated') $request->sortField = 'respondents.updated_at';
            if ($request->sortField == 'employer_size') $request->sortField = 'employer_size_id';
            if ($request->sortField == 'industry') $request->sortField = 'industry_id';
            if ($request->sortField == 'occupation') $request->sortField = 'occupation_id';
            if ($request->sortField == 'occupation_type') $request->sortField = 'respondent_occupations.type';
            if ($request->sortField == 'partner_employer_size') $request->sortField = 'partner_employer_size_id';
            if ($request->sortField == 'partner_occupation') $request->sortField = 'partner_occupation_id';
            if ($request->sortField == 'superannuation') $request->sortField = 'superannuation_id';
            if ($request->sortField == 'vehicle2_make') $request->sortField = 'vehicle2_make_id';
            if ($request->sortField == 'vehicle_make') $request->sortField = 'vehicle_make_id';
            if ($request->sortField == 'vehicle_type') $request->sortField = 'vehicle_type_id';
            if ($request->sortField == 'vehicle2_type') $request->sortField = 'vehicle2_type_id';
            if ($request->sortField == 'children_name_and_age') $request->sortField = 'children';

            $respondents = $respondents->orderBy($request->sortField, $request->sortOrder);
        } else {
            $respondents = $respondents->orderBy('respondents.id', 'desc');
        }

        if ($job_event_filter_detect) {
            $totalCount = $respondents->distinct('respondents.id')->where('respondents.deleted_at', null)->count('respondents.id');
            $respondents = $respondents->offset($offset)->limit($limit)->distinct('respondents.id')->get();
        } else {
            $totalCount = $respondents->where('respondents.deleted_at', null)->count('respondents.id');
            $respondents = $respondents->offset($offset)->limit($limit)->get();
        }

        $query_log = DB::getQueryLog();

        $result = [];
        foreach ($respondents as $item) {

            $respondent = Respondent::where('id', $item->id)->first();

            if ($respondent != null) {

                $item->check = '<input type="checkbox" class="row_check" name="row_check" value="' . $item->id . '" data-email="' . $respondent->email . '" data-mobile="' . $respondent->detail->mobile_phone . '" />';

                if (auth()->check() && auth()->user()->type == 'admin') {
                    $item->delete = '<a href="javascript:void(0);" class="delete_res btn btn-danger btn-sm" data-id="' . $item->id . '" data-name="' . $item->first_name . '"><i class="icon-trash"></i></a>';
                }
                $flags = '';
                if ($respondent->do_not_call) $flags .= '<b title="DO NOT CALL" style="background:#f97b2e"></b>';
                if ($respondent->one_off_resp) $flags .= '<b title="ONE OFF RESPONDENT" style="background:#f3dc32"></b>';
                if ($respondent->has_accent) $flags .= '<b title="HAS ACCENT" style="background:#3e9003"></b>';
                if ($respondent->excellent_candidate) $flags .= '<b title="EXCELLENT" style="background:#9a139a"></b>';

                if ($respondent->do_no_show) $flags .= '<b title="NO SHOW" style="background:#fb0202"></b>';
                if ($respondent->stop_often_email) $flags .= '<b title="DO NOT SEND INVITATION EMAIL OFTEN" style="background:#2b58bf"></b>';
                if ($respondent->keen_candidate) $flags .= '<b title="KEEN CANDIDATE" style="background:#eb66f3"></b>';

                $item->flags = '<div class="colorbox">' . $flags . '</div>';

                if (in_array('alcohol_consumed', $request->column) && !empty($item->alcohol_consumed)) {
                    $alcohol_consumed = MasterAlcoholConsumed::find($item->alcohol_consumed);
                    if ($alcohol_consumed != null) $item->alcohol_consumed = $alcohol_consumed->id;
                }
                if (in_array('bank_id', $request->column) && !empty($item->bank_id)) {
                    $bank_ids = explode(',', $item->bank_id);
                    $banks = MasterBank::select('name')->whereIn('id', $bank_ids)->pluck('name')->toArray();
                    if ($banks != null) $item->bank = implode(',', $banks);
                }
                if (in_array('birth_country', $request->column) && !empty($item->birth_country)) {
                    $birth_country = MasterBirthCountry::find($item->birth_country);
                    if ($birth_country != null) $item->birth_country = $birth_country->id;
                }
                if (in_array('children', $request->column) && isset($item->children)) {
                    $item->children = $item->children == 1 ? 'Yes' : 'No';
                }
                if (in_array('children_name_and_age', $query_columns) && in_array('children_name_and_age', $extra_columns)) {
                    $children = RespondentChildren::selectRaw('child_name, YEAR(CURDATE()) - birth_year as age')
                        ->where('respondent_id', $item->id)->get()->toArray();
                    if (count($children)) {
                        $children_name_and_age = [];
                        foreach ($children as $a) {
                            if (!empty($a['child_name']) || !empty($a['age'])) {
                                $name = !empty($a['child_name']) ? $a['child_name'] : 'Not Given';
                                $age = !empty($a['age']) ? $a['age'] : 'Not Given';
                                $children_name_and_age[] = $name  . ',' . $age;
                            }
                        }
                    }
                    $item->children_name_and_age = implode('<br/>', $children_name_and_age);
                }
                if (in_array('cigarette_brands', $request->column)) {
                    $item->cigarette_brands = isset($respondent->health) ? $respondent->health->brands : "";
                }
                if (in_array('respondents.created_at', $request->column) && isset($item->created_at)) {
                    $item->date_created = $item->created_at;
                }
                if (in_array('do_no_show', $request->column) && isset($item->do_no_show)) {
                    $item->do_no_show = $item->do_no_show == 1 ? 'Yes' : 'No';
                }
                if (in_array('stop_often_email', $request->column) && isset($item->stop_often_email)) {
                    $item->stop_often_email = $item->stop_often_email == 1 ? 'Yes' : 'No';
                }
                if (in_array('do_not_call', $request->column) && isset($item->do_not_call)) {
                    $item->do_not_call = $item->do_not_call == 1 ? 'Yes' : 'No';
                }
                if (in_array('employer_size_id', $request->column) && !empty($item->employer_size_id)) {
                    $employer_size_id = MasterEmployerSize::find($item->employer_size_id);
                    if ($employer_size_id != null) $item->employer_size = $employer_size_id->name;
                }
                if (in_array('has_accent', $request->column) && isset($item->has_accent)) {
                    $item->has_accent = $item->has_accent == 1 ? 'Yes' : 'No';
                }
                if (in_array('health_funds', $request->column) && !empty($item->health_funds)) {
                    $health_funds_ids = explode(',', $item->health_funds);
                    $health_funds = MasterHealthFund::select('name')->whereIn('id', $health_funds_ids)->pluck('name')->toArray();
                    if ($health_funds != null) $item->health_funds = implode(',', $health_funds);
                }
                if (in_array('home_phone_provider', $request->column) && !empty($item->home_phone_provider)) {
                    $home_phone_provider = MasterPhoneProvider::find($item->home_phone_provider);
                    if ($home_phone_provider != null) $item->home_phone_provider = $home_phone_provider->name;
                }
                if (in_array('industry_id', $request->column) && !empty($item->industry_id)) {
                    $industry = MasterIndustry::find($item->industry_id);
                    if ($industry != null) $item->industry = $industry->name;
                }
                if (in_array('internet_provider', $request->column) && !empty($item->internet_provider)) {
                    $internet_provider = MasterInternetProvider::find($item->internet_provider);
                    if ($internet_provider != null) $item->internet_provider = $internet_provider->name;
                }
                if (in_array('keen_candidate', $request->column) && isset($item->keen_candidate)) {
                    $item->keen_candidate = $item->keen_candidate == 1 ? 'Yes' : 'No';
                }
                if (in_array('respondents.updated_at', $request->column) && isset($item->updated_at)) {
                    $item->last_updated = $item->updated_at;
                }
                if (in_array('occupation_id', $request->column) && !empty($item->occupation_id)) {
                    $occupation = MasterOccupation::find($item->occupation_id);
                    if ($occupation != null) $item->occupation = $occupation->id;
                }
                if (in_array('respondent_occupations.type', $request->column) && isset($item->type)) {
                    $item->occupation_type = $item->type;
                }
                if (in_array('one_off_resp', $request->column) && isset($item->one_off_resp)) {
                    $item->one_off_resp = $item->one_off_resp == 1 ? 'Yes' : 'No';
                }
                if (in_array('partner_brands', $request->column)) {
                    $item->partner_brands = isset($respondent->health) ? $respondent->health->partner_brands : "";
                }
                if (in_array('partner_employer_size_id', $request->column) && !empty($item->partner_employer_size_id)) {
                    $partner_employer_size = MasterEmployerSize::find($item->partner_employer_size_id);
                    if ($partner_employer_size != null) $item->partner_employer_size = $partner_employer_size->name;
                }
                if (in_array('partner_occupation_id', $request->column) && !empty($item->partner_occupation_id)) {
                    $occupation = MasterOccupation::find($item->partner_occupation_id);
                    if ($occupation != null) $item->partner_occupation = $occupation->id;
                }
                if (in_array('preferred_research_location', $request->column) && !empty($item->preferred_research_location)) {
                    $preferred_research_location_ids = explode(',', $item->preferred_research_location);
                    $preferred_research_locations = MasterPreferredResearchLocation::select('name')->whereIn('id', $preferred_research_location_ids)->pluck('name')->toArray();
                    if ($preferred_research_locations != null) $item->preferred_research_location = implode(',', $preferred_research_locations);
                }
                if (in_array('rewards_progammes', $request->column) && !empty($item->rewards_progammes)) {
                    $rewards_progammes_ids = explode(',', $item->rewards_progammes);
                    $rewards_progammes = MasterRewardProgram::select('name')->whereIn('id', $rewards_progammes_ids)->pluck('name')->toArray();
                    if ($rewards_progammes != null) $item->rewards_progammes = implode(',', $rewards_progammes);
                }
                if (in_array('state', $request->column) && !empty($item->state)) {
                    $state = MasterState::find($item->state);
                    if ($state != null) $item->state = $state->id;
                }
                if (in_array('superannuation_id', $request->column) && !empty($item->superannuation_id)) {
                    $superannuation = MasterSuperannuation::find($item->superannuation_id);
                    if ($superannuation != null) $item->superannuation = $superannuation->name;
                }
                if (in_array('vehicle_make_id', $request->column) && !empty($item->vehicle_make_id)) {
                    $vehicle_make = MasterVehicleMake::find($item->vehicle_make_id);
                    if ($vehicle_make != null) $item->vehicle_make = $vehicle_make->name;
                }
                if (in_array('vehicle2_make_id', $request->column) && !empty($item->vehicle2_make_id)) {
                    $vehicle2_make = MasterVehicleMake::find($item->vehicle2_make_id);
                    if ($vehicle2_make != null) $item->vehicle2_make = $vehicle2_make->name;
                }
                if (in_array('vehicle_type_id', $request->column) && !empty($item->vehicle_type_id)) {
                    $vehicle_type = MasterVehicleType::find($item->vehicle_type_id);
                    if ($vehicle_type != null) $item->vehicle_type = $vehicle_type->name;
                }
                if (in_array('vehicle2_type_id', $request->column) && !empty($item->vehicle2_type_id)) {
                    $vehicle2_type = MasterVehicleType::find($item->vehicle2_type_id);
                    if ($vehicle2_type != null) $item->vehicle2_type = $vehicle2_type->name;
                }
                if (in_array('work_phone_provider', $request->column) && !empty($item->work_phone_provider)) {
                    $work_phone_provider = MasterPhoneProvider::find($item->work_phone_provider);
                    if ($work_phone_provider != null) $item->work_phone_provider = $work_phone_provider->name;
                }
                if (in_array('original_country', $request->column) && !empty($item->original_country)) {
                    $original_country = MasterBirthCountry::find($item->original_country);
                    if ($original_country != null) $item->original_country = $original_country->name;
                }

                if (isset($item->mobile_phone) && $respondent->wrong_number) $item->mobile_phone = '<span class="text-danger">' . $item->mobile_phone . '</span>';

                //previous events
                if (in_array('previous_events', $_req_columns)) {
                    $qualified_job_events = isset($respondent->qualified_job_events) ? $respondent->qualified_job_events : null;
                    $events = [];
                    if ($qualified_job_events != null) {
                        foreach ($qualified_job_events as $event) {
                            $project_name = $event->project ? $event->project->job_name : '';
                            $createdBy = ($event->createdBy && $event->createdBy->profile) ? '--By:' . $event->createdBy->profile->first_name . ' ' . $event->createdBy->profile->last_name : '';
                            $events[] = $project_name . '--' . $event->name . '--' . $event->created_date . '--' . ($event->createdBy ? $event->createdBy->profile->first_name : '') . $createdBy;
                        }
                    }

                    $item->previous_events = '<div class="previous_events_wrap">' . implode('<br/><br/>', $events) . '</div>';
                }


                $job_events = isset($respondent->job_events) ? $respondent->job_events : null;
                if (in_array('all_events', $_req_columns) || in_array('email_template', $_req_columns) || in_array('all_events', $default_columns)) {
                    $events2 = [];
                    $emailTemplates = [];
                    if ($job_events != null) {
                        foreach ($job_events as $event) {
                            $project_name = $event->project ? $event->project->job_name : '';


                            $invitation_email = false;
                            if ((isset($event->template) &&  strpos(strtolower($event->template->title), "invitation") !== false)) $invitation_email = true;
                            if ((isset($event->projectTemplate) &&  strpos(strtolower($event->projectTemplate->title), "invitation") !== false)) $invitation_email = true;

                            if ($invitation_email)
                                $events2[] = "Invitation Sent <br/>" . $project_name . '--' . $event->name . '--' . $event->created_date . '--' . ($event->createdBy ? $event->createdBy->profile->first_name : '');
                            else
                                $events2[] = $project_name . '--' . $event->name . '--' . $event->created_date . '--' . ($event->createdBy ? $event->createdBy->profile->first_name : '');

                            $createdBy = ($event->createdBy && $event->createdBy->profile) ? '--By:' . $event->createdBy->profile->first_name . ' ' . $event->createdBy->profile->last_name : '';
                            if (isset($event->template)) $emailTemplates[] = $event->template->title . '--' . $event->created_date . $createdBy;
                            if (isset($event->projectTemplate)) $emailTemplates[] = $event->projectTemplate->title . '--' . $event->created_date . $createdBy;
                        }
                    }

                    $item->all_events = '<div class="previous_events_wrap">' . implode('<br/><br/>', $events2) . '</div>';

                    $item->email_template = '<div class="email_templates_wrap">' . implode('<br/><br/>', $emailTemplates) . '</div>';
                }

                $isQualified = false;
                if ($job_events != null) {
                    foreach ($job_events as $event) {
                        if ($project->id == $event->project_id && in_array($event->name, ['Payment Sent', 'Paid Off at Session', 'Screened OK', 'Screened OK – Quotas Full'])) {
                            $isQualified = true;
                        }
                    }
                }
                $item->isQualified = $isQualified;

                $item->id = $isQualified ? '<a href="' . route('respondents.edit', $respondent->uuid) . '" target="_blank">' . $item->id . '</a><br/><span class="badge badge-success"><span class="blink"><i class="fa fa-check fa-sm mr-1"></i>Qualified</span></span>' : '<a href="' . route('respondents.edit', $respondent->uuid) . '" target="_blank">' . $item->id . '</a>';


                $result[] = $item;
            }
        }

        $values = $request->except(
            'column',
            '_token',
            'filter',
            'pageIndex',
            'pageSize',
            'sortField',
            'sortOrder'
        );

        $values = array_filter($values, function ($item) {
            return $item != '';
        });

        $searched_data = json_encode(['column' => $request->column, 'filter' => $request->filter, 'values' => $values]);
        $project->update(['searched_data' => $searched_data]);

        session(['searched_data' => $searched_data, 'query' => $query_log, 'table_data' => $result]);

        return response()->json([
            'data' => $result, 'itemsCount' => $totalCount, 'limit' => $limit, 'offset' => $offset,
            'columns' => $columns, 'query' => $query_log
        ], 200);
    }

    public function item_select($item, $option)
    {
        return 'selected';
    }

    public function exportQuery(Request $request)
    {
        $data['ids'] = $request->ids;
        Excel::store(new SearchRespondents($data), 'public/export-query.xls');
        return response()->json(asset('public/storage/export-query.xls'));
    }
}
