<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectGroup;
use App\Models\Masters\MasterState;
use Yajra\Datatables\Datatables;
use App\User;

class ProjectGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        return view('projects.groups.index', compact('project'));
    }

    public function DTProjectGroups($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $groups = ProjectGroup::where('job_id', $project->id)
            ->leftjoin('master_states', 'project_groups.location', '=', 'master_states.id')
            ->select(
                'project_groups.id',
                'project_groups.uuid',
                'project_groups.job_id',
                'project_groups.name',
                'project_groups.location',
                'project_groups.incentive',
                'project_groups.other',
                'master_states.name as mlocation',
                'project_groups.created_by',
                'project_groups.created_at'
            );
        return Datatables::of($groups)
            ->addColumn('group_name', function ($group) {
                return '<a href="' . route('editGroup', [$group->project->uuid, $group->uuid]) . '">
                    ' . $group->name  . '
                </a>';
            })
            ->addColumn('creator_name', function ($group) {
                if ($group->createdBy != null) {
                    if (auth()->user()->type == 'client') {
                        return $group->createdBy->profile->first_name . ' ' . $group->createdBy->profile->last_name;
                    } else {
                        return '<a href="' . route('staffs.edit', $group->createdBy->uuid) . '">
                                ' . $group->createdBy->profile->first_name . ' ' . $group->createdBy->profile->last_name . '
                            </a>';
                    }
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($group) {
                return $group->created_at->format('d-m-Y');
            })
            ->addColumn('actions', function ($group) {
                if (auth()->user()->type == 'client') {
                    return '<td>
                                <a href="' . route('editGroup', [$group->project->uuid, $group->uuid]) . '" class="btn btn-primary btn-sm">View</a>
                            </td>';
                } else {
                    return '<td>
                                <a href="' . route('editGroup', [$group->project->uuid, $group->uuid]) . '"><i class="icon-pencil mr-3"></i></a>
                                <a href="javascript:void(0);" class="delete_btn" data-id="' . $group->id . '" data-name="' . $group->name . '"><i class="icon-trash"></i></a>
                            </td>';
                }
            })->rawColumns(['group_name', 'actions', 'creator_name'])->make(true);
    }

    public function create($projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $locations = MasterState::where('isactive', 1)->orderBy('name', 'ASC')->get();
        return view('projects.groups.create', compact('locations', 'project'));
    }

    public function store(Request $request, $projectUuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $rules = [
            'name' => 'required|max:255',
            'location' => 'required|max:255',
            'venue_address' => 'required',
            'venue_phone' => 'required|max:255',
            'group_date' => 'required|date',
            'duration' => 'required|max:255'
        ];
        $this->validate($request, $rules, []);

        $data = $request->except('time');
        $data['created_by'] = auth()->id();

        if ($request->has('time')) {
            $times = [];
            foreach ($request->time as $time) {
                if ($time != null) {
                    $times[] = $time;
                }
            }
            if (count($times))
                $data['different_time'] = implode(',', $times);
        }

        $data['job_id'] = $project->id;
        $group = ProjectGroup::create($data);
        if ($group)
            return redirect(route('indexGroups', $project->uuid))->with('success', 'Group created successfully!');
        else
            return redirect()->back()->withErrors(['message' => 'Problem while creating group. Please try again']);
    }

    public function edit($projectUuid, $uuid)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $group = ProjectGroup::where('uuid', $uuid)->first();
        $locations = MasterState::where('isactive', 1)->orderBy('name', 'ASC')->get();
        $times = [];
        if ($group != null && $group->different_time) {
            $times = explode(',', $group->different_time);
        }
        return view('projects.groups.edit', compact('project', 'group', 'locations', 'times'));
    }

    public function update(Request $request, $projectUuid, ProjectGroup $group)
    {
        $project = Project::where('uuid', $projectUuid)->first();
        $rules = [
            'name' => 'required|max:255',
            'location' => 'required|max:255',
            'venue_address' => 'required',
            'venue_phone' => 'required|max:255',
            'group_date' => 'required|date',
            'duration' => 'required|max:255'
        ];
        $this->validate($request, $rules, []);

        $data = $request->except('time');
        $data['modified_by'] = auth()->id();

        if ($request->has('time')) {
            $times = [];
            foreach ($request->time as $time) {
                if ($time != null) {
                    $times[] = $time;
                }
            }
            if (count($times))
                $data['different_time'] = implode(',', $times);
        }

        $group->update($data);
        return redirect()->back()->withSuccess('Group updated successfully!');
    }

    public function destory($projectUuid, ProjectGroup $group)
    {
        $group->delete();
        return response()->json(1);
    }

    public function getTimings(ProjectGroup $group)
    {
        $timings = [];
        if ($group->group_time != null) $timings[] = $group->group_time;
        if ($group->different_time != null) {
            $different_times = explode(',', $group->different_time);
            $timings = array_merge($timings, $different_times);
        }
        $timings = array_map(function ($item) {
            return date('H:i:s', strtotime($item));
        }, $timings);
        return response()->json($timings, 200);
    }
}
