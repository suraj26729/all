<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SMSTemplate;
use Yajra\Datatables\Datatables;

class SMSTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('sms_templates.index');
    }

    public function DTData()
    {
        $templates = SMSTemplate::select(
            'id',
            'uuid',
            'title',
            'isactive',
            'created_by',
            'created_at'
        );
        return Datatables::of($templates)
            ->addColumn('template_name', function ($template) {
                return '<a href="' . route('sms-templates.edit', $template->uuid) . '">
                    ' . $template->title  . '
                </a>';
            })
            ->addColumn('created_user_name', function ($template) {
                if ($template->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $template->createdBy->uuid) . '">
                    ' . $template->createdBy->profile->first_name . ' ' . $template->createdBy->profile->last_name . '
                </a>';
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($template) {
                return $template->created_at->format('d-m-Y');
            })
            ->addColumn('actions', function ($template) {
                return '<td>
                <a href="' . route('sms-templates.edit', $template->uuid) . '"><i class="icon-pencil mr-3"></i></a>
                <a href="javascript:void(0);" class="delete_btn" data-id="' . $template->id . '" data-name="' . $template->title . '"><i class="icon-trash"></i></a>
            </td>';
            })->rawColumns(['template_name', 'actions', 'created_user_name'])->make(true);
    }

    public function create()
    {
        return view('sms_templates.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];
        $this->validate($request, $rules, []);

        //dd($request->all());

        $data = $request->all();
        $data['created_by'] = auth()->id();

        $template = SMSTemplate::create($data);
        if ($template) {
            return redirect()->back()->withSuccess('SMS Template created successfully!');
        } else
            return redirect()->back()->withErrors(['message' => 'Problem while creating sms template. Please try again']);
    }

    public function edit($uuid)
    {
        $template = SMSTemplate::where('uuid', $uuid)->first();
        return view('sms_templates.edit', compact('template'));
    }

    public function update(Request $request, SMSTemplate $smsTemplate)
    {
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];

        $this->validate($request, $rules, []);

        $data = $request->all();
        $data['modified_by'] = auth()->id();

        $smsTemplate->update($data);

        return redirect()->back()->withSuccess('SMS Template updated successfully!');
    }

    public function destroy(SMSTemplate $smsTemplate)
    {
        $smsTemplate->delete();
        return response()->json(1);
    }
}
