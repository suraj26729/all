<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User, Auth;


class ClientController extends Controller
{
    public function login()
    {
        return view('front.clients.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password, 'type' => 'client'], $request->get('remember'))) {

            return redirect()->intended('/home');
        }

        return redirect()->back()->withErrors(['email' => 'These credentials do not match our records.']);
    }
}
