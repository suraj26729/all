<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Masters\MasterState;
use App\Models\Masters\MasterOccupation;
use App\Models\Masters\MasterIndustry;
use App\Models\Masters\MasterHealthProblem;
use App\Models\Masters\MasterBrand;
use App\Models\Masters\MasterPostcode;
use App\Models\Masters\MasterEthnicBackground;
use App\Models\Masters\MasterBirthCountry;
use App\Models\Masters\MasterResidencyStatus;

use App\Models\Respondents\Respondent;
use App\Models\Respondents\RespondentDetail;
use App\Models\Respondents\RespondentHousehold;
use App\Models\Respondents\RespondentOccupation;
use App\Models\Respondents\RespondentHealth;
use App\Models\Respondents\RespondentService;
use App\Models\Respondents\RespondentProduct;
use App\Models\Respondents\RespondentSummary;
use Mail, Auth;
use Hash;


use Illuminate\Http\Request;


class RespondentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $states = MasterState::where('isactive', 1)->get();
        $occupations = MasterOccupation::where('isactive', 1)->get();
        $industries = MasterIndustry::where('isactive', 1)->get();
        $healthproblems = MasterHealthProblem::where('isactive', 1)->get();
        $brands = MasterBrand::where('isactive', 1)->get();
        $ethnic_backgrounds = MasterEthnicBackground::select('name')->distinct()->where('isactive', 1)->orderBy('name')->get();
        $countries = MasterBirthCountry::where('id', '!=', 238)->where('isactive', 1)->orderBy('name')->get();
        $residency_status = MasterResidencyStatus::where('isactive', 1)->get();

        return view('front.respondents.index', compact(
            'states',
            'occupations',
            'industries',
            'healthproblems',
            'brands',
            'ethnic_backgrounds',
            'countries',
            'residency_status'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            //'mobile_phone' => 'required|numeric',
            'email' => 'required|unique:respondents|email|max:255',
            'password' => 'required|max:10|min:5',
            'hear_about' => 'required',
            'source_name' => 'required',
            'original_country' => 'required',
            //'ethnic_background' => 'required',
            'residency_status' => 'required',

            //'dob' => 'required',
            'dob_day' => 'required',
            'dob_mon' => 'required',
            'dob_year' => 'required',

            'gender' => 'required',
            'state' => 'required',
            //'mobile_phone' => 'required',
            'home_postcode' => 'required'
        ];

        $this->validate($request, $rules);

        if($request->closest_city == "other"){
                $request->closest_city=$request->other_closest_city;
        }
        
        $args = $request->only(['email', 'password', 'first_name', 'last_name', 'gender']);
        $args['dob'] = $request->dob_year . '-' . $request->dob_mon . '-' . $request->dob_day;
        $args['password'] = bcrypt($request->password);

        $respondent = Respondent::create($args);
        if ($respondent) {
            //Respondent Details
            $detailargs = $request->only(['state', 'home_postcode', 'home_postsuburb', 'home_subdirection', 'work_postcode', 'work_postsuburb', 'work_subdirection', 'home_phone', 'work_phone', 'mobile_phone', 'mobile_phone2', 'hear_about', 'source_name', 'online_survey']);
            $detailargs['respondent_id'] = $respondent->id;
            $detailargs['closest_large_city'] = $request->closest_city;
            $detail = RespondentDetail::create($detailargs);

            //Respondent Occupation
            $occupationargs = $request->only(['occupation_id', 'industry_id']);
            $occupationargs['respondent_id'] = $respondent->id;
            $occupation = RespondentOccupation::create($occupationargs);

            //Respondent Health
            $healthargs = $request->only(['cigarette_brands', 'health_problems', 'other_health_problem', 'brands']);
            $healthargs['respondent_id'] = $respondent->id;
            $health = RespondentHealth::create($healthargs);

            //HouseHold
            $household['respondent_id'] = $respondent->id;
            $household['original_country'] = $request->original_country;
            $household['ethnic_background'] = $request->ethnic_background;
            $household['residency_status'] = $request->residency_status;
            $health = RespondentHousehold::create($household);

            //product
            $product['respondent_id'] = $respondent->id;
            $product = RespondentProduct::create($product);

            //services
            $service['respondent_id'] = $respondent->id;
            $service = RespondentService::create($service);

            //summary
            $summary['respondent_id'] = $respondent->id;
            $summary = RespondentSummary::create($summary);

            $data = ['respondent' => $respondent, 'password' => $request->password];
            Mail::send('emails.respondent_register_success', $data, function ($message) use ($respondent) {
                $message->from('office@avaresearch.com.au', 'Ava Research');
                $message->to($respondent->email);
                $message->subject('Ava Research: Registration successful');
            });

            if (Mail::failures()) {
                return json_encode(Mail::failures());
            }

            if ($detail)
                return redirect()->back()->withSuccess('Your Details submitted successfully!');
            else
                return redirect()->back()->withErrors(['message' => 'Problem while creating Respondent. Please try again']);
        } else {
            return redirect()->back()->withErrors(['message' => 'Problem while creating Respondent. Please try again']);
        }
    }

    public function suburb(Request $request)
    {
        $result = MasterPostcode::where(['postcode' => $request->postcode, 'isactive' => '1'])->pluck("suburb", "id");
        return response()->json($result);
    }

    public function ethnicity(Request $request)
    {
        $result = MasterEthnicBackground::whereIn('country_id', [0, $request->country_id])->where('isactive', 1)->pluck("name", "id");
        return response()->json($result);
    }

    public function respondentLoginForm()
    {
        return view('front.respondents.login');
    }

    public function respondentLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::guard('respondent')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/profile/edit');
        }

        return redirect()->back()->withErrors(['email' => 'These credentials do not match our records.']);
    }


    public function unsubscribe($uuid){
        return view('front.respondents.unsubscribe', compact('uuid'));
    }

    public function unsubscribeWithReason(Request $request){
        $this->validate($request, [
            'reason'   => 'required|',
            'password' => 'required'
        ]);

       //if (Auth::guard('respondent')->attempt(['uuid' => $request->uuid, 'password' => $request->password] )) {

        $respondent = Respondent::where('uuid', $request->uuid)->first();
        if($respondent){
        if(Hash::check($request->password, $respondent->password))
        {
            $respondent->update(["delete_reson" => $request->reason, "deleted_by" => "Self"]);        
            $respondent->delete();

            return view('front.respondents.unsubscribe-success');
        }

       return redirect()->back()->withErrors(['message' => 'Password Not Matched in our Database']);
        }else{
            return redirect()->back()->withErrors(['message' => 'Your Account Already Unsubscribed']);
        }
        
    }

     public function emailUnsubscribe($uuid)
    {
        $respondent = Respondent::where('uuid', $uuid)->first();
        if($respondent){
            $respondent->update(["stop_often_email" => '1']);        
            return view('front.respondents.unsubscribe-success');
        } else {
            return view('front.respondents.unsubscribe-error');
        }
    }


    
}
