<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Masters\MasterBrand;
use App\Models\Masters\MasterMobileProvider;
use App\Models\Masters\MasterState;
use App\Models\Masters\MasterPostcode;
use App\Models\Masters\MasterBirthCountry;
use App\Models\Masters\MasterOccupation;
use App\Models\Masters\MasterEmployerSize;
use App\Models\Masters\MasterIndustry;
use App\Models\Masters\MasterHealthProblem;
use App\Models\Masters\MasterHealthFund;
use App\Models\Masters\MasterBank;
use App\Models\Masters\MasterSuperannuation;
use App\Models\Masters\MasterInsuranceProvider;
use App\Models\Masters\MasterInsurancePolicy;
use App\Models\Masters\MasterPhoneProvider;
use App\Models\Masters\MasterVehicleMake;
use App\Models\Masters\MasterVehicleType;
use App\Models\Masters\MasterRewardProgram;
use App\Models\Masters\MasterTechnologyUse;
use App\Models\Masters\MasterInternetProvider;
use App\Models\Masters\MasterAlcoholConsumed;
use App\Models\Masters\MasterPreferredResearchLocation;
use App\Models\Masters\MasterEthnicBackground;
use App\Models\Masters\MasterResidencyStatus;

use App\User;
use App\UserDetail;
use App\Models\Project;
use App\Models\Respondents\Respondent;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth:web,respondent');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth('web')->check() && auth('web')->user()->type != 'client') {
            $clients_count = User::where('type', 'client')->count();
            $projects_count = Project::count();
            $staffs_count = User::where('type', 'staff')->count();
            $respondents_count = Respondent::count();

            //Running Projects
            $running_projects = Project::where('isactive', 1)->whereDate('start_date', '<=', date('Y-m-d'))
                ->whereDate('finish_date', '>=', date('Y-m-d'))->orderBy('created_at', 'DESC')->get();
            //Upcoming Projects
            $upcoming_projects = Project::where('isactive', 1)->whereDate('start_date', '>', date('Y-m-d'))
                ->orderBy('start_date', 'ASC')->get();

            return view('dashboard', compact(
                'clients_count',
                'projects_count',
                'staffs_count',
                'respondents_count',
                'running_projects',
                'upcoming_projects'
            ));
        } else if (auth('respondent')->check()) {
            return view('respondents_area.dashboard');
        } else if (auth('web')->user()->type == 'client') {
            return view('clients_area.dashboard');
        } else {
            return redirect(route('login'));
        }
    }

    public function editProfile()
    {
        if (auth('web')->check() && auth('web')->user()->type != 'client') {
            $user = User::with('profile')->find(Auth::id());
            return view('edit-profile', compact('user'));
        } else if (auth('respondent')->check()) {
            $respondent = Respondent::find(auth('respondent')->id());

            $brands = MasterBrand::where('isactive', 1)->get();
            $mobile_providers = MasterMobileProvider::where('isactive', 1)->get();
            $states = MasterState::where('isactive', 1)->get();
            $countries = MasterBirthCountry::where('isactive', 1)->get();
            $occupations = MasterOccupation::where('isactive', 1)->get();
            $employer_sizes = MasterEmployerSize::where('isactive', 1)->get();
            $industries = MasterIndustry::where('isactive', 1)->get();
            $health_problems = MasterHealthProblem::where('isactive', 1)->get();
            $health_funds = MasterHealthFund::where('isactive', 1)->get();
            $banks = MasterBank::where('isactive', 1)->get();
            $super_annutions = MasterSuperannuation::where('isactive', 1)->get();
            $insurance_providers = MasterInsuranceProvider::where('isactive', 1)->get();
            $insurance_policies = MasterInsurancePolicy::where('isactive', 1)->get();
            $phone_providers = MasterPhoneProvider::where('isactive', 1)->get();
            $vehicle_makes = MasterVehicleMake::where('isactive', 1)->get();
            $vehicle_types = MasterVehicleType::where('isactive', 1)->get();
            $reward_programs = MasterRewardProgram::where('isactive', 1)->get();
            $technology_uses = MasterTechnologyUse::where('isactive', 1)->get();
            $internet_providers = MasterInternetProvider::where('isactive', 1)->get();
            $alcohol_consumeds = MasterAlcoholConsumed::where('isactive', 1)->get();
            $preferred_locations = MasterPreferredResearchLocation::where('isactive', 1)->get();
            $ethnic_backgrounds = MasterEthnicBackground::select('name')->distinct()->where('isactive', 1)->orderBy('name')->get();
            $residency_status = MasterResidencyStatus::select('name')->distinct()->where('isactive', 1)->get();

            return view('respondents_area.edit-profile', compact(
                'respondent',
                'brands',
                'mobile_providers',
                'states',
                'countries',
                'occupations',
                'employer_sizes',
                'industries',
                'health_problems',
                'health_funds',
                'banks',
                'super_annutions',
                'insurance_providers',
                'insurance_policies',
                'phone_providers',
                'vehicle_makes',
                'vehicle_types',
                'reward_programs',
                'technology_uses',
                'internet_providers',
                'alcohol_consumeds',
                'preferred_locations',
                'ethnic_backgrounds',
                'residency_status'
            ));
        } elseif (auth('web')->user()->type == 'client') {
            $user = auth('web')->user();
            return view('clients_area.edit-profile', compact('user'));
        } else {
            return redirect(route('login'));
        }
    }

    public function saveProfile(Request $request)
    {
        $user_detail = UserDetail::where('user_id', Auth::id());
        $user_detail->update($request->except('_token'));
        return redirect()->back()->with(['message' => 'Profile updated']);
    }
}
