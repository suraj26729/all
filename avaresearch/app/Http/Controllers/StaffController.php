<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\UserDetail;
use Auth;

class StaffController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $staffs = User::with('profile')->where('type', 'staff')->get();
        return view('staffs.index', compact('staffs'));
    }

    public function create()
    {
        return view('staffs.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'contact_no' => 'required|numeric',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required|min:5|max:10|confirmed',
            'password_confirmation' => 'required|min:5|max:10',
            'address' => 'required'
        ];
        $this->validate($request, $rules, ['password.confirmed' => 'Password and Confirm Password not matched']);

        $args = $request->only(['email', 'password']);
        $args['type'] = 'staff';
        $args['password'] = bcrypt($request->password);
        $username = explode('@', $request->email);
        $username = $username[0];
        $user = User::where('username', $username)->exists();
        if ($user) $username = $username . rand(1, 99);
        $args['username'] = $username;

        $user = User::create($args);
        if ($user) {
            $path = 'avatar-images/' . date('Y') . '/' . date('m');
            $args = $request->except(['email', 'password', 'password_confirmation']);
            if ($request->file('image')) {
                $uploadedFile = $request->file('image');
                $filename = time() . $uploadedFile->getClientOriginalName();
                Storage::disk('public')->putFileAs($path, $uploadedFile, $filename);
                $args['image'] = $path . '/' . $filename;
            }

            $args['user_id'] = $user->id;

            $detail = UserDetail::create($args);
            if ($detail)
                return redirect()->back()->withSuccess('Staff created successfully!');
            else
                return redirect()->back()->withErrors(['message' => 'Problem while creating staff profile. Please try again']);
        } else {
            return redirect()->back()->withErrors(['message' => 'Problem while creating staff. Please try again']);
        }
    }

    public function edit($uuid)
    {
        $user = User::with(['profile'])->where('uuid', $uuid)->first();
        return view('staffs.edit', compact('user'));
    }

    public function update(Request $request, User $staff)
    {
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'contact_no' => 'required|numeric',
            'email' => 'required|email|max:255|unique:users,email,' . $staff->id,
            'address' => 'required'
        ];
        $this->validate($request, $rules, []);

        $staff->update(['email' => $request->email]);

        $data = $request->except(['email', 'image']);

        $path = 'avatar-images/' . date('Y') . '/' . date('m');
        $url = '';
        if ($request->file('image')) {
            $uploadedFile = $request->file('image');
            $filename = time() . $uploadedFile->getClientOriginalName();
            Storage::disk('public')->putFileAs($path, $uploadedFile, $filename);
            $url = $path . '/' . $filename;
            $data['image'] = $url;
        }

        $staff->profile->update($data);

        return redirect()->back()->withSuccess('Staff details updated successfully!');
    }

    public function destroy(User $staff)
    {
        $staff->profile->delete();
        $staff->delete();
        return response()->json(1);
    }
}
