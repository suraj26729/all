<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateAttachment;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

class EmailTemplateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('email_templates.index');
    }

    public function DTData()
    {
        $templates = EmailTemplate::select(
            'email_templates.id',
            'email_templates.uuid',
            'email_templates.title',
            'email_templates.isactive',
            'email_templates.created_by',
            'email_templates.created_at'
        );
        return Datatables::of($templates)
            ->addColumn('template_name', function ($template) {
                return '<a href="' . route('email-templates.edit', $template->uuid) . '">
                    ' . $template->title  . '
                </a>';
            })
            ->addColumn('created_user_name', function ($template) {
                if ($template->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $template->createdBy->uuid) . '">
                    ' . $template->createdBy->profile->first_name . ' ' . $template->createdBy->profile->last_name . '
                </a>';
                } else {
                    return '';
                }
            })
            ->editColumn('created_at', function ($template) {
                return $template->created_at->format('d-m-Y');
            })
            ->addColumn('actions', function ($template) {
                return '<td>
                <a href="' . route('email-templates.edit', $template->uuid) . '"><i class="icon-pencil mr-3"></i></a>
                <a href="javascript:void(0);" class="delete_btn" data-id="' . $template->id . '" data-name="' . $template->title . '"><i class="icon-trash"></i></a>
            </td>';
            })->rawColumns(['template_name', 'actions', 'created_user_name'])->make(true);
    }

    public function create()
    {
        return view('email_templates.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];
        $this->validate($request, $rules, []);

        //dd($request->all());

        $data = $request->except('attachment');
        $data['created_by'] = auth()->id();

        $template = EmailTemplate::create($data);
        if ($template) {
            if ($request->has('attachment') && count($request->attachment)) {
                $args = [];
                $files = $request->file('attachment');
                if (!empty($files) && count($files)) {
                    $path = 'docs/attachments';
                    foreach ($files as $file) {
                        $filename = time() . $file->getClientOriginalName();
                        Storage::disk('public')->putFileAs($path, $file, $filename);
                        $template->attachments()->create(['template_id' => $template->id, 'url' => $path . '/' . $filename]);
                    }
                }
            }
            return redirect()->back()->withSuccess('Email Template created successfully!');
        } else
            return redirect()->back()->withErrors(['message' => 'Problem while creating email template. Please try again']);
    }

    public function edit($uuid)
    {
        $template = EmailTemplate::where('uuid', $uuid)->first();
        return view('email_templates.edit', compact('template'));
    }

    public function update(Request $request, EmailTemplate $emailTemplate)
    {
        $rules = [
            'title' => 'required|max:255',
            'message' => 'required'
        ];

        $this->validate($request, $rules, []);

        $data = $request->except('attachment');
        $data['modified_by'] = auth()->id();

        $emailTemplate->update($data);

        if ($request->has('attachment') && count($request->attachment)) {
            $emailTemplate->attachments()->delete();
            $args = [];
            $files = $request->file('attachment');
            if (!empty($files) && count($files)) {
                $path = 'docs/attachments';
                foreach ($files as $file) {
                    $filename = time() . str_replace(' ', '-', $file->getClientOriginalName());
                    Storage::disk('public')->putFileAs($path, $file, $filename);
                    $emailTemplate->attachments()->create(['template_id' => $emailTemplate->id, 'url' => $path . '/' . $filename]);
                }
            }
            foreach ($request->attachment as $url) {
                if (is_string($url)) {
                    $emailTemplate->attachments()->create(['template_id' => $emailTemplate->id, 'url' => $url]);
                }
            }
        }
        return redirect()->back()->withSuccess('Email Template updated successfully!');
    }

    public function destroy(EmailTemplate $emailTemplate)
    {
        $emailTemplate->delete();
        return response()->json(1);
    }

    public function deleteEmailTempAttach(Request $request)
    {
        $attch = EmailTemplateAttachment::find($request->id);
        if ($attch != null) {
            unlink(storage_path('app/public/' . $attch->url));
            $attch->delete();
        }
        return response()->json(1);
    }

    public function singleJson(EmailTemplate $template)
    {
        $template = EmailTemplate::with(['attachments'])->find($template->id);
        return response()->json($template);
    }
}
