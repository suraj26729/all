<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\ProjectGroup;
use App\User;

use Yajra\Datatables\Datatables;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('calendar.index');
    }

    public function DTData(Request $request)
    {
        $date = date('Y-m-d', strtotime($request->date));
        $groups = ProjectGroup::whereDate('project_groups.group_date', $date)
            ->join('projects', 'projects.id', '=', 'project_groups.job_id')
            ->join('master_states', 'project_groups.location', '=', 'master_states.id')
            ->select(
                'project_groups.id',
                'project_groups.uuid',
                'project_groups.name',
                'projects.job_name',
                'projects.uuid as puuid',
                'project_groups.location',
                'project_groups.incentive',
                'project_groups.other',
                'master_states.name as mlocation'
            );

        return Datatables::of($groups)
            ->addColumn('project_name', function ($group) {
                return '<a href="' . route('projects.edit', $group->puuid) . '">
                    ' . $group->job_name  . '
                </a>';
            })
            ->addColumn('actions', function ($group) {
                return '<td>
                <a class="btn btn-primary btn-sm" title="View Group Details" href="' . route('editGroup', [$group->puuid, $group->uuid]) . '" target="_blank">
                <i class="icon-eye"></i> View
                </a>
            </td>';
            })->rawColumns(['project_name', 'location', 'actions'])->make(true);
    }
}
