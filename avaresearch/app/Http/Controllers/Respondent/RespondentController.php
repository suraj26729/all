<?php

namespace App\Http\Controllers\Respondent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Masters\MasterBrand;
use App\Models\Masters\MasterMobileProvider;
use App\Models\Masters\MasterState;
use App\Models\Masters\MasterPostcode;
use App\Models\Masters\MasterBirthCountry;
use App\Models\Masters\MasterOccupation;
use App\Models\Masters\MasterEmployerSize;
use App\Models\Masters\MasterIndustry;
use App\Models\Masters\MasterHealthProblem;
use App\Models\Masters\MasterHealthFund;
use App\Models\Masters\MasterBank;
use App\Models\Masters\MasterSuperannuation;
use App\Models\Masters\MasterInsuranceProvider;
use App\Models\Masters\MasterInsurancePolicy;
use App\Models\Masters\MasterPhoneProvider;
use App\Models\Masters\MasterVehicleMake;
use App\Models\Masters\MasterVehicleType;
use App\Models\Masters\MasterRewardProgram;
use App\Models\Masters\MasterTechnologyUse;
use App\Models\Masters\MasterInternetProvider;
use App\Models\Masters\MasterAlcoholConsumed;
use App\Models\Masters\MasterPreferredResearchLocation;
use App\Models\Masters\MasterEthnicBackground;
use App\Models\Masters\MasterResidencyStatus;
use App\Models\Masters\MasterClosestCity;

use App\Models\Respondents\Respondent;
use App\Models\Respondents\RespondentDetail;
use App\Models\Respondents\RespondentHousehold;
use App\Models\Respondents\RespondentOccupation;
use App\Models\Respondents\RespondentHealth;
use App\Models\Respondents\RespondentService;
use App\Models\Respondents\RespondentProduct;
use App\Models\Respondents\RespondentSummary;

use App\Models\Project\JobEvent;

use Yajra\Datatables\Datatables;
use DB;
use Exception;

class RespondentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:web,respondent'], ['only' => ['nameDOBCheck', 'mobileCheck', 'emailCheck', 'update']]);
        $this->middleware(['auth:web'], ['except' => ['getSuburbs','getclosescity', 'nameDOBCheck', 'mobileCheck', 'emailCheck', 'update']]);
    }
    public function index()
    {
        return view('respondents.index');
    }

    public function DTData()
    {
        $respondents = Respondent::join('respondent_details', 'respondent_details.respondent_id', '=', 'respondents.id')
            ->leftjoin('respondent_households', 'respondent_households.respondent_id', '=', 'respondents.id')
            ->leftjoin('master_states', 'master_states.id', '=', 'respondent_details.state')
            ->leftjoin('master_birth_countries', 'master_birth_countries.id', '=', 'respondent_households.original_country')
            ->where('respondents.isactive', 1)
            ->select(
                'respondents.id',
                'respondents.uuid',
                'respondents.first_name',
                'respondents.last_name',
                'respondents.email',
                'respondents.gender',
                'respondent_details.mobile_phone',
                'master_states.name as state',
                'master_birth_countries.name as country',
                'respondents.created_by',
                'respondents.created_at'
            );
        return Datatables::of($respondents)
            ->addColumn('respondent_name', function ($respondent) {
                return '<a href="' . route('respondents.edit', $respondent->uuid) . '">
                            <strong><i class="fa fa-' . strtolower($respondent->gender) . ' mr-2"></i>' . $respondent->first_name . ' ' . $respondent->last_name . '</strong>
                        </a><br/><small>' . $respondent->email . '</small>';
            })
            ->addColumn('created_user_name', function ($respondent) {
                if ($respondent->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $respondent->createdBy->uuid) . '">
                ' . $respondent->createdBy->profile->first_name . ' ' . $respondent->createdBy->profile->last_name . '
            </a>';
                } else {
                    return 'Self';
                }
            })
            ->editColumn('created_at', function ($respondent) {
                return $respondent->created_at->format('d-m-Y');
            })
            ->addColumn('actions', function ($respondent) {
                return '<td>
                <a href="' . route('respondents.edit', $respondent->uuid) . '"><i class="icon-pencil mr-3"></i></a>
                <a href="javascript:void(0);" class="delete_btn" data-id="' . $respondent->id . '" data-name="' . $respondent->first_name . ' ' . $respondent->last_name  . '"><i class="icon-trash"></i></a>
            </td>';
            })->rawColumns(['respondent_name', 'actions', 'created_user_name'])->make(true);
    }

    public function create()
    {
        $brands = MasterBrand::where('isactive', 1)->get();
        $mobile_providers = MasterMobileProvider::where('isactive', 1)->get();
        $states = MasterState::where('isactive', 1)->get();
        $countries = MasterBirthCountry::where('isactive', 1)->get();
        $occupations = MasterOccupation::where('isactive', 1)->get();
        $employer_sizes = MasterEmployerSize::where('isactive', 1)->get();
        $industries = MasterIndustry::where('isactive', 1)->get();
        $health_problems = MasterHealthProblem::where('isactive', 1)->orderByRaw('IF (name = "other", 9999999, 0)')->orderBy('name', 'ASC')->get();
        $health_funds = MasterHealthFund::where('isactive', 1)->orderByRaw('IF (name = "other", 9999999, 0)')->orderBy('name', 'ASC')->get();
        $banks = MasterBank::where('isactive', 1)->get();
        $super_annutions = MasterSuperannuation::where('isactive', 1)->get();
        $insurance_providers = MasterInsuranceProvider::where('isactive', 1)->get();
        $insurance_policies = MasterInsurancePolicy::where('isactive', 1)->get();
        $phone_providers = MasterPhoneProvider::where('isactive', 1)->orderBy('created_at', 'ASC')->get();
        $vehicle_makes = MasterVehicleMake::where('isactive', 1)->get();
        $vehicle_types = MasterVehicleType::where('isactive', 1)->orderBy('name')->get();
        $reward_programs = MasterRewardProgram::where('isactive', 1)->get();
        $technology_uses = MasterTechnologyUse::where('isactive', 1)->get();
        $internet_providers = MasterInternetProvider::where('isactive', 1)->get();
        $alcohol_consumeds = MasterAlcoholConsumed::where('isactive', 1)->get();
        $preferred_locations = MasterPreferredResearchLocation::where('isactive', 1)->get();
        $ethnic_backgrounds = MasterEthnicBackground::select('name')->distinct()->where('isactive', 1)->orderBy('name')->get();
        $residency_status = MasterResidencyStatus::select('name')->distinct()->where('isactive', 1)->get();

        return view('respondents.create', compact(
            'brands',
            'mobile_providers',
            'states',
            'countries',
            'occupations',
            'employer_sizes',
            'industries',
            'health_problems',
            'health_funds',
            'banks',
            'super_annutions',
            'insurance_providers',
            'insurance_policies',
            'phone_providers',
            'vehicle_makes',
            'vehicle_types',
            'reward_programs',
            'technology_uses',
            'internet_providers',
            'alcohol_consumeds',
            'preferred_locations',
            'ethnic_backgrounds',
            'residency_status'
        ));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only(['first_name', 'last_name', 'gender', 'email', 'additional_email', 'password']);

            $data['dob'] = $request->dob_year . '-' . $request->dob_mon . '-' . $request->dob_day;

            $detail = $request->only([
                'state', 'closest_large_city', 'home_postcode', 'home_postsuburb', 'home_subdirection', 'work_postcode', 'work_postsuburb', 'work_subdirection',
                'work_subdirection', 'home_phone', 'work_phone', 'mobile_phone', 'mobile_phone2', 'hear_about', 'source_name', 'online_survey',
                'articulation'
            ]);
            $detail['online_survey'] = isset($detail['online_survey']) ? $detail['online_survey'] : 0;

            $household = $request->only([
                'marital_status', 'home_ownership', 'household_type', 'income_level', 'residence_type',
                'residency_status', 'birth_country', 'other_country', 'original_country', 'ethnic_background', 'pets',
                'children', 'children_allowed'
            ]);
            $household['pets'] = isset($household['pets']) && count($household['pets']) ? implode(',', $household['pets']) : null;

            $children_names = $request->child_name;
            $children_birth_months = $request->birth_month;
            $children_birth_years = $request->birth_year;
            $children_genders = $request->child_gender;
            $children_left_homes = $request->left_home;

            $occupation = $request->only([
                'profession', 'occupation_id', 'partner_occupation_id', 'occupation_type', 'hours_per_week',
                'job_title', 'partner_job_title', 'employer_name', 'partner_employer_name', 'employer_size_id', 'partner_employer_size_id',
                'employer_turnover', 'partner_employer_turnover', 'industry_id', 'partner_industry', 'highest_education', 'education_institute_type',
                'education_institute_suburb', 'education_year', 'education_study_field', 'partner_education_study_field', 'company_name', 'company_postcode',
                'company_website', 'company_email'
            ]);
            $occupation['type'] = $occupation['occupation_type'];
            unset($occupation['occupation_type']);

            $health = $request->only([
                'health_problems', 'other_health_problem', 'dietary_requirements', 'health_funds', 'other_health_fund', 'cigarette_brands',
                'partner_cigarette_brands', 'organ_donate', 'donated_organ', 'quite_smoking_methods', 'nicotine_replacement',
                'brands', 'orther_brands', 'partner_brands', 'other_partner_brands'
            ]);

            $service = $request->only([
                'bank_id', 'superannuation_id', 'non_super_investments', 'insurance_providers', 'insurance_policies',
                'home_phone_provider', 'other_home_phone_provider', 'work_phone_provider', 'other_work_phone_provider', 'mobile1_provider',
                'mobile2_provider', 'mobile1_provider_other', 'mobile2_provider_other', 'mobile1_type', 'mobile2_type', 'tv_providers'
            ]);

            $product = $request->only([
                'vehicle_make_id', 'vehicle2_make_id', 'vehicle_type_id', 'other_vehicle_type', 'vehicle2_type_id', 'other_vehicle2_type',
                'vehicle_model', 'vehicle2_model', 'vehicle_year', 'vehicle2_year', 'travel_location', 'travel_reason',
                'travel_frequency', 'rewards_progammes', 'technology_use', 'general_products', 'internet_provider', 'alcohol_consumed',
                'optical', 'lens_brand'
            ]);

            $summary = $request->only([
                'preferred_research_location', 'availability_times', 'session_type', 'preferred_contact_method', 'additional_notes'
            ]);

            $data['password'] = bcrypt($data['password']);
            $data['created_by'] = auth()->id();
            $respondent = Respondent::create($data);
            if ($respondent) {
                $detail = $respondent->detail()->create($detail);
                $_houseHold = $respondent->houseHold()->create($household);
                if ($household['children'] == 1) {
                    foreach ($children_names as $key => $item) {
                        if ($item != null) {
                            $respondent->childrens()->create([
                                'respondent_id' => $respondent->id,
                                'child_name' => $item,
                                'gender' => $children_genders[$key],
                                'birth_month' => $children_birth_months[$key],
                                'birth_year' => $children_birth_years[$key],
                                'left_home' => isset($children_left_homes[$key]) ? 1 : 0
                            ]);
                        }
                    }
                }

                $occupation = $respondent->occupation()->create($occupation);

                //Health
                $health_problems = $request->health_problems;
                $health_problems = isset($health_problems) && count($health_problems) ? implode(',', $health_problems) : null;
                $health['health_problems'] = $health_problems;

                $dietary_requirements = $request->dietary_requirements;
                $dietary_requirements = isset($dietary_requirements) && count($dietary_requirements) ? implode(',', $dietary_requirements) : null;
                $health['dietary_requirements'] = $dietary_requirements;

                $health_funds = $request->health_funds;
                $health_funds = isset($health_funds) && count($health_funds) ? implode(',', $health_funds) : null;
                $health['health_funds'] = $health_funds;

                $quite_smoking_methods = $request->quite_smoking_methods;
                $quite_smoking_methods = isset($quite_smoking_methods) && count($quite_smoking_methods) ? implode(',', $quite_smoking_methods) : null;
                $health['quite_smoking_methods'] = $quite_smoking_methods;

                $nicotine_replacement = $request->nicotine_replacement;
                $nicotine_replacement = isset($nicotine_replacement) && count($nicotine_replacement) ? implode(',', $nicotine_replacement) : null;
                $health['nicotine_replacement'] = $nicotine_replacement;

                $brands = null;
                if ($request->cigratte_brands != "Doesn't Smoke") {
                    $brands = $request->brands;
                    $brands = isset($brands) && count($brands) ? implode(',', $brands) : null;
                }
                $health['brands'] = $brands;

                $partner_brands = null;
                if ($request->partner_brands != "Doesn't Smoke") {
                    $partner_brands = $request->partner_brands;
                    $partner_brands = isset($partner_brands) && count($partner_brands) ? implode(',', $partner_brands) : null;
                }
                $health['partner_brands'] = $partner_brands;

                $health = $respondent->health()->create($health);

                //Services
                $bank_id = $request->bank_id;
                $bank_id = isset($bank_id) && count($bank_id) ? implode(',', $bank_id) : null;
                $service['bank_id'] = $bank_id;

                $superannuation_id = $request->superannuation_id;
                $superannuation_id = isset($superannuation_id) && count($superannuation_id) ? implode(',', $superannuation_id) : null;
                $service['superannuation_id'] = $superannuation_id;

                $non_super_investments = $request->_non_super_investments;
                $non_super_investments = isset($non_super_investments) && count($non_super_investments) ? implode(',', $non_super_investments) : null;
                $service['non_super_investments'] = $non_super_investments;

                $insurance_providers = $request->insurance_providers;
                $insurance_providers = isset($insurance_providers) && count($insurance_providers) ? implode(',', $insurance_providers) : null;
                $service['insurance_providers'] = $insurance_providers;

                $insurance_policies = $request->insurance_policies;
                $insurance_policies = isset($insurance_policies) && count($insurance_policies) ? implode(',', $insurance_policies) : null;
                $service['insurance_policies'] = $insurance_policies;

                $tv_providers = $request->tv_providers;
                $tv_providers = isset($tv_providers) && count($tv_providers) ? implode(',', $tv_providers) : null;
                $service['tv_providers'] = $tv_providers;

                $service = $respondent->service()->create($service);

                //Products
                $rewards_progammes = $request->rewards_progammes;
                $rewards_progammes = isset($rewards_progammes) && count($rewards_progammes) ? implode(',', $rewards_progammes) : null;
                $product['rewards_progammes'] = $rewards_progammes;

                $technology_use = $request->technology_use;
                $technology_use = isset($technology_use) && count($technology_use) ? implode(',', $technology_use) : null;
                $product['technology_use'] = $technology_use;

                $general_products = $request->general_products;
                $general_products = isset($general_products) && count($general_products) ? implode(',', $general_products) : null;
                $product['general_products'] = $general_products;

                $optical = $request->optical;
                $optical = isset($optical) && count($optical) ? implode(',', $optical) : null;
                $product['optical'] = $optical;

                $product = $respondent->product()->create($product);

                //Summary
                $preferred_research_location = $request->preferred_research_location;
                $preferred_research_location = isset($preferred_research_location) && count($preferred_research_location) ? implode(',', $preferred_research_location) : null;
                $summary['preferred_research_location'] = $preferred_research_location;

                $availability_times = $request->availability_times;
                $availability_times = isset($availability_times) && count($availability_times) ? implode(',', $availability_times) : null;
                $summary['availability_times'] = $availability_times;

                $session_type = $request->session_type;
                $session_type = isset($session_type) && count($session_type) ? implode(',', $session_type) : null;
                $summary['session_type'] = $session_type;

                $preferred_contact_method = $request->preferred_contact_method;
                $preferred_contact_method = isset($preferred_contact_method) && count($preferred_contact_method) ? implode(',', $preferred_contact_method) : null;
                $summary['preferred_contact_method'] = $preferred_contact_method;

                $summary = $respondent->summary()->create($summary);

                DB::commit();
                session()->flash('success', 'Respondent created sucessfully');
            }
            return response()->json(['respondent' => $respondent->id], 201);
        } catch (Exception $ex) {
            DB::rollBack();
            return response()->json($ex, 500);
        }
    }

    public function edit($uuid)
    {
        $respondent = Respondent::where('uuid', $uuid)->first();

        $brands = MasterBrand::where('isactive', 1)->get();
        $mobile_providers = MasterMobileProvider::where('isactive', 1)->get();
        $states = MasterState::where('isactive', 1)->get();
        $countries = MasterBirthCountry::where('isactive', 1)->get();
        $occupations = MasterOccupation::where('isactive', 1)->get();
        $employer_sizes = MasterEmployerSize::where('isactive', 1)->get();
        $industries = MasterIndustry::where('isactive', 1)->get();
        $health_problems = MasterHealthProblem::where('isactive', 1)->orderByRaw('IF (name = "other", 9999999, 0)')->orderBy('name', 'ASC')->get();
        $health_funds = MasterHealthFund::where('isactive', 1)->orderByRaw('IF (name = "other", 9999999, 0)')->orderBy('name', 'ASC')->get();
        $banks = MasterBank::where('isactive', 1)->get();
        $super_annutions = MasterSuperannuation::where('isactive', 1)->get();
        $insurance_providers = MasterInsuranceProvider::where('isactive', 1)->get();
        $insurance_policies = MasterInsurancePolicy::where('isactive', 1)->get();
        $phone_providers = MasterPhoneProvider::where('isactive', 1)->orderBy('created_at', 'ASC')->get();
        $vehicle_makes = MasterVehicleMake::where('isactive', 1)->get();
        $vehicle_types = MasterVehicleType::where('isactive', 1)->orderBy('name')->get();
        $reward_programs = MasterRewardProgram::where('isactive', 1)->get();
        $technology_uses = MasterTechnologyUse::where('isactive', 1)->get();
        $internet_providers = MasterInternetProvider::where('isactive', 1)->get();
        $alcohol_consumeds = MasterAlcoholConsumed::where('isactive', 1)->get();
        $preferred_locations = MasterPreferredResearchLocation::where('isactive', 1)->get();
        $ethnic_backgrounds = MasterEthnicBackground::select('name')->distinct()->where('isactive', 1)->orderBy('name')->get();
        $residency_status = MasterResidencyStatus::select('name')->distinct()->where('isactive', 1)->get();

        return view('respondents.edit', compact(
            'respondent',
            'brands',
            'mobile_providers',
            'states',
            'countries',
            'occupations',
            'employer_sizes',
            'industries',
            'health_problems',
            'health_funds',
            'banks',
            'super_annutions',
            'insurance_providers',
            'insurance_policies',
            'phone_providers',
            'vehicle_makes',
            'vehicle_types',
            'reward_programs',
            'technology_uses',
            'internet_providers',
            'alcohol_consumeds',
            'preferred_locations',
            'ethnic_backgrounds',
            'residency_status'
        ));
    }

    public function update(Request $request, Respondent $respondent)
    {
        try {
            $data = $request->only([
                'do_not_call', 'one_off_resp', 'has_accent', 'do_no_show', 'stop_often_email', 'excellent_candidate', 'wrong_number',
                'keen_candidate', 'isactive', 'first_name', 'last_name', 'gender', 'email', 'additional_email', 'password'
            ]);

            $data['do_not_call'] = isset($data['do_not_call']) ? 1 : 0;
            $data['one_off_resp'] = isset($data['one_off_resp']) ? 1 : 0;
            $data['has_accent'] = isset($data['has_accent']) ? 1 : 0;
            $data['do_no_show'] = isset($data['do_no_show']) ? 1 : 0;
            $data['stop_often_email'] = isset($data['stop_often_email']) ? 1 : 0;
            $data['excellent_candidate'] = isset($data['excellent_candidate']) ? 1 : 0;
            $data['wrong_number'] = isset($data['wrong_number']) ? 1 : 0;
            $data['keen_candidate'] = isset($data['keen_candidate']) ? 1 : 0;
            $data['keen_candidate'] = isset($data['keen_candidate']) ? 1 : 0;
            $data['isactive'] = isset($data['isactive']) ? 0 : 1;

            $data['dob'] = $request->dob_year . '-' . $request->dob_mon . '-' . $request->dob_day;

            $detail = $request->only([
                'articulation','state', 'closest_large_city', 'home_postcode', 'home_postsuburb', 'home_subdirection', 'work_postcode', 'work_postsuburb', 'work_subdirection',
                'home_phone', 'work_phone', 'mobile_phone', 'mobile_phone2', 'hear_about', 'source_name', 'online_survey',
                
            ]);
            $detail['online_survey'] = isset($detail['online_survey']) ? $detail['online_survey'] : 0;

            $household = $request->only([
                'marital_status', 'home_ownership', 'household_type', 'income_level', 'residence_type',
                'residency_status', 'birth_country', 'other_country', 'original_country', 'ethnic_background', 'pets',
                'children', 'children_allowed'
            ]);
            $household['pets'] = isset($household['pets']) && count($household['pets']) ? implode(',', $household['pets']) : null;

            $children_names = $request->child_name;
            $children_birth_months = $request->children_birth_months;
            $children_birth_years = $request->children_birth_years;
            $children_genders = $request->child_gender;
            $children_left_homes = $request->left_home;

            $occupation = $request->only([
                'profession', 'occupation_id', 'partner_occupation_id', 'occupation_type', 'hours_per_week',
                'job_title', 'partner_job_title', 'employer_name', 'partner_employer_name', 'employer_size_id', 'partner_employer_size_id',
                'employer_turnover', 'partner_employer_turnover', 'industry_id', 'partner_industry', 'highest_education', 'education_institute_type',
                'education_institute_suburb', 'education_year', 'education_study_field', 'partner_education_study_field', 'company_name', 'company_postcode',
                'company_website', 'company_email'
            ]);
            $occupation['type'] = $occupation['occupation_type'];
            unset($occupation['occupation_type']);

            $health = $request->only([
                'health_problems', 'other_health_problem', 'dietary_requirements', 'health_funds', 'other_health_fund', 'cigarette_brands',
                'partner_cigarette_brands', 'organ_donate', 'donated_organ', 'quite_smoking_methods', 'nicotine_replacement',
                'brands', 'orther_brands', 'partner_brands', 'other_partner_brands'
            ]);

            $service = $request->only([
                'bank_id', 'superannuation_id', 'non_super_investments', 'insurance_providers', 'insurance_policies',
                'home_phone_provider', 'other_home_phone_provider', 'work_phone_provider', 'other_work_phone_provider', 'mobile1_provider', 'mobile2_provider', 'mobile1_provider_other',
                'mobile2_provider_other', 'mobile1_type', 'mobile2_type', 'tv_providers'
            ]);

            $product = $request->only([
                'vehicle_make_id', 'vehicle2_make_id', 'vehicle_type_id', 'other_vehicle_type', 'vehicle2_type_id', 'other_vehicle2_type',
                'vehicle_model', 'vehicle2_model', 'vehicle_year', 'vehicle2_year', 'travel_location', 'travel_reason',
                'travel_frequency', 'rewards_progammes', 'technology_use', 'general_products', 'internet_provider', 'alcohol_consumed',
                'optical', 'lens_brand'
            ]);

            $summary = $request->only([
                'preferred_research_location', 'availability_times', 'session_type', 'preferred_contact_method', 'additional_notes'
            ]);

            $data['password'] = $data['password'] != $respondent->password ? bcrypt($data['password']) : $data['password'];
            $respondent->update($data);

            $detail = $respondent->detail()->update($detail);
            $_houseHold = $respondent->houseHold()->update($household);
            if (isset($household['children']) && $household['children'] == 1) {
                $respondent->childrens()->forceDelete();
                foreach ($children_names as $key => $item) {
                    if ($item != null) {
                        $respondent->childrens()->create([
                            'respondent_id' => $respondent->id,
                            'child_name' => $item,
                            'gender' => $children_genders[$key],
                            'birth_month' => $children_birth_months[$key],
                            'birth_year' => $children_birth_years[$key],
                            'left_home' => isset($children_left_homes[$key]) ? 1 : 0
                        ]);
                    }
                }
            }

            $occupation = $respondent->occupation()->update($occupation);

            //Health
            $health_problems = $request->health_problems;
            $health_problems = isset($health_problems) && count($health_problems) ? implode(',', $health_problems) : null;
            $health['health_problems'] = $health_problems;

            $dietary_requirements = $request->dietary_requirements;
            $dietary_requirements = isset($dietary_requirements) && count($dietary_requirements) ? implode(',', $dietary_requirements) : null;
            $health['dietary_requirements'] = $dietary_requirements;

            $health_funds = $request->health_funds;
            $health_funds = isset($health_funds) && count($health_funds) ? implode(',', $health_funds) : null;
            $health['health_funds'] = $health_funds;

            $quite_smoking_methods = $request->quite_smoking_methods;
            $quite_smoking_methods = isset($quite_smoking_methods) && count($quite_smoking_methods) ? implode(',', $quite_smoking_methods) : null;
            $health['quite_smoking_methods'] = $quite_smoking_methods;

            $nicotine_replacement = $request->nicotine_replacement;
            $nicotine_replacement = isset($nicotine_replacement) && count($nicotine_replacement) ? implode(',', $nicotine_replacement) : null;
            $health['nicotine_replacement'] = $nicotine_replacement;

            $brands = null;
            if ($request->cigratte_brands != "Doesn't Smoke") {
                $brands = $request->brands;
                $brands = isset($brands) && count($brands) ? implode(',', $brands) : null;
            }
            $health['brands'] = $brands;

            $partner_brands = null;
            if ($request->partner_brands != "Doesn't Smoke") {
                $partner_brands = $request->partner_brands;
                $partner_brands = isset($partner_brands) && count($partner_brands) ? implode(',', $partner_brands) : null;
            }
            $health['partner_brands'] = $partner_brands;

            $health = $respondent->health()->update($health);

            //Services
            $bank_id = $request->bank_id;
            $bank_id = isset($bank_id) && count($bank_id) ? implode(',', $bank_id) : null;
            $service['bank_id'] = $bank_id;

            $superannuation_id = $request->superannuation_id;
            $superannuation_id = isset($superannuation_id) && count($superannuation_id) ? implode(',', $superannuation_id) : null;
            $service['superannuation_id'] = $superannuation_id;

            $non_super_investments = $request->_non_super_investments;
            $non_super_investments = isset($non_super_investments) && count($non_super_investments) ? implode(',', $non_super_investments) : null;
            $service['non_super_investments'] = $non_super_investments;

            $insurance_providers = $request->insurance_providers;
            $insurance_providers = isset($insurance_providers) && count($insurance_providers) ? implode(',', $insurance_providers) : null;
            $service['insurance_providers'] = $insurance_providers;

            $insurance_policies = $request->insurance_policies;
            $insurance_policies = isset($insurance_policies) && count($insurance_policies) ? implode(',', $insurance_policies) : null;
            $service['insurance_policies'] = $insurance_policies;

            $tv_providers = $request->tv_providers;
            $tv_providers = isset($tv_providers) && count($tv_providers) ? implode(',', $tv_providers) : null;
            $service['tv_providers'] = $tv_providers;

            $service = $respondent->service()->update($service);

            //Products
            $rewards_progammes = $request->rewards_progammes;
            $rewards_progammes = isset($rewards_progammes) && count($rewards_progammes) ? implode(',', $rewards_progammes) : null;
            $product['rewards_progammes'] = $rewards_progammes;

            $technology_use = $request->technology_use;
            $technology_use = isset($technology_use) && count($technology_use) ? implode(',', $technology_use) : null;
            $product['technology_use'] = $technology_use;

            $general_products = $request->general_products;
            $general_products = isset($general_products) && count($general_products) ? implode(',', $general_products) : null;
            $product['general_products'] = $general_products;

            $optical = $request->optical;
            $optical = isset($optical) && count($optical) ? implode(',', $optical) : null;
            $product['optical'] = $optical;

            $product = $respondent->product()->update($product);

            //Summary
            $preferred_research_location = $request->preferred_research_location;
            $preferred_research_location = isset($preferred_research_location) && count($preferred_research_location) ? implode(',', $preferred_research_location) : null;
            $summary['preferred_research_location'] = $preferred_research_location;

            $availability_times = $request->availability_times;
            $availability_times = isset($availability_times) && count($availability_times) ? implode(',', $availability_times) : null;
            $summary['availability_times'] = $availability_times;

            $session_type = $request->session_type;
            $session_type = isset($session_type) && count($session_type) ? implode(',', $session_type) : null;
            $summary['session_type'] = $session_type;

            $preferred_contact_method = $request->preferred_contact_method;
            $preferred_contact_method = isset($preferred_contact_method) && count($preferred_contact_method) ? implode(',', $preferred_contact_method) : null;
            $summary['preferred_contact_method'] = $preferred_contact_method;

            $summary = $respondent->summary()->update($summary);

            session()->flash('success', 'Respondent updated sucessfully');
            return response()->json(['respondent' => $respondent->id], 200);
        } catch (Exception $ex) {
            //dd($ex);
            return response()->json($ex, 500);
        }
    }

    public function destroy(Respondent $respondent)
    {
        $respondent->delete();
        return response()->json(1);
    }

    public function getSuburbs($postcode, $state_id)
    {
        $suburbs = MasterPostcode::where(['state_id' => $state_id, 'postcode' => $postcode, 'isactive' => 1])->get();
        return response($suburbs, 200);
    }
    public function getclosescity($state_id)
    {
        $closestcity = MasterClosestCity::where(['state_id' => $state_id])->get();
        return response($closestcity, 200);
    }

    public function nameDOBCheck(Request $request)
    {
        $first_name = $request->first_name;
        $dob = $request->dob;
        $id = $request->respondent_id;
        $exists = Respondent::where(['first_name' => $first_name, 'dob' => $dob]);
        if (!empty($id) && is_numeric($id))
            $exists = $exists->whereNotIn('id', [$id]);
        $exists = $exists->exists();
        return response()->json(['exists' => $exists], 200);
    }

    public function emailCheck(Request $request)
    {
        $email = $request->email;
        $id = $request->respondent_id;
        $exists = Respondent::where('email', $email);
        if (!empty($id) && is_numeric($id))
            $exists = $exists->whereNotIn('id', [$id]);
        $exists = $exists->exists();
        return response()->json(['exists' => $exists], 200);
    }

    public function mobileCheck(Request $request)
    {
        $mobile_phone = $request->mobile_phone;
        $id = $request->respondent_id;
        $exists = RespondentDetail::where('mobile_phone', $mobile_phone);
        if (!empty($id) && is_numeric($id))
            $exists = $exists->whereNotIn('respondent_id', [$id]);
        $exists = $exists->exists();
        return response()->json(['exists' => $exists], 200);
    }

    public function inlineUpdate(Request $request)
    {
        $respondent = Respondent::find($request->id);
        if ($respondent) {
            $args = [];
            if ($request->has('first_name')) $args['first_name'] = $request->first_name;
            if ($request->has('last_name')) $args['last_name'] = $request->last_name;
            if ($request->has('email')) $args['email'] = $request->email;

            $respondent->update($args);

            $args = [];
            if ($request->has('home_postcode')) $args['home_postcode'] = $request->home_postcode;
            if ($request->has('work_postcode')) $args['work_postcode'] = $request->work_postcode;
            if ($request->has('home_phone')) $args['home_phone'] = $request->home_phone;
            if ($request->has('work_phone')) $args['work_phone'] = $request->work_phone;
            if ($request->has('mobile_phone')) $args['mobile_phone'] = $request->mobile_phone;
            if ($request->has('mobile_phone2')) $args['mobile_phone2'] = $request->mobile_phone2;
            if ($request->has('state')) $args['state'] = $request->state;
            if ($request->has('closest_large_city')) $args['closest_large_city'] = $request->closest_large_city;

            $detail = RespondentDetail::where('respondent_id', $request->id)->first();
            if ($detail && count($args)) {
                $detail->update($args);
            }

            $args = [];
            if ($request->has('occupation')) $args['occupation_id'] = $request->occupation;
            if ($request->has('partner_occupation')) $args['partner_occupation_id'] = $request->partner_occupation;
            if ($request->has('profession')) $args['profession'] = $request->profession;
            if ($request->has('job_title')) $args['job_title'] = $request->job_title;
            if ($request->has('highest_education')) $args['highest_education'] = $request->highest_education;

            $occupation = RespondentOccupation::where('respondent_id', $request->id)->first();
            if ($occupation && count($args)) {
                $occupation->update($args);
            }

            $args = [];
            if ($request->has('birth_country')) $args['birth_country'] = $request->birth_country;
            if ($request->has('ethnic_background')) $args['ethnic_background'] = $request->ethnic_background;
            if ($request->has('income_level')) $args['income_level'] = $request->income_level;

            $household = RespondentHousehold::where('respondent_id', $request->id)->first();
            if ($household && count($args)) {
                $household->update($args);
            }

            $args = [];
            if ($request->has('cigarette_brands')) $args['cigarette_brands'] = $request->cigarette_brands;

            $health = RespondentHealth::where('respondent_id', $request->id)->first();
            if ($health && count($args)) {
                $health->update($args);
            }

            $args = [];
            if ($request->has('mobile1_type')) $args['mobile1_type'] = $request->mobile1_type;

            $service = RespondentService::where('respondent_id', $request->id)->first();
            if ($service && count($args)) {
                $service->update($args);
            }

            $args = [];
            if ($request->has('alcohol_consumed')) $args['alcohol_consumed'] = $request->alcohol_consumed;

            $product = RespondentProduct::where('respondent_id', $request->id)->first();
            if ($product && count($args)) {
                $product->update($args);
            }

            $args = [];
            if ($request->has('additional_notes')) $args['additional_notes'] = $request->additional_notes;

            $summaries = RespondentSummary::where('respondent_id', $request->id)->first();
            if ($summaries && count($args)) {
                $summaries->update($args);
            }
        }
    }
    public function flagUpdate($flag, Request $request)
    {
        $respondent_ids = JobEvent::whereIn('id', $request->ids)->pluck('respondent_id')->toArray();
        if (count($respondent_ids)) {
            foreach ($respondent_ids as $id) {
                $respondent = Respondent::find($id);
                if ($respondent) {
                    $respondent->update([$flag => 1]);
                }
            }
        }

        return response()->json(1);
    }
//Inactive Respondents
    public function listInactive()
    {
        return view('respondents.inactive');
    }

    public function DTInactiveData()
    {
        $respondents = Respondent::join('respondent_details', 'respondent_details.respondent_id', '=', 'respondents.id')
            ->join('respondent_households', 'respondent_households.respondent_id', '=', 'respondents.id')
            ->join('master_states', 'master_states.id', '=', 'respondent_details.state')
            ->where('respondents.isactive', 0)
            ->withTrashed()
            ->select(
                'respondents.id',
                'respondents.uuid',
                'respondents.first_name',
                'respondents.last_name',
                'respondents.email',
                'respondents.gender',
                'respondent_details.mobile_phone',
                'master_states.name as state',
                'respondent_households.ethnic_background',
                'respondents.created_by',
                'respondents.created_at'
            );
        return Datatables::of($respondents)
            ->addColumn('respondent_name', function ($respondent) {
                return '<a href="' . route('respondents.edit', $respondent->uuid) . '">
                        <strong><i class="fa fa-' . strtolower($respondent->gender) . ' mr-2"></i>' . $respondent->first_name . ' ' . $respondent->last_name . '</strong>
                    </a>';
            })
            ->addColumn('created_user_name', function ($respondent) {
                if ($respondent->createdBy != null) {
                    return '<a href="' . route('staffs.edit', $respondent->createdBy->uuid) . '">
                        ' . $respondent->createdBy->profile->first_name . ' ' . $respondent->createdBy->profile->last_name . '
                    </a>';
                } else {
                    return 'Self';
                }
            })
            ->editColumn('created_at', function ($respondent) {
                return $respondent->created_at->format('d-m-Y');
            })
            ->addColumn('actions', function ($respondent) {
                return '<td> 
                <a title="Activate" href="javascript:void(0);" class="activate_btn btn btn-sm btn-success" data-id="' . $respondent->id . '" data-name="' . $respondent->first_name . ' ' . $respondent->last_name  . '"><i class="icon-check"></i></a>
            </td>';
            })->rawColumns(['respondent_name', 'actions', 'created_user_name'])->make(true);
    }

    public function activate(Respondent $respondent)
    {
        $respondent->update(['isactive' => 1]);
        return response()->json(1);
    }

    //Deleted Respondents
    public function listDeleted()
    {
        return view('respondents.deleted');
    }

    public function DTDeleteData()
    {
        $respondents = Respondent::onlyTrashed()
            ->select(
                'id',
                'uuid',
                'first_name',
                'last_name',
                'gender',
                'email',
                'isactive',
                'delete_reson',
                'created_by',
                'deleted_at',
                'deleted_by'
            );

        return Datatables::of($respondents)
            ->addColumn('respondent_name', function ($respondent) {
                
                    return $respondent->first_name.' '.$respondent->last_name;
                
            })
            ->addColumn('deleted_user_name', function ($respondent) {
                if($respondent->deleted_by == 'Self'){
                    return 'Self';
                }
                else if ($respondent->deletedBy != null) {
                    return '<a href="' . route('staffs.edit', $respondent->deletedBy->uuid) . '">
                        ' . $respondent->deletedBy->profile->first_name . ' ' . $respondent->deletedBy->profile->last_name . '
                    </a>';
                } else if($respondent->delete_reson == 'Unsubscribed'){
                        return 'Self';
                } else {
                    return '-';
                }
            })
            ->editColumn('deleted_at', function ($respondent) {
                return $respondent->deleted_at;
            })
            
            ->rawColumns(['respondent_name', 'deleted_user_name'])->make(true);
    }

    //Duplicate Respondents

    public function duplicate()
    {
        return view('respondents.duplicate');
    }

    public function DTDuplicateData()
    {

       /*$respondents = Respondent::select('first_name','last_name','dob')
        ->groupBy('first_name','last_name','dob')
        ->havingRaw("count(*) > 1")
        ->get();*/
        $respondents = DB::select("SELECT  r1.id,r1.first_name,r1.last_name,r1.dob,r1.email,r1.uuid,r1.deleted_at,r1.isactive
            FROM respondents as r1
            INNER JOIN (
            SELECT first_name, last_name, dob, deleted_at, count(*)  as rows
            FROM respondents 
            GROUP BY first_name, last_name ,dob, deleted_at
            HAVING count(rows) > 1) as r
            WHERE r.first_name = r1.first_name and r.last_name = r1.last_name and r.dob = r1.dob and r1.deleted_at IS NULL and r1.isactive=1");

        return Datatables::of($respondents)->addColumn('actions', function ($respondent) {
                return '<td>
                <a href="' . route('respondents.edit', $respondent->uuid) . '"><i class="icon-pencil mr-3"></i></a>
                <a href="javascript:void(0);" class="delete_btn" data-id="' . $respondent->id . '" data-name="' . $respondent->first_name . ' ' . $respondent->last_name  . '"><i class="icon-trash"></i></a>
            </td>';
            })->rawColumns(['actions'])->make(true);
    }

    public function deletereason(Request $request){
        $deleted_by=auth()->id();

       $respondent= Respondent::find($request->id);
        $respondent->update(["delete_reson" => $request->reason]);
        $respondent->update(["deleted_by" => $deleted_by]);
        $respondent->delete();
        return response()->json(1);

    }

}
