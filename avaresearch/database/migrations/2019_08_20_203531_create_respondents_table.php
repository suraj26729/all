<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->date('dob')->nullable();
            $table->string('gender');
            $table->string('email');
            $table->string('additional_email')->nullable();
            $table->string('password');
            $table->boolean('do_not_call')->nullable();
            $table->boolean('one_off_resp')->nullable();
            $table->boolean('has_accent')->nullable();
            $table->boolean('do_no_show')->nullable();
            $table->boolean('stop_often_email')->nullable();
            $table->boolean('excellent_candidate')->nullable();
            $table->boolean('wrong_number')->nullable();
            $table->boolean('keen_candidate')->nullable();
            $table->boolean('isactive')->default(true);
            $table->text('delete_reson')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondents');
    }
}
