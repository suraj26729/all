<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterPostcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_postcodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('postcode');
            $table->integer('state_id');
            $table->string('suburb');
            $table->string('latitude');
            $table->string('longitude');
            $table->boolean('isactive')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_postcodes');
    }
}
