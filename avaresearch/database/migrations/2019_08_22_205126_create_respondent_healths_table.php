<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentHealthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_healths', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('respondent_id')->index();
            $table->text('health_problems')->nullable();
            $table->text('other_health_problem')->nullable();
            $table->text('dietary_requirements')->nullable();
            $table->text('health_funds')->nullable();
            $table->text('other_health_fund')->nullable();
            $table->text('cigarette_brands')->nullable();
            $table->text('partner_cigarette_brands')->nullable();
            $table->text('quite_smoking_methods')->nullable();
            $table->text('nicotine_replacement')->nullable();
            $table->integer('brands')->nullable();
            $table->text('orther_brands')->nullable();
            $table->integer('partner_brands')->nullable();
            $table->text('other_partner_brands')->nullable();
            $table->boolean('organ_donate')->nullable();
            $table->text('donated_organ')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondent_healths');
    }
}
