<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('respondent_id')->index();
            $table->text('bank_id')->nullable();
            $table->text('superannuation_id')->nullable();
            $table->text('non_super_investments')->nullable();
            $table->text('insurance_providers')->nullable();
            $table->text('insurance_policies')->nullable();
            $table->string('home_phone_provider')->nullable();
            $table->string('other_home_phone_provider')->nullable();
            $table->string('work_phone_provider')->nullable();
            $table->string('other_work_phone_provider')->nullable();
            $table->string('mobile1_provider')->nullable();
            $table->string('mobile2_provider')->nullable();
            $table->string('mobile1_provider_other')->nullable();
            $table->string('mobile2_provider_other')->nullable();
            $table->string('mobile1_type')->nullable();
            $table->string('mobile2_type')->nullable();
            $table->string('tv_providers')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondent_services');
    }
}
