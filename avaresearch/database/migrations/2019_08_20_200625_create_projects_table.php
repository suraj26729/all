<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable();
            $table->string('job_number');
            $table->string('job_name');
            $table->string('subject');
            $table->text('description');
            $table->integer('client_id');
            $table->string('location');
            $table->string('contact_phone');
            $table->string('contact_email');
            $table->string('venue_email');
            $table->string('validation_report')->nullable();
            $table->datetime('received_date');
            $table->datetime('start_date');
            $table->datetime('finish_date');
            $table->boolean('signed_sheets')->default(false);
            $table->text('searched_data')->nullable();
            $table->boolean('isactive')->default(true);
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
