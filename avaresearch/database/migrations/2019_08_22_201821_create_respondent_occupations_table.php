<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentOccupationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_occupations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('respondent_id')->index();
            $table->string('profession')->nullable();
            $table->integer('occupation_id')->nullable();
            $table->integer('partner_occupation_id')->nullable();
            $table->string('type')->nullable();
            $table->string('hours_per_week')->nullable();
            $table->string('job_title')->nullable();
            $table->string('partner_job_title')->nullable();
            $table->string('employer_name')->nullable();
            $table->string('partner_employer_name')->nullable();
            $table->integer('employer_size_id')->nullable();
            $table->integer('partner_employer_size_id')->nullable();
            $table->string('employer_turnover')->nullable();
            $table->string('partner_employer_turnover')->nullable();
            $table->integer('industry_id')->nullable();
            $table->integer('partner_industry')->nullable();
            $table->string('highest_education')->nullable();
            $table->string('education_institute_type')->nullable();
            $table->string('education_institute_suburb')->nullable();
            $table->string('education_year')->nullable();
            $table->string('education_study_field')->nullable();
            $table->string('partner_education_study_field')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_postcode')->nullable();
            $table->text('company_website')->nullable();
            $table->text('company_email')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondent_occupations');
    }
}
