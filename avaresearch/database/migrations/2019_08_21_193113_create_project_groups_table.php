<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable();
            $table->integer('job_id');
            $table->string('name');
            $table->text('location');
            $table->text('venue_address');
            $table->string('venue_phone');
            $table->string('client_phone')->nullable();
            $table->string('research_phone')->nullable();
            $table->string('client_email')->nullable();
            $table->string('research_email')->nullable();
            $table->string('incentive')->nullable();
            $table->string('other')->nullable();
            $table->datetime('group_date');
            $table->time('group_time')->nullable();
            $table->text('different_time')->nullable();
            $table->string('duration')->nullable();
            $table->boolean('isactive')->default(true);
            $table->integer('created_by')->nullable();
            $table->integer('modified_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_groups');
    }
}
