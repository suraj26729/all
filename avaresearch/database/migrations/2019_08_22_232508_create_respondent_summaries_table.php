<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_summaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('respondent_id')->index();
            $table->text('preferred_research_location')->nullable();
            $table->text('availability_times')->nullable();
            $table->text('session_type')->nullable();
            $table->text('preferred_contact_method')->nullable();
            $table->text('additional_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondent_summaries');
    }
}
