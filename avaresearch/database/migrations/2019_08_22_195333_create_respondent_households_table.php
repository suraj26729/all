<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentHouseholdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_households', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('respondent_id')->index();
            $table->string('marital_status')->nullable();
            $table->string('home_ownership')->nullable();
            $table->string('household_type')->nullable();
            $table->string('income_level')->nullable();
            $table->string('residence_type')->nullable();
            $table->string('residency_status')->nullable();
            $table->integer('birth_country')->nullable();
            $table->string('other_country')->nullable();
            $table->integer('original_country')->nullable();
            $table->string('ethnic_background')->nullable();
            $table->text('pets')->nullable();
            $table->boolean('children')->nullable();
            $table->boolean('children_allowed')->nullable();
            $table->boolean('children_seperate')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondent_households');
    }
}
