<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('respondent_id')->index();
            $table->integer('state')->nullable();
            $table->string('closest_large_city')->nullable();
            $table->string('home_postcode')->nullable();
            $table->string('home_postsuburb')->nullable();
            $table->string('home_subdirection')->nullable();
            $table->string('work_postcode')->nullable();
            $table->string('work_postsuburb')->nullable();
            $table->string('work_subdirection')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('mobile_phone')->nullable();
            $table->string('mobile_phone2')->nullable();
            $table->string('hear_about')->nullable();
            $table->text('source_name')->nullable();
            $table->text('source_email')->nullable();
            $table->boolean('online_survey');
            $table->boolean('articulation')->nullable();
            $table->integer('last_modified_by')->nullable();
            $table->text('unsub_reason')->nullable();
            $table->datetime('unsub_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondent_details');
    }
}
