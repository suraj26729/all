<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespondentProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('respondent_id')->index();
            $table->integer('vehicle_make_id')->nullable();
            $table->integer('vehicle2_make_id')->nullable();
            $table->integer('vehicle_type_id')->nullable();
            $table->integer('other_vehicle_type')->nullable();
            $table->integer('vehicle2_type_id')->nullable();
            $table->integer('other_vehicle2_type')->nullable();
            $table->string('vehicle_model')->nullable();
            $table->string('vehicle2_model')->nullable();
            $table->string('vehicle_year')->nullable();
            $table->string('vehicle2_year')->nullable();
            $table->string('travel_location')->nullable();
            $table->string('travel_reason')->nullable();
            $table->string('travel_frequency')->nullable();
            $table->string('rewards_progammes')->nullable();
            $table->string('technology_use')->nullable();
            $table->string('general_products')->nullable();
            $table->integer('internet_provider')->nullable();
            $table->integer('alcohol_consumed')->nullable();
            $table->string('optical')->nullable();
            $table->string('lens_brand')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respondent_products');
    }
}
