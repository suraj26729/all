const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
const env = require("dotenv");
const passport = require("passport");
const fs = require("fs");
const path = require("path");

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "access.log"),
  { flags: "a" }
);

env.config();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  morgan("combined", {
    skip: function(req, res) {
      return res.statusCode < 400;
    },
    stream: accessLogStream
  })
);

const prefix = "/mytest";

app.use(prefix + "/auth", require("./scr/routes/auth"));
app.use(prefix, require("./scr/routes/product"));
app.use(prefix, require("./scr/routes/rough"));
app.use(prefix, require("./scr/routes/order"));

app.use((req, res, next) => {
  const error = new Error("404 Not Found!");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  const status = error.status || 500;
  return res.status(status).json({
    status: status,
    message: error.message
  });
});

var port = process.env.PORT || 3000;
app.listen(port);

console.log("API is running at http://localhost:" + port);
