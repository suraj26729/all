const nodemailer = require("nodemailer");
exports.send = async (to, subject, message, next) => {
  try {
    var transport = nodemailer.createTransport({
      host: "smtp.mailtrap.io",
      port: 2525,
      auth: {
        user: "c6ebd410d299d5",
        pass: "e0af639c4fa85e"
      }
    });

    const mailOptions = {
      from: process.env.ADMIN_MAIL,
      to: to,
      subject: subject,
      generateTextFromHTML: true,
      html: message
    };

    let result = await transport.sendMail(mailOptions);
    // console.log(result);
    transport.close();
    return result;
  } catch (error) {
    next(error);
  }
};
