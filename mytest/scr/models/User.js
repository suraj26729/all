const Sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const User = db.define(
  "users",
  {
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    name: Sequelize.STRING,

    phone: { type: Sequelize.STRING, allowNull: true },
    last_login: { type: Sequelize.DATE, allowNull: true },
    remember_token: { type: Sequelize.STRING, allowNull: true },
    is_status: { type: Sequelize.INTEGER, defaultValue: 0 }
  },
  {
    paranoid: true,
    underscored: true
  }
);

User.sync();

//Validation Schema
const ValSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
  password: Joi.string()
    .pattern(
      new RegExp(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/
      )
    )
    .required(),

  name: Joi.string().required(),
  last_login: Joi.date().allow(null),
  is_status: Joi.number().required(),
  remember_token: Joi.string().allow(null),
  phone: Joi.number().required()
});

const LoginSchema = Joi.object({
  email: Joi.string(),
  password: Joi.string(),
  name: Joi.string()
});

module.exports = { User, ValSchema, LoginSchema };
