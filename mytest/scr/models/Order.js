const Sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const Order = db.define(
  "orders",
  {
    user_id: Sequelize.STRING,
    sub_total: Sequelize.STRING,
    grand_total: Sequelize.STRING,
    product_data: Sequelize.STRING
  },
  {
    paranoid: true,
    underscored: true
  }
);

Order.sync();

//Validation Schema
const ValSchema = Joi.object({
  user_id: Joi.string().required(),
  product_data: Joi.string().required(),
  quantity: Joi.string().required(),
  product_id: Joi.string().required()
});

module.exports = { Order, ValSchema };
