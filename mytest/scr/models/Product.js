const Sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const Product = db.define(
  "products",
  {
    name: Sequelize.STRING,
    sku: Sequelize.STRING,
    price: Sequelize.STRING,
    stack: Sequelize.STRING,
    is_status: { type: Sequelize.INTEGER, defaultValue: 1 }
  },
  {
    paranoid: true,
    underscored: true
  }
);

Product.sync();

//Validation Schema
const VallSchema = Joi.object({
  name: Joi.string().required(),
  sku: Joi.string().required(),
  is_status: Joi.number(),
  price: Joi.string().required(),
  stack: Joi.number().required()
});

module.exports = { Product, VallSchema };
