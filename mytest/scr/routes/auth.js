const router = require("express").Router();
const { User, ValSchema, LoginSchema } = require("../models/User");
const { validationError } = require("../helper/common");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Joi = require("@hapi/joi");
const mail = require("../config/mail");
var uuid = require("uuid");
const Sequelize = require("sequelize");

router.post(
  "/login",

  async (req, res, next) => {
    try {
      const validateResult = LoginSchema.validate(req.body, {
        abortEarly: false
      });

      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));
      let usernameOrEmail = req.body.email;
      let password = req.body.password;

      let user = await User.findOne({
        where: {
          [Sequelize.Op.or]: {
            name: usernameOrEmail,
            email: usernameOrEmail
          },
          is_status: 1
        }
      });
      if (user) {
        let result = await bcrypt.compare(password, user.password);
        if (result) {
          const token = jwt.sign(
            { name: usernameOrEmail, password: password },
          env.jwtSecret,
            { expiresIn: "36500d" }
          );
          user.update({ last_login: Date.now() });

          res.status(201).json({
            success: 1,
            data: {
              access_token: "Bearer " + token,
              user: user
            },
            message: "Login Success",
            user_id: user.id
          });
        } else {
          res.status(404).json({
            success: 0,
            error: 404,
            data: {},
            message: "Username or Password is not found in our records"
          });
        }
      } else {
        res.status(404).json({
          success: 0,
          error: 404,
          data: {},
          message: "Username or Password is not found in our records"
        });
      }
    } catch (err) {
      next(err);
    }
  }
);
// router.post("/login", async (req, res, next) => {
//   try {
//     const validateResult = LoginSchema.validate(req.body, {
//       abortEarly: false
//     });

//     if (validateResult.error)
//       return res.status(422).json(validationError(validateResult));

//     let email = await User.findOne({ where: { email: req.body.email } });
//     let name = await User.findOne({ where: { name: req.body.email } });
//     if (name == null && email == null)
//       return res
//         .status(400)
//         .json("Email or Password not match withghgf our records");

//     if (email.is_status == 0 && name.is_status == 0)
//       return res.status(400).json("user expire");

//     if (
//       bcrypt.compareSync(req.body.password, name.password) == null &&
//       bcrypt.compareSync(req.body.password, email.password) == null
//     )
//       return res
//         .status(400)
//         .json("Email or Password not match with22 oudssdr records");

//     const token = jwt.sign(
//       {
//         iss: "AED PLATFORM",
//         iat: new Date().getTime(),
//         sub: user
//       },
//       process.env.JWT_SECRET,
//       { expiresIn: "7d" }
//     );

//     return res.status(200).json({
//       token: token,
//       token_type: "Bearer",
//       role_id: user.role_id,
//       user: user
//     });
//   } catch (error) {
//     console.log(error);
//     next(error);
//   }
// });

router.patch("/token", async (req, res, next) => {
  try {
    email = req.body.email;
    token = req.body.token;
    let update_user = await User.update(
      {
        is_status: 1
      },
      { where: { email: email, remember_token: token } }
    );

    return res.status(200).json(update_user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/register", async (req, res, next) => {
  try {
    const validateResult = ValSchema.validate(req.body, {
      abortEarly: false
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let exists = await User.findOne({ where: { email: req.body.email } });
    if (exists != null)
      return res.status(400).json("Email is already registered");

    let uname = await User.findOne({ where: { name: req.body.name } });
    if (uname != null) return res.status(400).json("user name already exits");

    req.body.password = bcrypt.hashSync(req.body.password, 10);
    const token = uuid.v1();
    req.body.remember_token = token;
    let sentMail = await mail.send(
      req.body.email,
      "Activate your account",
      token,
      next
    );

    let new_user = await User.create(req.body);

    return res.status(201).json(new_user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/all_user", async (req, res, next) => {
  try {
    let all_user = await User.findAll();
    return res.status(201).json(all_user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
