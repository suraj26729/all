const router = require("express").Router();
const { Product, VallSchema } = require("../models/Product");
const { validationError } = require("../helper/common");
const excel = require('node-excel-export');
const fs= require("fs");
router.post("/product", async (req, res, next) => {
  try {
    
    let new_product = await Product.create(req.body);

    return res.status(201).json(new_product);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/all_products", async (req, res, next) => {
  try {
const styles = {
  headerDark: {
    fill: {
      fgColor: {
        rgb: 'FF000000'
      }
    },
    font: {
      color: {
        rgb: 'FFFFFFFF'
      },
      sz: 14,
      bold: true,
      underline: true
    }
  },
  cellPink: {
    fill: {
      fgColor: {
        rgb: 'FFFFCCFF'
      }
    }
  },
  cellGreen: {
    fill: {
      fgColor: {
        rgb: 'FF00FF00'
      }
    }
  }
};
 
//Array of objects representing heading rows (very top)

 
//Here you specify the export structure
const specification = {
  id: { // <- the key should match the actual data key
    displayName: 'id', // <- Here you specify the column header
    headerStyle: styles.headerDark, // <- Header style
    // <- Inline cell style is possible 
  
    width: 120 // <- width in pixels
  },
  name: {
    displayName: 'Name',
    headerStyle: styles.headerDark,
   
    
    width: 120 // <- width in chars (when the number is passed as string)
  },
  sku: {
    displayName: 'Sku',
    headerStyle: styles.headerDark, // <- Cell style
    width: 120 // <- width in pixels
  },
   price: {
    displayName: 'Price',
    headerStyle: styles.headerDark,
// <- Cell style
    width: 120 // <- width in pixels
  },
  stack: {
    displayName: 'Stack',
    headerStyle: styles.headerDark,
  // <- Cell style
    width: 120 // <- width in pixels
  },
  is_status: {
    displayName: 'is_status',
    headerStyle: styles.headerDark,
   // <- Cell style
    width: 120// <- width in pixels
    
  }
}

const dataset = await Product.findAll();


const report = excel.buildExport(
  [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
    {
      name: 'Report', // <- Specify sheet name (optional)
     // <- Raw heading array (optional)
       // <- Merge cell ranges
      specification: specification, // <- Report specification
      data:dataset // <-- Report data
    }
  ]
);
 
// You can then return this straight
res.attachment('reportsss.xlsx'); // This is sails.js specific (in general you need to set headers)
fs.writeFileSync("reporter.xlsx", report)
return res.send(report);
 
// OR you can save this buffer to the disk by creating a file.
 

  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
