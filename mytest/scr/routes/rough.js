const router = require("express").Router();
const { Product, VallSchema } = require("../models/Product");
const { validationError } = require("../helper/common");
const excel = require('node-excel-export');
router.post("/product", async (req, res, next) => {
  try {
    
    let new_product = await Product.create(req.body);

    return res.status(201).json(new_product);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/all_products", async (req, res, next) => {
  try {
const styles = {
  headerDark: {
    fill: {
      fgColor: {
        rgb: 'FF000000'
      }
    },
    font: {
      color: {
        rgb: 'FFFFFFFF'
      },
      sz: 14,
      bold: true,
      underline: true
    }
  },
  cellPink: {
    fill: {
      fgColor: {
        rgb: 'FFFFCCFF'
      }
    }
  },
  cellGreen: {
    fill: {
      fgColor: {
        rgb: 'FF00FF00'
      }
    }
  }
};
 
//Array of objects representing heading rows (very top)

 
//Here you specify the export structure
const specification = {
  id: { // <- the key should match the actual data key
    displayName: 'id', // <- Here you specify the column header
    headerStyle: styles.headerDark, // <- Header style
    // <- Inline cell style is possible 
  
    width: 120 // <- width in pixels
  },
  name: {
    displayName: 'Name',
    headerStyle: styles.headerDark,
   
    
    width: 120 // <- width in chars (when the number is passed as string)
  },
  sku: {
    displayName: 'Sku',
    headerStyle: styles.headerDark, // <- Cell style
    width: 120 // <- width in pixels
  },
   price: {
    displayName: 'Price',
    headerStyle: styles.headerDark,
// <- Cell style
    width: 120 // <- width in pixels
  },
  stack: {
    displayName: 'Stack',
    headerStyle: styles.headerDark,
  // <- Cell style
    width: 120 // <- width in pixels
  },
  is_status: {
    displayName: 'is_status',
    headerStyle: styles.headerDark,
   // <- Cell style
    width: 120// <- width in pixels
    
  }
}
 
// The data set should have the following shape (Array of Objects)
// The order of the keys is irrelevant, it is also irrelevant if the
// dataset contains more fields as the report is build based on the
// specification provided above. But you should have all the fields
// that are listed in the report specification
const dataset = await Product.findAll();

// Define an array of merges. 1-1 = A:1
// The merges are independent of the data.
// A merge will overwrite all data _not_ in the top-left cell.
// const merges = [
//   { start: { row: 1, column: 1 }, end: { row: 1, column: 6 } },
//   { start: { row: 2, column: 1  }, end: { row: 2, column: 6 }},
//   { start: { row: 3, column: 1 }, end: { row: 3, column: 6 } }
// ]
 
// Create the excel report.
// This function will return Buffer
const report = excel.buildExport(
  [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
    {
      name: 'Report', // <- Specify sheet name (optional)
     // <- Raw heading array (optional)
       // <- Merge cell ranges
      specification: specification, // <- Report specification
      data:dataset // <-- Report data
    }
  ]
);
 
// You can then return this straight
res.attachment('reportsss.xlsx'); // This is sails.js specific (in general you need to set headers)
return res.send(report);
 
// OR you can save this buffer to the disk by creating a file.
 

  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
