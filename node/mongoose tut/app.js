const express=require('express');
const app=express();
const morgan=require('morgan');
const bodyParser= require('body-parser')
const mongoose= require('mongoose');


const productRoutes= require('./api/routes/products');
const orderRoutes= require('./api/routes/orders');
const cat= require('./api/routes/cat');
const userRoutes= require('./api/routes/user');
const postRoutes= require('./api/routes/post');


app.use(morgan('dev'));
app.use(express.static('uploads'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.use('/products/cat', cat);
app.use('/products', productRoutes);
app.use('/orders', orderRoutes);
app.use('/user', userRoutes);
app.use('/post', postRoutes);
mongoose.connect('mongodb+srv://admin:admin@cluster0-mzd7o.mongodb.net/test?retryWrites=true&w=majority',
	{
		useMongoClient: true
	});

app.use((req, res, next)=>{
	const error=new Error('Not found');
	error.status(404);
	next(error);
})

app.use((error, req, res, next)=>{
	res.status(error.status || 500);
	res.json({
		error:{
			message: error.message
		}
	});
});
module.exports=app;
