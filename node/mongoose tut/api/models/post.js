// 103.219.207.246
const mongoose= require('mongoose');

const postSchema= mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	userid: { type:mongoose.Schema.Types.ObjectId, required: true},
	title: { type: String, required: true},
	discription: { type: String, required: true},
	productImage: { type: String, required: true}

});

module.exports = mongoose.model('Post',postSchema);
