const express = require('express');
const router = express.Router();



router.get('/', (req, res, next) => {

	res.status(200).json({
		message: 'getting all request in cat'

	});
});

router.post('/', (req, res, next) => {
	res.status(200).json({
		message: 'creating in cat'

	});
});
router.get('/id/:catId', (req, res, next) => {
	const id = req.params.catId;

	res.status(200).json({
		message: 'getting single id in cat',
		id: id
	});
});
// router.get('/id/:catId',(req, res, next)=>{
// 	const id=req.params.catId;
// 	if(id=='special'){
// 		res.status(200).json({
// 			message: 'getting single id in cat',
// 			id: id
// 		});
// 	}else{
// 		res.status(200).json({
// 			message: 'you passed an Id'
// 		});
// 	}
// });
router.patch('/id/:catId', (req, res, next) => {
	const id = req.params.catId;
	res.status(200).json({

		message: 'update few products in cat',
		id: id
	});
});
router.delete('/id/:catId', (req, res, next) => {
	const id = req.params.catId;
	res.status(200).json({
		message: 'deleted products in cat',
		id: id
	});
});
router.put('/', (req, res, next) => {

	res.status(200).json({

		message: 'update all products',

	});
});
module.exports = router;