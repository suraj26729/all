const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const checkAuth = require('../middleware/check-auth');


const storage = multer.diskStorage({

	destination: function (req, file, cb) {
		cb(null, './uploads/');
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});


const fileFilter = (req, file, cb) => {
	if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
		cb(null, true);
	}
	else {
		cb(null, false);
	}
};
const upload = multer({
	storage: storage, limits: {
		fileSize: 1024 * 1024 * 5
	},
	fileFilter: fileFilter
});

const Product = require('../models/product');

router.get('/', (req, res, next) => {
	Product.find()
		.select('name price _id productImage')
		.exec()
		.then(docs => {
			const response = {
				count: docs.length,
				product: docs.map(doc => {
					return {
						name: doc.name,
						price: doc.price,
						productImage: doc.productImage,
						_id: doc.id,
						request: {
							type: 'GET',
							url: 'http://localhost:3000/products/' + doc._id
						}
					}
				})
			};

			// if(docs.length>=0)
			// {
			res.status(200).json(response);
			// }else{
			// 	res.status(404).json({
			// 		message: 'no entries found'
			// 	})
			// }

		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});
});
router.post('/', upload.single('productImage'), (req, res, next) => {

	const product = new Product({
		_id: new mongoose.Types.ObjectId(),
		name: req.body.name,
		price: req.body.price,
		productImage: req.file.path
	});
	// (or)
	// // const produc t= {
	// // 	name: req.body.name,
	// // 	price: req.body.price

	// };
	product
		.save()
		.then(result => {
			console.log(result);
			res.status(201).json({
				message: 'Created product successfully',
				createdProduct: {
					name: result.name,
					price: result.price,
					_id: result._id,
					request: {
						type: 'GET',
						url: 'http://localhost:3000/products/' + result._id
					}

				}


			});
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});

});
router.get('/:productId', (req, res, next) => {
	const id = req.params.productId;
	Product.findById(id)
		.select('name price _id productImage')
		.exec()
		.then(doc => {
			console.log(doc);
			if (doc) {
				res.status(200).json({
					product: doc,
					request: {
						type: 'GET',
						url: 'http://localhost:3000/products'
					}
				});
			} else {
				res.status(404).json({ message: 'No valid entry found in provided id' });
			}

		})
		.catch(err => {
			console.log(err);
			res.status(500).json({ error: err });
		});

});


router.patch('/id/:productId', checkAuth, (req, res, next) => {
	const id = req.params.productId;
	// const updateOps = {};
	// for (const ops of req.body){
	// 	updateOps[ops.propName] =ops.value;
	// }
	// return res.status(200).json(updateOps);
	//(or) 
	Product.update({ _id: id }, { $set: req.body })
		.exec()
		.then(result => {
			res.status(200).json({
				message: 'Product update',
				request: {
					type: 'GET',
					url: 'http://localhost:3000/products/' + id


				}
			});

		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});

});


// 	const product= {
// 		productId: req.body.productId,
// 		price: req.body.price
// 	};
// 	res.status(200).json({
// 			message: 'update products in products',
// 			productdDetail: req.body
// 		});
// });
router.delete('/:productId', checkAuth, (req, res, next) => {
	const id = req.params.productId;
	Product.remove({ _id: id })
		.exec()
		.then(result => {
			res.status(200).json({
				message: 'Product deleted',
				request: {
					type: 'POST',
					url: 'http://localhost:3000/products',
					data: { name: 'String', price: 'Number' }
				}
			});
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});
});
module.exports = router;