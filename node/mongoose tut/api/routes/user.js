const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken')

const User = require("../models/user");

router.post("/signup", (req, res, next) => {

	User.find({ email: req.body.email })
		.exec()
		.then(user => {
			if (user.length >= 1) {
				return res.status(409).json({
					message: 'mail exits'
				});

			}
			else {
				bcrypt.hash(req.body.password, 10, (err, hash) => {
					if (err) {
						return

						next(err);
					}
					else {
						const user = new User({
							_id: new mongoose.Types.ObjectId(),
							email: req.body.email,
							password: hash,
							firstname: req.body.firstname,
							lastname: req.body.lastname,
							dob: req.body.dob,
							status: req.body.status



						});
						user
							.save()
							.then(result => {
								console.log(result);
								res.status(201).json(result)
							})
							.catch(err => {
								console.log(err);
								res.status(500).json({
									error: err
								});
							});
					}
				});
			}
		});

});

router.post("/login", (req, res, next) => {
	User.find({ email: req.body.email })
		.exec()
		.then(user => {
			if (user[0].status >= 2) {
				return res.status(400).json({
					message: 'token expire'
				});
			}
			else {
				if (user.length < 1) {
					return res.status(401).json({
						message: 'Auth failed'
					});
				}
				bcrypt.compare(req.body.password, user[0].password, (err, result) => {
					if (err) {
						return res.status(401).json({
							message: 'Auth failed'
						});
					}
					if (result) {
						const token = jwt.sign({
							email: user[0].email,
							userId: user[0]._id
						}, "hyhyyhtryt", { expiresIn: "1h" });


						return res.status(200).json({
							message: 'Auth successfull',
							token: token,
							user: user
						});
					}
					res.status(401).json({
						message: 'Auth failed'
					});
				});
			}
		})

		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});
});

router.get('/:productId', (req, res, next) => {
	const id = req.params.productId;
	User.findById(id)
		.select('firstname lastname _id password email dob status')
		.exec()
		.then(doc => {
			console.log(doc);
			if (doc) {
				res.status(200).json({
					product: doc,
				});
			} else {
				res.status(404).json({ message: 'No valid entry found in provided id' });
			}

		})
		.catch(err => {
			console.log(err);
			res.status(500).json({ error: err });
		});

});

router.get('/', (req, res, next) => {
	User.find()
		.select('firstname lastname _id password email dob status createdAt updatedAt')
		.exec()
		.then(docs => {
			const response = {
				count: docs.length,
				product: docs.map(doc => {
					return {
						_id: doc.id,
						email: doc.email,
						password: doc.password,
						firstname: doc.firstname,
						lastname: doc.lastname,
						dob: doc.dob,
						status: doc.status

					}
				})
			};

			// if(docs.length>=0)
			// {
			res.status(200).json(response);
			// }else{
			// 	res.status(404).json({
			// 		message: 'no entries found'
			// 	})
			// }

		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});
});
router.patch('/status/:productId', (req, res, next) => {
	const id = req.params.productId;
	// const updateOps = {};
	// for (const ops of req.body){
	// 	updateOps[ops.propName] =ops.value;
	// }
	// return res.status(200).json(updateOps);
	//(or) 
	User.update({ _id: id }, { $set: req.body })
		.exec()
		.then(result => {
			res.status(200).json({
				message: 'Product update',
				request: {
					type: 'GET',
					url: 'http://localhost:3000/products/' + id


				}
			});

		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});

});
router.delete("/:userId", (req, res, next) => {
	User.remove({ _id: req.params.userId })
		.exec()
		.then(result => {
			res.status(200).json({
				message: "User deleted"
			});
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});
});
module.exports = router;