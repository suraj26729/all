const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const storage = multer.diskStorage({

	destination: function (req, file, cb) {
		cb(null, './uploads/');
	},
	filename: function (req, file, cb) {
		cb(null, Date.now() + "-" + file.originalname);
	}
});


const fileFilter = (req, file, cb) => {
	if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
		cb(null, true);
	}
	else {
		cb(null, false);
	}
};
const upload = multer({
	storage: storage, limits: {
		fileSize: 1024 * 1024 * 5
	},
	fileFilter: fileFilter
});

const Post = require('../models/post');

router.get('/', (req, res, next) => {
	Post.find()
		.select('title discription productImage userid  ')
		.exec()
		.then(docs => {
			const response = {
				count: docs.length,
				post: docs.map(doc => {
					return {
						title: doc.title,
						discription: doc.discription,
						productImage: doc.productImage,
						userid: doc.userid,
						_id: doc._id

					}
				})
			};
			res.status(200).json(response);
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});
});
router.post('/', upload.single('productImage'), (req, res, next) => {

	const post = new Post({
		_id: new mongoose.Types.ObjectId(),
		title: req.body.title,
		discription: req.body.discription,
		productImage: req.file.path,
		userid: req.body.userid
	});
	post
		.save()
		.then(result => {
			console.log(result);
			res.status(201).json({
				message: 'Created product successfully',
				createdProduct: {
					title: result.title,
					discription: result.discription,
					_id: result._id,
					userid: result.userid,
					productImage: result.productImage


				}


			});
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});

});
router.get('/user/:userid', (req, res, next) => {
	const id = req.params.userid;
	Product.findById(id)
		.select('title discription productImage userid')
		.exec()
		.then(doc => {
			console.log(doc);
			if (doc) {
				res.status(200).json({
					post: doc,

				});
			} else {
				res.status(404).json({ message: 'No valid entry found in provided id' });
			}

		})
		.catch(err => {
			console.log(err);
			res.status(500).json({ error: err });
		});

});


router.patch('/user/:userid', (req, res, next) => {
	const id = req.params.userid;
	Product.update({ _id: id }, { $set: req.body })
		.exec()
		.then(result => {
			res.status(200).json({
				message: 'Product update',

			});

		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});

});


router.delete('/user/:userid', (req, res, next) => {
	const id = req.params.userid;
	Product.remove({ _id: id })
		.exec()
		.then(result => {
			res.status(200).json({
				message: 'Product deleted',

			});
		})
		.catch(err => {
			console.log(err);
			res.status(500).json({
				error: err
			});
		});
});
module.exports = router;