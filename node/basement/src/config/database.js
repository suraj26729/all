const Sequelize = require("sequelize");

const sequelize = new Sequelize(
  process.env.DATABASE_NAME,
  process.env.DATABASE_USER,
  process.env.DATABASE_PWD,
  {
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT,
    dialect: "mysql",
    logging: false,
    dialectOptions: {
      //useUTC: false, //for reading from database
      dateStrings: true,
      typeCast: true
    }
    //timezone: "+05:30"
  }
);

sequelize.authenticate().catch(function(err) {
  if (err) {
    console.log("There is connection ERROR 3");
  }
});

module.exports = sequelize;
