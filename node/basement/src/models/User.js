const Sequelize = require("sequelize");
const db = require("../config/database");

const User = db.define(
  "users",
  {
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    password_reset_token: { type: Sequelize.TEXT, allowNull: true },
    firstname: Sequelize.STRING,
    lastname: Sequelize.STRING,
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
    role_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

User.sync();

module.exports = { User };
