//Node.js ==> Express Framework (SIMPLE SERVER)
const express();
let app = express();
//port to listen on
const PORT =3000;

const path = require("path");

const bodyParser = require("body-parser");

//use a custome templating engie
app.set("view engie", "pug");

app.set("view", path.resolve("./src/views"));

//request parsing
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
//create express route
const route = express.Route();
app.use(route);

const rootpath = path.resolve("./dist");
app.use(express.static(rootPath));

//db connection
require("./src/database/connection");

route.use((err, req, res, next)=> {
	if(err){
		//handle file type and max size of image
		return res.send(err.message);

	}
});

app.listen(PORT, err=>{
	if(err) return console.log(`Cannot listen on PORT: ${PORT}`);
	console.log(`server is listentiing on: http://localhost: ${PORT}/`);

});