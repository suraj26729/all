const express = require("express");
const router = express.Router();
const mail = require("../config/mail");
const fss = require("fs");

router.post("/test/mail", async (req, res, next) => {
  try {
    res.writeHead(200, {"Content-Type": "text/html"});
    fss.readFile("../config/indeex.html",null, function(error, data){
    if (error) {
      res.writeHead(404);
      res.write("file not found");
    }
    else{ 
       let sentMail =  mail.send(
      req.body.email,
      "Activate your account - Naartjie Legal",
      res.write(data),
      next
    );
    console.log(sentMail);
    res.status(201).json({
      success: 1,
      message: "User Created",

      userData: sentMail
    });}
    })
  
  } catch (err) {
    return res.status(500).json(err);
  }
});
module.exports = router;
