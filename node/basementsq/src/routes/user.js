const router = require("express").Router();
const { User } = require("../models/User");
const { validationError } = require("../helpers/common");
const bcrypt = require("bcryptjs");
const Joi = require("@hapi/joi");

router.patch("/:id/update", async (req, res, next) => {
  try {
    const id = req.params.id;
    let user = await User.findOne({ where: { id: req.params.id } });
    const upSchema = Joi.object({
      email: Joi.string()
        .email({ minDomainSegments: 2 })
        .allow(null),
      password: Joi.string()
        .pattern(
          new RegExp(
            /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/
          )
        )
        .allow(null),
      firstname: Joi.string().allow(null),
      lastname: Joi.string().allow(null),
      gender: Joi.string().allow(null),
      status: Joi.number().allow(null),
      phone: Joi.allow(null),
      organization: Joi.allow(null),
      street: Joi.allow(null),
      home_phone: Joi.allow(null),
      addition_phone: Joi.allow(null),
      zip: Joi.allow(null),
      city: Joi.allow(null),
      country: Joi.allow(null),
      status: Joi.allow(null),
      role_id: Joi.number().allow(null)
    });

    const validateResult = upSchema.validate(req.body, {
      abortEarly: false
    });

    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    if (user == null) return res.status(400).json("No User Found");

    if (req.body.email != user.email) {
      let exists = await User.findOne({ where: { email: req.body.email } });
      if (exists != null) {
        return res.status(400).json("Email is already registered");
      }
    }
    let update_user = await User.update(
      {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        password: req.body.password,
        email: req.body.email,
        role_id: req.body.role_id,
        gender: req.body.gender
      },
      { where: { id: id } }
    );
    return res.status(200).json(update_user);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});

router.delete("/:id/delete", async (req, res, next) => {
  try {
    let deluser = await User.destroy({ where: { id: req.params.id } });
    return res.status(204).json(deluser);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});

router.delete("/bdelete", async (req, res, next) => {
  try {
    let deluser = await User.destroy({ where: { id: req.body.id } });
    return res.status(204).json(deluser);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});

router.patch("/bupdate", async (req, res, next) => {
  try {
    let deluser = await User.update(
      { deletedAt: null },
      { where: { id: req.body.id }, paranoid: false }
    );

    return res.status(200).json(deluser);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});

module.exports = router;
