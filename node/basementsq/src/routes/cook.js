const router = require("express").Router();
const { COMMENT, RECIPE, CHEF, TESTING } = require("../models/COOK");

router.post("/chef/insert", async (req, res, next) => {
  try {
    let chef = await CHEF.create({
      chef_name: req.body.chef_name,
      chef_native: req.body.chef_native,
      chef_exp: req.body.chef_exp,
      chef_special: req.body.chef_special
    });
    return res.status(201).json(chef);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/taste/test", async (req, res, next) => {
  try {
    let test = await TESTING.create({
      chef_id: req.body.chef_id,
      recipe_id: req.body.recipe_id
    });
    return res.status(201).json(test);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/comment", async (req, res, next) => {
  try {
    let comment = await COMMENT.create({
      recipe_id: req.body.recipe_id,
      user_name: req.body.user_name,
      comment: req.body.comment,
      user_rating: req.body.user_rating,
      user_taste: req.body.user_taste,
      user_location: req.body.user_location
    });
    return res.status(201).json(comment);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/d/insert", async (req, res, next) => {
  try {
    const dish = {
      dish_name: req.body.dish_name,
      dish_ingrediant: req.body.dish_ingrediant,
      dish_orgin: req.body.dish_orgin,
      dish_discription: req.body.dish_discription,
      dish_style: req.body.dish_style,
      chef_name: req.body.chef_name,
      chef_exp: req.body.chef_exp
    };

    let newdish = await RECIPE.create(dish);
    if (newdish != null) {
      newdish.addChefs([req.body.chef_id]);
    }

    return res.status(201).json(newdish);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/dish/insert", async (req, res, next) => {
  try {
    let dish = await RECIPE.create({
      dish_name: req.body.dish_name,
      dish_ingrediant: req.body.dish_ingrediant,
      dish_orgin: req.body.dish_orgin,
      dish_discription: req.body.dish_discription,
      dish_style: req.body.dish_style,
      chef_name: req.body.chef_name,
      chef_exp: req.body.chef_exp
    });

    return res.status(201).json(dish);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/hasone/:id", async (req, res, next) => {
  try {
    let comments = await RECIPE.findOne({
      where: { id: req.params.id },
      attributes: ["dish_name", "chef_name"],
      include: [
        {
          model: COMMENT,
          attributes: ["user_name", "comment"]
        }
      ]
    });
    return res.status(200).json(comments);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/belongsto/:id", async (req, res, next) => {
  try {
    let comments = await COMMENT.findOne({
      where: { id: req.params.id },
      attributes: ["user_name", "comment"],
      include: [
        {
          model: RECIPE,
          attributes: ["dish_name", "chef_name"]
        }
      ]
    });
    return res.status(200).json(comments);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/hasmany/:id", async (req, res, next) => {
  try {
    let comments = await RECIPE.findOne({
      where: { id: req.params.id },
      attributes: ["dish_name", "chef_name"],
      include: [
        {
          model: COMMENT,
          attributes: ["user_name", "comment"],
          as: "comments"
        }
      ]
    });
    return res.status(200).json(comments);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/belongstomany", async (req, res, next) => {
  try {
    let comments = await RECIPE.findAll({
      include: [
        {
          model: CHEF
        }
      ]
    });
    return res.status(200).json(comments);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.patch("/d/:id/update", async (req, res, next) => {
  try {
    const id = req.params.id;
    let newdish = await RECIPE.findByPk(id);
    if (newdish != null) {
      newdish.update({
        dish_name: req.body.dish_name,
        dish_ingrediant: req.body.dish_ingrediant,
        dish_orgin: req.body.dish_orgin,
        dish_discription: req.body.dish_discription,
        dish_style: req.body.dish_style,
        chef_name: req.body.chef_name,
        chef_exp: req.body.chef_exp
      });
      newdish.setChefs([req.body.chef_id], { through: { TESTING: false } });
    }

    return res.status(201).json(newdish);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
