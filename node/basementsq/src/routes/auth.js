const router = require("express").Router();
const { User, ValSchema, LoginSchema } = require("../models/User");
const { validationError } = require("../helpers/common");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Joi = require("@hapi/joi");

router.post("/login", async (req, res, next) => {
  try {
    const validateResult = LoginSchema.validate(req.body, {
      abortEarly: false
    });

    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let user = await User.findOne({ where: { email: req.body.email } });

    if (user == null)
      return res
        .status(400)
        .json("Email or Password not match with our records");

    if (user.status == 0) return res.status(400).json("user expire");

    let password_compare = bcrypt.compareSync(req.body.password, user.password);
    if (password_compare == false)
      return res
        .status(400)
        .json("Email or Password not match with our records");

    const token = jwt.sign(
      {
        iss: "AED PLATFORM",
        iat: new Date().getTime(),
        sub: user
      },
      process.env.JWT_SECRET,
      { expiresIn: "7d" }
    );

    return res.status(200).json({
      token: token,
      token_type: "Bearer",
      role_id: user.role_id,
      user: user
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/register", async (req, res, next) => {
  try {
    const validateResult = ValSchema.validate(req.body, {
      abortEarly: false
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let exists = await User.findOne({ where: { email: req.body.email } });
    if (exists != null)
      return res.status(400).json("Email is already registered");

    req.body.password = bcrypt.hashSync(req.body.password, 10);

    let new_user = await User.create(req.body);
    return res.status(201).json(new_user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/all_user", async (req, res, next) => {
  try {
    let all_user = await User.findAll();
    return res.status(201).json(all_user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
