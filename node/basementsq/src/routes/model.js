const router = require("express").Router();
// const { Model, ValSchema  } = require("../models/Model");
const { Model, modelSchema } = require("../models/Model")
const { validationError } = require("../helpers/common");

router.post("/model", async (req, res, next) => {
  try {
    const validateResult = modelSchema.validate(req.body, {
      abortEarly: false
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let new_model = await Model.create(req.body);
    return res.status(201).json(new_model);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get('/brand/:id/models',async (req, res, next) => {
	try{
    let brand_id = await Model.findAll({ where: { brand_id: req.params.id } });
    
		return res.status(201).json(brand_id);

	}
	catch (error) {
    console.log(error);
    next(error);
  }
});
module.exports = router;
