const router = require("express").Router();
const { validationError } = require("../helpers/common");
const { AED, AEDContact, HMANY, AEDSchema } = require("../models/AED");
const Joi = require("@hapi/joi");

router.post("/", async (req, res, next) => {
  try {
    const validateResult = AEDSchema.validate(req.body, {
      abortEarly: false
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));
    // create a data in single table and in body getting one table value we use
    //  ********let new_model = await AED.create();***********
    //to use ****** add that code and cut from here
    //or
    //when we have multiple table data value in body we use

    const Listings = {
      brand: req.body.brand,
      model: req.body.model,
      serial: req.body.serial,
      reg: req.body.reg,
      purchase_date: req.body.purchase_date,
      first_pad: req.body.first_pad,
      first_pad_exp: req.body.first_pad_exp,
      second_pad: req.body.second_pad,
      second_pad_exp: req.body.second_pad_exp,
      battry_exp: req.body.battry_exp,
      battery_serial: req.body.battery_serial,
      street: req.body.street,
      number: req.body.number,
      number_addition: req.body.number_addition,
      zipcode: req.body.zipcode,
      country: req.body.country,
      lat: req.body.lat,
      lng: req.body.lng,
      lock_code: req.body.lock_code,
      building_id: req.body.building_id,
      availability: req.body.availability,
      visibility: req.body.visibility,
      status: req.body.status,

      contact: {
        firstname: req.body.contact_firstname,
        lastname: req.body.contact_lastname,
        email: req.body.contact_email,
        gender: req.body.contact_gender,
        mobile: req.body.contact_mobile,
        street: req.body.contact_street,
        housenumber: req.body.contact_housenumber,
        additional_housenumber: req.body.contact_additional_housenumber,
        zip: req.body.contact_zip,
        city: req.body.contact_city,
        remarks: req.body.contact_remarks
      }
    };
    let new_model = await AED.create(Listings, {
      include: [
        {
          model: AEDContact,
          as: "contact"
        }
      ]
    });
    // end here
    return res.status(201).json(new_model);
  } catch (error) {
    console.log(error);
    next(error);
  }
});
router.post("/many", async (req, res, next) => {
  try {
    const mListings = {
      firstname: req.body.contact_firstname,
      lastname: req.body.contact_lastname,
      email: req.body.contact_email,
      gender: req.body.contact_gender,
      mobile: req.body.contact_mobile,
      street: req.body.contact_street,
      housenumber: req.body.contact_housenumber,
      additional_housenumber: req.body.contact_additional_housenumber,
      zip: req.body.contact_zip,
      city: req.body.contact_city,
      remarks: req.body.contact_remarks,

      many: {
        firstname: req.body.contact_firstname,
        lastname: req.body.contact_lastname,
        email: req.body.contact_email,
        gender: req.body.contact_gender,
        mobile: req.body.contact_mobile,
        street: req.body.contact_street,
        housenumber: req.body.contact_housenumber,
        additional_housenumber: req.body.contact_additional_housenumber,
        zip: req.body.contact_zip,
        city: req.body.contact_city,
        remarks: req.body.contact_remarks
      }
    };
    let mtable = await AED.create(mListings, {
      include: [
        {
          model: HMANY,
          as: "many"
        }
      ]
    });
    // end here
    return res.status(201).json(mtable);
  } catch (error) {
    console.log(error);
    next(error);
  }
});
router.get("/all", async (req, res, next) => {
  try {
    //let all_user = await AED.findAll();
    let all_user = await AED.findAll({
      include: [
        {
          model: AEDContact,
          as: "contact"
        }
      ]
    });
    return res.status(200).json(all_user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/id/:id", async (req, res, next) => {
  try {
    let user_id = await AED.findOne({ where: { id: req.params.id } });

    return res.status(201).json(user_id);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.delete("/id/:id", async (req, res, next) => {
  try {
    let deluser = await AED.destroy({ where: { id: req.params.id } });
    return res.status(204).json(deluser);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});

router.patch("/id/:id/restore", async (req, res, next) => {
  try {
    let deluser = await AED.update(
      { deletedAt: null },
      { where: { id: req.params.id }, paranoid: false }
    );

    return res.status(200).json(deluser);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});

router.patch("/id/:id/update", async (req, res, next) => {
  try {
    const id = req.params.id;
    let user = await AED.findOne({ where: { id: req.params.id } });
    const AEDSchema = Joi.object({
      brand: Joi.string().allow(null),
      model: Joi.string().allow(null),
      serial: Joi.string().allow(null),
      reg: Joi.string().allow(null),
      purchase_date: Joi.date().allow(null),
      first_pad: Joi.string().allow(null),
      first_pad_exp: Joi.date().allow(null),
      second_pad: Joi.allow(null),
      second_pad_exp: Joi.allow(null),
      battry_exp: Joi.allow(null),
      battery_serial: Joi.allow(null),
      street: Joi.string().allow(null),
      number: Joi.allow(null),
      number_addition: Joi.allow(null),
      zipcode: Joi.string().allow(null),
      city: Joi.string().allow(null),
      country: Joi.string().allow(null),
      lat: Joi.allow(null),
      lng: Joi.allow(null),
      lock_code: Joi.allow(null),
      building_id: Joi.allow(null),
      availability: Joi.allow(null),
      visibility: Joi.allow(null),
      status: Joi.allow(null),
      user_id: Joi.allow(null),

      contact_firstname: Joi.allow(null),
      contact_lastname: Joi.allow(null),
      contact_email: Joi.allow(null),
      contact_gender: Joi.allow(null),
      contact_mobile: Joi.allow(null),
      contact_street: Joi.allow(null),
      contact_housenumber: Joi.allow(null),
      contact_additional_housenumber: Joi.allow(null),
      contact_zip: Joi.allow(null),
      contact_city: Joi.allow(null),
      contact_remarks: Joi.allow(null)
    });
    const validateResult = AEDSchema.validate(req.body, {
      abortEarly: false
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));
    let update_user = await AED.update(
      {
        brand: req.body.brand,
        model: req.body.model,
        serial: req.body.serial,
        reg: req.body.reg,
        purchase_date: req.body.purchase_date,
        first_pad: req.body.first_pad,
        first_pad_exp: req.body.first_pad_exp,
        second_pad: req.body.second_pad,
        second_pad_exp: req.body.second_pad_exp,
        battry_exp: req.body.battry_exp,
        battery_serial: req.body.battery_serial,
        street: req.body.street,
        number: req.body.number,
        number_addition: req.body.number_addition,
        zipcode: req.body.zipcode,
        country: req.body.country,
        lat: req.body.lat,
        lng: req.body.lng,
        lock_code: req.body.lock_code,
        building_id: req.body.building_id,
        availability: req.body.availability,
        visibility: req.body.visibility,
        status: req.body.status,

        contact_firstname: req.body.contact_firstname,
        contact_lastname: req.body.contact_lastname,
        contact_email: req.body.contact_email,
        contact_gender: req.body.contact_gender,
        contact_mobile: req.body.contact_mobile,
        contact_street: req.body.contact_street,
        contact_housenumber: req.body.contact_housenumber,
        contact_additional_housenumber: req.body.contact_additional_housenumber,
        contact_zip: req.body.contact_zip,
        contact_city: req.body.contact_city,
        contact_remarks: req.body.contact_remarks
      },
      { where: { id: id } }
    );
    return res.status(200).json(update_user);
  } catch (error) {
    //   }
    console.log(error);
    next(error);
  }
});
module.exports = router;
