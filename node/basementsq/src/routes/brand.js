const router = require("express").Router();
// const { Model, ValSchema  } = require("../models/Model");
const { Brand, brandSchema } = require("../models/Brand")
const { validationError } = require("../helpers/common");

router.post("/brand", async (req, res, next) => {
  try {
    const validateResult = brandSchema.validate(req.body, {
      abortEarly: false
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let new_model = await Brand.create(req.body);
    return res.status(201).json(new_model);
  } catch (error) {
    console.log(error);
    next(error);
  }
});
router.get('/brands',async (req, res, next) => {
	try{
		let all_brands = await Brand.findAll();
		 return res.status(201).json(all_brands);

	}
	catch (error) {
    console.log(error);
    next(error);
  }
});
module.exports = router;


