const sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const AED = db.define(
  "aeds",
  {
    uuid: {
      type: sequelize.UUID,
      defaultValue: sequelize.UUIDV4
    },
    brand: sequelize.STRING,
    model: sequelize.STRING,
    serial: sequelize.STRING,
    reg: sequelize.STRING,
    purchase_date: sequelize.DATEONLY,
    first_pad: sequelize.STRING,
    first_pad_exp: sequelize.DATEONLY,
    second_pad: { type: sequelize.STRING, allowNull: true },
    second_pad_exp: { type: sequelize.DATEONLY, allowNull: true },
    battry_exp: { type: sequelize.DATEONLY, allowNull: true },
    battery_serial: { type: sequelize.STRING, allowNull: true },
    street: sequelize.STRING,
    number: { type: sequelize.STRING, allowNull: true },
    number_addition: { type: sequelize.STRING, allowNull: true },
    zipcode: sequelize.STRING,
    city: sequelize.STRING,
    country: sequelize.STRING,
    lat: { type: sequelize.STRING, allowNull: true },
    lng: { type: sequelize.STRING, allowNull: true },
    lock_code: { type: sequelize.STRING, allowNull: true },
    building_id: { type: sequelize.STRING, allowNull: true },
    availability: { type: sequelize.TEXT, allowNull: true },
    visibility: { type: sequelize.STRING, defaultValue: "public" },
    status: { type: sequelize.STRING, defaultValue: "active" },
    user_id: sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

AED.sync();

const AEDContact = db.define(
  "aed_contacts",
  {
    aed_id: sequelize.INTEGER,
    firstname: { type: sequelize.STRING, allowNull: true },
    lastname: { type: sequelize.STRING, allowNull: true },
    email: { type: sequelize.STRING, allowNull: true },
    gender: { type: sequelize.STRING, allowNull: true },
    mobile: { type: sequelize.STRING, allowNull: true },
    street: { type: sequelize.TEXT, allowNull: true },
    housenumber: { type: sequelize.TEXT, allowNull: true },
    additional_housenumber: { type: sequelize.TEXT, allowNull: true },
    zip: { type: sequelize.STRING, allowNull: true },
    city: { type: sequelize.STRING, allowNull: true },
    remarks: { type: sequelize.TEXT, allowNull: true }
  },
  { underscored: true }
);

AEDContact.sync();

const HMANY = db.define(
  "h_many",
  {
    aed_id: sequelize.INTEGER,
    firstname: { type: sequelize.STRING, allowNull: true },
    lastname: { type: sequelize.STRING, allowNull: true },
    email: { type: sequelize.STRING, allowNull: true },
    gender: { type: sequelize.STRING, allowNull: true },
    mobile: { type: sequelize.STRING, allowNull: true },
    street: { type: sequelize.TEXT, allowNull: true },
    housenumber: { type: sequelize.TEXT, allowNull: true },
    additional_housenumber: { type: sequelize.TEXT, allowNull: true },
    zip: { type: sequelize.STRING, allowNull: true },
    city: { type: sequelize.STRING, allowNull: true },
    remarks: { type: sequelize.TEXT, allowNull: true }
  },
  { underscored: true }
);

HMANY.sync();

AED.hasOne(AEDContact, { as: "contact", foreignKey: "aed_id" });

const AEDSchema = Joi.object({
  brand: Joi.string().required(),
  model: Joi.string().required(),
  serial: Joi.string().required(),
  reg: Joi.string().required(),
  purchase_date: Joi.date().required(),
  first_pad: Joi.string().required(),
  first_pad_exp: Joi.date().required(),
  second_pad: Joi.allow(null),
  second_pad_exp: Joi.allow(null),
  battry_exp: Joi.allow(null),
  battery_serial: Joi.allow(null),
  street: Joi.string().required(),
  number: Joi.allow(null),
  number_addition: Joi.allow(null),
  zipcode: Joi.string().required(),
  city: Joi.string().required(),
  country: Joi.string().required(),
  lat: Joi.allow(null),
  lng: Joi.allow(null),
  lock_code: Joi.allow(null),
  building_id: Joi.allow(null),
  availability: Joi.allow(null),
  visibility: Joi.allow(null),
  status: Joi.allow(null),
  user_id: Joi.allow(null),

  contact_firstname: Joi.allow(null),
  contact_lastname: Joi.allow(null),
  contact_email: Joi.allow(null),
  contact_gender: Joi.allow(null),
  contact_mobile: Joi.allow(null),
  contact_street: Joi.allow(null),
  contact_housenumber: Joi.allow(null),
  contact_additional_housenumber: Joi.allow(null),
  contact_zip: Joi.allow(null),
  contact_city: Joi.allow(null),
  contact_remarks: Joi.allow(null)
});

module.exports = { AED, AEDContact, HMANY, AEDSchema };
