const Sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const Brand = db.define(
  "brands",
  {
    name: Sequelize.STRING,
    status: { type: Sequelize.INTEGER, defaultValue: 1 }
},
  
  {
    paranoid: true
   
  }
);

Brand.sync();

const brandSchema = Joi.object({
	name: Joi.string().required(),
   	status: Joi.number().required()
});


//Validation Schema

module.exports = { Brand, brandSchema };
