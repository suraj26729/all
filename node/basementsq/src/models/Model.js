const Sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const Model = db.define(
  "models",
  {
    name: Sequelize.STRING,
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
   	brand_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
   
  }
);

Model.sync();

const modelSchema = Joi.object({
	name: Joi.string().required(),
  	status: Joi.number().required(),
  	brand_id: Joi.number().required()
});

//Validation Schema

module.exports = { Model, modelSchema };
