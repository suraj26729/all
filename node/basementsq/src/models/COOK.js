const sequelize = require("sequelize");
const db = require("../config/database");
// const Joi = require("@hapi/joi");

const RECIPE = db.define(
  "recipes",
  {
    dish_name: sequelize.STRING,
    dish_ingrediant: sequelize.STRING,
    dish_discription: sequelize.STRING,
    dish_orgin: sequelize.STRING,
    dish_style: sequelize.STRING,
    chef_name: sequelize.STRING,
    chef_exp: sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

RECIPE.sync();

const COMMENT = db.define(
  "comments",
  {
    recipe_id: sequelize.INTEGER,
    user_name: sequelize.STRING,
    comment: sequelize.STRING,
    user_rating: sequelize.INTEGER,
    user_taste: sequelize.STRING,
    user_location: sequelize.STRING
  },
  {
    paranoid: true,
    underscored: true
  }
);

COMMENT.sync();

const CHEF = db.define(
  "chefs",
  {
    chef_name: sequelize.STRING,
    chef_native: sequelize.STRING,
    chef_exp: sequelize.STRING,
    chef_special: sequelize.STRING
  },
  {
    paranoid: true,
    underscored: true
  }
);

CHEF.sync();

const TESTING = db.define(
  "tests",
  {
    chef_id: sequelize.INTEGER,
    recipe_id: sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

TESTING.sync();
// TESTING.belongsToMany(CHEF), { foreignKey: "chef_id" };
// CHEF.belongsToMany(RECIPE, { through: 'TESTING' });
// COMMENT.belongsTo(RECIPE, { foreignKey: "recipe_id" });
// RECIPE.hasOne(COMMENT, {foreignKey: "recipe_id"});

RECIPE.hasMany(COMMENT, { foreignKey: "recipe_id", as: "comments" });
// RECIPE.hasOne(COMMENT, { foreignKey: "recipe_id" });
// COMMENT.belongsTo(RECIPE, { foreignKey: "recipe_id" });

// RECIPE.belongsToMany(CHEF, {
//   foreignKey: "recipe_id",
//   through: /*table name*/ "tests"
// }); //if you want to st module name set thtough: TESTING
// CHEF.belongsToMany(RECIPE, { foreignKey: "chef_id", through: "tests" });

module.exports = { COMMENT, RECIPE, CHEF, TESTING };
