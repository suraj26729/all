const { Strategy, ExtractJwt } = require("passport-jwt");
const { User } = require("../models/User");
const secret = process.env.JWT_SECRET;
const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: secret
};

module.exports = passport => {
  passport.use(
    new Strategy(opts, (payload, done) => {
      User.findByPk(payload.id)
        .then(user => {
          //console.log(payload);
          if (user) {
            return done(null, user);
          } else {
            return done(null, false);
          }
        })
        .catch(err => console.error(err));
    })
  );
};
