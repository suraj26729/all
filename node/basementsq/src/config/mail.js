const nodemailer = require("nodemailer");

// var sgTransport = require("nodemailer-sendgrid-transport");
const hbs= require("nodemailer-express-handlebars");
exports.send = async (to, subject, message, next) => {
  try {
    var transport = nodemailer.createTransport({
      host: "smtp.mailtrap.io",
      port: 2525,
      auth: {
        user: "e6ddb8edd44b79",
        pass: "edbc49d4c6c7eb"
      }
    });
   
  
    // let transporter = nodemailer.createTransport(
    //   sgTransport({
    //     auth: {
    //       api_key: process.env.SEND_GRID_API_KEY
    //     }
    //   })
    // );
    const mailOptions = {
      from: process.env.ADMIN_MAIL,
      to: to,
      subject: subject,
      generateTextFromHTML: true,
      html: message
     
    };

    let result = await transport.sendMail(mailOptions);
    // console.log(result);
    transport.close();
    return result;
  } catch (error) {
    next(error);
  }
};
