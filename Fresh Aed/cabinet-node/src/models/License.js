const sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");
const { Order } = require("./Order");
const { Product } = require("./Product");
const { User } = require("./User");

const License = db.define(
  "licenses",
  {
    license: sequelize.TEXT,
    recurring_duration: sequelize.INTEGER,
    product_id: sequelize.INTEGER,
    order_id: sequelize.INTEGER,
    status: { type: sequelize.STRING, defaultValue: "new" },
    user_id: sequelize.INTEGER,
    expiry_date: sequelize.DATE,
  },
  { underscored: true, paranoid: true }
);

License.sync();

License.belongsTo(Order, { as: "order", foreignKey: "order_id" });
License.belongsTo(Product, { as: "product", foreignKey: "product_id" });
License.belongsTo(User, { as: "user", foreignKey: "user_id" });
Order.hasMany(License, { as: "licenses", foreignKey: "order_id" });

const ValSchema = Joi.object({
  license: Joi.string().required(),
  product_id: Joi.number().required(),
  order_id: Joi.number().required(),
  status: Joi.allow(true),
  user_id: Joi.number().required(),
  expiry_date: Joi.allow(null),
});

module.exports = { License, ValSchema };
