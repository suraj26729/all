const sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const Product = db.define(
  "products",
  {
    uuid: {
      type: sequelize.UUID,
      defaultValue: sequelize.UUIDV4,
    },
    name: sequelize.STRING,
    description: sequelize.TEXT,
    regular_price: { type: sequelize.DOUBLE, defaultValue: 0.0 },
    sale_price: { type: sequelize.DOUBLE, defaultValue: 0.0 },
    no_license: { type: sequelize.INTEGER, defaultValue: 1 },
    is_recurring: { type: sequelize.BOOLEAN, defaultValue: false },
    recurring_duration: { type: sequelize.INTEGER, defaultValue: 0 },
    recurring_cycle: { type: sequelize.INTEGER, defaultValue: 0 },
    status: { type: sequelize.STRING, defaultValue: "active" },
  },
  { paranoid: true, underscored: true }
);

Product.sync();

const ValSchema = Joi.object({
  name: Joi.string().required(),
  description: Joi.allow(null),
  regular_price: Joi.number().required(),
  sale_price: Joi.allow(null),
  no_license: Joi.allow(null),
  is_recurring: Joi.allow(null),
  recurring_duration: Joi.allow(null),
  recurring_cycle: Joi.allow(null),
  status: Joi.allow(null),
});

module.exports = { Product, ValSchema };
