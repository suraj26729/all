const Sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const User = db.define(
  "users",
  {
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    email_verify_token: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4
    },
    password_reset_token: { type: Sequelize.TEXT, allowNull: true },
    firstname: Sequelize.STRING,
    lastname: Sequelize.STRING,
    gender: Sequelize.STRING,
    phone: { type: Sequelize.STRING, allowNull: true },
    organization: { type: Sequelize.STRING, allowNull: true },
    street: { type: Sequelize.STRING, allowNull: true },
    home_num: { type: Sequelize.STRING, allowNull: true },
    addition_num: { type: Sequelize.STRING, allowNull: true },
    zip: { type: Sequelize.STRING, allowNull: true },
    city: { type: Sequelize.STRING, allowNull: true },
    country: { type: Sequelize.STRING, allowNull: true },
    status: { type: Sequelize.INTEGER, defaultValue: 0 },
    role_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

User.sync();

//Validation Schema
const ValSchema = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2 })
    .required(),
  password: Joi.string()
    .pattern(
      new RegExp(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/
      )
    )
    .required(),
  password_reset_token: Joi.allow(null),
  firstname: Joi.string().required(),
  lastname: Joi.string().required(),
  gender: Joi.string().required(),
  phone: Joi.allow(null),
  organization: Joi.allow(null),
  street: Joi.allow(null),
  home_num: Joi.allow(null),
  addition_num: Joi.allow(null),
  zip: Joi.allow(null),
  city: Joi.allow(null),
  country: Joi.allow(null),
  status: Joi.allow(null),
  role_id: Joi.number().required()
});

const LoginSchema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required()
});

module.exports = { User, ValSchema, LoginSchema };
