const sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");
const { License } = require("../models/License");

const AED = db.define(
  "aeds",
  {
    uuid: {
      type: sequelize.UUID,
      defaultValue: sequelize.UUIDV4,
    },
    cabinet_code: sequelize.STRING,
    license_key_id: sequelize.INTEGER,
    brand: sequelize.STRING,
    model: sequelize.STRING,
    serial: sequelize.STRING,
    reg: { type: sequelize.STRING, allowNull: true },
    purchase_date: { type: sequelize.DATEONLY, allowNull: true },
    first_pad: sequelize.STRING,
    first_pad_exp: sequelize.DATEONLY,
    second_pad: { type: sequelize.STRING, allowNull: true },
    second_pad_exp: { type: sequelize.DATEONLY, allowNull: true },
    battry_exp: { type: sequelize.DATEONLY, allowNull: true },
    battery_serial: { type: sequelize.STRING, allowNull: true },
    street: sequelize.STRING,
    number: { type: sequelize.STRING, allowNull: true },
    number_addition: { type: sequelize.STRING, allowNull: true },
    zipcode: sequelize.STRING,
    city: sequelize.STRING,
    country: sequelize.STRING,
    lat: { type: sequelize.STRING, allowNull: true },
    lng: { type: sequelize.STRING, allowNull: true },
    lock_code: { type: sequelize.STRING, allowNull: true },
    building_id: { type: sequelize.STRING, allowNull: true },
    availability: { type: sequelize.TEXT, allowNull: true },
    locality: sequelize.TEXT,
    visibility: { type: sequelize.STRING, defaultValue: "public" },
    status: { type: sequelize.STRING, defaultValue: "active" },
    open_closed: { type: sequelize.BOOLEAN, defaultValue: true },
    present: { type: sequelize.BOOLEAN, defaultValue: true },
    power: { type: sequelize.BOOLEAN, defaultValue: true },
    connection: { type: sequelize.BOOLEAN, defaultValue: true },
    user_id: sequelize.INTEGER,
  },
  {
    paranoid: true,
    underscored: true,
  }
);

AED.sync();

const Responder = db.define(
  "responders",
  {
    aed_id: sequelize.INTEGER,
    name: sequelize.STRING,
    email: sequelize.STRING,
    phone: sequelize.STRING,
    sms_open_closed: sequelize.BOOLEAN,
    sms_aed_present: sequelize.BOOLEAN,
    sms_aed_status: sequelize.BOOLEAN,
    sms_power: sequelize.BOOLEAN,
    sms_connection: sequelize.BOOLEAN,
    sms_maintenance: sequelize.BOOLEAN,
    email_open_closed: sequelize.BOOLEAN,
    email_aed_present: sequelize.BOOLEAN,
    email_aed_status: sequelize.BOOLEAN,
    email_power: sequelize.BOOLEAN,
    email_connection: sequelize.BOOLEAN,
    email_maintenance: sequelize.BOOLEAN,
  },
  { underscored: true }
);

Responder.sync();

AED.belongsTo(License, { as: "license", foreignKey: "license_key_id" });
AED.hasMany(Responder, { as: "responders", foreignKey: "aed_id" });

const AEDSchema = Joi.object({
  cabinet_code: Joi.string().required(),
  license_key: Joi.string().required(),
  brand: Joi.string().required(),
  model: Joi.string().required(),
  serial: Joi.string().required(),
  reg: Joi.allow(),
  purchase_date: Joi.allow(null),
  first_pad: Joi.string().required(),
  first_pad_exp: Joi.date().required(),
  second_pad: Joi.allow(null),
  second_pad_exp: Joi.allow(null),
  battry_exp: Joi.allow(null),
  battery_serial: Joi.allow(null),
  street: Joi.string().required(),
  number: Joi.allow(null),
  number_addition: Joi.allow(null),
  zipcode: Joi.string().required(),
  city: Joi.string().required(),
  country: Joi.string().required(),
  lat: Joi.allow(null),
  lng: Joi.allow(null),
  lock_code: Joi.allow(null),
  building_id: Joi.allow(null),
  availability: Joi.allow(null),
  locality: Joi.allow(null),
  visibility: Joi.allow(null),
  status: Joi.allow(null),
  open_closed: Joi.allow(null),
  present: Joi.allow(null),
  power: Joi.allow(null),
  connection: Joi.allow(null),
  user_id: Joi.allow(null),
  responders: Joi.allow(null),
});

const ResponderSchema = Joi.object({
  aed_id: Joi.number().required(),
  name: Joi.required(),
  email: Joi.allow(null),
  phone: Joi.required(),
});

module.exports = { AED, Responder, AEDSchema, ResponderSchema };
