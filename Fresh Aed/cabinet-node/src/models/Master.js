const sequelize = require("sequelize");
const db = require("../config/database");

const Brand = db.define(
  "brands",
  {
    brand: sequelize.STRING,
    status: sequelize.STRING,
  },
  { underscored: true }
);

Brand.sync();

const Model = db.define(
  "models",
  {
    model: sequelize.STRING,
    brand: sequelize.INTEGER,
    status: sequelize.STRING,
  },
  { underscored: true }
);

Model.sync();

const City = db.define(
  "cities",
  {
    name: sequelize.STRING,
    status: sequelize.STRING,
  },
  { underscored: true }
);

City.sync();

const CabinnetCode = db.define(
  "cabinet_codes",
  {
    code: sequelize.STRING,
    isconnected: { type: sequelize.BOOLEAN, defaultValue: false },
    cabinet_id: sequelize.INTEGER,
    emei_id: sequelize.STRING,
    sim_id: sequelize.STRING,
    previous_data: sequelize.TEXT,
  },
  { underscored: true }
);

CabinnetCode.sync();

Brand.hasMany(Model, { as: "models", foreignKey: "brand" });

module.exports = { Brand, Model, City, CabinnetCode };
