const sequelize = require("sequelize");
const db = require("../config/database");
const Joi = require("@hapi/joi");

const { User } = require("./User");
const { Product } = require("./Product");

const Order = db.define(
  "orders",
  {
    uuid: { type: sequelize.UUID, defaultValue: sequelize.UUIDV4 },
    product_id: sequelize.INTEGER,
    no_license: sequelize.INTEGER,
    billing_firstname: sequelize.STRING,
    billing_lastname: sequelize.STRING,
    billing_email: sequelize.STRING,
    billing_phone: sequelize.STRING,
    billing_organization: sequelize.STRING,
    billing_street: sequelize.STRING,
    billing_home_num: sequelize.STRING,
    billing_addition_num: sequelize.STRING,
    billing_zip: sequelize.STRING,
    billing_city: sequelize.STRING,
    total: sequelize.DOUBLE,
    status: { type: sequelize.STRING, defaultValue: "pending" },
    user_id: sequelize.INTEGER,
    payment_data: sequelize.TEXT,
  },
  { underscored: true, paranoid: true }
);

Order.sync();

Order.belongsTo(Product, { as: "product", foreignKey: "product_id" });
Order.belongsTo(User, { as: "user", foreignKey: "user_id" });

const ValSchema = Joi.object({
  product_id: Joi.number().required(),
  no_license: Joi.number().required(),
  billing_firstname: Joi.string().required(),
  billing_lastname: Joi.string().required(),
  billing_email: Joi.string().required(),
  billing_phone: Joi.string().required(),
  billing_organization: Joi.allow(null),
  billing_street: Joi.string().required(),
  billing_home_num: Joi.allow(null),
  billing_addition_num: Joi.allow(null),
  billing_zip: Joi.string().required(),
  billing_city: Joi.string().required(),
  total: Joi.number().required(),
  status: Joi.allow(null),
  user_id: Joi.number().required(),
  payment_data: Joi.allow(null),
});

const RegValSchema = Joi.object({
  product_id: Joi.number().required(),
  no_license: Joi.number().required(),
  billing_firstname: Joi.string().required(),
  billing_lastname: Joi.string().required(),
  billing_email: Joi.string().required(),
  gender: Joi.string().required(),
  password: Joi.string().required(),
  billing_phone: Joi.string().required(),
  billing_organization: Joi.allow(null),
  billing_street: Joi.string().required(),
  billing_home_num: Joi.allow(null),
  billing_addition_num: Joi.allow(null),
  billing_zip: Joi.string().required(),
  billing_city: Joi.string().required(),
  total: Joi.number().required(),
});

module.exports = { Order, ValSchema, RegValSchema };
