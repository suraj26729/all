const router = require("express").Router();
const { User, ValSchema, LoginSchema } = require("../models/User");
const { validationError } = require("../helpers/common");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const mail = require("../config/mail");

router.post("/login", async (req, res, next) => {
  try {
    const validateResult = LoginSchema.validate(req.body, {
      abortEarly: false,
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let user = await User.findOne({ where: { email: req.body.email } });
    if (user == null)
      return res
        .status(400)
        .json("Email or Password not match with our records");

    if (user.email_verify_token != null) {
      let result = await mail.sendEmailVerification(user);
      return res
        .status(400)
        .json(
          "Email is not verified yet. New Verification link has been sent to you now"
        );
    }

    if (user.status == 0)
      return res
        .status(400)
        .json("Account is suspended. Please contact administrator");

    let password_compare = bcrypt.compareSync(req.body.password, user.password);
    if (password_compare == false)
      return res
        .status(400)
        .json("Email or Password not match with our records");

    const token = jwt.sign(
      {
        iss: process.env.APP_NAME,
        iat: new Date().getTime(),
        sub: user,
      },
      process.env.JWT_SECRET,
      { expiresIn: "7d" }
    );

    return res.status(200).json({
      token: token,
      token_type: "Bearer",
      role_id: user.role_id,
      user: user,
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/register", async (req, res, next) => {
  try {
    const validateResult = ValSchema.validate(req.body, {
      abortEarly: false,
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let exists = await User.findOne({ where: { email: req.body.email } });
    if (exists != null)
      return res.status(400).json("Email is already registered");

    req.body.password = bcrypt.hashSync(req.body.password, 10);

    let new_user = await User.create(req.body);

    if (new_user != null) await mail.sendEmailVerification(new_user);

    return res.status(201).json(new_user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.patch("/verifiy/:token", async (req, res, next) => {
  try {
    let user = await User.findOne({
      where: { email_verify_token: req.params.token },
    });
    if (user == null)
      return res.status(404).json("User is not found to verify");

    user.update({ email_verify_token: null, status: 1 });

    return res.status(200).json(user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/forgot-password", async (req, res, next) => {
  try {
    let user = await User.findOne({
      where: { email: req.body.email },
    });
    if (user == null) return res.status(404).json("User is not found");

    let result = await mail.sendPasswordResetEmail(user);

    return res.status(200).json(result);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.patch("/reset-password/:token", async (req, res, next) => {
  try {
    let user = await User.findOne({
      where: { email: req.body.email },
    });
    if (user == null) return res.status(404).json("User is not found");

    user = await User.findOne({
      where: { email: req.body.email, password_reset_token: req.params.token },
    });
    if (user == null) return res.status(404).json("Email and token mismatch");

    let new_password = null;
    if (req.body.password != "" && req.body.confirm_password != "")
      new_password = bcrypt.hashSync(req.body.password, 10);

    if (new_password != null) {
      user.update({ password: new_password, password_reset_token: null });
    }
    return res.status(200).json("Password changed successfully");
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post("/register/send-token", async (req, res, next) => {
  try {
    let { email } = req.body;
    let user = await User.findOne({ where: { email: email } });
    if (user != null)
      return res.status(400).json("Email is already registered");

    let result = await mail.sendEmailVerificationCode(user);
    return res.status(200).json(result);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
