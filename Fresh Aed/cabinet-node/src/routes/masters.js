const router = require("express").Router();
const { Brand, Model, City } = require("../models/Master");

router.get("/brands", async (req, res, next) => {
  try {
    let brands = await Brand.findAll({
      where: { status: 1 },
      order: [["brand", "ASC"]],
    });
    return res.status(200).json(brands);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/brand/:brand/models", async (req, res, next) => {
  try {
    let models = await Model.findAll({
      where: { brand: req.params.brand, status: 1 },
      order: [["model", "ASC"]],
    });
    return res.status(200).json(models);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/cities", async (req, res, next) => {
  try {
    let cities = await City.findAll({
      where: { status: 1 },
      order: [["name", "ASC"]],
    });
    return res.status(200).json(cities);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
