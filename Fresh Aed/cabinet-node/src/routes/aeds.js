const router = require("express").Router();
const sequelize = require("sequelize");
const Op = sequelize.Op;
const { User } = require("../models/User");
const { AED, Responder, AEDSchema, ResponderSchema } = require("../models/AED");
const { validationError } = require("../helpers/common");
const passport = require("passport");
const NodeGeocoder = require("node-geocoder");
const { CabinnetCode } = require("../models/Master");
const { License } = require("../models/License");
const moment = require("moment");

const geocoder = NodeGeocoder({
  provider: "mapquest",
  apiKey: process.env.MAPQUEST_API_KEY,
});

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      const validateResult = AEDSchema.validate(req.body, {
        abortEarly: false,
      });
      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));

      let license = await License.findOne({
        where: { license: req.body.license_key },
        include: [{ association: "product" }],
      });

      let address = [];
      let lat = null;
      let lng = null;
      if (req.body.number != null) address.push(req.body.number);
      if (req.body.street != null) address.push(req.body.street);
      if (req.body.city != null) address.push(req.body.city);
      if (req.body.country != null) address.push(req.body.country);
      if (req.body.zipcode != null) address.push(req.body.zipcode);
      if (address.length) {
        address = address.join(",");
        const location = await geocoder.geocode(address);
        if (location[0] != undefined) {
          lat = location[0].latitude;
          lng = location[0].longitude;
        }
      }
      const NewAed = {
        cabinet_code: req.body.cabinet_code,
        license_key_id: license.id,
        brand: req.body.brand,
        model: req.body.model,
        serial: req.body.serial,
        reg: req.body.reg,
        purchase_date: req.body.purchase_date,
        first_pad: req.body.first_pad,
        first_pad_exp: req.body.first_pad_exp,
        second_pad: req.body.second_pad,
        second_pad_exp: req.body.second_pad_exp,
        battry_exp: req.body.battry_exp,
        battery_serial: req.body.battery_serial,
        street: req.body.street,
        number: req.body.number,
        number_addition: req.body.number_addition,
        zipcode: req.body.zipcode,
        city: req.body.city,
        country: req.body.country,
        lat: lat,
        lng: lng,
        lock_code: req.body.lock_code,
        building_id: req.body.building_id,
        availability: req.body.availability,
        locality: req.body.locality,
        visibility: req.body.visibility,
        status: req.body.status,
        user_id: req.user.id,
        responders: req.body.responders,
      };

      let aed = await AED.create(NewAed, {
        include: [{ association: "responders" }],
      });

      if (aed != null) {
        let cabinet_code = await CabinnetCode.findOne({
          where: { code: req.body.cabinet_code },
        });
        cabinet_code.update({ isconnected: true, cabinet_id: aed.id });

        let recurring_duration = license.product.recurring_duration;
        let expiry_date = moment(new Date()).add(recurring_duration, "months");

        license.update({ status: "used", expiry_date: expiry_date });
      }

      return res.status(201).json(aed);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get("/", async (req, res, next) => {
  try {
    let { lat, lng } = req.query;
    let userIds = await User.findAll({
      attributes: ["id"],
      where: { status: 1 },
    });
    userIds = userIds.map((item) => item.id);

    // lat = 52.132633;
    // lng = 5.291266;
    let attributes = Object.keys(AED.rawAttributes);
    let geo_cond = [];
    if (lat && lng) {
      attributes.push([
        sequelize.literal(
          "6371 * acos(cos(radians(" +
            parseFloat(lat) +
            ")) * cos(radians(lat)) * cos(radians(" +
            parseFloat(lng) +
            ") - radians(lng)) + sin(radians(" +
            parseFloat(lat) +
            ")) * sin(radians(lat)))"
        ),
        "distance",
      ]);
      geo_cond.push(
        sequelize.literal(
          "6371 * acos(cos(radians(" +
            parseFloat(lat) +
            ")) * cos(radians(lat)) * cos(radians(" +
            parseFloat(lng) +
            ") - radians(lng)) + sin(radians(" +
            parseFloat(lat) +
            ")) * sin(radians(lat))) <= 1"
        )
      );
    }

    let aeds = await AED.findAll({
      attributes: attributes,
      where: {
        status: "active",
        user_id: userIds,
        [Op.and]: geo_cond,
      },
      include: [{ association: "license" }],
    });
    return res.status(200).json(aeds);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get(
  "/user",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let aeds = await AED.findAll({
        where: { user_id: req.user.id },
        include: [{ association: "license" }],
      });
      return res.status(200).json(aeds);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let aed = await AED.findOne({
        where: { uuid: req.params.uuid },
        include: [{ association: "license" }, { association: "responders" }],
      });
      return res.status(200).json(aed);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.put(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      const validateResult = AEDSchema.validate(req.body, {
        abortEarly: false,
      });
      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));

      let aed = await AED.findOne({
        where: { uuid: req.params.uuid },
      });
      if (aed == null) return res.status(404).json("AED not found");

      let address = [];
      let lat = null;
      let lng = null;

      if (req.body.number != null) address.push(req.body.number);
      if (req.body.street != null) address.push(req.body.street);
      if (req.body.city != null) address.push(req.body.city);
      if (req.body.country != null) address.push(req.body.country);
      if (req.body.zipcode != null) address.push(req.body.zipcode);

      if (address.length) {
        address = address.join(",");
        const location = await geocoder.geocode(address);
        if (location[0] != undefined) {
          lat = location[0].latitude;
          lng = location[0].longitude;
        }
      }

      aed.update({
        brand: req.body.brand,
        model: req.body.model,
        serial: req.body.serial,
        reg: req.body.reg,
        purchase_date: req.body.purchase_date,
        first_pad: req.body.first_pad,
        first_pad_exp: req.body.first_pad_exp,
        second_pad: req.body.second_pad,
        second_pad_exp: req.body.second_pad_exp,
        battry_exp: req.body.battry_exp,
        battery_serial: req.body.battery_serial,
        street: req.body.street,
        number: req.body.number,
        number_addition: req.body.number_addition,
        zipcode: req.body.zipcode,
        city: req.body.city,
        country: req.body.country,
        lat: lat,
        lng: lng,
        lock_code: req.body.lock_code,
        building_id: req.body.building_id,
        availability: req.body.availability,
        locality: req.body.locality,
        visibility: req.body.visibility,
      });

      //Reset Responders
      await Responder.destroy({ where: { aed_id: aed.id } });
      if (req.body.responders != undefined) {
        for (let item of req.body.responders) {
          await Responder.create({
            aed_id: aed.id,
            name: item.name,
            email: item.email,
            phone: item.phone,
            sms_open_closed: item.sms_open_closed,
            sms_aed_present: item.sms_aed_present,
            sms_aed_status: item.sms_aed_status,
            sms_power: item.sms_power,
            sms_connection: item.sms_connection,
            sms_maintenance: item.sms_maintenance,
            email_open_closed: item.email_open_closed,
            email_aed_present: item.email_aed_present,
            email_aed_status: item.email_aed_status,
            email_power: item.email_power,
            email_connection: item.email_connection,
            email_maintenance: item.email_maintenance,
          });
        }
      }

      return res.status(200).json(aed);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.patch(
  "/uuid/:uuid/status",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let aed = await AED.findOne({ where: { uuid: req.params.uuid } });
      if (aed == null) return res.status(404).json("AED not found");

      aed.update({ status: req.body.status || aed.status });

      return res.status(200).json(aed);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.delete(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let aed = await AED.findOne({ where: { uuid: req.params.uuid } });
      if (aed == null) return res.status(404).json("AED not found");
      aed.destroy();
      return res.status(204).json(aed);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get(
  "/cabinet-code/check/:code",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let cabinet_code = await CabinnetCode.findOne({
        where: { code: req.params.code, isconnected: false },
      });
      if (cabinet_code == null)
        return res.status(400).json("Cabinet Code is invalid");
      return res.status(200).json(cabinet_code);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get(
  "/license-key/check/:license",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let license = await License.findOne({
        where: {
          license: req.params.license,
          status: "new",
          user_id: req.user.id,
        },
        include: [{ association: "product" }],
      });
      if (license == null)
        return res.status(400).json("License Key is invalid");

      return res.status(200).json(license);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

module.exports = router;
