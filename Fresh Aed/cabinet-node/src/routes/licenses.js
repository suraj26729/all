const router = require("express").Router();
const { License, ValSchema } = require("../models/License");
const passport = require("passport");
const { validationError } = require("../helpers/common");

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let licenses = [];
      if (req.user.role_id == 1) {
        licenses = await License.findAll({
          order: [["created_at", "DESC"]],
          include: [
            { association: "order" },
            { association: "product" },
            { association: "user" },
          ],
        });
      } else {
        licenses = await License.findAll({
          where: { user_id: req.user.id },
          order: [["created_at", "DESC"]],
          include: [{ association: "order" }, { association: "product" }],
        });
      }
      return res.status(200).json(licenses);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.delete(
  "/id/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let license = await License.findOne({ where: { id: req.params.id } });
      if (license == null) return res.status(404).json("Not Found");
      license.destroy();
      return res.status(204).json({});
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

module.exports = router;
