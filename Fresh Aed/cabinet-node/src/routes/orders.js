const router = require("express").Router();
const { Order, ValSchema, RegValSchema } = require("../models/Order");
const { License } = require("../models/License");
const passport = require("passport");
const { validationError } = require("../helpers/common");
const mollie = require("../config/mollie");
const serial = require("generate-serial-key");
const bcrypt = require("bcryptjs");
const { User } = require("../models/User");
const jwt = require("jsonwebtoken");

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      const validateResult = ValSchema.validate(req.body, {
        abortEarly: false,
      });
      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));

      let order = await Order.create(req.body);
      if (order == null)
        return res.status(400).json("Problem while creation order");

      const payment = await mollie.payments.create({
        amount: {
          currency: "EUR",
          value: order.total,
        },
        description: `Order #${order.id}`,
        redirectUrl: `${process.env.SITE_BASE_URL}/orders/${order.uuid}/success`,
        webhookUrl: `${process.env.MOLLIE_WEBHOOK_URL}/api/v1/orders/webhook`,
        metadata: {
          order_id: order.id,
        },
      });

      if (payment != null)
        order.update({ payment_data: JSON.stringify(payment) });

      return res.status(201).json({ order, payment });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

const generateLicenseKey = async () => {
  let license_key = null;
  let exists = null;

  do {
    license_key = serial.generate(25, "-", 5);
    license_key = license_key.substr(1, license_key.length);
    license_key = "L" + license_key;
    exists = await License.findAll({ where: { license: license_key } });
  } while (exists.length);

  return license_key;
};

router.post("/webhook", async (req, res, next) => {
  try {
    let { id } = req.body;
    if (id != undefined) {
      let payment = await mollie.payments.get(id);
      if (payment != null && payment.metadata.order_id != undefined) {
        let order_id = payment.metadata.order_id;
        let order = await Order.findByPk(order_id, {
          include: [{ association: "product" }],
        });
        if (order != null) {
          order.update({
            status: payment.status,
            payment_data: JSON.stringify(payment),
          });
          let order_licenses = await License.findAll({
            where: { order_id: order.id },
          });
          if (payment.status == "paid" && order_licenses.length == 0) {
            let _orders_loop = Array(order.no_license)
              .fill(0)
              .map((i) => i);
            for (let loop of _orders_loop) {
              let license_key = await generateLicenseKey();
              let license = await License.create({
                license: license_key,
                recurring_duration: order.product.recurring_duration,
                order_id: order.id,
                product_id: order.product_id,
                user_id: order.user_id,
              });
            }
          }
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
});

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let orders = [];
      if (req.user.role_id == 1) {
        orders = await Order.findAll({
          order: [["created_at", "DESC"]],
          include: [{ association: "product" }, { association: "user" }],
        });
      } else {
        orders = await Order.findAll({
          where: { user_id: req.user.id },
          order: [["created_at", "DESC"]],
          include: [{ association: "product" }],
        });
      }
      return res.status(200).json(orders);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let order = await Order.findOne({
        where: { uuid: req.params.uuid },
        include: [
          { association: "product" },
          { association: "licenses" },
          { association: "user" },
        ],
      });
      return res.status(200).json(order);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.patch(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let order = await Order.findOne({ where: { uuid: req.params.uuid } });
      if (order == null) return res.status(404).json("Not Found");
      order.update(req.body);
      return res.status(200).json(order);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.delete(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let order = await Order.findOne({ where: { uuid: req.params.uuid } });
      if (order == null) return res.status(404).json("Not Found");
      order.destroy();
      return res.status(204).json({});
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.post("/with-register", async (req, res, next) => {
  try {
    const validateResult = RegValSchema.validate(req.body, {
      abortEarly: false,
    });
    if (validateResult.error)
      return res.status(422).json(validationError(validateResult));

    let exists = await User.findOne({
      where: { email: req.body.billing_email },
    });
    if (exists != null)
      return res.status(400).json("Email is already registered");

    req.body.password = bcrypt.hashSync(req.body.password, 10);

    let user = await User.create({
      email: req.body.billing_email,
      password: req.body.password,
      firstname: req.body.billing_firstname,
      lastname: req.body.billing_lastname,
      gender: req.body.gender,
      phone: req.body.billing_phone,
      organization: req.body.billing_organization,
      street: req.body.billing_street,
      home_num: req.body.billing_home_num,
      addition_num: req.body.billing_addition_num,
      zip: req.body.billing_zip,
      city: req.body.billing_city,
      country: "Netherlands",
      status: 1,
      role_id: 2,
      email_verify_token: null,
    });
    if (user == null)
      return res.status(400).json("Problem while creating user");

    let order = await Order.create({
      product_id: req.body.product_id,
      no_license: req.body.no_license,
      billing_firstname: req.body.billing_firstname,
      billing_lastname: req.body.billing_lastname,
      billing_email: req.body.billing_email,
      billing_phone: req.body.billing_phone,
      billing_organization: req.body.billing_organization,
      billing_street: req.body.billing_street,
      billing_home_num: req.body.billing_home_num,
      billing_addition_num: req.body.billing_addition_num,
      billing_zip: req.body.billing_zip,
      billing_city: req.body.billing_city,
      total: req.body.total,
      user_id: user.id,
    });
    if (order == null)
      return res.status(400).json("Problem while creation order");

    const payment = await mollie.payments.create({
      amount: {
        currency: "EUR",
        value: order.total,
      },
      description: `Order #${order.id}`,
      redirectUrl: `${process.env.SITE_BASE_URL}/orders/${order.uuid}/success`,
      webhookUrl: `${process.env.MOLLIE_WEBHOOK_URL}/api/v1/orders/webhook`,
      metadata: {
        order_id: order.id,
      },
    });

    if (payment != null)
      order.update({ payment_data: JSON.stringify(payment) });

    const token = jwt.sign(
      {
        iss: process.env.APP_NAME,
        iat: new Date().getTime(),
        sub: user,
      },
      process.env.JWT_SECRET,
      { expiresIn: "7d" }
    );

    return res.status(201).json({
      order,
      payment,
      auth: {
        token: token,
        token_type: "Bearer",
        role_id: user.role_id,
        user: user,
      },
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/get-sample-license-key", async (req, res, next) => {
  let license_key = await generateLicenseKey();
  return res.json(license_key);
});
module.exports = router;
