const router = require("express").Router();
const { User, ValSchema } = require("../models/User");
const { validationError } = require("../helpers/common");
const bcrypt = require("bcryptjs");
const passport = require("passport");
const { AED } = require("../models/AED");
const { Order } = require("../models/Order");
const { License } = require("../models/License");
const sequelize = require("sequelize");
const axios = require("axios");

router.get(
  "/id/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let user = await User.findByPk(req.params.id);
      return res.status(200).json(user);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.put(
  "/id/:id",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let user = await User.findByPk(req.params.id);
      if (user == null) return res.status(404).json(user);

      if (req.body.password != "" && req.body.confirm_password != "")
        req.body.password = bcrypt.hashSync(req.body.password, 10);
      else delete req.body.password;

      user.update(req.body);

      return res.status(200).json(user);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      if (req.user.role_id != 1) return res.status(409).json("Unauthorized");

      let users = await User.findAll({ where: { role_id: 2 } });
      return res.status(200).json(users);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.patch(
  "/id/:id/status",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      if (req.user.role_id != 1) return res.status(409).json("Unauthorized");
      let user = await User.findByPk(req.params.id);
      user.update({ status: user.status ? 0 : 1 });
      return res.status(200).json(user);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get(
  "/id/:id/dashboard-summary",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let user = await User.findByPk(req.params.id);
      if (user.role_id == 2) {
        let aeds = await AED.findAll({ where: { user_id: req.params.id } });
        let license = await License.findAll({
          where: {
            user_id: req.params.id,
            status: { [sequelize.Op.notIn]: ["expired"] },
          },
        });
        let orders = await Order.findAll({ where: { user_id: req.params.id } });
        let total_sales = 0;
        if (orders.length) {
          for (let item of orders) {
            if (item.status == "paid") total_sales += item.total;
          }
        }
        return res.status(200).json({
          total_aeds: aeds.length,
          total_license: license.length,
          total_orders: orders.length,
          total_sales: total_sales,
        });
      } else {
        let aeds = await AED.findAll();
        let license = await License.findAll();
        let orders = await Order.findAll();
        let total_sales = 0;
        if (orders.length) {
          for (let item of orders) {
            if (item.status == "paid") total_sales += item.total;
          }
        }
        return res.status(200).json({
          total_aeds: aeds.length,
          total_license: license.length,
          total_orders: orders.length,
          total_sales: total_sales,
        });
      }
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

module.exports = router;
