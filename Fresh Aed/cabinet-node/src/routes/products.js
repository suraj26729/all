const router = require("express").Router();
const { Product, ValSchema } = require("../models/Product");
const passport = require("passport");
const { validationError } = require("../helpers/common");

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      const validateResult = ValSchema.validate(req.body, {
        abortEarly: false,
      });
      if (validateResult.error)
        return res.status(422).json(validationError(validateResult));

      let product = await Product.create(req.body);
      return res.status(201).json(product);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get("/", async (req, res, next) => {
  try {
    let products = await Product.findAll({
      where: { status: "active" },
      order: [
        ["regular_price", "ASC"],
        ["sale_price", "ASC"],
      ],
    });
    return res.status(200).json(products);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/all", async (req, res, next) => {
  try {
    let products = await Product.findAll({
      order: [
        ["regular_price", "ASC"],
        ["sale_price", "ASC"],
      ],
    });
    return res.status(200).json(products);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/uuid/:uuid", async (req, res, next) => {
  try {
    let product = await Product.findOne({ where: { uuid: req.params.uuid } });
    return res.status(200).json(product);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.put(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let product = await Product.findOne({ where: { uuid: req.params.uuid } });
      if (product == null) return res.status(404).json("not found");

      product.update(req.body);

      return res.status(200).json(product);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.delete(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let product = await Product.findOne({ where: { uuid: req.params.uuid } });
      if (product == null) return res.status(404).json("not found");

      product.destroy();

      return res.status(204).json({});
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.patch(
  "/uuid/:uuid",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let product = await Product.findOne({ where: { uuid: req.params.uuid } });
      if (product == null) return res.status(404).json("not found");

      product.update({ status: req.body.status });

      return res.status(200).json(product);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.post(
  "/bulk-get",
  passport.authenticate("jwt", { session: false }),
  async (req, res, next) => {
    try {
      let products = await Product.findAll({ where: { id: req.body.id } });
      return res.status(200).json(products);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

module.exports = router;
