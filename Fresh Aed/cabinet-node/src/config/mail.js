const nodemailer = require("nodemailer");
const uuid = require("uuid");

let transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: process.env.MAIL_PORT,
  auth: {
    user: process.env.MAIL_USERNAME,
    pass: process.env.MAIL_PASSWORD,
  },
});

exports.send = async (to, subject, message, attachments, next) => {
  try {
    const mailOptions = {
      from: `${process.env.MAIL_FROM_NAME} <${process.env.MAIL_FROM_ADDRESS}>`,
      to: to,
      subject: subject,
      generateTextFromHTML: true,
      html: message,
      attachments: attachments,
    };

    let result = await transporter.sendMail(mailOptions);

    transporter.close();
    return result;
  } catch (error) {
    next(error);
  }
};

exports.sendEmailVerification = async (user) => {
  try {
    let token = uuid.v1();
    user.update({ email_verify_token: token });
    let message = `
        Hi ${user.firstname} ${user.lastname},<br/><br/>

        Please confirm your email to start using our Platform <br/><br/>

        <a href="${process.env.SITE_BASE_URL}/auth/email-verify/${token}">${process.env.SITE_BASE_URL}/auth/email-verify/${token}</a>
    `;

    const mailOptions = {
      from: `${process.env.MAIL_FROM_NAME} <${process.env.MAIL_FROM_ADDRESS}>`,
      to: user.email,
      subject: `${process.env.APP_NAME}: Confirm Email`,
      generateTextFromHTML: true,
      html: message,
    };

    let result = await transporter.sendMail(mailOptions);

    // console.log(result);

    transporter.close();
    return result;
  } catch (error) {
    console.log(error);
  }
};

exports.sendPasswordResetEmail = async (user) => {
  try {
    let token = uuid.v1();
    user.update({ password_reset_token: token });
    let message = `
          Hi ${user.firstname} ${user.lastname},<br/><br/>
  
          Please click the below link to reset your password <br/><br/>
  
          <a href="${process.env.SITE_BASE_URL}/auth/reset-password/${token}">${process.env.SITE_BASE_URL}/auth/reset-password/${token}</a>
      `;

    const mailOptions = {
      from: `${process.env.MAIL_FROM_NAME} <${process.env.MAIL_FROM_ADDRESS}>`,
      to: user.email,
      subject: `${process.env.APP_NAME}: Reset Password`,
      generateTextFromHTML: true,
      html: message,
    };

    let result = await transporter.sendMail(mailOptions);

    transporter.close();
    return result;
  } catch (error) {
    console.log(error);
  }
};

exports.sendEmailVerificationCode = async (user) => {
  try {
    let token = uuid.v1();
    user.update({ email_verify_token: token });
    let message = `
        Hi ${user.firstname} ${user.lastname},<br/><br/>

        Here is your token <br/><br/>

        <strong>${token}</strong>
    `;

    const mailOptions = {
      from: `${process.env.MAIL_FROM_NAME} <${process.env.MAIL_FROM_ADDRESS}>`,
      to: user.email,
      subject: `${process.env.APP_NAME}: Confirm Email`,
      generateTextFromHTML: true,
      html: message,
    };

    let result = await transporter.sendMail(mailOptions);

    // console.log(result);

    transporter.close();
    return result;
  } catch (error) {
    console.log(error);
  }
};
