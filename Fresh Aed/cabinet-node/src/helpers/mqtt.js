const mqtt = require("mqtt");
const { CabinnetCode } = require("../models/Master");
const { AED } = require("../models/AED");

exports.connect = async () => {
  const client = mqtt.connect(process.env.MQTT_HOST, {
    port: process.env.MQTT_PORT,
    username: process.env.MQTT_USERNAME,
    password: process.env.MQTT_PASSWORD,
  });
  let cabinet_codes = await CabinnetCode.findAll({ where: { isconnected: 1 } });
  const codes = cabinet_codes.map((item) => item.code);

  client.on("connect", () => {
    console.log(
      `MQTT Server connected with ${process.env.MQTT_HOST}:${process.env.MQTT_PORT}`
    );
    for (const element of codes) {
      //Subscribe AED device channel
      client.subscribe(`aed/devices/${element}/AED_state`, (err) => {
        if (err) {
          console.log(err);
          return;
        }
      });
      client.subscribe(`aed/devices/${element}/temperature`, (err) => {
        if (err) {
          console.log(err);
          return;
        }
      });
      client.subscribe(`aed/devices/${element}/door_state`, (err) => {
        if (err) {
          console.log(err);
          return;
        }
      });
    }
  });

  client.on("error", (err) => {
    console.log(err);
  });

  client.on("message", async (topic, message) => {
    let res = topic.slice(12);
    let spl = [];
    spl = res.split("/");
    console.log(spl);
    let aed = await AED.findOne({ where: { cabinet_code: spl[0] } });
    if (spl[1] === "door_state") {
      let status = message == 1 ? 1 : 0;
      aed.update({ open_closed: status });
    }
    if (spl[1] === "AED_state") {
      let status = message == 1 ? 1 : 0;
      aed.update({ present: status });
      // console.log(res, message.toString());
    }
  });

  client.on("close", () => {
    console.log("MQTT Server is disconnected");
  });
};
