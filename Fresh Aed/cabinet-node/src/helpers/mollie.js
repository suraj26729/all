const mollie = require("../config/mollie");
const { Product } = require("../models/Product");

exports.createOrder = async (order) => {
  let product = await Product.findByPk(order.product_id);
  return await mollie.orders.create({
    amount: { currency: "EUR", value: order.total },
    orderNumber: order.id,
    lines: [
      {
        type: "digital",
        name: product.name,
        quantity: order.no_license,
        unitPrice: {
          currency: "EUR",
          value: product.sale_price
            ? product.sale_price
            : product.regular_price,
        },
        totalAmount: { currency: "EUR", value: order.total },
        vatRate: "0",
        vatAmount: { currency: "EUR", value: "0" },
      },
    ],
    billingAddress: {
      organizationName: order.billing_organization,
      givenName: order.billing_firstname,
      familyName: order.billing_lastname,
      email: order.billing_email,
      phone: order.billing_phone,
      streetAndNumber: order.billing_street,
      postalCode: order.billing_zip,
      city: order.billing_city,
      country: "NL",
    },
    redirectUrl: `${process.env.SITE_BASE_URL}/shop/checkout/success`,
    locale: "en_US",
    metadata: { order_id: order.id },
  });
};
