import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { CorporateLayout } from "./@pages/layouts";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { AuthGuard } from "./_guards/auth.guard";
import { AEDListComponent } from "./modules/aed/aedlist/aedlist.component";

export const AppRoutes: Routes = [
  {
    path: "",
    redirectTo: "/auth/login",
    pathMatch: "full",
  },
  {
    path: "auth",
    loadChildren: "./modules/auth/auth.module#AuthModule",
    data: {
      title: "Login",
    },
  },
  {
    path: "",
    component: CorporateLayout,
    children: [
      {
        path: "dashboard",
        component: DashboardComponent,
      },
      {
        path: "aeds",
        loadChildren: "./modules/aed/aed.module#AedModule",
      },
      {
        path: "users",
        loadChildren: "./modules/user/user.module#UserModule",
      },
      {
        path: "products",
        loadChildren: "./modules/products/products.module#ProductsModule",
      },
      {
        path: "shop",
        loadChildren:
          "./modules/shopping-cart/shopping-cart.module#ShoppingCartModule",
      },
      {
        path: "orders",
        loadChildren: "./modules/orders/orders.module#OrdersModule",
      },
      {
        path: "licenses",
        loadChildren: "./modules/licenses/licenses.module#LicensesModule",
      },
    ],
    canActivate: [AuthGuard],
  },
  {
    path: "**",
    redirectTo: "/dashboard",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(AppRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
