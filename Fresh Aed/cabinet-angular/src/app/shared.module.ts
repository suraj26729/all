import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { NotifierModule } from "angular-notifier";
import { DataTablesModule } from "angular-datatables";
import { Select2Module } from "ng2-select2";
import { pgDatePickerModule } from "./@pages/components/datepicker/datepicker.module";
import { pgSelectfx } from "./@pages/components/cs-select/select.module";
import { pgSelectModule } from "./@pages/components/select/select.module";
import { AgmCoreModule } from "@agm/core";
import { CKEditorModule } from "ckeditor4-angular";
import { environment } from "../environments/environment.prod";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    NotifierModule,
    Select2Module,
    pgDatePickerModule,
    pgSelectModule,
    pgSelectfx,
    AgmCoreModule.forRoot({
      apiKey: environment.mapApiKey,
      libraries: ["places"],
    }),
    CKEditorModule,
  ],
  declarations: [],
  exports: [
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    NotifierModule,
    DataTablesModule,
    Select2Module,
    pgDatePickerModule,
    pgSelectfx,
    pgSelectModule,
    AgmCoreModule,
    CKEditorModule,
  ],
})
export class SharedModule {}
