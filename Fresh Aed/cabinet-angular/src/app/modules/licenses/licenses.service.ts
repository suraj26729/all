import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class LicensesService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  get getAll() {
    return this.http.get(`${environment.serviceURL}/licenses`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }
  delete(id) {
    return this.http.delete(`${environment.serviceURL}/licenses/id/${id}`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }
}
