import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LicensesListComponent } from "./licenses-list/licenses-list.component";

const routes: Routes = [
  {
    path: "",
    component: LicensesListComponent,
    data: { title: "Licenses", breadcrumb: "Licenses" },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LicensesRoutingModule {}
