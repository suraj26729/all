import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared.module";
import { LicensesListComponent } from "./licenses-list/licenses-list.component";
import { LicensesRoutingModule } from "./licenses-routing.module";

@NgModule({
  imports: [CommonModule, SharedModule, LicensesRoutingModule],
  declarations: [LicensesListComponent],
})
export class LicensesModule {}
