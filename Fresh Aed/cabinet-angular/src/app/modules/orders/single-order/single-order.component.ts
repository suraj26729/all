import { Component, OnInit } from "@angular/core";
import { OrdersService } from "../orders.service";
import { NotifierService } from "angular-notifier";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "../../auth/auth.service";

@Component({
  selector: "app-single-order",
  templateUrl: "./single-order.component.html",
  styleUrls: ["./single-order.component.scss"],
})
export class SingleOrderComponent implements OnInit {
  order: any;
  loadingPage = true;
  formLoading = false;
  currentUserRole: any;

  constructor(
    private api: OrdersService,
    private notify: NotifierService,
    private route: ActivatedRoute,
    private auth: AuthService
  ) {}

  ngOnInit() {
    this.currentUserRole = this.auth.currentUserRole;
    this.fetch();
  }

  fetch() {
    this.loadingPage = true;
    this.api.get(this.route.snapshot.params.uuid).subscribe(
      (order) => {
        this.order = order;
        this.loadingPage = false;
      },
      (err) => console.log(err)
    );
  }

  update() {
    this.formLoading = true;
    this.api
      .update({ status: this.order.status }, this.route.snapshot.params.uuid)
      .subscribe(
        (res) => {
          this.formLoading = false;
          this.notify.show({
            type: "success",
            message: "Order status changed",
          });
        },
        (err) => console.log(err)
      );
  }
}
