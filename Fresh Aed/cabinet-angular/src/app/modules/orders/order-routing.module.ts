import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { OrderListComponent } from "./order-list/order-list.component";
import { SingleOrderComponent } from "./single-order/single-order.component";
import { OrderSuccessComponent } from "./order-success/order-success.component";

const routes: Routes = [
  {
    path: "",
    component: OrderListComponent,
    data: { title: "Orders", breadcrumb: "Orders" },
  },
  {
    path: ":uuid",
    component: SingleOrderComponent,
    data: { title: "Order Details", breadcrumb: "Order Details" },
  },
  {
    path: ":uuid/success",
    component: OrderSuccessComponent,
    data: { title: "Order Success", breadcrumb: "Order Success" },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderRoutingModule {}
