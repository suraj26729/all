import { Component, OnInit } from "@angular/core";
import { OrdersService } from "../orders.service";
import { NotifierService } from "angular-notifier";
import { AuthService } from "../../auth/auth.service";

@Component({
  selector: "app-order-list",
  templateUrl: "./order-list.component.html",
  styleUrls: ["./order-list.component.scss"],
})
export class OrderListComponent implements OnInit {
  list: any = [];
  loadingPage = true;
  success = false;
  currentUserRole: any;

  constructor(
    private api: OrdersService,
    private notify: NotifierService,
    private auth: AuthService
  ) {}

  ngOnInit() {
    this.currentUserRole = this.auth.currentUserRole;
    this.fetch();
  }

  fetch() {
    this.loadingPage = true;
    this.api.getAll.subscribe(
      (data) => {
        this.list = data;
        this.loadingPage = false;
        this.success = true;
      },
      (err) => console.log(err)
    );
  }

  delete(uuid) {
    let result = confirm("Are you sure to delete ?");
    if (result) {
      this.api.delete(uuid).subscribe(
        (res) => {
          this.notify.show({
            type: "info",
            message: "Order has been removed",
          });
          setTimeout(() => {
            this.fetch();
          }, 1000);
        },
        (err: any) => this.notify.show({ type: "error", message: err.error })
      );
    }
  }
}
