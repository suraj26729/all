import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class OrdersService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  get getAll() {
    return this.http.get(`${environment.serviceURL}/orders`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  get(uuid) {
    return this.http.get(`${environment.serviceURL}/orders/uuid/${uuid}`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  create(data) {
    return this.http.post(`${environment.serviceURL}/orders`, data, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  update(data, uuid) {
    return this.http.patch(
      `${environment.serviceURL}/orders/uuid/${uuid}`,
      data,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }

  delete(uuid) {
    return this.http.delete(`${environment.serviceURL}/orders/uuid/${uuid}`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }
}
