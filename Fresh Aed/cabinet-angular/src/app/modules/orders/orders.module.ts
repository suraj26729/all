import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared.module";
import { OrderListComponent } from "./order-list/order-list.component";
import { SingleOrderComponent } from "./single-order/single-order.component";
import { OrderRoutingModule } from "./order-routing.module";
import { OrderSuccessComponent } from './order-success/order-success.component';

@NgModule({
  imports: [CommonModule, OrderRoutingModule, SharedModule],
  declarations: [OrderListComponent, SingleOrderComponent, OrderSuccessComponent],
})
export class OrdersModule {}
