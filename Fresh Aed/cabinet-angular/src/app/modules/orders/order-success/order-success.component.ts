import { Component, OnInit } from "@angular/core";
import { OrdersService } from "../orders.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-order-success",
  templateUrl: "./order-success.component.html",
  styleUrls: ["./order-success.component.scss"],
})
export class OrderSuccessComponent implements OnInit {
  order: any;
  loadingPage = true;

  constructor(private api: OrdersService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.fetch();
  }

  fetch() {
    this.loadingPage = true;
    this.api.get(this.route.snapshot.params.uuid).subscribe(
      (order) => {
        this.order = order;
        this.loadingPage = false;
      },
      (err) => console.log(err)
    );
  }
}
