import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthRoutingModule } from "./auth-routing.module";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { BlankCorporateComponent } from "../../@pages/layouts";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { SharedModule } from "../../shared.module";
import { VerifyEmailComponent } from './register/verify-email/verify-email.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RegisterCheckoutComponent } from './register-checkout/register-checkout.component';

@NgModule({
  imports: [CommonModule, AuthRoutingModule, SharedModule],
  declarations: [
    BlankCorporateComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    ResetPasswordComponent,
    RegisterCheckoutComponent
  ]
})
export class AuthModule {}
