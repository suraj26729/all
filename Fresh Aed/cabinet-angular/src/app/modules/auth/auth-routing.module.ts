import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { BlankCorporateComponent } from "../../@pages/layouts";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { VerifyEmailComponent } from "./register/verify-email/verify-email.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
import { RegisterCheckoutComponent } from "./register-checkout/register-checkout.component";

const routes: Routes = [
  {
    path: "",
    component: BlankCorporateComponent,
    children: [
      { path: "login", component: LoginComponent, data: { title: "Login" } },
      {
        path: "register/:product_uuid/checkout",
        component: RegisterCheckoutComponent,
        data: { title: "Checkout" },
      },
      {
        path: "register",
        component: RegisterCheckoutComponent,
        data: { title: "Register" },
      },
      {
        path: "forgot-password",
        component: ForgotPasswordComponent,
        data: { title: "Forgot Password" },
      },
      {
        path: "email-verify/:token",
        component: VerifyEmailComponent,
        data: { title: "Verify Email" },
      },
      {
        path: "reset-password/:token",
        component: ResetPasswordComponent,
        data: { title: "Reset Password" },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
