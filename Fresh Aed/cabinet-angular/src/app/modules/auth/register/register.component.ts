import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../auth.service";
import { NotifierService } from "angular-notifier";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  RegForm: FormGroup;
  errormsg: "";
  cities: any = [];
  submitted = false;
  ematchError = "";
  matchError = "";
  success = false;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private api: AuthService,
    private notify: NotifierService
  ) {}

  ngOnInit() {
    this.getcity();
    this.setForm();
  }

  getcity() {
    this.api.cities.subscribe(
      (cities) => (this.cities = cities),
      (err) => {
        console.log(err);
      }
    );
  }

  setForm() {
    this.RegForm = this.fb.group(
      {
        firstname: ["", Validators.required],
        lastname: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        confirm_email: ["", [Validators.required, Validators.email]],
        password: [
          "",
          [
            Validators.required,
            Validators.pattern(
              "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!%^&*#@$?]).{8,}$"
            ),
          ],
        ],
        confirm_password: [
          "",
          [
            Validators.required,
            Validators.pattern(
              "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!%^&*#@$?]).{8,}$"
            ),
          ],
        ],
        phone: ["", Validators.required],
        home_num: [""],
        addition_num: [""],
        street: ["", Validators.required],
        city: ["", Validators.required],
        country: ["Netherlands"],
        zip: ["", Validators.required],
        organization: ["", Validators.required],
        gender: ["", Validators.required],
        newsletter: [""],
      },
      { validator: this.checkMatch }
    );
  }

  checkMatch(group: FormGroup) {
    let pass = group.get("password").value;
    let confirmPass = group.get("confirm_password").value;
    return pass === confirmPass ? null : { confirmedValidator: true };
  }

  get f() {
    return this.RegForm.controls;
  }

  checkEmail() {
    var form = this.RegForm.value;
    var email = form.email;
    var c_email = form.confirm_email;
    if (email === null) {
      this.ematchError = "Email Empty";
    } else if (c_email !== email) {
      this.ematchError = "Email Not Matched";
    } else {
      this.ematchError = "";
    }
  }

  submit() {
    this.submitted = true;
    if (this.RegForm.invalid) return;

    this.loading = true;

    let data = this.RegForm.value;

    delete data.confirm_email;
    delete data.confirm_password;
    delete data.newsletter;

    data["role_id"] = 2;

    this.api.register(this.RegForm.value).subscribe(
      (res) => {
        this.notify.show({
          type: "success",
          message: "Please check email to confirm your account",
        });
        this.success = true;
      },
      (err) => {
        this.notify.show({ type: "error", message: err.error });
        this.loading = false;
      }
    );
  }
}
