import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import { NotifierService } from "angular-notifier";
import { environment } from "../../../../../environments/environment.prod";

@Component({
  selector: "app-verify-email",
  templateUrl: "./verify-email.component.html",
  styleUrls: ["./verify-email.component.scss"]
})
export class VerifyEmailComponent implements OnInit {
  siteURL = environment.siteURL;
  success = false;

  constructor(
    private api: AuthService,
    private route: ActivatedRoute,
    private notify: NotifierService,
    private router: Router
  ) {}

  ngOnInit() {
    this.api.verfifyUser(this.route.snapshot.params.token).subscribe(
      res => {
        this.success = true;
        this.notify.show({
          type: "success",
          message: "Verification Successfull"
        });
      },
      err => {
        this.notify.show({
          type: "error",
          message: err.error
        });
        this.router.navigate(["/auth/login"]);
      }
    );
  }
}
