import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../auth.service";
import { ProductsService } from "../../products/products.service";
import { NotifierService } from "angular-notifier";
import { ShoppingCartService } from "../../shopping-cart/shopping-cart.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-register-checkout",
  templateUrl: "./register-checkout.component.html",
  styleUrls: ["./register-checkout.component.scss"],
})
export class RegisterCheckoutComponent implements OnInit {
  products: any = [];
  loadingProducts = true;
  loading = false;

  checkout = false;
  product: any;
  quantity = 1;
  subtotal: any = 0.0;
  total: any = 0.0;
  form: FormGroup;
  cities: any = [];
  licenses: any = [];
  submitted = false;
  formLoading = false;
  matchError = "";

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private productsApi: ProductsService,
    private shopping: ShoppingCartService,
    private route: ActivatedRoute,
    private notify: NotifierService,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.route.snapshot.params.product_uuid == undefined) {
      this.checkout = false;
      this.auth.getPackages.subscribe(
        (products) => (this.products = products),
        (err) => console.log(err),
        () => {
          this.loadingProducts = false;
        }
      );
    } else {
      if (localStorage.token != undefined)
        this.router.navigate([
          `/shop/${this.route.snapshot.params.product_uuid}/checkout`,
        ]);
      this.checkout = true;
      this.form = this.fb.group(
        {
          product_id: "",
          no_license: "",
          gender: ["", Validators.required],
          billing_firstname: ["", Validators.required],
          billing_lastname: ["", Validators.required],
          billing_email: ["", Validators.required],
          billing_phone: ["", Validators.required],
          password: [
            "",
            [
              Validators.required,
              Validators.pattern(
                "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!%^&*#@$?]).{8,}$"
              ),
            ],
          ],
          confirm_password: [
            "",
            [
              Validators.required,
              Validators.pattern(
                "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!%^&*#@$?]).{8,}$"
              ),
            ],
          ],
          billing_organization: [""],
          billing_street: ["", Validators.required],
          billing_home_num: [""],
          billing_addition_num: [""],
          billing_zip: ["", Validators.required],
          billing_city: ["", Validators.required],
          total: 0,
        },
        { validator: this.checkMatch }
      );
      this.auth.cities.subscribe((cities) => (this.cities = cities));
      this.auth
        .getOnePackage(this.route.snapshot.params.product_uuid)
        .subscribe(
          (product: any) => {
            this.product = product;
            this.subtotal = product.sale_price
              ? product.sale_price
              : product.regular_price;
            this.licenses = Array(parseInt(product.no_license) + 5)
              .fill(0)
              .map((x, i) => i + parseInt(product.no_license));
            this.total = (
              parseFloat(this.subtotal) * parseInt(product.no_license)
            ).toFixed(2);

            this.form.patchValue({
              product_id: product.id,
              no_license: product.no_license,
              total: this.total,
            });
          },
          (err) => console.log(err),
          () => {
            this.loading = false;
          }
        );
    }
  }

  checkMatch(group: FormGroup) {
    let pass = group.get("password").value;
    let confirmPass = group.get("confirm_password").value;
    return pass === confirmPass ? null : { confirmedValidator: true };
  }

  updateTotal(event) {
    this.total = (
      parseFloat(this.subtotal) * parseInt(event.target.value)
    ).toFixed(2);
    this.form.patchValue({
      no_license: parseInt(event.target.value),
      total: this.total,
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.formLoading = true;

    let data = this.form.value;

    delete data.confirm_email;
    delete data.confirm_password;

    this.auth.registerWithOrder(this.form.value).subscribe(
      (res: any) => {
        localStorage.clear();
        localStorage.setItem(
          "token",
          `${res.auth.token_type} ${res.auth.token}`
        );
        localStorage.setItem("userData", JSON.stringify(res.auth.user));
        window.location.href = res.payment._links.checkout.href;
      },
      (err: any) => {
        this.notify.show({ type: "error", message: err.error });
        this.formLoading = false;
      }
    );
  }
}
