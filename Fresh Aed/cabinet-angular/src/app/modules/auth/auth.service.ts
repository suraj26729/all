import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private http: HttpClient) {}

  get brands() {
    return this.http.get(`${environment.serviceURL}/masters/brands`);
  }
  models(brand) {
    return this.http.get(
      `${environment.serviceURL}/masters/brand/${brand}/models`
    );
  }
  get cities() {
    return this.http.get(`${environment.serviceURL}/masters/cities`);
  }

  register(data) {
    return this.http.post(`${environment.serviceURL}/auth/register`, data);
  }

  login(data) {
    return this.http.post(`${environment.serviceURL}/auth/login`, data);
  }

  get currentUser() {
    return JSON.parse(localStorage.userData);
  }

  get currentUserID() {
    let userData = JSON.parse(localStorage.userData);
    return userData.id;
  }

  get currentUserRole() {
    let userData = JSON.parse(localStorage.userData);
    return userData.role_id;
  }

  verfifyUser(token) {
    return this.http.patch(
      `${environment.serviceURL}/auth/verifiy/${token}`,
      {}
    );
  }

  sendPasswordReset(data) {
    return this.http.post(
      `${environment.serviceURL}/auth/forgot-password`,
      data
    );
  }

  resetPassword(data, token) {
    return this.http.patch(
      `${environment.serviceURL}/auth/reset-password/${token}`,
      data
    );
  }

  sendEmailVerifyToken(email) {
    return this.http.post(
      `${environment.serviceURL}/auth/register/send-token`,
      { email }
    );
  }

  get getPackages() {
    return this.http.get(`${environment.serviceURL}/products`);
  }

  getOnePackage(uuid) {
    return this.http.get(`${environment.serviceURL}/products/uuid/${uuid}`);
  }

  registerWithOrder(data) {
    return this.http.post(
      `${environment.serviceURL}/orders/with-register`,
      data
    );
  }
}
