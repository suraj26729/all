import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "../auth.service";
import { NotifierService } from "angular-notifier";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  redirect: any = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: AuthService,
    private fb: FormBuilder,
    private notify: NotifierService
  ) {
    if (localStorage.token) this.router.navigate(["/dashboard"]);
  }

  ngOnInit() {
    this.form = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required]
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) {
      this.notify.show({
        type: "error",
        message: "Please check the data validations in the form"
      });
      return;
    }

    this.loading = true;

    this.api.login(this.form.value).subscribe(
      (res: any) => {
        localStorage.clear();
        localStorage.setItem("token", `${res.token_type} ${res.token}`);
        localStorage.setItem("userData", JSON.stringify(res.user));
        if (this.route.snapshot.queryParams.returnUrl != undefined)
          this.router.navigate([this.route.snapshot.queryParams.returnUrl]);
        // location.replace(this.route.snapshot.queryParams.returnUrl);
        else this.router.navigate(["/dashboard"]);
        // else location.replace("/dashboard");
      },
      (err: any) => {
        this.notify.show({ type: "error", message: err.error });
        this.loading = false;
      }
    );
  }
}
