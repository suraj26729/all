import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import { NotifierService } from "angular-notifier";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.scss"],
})
export class ResetPasswordComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private api: AuthService,
    private fb: FormBuilder,
    private notify: NotifierService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    if (localStorage.token) this.router.navigate(["/dashboard"]);
  }

  ngOnInit() {
    this.form = this.fb.group(
      {
        email: ["", [Validators.required, Validators.email]],
        password: [
          "",
          [
            Validators.required,
            Validators.pattern(
              "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!%^&*#@$?]).{8,}$"
            ),
          ],
        ],
        confirm_password: [
          "",
          [
            Validators.required,
            Validators.pattern(
              "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!%^&*#@$?]).{8,}$"
            ),
          ],
        ],
      },
      { validator: this.checkPasswords }
    );
  }

  get f() {
    return this.form.controls;
  }

  checkPasswords(group: FormGroup) {
    let pass = group.get("password").value;
    let confirmPass = group.get("confirm_password").value;

    return pass === confirmPass ? null : { notSame: true };
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) return;

    this.loading = true;
    this.api
      .resetPassword(this.form.value, this.route.snapshot.params.token)
      .subscribe(
        (res: any) => {
          this.notify.show({
            type: "success",
            message: res,
          });
          this.loading = false;
          // this.modalRef.hide();
          this.router.navigate(["/login"]);
        },
        (err) => {
          this.notify.show({ type: "error", message: err.error });
          this.loading = false;
        }
      );
  }
}
