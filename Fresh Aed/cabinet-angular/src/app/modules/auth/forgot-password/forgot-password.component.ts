import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import { NotifierService } from "angular-notifier";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"]
})
export class ForgotPasswordComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private api: AuthService,
    private fb: FormBuilder,
    private notify: NotifierService,
    private router: Router
  ) {
    if (localStorage.token) this.router.navigate(["/dashboard"]);
  }

  ngOnInit() {
    this.form = this.fb.group({
      email: ["", [Validators.required, Validators.email]]
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    this.api.sendPasswordReset(this.form.value).subscribe(
      (res: any) => {
        this.submitted = false;
        this.loading = false;
        this.form.patchValue({ email: null });
        this.notify.show({
          type: "success",
          message: "Please check your email for password reset link"
        });
      },
      (err: any) => {
        this.notify.show({ type: "error", message: err.error });
        this.loading = false;
      }
    );
  }
}
