import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  get getUser() {
    return this.http.get(
      `${environment.serviceURL}/users/id/${this.auth.currentUserID}`,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }

  updateUser(body) {
    return this.http.put(
      `${environment.serviceURL}/users/id/${this.auth.currentUserID}`,
      body,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }

  get getAllUser() {
    return this.http.get(`${environment.serviceURL}/users`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  updateStatus(id) {
    return this.http.patch(
      `${environment.serviceURL}/users/id/${id}/status`,
      {},
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }

  get dashboardSummary() {
    return this.http.get(
      `${environment.serviceURL}/users/id/${this.auth.currentUserID}/dashboard-summary`,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }
}
