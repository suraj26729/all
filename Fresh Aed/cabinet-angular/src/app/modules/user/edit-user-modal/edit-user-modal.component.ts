import { Component, OnInit } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../auth/auth.service";
import { UserService } from "../user.service";
import { NotifierService } from "angular-notifier";

@Component({
  selector: "app-edit-user-modal",
  templateUrl: "./edit-user-modal.component.html",
  styleUrls: ["./edit-user-modal.component.scss"]
})
export class EditUserModalComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  passwordMismatch = false;
  cities: any = [];
  user: any = {};
  constructor(
    public modalRef: BsModalRef,
    private fb: FormBuilder,
    private auth: AuthService,
    private api: UserService,
    private notify: NotifierService
  ) {}

  ngOnInit() {
    this.form = this.fb.group(
      {
        gender: ["", Validators.required],
        firstname: ["", Validators.required],
        lastname: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        phone: ["", Validators.required],
        city: [""],
        organization: [""],
        street: [""],
        zip: [""],
        home_num: [""],
        addition_num: [""],
        password: [""],
        confirm_password: [""]
      },
      { validator: this.checkPasswords }
    );
    this.auth.cities.subscribe(cities => (this.cities = cities));
    this.api.getUser.subscribe(user => {
      this.user = user;
      this.form.patchValue(user);
      this.form.patchValue({ password: "", confirm_password: "" });
    });
  }

  get f() {
    return this.form.controls;
  }

  checkPasswords(group: FormGroup) {
    let pass = group.get("password").value;
    let confirmPass = group.get("confirm_password").value;

    return pass === confirmPass ? null : { notSame: true };
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) return;

    this.loading = true;
    this.api.updateUser(this.form.value).subscribe(
      res => {
        this.notify.show({
          type: "success",
          message: "Profile details updated"
        });
        this.loading = false;
        // this.modalRef.hide();
      },
      err => {
        this.notify.show({ type: "error", message: err.error });
        this.loading = false;
      }
    );
  }
}
