import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UserRoutingModule } from "./user-routing.module";
import { SharedModule } from "../../shared.module";
import { UsersListComponent } from "./users-list/users-list.component";

@NgModule({
  imports: [CommonModule, UserRoutingModule, SharedModule],
  declarations: [UsersListComponent]
})
export class UserModule {}
