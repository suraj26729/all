import { Component, OnInit, ViewChild } from "@angular/core";
import { DataTableDirective } from "angular-datatables";
import { UserService } from "../user.service";
import { NotifierService } from "angular-notifier";

@Component({
  selector: "app-users-list",
  templateUrl: "./users-list.component.html",
  styleUrls: ["./users-list.component.scss"]
})
export class UsersListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  list: any = [];
  dtOptions: any = {};
  loadingPage = true;

  constructor(private api: UserService, private notify: NotifierService) {}

  ngOnInit() {
    this.dtOptions = {
      dom: "Bfrtip",
      buttons: [{ extend: "excel", className: "btn btn-secondary" }],
      pagingType: "full_numbers",
      pageLength: 10
    };
    this.fetchUsers();
  }

  fetchUsers() {
    this.loadingPage = true;
    this.api.getAllUser.subscribe(
      users => {
        this.list = users;
        this.loadingPage = false;
      },
      err => console.log(err)
    );
  }

  updateStatus(id) {
    this.api.updateStatus(id).subscribe(
      res => {
        setTimeout(() => {
          this.fetchUsers();
        }, 1000);
        this.notify.show({
          type: "success",
          message: "User status is changed"
        });
      },
      err => console.log(err)
    );
  }
}
