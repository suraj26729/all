import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { AEDService } from "../aed.service";
import { AuthService } from "../../auth/auth.service";
import { NotifierService } from "angular-notifier";
import { Router } from "@angular/router";
import * as $ from "jquery";

@Component({
  selector: "app-add-aed",
  templateUrl: "./add-aed.component.html",
  styleUrls: ["./add-aed.component.scss"],
})
export class AddAEDComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  city: any;
  brand: any;
  models: any;
  showmon: any;
  showtue: any;
  showwed: any;
  showthu: any;
  showfri: any;
  showsat: any;
  showsun: any;
  availability: any = [];
  options = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];
  optionsMap = {
    Monday: false,
    Tuesday: false,
    Wednesday: false,
    Thursday: false,
    Friday: false,
    Saturday: false,
    Sunday: false,
  };
  optionsChecked = [];
  optionsP: any;
  public: any;
  flag = 0;

  constructor(
    private fb: FormBuilder,
    private api: AEDService,
    public auth: AuthService,
    private notify: NotifierService,
    private router: Router
  ) {}

  ngOnInit() {
    this.setForm();
    this.getcity();
    this.getbrand();
  }

  setForm() {
    this.form = this.fb.group({
      cabinet_code: ["", Validators.required],
      license_key: ["", Validators.required],
      brand: ["", Validators.required],
      model: ["", Validators.required],
      serial: ["", Validators.required],
      reg: null,
      purchase_date: null,
      first_pad: ["", Validators.required],
      first_pad_exp: [null, Validators.required],
      second_pad: null,
      second_pad_exp: [null],
      battery_serial: null,
      battry_exp: [null],
      street: ["", Validators.required],
      number: null,
      number_addition: null,
      zipcode: ["", Validators.required],
      city: ["", Validators.required],
      country: ["Netherlands"],
      lock_code: null,
      building_id: null,
      visibility: null,
      user_id: null,
      // contact_firstname: null,
      // contact_lastname: null,
      // contact_email: null,
      // contact_gender: null,
      // contact_mobile: null,
      // contact_street: null,
      // contact_housenumber: null,
      // contact_additional_housenumber: null,
      // contact_zip: null,
      // contact_city: null,
      // contact_remarks: null,
      Monday: null,
      Tuesday: null,
      Wednesday: null,
      Thursday: null,
      Friday: null,
      Saturday: null,
      Sunday: null,
      mondtime: ["00:00"],
      monntime: ["23:59"],
      tuedtime: ["00:00"],
      tuentime: ["23:59"],
      weddtime: ["00:00"],
      wedntime: ["23:59"],
      thudtime: ["00:00"],
      thuntime: ["23:59"],
      frintime: ["23:59"],
      fridtime: ["00:00"],
      satdtime: ["00:00"],
      satntime: ["23:59"],
      sundtime: ["00:00"],
      sunntime: ["23:59"],
      availability: null,
      locality: null,
      responders: this.fb.array([]),
    });

    this.addResponders();
  }

  get f() {
    return this.form.controls;
  }

  getcity() {
    this.auth.cities.subscribe((cities) => (this.city = cities));
  }

  getbrand() {
    this.auth.brands.subscribe((brands) => (this.brand = brands));
  }

  getmodel(event) {
    this.auth
      .models(event.target.value)
      .subscribe((models) => (this.models = models));
  }

  updateCheckedOptions(option, event) {
    this.optionsMap[option] = event.target.checked;

    console.log(this.optionsMap);
  }

  showpublic() {
    this.public = true;
  }

  hidepublic() {
    this.showthu = false;
    this.showtue = false;
    this.showmon = false;
    this.showwed = false;
    this.showfri = false;
    this.showsat = false;
    this.showsun = false;
    this.public = false;
    this.availability = "private";
  }

  showmonday() {
    if (this.showmon) {
      this.showmon = false;
    } else {
      this.showmon = true;
    }
  }

  showtuesday() {
    if (this.showtue) {
      this.showtue = false;
    } else {
      this.showtue = true;
    }
  }

  showwednesday() {
    if (this.showwed) {
      this.showwed = false;
    } else {
      this.showwed = true;
    }
  }

  showthursday() {
    if (this.showthu) {
      this.showthu = false;
    } else {
      this.showthu = true;
    }
  }

  showfriday() {
    if (this.showfri) {
      this.showfri = false;
    } else {
      this.showfri = true;
    }
  }

  showsaturday() {
    if (this.showsat) {
      this.showsat = false;
    } else {
      this.showsat = true;
    }
  }

  showsunday() {
    if (this.showsun) {
      this.showsun = false;
    } else {
      this.showsun = true;
    }
  }

  twentyfourseven(event) {
    for (var i = 0; i < 7; i++) {
      // console.log(this._permissions.controls[i].value.permission_id);
      if (event.target.checked) {
        (<HTMLInputElement>(
          document.getElementsByClassName("selectchild")[i]
        )).checked = true;
      } else {
        (<HTMLInputElement>(
          document.getElementsByClassName("selectchild")[i]
        )).checked = false;
      }
    }
    this.showthu = false;
    this.showtue = false;
    this.showmon = false;
    this.showwed = false;
    this.showfri = false;
    this.showsat = false;
    this.showsun = false;
    this.public = true;

    this.showmonday();
    this.showtuesday();
    this.showwednesday();
    this.showthursday();
    this.showfriday();
    this.showsaturday();
    this.showsunday();
  }

  getavailability() {
    var form = this.form.value;
    if (form.visibility == "public") {
      this.availability = {
        Monday: {
          name: this.showmon,
          dtime: form.mondtime,
          etime: form.monntime,
        },
        Tuesday: {
          name: this.showtue,
          dtime: form.tuedtime,
          etime: form.tuentime,
        },
        Wednesday: {
          name: this.showwed,
          dtime: form.weddtime,
          etime: form.wedntime,
        },
        Thursday: {
          name: this.showthu,
          dtime: form.thudtime,
          etime: form.thuntime,
        },
        Friday: {
          name: this.showfri,
          dtime: form.fridtime,
          etime: form.frintime,
        },
        Saturday: {
          name: this.showsat,
          dtime: form.satdtime,
          etime: form.satntime,
        },
        Sunday: {
          name: this.showsun,
          dtime: form.sundtime,
          etime: form.sunntime,
        },
      };
      this.form.patchValue({ availability: JSON.stringify(this.availability) });
    } else {
      this.availability = "private";
      this.form.patchValue({ availability: this.availability });
    }

    // console.log(JSON.stringify(this.availability));
  }

  get responders() {
    return this.form.get("responders") as FormArray;
  }

  addResponders() {
    this.responders.push(
      this.fb.group({
        name: ["", Validators.required],
        email: ["", Validators.email],
        phone: ["", Validators.required],
        sms_open_closed: false,
        sms_aed_present: false,
        sms_aed_status: false,
        sms_power: false,
        sms_connection: false,
        sms_maintenance: false,
        email_open_closed: false,
        email_aed_present: false,
        email_aed_status: false,
        email_power: false,
        email_connection: false,
        email_maintenance: false,
      })
    );
  }

  deleteResponders(index) {
    this.responders.removeAt(index);
  }

  selectAllSMSNotifications(event, index) {
    if (event.target.checked) {
      $(".set_sms").prop("checked", true);
      this.f.responders["controls"][index].patchValue({
        sms_open_closed: true,
        sms_aed_present: true,
        sms_aed_status: true,
        sms_power: true,
        sms_connection: true,
        sms_maintenance: true,
      });
    } else {
      $(".set_sms").prop("checked", false);
      this.f.responders["controls"][index].patchValue({
        sms_open_closed: false,
        sms_aed_present: false,
        sms_aed_status: false,
        sms_power: false,
        sms_connection: false,
        sms_maintenance: false,
      });
    }
  }

  selectAllMailNotifications(event, index) {
    if (event.target.checked) {
      $(".set_email").prop("checked", true);
      this.f.responders["controls"][index].patchValue({
        email_open_closed: true,
        email_aed_present: true,
        email_aed_status: true,
        email_power: true,
        email_connection: true,
        email_maintenance: true,
      });
    } else {
      $(".set_email").prop("checked", false);
      this.f.responders["controls"][index].patchValue({
        email_open_closed: false,
        email_aed_present: false,
        email_aed_status: false,
        email_power: false,
        email_connection: false,
        email_maintenance: false,
      });
    }
  }

  setSMSNotifications(event, index, name) {
    if (event.target.checked) {
      this.f.responders["controls"][index].patchValue({
        [name]: true,
      });
    } else {
      this.f.responders["controls"][index].patchValue({
        [name]: false,
      });
    }
  }

  setEmailNotifications(event, index, value) {
    if (event.target.checked) {
      this.f.responders["controls"][index].patchValue({
        [name]: true,
      });
    } else {
      this.f.responders["controls"][index].patchValue({
        [name]: false,
      });
    }
  }

  submit() {
    this.submitted = true;

    if (this.flag == 0 && this.form.value.cabinet_code != "") {
      this.api.checkCabinetCode(this.form.value.cabinet_code).subscribe(
        (res) => {
          this.flag += 1;
          this.submitted = false;
        },
        (err) => {
          this.notify.show({ type: "error", message: err.error });
          this.submitted = false;
        }
      );
      return;
    }

    if (this.flag == 1 && this.form.value.license_key != "") {
      this.api.checkLicenseKey(this.form.value.license_key).subscribe(
        (res) => {
          this.flag += 1;
          this.submitted = false;
        },
        (err) => {
          this.notify.show({ type: "error", message: err.error });
          this.submitted = false;
        }
      );
      return;
    }

    if (this.form.invalid) {
      this.notify.show({
        type: "error",
        message: "Please check the form data once again",
      });
      return;
    }
    this.getavailability();

    let {
      cabinet_code,
      license_key,
      brand,
      model,
      serial,
      reg,
      purchase_date,
      first_pad,
      first_pad_exp,
      second_pad,
      second_pad_exp,
      battry_exp,
      battery_serial,
      street,
      number,
      number_addition,
      zipcode,
      city,
      country,
      lock_code,
      building_id,
      availability,
      locality,
      visibility,
      responders,
    } = this.form.value;

    // console.log(availability);
    // return;

    this.loading = true;
    this.api
      .create({
        cabinet_code,
        license_key,
        brand,
        model,
        serial,
        reg,
        purchase_date,
        first_pad,
        first_pad_exp,
        second_pad,
        second_pad_exp,
        battry_exp,
        battery_serial,
        street,
        number,
        number_addition,
        zipcode,
        city,
        country,
        lock_code,
        building_id,
        availability,
        locality,
        visibility,
        responders,
      })
      .subscribe(
        (res: any) => {
          this.setForm();
          this.notify.show({ type: "success", message: "AED is created" });
          this.loading = false;
          this.submitted = false;
          this.router.navigate([`/aeds/details/${res.uuid}`]);
        },
        (err) =>
          this.notify.show({
            type: "error",
            message: JSON.stringify(err.error),
          })
      );
  }
}
