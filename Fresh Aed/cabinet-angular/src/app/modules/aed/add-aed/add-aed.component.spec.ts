import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAEDComponent } from './add-aed.component';

describe('AddAEDComponent', () => {
  let component: AddAEDComponent;
  let fixture: ComponentFixture<AddAEDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAEDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAEDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
