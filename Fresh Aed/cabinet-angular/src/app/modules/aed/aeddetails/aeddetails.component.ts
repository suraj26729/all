import { Component, OnInit } from "@angular/core";
import { AEDService } from "../aed.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-aeddetails",
  templateUrl: "./aeddetails.component.html",
  styleUrls: ["./aeddetails.component.scss"]
})
export class AEDDetailsComponent implements OnInit {
  data: any;

  showmon: any;
  showtue: any;
  showwed: any;
  showthu: any;
  showfri: any;
  showsat: any;
  showsun: any;
  mondtime: any;
  monntime: any;
  tuedtime: any;
  tuentime: any;
  weddtime: any;
  wedntime: any;
  thudtime: any;
  thuntime: any;
  frintime: any;
  fridtime: any;
  satdtime: any;
  satntime: any;
  sundtime: any;
  sunntime: any;

  constructor(private api: AEDService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.api.getAED(this.route.snapshot.params.uuid).subscribe(data => {
      this.data = data;
      if (this.data.availability) {
        var av = JSON.parse(this.data.availability);
        if (av.Monday.name) {
          this.hidepublic();
          this.showmonday();
          this.mondtime = av.Monday.dtime;
          this.monntime = av.Monday.etime;
        }
        if (av.Tuesday.name) {
          this.showtuesday();
          this.tuedtime = av.Tuesday.dtime;
          this.tuentime = av.Tuesday.etime;
        }
        if (av.Wednesday.name) {
          this.showwednesday();
          this.weddtime = av.Wednesday.dtime;
          this.wedntime = av.Wednesday.etime;
        }
        if (av.Thursday.name) {
          this.showthursday();
          this.thudtime = av.Thursday.dtime;
          this.thuntime = av.Thursday.etime;
        }
        if (av.Friday.name) {
          this.showfriday();
          this.frintime = av.Friday.dtime;
          this.fridtime = av.Friday.etime;
        }
        if (av.Saturday.name) {
          this.showsaturday();
          this.satdtime = av.Saturday.dtime;
          this.satntime = av.Saturday.etime;
        }
        if (av.Sunday.name) {
          this.showsunday();
          this.sundtime = av.Sunday.dtime;
          this.sunntime = av.Sunday.etime;
        }
      }
    });
  }

  hidepublic() {
    this.showthu = false;
    this.showtue = false;
    this.showmon = false;
    this.showwed = false;
    this.showfri = false;
    this.showsat = false;
    this.showsun = false;
  }

  showmonday() {
    if (this.showmon) {
      this.showmon = false;
    } else {
      this.showmon = true;
    }
  }

  showtuesday() {
    if (this.showtue) {
      this.showtue = false;
    } else {
      this.showtue = true;
    }
  }

  showwednesday() {
    if (this.showwed) {
      this.showwed = false;
    } else {
      this.showwed = true;
    }
  }

  showthursday() {
    if (this.showthu) {
      this.showthu = false;
    } else {
      this.showthu = true;
    }
  }

  showfriday() {
    if (this.showfri) {
      this.showfri = false;
    } else {
      this.showfri = true;
    }
  }

  showsaturday() {
    if (this.showsat) {
      this.showsat = false;
    } else {
      this.showsat = true;
    }
  }

  showsunday() {
    if (this.showsun) {
      this.showsun = false;
    } else {
      this.showsun = true;
    }
  }
}
