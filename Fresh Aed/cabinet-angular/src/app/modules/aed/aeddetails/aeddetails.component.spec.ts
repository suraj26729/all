import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AEDDetailsComponent } from './aeddetails.component';

describe('AEDDetailsComponent', () => {
  let component: AEDDetailsComponent;
  let fixture: ComponentFixture<AEDDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AEDDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AEDDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
