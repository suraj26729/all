import { TestBed } from '@angular/core/testing';

import { AEDService } from './aed.service';

describe('AEDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AEDService = TestBed.get(AEDService);
    expect(service).toBeTruthy();
  });
});
