import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class AEDService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  get getAllAEDs() {
    return this.http.get(`${environment.serviceURL}/aeds`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  get getAEDs() {
    return this.http.get(`${environment.serviceURL}/aeds/user`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  deleteAED(uuid) {
    return this.http.delete(`${environment.serviceURL}/aeds/uuid/${uuid}`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  create(data) {
    return this.http.post(`${environment.serviceURL}/aeds`, data, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  getAED(uuid) {
    return this.http.get(`${environment.serviceURL}/aeds/uuid/${uuid}`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  update(data, uuid) {
    return this.http.put(`${environment.serviceURL}/aeds/uuid/${uuid}`, data, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  updateStatus(data, uuid) {
    return this.http.patch(
      `${environment.serviceURL}/aeds/uuid/${uuid}/status`,
      data,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }

  checkCabinetCode(code) {
    return this.http.get(
      `${environment.serviceURL}/aeds/cabinet-code/check/${code}`,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }

  checkLicenseKey(key) {
    return this.http.get(
      `${environment.serviceURL}/aeds/license-key/check/${key}`,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }
}
