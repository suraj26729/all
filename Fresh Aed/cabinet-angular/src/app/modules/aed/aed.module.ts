import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AEDListComponent } from "./aedlist/aedlist.component";
import { AddAEDComponent } from "./add-aed/add-aed.component";
import { EditAEDComponent } from "./edit-aed/edit-aed.component";
import { AedRoutingModule } from "./aed-routing.module";
import { SharedModule } from "../../shared.module";
import { AEDDetailsComponent } from './aeddetails/aeddetails.component';
import { AEDMapComponent } from './aedmap/aedmap.component';

@NgModule({
  imports: [CommonModule, AedRoutingModule, SharedModule],
  declarations: [AEDListComponent, AddAEDComponent, EditAEDComponent, AEDDetailsComponent, AEDMapComponent]
})
export class AedModule {}
