import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AEDMapComponent } from './aedmap.component';

describe('AEDMapComponent', () => {
  let component: AEDMapComponent;
  let fixture: ComponentFixture<AEDMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AEDMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AEDMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
