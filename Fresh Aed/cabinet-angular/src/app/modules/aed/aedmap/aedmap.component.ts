import { Component, OnInit, ViewChild } from "@angular/core";
import { MapsAPILoader, AgmMap } from "@agm/core";
import { AEDService } from "../aed.service";
import { environment } from "../../../../environments/environment.prod";

@Component({
  selector: "app-aedmap",
  templateUrl: "./aedmap.component.html",
  styleUrls: ["./aedmap.component.scss"]
})
export class AEDMapComponent implements OnInit {
  geocoder: any;
  @ViewChild(AgmMap) map: AgmMap;
  name: string = "My first AGM project";
  lat: number = 52.132633;
  lng: number = 5.291266;

  aeds: any = [];
  markerURL = `${environment.siteURL}/assets/images/marker-active.png`;
  suspendMarkerURL = `${environment.siteURL}/assets/images/marker-suspend.png`;
  expiredMarkerURL = `${environment.siteURL}/assets/images/marker-expired.png`;

  loading = true;

  constructor(private api: AEDService) {}

  ngOnInit() {
    this.getall();
  }

  getall() {
    this.api.getAllAEDs.subscribe((aeds: any) => {
      this.aeds = aeds.map(item => {
        item.availability =
          item.availability != null ? JSON.parse(item.availability) : null;
        return item;
      });
      this.loading = false;
    });
  }
}
