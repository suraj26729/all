import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { AEDListComponent } from "./aedlist/aedlist.component";
import { AddAEDComponent } from "./add-aed/add-aed.component";
import { EditAEDComponent } from "./edit-aed/edit-aed.component";
import { AEDDetailsComponent } from "./aeddetails/aeddetails.component";
import { AEDMapComponent } from "./aedmap/aedmap.component";

const routes: Routes = [
  {
    path: "",
    component: AEDListComponent,
    pathMatch: "full",
    data: { title: "AED's", breadcrumb: "AED's" }
  },
  {
    path: "create",
    component: AddAEDComponent,
    data: { title: "Create AED", breadcrumb: "Create AED" }
  },
  {
    path: "details/:uuid",
    component: AEDDetailsComponent,
    data: { title: "AED Details", breadcrumb: "AED Details" }
  },
  {
    path: "edit/:uuid",
    component: EditAEDComponent,
    data: { title: "Edit AED", breadcrumb: "Edit AED" }
  },
  {
    path: "map",
    component: AEDMapComponent,
    data: { title: "AED Map", breadcrumb: "AED Map" }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AedRoutingModule {}
