import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import * as moment from "moment";
import { DataTableDirective } from "angular-datatables";
import { AEDService } from "../aed.service";
import { Select2OptionData } from "ng2-select2";
import { AuthService } from "../../auth/auth.service";
import { NotifierService } from "angular-notifier";

@Component({
  selector: "app-aedlist",
  templateUrl: "./aedlist.component.html",
  styleUrls: ["./aedlist.component.scss"],
})
export class AEDListComponent implements OnInit {
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  list: any = [];
  dtOptions: any = {};
  loadingPage = true;
  openFilters = true;
  filterForm: FormGroup;
  _cities: any = [];
  success = false;

  public cities: Array<Select2OptionData> = [];
  public owners: Array<Select2OptionData> = [];
  public zipcodes: Array<Select2OptionData> = [];
  public serials: Array<Select2OptionData> = [];
  public aed_regs: Array<Select2OptionData> = [];
  public select2_options: Select2Options;

  constructor(
    private api: AEDService,
    private fb: FormBuilder,
    private auth: AuthService,
    private notify: NotifierService
  ) {}

  ngOnInit() {
    let placeholder = [{ id: "", text: "All" }];
    this.auth.cities.subscribe((cities: any) => {
      this._cities = cities.map((item) => {
        return { id: item.name, text: item.name };
      });
      this._cities = placeholder.concat(this._cities);
    });
    $.fn["dataTable"].ext.search.push((settings, data, dataIndex) => {
      const d_city = data[0] || "";
      const d_zip = data[1] || "";
      const d_serial = data[5] || "";

      const { city, zip, serial } = this.filterForm.value;

      if (city != "" || zip != "" || serial != "") {
        let cond = true;
        if (city != "") cond = cond && city == d_city;
        if (zip != "") cond = cond && d_zip.includes(zip);
        if (serial != "") cond = cond && d_serial.includes(serial);

        return cond;
      }
      return true;
    });
    this.dtOptions = {
      dom: "Bfrtip",
      buttons: [{ extend: "excel", className: "btn btn-secondary" }],
      pagingType: "full_numbers",
      pageLength: 10,
      columns: [
        { name: "cabinet_code", orderable: true },
        { name: "city", orderable: true },
        { name: "zip", orderable: true },
        { name: "street", orderable: true },
        { name: "house_num", orderable: true },
        { name: "status", orderable: true },
        { name: "serial", orderable: true },
        { name: "date", orderable: true, date: true },
        { name: "actions", orderable: false },
      ],
      order: [[7, "desc"]],
    };
    this.select2_options = {
      placeholder: "All",
      allowClear: true,
    };
    this.fetchAEDs();
    this.filterForm = this.fb.group({
      city: [""],
      zip: [""],
      serial: [""],
    });
  }

  removeDuplicates(array, key) {
    return array.filter(
      (obj, index, self) =>
        index === self.findIndex((el) => el[key] === obj[key])
    );
  }

  fetchAEDs() {
    this.loadingPage = true;
    this.openFilters = false;
    this.api.getAEDs.subscribe(
      (aeds) => {
        this.list = aeds;
        this.loadingPage = false;
        let placeholder = [{ id: "", text: "All" }];
        this.zipcodes = this.list.map((item) => {
          if (item.zipcode != null) {
            return {
              id: item.zipcode,
              text: item.zipcode,
            };
          }
        });
        this.zipcodes = placeholder.concat(this.zipcodes);
        this.zipcodes = this.removeDuplicates(this.zipcodes, "id");

        placeholder = [{ id: "", text: "All" }];
        this.serials = this.list.map((item) => {
          if (item.serial != null)
            return {
              id: item.serial,
              text: item.serial,
            };
        });
        this.serials = placeholder.concat(this.serials);
        this.serials = this.removeDuplicates(this.serials, "id");

        this.success = true;
      },
      (err) => console.log(err)
    );
  }

  city_changed(event) {
    this.filterForm.patchValue({ city: event.value });
  }

  owner_changed(event) {
    this.filterForm.patchValue({ owner: event.value });
  }

  filter_changed(event, name) {
    this.filterForm.patchValue({ [name]: event.value });
  }

  filter() {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.draw();
    });
  }

  ngOnDestroy(): void {
    $.fn["dataTable"].ext.search.pop();
  }

  open_Filters() {
    this.openFilters = !this.openFilters;
  }

  delete(uuid) {
    let result = confirm("Are you sure to delete ?");
    if (result) {
      this.api.deleteAED(uuid).subscribe(
        (res) => {
          this.notify.show({ type: "info", message: "AED has been removed" });
          this.fetchAEDs();
        },
        (err: any) => this.notify.show({ type: "error", message: err.error })
      );
    }
  }
}
