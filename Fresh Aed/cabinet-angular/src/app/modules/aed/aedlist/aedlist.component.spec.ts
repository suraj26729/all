import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AEDListComponent } from './aedlist.component';

describe('AEDListComponent', () => {
  let component: AEDListComponent;
  let fixture: ComponentFixture<AEDListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AEDListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AEDListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
