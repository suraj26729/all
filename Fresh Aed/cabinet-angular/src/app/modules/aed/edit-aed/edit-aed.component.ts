import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { AEDService } from "../aed.service";
import { AuthService } from "../../auth/auth.service";
import { NotifierService } from "angular-notifier";
import { ActivatedRoute } from "@angular/router";
import * as $ from "jquery";

@Component({
  selector: "app-edit-aed",
  templateUrl: "./edit-aed.component.html",
  styleUrls: ["./edit-aed.component.scss"],
})
export class EditAEDComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  city: any;
  brand: any;
  models: any;
  showmon: any;
  showtue: any;
  showwed: any;
  showthu: any;
  showfri: any;
  showsat: any;
  showsun: any;
  mondtime: any;
  monntime: any;
  tuedtime: any;
  tuentime: any;
  weddtime: any;
  wedntime: any;
  thudtime: any;
  thuntime: any;
  frintime: any;
  fridtime: any;
  satdtime: any;
  satntime: any;
  sundtime: any;
  sunntime: any;
  availability: any = [];
  options = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];
  optionsMap = {
    Monday: false,
    Tuesday: false,
    Wednesday: false,
    Thursday: false,
    Friday: false,
    Saturday: false,
    Sunday: false,
  };
  optionsChecked = [];
  optionsP: any;
  public: any;
  data: any;
  aedStatus: any;
  statusList = [
    { value: "active", label: "Active" },
    { value: "suspended", label: "Suspended" },
    { value: "expired", label: "Expired" },
  ];
  cabinet_code: any;
  license_key: any;

  constructor(
    private fb: FormBuilder,
    private api: AEDService,
    public auth: AuthService,
    private notify: NotifierService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.setForm();
    this.getcity();
    this.getbrand();
    this.fillData();
  }

  fillData() {
    this.api.getAED(this.route.snapshot.params.uuid).subscribe((data) => {
      this.data = data;
      this.form.patchValue(data);
      if (this.data.license.license != undefined)
        this.form.patchValue({ license_key: this.data.license.license });
      setTimeout(() => {
        document.getElementById("brand").dispatchEvent(new Event("change"));
        this.form.patchValue({
          brand: this.data.brand,
          model: this.data.model,
        });
      }, 1000);
      this.showthu = false;
      this.showtue = false;
      this.showmon = false;
      this.showwed = false;
      this.showfri = false;
      this.showsat = false;
      this.showsun = false;

      if (this.data.visibility == "public" && this.data.availability != null) {
        let availability = JSON.parse(this.data.availability);
        // console.log(availability);
        if (availability.Monday.name) {
          this.form.patchValue({
            Monday: availability.Monday.name,
            mondtime: availability.Monday.dtime,
            monntime: availability.Monday.etime,
          });
          this.showmonday();
        }
        if (availability.Tuesday.name) {
          this.form.patchValue({
            Tuesday: availability.Tuesday.name,
            tuedtime: availability.Tuesday.dtime,
            tuentime: availability.Tuesday.etime,
          });
          this.showtuesday();
        }
        if (availability.Wednesday.name) {
          this.form.patchValue({
            Wednesday: availability.Wednesday.name,
            weddtime: availability.Wednesday.dtime,
            wedntime: availability.Wednesday.etime,
          });
          this.showwednesday();
        }
        if (availability.Thursday.name) {
          this.form.patchValue({
            Thursday: availability.Thursday.name,
            thudtime: availability.Thursday.dtime,
            thuntime: availability.Thursday.etime,
          });
          this.showthursday();
        }
        if (availability.Friday.name) {
          this.form.patchValue({
            Friday: availability.Friday.name,
            fridtime: availability.Friday.dtime,
            frintime: availability.Friday.etime,
          });
          this.showfriday();
        }
        if (availability.Saturday.name) {
          this.form.patchValue({
            Saturday: availability.Saturday.name,
            satdtime: availability.Saturday.dtime,
            satntime: availability.Saturday.etime,
          });
          this.showsaturday();
        }
        if (availability.Sunday.name) {
          this.form.patchValue({
            Sunday: availability.Sunday.name,
            sundtime: availability.Sunday.dtime,
            sunntime: availability.Sunday.etime,
          });
          this.showsunday();
        }
      }
      if (
        Array.isArray(this.data.responders) &&
        this.data.responders.length > 0
      ) {
        for (let index in this.data.responders) {
          this.addResponders();
          this.f.responders["controls"][index].patchValue({
            name: this.data.responders[index].name,
            email: this.data.responders[index].email,
            phone: this.data.responders[index].phone,
            sms_open_closed: this.data.responders[index].sms_open_closed,
            sms_aed_present: this.data.responders[index].sms_aed_present,
            sms_aed_status: this.data.responders[index].sms_aed_status,
            sms_power: this.data.responders[index].sms_power,
            sms_connection: this.data.responders[index].sms_connection,
            sms_maintenance: this.data.responders[index].sms_maintenance,
            email_open_closed: this.data.responders[index].email_open_closed,
            email_aed_present: this.data.responders[index].email_aed_present,
            email_aed_status: this.data.responders[index].email_aed_status,
            email_power: this.data.responders[index].email_power,
            email_connection: this.data.responders[index].email_connection,
            email_maintenance: this.data.responders[index].email_maintenance,
          });
        }
      } else {
        this.addResponders();
      }
    });
  }

  setForm() {
    this.form = this.fb.group({
      cabinet_code: "",
      license_key: "",
      brand: ["", Validators.required],
      model: ["", Validators.required],
      serial: ["", Validators.required],
      reg: null,
      purchase_date: null,
      first_pad: ["", Validators.required],
      first_pad_exp: [null, Validators.required],
      second_pad: null,
      second_pad_exp: [null],
      battery_serial: null,
      battry_exp: [null],
      street: ["", Validators.required],
      number: null,
      number_addition: null,
      zipcode: ["", Validators.required],
      city: ["", Validators.required],
      country: ["Netherlands"],
      lock_code: null,
      building_id: null,
      visibility: null,
      status: null,
      user_id: null,
      // contact_firstname: null,
      // contact_lastname: null,
      // contact_email: null,
      // contact_gender: null,
      // contact_mobile: null,
      // contact_street: null,
      // contact_housenumber: null,
      // contact_additional_housenumber: null,
      // contact_zip: null,
      // contact_city: null,
      // contact_remarks: null,
      Monday: null,
      Tuesday: null,
      Wednesday: null,
      Thursday: null,
      Friday: null,
      Saturday: null,
      Sunday: null,
      mondtime: ["00:00"],
      monntime: ["23:59"],
      tuedtime: ["00:00"],
      tuentime: ["23:59"],
      weddtime: ["00:00"],
      wedntime: ["23:59"],
      thudtime: ["00:00"],
      thuntime: ["23:59"],
      frintime: ["23:59"],
      fridtime: ["00:00"],
      satdtime: ["00:00"],
      satntime: ["23:59"],
      sundtime: ["00:00"],
      sunntime: ["23:59"],
      availability: null,
      locality: null,
      responders: this.fb.array([]),
    });
  }

  get f() {
    return this.form.controls;
  }

  getcity() {
    this.auth.cities.subscribe((cities) => (this.city = cities));
  }

  getbrand() {
    this.auth.brands.subscribe((brands) => (this.brand = brands));
  }

  getmodel(event) {
    this.auth
      .models(event.target.value)
      .subscribe((models) => (this.models = models));
  }

  showpublic() {
    this.public = true;
  }

  hidepublic() {
    this.showthu = false;
    this.showtue = false;
    this.showmon = false;
    this.showwed = false;
    this.showfri = false;
    this.showsat = false;
    this.showsun = false;
    this.public = false;
    this.availability = "private";
  }

  showmonday() {
    if (this.showmon) {
      this.showmon = false;
    } else {
      this.showmon = true;
    }
  }

  showtuesday() {
    if (this.showtue) {
      this.showtue = false;
    } else {
      this.showtue = true;
    }
  }

  showwednesday() {
    if (this.showwed) {
      this.showwed = false;
    } else {
      this.showwed = true;
    }
  }

  showthursday() {
    if (this.showthu) {
      this.showthu = false;
    } else {
      this.showthu = true;
    }
  }

  showfriday() {
    if (this.showfri) {
      this.showfri = false;
    } else {
      this.showfri = true;
    }
  }

  showsaturday() {
    if (this.showsat) {
      this.showsat = false;
    } else {
      this.showsat = true;
    }
  }

  showsunday() {
    if (this.showsun) {
      this.showsun = false;
    } else {
      this.showsun = true;
    }
  }

  twentyfourseven(event) {
    for (var i = 0; i < 7; i++) {
      // console.log(this._permissions.controls[i].value.permission_id);
      if (event.target.checked) {
        (<HTMLInputElement>(
          document.getElementsByClassName("selectchild")[i]
        )).checked = true;
      } else {
        (<HTMLInputElement>(
          document.getElementsByClassName("selectchild")[i]
        )).checked = false;
      }
    }
    this.showthu = false;
    this.showtue = false;
    this.showmon = false;
    this.showwed = false;
    this.showfri = false;
    this.showsat = false;
    this.showsun = false;
    this.public = true;

    this.showmonday();
    this.showtuesday();
    this.showwednesday();
    this.showthursday();
    this.showfriday();
    this.showsaturday();
    this.showsunday();
  }

  getavailability() {
    var form = this.form.value;
    if (form.visibility == "public") {
      this.availability = {
        Monday: {
          name: this.showmon,
          dtime: form.mondtime,
          etime: form.monntime,
        },
        Tuesday: {
          name: this.showtue,
          dtime: form.tuedtime,
          etime: form.tuentime,
        },
        Wednesday: {
          name: this.showwed,
          dtime: form.weddtime,
          etime: form.wedntime,
        },
        Thursday: {
          name: this.showthu,
          dtime: form.thudtime,
          etime: form.thuntime,
        },
        Friday: {
          name: this.showfri,
          dtime: form.fridtime,
          etime: form.frintime,
        },
        Saturday: {
          name: this.showsat,
          dtime: form.satdtime,
          etime: form.satntime,
        },
        Sunday: {
          name: this.showsun,
          dtime: form.sundtime,
          etime: form.sunntime,
        },
      };
    } else {
      this.availability = "private";
    }

    this.form.patchValue({ availability: JSON.stringify(this.availability) });

    // console.log(JSON.stringify(this.availability));
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) {
      this.notify.show({
        type: "error",
        message: "Please check the form data once again",
      });
      return;
    }
    this.getavailability();

    let {
      cabinet_code,
      license_key,
      brand,
      model,
      serial,
      reg,
      purchase_date,
      first_pad,
      first_pad_exp,
      second_pad,
      second_pad_exp,
      battry_exp,
      battery_serial,
      street,
      number,
      number_addition,
      zipcode,
      city,
      country,
      lock_code,
      building_id,
      availability,
      locality,
      visibility,
      responders,
    } = this.form.value;

    this.loading = true;
    this.api
      .update(
        {
          cabinet_code,
          license_key,
          brand,
          model,
          serial,
          reg,
          purchase_date,
          first_pad,
          first_pad_exp,
          second_pad,
          second_pad_exp,
          battry_exp,
          battery_serial,
          street,
          number,
          number_addition,
          zipcode,
          city,
          country,
          lock_code,
          building_id,
          availability,
          locality,
          visibility,
          responders,
        },
        this.route.snapshot.params.uuid
      )
      .subscribe(
        (res) => {
          this.notify.show({ type: "success", message: "AED is updated" });
          this.loading = false;
          this.submitted = false;
        },
        (err) =>
          this.notify.show({
            type: "error",
            message: JSON.stringify(err.error),
          })
      );
  }

  updateStatus() {
    if (this.aedStatus != undefined) {
      this.api
        .updateStatus(
          { status: this.aedStatus },
          this.route.snapshot.params.uuid
        )
        .subscribe((res: any) => {
          if (res.status == "active") {
            this.notify.show({
              type: "success",
              message: "AED is active",
            });
          }
          if (res.status == "suspended") {
            this.notify.show({
              type: "warning",
              message: "AED is suspended",
            });
          }
          if (res.status == "expired") {
            this.notify.show({
              type: "error",
              message: "AED is expired",
            });
          }
        });
    }
  }

  get responders() {
    return this.form.get("responders") as FormArray;
  }

  addResponders() {
    this.responders.push(
      this.fb.group({
        name: ["", Validators.required],
        email: ["", Validators.email],
        phone: ["", Validators.required],
        sms_open_closed: false,
        sms_aed_present: false,
        sms_aed_status: false,
        sms_power: false,
        sms_connection: false,
        sms_maintenance: false,
        email_open_closed: false,
        email_aed_present: false,
        email_aed_status: false,
        email_power: false,
        email_connection: false,
        email_maintenance: false,
      })
    );
  }

  deleteResponders(index) {
    this.responders.removeAt(index);
  }

  selectAllSMSNotifications(event, index) {
    if (event.target.checked) {
      $(".set_sms").prop("checked", true);
      this.f.responders["controls"][index].patchValue({
        sms_open_closed: true,
        sms_aed_present: true,
        sms_aed_status: true,
        sms_power: true,
        sms_connection: true,
        sms_maintenance: true,
      });
    } else {
      $(".set_sms").prop("checked", false);
      this.f.responders["controls"][index].patchValue({
        sms_open_closed: false,
        sms_aed_present: false,
        sms_aed_status: false,
        sms_power: false,
        sms_connection: false,
        sms_maintenance: false,
      });
    }
  }

  selectAllMailNotifications(event, index) {
    if (event.target.checked) {
      $(".set_email").prop("checked", true);
      this.f.responders["controls"][index].patchValue({
        email_open_closed: true,
        email_aed_present: true,
        email_aed_status: true,
        email_power: true,
        email_connection: true,
        email_maintenance: true,
      });
    } else {
      $(".set_email").prop("checked", false);
      this.f.responders["controls"][index].patchValue({
        email_open_closed: false,
        email_aed_present: false,
        email_aed_status: false,
        email_power: false,
        email_connection: false,
        email_maintenance: false,
      });
    }
  }

  setSMSNotifications(event, index, name) {
    if (event.target.checked) {
      this.f.responders["controls"][index].patchValue({
        [name]: true,
      });
    } else {
      this.f.responders["controls"][index].patchValue({
        [name]: false,
      });
    }
  }

  setEmailNotifications(event, index, value) {
    if (event.target.checked) {
      this.f.responders["controls"][index].patchValue({
        [name]: true,
      });
    } else {
      this.f.responders["controls"][index].patchValue({
        [name]: false,
      });
    }
  }
}
