import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAEDComponent } from './edit-aed.component';

describe('EditAEDComponent', () => {
  let component: EditAEDComponent;
  let fixture: ComponentFixture<EditAEDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAEDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAEDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
