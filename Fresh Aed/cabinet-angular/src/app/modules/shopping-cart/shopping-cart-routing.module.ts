import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CheckoutComponent } from "./checkout/checkout.component";
import { PlansComponent } from "./plans/plans.component";

const routes: Routes = [
  {
    path: "",
    component: PlansComponent,
    data: { title: "Plans", breadcrumb: "Plans" },
  },
  {
    path: ":product_uuid/checkout",
    component: CheckoutComponent,
    data: { title: "Checkout", breadcrumb: "Checkout" },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShoppingCartRoutingModule {}
