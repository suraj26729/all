import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared.module";
import { CartComponent } from "./cart/cart.component";
import { ShoppingCartRoutingModule } from "./shopping-cart-routing.module";
import { CheckoutComponent } from "./checkout/checkout.component";
import { PlansComponent } from "./plans/plans.component";
import { ProductsService } from "../products/products.service";

@NgModule({
  imports: [CommonModule, ShoppingCartRoutingModule, SharedModule],
  declarations: [CartComponent, CheckoutComponent, PlansComponent],
  providers: [ProductsService],
})
export class ShoppingCartModule {}
