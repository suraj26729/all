let products: any = [
  { id: 1, quantity: 1 },
  { id: 2, quantity: 1 },
];

const add_to_cart = (data) => {
  products.push(data);
};

const remove_item = (id) => {
  products = products.filter((item) => item.id != id);
};

export { products, add_to_cart, remove_item };
