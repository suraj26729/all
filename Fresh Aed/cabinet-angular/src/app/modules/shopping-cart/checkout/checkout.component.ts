import { Component, OnInit } from "@angular/core";
import { ProductsService } from "../../products/products.service";
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../auth/auth.service";
import { NotifierService } from "angular-notifier";
import { ShoppingCartService } from "../shopping-cart.service";

@Component({
  selector: "app-checkout",
  templateUrl: "./checkout.component.html",
  styleUrls: ["./checkout.component.scss"],
})
export class CheckoutComponent implements OnInit {
  product: any;
  quantity = 1;
  subtotal: any = 0.0;
  total: any = 0.0;
  loading = true;
  form: FormGroup;
  cities: any = [];
  licenses: any = [];
  submitted = false;
  formLoading = false;

  constructor(
    private api: ProductsService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private auth: AuthService,
    private notify: NotifierService,
    private shopping: ShoppingCartService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({
      product_id: "",
      no_license: "",
      billing_firstname: [this.auth.currentUser.firstname, Validators.required],
      billing_lastname: [this.auth.currentUser.lastname, Validators.required],
      billing_email: [this.auth.currentUser.email, Validators.required],
      billing_phone: [this.auth.currentUser.phone, Validators.required],
      billing_organization: [this.auth.currentUser.organization],
      billing_street: [this.auth.currentUser.street, Validators.required],
      billing_home_num: [this.auth.currentUser.home_num],
      billing_addition_num: [this.auth.currentUser.addition_num],
      billing_zip: [this.auth.currentUser.zip, Validators.required],
      billing_city: [this.auth.currentUser.city, Validators.required],
      total: 0,
      user_id: this.auth.currentUserID,
    });

    this.auth.cities.subscribe((cities) => (this.cities = cities));

    this.api.getOne(this.route.snapshot.params.product_uuid).subscribe(
      (product: any) => {
        this.product = product;
        this.subtotal = product.sale_price
          ? product.sale_price
          : product.regular_price;
        this.licenses = Array(parseInt(product.no_license) + 5)
          .fill(0)
          .map((x, i) => i + parseInt(product.no_license));
        this.total = (
          parseFloat(this.subtotal) * parseInt(product.no_license)
        ).toFixed(2);

        this.form.patchValue({
          product_id: product.id,
          no_license: product.no_license,
          total: this.total,
        });
      },
      (err) => console.log(err),
      () => {
        this.loading = false;
      }
    );
  }
  updateTotal(event) {
    this.total = (
      parseFloat(this.subtotal) * parseInt(event.target.value)
    ).toFixed(2);
    this.form.patchValue({
      no_license: parseInt(event.target.value),
      total: this.total,
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.formLoading = true;

    this.shopping.createOrder(this.form.value).subscribe(
      (res: any) => {
        window.location.href = res.payment._links.checkout.href;
      },
      (err) => console.log(err)
    );
  }
}
