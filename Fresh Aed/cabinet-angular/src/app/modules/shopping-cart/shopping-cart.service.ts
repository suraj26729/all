import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class ShoppingCartService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  createOrder(data) {
    return this.http.post(`${environment.serviceURL}/orders`, data, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }
}
