import { Component, OnInit } from "@angular/core";
import { ProductsService } from "../../products/products.service";

@Component({
  selector: "app-plans",
  templateUrl: "./plans.component.html",
  styleUrls: ["./plans.component.scss"],
})
export class PlansComponent implements OnInit {
  products: any = [];
  loading = true;

  constructor(private api: ProductsService) {}

  ngOnInit() {
    this.api.getActiveOnly.subscribe(
      (products) => {
        this.products = products;
      },
      (err) => console.log(err),
      () => {
        this.loading = false;
      }
    );
  }
}
