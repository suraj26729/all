import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ListComponent } from "./list/list.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { ProductsRoutingModule } from "./products-routing.module";
import { SharedModule } from "../../shared.module";

@NgModule({
  imports: [CommonModule, ProductsRoutingModule, SharedModule],
  declarations: [ListComponent, CreateComponent, EditComponent],
})
export class ProductsModule {}
