import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ProductsService } from "../products.service";
import { NotifierService } from "angular-notifier";

@Component({
  selector: "app-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.scss"],
})
export class CreateComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private api: ProductsService,
    private notify: NotifierService
  ) {}

  ngOnInit() {
    this.setForm();
  }

  setForm() {
    this.form = this.fb.group({
      name: ["", Validators.required],
      description: "",
      regular_price: ["", Validators.required],
      sale_price: null,
      no_license: true,
      is_recurring: 1,
      recurring_duration: null,
      recurring_cycle: null,
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) {
      this.notify.show({
        type: "error",
        message: "Please check the form data once again",
      });
      return;
    }

    this.loading = true;
    this.api.create(this.form.value).subscribe(
      (res) => {
        this.setForm();
        this.notify.show({ type: "success", message: "Product is created" });
        this.loading = false;
        this.submitted = false;
      },
      (err) =>
        this.notify.show({
          type: "error",
          message: JSON.stringify(err.error),
        })
    );
  }
}
