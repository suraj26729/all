import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "../auth/auth.service";
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: "root",
})
export class ProductsService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  create(data) {
    return this.http.post(`${environment.serviceURL}/products`, data, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  get getAll() {
    return this.http.get(`${environment.serviceURL}/products/all`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  get getActiveOnly() {
    return this.http.get(`${environment.serviceURL}/products`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  getOne(uuid) {
    return this.http.get(`${environment.serviceURL}/products/uuid/${uuid}`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  update(data, uuid) {
    return this.http.put(
      `${environment.serviceURL}/products/uuid/${uuid}`,
      data,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }

  delete(uuid) {
    return this.http.delete(`${environment.serviceURL}/products/uuid/${uuid}`, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("token"),
      }),
    });
  }

  updateStatus(data, uuid) {
    return this.http.patch(
      `${environment.serviceURL}/products/uuid/${uuid}`,
      data,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          Authorization: localStorage.getItem("token"),
        }),
      }
    );
  }
}
