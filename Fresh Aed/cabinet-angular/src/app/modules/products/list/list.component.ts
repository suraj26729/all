import { Component, OnInit } from "@angular/core";
import { ProductsService } from "../products.service";
import { NotifierService } from "angular-notifier";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
})
export class ListComponent implements OnInit {
  list: any = [];
  loadingPage = true;
  success = false;

  constructor(private api: ProductsService, private notify: NotifierService) {}

  ngOnInit() {
    this.fetch();
  }

  fetch() {
    this.loadingPage = true;
    this.api.getAll.subscribe(
      (data) => {
        this.list = data;
        this.loadingPage = false;
        this.success = true;
      },
      (err) => console.log(err)
    );
  }

  delete(uuid) {
    let result = confirm("Are you sure to delete ?");
    if (result) {
      this.api.delete(uuid).subscribe(
        (res) => {
          this.notify.show({
            type: "info",
            message: "Product has been removed",
          });
          setTimeout(() => {
            this.fetch();
          }, 1000);
        },
        (err: any) => this.notify.show({ type: "error", message: err.error })
      );
    }
  }

  updateStatus(uuid, status) {
    this.api.updateStatus({ status: status }, uuid).subscribe(
      (res) => {
        this.notify.show({
          type: "info",
          message: `Product is ${status} now`,
        });
        setTimeout(() => {
          this.fetch();
        }, 1000);
      },
      (err) => console.log(err)
    );
  }
}
