import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ProductsService } from "../products.service";
import { NotifierService } from "angular-notifier";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.scss"],
})
export class EditComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  loading = false;
  product: any;

  constructor(
    private fb: FormBuilder,
    private api: ProductsService,
    private notify: NotifierService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.setForm();
  }

  setForm() {
    this.form = this.fb.group({
      name: ["", Validators.required],
      description: "",
      regular_price: ["", Validators.required],
      sale_price: null,
      no_license: true,
      is_recurring: 1,
      recurring_duration: null,
      recurring_cycle: null,
    });

    this.api.getOne(this.route.snapshot.params.uuid).subscribe(
      (product) => {
        this.product = product;
        this.form.patchValue(product);
      },
      (err) => console.log(err)
    );
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.submitted = true;
    if (this.form.invalid) {
      this.notify.show({
        type: "error",
        message: "Please check the form data once again",
      });
      return;
    }

    this.loading = true;
    this.api.update(this.form.value, this.route.snapshot.params.uuid).subscribe(
      (res) => {
        this.notify.show({ type: "success", message: "Product is updated" });
        this.loading = false;
        this.submitted = false;
      },
      (err) =>
        this.notify.show({
          type: "error",
          message: JSON.stringify(err.error),
        })
    );
  }
}
