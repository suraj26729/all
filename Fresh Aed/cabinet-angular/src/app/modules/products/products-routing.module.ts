import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ListComponent } from "./list/list.component";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";

const routes: Routes = [
  {
    path: "",
    component: ListComponent,
    pathMatch: "full",
    data: { title: "Products", breadcrumb: "Products" },
  },
  {
    path: "create",
    component: CreateComponent,
    data: { title: "Create Product", breadcrumb: "Create" },
  },
  {
    path: ":uuid/edit",
    component: EditComponent,
    data: { title: "Edit Product", breadcrumb: "Edit" },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsRoutingModule {}
