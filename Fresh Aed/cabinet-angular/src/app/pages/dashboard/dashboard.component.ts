import { Component, OnInit } from "@angular/core";
import { UserService } from "../../modules/user/user.service";
import * as moment from "moment";
import { AuthService } from "../../modules/auth/auth.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
  loading = true;
  summary: any;
  date: any;
  day: any;

  constructor(private api: UserService, public auth: AuthService) {}

  ngOnInit() {
    this.day = moment(new Date()).format("dddd");
    this.date = moment(new Date()).format("Do MMMM YYYY");

    this.api.dashboardSummary.subscribe(
      (res: any) => {
        this.loading = false;
        this.summary = res;
      },
      (err) => console.log(err)
    );
  }
}
