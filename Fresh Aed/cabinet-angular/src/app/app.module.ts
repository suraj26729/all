//Angular Core
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  BrowserModule,
  HammerGestureConfig,
  HAMMER_GESTURE_CONFIG,
} from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";

//Routing
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

//Layouts
import { CorporateLayout } from "./@pages/layouts";
//Layout Service - Required
import { pagesToggleService } from "./@pages/services/toggler.service";

//Shared Layout Components
import { SidebarComponent } from "./@pages/components/sidebar/sidebar.component";
import { HeaderComponent } from "./@pages/components/header/header.component";
import { SharedModule } from "./@pages/components/shared.module";
import { SharedModule as sm } from "./shared.module";
import { pgListViewModule } from "./@pages/components/list-view/list-view.module";
import { pgCardModule } from "./@pages/components/card/card.module";
import { pgCardSocialModule } from "./@pages/components/card-social/card-social.module";

//Basic Bootstrap Modules
import {
  BsDropdownModule,
  BsDatepickerModule,
  AccordionModule,
  AlertModule,
  ButtonsModule,
  CollapseModule,
  ModalModule,
  ProgressbarModule,
  TabsModule,
  TooltipModule,
  TypeaheadModule,
} from "ngx-bootstrap";

//Pages Globaly required Components - Optional
import { pgTabsModule } from "./@pages/components/tabs/tabs.module";
import { pgSwitchModule } from "./@pages/components/switch/switch.module";
import { ProgressModule } from "./@pages/components/progress/progress.module";

//Thirdparty Components / Plugins - Optional
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { AuthModule } from "./modules/auth/auth.module";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { EditUserModalComponent } from "./modules/user/edit-user-modal/edit-user-modal.component";

import { RootLayout } from "./@pages/layouts";
import { TitleService } from "./@pages/services/title-service.service";
// import { HorizontalMenuComponent } from "./@pages/components/horizontal-menu/horizontal-menu.component";
// import { QuickviewComponent } from "./@pages/components/quickview/quickview.component";
// import { SearchOverlayComponent } from "./@pages/components/search-overlay/search-overlay.component";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

//Hammer Config Overide
//https://github.com/angular/angular/issues/10541
export class AppHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    pinch: { enable: false },
    rotate: { enable: false },
  };
}

@NgModule({
  declarations: [
    AppComponent,
    CorporateLayout,
    SidebarComponent,
    HeaderComponent,
    DashboardComponent,
    EditUserModalComponent,
    RootLayout,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    SharedModule,
    sm,
    ProgressModule,
    pgListViewModule,
    pgCardModule,
    pgCardSocialModule,
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    pgTabsModule,
    PerfectScrollbarModule,
    pgSwitchModule,
  ],
  providers: [
    pagesToggleService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: AppHammerConfig,
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [EditUserModalComponent],
})
export class AppModule {
  constructor(private titleService: TitleService) {
    titleService.init();
  }
}
