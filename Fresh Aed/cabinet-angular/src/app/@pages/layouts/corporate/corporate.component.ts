import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { RootLayout } from "../root/root.component";

@Component({
  selector: "corporate-layout",
  templateUrl: "./corporate.component.html",
  styleUrls: ["./corporate.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class CorporateLayout extends RootLayout implements OnInit {
  menuLinks = [
    {
      label: "Dashboard",
      details: "",
      routerLink: "/dashboard",
      iconType: "pg",
      iconName: "home",
      thumbNailClass: "text-white",
    },
    {
      label: "My AED's",
      iconType: "pg",
      iconName: "layouts2",
      toggle: "close",
      submenu: [
        {
          label: "AED List",
          routerLink: "/aeds",
          iconType: "pg",
          iconName: "grid",
        },
        {
          label: "AED on Maps",
          routerLink: "/aeds/map",
          iconType: "pg",
          iconName: "map",
        },
      ],
    },
  ];

  ngOnInit() {
    if (this.auth.currentUserRole == 1) {
      this.menuLinks.push({
        label: "Users",
        details: "",
        routerLink: "/users",
        iconType: "pg",
        iconName: "grid",
        thumbNailClass: "text-white",
      });
      this.menuLinks.push({
        label: "Packages",
        iconType: "pg",
        iconName: "layouts2",
        toggle: "close",
        submenu: [
          {
            label: "All Packages",
            routerLink: "/products",
            iconType: "pg",
            iconName: "grid",
          },
          {
            label: "Add New",
            routerLink: "/products/create",
            iconType: "pg",
            iconName: "plus",
          },
        ],
      });
    }

    this.menuLinks.push({
      label: "Orders",
      details: "",
      routerLink: "/orders",
      iconType: "pg",
      iconName: "grid",
      thumbNailClass: "text-white",
    });

    this.menuLinks.push({
      label: "Licenses",
      details: "",
      routerLink: "/licenses",
      iconType: "pg",
      iconName: "grid",
      thumbNailClass: "text-white",
    });

    this.changeLayout("menu-pin");
    this.changeLayout("menu-behind");
    //Will sidebar close on screens below 1024
    this.autoHideMenuPin();
  }
}
