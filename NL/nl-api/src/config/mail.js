const nodemailer = require("nodemailer");
var sgTransport = require("nodemailer-sendgrid-transport");
const env = require("../../env");
const host = "smtp.softalliancetech.com";
const port = 587;
const from_email = "divakar@softalliancetech.com";
const from_email_pass = "DIVAKAR1234@";
let transporter = null;
// const host = "smtp.naartjielegal.com";
// const port = 587;
// const from_email = "support@naartjielegal.com ";
// const from_email_pass = "End@weni2018";

exports.send = async (to, subject, message, next) => {
  try {
    // let transporter = nodemailer.createTransport({
    //     host: "smtp.gmail.com",
    //     port: 465,
    //     secure: true,
    //     auth: {
    //         type: "OAuth2",
    //         user: env.gmail.email,
    //         clientId: env.gmail.clientId,
    //         clientSecret: env.gmail.clientSecret,
    //         refreshToken: env.gmail.refreshToken,
    //         accessToken: env.gmail.accessToken
    //     }
    // });
    // let transporter = nodemailer.createTransport({
    //   host: host,
    //   port: port,
    //   auth: {
    //     user: from_email,
    //     pass: from_email_pass
    //   },
    //   tls: { rejectUnauthorized: false },
    //   debug: true
    // });
    if (env.mail == "development") {
      transporter = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        auth: {
          user: "c6ebd410d299d5",
          pass: "e0af639c4fa85e",
        },
      });
    } else {
      transporter = nodemailer.createTransport(
        sgTransport({
          auth: {
            api_key: env.sendGridAPIKey,
          },
        })
      );
    }
    const mailOptions = {
      from: env.adminMail,
      to: to,
      subject: subject,
      generateTextFromHTML: true,
      html: message,
    };
    let result = await transporter.sendMail(mailOptions);
    transporter.close();
    return result;
  } catch (error) {
    next(error);
  }
};

exports.enquiry = async (to, subject, message, next) => {
  try {
    // let transporter = nodemailer.createTransport({
    //   host: host,
    //   port: port,
    //   auth: {
    //     user: from_email,
    //     pass: from_email_pass
    //   },
    //   tls: { rejectUnauthorized: false },
    //   debug: true
    // });
    let transporter = nodemailer.createTransport(
      sgTransport({
        auth: {
          api_key: env.sendGridAPIKey,
        },
      })
    );
    const mailOptions = {
      from: env.adminMail,
      to: to,
      cc: env.cc_email,
      subject: subject,
      generateTextFromHTML: true,
      html: message,
    };
    let result = await transporter.sendMail(mailOptions);
    transporter.close();
    return result;
  } catch (error) {
    next(error);
  }
};

exports.sendWithAttachment = async (to, subject, message, attachment, next) => {
  try {
    // let transporter = nodemailer.createTransport({
    //     host: "smtp.gmail.com",
    //     port: 465,
    //     secure: true,
    //     auth: {
    //         type: "OAuth2",
    //         user: env.gmail.email,
    //         clientId: env.gmail.clientId,
    //         clientSecret: env.gmail.clientSecret,
    //         refreshToken: env.gmail.refreshToken,
    //         accessToken: env.gmail.accessToken
    //     }
    // });
    // let transporter = nodemailer.createTransport({
    //   host: host,
    //   port: port,
    //   auth: {
    //     user: from_email,
    //     pass: from_email_pass
    //   },
    //   tls: { rejectUnauthorized: false },
    //   debug: true
    // });
    let transporter = nodemailer.createTransport(
      sgTransport({
        auth: {
          api_key: env.sendGridAPIKey,
        },
      })
    );
    let attach_url = `${env.baseUrl}${attachment}`;
    let filename = attach_url.substring(attach_url.lastIndexOf("/") + 1);
    const mailOptions = {
      from: env.adminMail,
      to: to,
      subject: subject,
      generateTextFromHTML: true,
      html: message,
      attachments: [
        {
          filename: filename,
          path: `${env.baseUrl}${attachment}`,
        },
      ],
    };
    let result = await transporter.sendMail(mailOptions);
    transporter.close();
    return result;
  } catch (error) {
    next(error);
  }
};

exports.sendInvoiceMail = async (to, subject, message, attachment, next) => {
  try {
    let transporter = nodemailer.createTransport(
      sgTransport({
        auth: {
          api_key: env.sendGridAPIKey,
        },
      })
    );
    console.log(attachment.substring(attachment.lastIndexOf("/") + 1));
    const mailOptions = {
      from: env.adminMail,
      to: to,
      subject: subject,
      generateTextFromHTML: true,
      html: message,
      attachments: [
        {
          filename: attachment.substring(attachment.lastIndexOf("/") + 1),
          contentType: "application/pdf",
          path: `${attachment}`,
        },
      ],
    };
    let result = await transporter.sendMail(mailOptions);
    transporter.close();
    return result;
  } catch (error) {
    next(error);
  }
};
