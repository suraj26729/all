const Sequelize = require("sequelize");

const sequelize = new Sequelize("naartjiedb", "root", "", {
  host: "127.0.0.1",
  port: 3306,
  dialect: "mysql",
  logging: false,
  operatorsAliases: false,
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },
});
sequelize.authenticate().catch(function (err) {
  if (err) {
    console.log("There is connection in ERROR");
  }
});

module.exports = sequelize;
