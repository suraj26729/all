const superagent = require("superagent");
const env = require("../../env");
const moment = require("moment");
const User = require("../models/User");
const common = require("./common");

exports.send = async (to, content) => {
  try {
    let url = `https://platform.clickatell.com/messages/http/send?apiKey=${
      env.CLICKATELL_KEY
    }&to=${to}&content=${encodeURI(content)}`;
    let result = await superagent.get(url);
    return result;
  } catch (error) {
    console.log(error);
  }
};

exports.sendInvoice = async (client_name, invoice_no, to, user_id = "") => {
  try {
    let firm_name = "";
    if (user_id != "") {
      let userIds = await common.firmUsers(user_id);
      let superAdmin = await User.findOne({
        where: { parent_user_id: null, role_id: 2, id: userIds },
        include: [{ association: "userdetail" }]
      });
      if (superAdmin != null && superAdmin.userdetail != undefined) {
        firm_name = superAdmin.userdetail.name_of_firm;
      }
    }

    firm_name = firm_name != "" ? `Kind Regards ${firm_name}` : firm_name;

    let messageTemplate = `Dear <Client>, Invoice <Inv> has been generated on ${moment(
      new Date()
    ).format("DD-MM-YYYY")}. Kindly check your email!! ${firm_name}`;

    messageTemplate = messageTemplate.replace("<Client>", client_name);
    messageTemplate = messageTemplate.replace("<Inv>", invoice_no);

    // console.log(messageTemplate);

    // to = "+919842640172";

    let url = `https://platform.clickatell.com/messages/http/send?apiKey=${
      env.CLICKATELL_KEY
    }&to=${to}&content=${encodeURI(messageTemplate)}`;
    let result = await superagent.get(url);
    return result;
  } catch (error) {
    console.log(error);
  }
};
