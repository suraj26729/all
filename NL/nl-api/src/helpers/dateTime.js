exports.weekStartDates = (date = new Date()) => {
  let weekMap = [6, 0, 1, 2, 3, 4, 5];
  var now = new Date(date);
  now.setHours(0, 0, 0, 0);
  var monday = new Date(now);
  monday.setDate(monday.getDate() - weekMap[monday.getDay()] - 1);
  var sunday = new Date(now);
  sunday.setDate(sunday.getDate() - weekMap[sunday.getDay()] + 5);
  sunday.setHours(23, 59, 59, 999);

  monday =
    monday.getFullYear() +
    "-" +
    (monday.getMonth() + 1 < 10
      ? "0" + (monday.getMonth() + 1)
      : monday.getMonth() + 1) +
    "-" +
    (monday.getDate() < 10 ? "0" + monday.getDate() : monday.getDate());
  sunday =
    sunday.getFullYear() +
    "-" +
    (sunday.getMonth() + 1 < 10
      ? "0" + (sunday.getMonth() + 1)
      : sunday.getMonth() + 1) +
    "-" +
    (sunday.getDate() < 10 ? "0" + sunday.getDate() : sunday.getDate());
  return { start: monday, end: sunday };
};

exports.getDateArray = (start, end) => {
  let arr = new Array(),
    dt = new Date(start);

  while (dt <= end) {
    arr.push(new Date(dt));
    dt.setDate(dt.getDate() + 1);
  }

  return arr;
};

exports.getTimesArray = () => {
  var x = 60; //minutes interval
  var times = []; // time array
  var tt = 0; // start time
  var ap = ["am", "pm"]; // AM-PM

  //loop to increment the time and push results in array
  for (var i = 0; tt < 24 * 60; i++) {
    var hh = Math.floor(tt / 60); // getting hours of day in 0-24 format
    var mm = tt % 60; // getting minutes of the hour in 0-55 format
    times[i] =
      ("0" + (hh % 12)).slice(-2) +
      ":" +
      ("0" + mm).slice(-2) +
      " " +
      ap[Math.floor(hh / 12)]; // pushing data in array in [00:00 - 12:00 AM/PM format]
    tt = tt + x;
  }

  return times;
};

exports.getTime24Array = () => {
  var x = 60; //minutes interval
  var times = []; // time array
  var tt = 0; // start time

  for (var i = 0; tt < 24 * 60; i++) {
    var hh = Math.floor(tt / 60);
    var mm = tt % 60;
    times[i] = (hh < 10 ? "0" + hh : hh) + ":" + ("0" + mm).slice(-2); // pushing data in array in [00:00 - 12:00 AM/PM format]
    tt = tt + x;
  }

  return times;
};

exports.convertTime = time => {
  var hours = Number(time.match(/^(\d+)/)[1]);
  var minutes = Number(time.match(/:(\d+)/)[1]);
  var AMPM = time.match(/\s(.*)$/)[1];
  if (AMPM == "pm" && hours < 12) hours = hours + 12;
  if (AMPM == "am" && hours == 12) hours = hours - 12;
  var sHours = hours.toString();
  var sMinutes = minutes.toString();
  if (hours < 10) sHours = "0" + sHours;
  if (minutes < 10) sMinutes = "0" + sMinutes;
  return sHours + ":" + sMinutes;
};

exports.convertTo24Hour = time12h => {
  const [time, modifier] = time12h.split(" ");

  let [hours, minutes] = time.split(":");

  if (hours === "12") {
    hours = "00";
  }

  if (modifier === "pm") {
    hours = parseInt(hours, 10) + 12;
  }

  return `${hours}:${minutes}:00`;
};
