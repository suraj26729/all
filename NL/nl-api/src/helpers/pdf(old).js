const pdf = require("html-pdf");
const fs = require("fs");
const moment = require("moment");
const User = require("../models/User");
const CaseThirdparty = require("../models/CaseThirdparty");
const CaseContacts = require("../models/CaseContacts");
const DefaultMessage = require("../models/accounts/DefaultMessage");
const BankDetails = require("../models/accounts/BankDetails");
const env = require("../../env");
const iplocation = require("iplocation").default;
const Moment = require("moment");
const {
  getCountries,
  getCurrencySymbol,
  getCurrencySymbolFromIso2
} = require("country-from-iso2");
const http = require("http");
var request = require("request").defaults({ encoding: null });
const fetch = require("fetch-base64");
const Tax = require("../models/accounts/Tax");
exports.generateQuotePDF = async params => {
  try {
    let name =
      "Quote-" +
      moment(new Date()).format("DD-MM-YYYY") +
      "-" +
      params.quote.uuid +
      ".pdf";
    let content = fs.readFileSync("./src/pdf-templates/quote.html", "utf8");
    let baseUrl = env.baseUrl;

    let user = await User.findByPk(params.user_id, {
      include: [{ association: "userdetail" }]
    });
    if (user.parent_user_id != null) {
      user = await User.findByPk(user.parent_user_id, {
        include: [{ association: "userdetail" }]
      });
    }

    let firmlogo =
      user.userdetail.logo != null
        ? "https://naartjielegal.com/api/" +
          user.userdetail.logo.replace(/\\/g, "/")
        : baseUrl + "assets/images/logo-dark.png";
    let firmaddress = user.userdetail.address;
    let firmphone = user.userdetail.contact1;
    let firmemail = user.email;
    let to = {
      name: "",
      email: "",
      address1: "",
      address2: "",
      zip: "",
      phone: ""
    };
    let thirdparty = null;
    if (params.quote.case_id != null) {
      thirdparty = await CaseContacts.findOne({
        where: {
          case_id: params.quote.matter.id,
          c_id: params.quote.thirdparty.id
        },
        include: [
          //{ association: "clientthirdparty" },
          { association: "clientcontact" }
        ]
      });
    }

    let defaultMessage = await DefaultMessage.findOne({
      where: { user_id: user.id }
    });
    let bankDetails = await BankDetails.findOne({
      where: { user_id: user.id }
    });

    if (params.quote.client.type == "i") {
      to.companyname = "";
      to.name =
        params.quote.client.firstname + " " + params.quote.client.surname;
      to.email = params.quote.client.email;
      to.address1 = params.quote.client.address1;
      to.address2 = params.quote.client.address2;
      to.zip = params.quote.client.zipcode;
      to.phone = params.quote.client.mobile;
    } else if (params.quote.client.type == "c" || thirdparty == null) {
      to.companyname =
        params.quote.client.firstname + " " + params.quote.client.surname;
      to.name = params.quote.client.companyname;
      to.email = params.quote.client.email;
      to.address1 = params.quote.client.address1;
      to.address2 = params.quote.client.address2;
      to.zip = params.quote.client.zipcode;
      to.phone = params.quote.client.mobile;
    } else {
      to.companyname = "";
      to.name =
        thirdparty.clientcontact.firstname +
        " " +
        thirdparty.clientcontact.surname;
      to.email = thirdparty.email;
      to.address1 = thirdparty.address1;
      to.address2 = thirdparty.address2;
      to.zip = thirdparty.zipcode;
      to.phone = thirdparty.mobile;
    }

    //items
    let items = params.quote.items;
    let subTotal = 0.0;
    let taxTotal = 0.0;
    let html = "";
    let i = 1;

    ///CURRENCY SYMBOL////
    var currency = params.currency;
    var cs = await getCurrencySymbol(currency);
    // let location = await iplocation(ip);
    // let cc = await getCurrencySymbolFromIso2(location.countryCode);
    var currencySymbol = "";
    if (cs) {
      currencySymbol = cs;
    } else {
      currencySymbol = "R";
    }
    ///CURRENCY SYMBOL ENDS////

    for (let item of items) {
      let tax = await Tax.findOne({
        where: { id: item.tax_id }
      });
      // console.log(tax.name);
      // console.log(tax.percent);
      var product_price = parseFloat(item.price) * parseFloat(item.quantity);
      var vat = (parseFloat(item.total) - parseFloat(product_price)).toFixed(2);
      subTotal = parseFloat(subTotal) + parseFloat(item.price);
      taxTotal =
        parseFloat(taxTotal) +
        (parseFloat(item.total) - parseFloat(item.price));
      html += `<tr>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${i}</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${
                  item.description
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${
                  item.costtype.name
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${parseFloat(
                  item.price
                ).toFixed(2)}</td>
                <td style="text-align:center;font-size:9px;vertical-align:top;">${
                  item.quantity
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">
                ${currencySymbol + " " + vat}
                <p style="font-size:5px;">${tax.name +
                  " " +
                  "(" +
                  tax.percent +
                  "%)"}</p>
                </td>
                <td style="text-align:center;font-size:9px;vertical-align:top;">${currencySymbol +
                  " " +
                  parseFloat(item.total).toFixed(2)}</td>  
              </tr>`;
      i += 1;
    }

    var t = firmlogo;
    var lastslashindex = t.lastIndexOf("/");
    var text = t.substring(lastslashindex + 1);

    let phone = await fetch.remote(
      "http://localhost:3000/api/assets/images/phone.jpg",
      "phone.jpg"
    );
    phones = phone[1];
    let email = await fetch.remote(
      "http://localhost:3000/api/assets/images/mail.jpg",
      "mail.jpg"
    );
    emails = email[1];

    let data = await fetch.remote(firmlogo, text);
    datas = data[1];
    var vatR = "";
    if (params.quote.vat_ref) {
      vatR = params.quote.vat_ref;
    }
    content = content.replace("**vatreference**", vatR);
    content = content.replace("**logophone**", phones);
    content = content.replace("**logoemail**", emails);
    content = content.replace("**logoed**", datas);

    content = content.replace("**firmdlogo**", firmlogo);
    content = content.replace("**firmlogo**", firmlogo);
    content = content.replace("**firmaddress**", firmaddress);
    content = content.replace("**firmphone**", firmphone);
    content = content.replace("**firmemail**", firmemail);

    content = content.replace("**toname**", to.name);
    content = content.replace("**tocontactname**", to.companyname);
    content = content.replace("**toemail**", to.email);
    content = content.replace("**toaddress1**", to.address1);
    content = content.replace("**toaddress2**", to.address2);
    content = content.replace("**tozip**", to.zip);

    content = content.replace("**quoteno**", params.quote.quote_number);
    content = content.replace(
      "**quotedate**",
      moment(params.quote.created_at).format("DD-MM-YYYY")
    );
    if (params.quote.case_id != null)
      content = content.replace("**caseno**", params.quote.matter.casenumber);
    content = content.replace("**clientref**", params.quote.file_no);
    if (params.quote.thirdparty != null) {
      content = content.replace(
        "**thirdparty**",
        params.quote.thirdparty.firstname +
          " " +
          params.quote.thirdparty.surname
      );
    }

    content = content.replace("**items**", html);
    // content = content.replace("**subtotal**", "R " + subTotal.toFixed(2));
    // content = content.replace("**taxtotal**", "R " + taxTotal.toFixed(2));
    // content = content.replace(
    //     "**total**",
    //     "R " + params.quote.total.toFixed(2)
    // );
    // content = content.replace(
    //     "**total**",
    //     "R " + params.quote.total.toFixed(2)
    // );

    content = content.replace(
      "**subtotal**",
      currencySymbol + " " + subTotal.toFixed(2)
    );
    content = content.replace(
      "**taxtotal**",
      currencySymbol + " " + taxTotal.toFixed(2)
    );
    content = content.replace(
      "**total**",
      currencySymbol + " " + params.quote.total.toFixed(2)
    );
    content = content.replace(
      "**total**",
      currencySymbol + " " + params.quote.total.toFixed(2)
    );

    let header = fs.readFileSync("./src/pdf-templates/header.html", "utf8");

    header = header.replace("**phonelogo**", phones);
    header = header.replace("**emaillogo**", emails);
    header = header.replace("**logohead**", datas);
    header = header.replace("**firmaddress**", firmaddress);
    header = header.replace("**firmphone**", firmphone);
    header = header.replace("**firmemail**", firmemail);

    let footer = fs.readFileSync("./src/pdf-templates/footer.html", "utf8");

    if (bankDetails == null) {
      footer = footer.replace("**bankdetail**", "");
    } else {
      footer = footer.replace("**bankdetail**", bankDetails.message);
    }

    if (defaultMessage == null) {
      footer = footer.replace("**defaultmessage**", "");
    } else {
      footer = footer.replace("**defaultmessage**", params.quote.message);
    }

    footer = footer.replace("**phoneno**", to.phone);

    footer = footer.replace("**email**", to.email);

    footer = footer.replace(
      "**firmownername**",
      user.userdetail.firstname + " " + user.userdetail.lastname
    );

    footer = footer.replace(
      "**trustbal**",
      currencySymbol + " " + params.balance.toFixed(2) || 0
    );

    if (user.userdetail.signature == null) {
      footer = footer.replace("**sign**", "");
    } else {
      footer = footer.replace(
        "**sign**",
        "<img src='" +
          baseUrl +
          user.userdetail.signature +
          "' alt='' height='65' />"
      );
    }

    footer +=
      '<p style="font-size: 10px; text-align: right;">Page {{page}} of {{pages}}</p>';

    let result = await pdf
      .create(content, {
        header: {
          height: "30mm",
          contents: header
        },
        orientation: "portrait",
        format: "letter",
        border: "20px",
        footer: {
          height: "150px",
          contents: footer
        }
      })
      .toFile("./uploads/pdf/quote/" + name, (err, res) => {});
    return "uploads/pdf/quote/" + name;
  } catch (err) {
    console.log(err);
  }
};

exports.generateInvoicePDF = async params => {
  try {
    var datas = "";
    var phones = "";
    var emails = "";
    let name =
      "Invoice-" +
      moment(new Date()).format("DD-MM-YYYY") +
      "-" +
      params.invoice.uuid +
      ".pdf";
    let content = fs.readFileSync("./src/pdf-templates/invoice.html", "utf8");
    let baseUrl = env.baseUrl;

    let user = await User.findByPk(params.user_id, {
      include: [{ association: "userdetail" }]
    });
    if (user.parent_user_id != null) {
      user = await User.findByPk(user.parent_user_id, {
        include: [{ association: "userdetail" }]
      });
    }

    let firmlogo =
      user.userdetail.logo != null
        ? "https://naartjielegal.com/api/" +
          user.userdetail.logo.replace(/\\/g, "/")
        : baseUrl + "assets/images/logo-dark.png";
    let firmaddress = user.userdetail.address;
    let firmphone = user.userdetail.contact1;
    let firmemail = user.email;
    let to = {
      name: "",
      email: "",
      address1: "",
      address2: "",
      zip: "",
      phone: ""
    };
    let thirdparty = null;
    if (params.invoice.thirdparty) {
      thirdparty = await CaseContacts.findOne({
        where: {
          case_id: params.invoice.matter.id,
          c_id: params.invoice.thirdparty.id
        },
        include: [
          //{ association: "clientthirdparty" },
          { association: "clientcontact" }
        ]
      });
    }

    let defaultMessage = await DefaultMessage.findOne({
      where: { user_id: user.id }
    });
    let bankDetails = await BankDetails.findOne({
      where: { user_id: user.id }
    });

    if (params.invoice.client.type == "i" || thirdparty == null) {
      to.companyname = "";
      to.name =
        params.invoice.client.firstname + " " + params.invoice.client.surname;
      to.email = params.invoice.client.email;
      to.address1 = params.invoice.client.address1;
      to.address2 = params.invoice.client.address2;
      to.zip = params.invoice.client.zipcode;
      to.phone = params.invoice.client.mobile;
    } else if (params.invoice.client.type == "c" || thirdparty == null) {
      to.companyname =
        params.invoice.client.firstname + " " + params.invoice.client.surname;
      to.name = params.invoice.client.companyname;
      to.email = params.invoice.client.email;
      to.address1 = params.invoice.client.address1;
      to.address2 = params.invoice.client.address2;
      to.zip = params.invoice.client.zipcode;
      to.phone = params.invoice.client.mobile;
    } else {
      to.companyname = "";
      to.name =
        thirdparty.clientcontact.firstname +
        " " +
        thirdparty.clientcontact.surname;
      to.email = thirdparty.email;
      to.address1 = thirdparty.address1;
      to.address2 = thirdparty.address2;
      to.zip = thirdparty.zipcode;
      to.phone = thirdparty.mobile;
    }

    //items
    let items = params.invoice.items;
    let subTotal = 0.0;
    let taxTotal = 0.0;
    let html = "";
    let i = 1;

    ///CURRENCY SYMBOL////
    var currency = params.currency;
    var cs = await getCurrencySymbol(currency);
    // let location = await iplocation(ip);
    // let cc = await getCurrencySymbolFromIso2(location.countryCode);
    var currencySymbol = "";
    if (cs) {
      currencySymbol = cs;
    } else {
      currencySymbol = "R";
    }
    ///CURRENCY SYMBOL ENDS////
    //console.log(currencySymbol);

    for (let item of items) {
      let tax = await Tax.findOne({
        where: { id: item.tax_id }
      });
      // console.log(tax.name);
      // console.log(tax.percent);
      var product_price = parseFloat(item.price) * parseFloat(item.quantity);
      var vat = (parseFloat(item.total) - parseFloat(product_price)).toFixed(2);
      subTotal = parseFloat(subTotal) + parseFloat(item.price);
      taxTotal =
        parseFloat(taxTotal) +
        (parseFloat(item.total) - parseFloat(item.price));

      html += `<tr>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${i}</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${
                  item.description
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${
                  item.costtype.name
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${parseFloat(
                  item.price
                ).toFixed(2)}</td>
                <td style="text-align:center;font-size:9px;vertical-align:top;">${
                  item.quantity
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">
                ${currencySymbol + " " + vat}
                <p style="font-size:5px;">${tax.name +
                  " " +
                  "(" +
                  tax.percent +
                  "%)"}</p>
                </td>
                <td style="text-align:center;font-size:9px;vertical-align:top;">${currencySymbol +
                  " " +
                  parseFloat(item.total).toFixed(2)}</td>
              </tr>`;
      i += 1;
    }

    var t = user.userdetail.logo;
    var lastslashindex = t.lastIndexOf("/");
    var text = t.substring(lastslashindex + 1);
    let phone = await fetch.remote(
      "http://localhost:3000/api/assets/images/phone.jpg",
      "phone.jpg"
    );
    phones = phone[1];
    let email = await fetch.remote(
      "http://localhost:3000/api/assets/images/mail.jpg",
      "mail.jpg"
    );
    emails = email[1];
    let data = await fetch.remote(firmlogo, text);
    datas = data[1];
    // console.log(datas);
    var vatR = "";
    if (params.invoice.vat_ref) {
      vatR = params.invoice.vat_ref;
    }
    // console.log(datas);
    content = content.replace("**vatreference**", vatR);
    content = content.replace("**logophone**", phones);
    content = content.replace("**logoemail**", emails);
    content = content.replace("**logoed**", datas);

    content = content.replace("**firmdlogo**", firmlogo);

    content = content.replace("**firmaddress**", firmaddress);
    content = content.replace("**firmphone**", firmphone);
    content = content.replace("**firmemail**", firmemail);

    content = content.replace("**toname**", to.name);
    content = content.replace("**tocontactname**", to.companyname);
    content = content.replace("**toemail**", to.email);
    content = content.replace("**toaddress1**", to.address1);
    content = content.replace("**toaddress2**", to.address2);
    content = content.replace("**tozip**", to.zip);

    content = content.replace("**invoiceno**", params.invoice.invoice_number);
    content = content.replace(
      "**invoicedate**",
      moment(params.invoice.created_at).format("DD-MM-YYYY")
    );
    content = content.replace("**caseno**", params.invoice.matter.casenumber);
    var ClientRef = "";
    if (params.invoice.file_no) {
      ClientRef = params.invoice.file_no;
    }
    content = content.replace("**clientref**", ClientRef);
    if (params.invoice.thirdparty) {
      content = content.replace(
        "**thirdparty**",
        params.invoice.thirdparty.firstname +
          " " +
          params.invoice.thirdparty.surname
      );
    } else {
      content = content.replace("**clientref**", "");
      content = content.replace("**thirdparty**", "");
    }

    content = content.replace("**items**", html);
    content = content.replace(
      "**subtotal**",
      currencySymbol + " " + subTotal.toFixed(2)
    );
    content = content.replace(
      "**taxtotal**",
      currencySymbol + " " + taxTotal.toFixed(2)
    );
    content = content.replace(
      "**total**",
      currencySymbol + " " + params.invoice.total.toFixed(2)
    );
    content = content.replace(
      "**total**",
      currencySymbol + " " + params.invoice.total.toFixed(2)
    );

    let header = fs.readFileSync("./src/pdf-templates/header.html", "utf8");

    header = header.replace("**phonelogo**", phones);
    header = header.replace("**emaillogo**", emails);
    header = header.replace("**logohead**", datas);
    header = header.replace("**firmaddress**", firmaddress);
    header = header.replace("**firmphone**", firmphone);
    header = header.replace("**firmemail**", firmemail);
    let footer = fs.readFileSync("./src/pdf-templates/footer.html", "utf8");

    if (bankDetails == null) {
      footer = footer.replace("**bankdetail**", "");
    } else {
      footer = footer.replace("**bankdetail**", bankDetails.message);
    }

    if (defaultMessage == null) {
      footer = footer.replace("**defaultmessage**", "");
    } else {
      footer = footer.replace("**defaultmessage**", defaultMessage.message);
    }

    footer = footer.replace("**phoneno**", to.phone);

    footer = footer.replace("**email**", to.email);

    footer = footer.replace(
      "**firmownername**",
      user.userdetail.firstname + " " + user.userdetail.lastname
    );

    footer = footer.replace(
      "**trustbal**",
      currencySymbol + " " + params.balance.toFixed(2) || 0
    );

    if (user.userdetail.signature == null) {
      footer = footer.replace("**sign**", "");
    } else {
      footer = footer.replace(
        "**sign**",
        "<img src='" +
          baseUrl +
          user.userdetail.signature +
          "' alt='' height='65' />"
      );
    }

    footer +=
      '<p style="font-size: 10px; text-align: right;">Page {{page}} of {{pages}}</p>';

    let result = await pdf
      .create(content, {
        header: {
          height: "30mm",
          contents: header
        },
        orientation: "portrait",
        format: "letter",
        border: "20px",
        footer: {
          height: "150px",
          contents: footer
        }
      })
      .toFile("./uploads/pdf/invoice/" + name, (err, res) => {});
    return "uploads/pdf/invoice/" + name;
  } catch (err) {
    console.log(err);
  }
};

// exports.generateInvoicePDF = async params => {
//     try {
//         let name =
//             "Invoice-" +
//             moment(new Date()).format("DD-MM-YYYY") +
//             "-" +
//             params.invoice.uuid +
//             ".pdf";
//         let content = fs.readFileSync("./src/pdf-templates/invoice.html", "utf8");
//         let baseUrl = env.baseUrl;

//         let user = await User.findByPk(params.user_id, {
//             include: [{ association: "userdetail" }]
//         });
//         if (user.parent_user_id != null) {
//             user = await User.findByPk(user.parent_user_id, {
//                 include: [{ association: "userdetail" }]
//             });
//         }

//         let firmlogo =
//             user.userdetail.logo != null ?
//             baseUrl + user.userdetail.logo.replace(/\\/g, "/") :
//             baseUrl + "assets/images/logo-dark.png";
//         let firmaddress = user.userdetail.address;
//         let firmphone = user.userdetail.contact1;
//         let firmemail = user.email;
//         let to = {
//             name: "",
//             email: "",
//             address1: "",
//             address2: "",
//             zip: "",
//             phone: ""
//         };
//         let thirdparty = null;
//         if (params.invoice.thirdparty) {
//             thirdparty = await CaseContacts.findOne({
//                 where: {
//                     case_id: params.invoice.matter.id,
//                     c_id: params.invoice.thirdparty.id
//                 },
//                 include: [
//                     //{ association: "clientthirdparty" },
//                     { association: "clientcontact" }
//                 ]
//             });
//         }

//         let defaultMessage = await DefaultMessage.findOne({
//             where: { user_id: user.id }
//         });
//         let bankDetails = await BankDetails.findOne({
//             where: { user_id: user.id }
//         });

//         if (params.invoice.client.type == "i" || thirdparty == null) {
//             to.name =
//                 params.invoice.client.firstname + " " + params.invoice.client.surname;
//             to.email = params.invoice.client.email;
//             to.address1 = params.invoice.client.address1;
//             to.address2 = params.invoice.client.address2;
//             to.zip = params.invoice.client.zipcode;
//             to.phone = params.invoice.client.mobile;
//         } else if (params.invoice.client.type == "c" || thirdparty == null) {
//             to.name = params.invoice.client.companyname;
//             to.email = params.invoice.client.email;
//             to.address1 = params.invoice.client.address1;
//             to.address2 = params.invoice.client.address2;
//             to.zip = params.invoice.client.zipcode;
//             to.phone = params.invoice.client.mobile;
//         } else {
//             to.name =
//                 thirdparty.clientcontact.firstname +
//                 " " +
//                 thirdparty.clientcontact.surname;
//             to.email = thirdparty.email;
//             to.address1 = thirdparty.address1;
//             to.address2 = thirdparty.address2;
//             to.zip = thirdparty.zipcode;
//             to.phone = thirdparty.mobile;
//         }

//         //items
//         let items = params.invoice.items;
//         let subTotal = 0.0;
//         let taxTotal = 0.0;
//         let html = "";
//         let i = 1;

//         for (let item of items) {
//             subTotal = parseFloat(subTotal) + parseFloat(item.price);
//             taxTotal =
//                 parseFloat(taxTotal) +
//                 (parseFloat(item.total) - parseFloat(item.price));
//             html += `<tr>
//                 <td style="text-align:center;font-size:9px;vertical-align:top;">${i}</td>
//                 <td style="text-align:center;font-size:9px;vertical-align:top;">${moment(
//                   item.created_at
//                 ).format("DD-MM-YYYY")}</td>
//                 <td style="text-align:center;font-size:9px;vertical-align:top;">${
//                   item.description
//                 }</td>
//                 <td style="text-align:center;font-size:9px;vertical-align:top;">${
//                   item.costtype.name
//                 }</td>
//                 <td style="text-align:center;font-size:9px;vertical-align:top;">${
//                   item.quantity
//                 }</td>
//                 <td style="text-align:center;font-size:9px;vertical-align:top;">R ${
//                   item.price
//                 }</td>
//               </tr>`;
//             i += 1;
//         }

//         content = content.replace("**firmlogo**", firmlogo);
//         content = content.replace("**firmaddress**", firmaddress);
//         content = content.replace("**firmphone**", firmphone);
//         content = content.replace("**firmemail**", firmemail);

//         content = content.replace("**toname**", to.name);
//         content = content.replace("**toemail**", to.email);
//         content = content.replace("**toaddress1**", to.address1);
//         content = content.replace("**toaddress2**", to.address2);
//         content = content.replace("**tozip**", to.zip);

//         content = content.replace("**invoiceno**", params.invoice.invoice_number);
//         content = content.replace(
//             "**invoicedate**",
//             moment(params.invoice.created_at).format("DD-MM-YYYY")
//         );
//         content = content.replace("**caseno**", params.invoice.matter.casenumber);
//         var ClientRef = "";
//         if (params.invoice.file_no) {
//             ClientRef = params.invoice.file_no;
//         }
//         content = content.replace("**clientref**", ClientRef);
//         if (params.invoice.thirdparty) {

//             content = content.replace(
//                 "**thirdparty**",
//                 params.invoice.thirdparty.firstname +
//                 " " +
//                 params.invoice.thirdparty.surname
//             );
//         } else {
//             content = content.replace("**clientref**", "");
//             content = content.replace("**thirdparty**", "");
//         }

//         content = content.replace("**items**", html);
//         content = content.replace("**subtotal**", "R " + subTotal.toFixed(2));
//         content = content.replace("**taxtotal**", "R " + taxTotal.toFixed(2));
//         content = content.replace(
//             "**total**",
//             "R " + params.invoice.total.toFixed(2)
//         );
//         content = content.replace(
//             "**total**",
//             "R " + params.invoice.total.toFixed(2)
//         );

//         let footer = fs.readFileSync("./src/pdf-templates/footer.html", "utf8");

//         if (bankDetails == null) {
//             footer = footer.replace("**bankdetail**", "");
//         } else {
//             footer = footer.replace("**bankdetail**", bankDetails.message);
//         }

//         if (defaultMessage == null) {
//             footer = footer.replace("**defaultmessage**", "");
//         } else {
//             footer = footer.replace("**defaultmessage**", defaultMessage.message);
//         }

//         footer = footer.replace("**phoneno**", to.phone);

//         footer = footer.replace("**email**", to.email);

//         footer = footer.replace(
//             "**firmownername**",
//             user.userdetail.firstname + " " + user.userdetail.lastname
//         );

//         footer = footer.replace(
//             "**trustbal**",
//             "R " + params.balance.toFixed(2) || 0
//         );

//         if (user.userdetail.signature == null) {
//             footer = footer.replace("**sign**", "");
//         } else {
//             footer = footer.replace(
//                 "**sign**",
//                 "<img src='" +
//                 baseUrl +
//                 user.userdetail.signature +
//                 "' alt='' height='65' />"
//             );
//         }

//         footer +=
//             '<p style="font-size: 10px; text-align: right;">Page {{page}} of {{pages}}</p>';

//         let result = await pdf
//             .create(content, {
//                 orientation: "portrait",
//                 format: "letter",
//                 border: "20px",
//                 footer: {
//                     height: "150px",
//                     contents: footer
//                 }
//             })
//             .toFile("./uploads/pdf/invoice/" + name, (err, res) => {});
//         return "uploads/pdf/invoice/" + name;
//     } catch (err) {
//         console.log(err);
//     }
// };

exports.generateCreditNotePDF = async params => {
  try {
    let name =
      "CreditNote-" +
      moment(new Date()).format("DD-MM-YYYY") +
      "-" +
      params.creditnote.uuid +
      ".pdf";
    let content = fs.readFileSync(
      "./src/pdf-templates/creditnote.html",
      "utf8"
    );
    let baseUrl = env.baseUrl;

    let user = await User.findByPk(params.user_id, {
      include: [{ association: "userdetail" }]
    });
    if (user.parent_user_id != null) {
      user = await User.findByPk(user.parent_user_id, {
        include: [{ association: "userdetail" }]
      });
    }

    let firmlogo =
      user.userdetail.logo != null
        ? "https://naartjielegal.com/api/" +
          user.userdetail.logo.replace(/\\/g, "/")
        : baseUrl + "assets/images/logo-dark.png";
    let firmaddress = user.userdetail.address;
    let firmphone = user.userdetail.contact1;
    let firmemail = user.email;
    let to = {
      name: "",
      email: "",
      address1: "",
      address2: "",
      zip: "",
      phone: ""
    };
    let thirdparty = await CaseContacts.findOne({
      where: {
        case_id: params.creditnote.matter.id,
        c_id: params.creditnote.thirdparty.id
      },
      include: [
        //{ association: "clientthirdparty" },
        { association: "clientcontact" }
      ]
    });

    let defaultMessage = await DefaultMessage.findOne({
      where: { user_id: user.id }
    });
    let bankDetails = await BankDetails.findOne({
      where: { user_id: user.id }
    });

    if (params.creditnote.client.type == "i") {
      to.companyname = "";
      to.name =
        params.creditnote.client.firstname +
        " " +
        params.creditnote.client.surname;
      to.email = params.creditnote.client.email;
      to.address1 = params.creditnote.client.address1;
      to.address2 = params.creditnote.client.address2;
      to.zip = params.creditnote.client.zipcode;
      to.phone = params.creditnote.client.mobile;
    } else if (params.creditnote.client.type == "c" || thirdparty == null) {
      to.companyname =
        params.creditnote.client.firstname +
        " " +
        params.creditnote.client.surname;
      to.name = params.creditnote.client.companyname;
      to.email = params.creditnote.client.email;
      to.address1 = params.creditnote.client.address1;
      to.address2 = params.creditnote.client.address2;
      to.zip = params.creditnote.client.zipcode;
      to.phone = params.creditnote.client.mobile;
    } else {
      to.companyname = "";
      to.name =
        thirdparty.clientcontact.firstname +
        " " +
        thirdparty.clientcontact.surname;
      to.email = thirdparty.email;
      to.address1 = thirdparty.address1;
      to.address2 = thirdparty.address2;
      to.zip = thirdparty.zipcode;
      to.phone = thirdparty.mobile;
    }

    //items
    let items = params.creditnote.items;
    let subTotal = 0.0;
    let taxTotal = 0.0;
    let html = "";
    let i = 1;

    ///CURRENCY SYMBOL////
    var currency = params.currency;
    var cs = await getCurrencySymbol(currency);
    // let location = await iplocation(ip);
    // let cc = await getCurrencySymbolFromIso2(location.countryCode);
    var currencySymbol = "";
    if (cs) {
      currencySymbol = cs;
    } else {
      currencySymbol = "R";
    }

    console.log(currencySymbol);
    ///CURRENCY SYMBOL ENDS////

    for (let item of items) {
      let tax = await Tax.findOne({
        where: { id: item.tax_id }
      });
      // console.log(tax.name);
      // console.log(tax.percent);
      var product_price = parseFloat(item.price) * parseFloat(item.quantity);
      var vat = (parseFloat(item.total) - parseFloat(product_price)).toFixed(2);
      subTotal = parseFloat(subTotal) + parseFloat(item.price);
      taxTotal =
        parseFloat(taxTotal) +
        (parseFloat(item.total) - parseFloat(item.price));
      html += `<tr>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${i}</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${
                  item.description
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${
                  item.costtype.name
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">${parseFloat(
                  item.price
                ).toFixed(2)}</td>
                <td style="text-align:center;font-size:9px;vertical-align:top;">${
                  item.quantity
                }</td>
                <td style="text-align:left;font-size:9px;vertical-align:top;">
                ${currencySymbol + " " + vat}
                <p style="font-size:5px;">${tax.name +
                  " " +
                  "(" +
                  tax.percent +
                  "%)"}</p>
                </td>
                <td style="text-align:center;font-size:9px;vertical-align:top;">${currencySymbol +
                  " " +
                  parseFloat(item.total).toFixed(2)}</td>  
              </tr>`;

      // html += `<tr>
      //     <td style="text-align:center;font-size:9px;vertical-align:top;">${i}</td>
      //     <td style="text-align:center;font-size:9px;vertical-align:top;">${moment(
      //       item.created_at
      //     ).format("DD-MM-YYYY")}</td>
      //     <td style="text-align:center;font-size:9px;vertical-align:top;">${
      //       item.description
      //     }</td>
      //     <td style="text-align:center;font-size:9px;vertical-align:top;">${
      //       item.costtype.name
      //     }</td>
      //     <td style="text-align:center;font-size:9px;vertical-align:top;">${
      //       item.quantity
      //     }</td>
      //     <td style="text-align:left;font-size:9px;vertical-align:top;">
      //     ${currencySymbol+' '+ vat}
      //     <p style="font-size:5px;">${tax.name+" "+"("+tax.percent+"%)"}</p>
      //     </td>
      //     <td style="text-align:center;font-size:9px;vertical-align:top;">${currencySymbol+' '+item.total}</td>
      //   </tr>`;
      i += 1;
    }

    var t = firmlogo;
    var lastslashindex = t.lastIndexOf("/");
    var text = t.substring(lastslashindex + 1);

    let phone = await fetch.remote(
      "http://localhost:3000/api/assets/images/phone.jpg",
      "phone.jpg"
    );
    phones = phone[1];
    let email = await fetch.remote(
      "http://localhost:3000/api/assets/images/mail.jpg",
      "mail.jpg"
    );
    emails = email[1];

    let data = await fetch.remote(firmlogo, text);
    datas = data[1];
    var vatR = "";
    if (params.creditnote.vat_ref) {
      vatR = params.creditnote.vat_ref;
    }
    // console.log(datas);
    content = content.replace("**vatreference**", vatR);

    content = content.replace("**logophone**", phones);
    content = content.replace("**logoemail**", emails);
    content = content.replace("**logoed**", datas);

    content = content.replace("**firmdlogo**", firmlogo);

    content = content.replace("**firmlogo**", firmlogo);
    content = content.replace("**firmaddress**", firmaddress);
    content = content.replace("**firmphone**", firmphone);
    content = content.replace("**firmemail**", firmemail);

    content = content.replace("**toname**", to.name);
    content = content.replace("**tocontactname**", to.companyname);
    content = content.replace("**toemail**", to.email);
    content = content.replace("**toaddress1**", to.address1);
    content = content.replace("**toaddress2**", to.address2);
    content = content.replace("**tozip**", to.zip);

    content = content.replace(
      "**creditnoteno**",
      params.creditnote.credit_note_number
    );
    content = content.replace(
      "**creditnotedate**",
      moment(params.creditnote.created_at).format("DD-MM-YYYY")
    );
    content = content.replace(
      "**caseno**",
      params.creditnote.matter.casenumber
    );
    content = content.replace("**clientref**", params.creditnote.file_no);
    content = content.replace(
      "**thirdparty**",
      params.creditnote.thirdparty.firstname +
        " " +
        params.creditnote.thirdparty.surname
    );

    content = content.replace("**items**", html);

    content = content.replace(
      "**subtotal**",
      currencySymbol + " " + subTotal.toFixed(2)
    );
    content = content.replace(
      "**taxtotal**",
      currencySymbol + " " + taxTotal.toFixed(2)
    );
    content = content.replace(
      "**total**",
      currencySymbol + " " + params.creditnote.total.toFixed(2)
    );
    content = content.replace(
      "**total**",
      currencySymbol + " " + params.creditnote.total.toFixed(2)
    );

    let header = fs.readFileSync("./src/pdf-templates/header.html", "utf8");

    header = header.replace("**phonelogo**", phones);
    header = header.replace("**emaillogo**", emails);
    header = header.replace("**logohead**", datas);
    header = header.replace("**firmaddress**", firmaddress);
    header = header.replace("**firmphone**", firmphone);
    header = header.replace("**firmemail**", firmemail);

    let footer = fs.readFileSync("./src/pdf-templates/footer.html", "utf8");

    if (bankDetails == null) {
      footer = footer.replace("**bankdetail**", "");
    } else {
      footer = footer.replace("**bankdetail**", bankDetails.message);
    }

    if (defaultMessage == null) {
      footer = footer.replace("**defaultmessage**", "");
    } else {
      footer = footer.replace("**defaultmessage**", defaultMessage.message);
    }

    footer = footer.replace("**phoneno**", to.phone);

    footer = footer.replace("**email**", to.email);

    footer = footer.replace(
      "**firmownername**",
      user.userdetail.firstname + " " + user.userdetail.lastname
    );

    footer = footer.replace(
      "**trustbal**",
      currencySymbol + " " + params.balance.toFixed(2) || 0
    );

    if (user.userdetail.signature == null) {
      footer = footer.replace("**sign**", "");
    } else {
      footer = footer.replace(
        "**sign**",
        "<img src='" +
          baseUrl +
          user.userdetail.signature +
          "' alt='' height='65' />"
      );
    }
    footer +=
      '<p style="font-size: 10px; text-align: right;">Page {{page}} of {{pages}}</p>';

    let result = await pdf
      .create(content, {
        header: {
          height: "30mm",
          contents: header
        },
        orientation: "portrait",
        format: "letter",
        border: "20px",
        footer: {
          height: "150px",
          contents: footer
        }
      })
      .toFile("./uploads/pdf/creditnote/" + name, (err, res) => {});
    return "uploads/pdf/creditnote/" + name;
  } catch (err) {
    console.log(err);
  }
};

exports.generateTransactionsPDF = async (
  params,
  currentBalance,
  currencySymbol = "R"
) => {
  try {
    let name = `Transactions-${moment(new Date()).format("DD-MM-YYYY")}.pdf`;
    let items = "";

    for (let row of params) {
      items += `<tr>
              <td>${Moment(row.date).format("DD-MM-YYYY")}</td>
              <td>${row.invoice_id}</td>
              <td>${row.casenumber}</td>
              <td>${row.description}</td>
              <td>${currencySymbol + " " + row.debit}</td>
              <td>${currencySymbol + " " + row.credit}</td>
              <td>${currencySymbol + " " + row.balance}</td>
          </tr>`;
    }

    let content = `<table style="width: 100%;">
        <thead>
            <tr>
                <th style="font-size: 10px;color: #fff;background: #88bc45;padding:3px 3px;">Date</th>
                <th style="font-size: 10px;color: #fff;background: #88bc45;padding:3px 3px;">Invoice #</th>
                <th style="font-size: 10px;color: #fff;background: #88bc45;padding:3px 3px;">Case Number</th>
                <th style="font-size: 10px;color: #fff;background: #88bc45;padding:3px 3px;">Description</th>
                <th style="font-size: 10px;color: #fff;background: #88bc45;padding:3px 3px;">Debit</th>
                <th style="font-size: 10px;color: #fff;background: #88bc45;padding:3px 3px;">Credit</th>
                <th style="font-size: 10px;color: #fff;background: #88bc45;padding:3px 3px;">Balance</th>
            </tr>
        </thead>
        <tbody style="font-size: 9px;width: 100%;padding: 3px 0px;line-height: 24px;">
            <span style="padding: 3px 0px;">${items}</span>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="6" style="font-size: 10px;color: #fff;text-align: right;background: #88bc45;padding:3px 3px;">Account Current Balance</th>
            <th style="font-size: 10px;color: #fff;background: #88bc45;padding:3px 3px;">${currencySymbol +
              " " +
              Math.abs(currentBalance)}</th>
          </tr>
        </tfoot>
    </table>`;

    let result = await pdf
      .create(content, {
        orientation: "portrait",
        format: "letter",
        border: "20px"
      })
      .toFile("./uploads/pdf/transactions/" + name, (err, res) => {});
    return "uploads/pdf/transactions/" + name;
  } catch (err) {
    console.log(err);
  }
};
