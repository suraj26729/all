const User = require("../models/User");
const UserPermission = require("../models/UserPermission");

exports.firmUsersWithDeleted = async user_id => {
  try {
    let user = await User.findByPk(user_id);
    let superadmin = "";
    if (user.role_id == 3) {
      // If attorney
      let admin = await User.findByPk(user.parent_user_id);
      superadmin = admin.parent_user_id || admin.id;
    } else if (user.parent_user_id == null) {
      //If super admin
      superadmin = user.id;
    } else if (user.parent_user_id != null) {
      //If sub admin
      superadmin = user.parent_user_id;
    }

    let attoneys = await User.findAll({
      where: { parent_user_id: superadmin, role_id: 3 },
      paranoid: false
    }); //Get attorneys created by super admin
    let admins = await User.findAll({
      where: { parent_user_id: superadmin, role_id: 2 },
      paranoid: false
    }); //Get sub admins craeted by super admin
    let adminIds = admins.map(item => item.id);
    let adminAttorneys = await User.findAll({
      where: { parent_user_id: adminIds, role_id: 3 }
    });

    let userIds = [superadmin];
    for (let user of attoneys) userIds.push(user.id);
    for (let user of admins) userIds.push(user.id);
    for (let user of adminAttorneys) userIds.push(user.id);

    return userIds;
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.firmUsers = async user_id => {
  let user = await User.findByPk(user_id);
  let superadmin = "";
  if (user.role_id == 3) {
    // If attorney
    let admin = await User.findByPk(user.parent_user_id);
    superadmin = admin.parent_user_id || admin.id;
  } else if (user.parent_user_id == null) {
    //If super admin
    superadmin = user.id;
  } else if (user.parent_user_id != null) {
    //If sub admin
    superadmin = user.parent_user_id;
  }

  let attoneys = await User.findAll({
    where: { parent_user_id: superadmin, role_id: 3 }
  }); //Get attorneys created by super admin
  let admins = await User.findAll({
    where: { parent_user_id: superadmin, role_id: 2 }
  }); //Get sub admins craeted by super admin
  let adminIds = admins.map(item => item.id);
  let adminAttorneys = await User.findAll({
    where: { parent_user_id: adminIds, role_id: 3 }
  });

  let userIds = [superadmin];
  for (let user of attoneys) userIds.push(user.id);
  for (let user of admins) userIds.push(user.id);
  for (let user of adminAttorneys) userIds.push(user.id);

  return userIds;
};

exports.firmAdmins = async user_id => {
  let user = await User.findByPk(user_id);
  let superadmin = "";
  if (user.role_id == 3) {
    // If attorney
    let admin = await User.findByPk(user.parent_user_id);
    superadmin = admin.parent_user_id || admin.id;
  } else if (user.parent_user_id == null) {
    //If super admin
    superadmin = user.id;
  } else if (user.parent_user_id != null) {
    //If sub admin
    superadmin = user.parent_user_id;
  }

  let admins = await User.findAll({
    where: { parent_user_id: superadmin, role_id: 2, isactive: 1 }
  }); //Get sub admins craeted by super admin
  let adminIds = admins.map(item => item.id);

  let userIds = [superadmin];
  for (let user of admins) userIds.push(user.id);

  return userIds;
};

exports.Attorney = async user_id => {
  let userIds = [];
  let user = await User.findByPk(user_id);
  if (user.role_id == 1 || user.role_id == 2) {
    if (user.isattorney == 1) {
      userIds.push(user.id);
    } else {
      let sattorney = await User.findAll({
        where: { parent_user_id: user.id, role_id: 3, isactive: 1 }
      });
      for (sa of sattorney) {
        userIds.push(sa.id);
      }
    }
  } else if (user.role_id == 3) {
    // If attorney
    let attorney = await User.findByPk(user.id);
    if (attorney) {
      userIds.push(attorney.id);
      let subs = await User.findAll({
        where: { id: user.parent_user_id, isactive: 1 }
      });
      for (s of subs) {
        userIds.push(s.id);
      }
    }
    let attorney1 = await User.findByPk(user.parent_user_id);
    if (attorney1.isattorney == 1) {
      userIds.push(attorney1.id);
    }
  }
  let attorneys = await User.findAll({
    where: { parent_user_id: userIds, role_id: 3, isactive: 1 }
  }); //Get attorneys created by super Attorney
  let attorneyIds = attorneys.map(item => item.id);
  let subAttorneys = await User.findAll({
    where: { parent_user_id: attorneyIds, role_id: 3, isactive: 1 }
  });
  for (let user of attorneys) userIds.push(user.id);
  for (let user of subAttorneys) userIds.push(user.id);

  return userIds;
};

exports.Admin = async user_id => {
  let userIds = [];
  let user = await User.findByPk(user_id);
  if (user.role_id == 1 || user.role_id == 2) {
    if (user.isadmin == 1) {
      userIds.push(user.id);
    } else {
      let sadmin = await User.findAll({
        where: { parent_user_id: user.id, role_id: 2, isactive: 1 }
      });
      for (sa of sadmin) {
        userIds.push(sa.id);
      }
    }
  }
  let admins = await User.findAll({
    where: { parent_user_id: userIds, role_id: 2, isactive: 1 }
  }); //Get attorneys created by super Attorney
  let adminIds = admins.map(item => item.id);
  let subadmin = await User.findAll({
    where: { parent_user_id: adminIds, role_id: 2, isactive: 1 }
  });
  for (let user of admins) userIds.push(user.id);
  for (let user of subadmin) userIds.push(user.id);

  return userIds;
};

exports.otherDiaryUserIds = async user_id => {
  let user = await User.findByPk(user_id);
  let attorneyIds = [];

  if (user.role_id == 3 && user.parent_user_id != null) {
    let userPermissions = await UserPermission.findOne({
      where: { user_id: user_id, permission_id: 6 },
      include: [{ association: "calendarAttorneys" }]
    });
    attorneyIds = [parseInt(user_id)];
    if (userPermissions != null && userPermissions.calendarAttorneys.length) {
      for (let item of userPermissions.calendarAttorneys) {
        attorneyIds.push(item.attorney_id);
      }
    }
  } else if (user.role_id == 2 && user.parent_user_id != null) {
    let userPermissions = await UserPermission.findOne({
      where: { user_id: user_id, permission_id: 6 },
      include: [{ association: "calendarAttorneys" }]
    });
    attorneyIds = [parseInt(user_id)];
    if (userPermissions != null && userPermissions.calendarAttorneys.length) {
      for (let item of userPermissions.calendarAttorneys) {
        attorneyIds.push(item.attorney_id);
      }
    }
    let allIds = await this.firmUsers(user_id);
    //attorneyIds = [parseInt(user_id)];
    for (let id of allIds) {
      let user2 = await User.findByPk(id);
      if ((user2.role_id == 3 || user2.isattorney == 1) && user2.isactive == 1)
        attorneyIds.push(id);
    }
  } else {
    let allIds = await this.firmUsers(user_id);
    attorneyIds = [parseInt(user_id)];
    for (let id of allIds) {
      let user2 = await User.findByPk(id);
      if ((user2.role_id == 3 || user2.isattorney == 1) && user2.isactive == 1)
        attorneyIds.push(id);
    }
  }
  return attorneyIds;
};

exports.permittedUsers = async user_id => {
  let assigneeID = [user_id];
  let user = await User.findByPk(user_id);
  if (user.role_id == 2 && user.parent_user_id == null) {
    assigneeID = this.firmUsers(user_id);
  } else if (user.role_id == 2 && user.parent_user_id != null) {
    //Other diary admins
    assigneeID = this.otherDiaryUserIds(user_id);

    //All attorneys
    let ids = this.firmUsers(user_id);
    let firm_users = await User.findAll({ where: { id: ids, role_id: 3 } });
    for (let item of firm_users) assigneeID.push(item.id);
  } else if (user.role_id == 3 && user.parent_user_id != null) {
    //Other diary attorneys
    assigneeID = this.otherDiaryUserIds(user_id);
  } else {
    assigneeID = this.firmUsers(user_id);
  }

  return assigneeID;
};
