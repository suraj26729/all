const jwt = require("jsonwebtoken");
const env = require("../../env");

const checkToken = (req, res, next) => {
  let token = req.headers["x-access-token"] || req.headers["authorization"]; // Express headers are auto converted to lowercase

  if (token) {
    if (token.startsWith("Bearer ")) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }

    jwt.verify(token, env.jwtSecret, (err, decoded) => {
      if (err) {
        return res.json({
          success: 0,
          code: 401, 
          message: "Access is denied."
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.json({
      success: 0,
      code: 500,
      message: "Auth token is not supplied or invalid"
    });
  }
};

module.exports = checkToken;