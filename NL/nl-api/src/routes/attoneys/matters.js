const express = require("express");
const router = express.Router();
const Matter = require("../../models/CaseDetails");
const checkToken = require("../../middlewares/checkToken");

const middleware = {
  auth: checkToken
};

let limit = 10;

router.get("/:attorneyID", middleware.auth, async (req, res) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  let matters1 = await Matter.findAll({
    where: { priattorney: req.params.attorneyID },
    attributes: ["id"]
  });

  let matter2 = await Matter.findAll({
    distinct: true,
    attributes: ["id"],
    include: [
      {
        association: "assistattorney",
        where: { attorney_id: req.params.attorneyID }
      }
    ]
  });

  let ids = [];
  for (let row of matters1) {
    if (ids.indexOf(row.id) == -1) ids.push(row.id);
  }
  for (let row of matter2) {
    if (ids.indexOf(row.id) == -1) ids.push(row.id);
  }

  Matter.paginate({
    where: { id: ids },
    page: page,
    paginate: limit,
    attributes: [
      "id",
      "client_id",
      "prefix",
      "casenumber",
      "priattorney",
      "status"
    ],
    include: [
      {
        association: "clientdetail",
        attributes: ["id", "firstname", "surname"]
      },
      {
        association: "principleAttorney",
        attributes: ["id"],
        include: [
          { association: "userdetail", attributes: ["firstname", "lastname"] }
        ]
      },
      {
        association: "assistattorney",
        attributes: ["attorney_id"],
        include: [
          {
            association: "attorney",
            attributes: ["id"],
            include: [
              {
                association: "userdetail",
                attributes: ["firstname", "lastname"]
              }
            ]
          }
        ]
      }
    ]
  })
    .then(matters => {
      res.status(200).json(matters);
    })
    .catch(err => res.status(500).json({ error: err }));
});

module.exports = router;
