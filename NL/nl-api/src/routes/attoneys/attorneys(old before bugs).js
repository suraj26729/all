const express = require("express");
const router = express.Router();
const multer = require("multer");
const User = require("../../models/User");
const UserDetail = require("../../models/UserDetail");
const bcrypt = require("bcryptjs");
const env = require("../../../env");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const Op = sequelize.Op;
const paginate = require("sequelize-paginate");
const Moment = require('moment');
const mail = require("../../config/mail");
const fs = require("fs");

const middleware = {
    validations: [
        check("firstname").isString(),
        // check("username").isString(),
        // check("password").isString(),
        check("email").isEmail(),
        check("contact1").isMobilePhone("any"),
        check("address").isString(),
        check("yearofpractice").isInt(),
        check("user_id").isInt()
    ],
    auth: checkToken
};

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./uploads/attorneys/");
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "-" + file.originalname);
    }
});
const upload = multer({
    storage: storage,
    limits: { fileSize: 4194304 },
    fileFilter: (req, file, cb) => {
        if (
            file.mimetype == "image/jpeg" ||
            file.mimetype == "image/png" ||
            file.mimetype == "image/jpg"
        )
            cb(null, true);
        else {
            cb(new Error("Only jpeg, jpg, png format images allowed!"), false);
        }
    }
});

const uploadImage = upload.single("profile_img");

router.post("/uploadImage", uploadImage, (req, res) => {
    uploadImage(req, res, function(err) {
        if (err instanceof multer.MulterError) {
            return res.status(200).json(err);
        } else {
            return res.status(201).json({
                success: 1,
                data: req.file,
                message: "Image uploaded successfully"
            });
        }
    });
});

let limit = 15;

router.get("/", middleware.auth, (req, res) => {
    let offset = 0;
    User.count({
        where: {
            role_id: 3,
            isactive: 1
        }
    }).then(count => {
        limit = req.query.limit ? parseInt(req.query.limit) : limit;
        let page = req.query.page ? req.query.page : 1; // page number
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        User.findAndCountAll({
                include: [{
                    association: "userdetail"
                }],
                where: {
                    role_id: 3,
                    isactive: 1
                },
                order: [
                    ["created_at", "DESC"]
                ],
                limit: limit,
                offset: offset
            })
            .then(attorneys => {
                res.status(200).json({
                    success: 1,
                    pages: pages,
                    data: attorneys,
                    message: "List of attroneys for a particular user"
                });
            })
            .catch(err => res.status(500).json(err));
    });
});




router.get("/user/:userId", middleware.auth, async(req, res, next) => {
    try {
        var userIDS = [req.params.userId];
        let userid = await User.findAll({
            where: {
                parent_user_id: req.params.userId,
            }
        });
        for (u of userid) {
            userIDS.push(u.id);
        }

        let offset = 0;
        let count = await User.count({
            where: {
                role_id: 3,
                parent_user_id: userIDS,
                isactive: [1, -1],
            }
        });
        limit = req.query.limit ? parseInt(req.query.limit) : limit;
        let page = req.query.page ? req.query.page : 1; // page number
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        let attorneys = await User.findAndCountAll({
            include: [{
                association: "userdetail"
            }],
            where: {
                role_id: 3,
                parent_user_id: userIDS,
                isactive: [1, -1],
            },
            order: [
                ["created_at", "DESC"]
            ],
            limit: limit,
            offset: offset
        });

        let currentUser = await User.findAndCountAll({
            include: [{
                association: "userdetail"
            }],
            where: {
                parent_user_id: null,
                isattorney: 1,
                id: req.params.userId,
                isactive: [1, -1],
            },
            order: [
                ["created_at", "DESC"]
            ],
        });


        // attorneys.rows.push(currentUser.rows[0]);

        res.status(200).json({
            success: 1,
            pages: pages,
            data: attorneys,
            currentUser: currentUser,
            userids: userIDS,
            message: "List of attroneys for a particular user"
        });



    } catch (err) {
        next(err);
    }
});


router.get("/primary/user/:userId", middleware.auth, async(req, res, next) => {
    try {
        var userIDS = [req.params.userId];
        let userid = await User.findAll({
            where: {
                parent_user_id: req.params.userId,
            }
        });
        if (userid.length) {
            for (u of userid) {
                userIDS.push(u.id);
            }
        } else {
            let up = await User.findOne({
                where: {
                    id: req.params.userId,
                }
            });
            var user_parent_id = up.parent_user_id
        }
        var whereCond = {};
        if (user_parent_id) {
            whereCond = {
                id: req.params.userId,
            }
        } else {
            whereCond = {
                role_id: 3,
                parent_user_id: userIDS,
                isactive: [1, -1],
            }
        }


        let offset = 0;
        let count = await User.count({
            where: whereCond,
        });
        limit = req.query.limit ? parseInt(req.query.limit) : limit;
        let page = req.query.page ? req.query.page : 1; // page number
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        let attorneys = await User.findAndCountAll({
            include: [{
                association: "userdetail"
            }],
            where: whereCond,
            order: [
                ["created_at", "DESC"]
            ],
            limit: limit,
            offset: offset
        });

        let currentUser = await User.findAndCountAll({
            include: [{
                association: "userdetail"
            }],
            where: {
                parent_user_id: null,
                isattorney: 1,
                id: req.params.userId,
                isactive: [1, -1],
            },
            order: [
                ["created_at", "DESC"]
            ],
        });


        // attorneys.rows.push(currentUser.rows[0]);

        res.status(200).json({
            success: 1,
            pages: pages,
            data: attorneys,
            currentUser: currentUser,
            userids: userIDS,
            message: "List of attroneys for a particular user"
        });



    } catch (err) {
        next(err);
    }
});


router.get("/checkVerification/id/:id", async(req, res, next) => {
    try {
        let user_id = req.params.id;
        let userStatus = await User.findOne({
            where: { id: user_id },
        });
        res.status(200).json({ userstatus: userStatus.isactive });

    } catch (err) {
        next(err);
    }
});

router.post("/user-credentials/id/:id", async(req, res, next) => {
    // res.status(200).json({ data: req.body, id: req.params.id });
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ success: 0, error: errors.array() });
        }
        let userexist = await User.findOne({
            where: { id: req.params.id }
        });

        if (userexist) {
            var created_at = userexist.created_at;
            var AddedDate = Moment(req.params.dt).add(10, 'hours');
            var convertedDate = Moment(AddedDate).format("DD-MM-YYYY");
            var created_date = Moment(userexist.created_at).format("DD-MM-YYYY");
            const HOUR = 1000 * 60 * 60;
            const TenHourAgo = Date.now() - (HOUR * 10);

            if (userexist.isactive == 1) {
                res.status(200).json({ success: 0, code: 200, message: "User Already Verified" });
            } else {

                // if (convertedDate == created_date) {
                if (1 == 1) {
                    if (req.body.password != null) {

                        bcrypt.hash(req.body.password, env.SALTROUNDS, (err, hash) => {
                            var userCredentials = {
                                username: req.body.username,
                                password: hash,
                                isactive: 1,

                            };

                            var userdetail = {
                                picturepath: req.body.picturepath ?
                                    req.body.picturepath : ""
                            }

                            let result = userexist.update(userCredentials, {
                                include: [{ model: UserDetail, as: "userdetail" }]
                            });

                            let result1 = UserDetail.update(userdetail, {
                                where: { user_id: req.params.id }
                            });

                            res.status(200).json({
                                success: 1,
                                message: "User Details updated",
                                result: result
                            });
                        });

                    }


                } else {
                    res.status(200).json({ code: 200, success: 0, message: "Link Expired" })
                }

            }
        } else {

            res.status(200).json({
                success: 0,
                message: "User not found in our records"
            });
        }

    } catch (error) {
        next(error);
    }
});

router.post("/", [middleware.validations, middleware.auth], async(req, res, next) => {

    try {

        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ success: 0, errors: errors.array() });
        }
        let userexist = await User.findOne({
            where: { username: req.body.username }
        });

        if (userexist == null) {
            let user = await User.findOne({
                where: { email: req.body.email }
            });

            if (user == null) {

                const NewUser = {
                    email: req.body.email,
                    role_id: 3,
                    parent_user_id: req.body.user_id,
                    isactive: -1,
                    userdetail: {
                        firstname: req.body.firstname,
                        lastname: req.body.lastname,
                        contact1: req.body.contact1_code + "-" + req.body.contact1,
                        contact2: req.body.contact2_code + "-" + req.body.contact2,
                        address: req.body.address,
                        yearofpractice: req.body.yearofpractice,
                        picturepath: req.body.picturepath ?
                            req.body.picturepath : ""
                    }
                };

                let result = await User.create(NewUser, {
                    include: [{ model: UserDetail, as: "userdetail" }]
                });

                var ConvertedDate = Moment(result.created_at).unix();

                let content = fs.readFileSync("./src/email-templates/crm/email_verification.html", "utf8");
                content = content.replace("**name**", req.body.firstname + " " + req.body.lastname);
                content = content.replace("**userid**", result.id);
                content = content.replace("**date**", ConvertedDate);
                let sendmail = await mail.send(
                    req.body.email,
                    "You have been invited",
                    content,
                    next
                );


                res.status(201).json({
                    success: 1,
                    message: "User Created",
                    data: result,
                    sendmail: sendmail
                });



            } else {
                res.status(200).json({
                    success: 0,
                    message: "User is already registered with given email address"
                });
            }

        } else {
            res.status(200).json({
                success: 0,
                message: "User is already registered with given username"
            });
        }

    } catch (error) {
        next(error);
    }



});

router.get("/archive/user/:userId", middleware.auth, async(req, res, next) => {

    try {
        var userIDS = [req.params.userId];
        let userid = await User.findAll({
            where: {
                parent_user_id: req.params.userId,
            }
        });
        for (u of userid) {
            userIDS.push(u.id);
        }
        let offset = 0;
        let count = await User.count({
            where: {
                role_id: 3,
                parent_user_id: userIDS,
                //deleted_at: { [Op.not]: "" }
                isactive: 0
            },
            paranoid: false,
        });
        limit = req.query.limit ? parseInt(req.query.limit) : limit;
        let page = req.query.page ? req.query.page : 1; // page number
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        let attorneys = await User.findAndCountAll({
            include: [{
                association: "userdetail"
            }],
            where: {
                role_id: 3,
                parent_user_id: userIDS,
                //deleted_at: { [Op.not]: "" }
                isactive: 0,

            },
            paranoid: false,
            limit: limit,
            offset: offset
        });

        res.status(200).json({
            success: 1,
            pages: pages,
            data: attorneys,
            userIDS: userIDS,
            message: "List of attroneys for a particular user"
        });
    } catch (err) {
        next(err);
    }

});

router.get("/:id", middleware.auth, (req, res) => {
    User.findOne({
            where: { id: req.params.id },
            include: [{
                association: "userdetail"
            }]
        })
        .then(user => {
            res.status(200).json({
                success: 1,
                data: user,
                message: "User is found"
            });
        })
        .catch(err => res.status(500).json(err));
});

router.patch(
    "/:id/status", [middleware.auth, middleware.validations],
    (req, res) => {
        User.findOne({
                where: { id: req.params.id },
                paranoid: false
            })
            .then(user => {
                if (user != null) {
                    user.update({ isactive: req.body.status });
                    if (req.body.status == 1) user.restore();
                    user.setDataValue("deleted_at", null);
                    res.status(200).json({
                        success: 1,
                        data: user,
                        message: "User Status is updated"
                    });
                } else {
                    res.status(200).json({
                        success: 0,
                        data: {},
                        message: "User is not found"
                    });
                }
            })
            .catch(err => res.status(500).json(err));
    }
);

router.put("/:id", middleware.auth, (req, res) => {
    User.findOne({
            where: { id: req.params.id },
            include: [{
                association: "userdetail"
            }]
        })
        .then(user => {
            if (user != null) {
                if (user.email == req.body.email) {
                    if (req.body.password != null) {
                        bcrypt.hash(req.body.password, env.SALTROUNDS, (err, hash) => {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                const UpdatedAttorney = {
                                    password: hash,
                                    userdetail: {
                                        firstname: req.body.firstname,
                                        lastname: req.body.lastname,
                                        contact1: req.body.contact1_code + "-" + req.body.contact1,
                                        contact2: req.body.contact2_code + "-" + req.body.contact2,
                                        address: req.body.address,
                                        yearofpractice: req.body.yearofpractice,
                                        picturepath: req.body.picturepath ?
                                            req.body.picturepath : user.picturepath
                                    }
                                };
                                user.update({ password: UpdatedAttorney.password });
                                user.userdetail.update(UpdatedAttorney.userdetail);
                                res.status(200).json({
                                    success: 1,
                                    data: user,
                                    message: "User Info is updated"
                                });
                            }
                        });
                    } else {
                        const UpdatedAttorney = {
                            userdetail: {
                                firstname: req.body.firstname,
                                lastname: req.body.lastname,
                                contact1: req.body.contact1_code + "-" + req.body.contact1,
                                contact2: req.body.contact2_code + "-" + req.body.contact2,
                                address: req.body.address,
                                yearofpractice: req.body.yearofpractice,
                                picturepath: req.body.picturepath ?
                                    req.body.picturepath : user.picturepath
                            }
                        };
                        user.userdetail.update(UpdatedAttorney.userdetail);
                        res.status(200).json({
                            success: 1,
                            data: user,
                            message: "User Info is updated"
                        });
                    }
                } else {
                    User.findOne({
                            where: { email: req.body.email },
                            include: [{
                                association: "userdetail"
                            }]
                        })
                        .then(user => {
                            if (user == null) {
                                if (req.body.password != null) {
                                    bcrypt.hash(
                                        req.body.password,
                                        env.SALTROUNDS,
                                        (err, hash) => {
                                            if (err) {
                                                res.status(500).json(err);
                                            } else {
                                                const UpdatedAttorney = {
                                                    password: hash,
                                                    email: req.body.email,
                                                    userdetail: {
                                                        firstname: req.body.firstname,
                                                        lastname: req.body.lastname,
                                                        contact1: req.body.contact1_code + "-" + req.body.contact1,
                                                        contact2: req.body.contact2_code + "-" + req.body.contact2,
                                                        address: req.body.address,
                                                        yearofpractice: req.body.yearofpractice,
                                                        picturepath: req.body.picturepath ?
                                                            req.body.picturepath : user.picturepath
                                                    }
                                                };
                                                user.update({
                                                    password: UpdatedAttorney.password,
                                                    email: UpdatedAttorney.email
                                                });
                                                user.userdetail.update(UpdatedAttorney.userdetail);
                                                res.status(200).json({
                                                    success: 1,
                                                    data: user,
                                                    message: "User Info is updated"
                                                });
                                            }
                                        }
                                    );
                                } else {
                                    const UpdatedAttorney = {
                                        email: req.body.email,
                                        userdetail: {
                                            firstname: req.body.firstname,
                                            lastname: req.body.lastname,
                                            contact1: req.body.contact1_code + "-" + req.body.contact1,
                                            contact2: req.body.contact2_code + "-" + req.body.contact2,
                                            address: req.body.address,
                                            yearofpractice: req.body.yearofpractice,
                                            picturepath: req.body.picturepath ?
                                                req.body.picturepath : user.picturepath
                                        }
                                    };
                                    user.update({
                                        email: UpdatedAttorney.email
                                    });
                                    user.userdetail.update(UpdatedAttorney.userdetail);
                                    res.status(200).json({
                                        success: 1,
                                        data: user,
                                        message: "User Info is updated"
                                    });
                                }
                            } else {
                                res.status(200).json({
                                    success: 0,
                                    data: {},
                                    message: "User is alread found with the given new email address"
                                });
                            }
                        })
                        .catch(err => res.status(500).json(err));
                }
            } else {
                res.status(200).json({
                    success: 0,
                    data: {},
                    message: "User is not found"
                });
            }
        })
        .catch(err => res.status(500).json(err));
});

router.delete("/:id", middleware.auth, (req, res) => {
    User.findOne({
            where: { id: req.params.id }
        })
        .then(user => {
            if (user != null) {
                user.update({ isactive: 0 });
                user.destroy();
                res.status(200).json({
                    success: 1,
                    data: user,
                    message: "User is deleted"
                });
            } else {
                res.status(200).json({
                    success: 0,
                    data: {},
                    message: "User is not found"
                });
            }
        })
        .catch(err => res.status(500).json(err));
});

router.get("/list/user/:userId", middleware.auth, (req, res, next) => {
    User.findAll({
            include: [{
                association: "userdetail"
            }],
            where: {
                role_id: 3,
                parent_user_id: req.params.userId,
                isactive: 1
            },
            order: [
                ["username", "ASC"]
            ]
        })
        .then(attorneys => {

            User.findAndCountAll({
                include: [{
                    association: "userdetail"
                }],
                where: {
                    id: req.params.userId,
                    isactive: [1],
                    parent_user_id: null,
                    isadmin: 1,
                },
                order: [
                    ["username", "ASC"]
                ],
            }).then(currentUser => {

                // console.log(currentUser);
                // attorneys.rows.push(currentUser.rows[0]);


            }).catch(err => res.status(500).json(err));

            res.status(200).json(attorneys);


        })
        .catch(err => next(err));
});

router.delete(
    "/bulk/delete", [middleware.auth, [check("ids").isArray()]],
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ error: errors.array() });
        }

        User.findOne({
                where: {
                    id: {
                        [Op.in]: req.body.ids
                    }
                }
            })
            .then(user => {
                if (user != null) {
                    user.update({ isactive: 0 });
                    user.destroy();
                    res.status(204).json({});
                } else {
                    res.status(404).json({
                        error: "Users are not found"
                    });
                }
            })
            .catch(err => next(err));
    }
);

router.patch(
    "/bulk/active", [middleware.auth, [check("ids").isArray()]],
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ error: errors.array() });
        }

        User.findOne({
                where: {
                    id: {
                        [Op.in]: req.body.ids
                    }
                }
            })
            .then(user => {
                if (user != null) {
                    user.update({ isactive: 1 });
                    res.status(204).json({});
                } else {
                    res.status(404).json({
                        error: "Users are not found"
                    });
                }
            })
            .catch(err => next(err));
    }
);

router.patch(
    "/bulk/inactive", [middleware.auth, [check("ids").isArray()]],
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ error: errors.array() });
        }

        User.findOne({
                where: {
                    id: {
                        [Op.in]: req.body.ids
                    }
                }
            })
            .then(user => {
                if (user != null) {
                    user.update({ isactive: 0 });
                    res.status(204).json({});
                } else {
                    res.status(404).json({
                        error: "Users are not found"
                    });
                }
            })
            .catch(err => next(err));
    }
);

router.get("/mail/send", async(req, res, next) => {
    try {
        let result = await mail.send(
            "satheeshraju@hotmail.com",
            "Test subject",
            "<h1>Hello Satheesh</h1>",
            next
        );
        res.json(result);
    } catch (error) {
        next(error);
    }
});

module.exports = router;