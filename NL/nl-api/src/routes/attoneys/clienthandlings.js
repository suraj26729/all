const express = require("express");
const router = express.Router();
const Matter = require("../../models/CaseDetails");
const Client = require("../../models/Client");
const checkToken = require("../../middlewares/checkToken");

const middleware = {
  auth: checkToken
};

let limit = 10;

router.get("/:attorneyID", middleware.auth, async (req, res) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  let clients1 = await Matter.findAll({
    where: { priattorney: req.params.attorneyID },
    attributes: ["client_id"],
    distinct: true
  });

  let clients2 = await Matter.findAll({
    distinct: true,
    attributes: ["client_id"],
    include: [
      {
        association: "assistattorney",
        where: { attorney_id: req.params.attorneyID }
      }
    ]
  });

  let client_ids = [];
  for (let row of clients1) {
    if (client_ids.indexOf(row.client_id) == -1) client_ids.push(row.client_id);
  }
  for (let row of clients2) {
    if (client_ids.indexOf(row.client_id) == -1) client_ids.push(row.client_id);
  }

  Client.paginate({
    where: { id: client_ids },
    attributes: ["id", "firstname", "surname", "email", "address1", "isactive"],
    page: page,
    paginate: limit
  })
    .then(clients => {
      res.status(200).json(clients);
    })
    .catch(err => res.status(500).json({ error: err }));
});

module.exports = router;
