const express = require("express");
const router = express.Router();
const Message = require("../../models/Message");
const User = require("../../models/User");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Pusher = require("pusher");
const Sequelize = require("sequelize");
const Common = require("../../helpers/common");
const middleware = {
  validations: [
    check("attorney_id").isInt(),
    check("content").isString(),
    // check("end_date").isString(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

let limit = 15;

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }
    if (req.body.attorney_id) {
      var data = [];

      for (let at_id of req.body.attorney_id) {
        let existThread = await Message.findOne({
          where: {
            thread_id: req.body.thread_id
            // $or: [
            //   {
            //     attorney_id: at_id,
            //     user_id: req.body.user_id
            //   },
            //   {
            //     attorney_id: req.body.user_id,
            //     user_id: at_id
            //   }
            // ]
          }
        });
        if (existThread != null) {
          data.push({
            thread_id: existThread.thread_id,
            attorney_id: at_id,
            content: req.body.content,
            is_first: 0,
            user_id: req.body.user_id
          });
        } else {
          data.push({
            attorney_id: at_id,
            content: req.body.content,
            is_first: req.body.is_first,
            user_id: req.body.user_id
          });
        }
      }
      let message = await Message.bulkCreate(data);
      res.status(201).json(message);
    }
  }
);

router.get("/attorney/:attorneyID", middleware.auth, (req, res) => {
  Message.findAll({
    where: { attorney_id: req.params.attorneyID },
    attributes: ["content", "end_date"]
  })
    .then(messages => {
      res.status(200).json(messages);
    })
    .catch(error => res.status(500).json({ error: error }));
});

router.get("/receive/:attorneyID", middleware.auth, async (req, res) => {
  let ids = [];
  ids = await Message.findAll({
    where: { attorney_id: req.params.attorneyID },
    group: ["thread_id"],
    order: [["id", "DESC"]],
    attributes: [[Sequelize.fn("max", Sequelize.col("id")), "id"]]
  });
  ids = ids.map(item => item.id);

  if (ids.length) {
    let page = req.query.page || 1;
    limit = parseInt(req.query.limit) || limit;
    let messages = await Message.findAll({
      where: { id: ids },
      include: [
        { association: "user", include: [{ association: "userdetail" }] }
      ],
      order: [["created_at", "DESC"]],
      group: ["thread_id", "user_id"]
    });
    res.status(200).json(messages);
  } else {
    res.status(200).json([]);
  }
});

router.get("/sent/:userID", middleware.auth, (req, res) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  Message.findAll({
    where: { user_id: req.params.userID },
    include: [
      { association: "attorney", include: [{ association: "userdetail" }] }
    ],
    order: [["created_at", "DESC"]]
  })
    .then(messages => {
      res.status(200).json(messages);
    })
    .catch(err => res.status(500).json({ error: err }));
});

router.get("/list/users/:userID", middleware.auth, async (req, res, next) => {
  try {
    let attorney_ids = await Common.Attorney(req.params.userID);
    let admin_ids = await Common.Admin(req.params.userID);
    let user = await User.findOne({ where: { id: req.params.userID } });
    if (user == null)
      return res.status(404).json({ error: "User is not found" });

    let userIds = await Common.firmUsers(req.params.userID);

    let admins = await User.findAll({
      where: {
        id: userIds,
        // parent_user_id: user.id,
        role_id: 2,
        isactive: 1
        // deleted_at: null
      },
      include: [{ association: "userdetail" }]
    });

    let attorneys = await User.findAll({
      where: {
        id: userIds,
        // parent_user_id: user.id,
        role_id: 3,
        isactive: 1
        // deleted_at: null
      },
      include: [{ association: "userdetail" }]
    });

    res.status(200).json({ admins, attorneys });
  } catch (error) {
    next(error);
  }
});

router.delete("/thread/:id", middleware.auth, async (req, res, next) => {
  try {
    let messages = await Message.destroy({
      where: { thread_id: req.params.id }
    });
    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
});

router.delete(
  "/thread/message/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let messages = await Message.destroy({
        where: { id: req.params.id }
      });
      return res.status(204).json({});
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
