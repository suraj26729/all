const supplierRoutes = require("../routes/suppliers/suppliers");
const supplierContactRoutes = require("../routes/suppliers/suppliercontacts");
const supplierCategoryRoutes = require("../routes/suppliers/suppliercat");
const suppnotesRoutes = require("./suppliers/suppliernotes");
const suppdocumentsRoutes = require("./suppliers/supplierdoc");

exports.routes = (app) => {
  app.use("/api/suppliers", supplierRoutes);
  app.use("/api/suppliers/contacts", supplierContactRoutes);
  app.use("/api/suppliers/category", supplierCategoryRoutes);
  app.use("/api/suppliers/notes", suppnotesRoutes);
  app.use("/api/suppliers/documents", suppdocumentsRoutes);
};
