const express = require("express");
const router = express.Router();
const Role = require("../models/Role");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");

const middleware = {
  validations: [check("name").isLength({ min: 1 })],
  auth: checkToken
};

router.get("/", middleware.auth, (req, res) => {
  Role.findAndCountAll()
    .then(roles => {
      res.status(200).json({
        success: 1,
        data: roles,
        message: "List of roles"
      });
    })
    .catch(err => res.status(500).json(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewRole = { name: req.body.name };

  Role.findOne({
    where: { name: req.body.name }
  }).then(role => {
    if (role == null) {
      Role.create(NewRole)
        .then(role => {
          res.status(201).json({
            success: 1,
            data: role,
            message: "Role Created Successfully"
          });
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(200).json({
        success: 0,
        data: {},
        message: "Role is already created with same name"
      });
    }
  });
});

router.get("/:id", middleware.auth, (req, res) => {
  if (req.params.id) {
    Role.findOne({
      where: { id: req.params.id }
    })
      .then(role => {
        if (role != null) {
          res.status(200).json({
            success: 1,
            data: role,
            message: "Role is found"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Role is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Role Id is not found in the request"
    });
  }
});

router.patch("/:id", [middleware.auth, middleware.validations], (req, res) => {
  if (req.params.id) {
    Role.findOne({
      where: { id: req.params.id }
    })
      .then(role => {
        if (role != null) {
          role.update({ name: req.body.name });
          res.status(200).json({
            success: 1,
            data: role,
            message: "Role is updated"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Role is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Role Id is not found in the request"
    });
  }
});

router.delete("/:id", middleware.auth, (req, res) => {
  if (req.params.id) {
    Role.findOne({
      where: { id: req.params.id }
    })
      .then(role => {
        if (role != null) {
          role.destroy();
          res.status(200).json({
            success: 1,
            data: role,
            message: "Role is deleted"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Role is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Role Id is not found in the request"
    });
  }
});

module.exports = router;
