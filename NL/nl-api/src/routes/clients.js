const clientRoutes = require("../routes/clients/clients");
const clientContactRoutes = require("../routes/clients/contacts");
const casedetailsRoutes = require("../routes/clients/casedetails");
const hearingsRoutes = require("./clients/hearings");
const notesRoutes = require("./clients/notes");
const documentsRoutes = require("./clients/documents");

exports.routes = (app) => {
  app.use("/api/clients", clientRoutes);
  app.use("/api/clients/contacts", clientContactRoutes);
  app.use("/api/clients/casedetails", casedetailsRoutes);
  app.use("/api/clients/hearings", hearingsRoutes);
  app.use("/api/clients/notes", notesRoutes);
  app.use("/api/clients/documents", documentsRoutes);
};
