const express = require("express");
const router = express.Router();
const Link = require("../models/Links");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const multer = require("multer");
const common = require("../helpers/common");

const middleware = {
  validations: [
    check("attorneys").isLength({ min: 1 }),
    check("type").isInt(),
    check("link").isString(),
    check("title").isString(),
    check("user_id").isInt(),
  ],
  auth: checkToken,
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/links/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 4 }, // 4 MB
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "application/pdf" ||
      file.mimetype == "application/vnd.ms-excel" ||
      file.mimetype ==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
      file.mimetype == "application/msword" ||
      file.mimetype ==
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ) {
      cb(null, true);
    } else {
      cb(new Error("Only jpeg, jpg, png, Excel format files allowed!"), false);
    }
  },
});

const uploadDoc = upload.single("doc");

router.post("/uploadDoc", uploadDoc, middleware.auth, (req, res) => {
  uploadDoc(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      nexts(err);
    } else {
      return res.status(201).json({
        data: req.file,
        message: "Document uploaded successfully",
      });
    }
  });
});

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      let attorneys = req.body.attorneys;
      let links = [];
      for (let attorney_id of attorneys) {
        links.push(
          await Link.create({
            attorney_id: attorney_id,
            type: req.body.type,
            link: req.body.link,
            title: req.body.title,
            notes: req.body.notes,
            user_id: req.body.user_id,
          })
        );
      }
      res.status(201).json(links);
    } catch (error) {
      next(error);
    }
  }
);

router.get("/user/:userId", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.userId);
  let offset = 0;
  let limit = 10;
  Link.count({
    where: {
      user_id: userIds,
    },
  }).then((count) => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    Link.findAndCountAll({
      include: [
        {
          association: "attorney",
          include: [{ association: "userdetail" }],
        },
        { association: "user", include: [{ association: "userdetail" }] },
      ],
      where: {
        user_id: userIds,
      },
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
    })
      .then((links) => {
        links["count"] = count;
        res.status(200).json({
          pages: pages,
          data: links,
        });
      })
      .catch((err) => next(err));
  });
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Link.findOne({ where: { id: req.params.id } })
    .then((link) => {
      res.status(200).json(link);
    })
    .catch((err) => next(err));
});

router.put(
  "/id/:id",
  [
    middleware.auth,
    [
      check("type").isInt(),
      check("link").isString(),
      check("title").isString(),
    ],
  ],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    Link.findOne({ where: { id: req.params.id } })
      .then((lead) => {
        lead.update({
          type: req.body.type,
          link: req.body.link,
          title: req.body.title,
          notes: req.body.notes,
        });
        res.status(200).json(lead);
      })
      .catch((err) => next(err));
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  Link.findOne({ where: { id: req.params.id } })
    .then((lead) => {
      if (lead != null) {
        lead.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Link is not found",
        });
      }
    })
    .catch((err) => next(err));
});

module.exports = router;
