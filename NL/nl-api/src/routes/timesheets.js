const express = require("express");
const router = express.Router();
const Timesheet = require("../models/Timesheet");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const moment = require("moment");
const sq = require("sequelize");
const Common = require("../helpers/common");
const middleware = {
    validations: [
        check("client_id").isUUID(),
        check("case_id").isUUID(),
        check("date").isString(),
        check("hours").isString(),
        check("user_id").isInt()
    ],
    auth: checkToken
};

router.post(
    "/", [middleware.auth, middleware.validations],
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        Timesheet.create(req.body)
            .then(timesheet => {
                res.status(201).json(timesheet);
            })
            .catch(next);
    }
);

router.get("/list/user/:userId", middleware.auth, async(req, res, next) => {
    let userIds = await Common.firmUsers(req.params.userId);
    Timesheet.findAll({
            where: { user_id: userIds },
            include: [{
                    association: "user",
                    attributes: ["id"],
                    include: [
                        { association: "userdetail", attributes: ["firstname", "lastname"] }
                    ]
                },
                {
                    association: "client",
                    attributes: ["id", "companyname", "firstname", "surname"]
                },
                { association: "matter", attributes: ["id", "prefix", "casenumber"] }
            ],
            order: [
                ["created_at", "DESC"]
            ]
        })
        .then(timesheets => {
            res.status(200).json(timesheets);
        })
        .catch(err => next(err));
});

router.post(
    "/paginate/user/:userId",
    middleware.auth,
    async(req, res, next) => {
        try {
            let userIDS = await Common.firmUsers(req.params.userId);
            let offset = 0;
            let limit = 999;
            let ids = [req.params.userId];
            let userids = req.body;
            if (userids.length) {
                for (i of userids) {
                    ids.push(i);
                }
            }
            let count = await Timesheet.count({
                where: {
                    user_id: userIDS
                }
            });
            limit = req.query.limit ? parseInt(req.query.limit) : limit;
            let page = req.query.page ? req.query.page : 1; // page number
            let pages = Math.ceil(count / limit);
            offset = limit * (page - 1);

            let timesheets = await Timesheet.findAndCountAll({
                where: { user_id: userIDS },
                include: [{
                        association: "user",
                        attributes: ["id"],
                        include: [{
                            association: "userdetail",
                            attributes: ["firstname", "lastname"]
                        }]
                    },
                    {
                        association: "client",
                        attributes: ["id", "companyname", "firstname", "surname", "type"]
                    },
                    { association: "matter", attributes: ["id", "prefix", "casenumber"] }
                ],
                order: [
                    ["created_at", "DESC"]
                ],
                limit: limit,
                offset: offset
            });
            timesheets["count"] = count;
            res.status(200).json({
                pages: pages,
                data: timesheets,
                id: userIDS
            });
        } catch (error) {
            next(error);
        }
    }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
    Timesheet.findByPk(req.params.id)
        .then(timesheet => {
            if (timesheet == null)
                return res.status(404).json("Timesheet is not found");
            timesheet.destroy();
            res.status(204).json({});
        })
        .catch(next);
});

router.get("/user/:userId/hours-week", middleware.auth, (req, res, next) => {
    Timesheet.findAll({
            attributes: ["hours"],
            where: {
                user_id: req.params.userId,
                created_at: {
                    $between: [
                        moment()
                        .startOf("isoWeek")
                        .format("YYYY-MM-DD"),
                        moment()
                        .endOf("isoWeek")
                        .format("YYYY-MM-DD")
                    ]
                }
            }
        })
        .then(times => {
            let seconds = 0;
            for (let time of times) {
                let stime = time.hours.split(":");
                seconds = parseFloat(seconds) + parseFloat(stime[0] * 3600);
                seconds = parseFloat(seconds) + parseFloat(stime[1] * 60);
                seconds = parseFloat(seconds) + parseFloat(stime[2]);
            }
            let hours = Math.floor(seconds / 3600);
            hours = hours < 10 ? "0" + hours : hours;
            seconds = seconds - hours * 3600;
            let minutes = Math.floor(seconds / 60);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds - minutes * 60;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            res.status(200).json({ hours, minutes, seconds });
        })
        .catch(next);
});

router.get("/user/:userId/hours-month", middleware.auth, (req, res, next) => {
    Timesheet.findAll({
            attributes: ["hours"],
            where: {
                user_id: req.params.userId,
                created_at: {
                    $between: [
                        moment()
                        .startOf("month")
                        .format("YYYY-MM-DD"),
                        moment()
                        .endOf("month")
                        .format("YYYY-MM-DD")
                    ]
                }
            }
        })
        .then(times => {
            let seconds = 0;
            for (let time of times) {
                let stime = time.hours.split(":");
                seconds = parseFloat(seconds) + parseFloat(stime[0] * 3600);
                seconds = parseFloat(seconds) + parseFloat(stime[1] * 60);
                seconds = parseFloat(seconds) + parseFloat(stime[2]);
            }
            let hours = Math.floor(seconds / 3600);
            hours = hours < 10 ? "0" + hours : hours;
            seconds = seconds - hours * 3600;
            let minutes = Math.floor(seconds / 60);
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds - minutes * 60;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            res.status(200).json({ hours, minutes, seconds });
        })
        .catch(next);
});

module.exports = router;