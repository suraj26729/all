const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator/check");
const checkToken = require("../../middlewares/checkToken");
const Blog = require('../../models/Blog');
const multer = require("multer");
const Catagory = require('../../models/Catagory');
const middleware = {
    validations: [check("title").isString(), check("content").isString()],
    auth: checkToken
};

const Sequelize = require("sequelize");

router.post("/", [middleware.auth, middleware.validations], (req, res, next) => {
    let obj = {
        name: req.body.name,

    }
    Catagory.create(obj).then(data => {
        res.status(200).json({ code: 200, message: "Catagory Created Successfully" });
    }).catch(err => {
        res.status(400).json({ code: 400, error: err });
    });
});

router.get("/", (req, res, next) => {
    Catagory.findAll({
        include: [{
            model: Blog,
            as: 'blog_catagory',
            attributes: ['id']

        }],
    }).then(result => {
        res.status(200).json(result);
    }).catch(err => {
        console.log(err);
    });
});




module.exports = router;