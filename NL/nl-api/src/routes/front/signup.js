const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const UserDetail = require("../../models/UserDetail");
const bcrypt = require("bcryptjs");
const env = require("../../../env");
const Paypal = require("paypal-rest-sdk");
const Moment = require("moment");
const Order = require("../../models/Order");
const Payment = require("../../models/Payment");
const DateDiff = require("date-diff");
const Billing = require("../../models/Billing");
const fs = require("fs");
const mail = require("../../config/mail");
const common = require("../../helpers/common");
const checkToken = require("../../middlewares/checkToken");
const uuid = require("uuid/v1");

Paypal.configure({
  mode: env.paypal.mode, //sandbox or live
  client_id: env.paypal.clientID,
  client_secret: env.paypal.clientSecret
});

router.post("/", async (req, res, next) => {
  try {
    var via = req.body.via;
    let find = await User.findOne({
      where: {
        username: {
          $like: "%" + req.body.username + "%"
        }
      }
    });
    if (find == null) {
      let cemail = await User.findOne({
        where: { email: req.body.email }
      });

      if (cemail == null) {
        let hash = bcrypt.hashSync(req.body.password, env.SALTROUNDS);
        var now = Moment(new Date())
          .add(14, "days")
          // .endOf("day")
          .format("YYYY-MM-DD H:mm:ss");
        const NewUser = {
          username: req.body.username,
          password: hash,
          email: req.body.email,
          role_id: req.body.role_id,
          isactive: 0,
          expiry_date: now,
          userdetail: {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            contact1: req.body.contact1,
            contact2: req.body.contact2,
            address: req.body.address,
            yearofpractice: req.body.yearofpractice,
            logo: req.body.picturepath,
            country_code: req.body.country,
            currency: req.body.currency,
            isactive: 0
          }
        };

        if (via == "trial") {
          // NewUser.expiry_date = Moment(new Date())
          //   .add(14, "days")
          //   .endOf("day")
          //   .format("YYYY-MM-DD H:mm:ss");
          // NewUser.isactive = 1;
          let result = await User.create(NewUser, {
            include: [{ model: UserDetail, as: "userdetail" }]
          });

          // var payment_details = {
          //   user_id: result.id,
          //   amount: 30,
          //   expiry_date: now,
          //   no_due_paid: 1
          // };
          // let pm = Payment.create(payment_details, {});

          // let content = fs.readFileSync(
          //   "./src/email-templates/fourteen-day-trail-begun-email-notification.html",
          //   "utf8"
          // );
          // content = content.replace("**username**", req.body.username);
          // let sentMail = await mail.send(
          //   req.body.email,
          //   "Your 14 days free trial has begun",
          //   content,
          //   next
          // );

          let content = fs.readFileSync(
            "./src/email-templates/email-verification.html",
            "utf8"
          );
          content = content.replace(
            "**username**",
            result.userdetail.firstname + " " + result.userdetail.lastname
          );
          let remember_token = uuid();
          result.update({ remember_token: remember_token });
          let activate_link = `https://www.naartjielegal.com/account/activate/${remember_token}`;
          content = content.replace("**activate_link**", activate_link);
          let sentMail = await mail.send(
            req.body.email,
            "Activate your account - Naartjie Legal",
            content,
            next
          );

          return res.status(201).json({
            success: 1,
            message: "User Created",
            data: result,
            userData: sentMail
          });
        } else {
          let user = await User.create(NewUser, {
            include: [{ model: UserDetail, as: "userdetail" }]
          });
          const data = {
            intent: "sale",
            payer: {
              payment_method: "paypal",
              payer_info: {
                email: user.email,
                first_name: user.userdetail.firstname,
                last_name: user.userdetail.lastname
              }
            },
            redirect_urls: {
              return_url: env.paypal.return_url,
              cancel_url: env.paypal.cancel_url
            },
            transactions: [
              {
                item_list: {
                  items: [
                    {
                      name:
                        via == "account-beta"
                          ? "Accounting Package (Beta)"
                          : "Per Individual Subscriber Payment Initial",
                      price: 30,
                      currency: "USD",
                      quantity: 1
                    }
                  ]
                },
                amount: {
                  currency: "USD",
                  total: 30
                },
                description: ""
              }
            ]
          };
          Paypal.payment.create(data, async (error, payment) => {
            if (error) {
              console.log(JSON.stringify(error));
              throw error;
            } else {
              let paymentDB = await Payment.create({
                user_id: user.id,
                amount: 30,
                payment_id: payment.id,
                details: null
              });
              let links = payment.links;
              let redirect_link = "";
              for (let link of links) {
                if (link.method == "REDIRECT") {
                  return res.status(201).json(link.href);
                }
              }
            }
          });
        }
      } else {
        res.status(409).json({
          success: 0,
          message: "User is already registered with given email address"
        });
      }
    } else {
      res.status(409).json({
        success: 0,
        message: "User is already registered with given username"
      });
    }
  } catch (err) {
    next(err);
  }
});

router.post("/:id/execute", async (req, res, next) => {
  try {
    let payer_id = req.params.id;
    let paymentId = req.body.paymentId;
    let payment = await Payment.findOne({ where: { payment_id: paymentId } });
    let user_id = payment.user_id;
    let total = await getBillingAmount(user_id);
    total = total.total;

    var execute_payment_json = {
      payer_id: payer_id,
      transactions: [
        {
          amount: {
            currency: "USD",
            total: total
          }
        }
      ]
    };
    Paypal.payment.execute(
      paymentId,
      execute_payment_json,
      async (error, payment) => {
        if (error) {
          console.log(JSON.stringify(error.response));
          throw error;
        } else {
          let renew = 0;
          let paymentDB = await Payment.findOne({
            where: { payment_id: payment.id }
          });
          let user = null;
          if (paymentDB != null) {
            user = await User.findOne({
              where: { id: paymentDB.user_id },
              include: [{ association: "userdetail" }]
            });
            if (user.isactive == 0) user.update({ isactive: 1 });
            else {
              renew = 1;
              var start = Moment(user.expiry_date)
                .add(30, "days")
                .endOf("day")
                .format("YYYY-MM-DD HH:mm:ss");
              user.update({
                expiry_date: start
              });
            }

            paymentDB.update({
              payer_id: payer_id,
              payer_email: payment.payer.payer_info.email,
              status: "completed"
            });

            if (renew) {
              //Send renewed email notification
              let content = fs.readFileSync(
                "./src/email-templates/renew-success.html",
                "utf8"
              );
              content = content.replace(
                "**name**",
                user.userdetail.firstname + " " + user.userdetail.lastname
              );
              content = content.replace(
                "**message**",
                "Thank you for your renewal of account! Your payment has been received by us. You can start using our system now."
              );
              content = content.replace("**amount**", "$" + paymentDB.amount);
              content = content.replace("**next_billing_date**", start);

              let sentMail = await mail.send(
                user.email,
                "Account Renewed",
                content,
                next
              );
            } else {
              let content = fs.readFileSync(
                "./src/email-templates/payment-email.html",
                "utf8"
              );
              content = content.replace(
                "**name**",
                user.userdetail.firstname + " " + user.userdetail.lastname
              );
              content = content.replace(
                "**payment_date**",
                Moment(paymentDB.created_at).format("DD-MM-YYYY")
              );
              content = content.replace("**payment_gateway**", "PayPal");
              content = content.replace("**amount**", "$" + paymentDB.amount);
              content = content.replace("**password**", "************");
              let sentMail = await mail.send(
                user.email,
                "Payment Completed",
                content,
                next
              );
            }
          }
          return res.json({ payment, user });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});

router.post("/getuser", (req, res, next) => {
  var email = req.body.email;
  User.findOne({
    where: { email: email }
  })
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => {
      res.status(400).json(err);
    });
});

router.post("/getuser/:id", (req, res, next) => {
  User.findOne({
    where: { id: req.params.id }
  })
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => {
      res.status(400).json(err);
    });
});

router.post("/buy", (req, res, next) => {
  //Atrributs for creating the billing plan of  a user.
  let billingPlanAttributes = {
    description: " Add about subscription details.",
    merchant_preferences: {
      auto_bill_amount: "yes",
      cancel_url: "http://localhost:4201",
      initial_fail_amount_action: "continue",
      max_fail_attempts: "1",
      return_url: "http://localhost:4201/paypal",
      setup_fee: {
        currency: "USD",
        value: "0"
      }
    },
    name: "Paypal Agreement",
    payment_definitions: [
      {
        amount: {
          currency: "USD",
          value: "0"
        },
        cycles: "1",
        frequency: "day",
        frequency_interval: "14",
        name: "Trial 1",
        type: "TRIAL"
      },
      {
        amount: {
          currency: "USD",
          value: "85"
        },
        cycles: "0",
        frequency: "MONTH",
        frequency_interval: "1",
        name: "Regular 1",
        type: "REGULAR"
      }
    ],
    type: "INFINITE"
  };

  //Once a billing plan is created it must be updated with the following attributes.
  let billingPlanUpdateAttributes = [
    {
      op: "replace",
      path: "/",
      value: {
        state: "ACTIVE"
      }
    }
  ];

  //Attributes for creating the billing agreement.
  //Start Date should be greater than current time and date.
  let startDate =
    Moment(new Date())
      .add(1, "days")
      .format("gggg-MM-DDTHH:mm:ss") + "Z";
  let billingAgreementAttributes = {
    name: "Name of Payment Agreement",
    description: "Description of  your payment  agreement",
    start_date: startDate,
    plan: {
      id: ""
    },
    payer: {
      payment_method: "paypal"
    }
  };

  //Creating the billing plan and agreement of payment.
  //Step 6:
  Paypal.billingPlan.create(billingPlanAttributes, (error, billingPlan) => {
    if (error) {
      console.log(error);
    } else {
      //Step 7:
      Paypal.billingPlan.update(
        billingPlan.id,
        billingPlanUpdateAttributes,
        (error, response) => {
          if (error) {
            console.log(error);
          } else {
            // update the billing agreement attributes before creating it.
            billingAgreementAttributes.plan.id = billingPlan.id;

            //Step 8:
            Paypal.billingAgreement.create(
              billingAgreementAttributes,
              (error, billingAgreement) => {
                if (error) {
                  console.log(error);
                } else {
                  var url = require("url");
                  for (
                    var index = 0;
                    index < billingAgreement.links.length;
                    index++
                  ) {
                    if (billingAgreement.links[index].rel === "approval_url") {
                      var approval_url = billingAgreement.links[index].href;

                      res.status(200).json(billingAgreement);

                      //console.log(url.parse(approval_url, true).query.token);

                      // See billing_agreements/execute.js to see example for executing agreement
                      // after you have payment token
                    }
                  }

                  // res.status(200).json(billingAgreement);
                }
              }
            );
          }
        }
      );
    }
  });
});

router.post("/execute", (req, res, next) => {
  paymentToken = req.body.token;
  email = req.body.username;

  Paypal.billingAgreement.execute(paymentToken, {}, function(
    error,
    billingAgreement
  ) {
    if (error) {
      console.log(error);
      throw error;
    } else {
      User.findOne({
        where: { email: email }
      })
        .then(data => {
          var start = Moment(data.expiry_date).add(30, "days");
          data.update({
            expiry_date: start
          });

          Payment.findOne({
            where: { user_id: data.id }
          })
            .then(pay => {
              var billing_order = {
                agreement_id: billingAgreement.id,
                outstanding_balance:
                  billingAgreement.agreement_details.outstanding_balance.value,
                payer_email: billingAgreement.payer.payer_info.email,
                payer_firstName: billingAgreement.payer.payer_info.first_name,
                payer_lastName: billingAgreement.payer.payer_info.last_name,
                payer_id: billingAgreement.payer.payer_info.payer_id,
                paypal_state: billingAgreement.state,
                billing_useremail: email,
                expiry_date: start,
                due_paid: pay.no_due_paid
              };
              var start = Moment(data.expiry_date).add(30, "days");
              pay.update({
                no_due_paid: parseInt(pay.no_due_paid) + parseInt(1),
                expiry_date: start
              });

              Billing.create(billing_order, {})
                .then(billing => {
                  User.findOne({
                    where: { email: req.body.username }
                  })
                    .then(userd => {
                      let attorney_id = userd.id;
                      User.findAll({
                        where: { parent_user_id: attorney_id }
                      })
                        .then(data => {
                          pay.update({
                            no_due_paid:
                              parseInt(pay.no_due_paid) + parseInt(1),
                            expiry_date: start
                          });

                          // var values = { created_at: billing.created_at };
                          // var selector = {
                          //     where: { parent_user_id: attorney_id }
                          // };

                          // User.update(values, selector).then(data => {
                          //     res.status(200).json(data);
                          // }).catch(err => {
                          //     res.status(400).json(err);
                          // });

                          res
                            .status(200)
                            .json({ message: "payment successfull" });
                        })
                        .catch(error => {
                          res.status(400).json({
                            error: error,
                            message: "cannot update sub attorney created date"
                          });
                        });
                    })
                    .catch(usererror => {
                      res.status(400).json({
                        error: usererror,
                        message: "cannot find user"
                      });
                    });
                  res.status(200).json({ message: "payment successfull" });
                })
                .catch(billing_error => {
                  res.status(400).json({
                    error: billing_error,
                    message: "cannot create billing order"
                  });
                });
            })
            .catch(error => {
              res.status(400).json({
                code: 400,
                message: "cannot update payment",
                error: error
              });
            });
        })
        .catch(err => {
          res
            .status(400)
            .json({ code: 400, message: "error occured", error: err });
        });
    }
  });
});

router.post("/calculate", async (req, res, next) => {
  try {
    var today = Moment(new Date()).format("DD-MM-YYYY");

    let user_id = req.body.id;
    let userIds = await common.firmUsers(req.body.id);
    let rootUser = await User.findOne({
      where: { id: userIds, role_id: 2, parent_user_id: null }
    });
    if (rootUser != null) user_id = rootUser.id;

    let user = await User.findOne({
      where: { id: user_id }
    });
    // res.status(200).json(data);
    let attorney_id = user.id;
    let created_date = user.created_at;
    // var now = new Date();
    // now.setDate(now.getDate() + 30);

    let exp_date = Moment(user.expiry_date).format("DD-MM-YYYY");
    //today == exp_date
    if (1 == 1) {
      let pay = await Payment.findOne({
        where: { user_id: user.id }
      });

      let due = pay.no_due_paid;
      let data = await User.findAll({
        where: { parent_user_id: attorney_id }
      });
      //res.status(200).json(data);
      var sub_atotal = [];
      var total_amount = 0;
      var attorneys = [];
      var count = 1;
      var total = [];

      for (lawyer of data) {
        count++;
        console.log(lawyer.username);

        if (due == 1) {
          let owner_created_date = Moment(created_date).format("YYYY, MM, DD");
          let lawyer_created_date = Moment(lawyer.created_at).format(
            "YYYY, MM ,DD"
          );

          var date1 = new Date(lawyer_created_date); // 2015-12-1
          var date2 = new Date(owner_created_date); // 2014-01-1

          var diff = new DateDiff(date2, date1);

          sub_atotal.push({
            name: lawyer.username,
            days: Math.abs(diff.days())
          });
          total.push(Math.abs(diff.days()));

          for (x in total) {
            total_amount += total[x];
          }

          pay.update({
            attorney: JSON.stringify(sub_atotal),
            attorney_fee: total_amount
          });
          obj = {
            success: 1,
            attorney: sub_atotal,
            subtotal: total_amount,
            total: total_amount + pay.amount
          };
        } else {
          let billing = await Billing.findOne({
            where: { billing_useremail: req.body.email }
          });

          let owner_created_date = Moment(billing.created_at).format(
            "YYYY, MM, DD"
          );
          let lawyer_created_date = Moment(lawyer.created_at).format(
            "YYYY, MM ,DD"
          );

          var date1 = new Date(lawyer_created_date); // 2015-12-1
          var date2 = new Date(owner_created_date); // 2014-01-1

          var diff = new DateDiff(date2, date1);

          sub_atotal.push({
            name: lawyer.username,
            days: Math.abs(diff.days())
          });
          total.push(Math.abs(diff.days()));

          for (x in total) {
            total_amount += total[x];
          }
          obj = {
            success: 1,
            attorney: sub_atotal,
            subtotal: total_amount,
            total: total_amount + pay.amount
          };
        }
      }

      return res.status(200).json(obj);

      // //Atrributs for creating the billing plan of  a user.

      // let billingPlanAttributes = {
      //     "description": " Add about subscription details.",
      //     "merchant_preferences": {
      //         "auto_bill_amount": "yes",
      //         "cancel_url": env.paypal.cancel_url,
      //         "initial_fail_amount_action": "continue",
      //         "max_fail_attempts": "1",
      //         "return_url": env.paypal.return_url + '/' + req.body.email,
      //         "setup_fee": {
      //             "currency": env.paypal.currency,
      //             "value": env.paypal.setup_fee
      //         }
      //     },
      //     "name": "Paypal Agreement",
      //     "payment_definitions": [{
      //             "amount": {
      //                 "currency": env.paypal.currency,
      //                 "value": total_amount + pay.amount
      //             },
      //             "cycles": "0",
      //             "frequency": "MONTH",
      //             "frequency_interval": "1",
      //             "name": "Regular 1",
      //             "type": "REGULAR"
      //         }

      //     ],
      //     "type": "INFINITE"
      // };

      // //Once a billing plan is created it must be updated with the following attributes.
      // let billingPlanUpdateAttributes = [{
      //     "op": "replace",
      //     "path": "/",
      //     "value": {
      //         "state": "ACTIVE"
      //     }
      // }];

      // //Attributes for creating the billing agreement.
      // //Start Date should be greater than current time and date.
      // let startDate = Moment(new Date()).add(1, 'days').format('gggg-MM-DDTHH:mm:ss') + 'Z';
      // let billingAgreementAttributes = {
      //     "name": "Name of Payment Agreement",
      //     "description": "Description of  your payment  agreement",
      //     "start_date": startDate,
      //     "plan": {
      //         "id": ""
      //     },
      //     "payer": {
      //         "payment_method": "paypal"
      //     }
      // };

      // //Creating the billing plan and agreement of payment.
      // //Step 6:
      // Paypal.billingPlan.create(billingPlanAttributes, (error, billingPlan) => {
      //     if (error) {
      //         console.log(error);
      //     } else {
      //         //Step 7:
      //         Paypal.billingPlan.update(billingPlan.id, billingPlanUpdateAttributes, (error, response) => {
      //             if (error) {
      //                 console.log(error);
      //             } else {
      //                 // update the billing agreement attributes before creating it.
      //                 billingAgreementAttributes.plan.id = billingPlan.id;

      //                 //Step 8:
      //                 Paypal.billingAgreement.create(billingAgreementAttributes, (error, billingAgreement) => {
      //                     if (error) {
      //                         console.log(error);
      //                     } else {
      //                         var url = require('url');
      //                         for (var index = 0; index < billingAgreement.links.length; index++) {
      //                             if (billingAgreement.links[index].rel === 'approval_url') {
      //                                 var approval_url = billingAgreement.links[index].href;
      //                                 res.status(201).json({
      //                                     success: 1,
      //                                     message: "Agreement Created",
      //                                     data: obj,
      //                                     approval_url: approval_url
      //                                 });

      //                             }
      //                         }
      //                     }
      //                 });
      //             }
      //         });
      //     }
      // });
    } else {
      res.status(200).json({
        success: 0,
        message: "Not Expiring today"
      });
    }
  } catch (error) {
    next(error);
  }
});

const getBillingAmount = async user_id => {
  let userIds = await common.firmUsersWithDeleted(user_id);
  let rootUser = await User.findOne({
    where: { id: userIds, role_id: 2, parent_user_id: null },
    include: [{ association: "userdetail" }]
  });
  let attorneys = await User.findAll({
    where: { id: userIds, role_id: 3 },
    include: [{ association: "userdetail" }],
    paranoid: false
  });
  let admins = await User.findAll({
    where: { id: userIds, role_id: 2, parent_user_id: { $not: null } },
    include: [{ association: "userdetail" }],
    paranoid: false
  });
  let billing = await Payment.findOne({
    where: { user_id: rootUser.id, status: "completed" },
    order: [["created_at", "DESC"]]
  });
  let owner_expiry_date = Moment(rootUser.expiry_date).format("YYYY, MM, DD");
  let sub_atotal = [];
  let a_sub_atotal = [];
  let total_amount = 0;
  let total = [];

  for (let lawyer of attorneys) {
    var date1 = "";
    var date2 = "";
    var diff = "";
    var del_usage = "";
    let days = 0;
    date1 = new Date(owner_expiry_date);
    if (lawyer.deleted_at != null || lawyer.archive_date != null) {
      let date =
        lawyer.archive_date != null
          ? lawyer.archive_date
          : lawyer.deleted_at != null
          ? lawyer.deleted_at
          : null;
      let lawyer_deleted_date = Moment(date).format("YYYY, MM ,DD");
      date2 = new Date(lawyer_deleted_date);
      diff = new DateDiff(date1, date2);
      del_usage = 30 - Math.abs(diff.days());
      // console.log(del_usage);
      if (del_usage > 0 && del_usage <= 30) {
        sub_atotal.push({ attorney: lawyer, days: del_usage });
        days = del_usage;
      }
    } else {
      let lawyer_created_date = Moment(lawyer.created_at).format(
        "YYYY, MM ,DD"
      );
      date2 = new Date(lawyer_created_date);
      diff = new DateDiff(date1, date2);
      sub_atotal.push({ attorney: lawyer, days: Math.abs(diff.days()) });
      days = Math.abs(diff.days());
      if (days >= 30) days = 30;
    }

    if (billing != null) total.push(days);
    else total.push(0);
  }

  for (let lawyer of admins) {
    var date1 = "";
    var date2 = "";
    var diff = "";
    var del_usage = "";
    let days = 0;
    date1 = new Date(owner_expiry_date);
    if (lawyer.deleted_at != null || lawyer.archive_date != null) {
      let date =
        lawyer.archive_date != null
          ? lawyer.archive_date
          : lawyer.deleted_at != null
          ? lawyer.deleted_at
          : null;
      let lawyer_deleted_date = Moment(date).format("YYYY, MM ,DD");
      date2 = new Date(lawyer_deleted_date);
      diff = new DateDiff(date1, date2);
      del_usage = 30 - Math.abs(diff.days());
      // console.log(del_usage);
      if (del_usage > 0 && del_usage <= 30) {
        a_sub_atotal.push({ admin: lawyer, days: del_usage });
        days = del_usage;
      }
    } else {
      let lawyer_created_date = Moment(lawyer.created_at).format(
        "YYYY, MM ,DD"
      );
      date2 = new Date(lawyer_created_date);
      diff = new DateDiff(date1, date2);
      a_sub_atotal.push({ admin: lawyer, days: Math.abs(diff.days()) });
      days = Math.abs(diff.days());
      if (days >= 30) days = 30;
    }

    if (billing != null) total.push(days);
    else total.push(0);
  }

  if (billing != null) {
    total_amount = total.reduce((a, b) => a + b, 0);
    obj = {
      success: 1,
      user: rootUser,
      attorney: sub_atotal,
      admin: a_sub_atotal,
      subtotal: total_amount,
      total: total_amount + 30,
      first: 0
    };
  } else {
    obj = {
      success: 1,
      user: rootUser,
      attorney: sub_atotal,
      admin: a_sub_atotal,
      subtotal: 30,
      total: 30,
      first: 1
    };
  }
  return obj;
};

router.get("/id/:id/billing-amount", async (req, res, next) => {
  let user_id = req.params.id;
  let obj = await getBillingAmount(user_id);
  return res.status(200).json(obj);
});

router.post("/paypal/user/:id/pay", async (req, res, next) => {
  try {
    let user = await User.findOne({
      where: { id: req.params.id },
      include: [{ association: "userdetail" }]
    });
    let obj = await getBillingAmount(user.id);
    const data = {
      intent: "sale",
      payer: {
        payment_method: "paypal",
        payer_info: {
          email: user.email,
          first_name: user.userdetail.firstname,
          last_name: user.userdetail.lastname
        }
      },
      redirect_urls: {
        return_url: env.paypal.return_url + "/renew/success",
        cancel_url: env.paypal.cancel_url
      },
      transactions: [
        {
          item_list: {
            items: [
              {
                name: "Per Individual Subscriber Payment / Renewal",
                price: obj.total,
                currency: "USD",
                quantity: 1
              }
            ]
          },
          amount: {
            currency: "USD",
            total: obj.total
          },
          description: ""
        }
      ]
    };
    Paypal.payment.create(data, async (error, payment) => {
      if (error) {
        console.log(JSON.stringify(error));
        throw error;
      } else {
        let paymentDB = await Payment.create({
          user_id: user.id,
          amount: obj.total,
          payment_id: payment.id,
          details: JSON.stringify({ attorney: obj.attorney, admin: obj.admin })
        });
        let links = payment.links;
        let redirect_link = "";
        for (let link of links) {
          if (link.method == "REDIRECT") {
            return res.status(201).json(link.href);
          }
        }
      }
    });
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.post("/paypal/:id/execute", async (req, res, next) => {
  try {
    let payer_id = req.params.id;
    let paymentId = req.body.paymentId;
    let user_id = req.body.user_id;
    let total = await getBillingAmount(user_id);
    total = total.total;

    var execute_payment_json = {
      payer_id: payer_id,
      transactions: [
        {
          amount: {
            currency: "USD",
            total: total
          }
        }
      ]
    };
    Paypal.payment.execute(
      paymentId,
      execute_payment_json,
      async (error, payment) => {
        if (error) {
          console.log(error.response);
          throw error;
        } else {
          let user = await User.findOne({
            where: { id: user_id },
            include: [{ association: "userdetail" }]
          });
          var start = Moment(user.expiry_date)
            .add(30, "days")
            .endOf("day")
            .format("YYYY-MM-DD HH:mm:ss");
          user.update({
            expiry_date: start
          });
          let paymentDB = await Payment.findOne({
            where: { payment_id: payment.id }
          });
          if (paymentDB != null) {
            paymentDB.update({
              payer_id: payer_id,
              payer_email: payment.payer.payer_info.email,
              status: "completed"
            });
          }
          //Send renewed email notification
          let content = fs.readFileSync(
            "./src/email-templates/renew-success.html",
            "utf8"
          );
          content = content.replace(
            "**name**",
            user.userdetail.firstname + " " + user.userdetail.lastname
          );
          content = content.replace(
            "**message**",
            "Thank you for your renewal of account! Your payment has been received by us. You can start using our system now."
          );
          content = content.replace("**amount**", "$" + paymentDB.amount);
          content = content.replace("**next_billing_date**", start);

          let sentMail = await mail.send(
            user.email,
            "Account Renewed",
            content,
            next
          );
          return res.json({ payment, user, sentMail });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});

router.post("/paypal/payment/:id/pay", checkToken, async (req, res, next) => {
  try {
    let payment = await Payment.findByPk(req.params.id);
    let user = await User.findOne({
      where: { id: payment.user_id },
      include: [{ association: "userdetail" }]
    });
    const data = {
      intent: "sale",
      payer: {
        payment_method: "paypal",
        payer_info: {
          email: user.email,
          first_name: user.userdetail.firstname,
          last_name: user.userdetail.lastname
        }
      },
      redirect_urls: {
        return_url: env.paypal.return_url + "/renew/success",
        cancel_url: env.paypal.cancel_url
      },
      transactions: [
        {
          item_list: {
            items: [
              {
                name: "Per Individual Subscriber Payment / Renewal",
                price: payment.amount,
                currency: "USD",
                quantity: 1
              }
            ]
          },
          amount: {
            currency: "USD",
            total: payment.amount
          },
          description: ""
        }
      ]
    };
    Paypal.payment.create(data, async (error, payment) => {
      if (error) {
        console.log(JSON.stringify(error));
        throw error;
      } else {
        let links = payment.links;
        let redirect_link = "";
        for (let link of links) {
          if (link.method == "REDIRECT") {
            return res.status(201).json(link.href);
          }
        }
      }
    });
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.post("/test-send-mail", async (req, res, next) => {
  try {
    let content = fs.readFileSync(
      "./src/email-templates/email-verification.html",
      "utf8"
    );
    content = content.replace("**username**", "Satheesh");
    let activate_link = `https://naartjielegal.com/account/activate/${uuid()}`;
    content = content.replace("**activate_link**", activate_link);
    let sentMail = await mail.send(
      req.body.email,
      "Activate your account - Naartjie Legal",
      content,
      next
    );
    return res.json(sentMail);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.patch("/activate/:token", async (req, res, next) => {
  try {
    let user = await User.findOne({
      where: { remember_token: req.params.token },
      include: [{ association: "userdetail" }]
    });
    if (user == null) return res.status(404).json("Token invalid");

    user.update({ remember_token: null, isactive: 1 });

    let content = fs.readFileSync(
      "./src/email-templates/fourteen-day-trail-begun-email-notification.html",
      "utf8"
    );
    content = content.replace(
      "**username**",
      user.userdetail.firstname + " " + user.userdetail.lastname
    );
    let sentMail = await mail.send(
      user.email,
      "Your 14 days free trial has begun",
      content,
      next
    );

    return res.status(200).json(user);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
