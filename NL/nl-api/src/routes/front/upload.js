const express = require("express");
const router = express.Router();
const multer = require("multer");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/admins/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 4 }, // 4 MB
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    ) {
      cb(null, true);
    } else {
      cb(new Error("Only jpeg, jpg, png, Excel format files allowed!"), false);
    }
  },
});

const uploadImage = upload.single("profile_img");

router.post("/", uploadImage, (req, res) => {
  uploadImage(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(200).json(err);
    } else {
      return res.status(201).json({
        success: 1,
        data: req.file,
        message: "Image uploaded successfully",
      });
    }
  });
});

module.exports = router;
