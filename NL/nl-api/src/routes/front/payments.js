const router = require("express").Router();
const Payment = require("../../models/Payment");
const checkToken = require("../../middlewares/checkToken");
const moment = require("moment");

router.get("/user/:userID", checkToken, async (req, res, next) => {
  try {
    let from = req.query.from || null;
    let to = req.query.to || null;
    let cond = [];
    cond.push({ user_id: req.params.userID });
    cond.push({ status: "completed" });

    if (from != "" && from != null) {
      from = moment(from, "YYYY-MM-DD")
        .startOf("day")
        .format("YYYY-MM-DD H:mm:ss");
    }
    if (to != "" && to != null) {
      to = moment(to, "YYYY-MM-DD")
        .endOf("day")
        .format("YYYY-MM-DD H:mm:ss");
    }

    if (from != null && to != null)
      cond.push({ created_at: { $between: [from, to] } });
    if (from != null && to == null) cond.push({ created_at: { $gte: from } });
    if (from == null && to != null) cond.push({ created_at: { $lte: to } });

    var conditions = cond.reduce((a, b) => Object.assign(a, b), {});

    let payments = await Payment.findAll({
      where: conditions,
      order: [["created_at", "DESC"]]
    });
    return res.status(200).json(payments);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/id/:id", checkToken, async (req, res, next) => {
  try {
    let payment = await Payment.findByPk(req.params.id);
    return res.status(200).json(payment);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.delete("/id/:id", checkToken, async (req, res, next) => {
  try {
    let payment = await Payment.findByPk(req.params.id);
    payment.delete();
    return res.status(204).json({});
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
