const express = require("express");
const router = express.Router();
const mail = require("../../config/mail");
const { check, validationResult } = require("express-validator/check");
const env = require("../../../env");
const request = require("superagent");
const bcrypt = require("bcryptjs");
const User = require("../../models/User");
const fs = require("fs");
const UserDetails = require("../../models/UserDetail");
const sequelize = require("sequelize");

router.post(
  "/",
  [
    check("name").isString(),
    check("email")
      .isString()
      .isEmail(),
    check("message").isString()
  ],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      let message = `<p>Hi Admin ,</p><br/>
    Name: ${req.body.name}<br/>
    Email: ${req.body.email}<br/>
    Message: ${req.body.message}<br/>`;

      var mail_address = [env.adminMail];
      if (req.body.copy_yourself == true) {
        mail_address.push(req.body.email);
      }

      let result = await mail.enquiry(
        mail_address,
        "New contact",
        message,
        next
      );

      let content = fs.readFileSync("./src/email-templates/index.html", "utf8");
      content = content.replace("**username**", req.body.name);
      let sentMail = await mail.send(
        req.body.email,
        "Thanks for your enquiry",
        content,
        next
      );

      res.json({ result: result, code: 200, userMail: sentMail });
    } catch (error) {
      next(error);
    }
  }
);

router.post("/submit", async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    let message = `<p>Hi Admin ,</p><br/>
        Name: ${req.body.name}<br/>
        Email: ${req.body.email_id}<br/>
        Message: ${req.body.message}<br/>
        Lawfirm:${req.body.law_firm}<br/>
        MobileNumber:${req.body.mobile_code} ${req.body.mobile}<br/>`;

    var mail_address = [env.adminMail];
    if (req.body.copy_yourself == true) {
      mail_address.push(req.body.email_id);
    }

    let result = await mail.enquiry(mail_address, "New contact", message, next);
    let content = fs.readFileSync("./src/email-templates/index.html", "utf8");
    content = content.replace("**username**", req.body.name);
    let sentMail = await mail.send(
      req.body.email_id,
      "Thanks for your enquiry",
      content,
      next
    );

    res.status(200).json({
      success: 1,
      data: result,
      userdata: sentMail,
      message: "Mail Sent"
    });
  } catch (error) {
    next(error);
  }
});

router.post("/emailus", async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    let message = `<p>Hi Admin</p>,<br/>
Name: ${req.body.name}<br/>
Email: ${req.body.email}<br/>
Message: ${req.body.message}<br/>
Lawfirm:${req.body.lawfirm}<br/>
MobileNumber:${req.body.mobile}<br/>`;

    let result = await mail.send(env.adminMail, "New contact", message, next);
    res.status(200).json({ success: 1, data: result, message: "Mail Sent" });
  } catch (error) {
    next(error);
  }
});

router.post("/subscribers", (req, res, next) => {
  request
    .post(
      "https://" +
        env.mailChimp.mailchimpInstance +
        ".api.mailchimp.com/3.0/lists/" +
        env.mailChimp.listUniqueId +
        "/members/"
    )
    .set("Content-Type", "application/json;charset=utf-8")
    .set(
      "Authorization",
      "Basic " +
        new Buffer("any:" + env.mailChimp.mailchimpApiKey).toString("base64")
    )
    .send({
      email_address: req.body.email,
      status: "subscribed"
      // 'merge_fields': {
      //     'FNAME': req.body.firstName,
      //     'LNAME': req.body.lastName
      // }
    })
    .end(function(err, response) {
      if (
        response.status < 300 ||
        (response.status === 400 && response.body.title === "Member Exists")
      ) {
        if (response.body.title) {
          res
            .status(200)
            .json({ code: 200, status: 2, message: "Member Already Exist" });
        } else {
          let content = fs.readFileSync(
            "./src/email-templates/subscriber-email.html",
            "utf8"
          );
          let result = mail.send(
            req.body.email,
            "subscribed successfully",
            content,
            next
          );
          res.status(200).json({
            code: 200,
            status: 1,
            data: result,
            message: "Successfully Subscribed!"
          });
        }
      } else {
        res
          .status(400)
          .json({ code: 400, status: 3, message: "Subscription Failed" });
      }
    });
});

router.post("/checkUser", (req, res, next) => {
  User.findOne({
    where: { email: req.body.email },
    include: [
      {
        association: "userdetail"
      }
    ]
  })
    .then(data => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/forgot-password.html",
        "utf8"
      );
      content = content.replace(
        "**username**",
        data.userdetail.firstname + " " + data.userdetail.lastname
      );

      let reset_password_link = `${env.Forgoturl}/${data.userdetail.id}`;
      content = content.replace("**reset_password_link**", reset_password_link);

      let result = mail.send(data.email, "Reset Password", content);
      res.status(200).json({ success: 1, data: result, message: "Mail Sent" });

      // res.status(200).json({ success: 1, code: 200, data: data, message: "user found" });
    })
    .catch(err => {
      res.status(400).json({
        code: 400,
        error: err,
        message: "user not found in our records"
      });
    });
  // res.status(200).json(req.body);
});

router.post("/checkUserName", (req, res, next) => {
  User.findOne({
    where: { email: req.body.email },
    include: [
      {
        association: "userdetail"
      }
    ]
  })
    .then(data => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/forgot-username.html",
        "utf8"
      );
      content = content.replace(
        "**username**",
        data.userdetail.firstname + " " + data.userdetail.lastname
      );

      let reset_username_link = `${env.Forgotname}/${data.userdetail.id}`;
      content = content.replace("**reset_username_link**", reset_username_link);

      let result = mail.send(data.email, "Username reset link", content);
      res.status(200).json({ success: 1, data: result, message: "Mail Sent" });

      // res.status(200).json({ success: 1, code: 200, data: data, message: "user found" });
    })
    .catch(err => {
      res.status(400).json({
        code: 400,
        error: err,
        message: "user not found in our records"
      });
    });
  // res.status(200).json(req.body);
});

router.post("/UpdatePassword", async (req, res, next) => {
  //res.status(200).json(req.body);
  try {
    let userdetail = await UserDetails.findOne({
      where: { id: req.body.id }
    });

    // return res.json(userdetail);

    let data = await User.findOne({
      where: { username: req.body.name, id: userdetail.user_id }
    });
    if (data == null) {
      res
        .status(400)
        .json({ code: 400, message: "Please Check the user name" });
    } else {
      bcrypt.hash(req.body.password, env.SALTROUNDS, (err, hash) => {
        if (err) {
          res.status(500).json(err);
        } else {
          data
            .update({
              password: hash
            })
            .then(
              success => {
                res.status(200).json({
                  success: 1,
                  code: 200,
                  data: data,
                  message: "Passsword Updated Successfully"
                });
              },
              err => {
                res.status(400).json({
                  success: 1,
                  code: 400,
                  data: data,
                  message: "Error Updating Password"
                });
              }
            );
        }
      });
    }
  } catch (err) {
    next(err);
  }
});

router.post("/UpdateUsername", async (req, res, next) => {
  let userdetail = await UserDetails.findOne({
    where: { id: req.body.id }
  });

  if (userdetail) {
    let user = await User.findOne({
      where: { id: userdetail.user_id }
    });

    let exists = await User.findOne({
      where: {
        username: req.body.username,
        id: { [sequelize.Op.not]: user.id }
      }
    });

    if (exists != null)
      return res
        .status(400)
        .json({ success: 0, message: "Username already used by someone" });

    let result = user.update({
      username: req.body.username
    });
    if (result) {
      res
        .status(200)
        .json({ success: 1, message: "Username Updated Successfully" });
    }
  } else {
    res.status(400).json({ success: 0, message: "user not found" });
  }
});

module.exports = router;
