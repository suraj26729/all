const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator/check");
const checkToken = require("../../middlewares/checkToken");
const Blog = require("../../models/Blog");
const multer = require("multer");
const Catagory = require("../../models/Catagory");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const env = require("../../../env");
const middleware = {
  validations: [check("title").isString(), check("content").isString()],
  auth: checkToken,
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/blogs/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 4 }, // 4 MB
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    ) {
      cb(null, true);
    } else {
      cb(new Error("Only jpeg, jpg, png, Excel format files allowed!"), false);
    }
  },
});

const uploadImage = upload.single("featured_img");

router.post("/uploadImage", uploadImage, middleware.auth, (req, res) => {
  uploadImage(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(400).json(err);
    } else {
      return res.status(201).json({
        success: 1,
        data: req.file,
        url: "api/blogs/" + req.file.filename,
        message: "Image uploaded successfull",
      });
    }
  });
});

router.post(
  "/",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    Blog.create(req.body)
      .then((data) => {
        res.status(200).json(data);
      })
      .catch((err) => {
        res.status(400).json({ code: 400, error: err });
      });
  }
);

router.get("/paginate", middleware.validations, (req, res, next) => {
  let search = req.query.search ? req.query.search : "";

  let cond = {};

  if (search !== "") {
    cond = {
      title: {
        [Op.like]: "%" + search + "%",
      },
    };
  }

  //console.log(cond);

  Blog.count({
    order: [["created_at", "DESC"]],
    where: cond,
  })
    .then((count) => {
      let limit = req.query.limit ? parseInt(req.query.limit) : 10;
      let offset = 0;
      let page = req.query.page ? req.query.page : 1;
      let pages = Math.ceil(count / limit);
      offset = limit * (page - 1);
      Blog.findAndCountAll({
        where: cond,
        order: [["created_at", "DESC"]],
        include: [
          {
            model: Catagory,
            as: "blog_catagory",
          },
        ],
        limit: limit,
        offset: offset,
      })
        .then((data) => {
          data["count"] = count;
          res.status(200).json({
            success: 1,
            page: pages,
            data: data,
            pageno: parseInt(page),
            message: "All Blog Content",
          });
        })
        .catch((error) => next(error));
    })
    .catch((error) => next(error));
});

router.get(
  "catagory/:slug/paginate",
  middleware.validations,
  (req, res, next) => {
    Catagory.findOne({
      where: { slug: req.params.slug },
    })
      .then((data) => {
        //res.status(200).json(data);
        Blog.count({
          where: { catagory_id: data.id },
        })
          .then((count) => {
            let limit = req.query.limit ? parseInt(req.query.limit) : 15;
            let offset = 0;
            let page = req.query.page ? req.query.page : 1;
            let pages = Math.ceil(count / limit);
            offset = limit * (page - 1);
            Blog.findAndCountAll({
              where: { catagory_id: data.id },
              order: [["created_at", "DESC"]],
              limit: limit,
              offset: offset,
            })
              .then((data) => {
                data["count"] = count;
                res.status(200).json({
                  success: 1,
                  page: pages,
                  data: data,
                  pageno: parseInt(page),
                  message: "All Blog Content",
                });
              })
              .catch((error) => next(error));
          })
          .catch((error) => next(error));
      })
      .catch((err) => {
        res.status(400).json({
          err: err,
          message: "Cannot find Catagory with slug:" + req.params.slug,
        });
      });
  }
);

router.get("/all", middleware.validations, (req, res, next) => {
  Blog.findAll({
    include: [
      {
        model: Catagory,
        as: "blog_catagory",
      },
    ],
    order: [["created_at", "DESC"]],
  })
    .then((data) => {
      res.status(200).json({
        data: data,
        message: "All Blog Content",
      });
    })
    .catch((error) => next(error));
}),
  router.get("/slug/:slug", middleware.validations, (req, res, next) => {
    Blog.findOne({
      where: { slug: req.params.slug },
    })
      .then((data) => {
        res.status(200).json(data);
      })
      .catch((err) => {
        next(err);
      });
  });

router.get("/catagory/:slug", (req, res, next) => {
  Catagory.findOne({
    where: { slug: req.params.slug },
  })
    .then((data) => {
      //res.status(200).json(data);
      Blog.count({
        where: { catagory_id: data.id },
      })
        .then((count) => {
          let limit = req.query.limit ? parseInt(req.query.limit) : 15;
          let offset = 0;
          let page = req.query.page ? req.query.page : 1;
          let pages = Math.ceil(count / limit);
          offset = limit * (page - 1);
          Blog.findAndCountAll({
            where: { catagory_id: data.id },
            order: [["created_at", "DESC"]],
            limit: limit,
            offset: offset,
          })
            .then((data) => {
              data["count"] = count;
              res.status(200).json({
                success: 1,
                page: pages,
                data: data,
                pageno: parseInt(page),
                message: "All Blog Content",
              });
            })
            .catch((error) => next(error));
        })
        .catch((error) => next(error));
    })
    .catch((err) => {
      res.status(400).json({
        err: err,
        message: "Cannot find Catagory with slug:" + req.params.slug,
      });
    });
});

router.put("/id/:id", (req, res, next) => {
  // res.status(200).json(req.body);
  let id = req.params.id;
  Blog.findOne({
    where: { id: id },
  })
    .then((data) => {
      data.update(req.body);
      res.status(200).json({ code: 201, message: "Blog Updated Successfully" });
    })
    .catch((err) => {
      next(err);
    });
});

router.get("/id/:id", (req, res, next) => {
  // res.status(200).json(req.body);
  let id = req.params.id;
  Blog.findOne({
    where: { id: id },
  })
    .then((data) => {
      res.status(200).json({ code: 200, message: "Blog Details", data: data });
    })
    .catch((err) => {
      next(err);
    });
});

router.post("/id/:id/status", (req, res, next) => {
  // res.status(200).json(req.body);
  let id = req.params.id;
  Blog.findOne({
    where: { id: id },
  })
    .then((data) => {
      data.update(req.body);
      res.status(200).json({ code: 200, message: "Blog Details", data: data });
    })
    .catch((err) => {
      next(err);
    });
});

router.delete("/id/:id", middleware.validations, (req, res, next) => {
  let id = req.params.id;
  Blog.findOne({
    where: { id: id },
  })
    .then((data) => {
      data.destroy();
      res.status(200).json({ code: 201, message: "Blog Deleted Successfully" });
    })
    .catch((err) => {
      next(err);
    });
});

module.exports = router;
