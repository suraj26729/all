const express = require("express");
const router = express.Router();
const Permission = require("../models/Permission");
const UserPermission = require("../models/UserPermission");
const CalendarAttorney = require("../models/PermissionCalendarAttorneys");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const TimerAttorney = require("../models/PermissionTimerAttorneys");
const User = require("../models/User");
const mail = require("../config/mail");
const common = require("../helpers/common");
const fs = require("fs");
const Email = require("../models/Email");

const middleware = {
  validations: [check("name").isString(), check("url").isString()],
  auth: checkToken
};

const middleware2 = {
  validations: [
    check("permission_id").isInt(),
    check("role").isString(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    const NewPermission = { name: req.body.name };

    Permission.findOne({
      where: { name: req.body.name }
    }).then(permission => {
      if (permission == null) {
        Permission.create(req.body)
          .then(permission => {
            res.status(201).json(permission);
          })
          .catch(err => next(err));
      } else {
        res.status(400).json("Permission is already created with same name");
      }
    });
  }
);

router.get("/attorney/list", middleware.auth, (req, res, next) => {
  Permission.findAll({
    where: { isactive: 1, id: { $notIn: [25, 26, 27] } },
    order: [["id", "ASC"]]
  })
    .then(permissions => {
      res.status(200).json(permissions);
    })
    .catch(next);
});

router.get("/admin/list", middleware.auth, (req, res, next) => {
  Permission.findAll({
    where: { isactive: 1 },
    order: [["id", "ASC"]]
  })
    .then(permissions => {
      res.status(200).json(permissions);
    })
    .catch(next);
});

router.get("/user/:id/list", middleware.auth, async (req, res, next) => {
  try {
    let user_id = req.params.id;
    let user = await User.findByPk(user_id);
    let permissions = [];
    if (user.role_id == 3) {
      permissions = await Permission.findAll({
        where: { isactive: 1, id: { $notIn: [25, 26, 27] } },
        order: [["id", "ASC"]]
      });
    } else {
      permissions = await Permission.findAll({
        where: { isactive: 1 },
        order: [["id", "ASC"]]
      });
    }
    return res.status(200).json(permissions);
  } catch (err) {
    next(err);
  }
});

router.get(
  "/user/:userId/permission/:pId/roles",
  middleware.auth,
  async (req, res, next) => {
    try {
      let user_id = req.params.userId;
      let permission_id = req.params.pId;

      let userPermissions = await UserPermission.findOne({
        where: { user_id: user_id, permission_id: permission_id }
      });

      return res.status(200).json(userPermissions);
    } catch (err) {
      next(err);
    }
  }
);

router.post(
  "/request",
  [middleware2.auth, middleware2.validations],
  async (req, res, next) => {
    try {
      let { permission_id, role, user_id } = req.body;
      let permission = await Permission.findByPk(permission_id);
      let user = await User.findByPk(user_id, {
        include: [{ association: "userdetail" }]
      });

      let admins = await common.firmAdmins(user_id);
      let to = [];
      for (let admin_id of admins) {
        let admin = await User.findByPk(admin_id);
        to.push(admin.email);
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/access_request.html",
        "utf8"
      );
      content = content.replace("**permission**", permission.name);
      content = content.replace("**role**", role);
      content = content.replace(
        "**user**",
        user.userdetail.firstname + " " + user.userdetail.lastname
      );

      let email = await Email.create({
        subject: "New Access Request",
        type: "new_access_request",
        user_id: user.id
      });

      let result = await mail.send(to, "New Access Request", content, next);

      res.status(201).json(result);
    } catch (err) {
      next(err);
    }
  }
);

router.post(
  "/user/:id",
  [middleware.auth, [check("permissions").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }
    UserPermission.findAll({ where: { user_id: req.params.id } })
      .then(permissions => {
        UserPermission.destroy({ where: { user_id: req.params.id } })
          .then(result => {
            UserPermission.bulkCreate(req.body.permissions, {
              individualHooks: true
            })
              .then(result => {
                let calAtt = [];
                let timAtt = [];
                result.map((r, index) => {
                  if (req.body.permissions[index].calendar_attorneys.length) {
                    req.body.permissions[index].calendar_attorneys.map(ca => {
                      calAtt.push({
                        user_permission_id: r.id,
                        attorney_id: ca
                      });
                    });
                  }

                  if (req.body.permissions[index].timer_attorneys.length) {
                    req.body.permissions[index].timer_attorneys.map(ta => {
                      timAtt.push({
                        user_permission_id: r.id,
                        attorney_id: ta
                      });
                    });
                  }
                });
                // if (calAtt.length) {
                //     CalendarAttorney.bulkCreate(calAtt)
                //         .then(result2 => res.status(201).json({ result, result2 }))
                //         .catch(err => console.log(err));
                // } else {
                //     res.status(201).json({ result });
                // }
                if (calAtt.length && !timAtt.length) {
                  CalendarAttorney.bulkCreate(calAtt)
                    .then(result2 => res.status(201).json({ result, result2 }))
                    .catch(err => console.log(err));
                } else if (timAtt.length && !calAtt.length) {
                  TimerAttorney.bulkCreate(timAtt)
                    .then(result3 => res.status(201).json({ result, result3 }))
                    .catch(err => console.log(err));
                } else if (timAtt.length && calAtt.length) {
                  TimerAttorney.bulkCreate(timAtt)
                    .then(result3 => {
                      CalendarAttorney.bulkCreate(calAtt)
                        .then(result2 =>
                          res.status(201).json({ result, result2, result3 })
                        )
                        .catch(err => console.log(err));
                    })
                    .catch(err => console.log(err));
                } else {
                  res.status(201).json({ result });
                }
              })
              .catch(next);
          })
          .catch(next);
      })
      .catch(next);
  }
);

router.get("/user/:id", middleware.auth, (req, res, next) => {
  UserPermission.findAll({
    where: { user_id: req.params.id },
    include: [
      { association: "calendarAttorneys" },
      { association: "timerAttorneys" }
    ],
    order: [["permission_id", "ASC"]]
  })
    .then(permissions => {
      let t_cal_at = [];
      let t_tim_at = [];
      let data = permissions.map(p => {
        t_cal_at = [];
        t_tim_at = [];
        if (p.calendarAttorneys.length) {
          t_cal_at = p.calendarAttorneys.map(ca => ca.attorney_id);
        }
        if (p.timerAttorneys.length) {
          t_tim_at = p.timerAttorneys.map(ta => ta.attorney_id);
        }
        return {
          add: p.add,
          assignee: p.assignee,
          calendar_attorneys: t_cal_at,
          timer_attorneys: t_tim_at,
          created_at: p.created_at,
          delete: p.delete,
          edit: p.edit,
          id: p.id,
          isactive: p.isactive,
          permission_id: p.permission_id,
          task_assign_admin: p.task_assign_admin,
          task_assign_attorney: p.task_assign_attorney,
          updated_at: p.updated_at,
          user_id: p.user_id,
          view: p.view
        };
      });
      res.status(200).json(data);
    })
    .catch(next);
});

router.get(
  "/list/attorneys/user/:id",
  middleware.auth,
  async (req, res, next) => {
    let userIds = await common.firmUsers(req.params.id);
    let attoneyIds = [];
    for (let id of userIds) {
      let user = await User.findByPk(id);
      if (
        user.id != req.params.userId &&
        (user.isattorney == 1 || user.role_id == 3 || user.role_id == 2)
      )
        attoneyIds.push(id);
    }
    let attorneys = await User.findAll({
      where: {
        id: attoneyIds,
        isactive: [1, -1]
      },
      include: [
        {
          association: "userdetail"
        }
      ],
      order: [["created_at", "DESC"]]
    });
    return res.status(200).json(attorneys);
  }
);

router.get("/list/admins/user/:id", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.id);
  let adminIds = [];
  for (let id of userIds) {
    let user = await User.findByPk(id);
    if (
      user.id != req.params.userId &&
      (user.isadmin == 1 || (user.role_id == 2 && user.parent_user_id != null))
    )
      adminIds.push(id);
  }
  let admins = await User.findAll({
    where: {
      id: adminIds,
      isactive: 1
    },
    include: [
      {
        association: "userdetail"
      }
    ],
    order: [["created_at", "DESC"]]
  });
  return res.status(200).json(admins);
});

module.exports = router;
