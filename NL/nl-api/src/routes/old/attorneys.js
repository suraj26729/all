const attoneyRoutes = require("../routes/attoneys/attorneys");
const clientHandlingRoutes = require("./attoneys/clienthandlings");
const matterRoutes = require("./attoneys/matters");
const messagingRoutes = require("./attoneys/messages");
const logRoutes = require("./attoneys/loginlogs");

exports.routes = app => {
  app.use("/api/attorneys", attoneyRoutes);
  app.use("/api/attorneys/client-handlings", clientHandlingRoutes);
  app.use("/api/attorneys/matters", matterRoutes);
  app.use("/api/attorneys/messages", messagingRoutes);
  app.use("/api/attorneys/logs", logRoutes);
};
