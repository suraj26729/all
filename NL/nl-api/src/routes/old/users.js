const express = require("express");
const router = express.Router();
const multer = require("multer");
const User = require("../models/User");
const UserDetail = require("../models/UserDetail");
const bcrypt = require("bcryptjs");
const env = require("../../env");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Message = require("../models/Message");

const middleware = {
  validations: [
    check("firstname").isString(),
    check("username").isString(),
    check("password").isString(),
    check("email").isEmail(),
    check("contact1").isMobilePhone("any"),
    check("address").isString(),
    check("yearofpractice").isInt(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/admins/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  }
});
const upload = multer({
  storage: storage,
  // limits: {
  //   fileSize: 1024 * 1024 * 5
  // },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    )
      cb(null, true);
    else {
      cb(new Error("Only jpeg, jpg, png format images allowed!"), false);
    }
  }
});

const uploadImage = upload.single("profile_img");

router.post("/uploadImage", uploadImage, middleware.auth, (req, res) => {
  uploadImage(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      return res.status(200).json(err);
    } else {
      return res.status(201).json({
        success: 1,
        data: req.file,
        message: "Image uploaded successfully"
      });
    }
  });
});

let limit = 15;

router.post("/", (req, res) => {
  User.findOne({
    where: { username: req.body.username }
  })
    .then(user => {
      if (user == null) {
        User.findOne({
          where: { email: req.body.email }
        })
          .then(user => {
            if (user == null) {
              bcrypt.hash(req.body.password, env.SALTROUNDS, (err, hash) => {
                if (err) {
                  res.status(500).json(err);
                } else {
                  const NewUser = {
                    username: req.body.username,
                    password: hash,
                    email: req.body.email,
                    role_id: req.body.role_id,
                    userdetail: {
                      firstname: req.body.firstname,
                      lastname: req.body.lastname,
                      contact1: req.body.contact1,
                      contact2: req.body.contact2,
                      address: req.body.address,
                      yearofpractice: req.body.yearofpractice,
                      picturepath: req.body.picturepath
                    }
                  };
                  User.create(NewUser, {
                    include: [{ model: UserDetail, as: "userdetail" }]
                  })
                    .then(result => {
                      res.status(201).json({
                        success: 1,
                        message: "User Created",
                        data: result
                      });
                    })
                    .catch(err => {
                      res.status(500).json(err);
                    });
                }
              });
            } else {
              res.status(409).json({
                success: 0,
                message: "User is already registered with given email address"
              });
            }
          })
          .catch(err => {
            res.status(500).json(err);
          });
      } else {
        res.status(409).json({
          success: 0,
          message: "User is already registered with given username"
        });
      }
    })
    .catch(err => {
      res.status(500).json(err);
    });
});

router.get("/id/:id", middleware.auth, (req, res) => {
  User.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "userdetail"
      }
    ]
  })
    .then(user => {
      res.status(200).json(user);
    })
    .catch(err => res.status(500).json(err));
});

router.patch(
  "/:id/status",
  [middleware.auth, middleware.validations],
  (req, res) => {
    User.findOne({
      where: { id: req.params.id },
      paranoid: false
    })
      .then(user => {
        if (user != null) {
          user.update({ isactive: req.body.status });
          if (req.body.status == 1) user.restore();
          user.setDataValue("deleted_at", null);
          res.status(200).json(user);
        } else {
          res.status(400).json({
            error: "User is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  }
);

router.put("/:id", middleware.auth, (req, res) => {
  User.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "userdetail"
      }
    ]
  })
    .then(user => {
      if (user != null) {
        if (user.email == req.body.email) {
          if (req.body.password != null) {
            bcrypt.hash(req.body.password, env.SALTROUNDS, (err, hash) => {
              if (err) {
                res.status(500).json(err);
              } else {
                const UpdatedAttorney = {
                  password: hash,
                  userdetail: {
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    contact1: req.body.contact1,
                    contact2: req.body.contact2,
                    address: req.body.address,
                    yearofpractice: req.body.yearofpractice,
                    picturepath: req.body.picturepath
                      ? req.body.picturepath
                      : user.picturepath
                  }
                };
                user.update({ password: UpdatedAttorney.password });
                user.userdetail.update(UpdatedAttorney.userdetail);
                res.status(200).json(user);
              }
            });
          } else {
            const UpdatedAttorney = {
              userdetail: {
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                contact1: req.body.contact1,
                contact2: req.body.contact2,
                address: req.body.address,
                yearofpractice: req.body.yearofpractice,
                picturepath: req.body.picturepath
                  ? req.body.picturepath
                  : user.picturepath
              }
            };
            user.userdetail.update(UpdatedAttorney.userdetail);
            res.status(200).json(user);
          }
        } else {
          User.findOne({
            where: { email: req.body.email },
            include: [
              {
                association: "userdetail"
              }
            ]
          })
            .then(user => {
              if (user == null) {
                if (req.body.password != null) {
                  bcrypt.hash(
                    req.body.password,
                    env.SALTROUNDS,
                    (err, hash) => {
                      if (err) {
                        res.status(500).json(err);
                      } else {
                        const UpdatedAttorney = {
                          password: hash,
                          email: req.body.email,
                          userdetail: {
                            firstname: req.body.firstname,
                            lastname: req.body.lastname,
                            contact1: req.body.contact1,
                            contact2: req.body.contact2,
                            address: req.body.address,
                            yearofpractice: req.body.yearofpractice,
                            picturepath: req.body.picturepath
                              ? req.body.picturepath
                              : user.picturepath
                          }
                        };
                        user.update({
                          password: UpdatedAttorney.password,
                          email: UpdatedAttorney.email
                        });
                        user.userdetail.update(UpdatedAttorney.userdetail);
                        res.status(200).json(user);
                      }
                    }
                  );
                } else {
                  const UpdatedAttorney = {
                    email: req.body.email,
                    userdetail: {
                      firstname: req.body.firstname,
                      lastname: req.body.lastname,
                      contact1: req.body.contact1,
                      contact2: req.body.contact2,
                      address: req.body.address,
                      yearofpractice: req.body.yearofpractice,
                      picturepath: req.body.picturepath
                        ? req.body.picturepath
                        : user.picturepath
                    }
                  };
                  user.update({
                    email: UpdatedAttorney.email
                  });
                  user.userdetail.update(UpdatedAttorney.userdetail);
                  res.status(200).json(user);
                }
              } else {
                res.status(400).json({
                  error: "User is alread found with the given new email address"
                });
              }
            })
            .catch(err => res.status(500).json(err));
        }
      } else {
        res.status(400).json({
          error: "User is not found"
        });
      }
    })
    .catch(err => res.status(500).json(err));
});

router.delete("/:id", middleware.auth, (req, res) => {
  User.findOne({
    where: { id: req.params.id }
  })
    .then(user => {
      if (user != null) {
        user.update({ isactive: 0 });
        user.destroy();
        res.status(200).json({});
      } else {
        res.status(400).json({
          error: "User is not found"
        });
      }
    })
    .catch(err => res.status(500).json(err));
});

router.get("/:userID/inbox", middleware.auth, (req, res, next) => {
  Message.findAll({
    where: { attorney_id: req.params.userID },
    include: [{ association: "user", include: [{ association: "userdetail" }] }]
  })
    .then(messages => {
      res.status(200).json(messages);
    })
    .catch(err => next(err));
});

module.exports = router;
