const costTypeRoutes = require("./accounts/costtypes");
const timesheetCostRoutes = require("./accounts/timesheetcosts");
const taxRoutes = require("./accounts/taxes");
const quoteRoutes = require("./accounts/quotes");
const invoiceRoutes = require("./accounts/invoices");
const smsRoutes = require("./accounts/sms-messages");
const defaultMessageRoutes = require("./accounts/default-messages");
const bankDetailRoutes = require("./accounts/bank-details");
const creditNoteRoutes = require("./accounts/credit-notes");
const transactionRoutes = require("./accounts/transactions");
const invoiceTemplateRoutes = require("./accounts/invoice-template");

exports.routes = app => {
  app.use("/api/accounts/costtypes", costTypeRoutes);
  app.use("/api/accounts/timesheet-costs", timesheetCostRoutes);
  app.use("/api/accounts/taxes", taxRoutes);
  app.use("/api/accounts/quotes", quoteRoutes);
  app.use("/api/accounts/invoices", invoiceRoutes);
  app.use("/api/accounts/sms-messages", smsRoutes);
  app.use("/api/accounts/default-messages", defaultMessageRoutes);
  app.use("/api/accounts/bank-details", bankDetailRoutes);
  app.use("/api/accounts/credit-notes", creditNoteRoutes);
  app.use("/api/accounts/transactions", transactionRoutes);
  app.use("/api/accounts/invoice-template", invoiceTemplateRoutes);
};
