const reportRoutes = require("./reports/matters");
const thirdpartyRoutes = require("./reports/thirdparties");
const taskRoutes = require("./reports/tasks");
const clientRoutes = require("./reports/clients");
const hearingRoutes = require("./reports/hearings");
const itemSalesRoutes = require("./reports/salesitem");
const balancesRoutes = require("./reports/balances");
const transactionRoutes = require("./reports/transactions");

exports.routes = app => {
  app.use("/api/reports/matters", reportRoutes);
  app.use("/api/reports/thirdparties", thirdpartyRoutes);
  app.use("/api/reports/tasks", taskRoutes);
  app.use("/api/reports/clients", clientRoutes);
  app.use("/api/reports/hearings", hearingRoutes);
  app.use("/api/reports/item-sales", itemSalesRoutes);
  app.use("/api/reports/balances", balancesRoutes);
  app.use("/api/reports/transactions", transactionRoutes);
};
