const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const env = require("../../env");
const { check, validationResult } = require("express-validator/check");
const User = require("../models/User");
const Sequelize = require("sequelize");
const LoginLog = require("../models/LoginLog");
const UserPermission = require("../models/UserPermission");

router.post(
    "/", [
        check("username").isLength({ min: 1 }),
        check("password").isLength({ min: 1 })
    ],
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ success: 0, errors: errors.array() });
        }

        let usernameOrEmail = req.body.username;
        let password = req.body.password;

        User.findOne({
                where: {
                    [Sequelize.Op.or]: {
                        username: usernameOrEmail,
                        email: usernameOrEmail
                    }
                },
                include: [
                    { association: "userdetail" },
                    {
                        association: "permissions",
                        attributes: [
                            "permission_id",
                            "view",
                            "add",
                            "edit",
                            "delete",
                            "task_assign_admin",
                            "task_assign_attorney"
                        ],
                        include: [
                            { association: "calendarAttorneys" },
                            { association: "permission" }
                        ]
                    }
                ]
            })
            .then(user => {
                if (user != null) {
                    bcrypt.compare(password, user.password, (err, result) => {
                        if (err) next(err);

                        if (result) {
                            const token = jwt.sign({ username: usernameOrEmail, password: password },
                                env.jwtSecret, { expiresIn: "36500d" }
                            );
                            user.update({ last_login: Date.now() });
                            LoginLog.create({
                                ip: req.headers["x-real-ip"] || "0.0.0.0",
                                attorney_id: user.id
                            });
                            res.status(201).json({
                                success: 1,
                                data: {
                                    access_token: "Bearer " + token,
                                    user: user,
                                    ip: req.connection.remoteAddress
                                },
                                message: "Login Success"
                            });
                        } else {
                            res.status(404).json({
                                success: 0,
                                error: 404,
                                data: {},
                                message: "Username or Password is not found in our records"
                            });
                        }
                    });
                } else {
                    res.status(404).json({
                        success: 0,
                        error: 404,
                        data: {},
                        message: "Username or Password is not found in our records"
                    });
                }
            })
            .catch(err => res.status(500).json(err));
    }
);




module.exports = router;