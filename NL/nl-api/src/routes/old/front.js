const contactRoutes = require("./front/contact");
const uploadImage = require("./front/upload");
const Signup = require("./front/signup");
const express = require("express");
const router = express.Router();
const multer = require("multer");
const User = require("../models/User");
const UserDetail = require("../models/UserDetail");
const bcrypt = require("bcryptjs");
const env = require("../../env");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Message = require("../models/Message");
const blogs = require('./front/blog');
const catagory = require('./front/catagory');

exports.routes = app => {
    app.use("/api/front/contact", contactRoutes);
    app.use("/api/front/uploadImage", uploadImage);
    app.use("/api/front/signup", Signup);
    app.use("/api/front/blogs", blogs);
    app.use("/api/front/catagory", catagory);

};