const tasksRoutes = require("./dashboard/tasks");
const meetingsRoutes = require("./dashboard/meetings");
const summaryRoutes = require("./dashboard/summarys");
const calendarListRoutes = require("./dashboard/calendarList");
const calendarRoutes = require("./dashboard/calendar");

exports.routes = app => {
  app.use("/api/dashboard/tasks", tasksRoutes);
  app.use("/api/dashboard/meetings", meetingsRoutes);
  app.use("/api/dashboard/summary", summaryRoutes);
  app.use("/api/dashboard/calendar-list", calendarListRoutes);
  app.use("/api/dashboard/calendar", calendarRoutes);
};
