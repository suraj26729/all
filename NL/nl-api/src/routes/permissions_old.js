const express = require("express");
const router = express.Router();
const Permission = require("../models/Permission");
const UserPermission = require("../models/UserPermission");
const CalendarAttorney = require("../models/PermissionCalendarAttorneys");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");

const middleware = {
  validations: [check("name").isString(), check("url").isString()],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    const NewPermission = { name: req.body.name };

    Permission.findOne({
      where: { name: req.body.name }
    }).then(permission => {
      if (permission == null) {
        Permission.create(req.body)
          .then(permission => {
            res.status(201).json(permission);
          })
          .catch(err => next(err));
      } else {
        res.status(400).json("Permission is already created with same name");
      }
    });
  }
);

router.get("/attorney/list", middleware.auth, (req, res, next) => {
  Permission.findAll({
    where: { isactive: 1, id: { $notIn: [25, 26, 27] } },
    order: [["id", "ASC"]]
  })
    .then(permissions => {
      res.status(200).json(permissions);
    })
    .catch(next);
});

router.get("/admin/list", middleware.auth, (req, res, next) => {
  Permission.findAll({ where: { isactive: 1 }, order: [["id", "ASC"]] })
    .then(permissions => {
      res.status(200).json(permissions);
    })
    .catch(next);
});

router.post(
  "/user/:id",
  [middleware.auth, [check("permissions").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }
    UserPermission.findAll({ where: { user_id: req.params.id } })
      .then(permissions => {
        UserPermission.destroy({ where: { user_id: req.params.id } })
          .then(result => {
            UserPermission.bulkCreate(req.body.permissions, {
              individualHooks: true
            })
              .then(result => {
                let calAtt = [];
                result.map((r, index) => {
                  if (req.body.permissions[index].calendar_attorneys.length) {
                    req.body.permissions[index].calendar_attorneys.map(ca => {
                      calAtt.push({
                        user_permission_id: r.id,
                        attorney_id: ca
                      });
                    });
                  }
                });
                if (calAtt.length) {
                  CalendarAttorney.bulkCreate(calAtt)
                    .then(result2 => res.status(201).json({ result, result2 }))
                    .catch(err => console.log(err));
                } else {
                  res.status(201).json({ result });
                }
              })
              .catch(next);
          })
          .catch(next);
      })
      .catch(next);
  }
);

router.get("/user/:id", middleware.auth, (req, res, next) => {
  UserPermission.findAll({
    where: { user_id: req.params.id },
    include: [{ association: "calendarAttorneys" }],
    order: [["permission_id", "ASC"]]
  })
    .then(permissions => {
      let t_cal_at = [];
      let data = permissions.map(p => {
        t_cal_at = [];
        if (p.calendarAttorneys.length) {
          t_cal_at = p.calendarAttorneys.map(ca => ca.attorney_id);
        }
        return {
          add: p.add,
          assignee: p.assignee,
          calendar_attorneys: t_cal_at,
          created_at: p.created_at,
          delete: p.delete,
          edit: p.edit,
          id: p.id,
          isactive: p.isactive,
          permission_id: p.permission_id,
          task_assign_admin: p.task_assign_admin,
          task_assign_attorney: p.task_assign_attorney,
          updated_at: p.updated_at,
          user_id: p.user_id,
          view: p.view
        };
      });
      res.status(200).json(data);
    })
    .catch(next);
});

module.exports = router;
