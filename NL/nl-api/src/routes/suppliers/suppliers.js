const express = require("express");
const router = express.Router();
const multer = require("multer");
const Supplier = require("../../models/Supplier");
const SupplierContact = require("../../models/SupplierContacts");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const Op = sequelize.Op;
const User = require("../../models/User");
const common = require("../../helpers/common");

const middleware = {
  validations: [check("firstname").isString(), check("user_id").isInt()],
  auth: checkToken,
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload = multer({
  storage: storage,
  // limits: { fileSize: 4194304 },
  // limits: { fileSize: 1024 * 1024 * 4 }, // 4 MB
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    ) {
      cb(null, true);
    } else {
      cb(new Error("Only jpeg, jpg, png format files allowed!"), false);
    }
  },
});

const uploadImage = upload.single("profile_img");
let limit = 15;

router.post("/uploadImage", uploadImage, middleware.auth, (req, res) => {
  uploadImage(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(400).json(err);
    } else {
      return res.status(201).json({
        success: 1,
        data: req.file,
        message: "Image uploaded successfully",
      });
    }
  });
});

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    let userIds = await common.firmUsers(req.body.user_id);
    let supplier = null;
    if (req.body.email != "") {
      supplier = await Supplier.findOne({
        where: { email: req.body.email, user_id: userIds },
      });
    }

    if (supplier == null) {
      const NewSupplier = {
        companyname: req.body.companyname || "",
        companyreg: req.body.companyreg || "",
        companyvat: req.body.companyvat || "",
        firstname: req.body.firstname || "",
        surname: req.body.surname || "",
        email: req.body.email || "",
        creditlimits: req.body.creditlimits || "",
        contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
        contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
        mobile: req.body.mobile_code + "-" + req.body.mobile || "",
        dob: req.body.dob || "",
        bankdetails: req.body.bankdetails || "",
        personaldetails: req.body.personaldetails || "",
        address1: req.body.address1 || "",
        address2: req.body.address2 || "",
        city: req.body.city || "",
        zipcode: req.body.zipcode || "",
        country: req.body.country || "",
        province: req.body.province || "",
        type: req.body.type || "",
        picturepath: req.body.picturepath || "",
        isactive: 1,
        user_id: req.body.user_id || "",
        suppliercontact: req.body.contacts,
      };
      let result = await Supplier.create(NewSupplier, {
        include: [{ model: SupplierContact, as: "suppliercontact" }],
      });
      if (result) {
        const NewContact = {
          supplier_id: result.id || "",
          firstname: req.body.firstname || "",
          surname: req.body.surname || "",
          email: req.body.email || "",
          creditlimits: req.body.creditlimits || "",
          contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
          contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
          mobile: req.body.mobile_code + "-" + req.body.mobile || "",
          dob: req.body.dob || "",
          bankdetails: req.body.bankdetails || "",
          personaldetails: req.body.personaldetails || "",
          address1: req.body.address1 || "",
          address2: req.body.address2 || "",
          city: req.body.city || "",
          zipcode: req.body.zipcode || "",
          country: req.body.country || "",
          province: req.body.province || "",
          type: "c",
          isactive: 1,
          user_id: req.body.user_id || "",
        };
        let result2 = await SupplierContact.create(NewContact);
        res.status(201).json({
          success: 1,
          message: "Supplier Created",
          data: result,
          contact: result2,
        });
      }
    } else {
      res.status(200).json({
        success: 0,
        data: {},
        message: "Supplier is already found with the given new email address",
      });
    }
  }
);

router.get("/", middleware.auth, async (req, res, next) => {
  try {
    let offset = 0;
    const getsup = await Supplier.count();
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(getsup / limit);
    offset = limit * (page - 1);
    const getallsup = await Supplier.findAndCountAll({
      include: [
        {
          association: "suppliercontact",
        },
      ],
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
    });

    res.status(200).json({
      success: 1,
      pages: pages,
      data: getallsup,
      message: "List of suppliers",
    });
  } catch (err) {
    next(err);
  }
});

router.get("/user/:userId", middleware.auth, async (req, res) => {
  try {
    let search = req.query.search
      ? req.query.search
      : undefined
      ? req.query.search
      : "";
    let offset = 0;
    let userIds = await common.firmUsers(req.params.userId);
    let cond = {};
    if (search !== "") {
      cond = {
        $or: [
          {
            email: {
              [Op.like]: "%" + search + "%",
            },
          },
          {
            companyname: {
              [Op.like]: "%" + search + "%",
            },
          },
          {
            firstname: {
              [Op.like]: "%" + search + "%",
            },
          },
          {
            surname: {
              [Op.like]: "%" + search + "%",
            },
          },
          {
            mobile: {
              [Op.like]: "%" + search + "%",
            },
          },
          {
            address1: {
              [Op.like]: "%" + search + "%",
            },
          },
        ],
        user_id: userIds,
      };
    } else {
      cond = {
        user_id: userIds,
      };
    }

    //console.log(cond);

    const particular = await Supplier.count({
      where: cond,
      //user_id: userIds
    });
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(particular / limit);
    offset = limit * (page - 1);
    const particularsupplier = await Supplier.findAndCountAll({
      include: [
        {
          association: "suppliercontact",
        },
      ],
      where: cond,
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
    });

    particularsupplier["count"] = particular;
    res.status(200).json({
      success: 1,
      pages: pages,
      data: particularsupplier,
      search: cond,
      user: userIds,
      message: "List of suppliers for a particular user",
    });
  } catch (err) {
    next(err);
  }
});

router.get("/:id", middleware.auth, async (req, res) => {
  try {
    const gettsup = await Supplier.findOne({
      where: { id: req.params.id },
      include: [
        {
          association: "suppliercontact",
        },
      ],
      paranoid: false,
    });

    res.status(200).json({
      success: 1,
      data: gettsup,
      message: "Supplier is found",
    });
  } catch (err) {
    next(err);
  }
});

router.patch(
  "/:id/status",
  [middleware.auth, middleware.validations],
  async (req, res) => {
    try {
      const patsup = await Supplier.findOne({
        where: { id: req.params.id },
        paranoid: false,
      });

      if (patsup != null) {
        patsup.update({ isactive: req.body.status });
        if (req.body.status == 1) patsup.restore();
        patsup.setDataValue("deleted_at", null);
        res.status(200).json({
          success: 1,
          data: patsup,
          message: "Supplier Status is updated",
        });
      } else {
        res.status(200).json({
          success: 0,
          data: {},
          message: "Supplier is not found",
        });
      }
    } catch (err) {
      next(err);
    }
  }
);

router.put(
  "/:id",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    let supplier = await Supplier.findOne({
      where: { id: req.params.id },
      include: [
        {
          association: "suppliercontact",
        },
      ],
    });
    if (supplier != null) {
      let userIds = await common.firmUsers(supplier.user_id);
      let dob = req.body.dob;
      if (typeof dob === "object" && dob != null) {
        dob =
          dob.year +
          "-" +
          (dob.month < 10 ? "0" + dob.month : dob.month) +
          "-" +
          (dob.day < 10 ? "0" + dob.day : dob.day);
      }
      const UpdatedSupplier = {
        companyname: req.body.companyname || "",
        companyreg: req.body.companyreg || "",
        companyvat: req.body.companyvat || "",
        firstname: req.body.firstname || "",
        surname: req.body.surname || "",
        email: req.body.email || "",
        creditlimits: req.body.creditlimits || "",
        contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
        contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
        mobile: req.body.mobile_code + "-" + req.body.mobile || "",
        dob: dob || "",
        bankdetails: req.body.bankdetails || "",
        personaldetails: req.body.personaldetails || "",
        address1: req.body.address1 || "",
        address2: req.body.address2 || "",
        city: req.body.city || "",
        zipcode: req.body.zipcode || "",
        country: req.body.country || "",
        province: req.body.province || "",
        picturepath: req.body.picturepath || "",
      };
      if (supplier.email == req.body.email) {
        supplier.update(UpdatedSupplier);

        res.status(200).json({
          success: 1,
          data: supplier,
          message: "Supplier Info is updated",
        });
      } else if (req.body.email != "") {
        let fsupplier = await Supplier.findOne({
          where: { email: req.body.email, user_id: userIds },
          include: [
            {
              association: "suppliercontact",
            },
          ],
        });
        if (fsupplier == null) {
          supplier.update(UpdatedSupplier);
          res.status(200).json({
            success: 1,
            data: supplier,
            message: "Supplier Info is updated",
          });
        } else {
          res.status(200).json({
            success: 0,
            data: {},
            message:
              "Supplier is already found with the given new email address",
          });
        }
      } else {
        supplier.update(UpdatedSupplier);
        res.status(200).json({
          success: 1,
          data: supplier,
          message: "Supplier Info is updated",
        });
      }
    } else {
      res.status(200).json({
        success: 0,
        data: {},
        message: "Supplier is not found",
      });
    }
  }
);

router.delete("/:id", middleware.auth, async (req, res) => {
  try {
    if (req.params.id) {
      const dellsup = await Supplier.findOne({
        where: { id: req.params.id },
      });

      if (dellsup != null) {
        dellsup.update({ isactive: 0 });
        dellsup.destroy();
        res.status(200).json({
          success: 1,
          data: dellsup,
          message: "Supplier is deleted",
        });
      } else {
        res.status(200).json({
          success: 0,
          data: {},
          message: "Supplier is not found",
        });
      }
    } else {
      res.status(500).json({
        success: 0,
        data: {},
        message: "Supplier Id is not found in the request",
      });
    }
  } catch (err) {
    next(err);
  }
});

router.delete(
  "/bulk/delete",
  [middleware.auth, [check("ids").isArray()]],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      const sup2 = await Supplier.findOne({
        where: {
          id: {
            [Op.in]: req.body.ids,
          },
        },
      });

      if (sup2 != null) {
        sup2.update({ isactive: 0 });
        sup2.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Supplier are not found",
        });
      }
    } catch (err) {
      next(err);
    }
  }
);

router.patch(
  "/bulk/active",
  [middleware.auth, [check("ids").isArray()]],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      const sup1 = await Supplier.findOne({
        where: {
          id: {
            [Op.in]: req.body.ids,
          },
        },
      });

      if (sup1 != null) {
        sup1.update({ isactive: 1 });
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Supplier are not found",
        });
      }
    } catch (err) {
      next(err);
    }
  }
);

router.patch(
  "/bulk/inactive",
  [middleware.auth, [check("ids").isArray()]],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let sup = await Supplier.findOne({
        where: {
          id: {
            [Op.in]: req.body.ids,
          },
        },
      });

      if (sup != null) {
        sup.update({ isactive: 0 });
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Users are not found",
        });
      }
    } catch (err) {
      next(err);
    }
  }
);

router.get("/all/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.userID);
    let suppliers = await Supplier.findAll({
      where: { user_id: userIds, isactive: 1 },
      order: [["firstname", "ASC"]],
    });
    let results = [];
    for (let supplier of suppliers) {
      results.push({
        address1: supplier.address1,
        address2: supplier.address2,
        bankdetails: supplier.bankdetails,
        city: supplier.city,
        companyname: supplier.companyname,
        companyreg: supplier.companyreg,
        companyvat: supplier.companyvat,
        contact1: supplier.contact1,
        contact2: supplier.contact2,
        country: supplier.country,
        created_at: supplier.created_at,
        creditlimits: supplier.creditlimits,
        deleted_at: supplier.deleted_at,
        dob: supplier.dob,
        email: supplier.email,
        firstname:
          supplier.type == "c" ? supplier.companyname : supplier.firstname,
        id: supplier.id,
        isactive: supplier.isactive,
        mobile: supplier.mobile,
        personaldetails: supplier.personaldetails,
        picturepath: supplier.picturepath,
        province: supplier.province,
        surname: supplier.type == "c" ? "" : supplier.surname,
        type: supplier.type,
        updated_at: supplier.updated_at,
        user_id: supplier.user_id,
        zipcode: supplier.zipcode,
      });
    }
    res.status(200).json(results);
  } catch (err) {
    next(err);
  }
});

router.post(
  "/all/timer/user/:userID",
  middleware.auth,
  async (req, res, next) => {
    try {
      // var ids = [req.params.userID];
      // let user = await User.findOne({ where: { id: req.params.userID } });
      // if (user.parent_user_id == null) {
      //   let allusers = await User.findAll({
      //     where: { parent_user_id: req.params.userID }
      //   });
      //   for (au of allusers) {
      //     ids.push(au.id);
      //   }
      // }
      // let mainids = req.body;
      // if (mainids.length) {
      //   for (let i of mainids) {
      //     ids.push(i);
      //   }
      // }
      let ids = await common.firmUsers(req.params.userID);
      let suppliers = await Supplier.findAll({
        where: { user_id: ids, isactive: 1 },
      });
      res.status(200).json(suppliers);
    } catch (err) {
      next(err);
    }
  }
);

router.get("/archive/user/:userId", middleware.auth, async (req, res) => {
  let search = req.query.search
    ? req.query.search
    : undefined
    ? req.query.search
    : "";
  let offset = 0;
  let userIds = await common.firmUsers(req.params.userId);
  let cond = {};
  if (search !== "") {
    cond = {
      $or: [
        {
          email: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          companyname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          firstname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          surname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          mobile: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          address1: {
            [Op.like]: "%" + search + "%",
          },
        },
      ],
      user_id: userIds,
      deleted_at: { $not: null },
    };
  } else {
    cond = {
      user_id: userIds,
      deleted_at: { $not: null },
    };
  }

  //console.log(cond);

  Supplier.count({
    where: cond,
    paranoid: false,
    //user_id: userIds
  }).then((count) => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    limit = 9999999;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    Supplier.findAndCountAll({
      include: [
        {
          association: "suppliercontact",
        },
      ],
      where: cond,
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
      paranoid: false,
    })
      .then((suppliers) => {
        suppliers["count"] = count;
        res.status(200).json({
          success: 1,
          pages: pages,
          data: suppliers,
          search: cond,
          user: userIds,
          message: "List of archive suppliers for a particular user",
        });
      })
      .catch((err) => res.status(500).json(err));
  });
});

module.exports = router;
