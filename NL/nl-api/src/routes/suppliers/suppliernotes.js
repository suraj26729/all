const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const SupNotes = require("../../models/SupNotes");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const middleware = {
  validations: [check("content").isLength({ min: 1 })],
  auth: checkToken,
};

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    SupNotes.create(req.body)
      .then((NewNote) => {
        return res.status(201).json(NewNote);
      })
      .catch((error) => res.status(500).json({ error: error }));
  }
);

router.get("/supplier/:parentID", middleware.auth, async (req, res) => {
  SupNotes.findAll({
    where: { parent_id: req.params.parentID, type: "supplier" },
    order: [["created_at", "DESC"]],
  })
    .then((notes) => {
      return res.status(200).json(notes);
    })
    .catch((error) => res.status(500).json({ error: error }));
});

router.post(
  "/MorefileNotes/:parentID",
  middleware.auth,
  async (req, res, next) => {
    try {
      let notes = await SupNotes.findAndCountAll({
        where: {
          parent_id: req.params.parentID,
          id: {
            [Op.notIn]: req.body,
          },
        },
        order: [["created_at", "DESC"]],
        limit: 2,
      });
      return res.status(200).json(notes);
    } catch (error) {
      next(error);
    }
  }
);

router.delete("/:id", middleware.auth, (req, res) => {
  SupNotes.findOne({
    where: { id: req.params.id },
  })
    .then((note) => {
      if (note != null) {
        note.destroy();
        return res.status(200).json(note);
      } else {
        return res.status(404).json({});
      }
    })
    .catch((error) => res.status(500).json({ error: error }));
});

module.exports = router;
