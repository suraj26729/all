const express = require("express");
const sq = require("sequelize");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const db = require("../../config/database");
const SupInvoice = require("../../models/accounts/SupInvoice");
const common = require("../../helpers/common");

router.get("/user/:user_id", checkToken, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.user_id);
    userIds = userIds.join(",");

    let query = `select supplier_id, firstname, surname, companyname,type,
    sum(rate5) as rate5, sum(rate4) as rate4, sum(rate3) as rate3, sum(rate2) as rate2, sum(rate1) as rate1
    
    from (select supplier_invoices.supplier_id, supplier_invoices.id, supplier_invoices.invoice_number, 	supplier_details.firstname, 	supplier_details.surname, 	supplier_details.companyname,
    	supplier_details.type,
    
    TRUNCATE((case when supplier_invoices.created_at <= NOW() - INTERVAL 3 MONTH then (ROUND(supplier_invoices.total,1)-ifnull(amount,'0')) else 0 end ),2)  as rate5,
    
    TRUNCATE((case when supplier_invoices.created_at <= NOW() - INTERVAL 2 MONTH and supplier_invoices.created_at >= NOW() - INTERVAL 3 MONTH 
    then (ROUND(supplier_invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate4, 
    
    TRUNCATE((case when supplier_invoices.created_at <= NOW() - INTERVAL 1 MONTH and supplier_invoices.created_at >= NOW() - INTERVAL 2 MONTH 
    then (ROUND(supplier_invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate3,
    
    TRUNCATE((case when supplier_invoices.created_at <= NOW() - INTERVAL 1 DAY and supplier_invoices.created_at >= NOW() - INTERVAL 1 MONTH 
    then (ROUND(supplier_invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate2,
    
    TRUNCATE((case when supplier_invoices.created_at >= NOW() - INTERVAL 1 DAY then (ROUND(supplier_invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate1,
    
    IFNULL(a.amount,0) as paid_amt, supplier_invoices.created_at as crdate
    
    FROM supplier_invoices
    
    JOIN 	supplier_details ON 	supplier_details.id=supplier_invoices.supplier_id
    
    LEFT JOIN (select ROUND(sum(amount),2) as amount, invoice_id from supplier_money_allocations group by invoice_id ) a on a.invoice_id = supplier_invoices.id
    
    WHERE 	supplier_details.isactive =  1 AND 	supplier_details.user_id IN (${userIds}) AND 	supplier_details.deleted_at IS null
    
    ORDER BY supplier_invoices.id desc, supplier_invoices.created_at DESC) as aaa group by supplier_id`;

    let invoice = await db.query(query, { type: sq.QueryTypes.SELECT });

    return res.status(200).json(invoice);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get(
  "/user/:userId/supplier/:supplierId",
  checkToken,
  async (req, res, next) => {
    let userIds = await common.firmUsers(req.params.userId);
    userIds = userIds.join(",");
    let query = `select supplier_invoices.supplier_id, supplier_invoices.id, supplier_invoices.invoice_number, supplier_details.firstname, supplier_details.surname, supplier_details.companyname,supplier_details.type,
    'i' as type,

    TRUNCATE((case when supplier_invoices.created_at <= NOW() - INTERVAL 3 MONTH then ROUND(supplier_invoices.total,1) else 0 end ),2)  as rate5,
    
    TRUNCATE((case when supplier_invoices.created_at <= NOW() - INTERVAL 2 MONTH and supplier_invoices.created_at >= NOW() - INTERVAL 3 MONTH 
    then ROUND(supplier_invoices.total,1) else 0 end),2)  as rate4, 
    
    TRUNCATE((case when supplier_invoices.created_at <= NOW() - INTERVAL 1 MONTH and supplier_invoices.created_at >= NOW() - INTERVAL 2 MONTH 
    then ROUND(supplier_invoices.total,1) else 0 end),2)  as rate3,
    
    TRUNCATE((case when supplier_invoices.created_at <= NOW() - INTERVAL 1 DAY and supplier_invoices.created_at >= NOW() - INTERVAL 1 MONTH 
    then ROUND(supplier_invoices.total,1) else 0 end),2)  as rate2,
    
    TRUNCATE((case when supplier_invoices.created_at >= NOW() - INTERVAL 1 DAY then ROUND(supplier_invoices.total,1) else 0 end),2)  as rate1,
    
    IFNULL(a.amount,0) as paid_amt, supplier_invoices.created_at as crdate
    
    FROM supplier_invoices
    
    JOIN supplier_details ON supplier_details.id = supplier_invoices.supplier_id

   
    
    LEFT JOIN (select ROUND(sum(amount),2) as amount, invoice_id from supplier_money_allocations group by invoice_id ) a on a.invoice_id = supplier_invoices.id
    
    WHERE supplier_details.isactive =  1 AND supplier_details.user_id IN (${userIds}) AND supplier_details.id = '${req.params.supplierId}' AND supplier_details.deleted_at IS null
    
    ORDER BY supplier_invoices.id desc, supplier_invoices.created_at DESC`;

    let invoice = await db.query(query, { type: sq.QueryTypes.SELECT });

    let result = [...invoice].sort((a, b) => (a.crdate < b.crdate ? 1 : -1));

    return res.status(200).json(result);
  }
);

module.exports = router;
