const express = require("express");
const router = express.Router();
const Category = require("../../models/SupplierCat");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");
const middleware = {
  validations: [check("name").isLength({ min: 1 }), check("user_Id").isInt()],
  auth: checkToken,
};

let limit = 10;

// router.get("/list/user/:userID", middleware.auth, (req, res, next) => {
//   let page = req.query.page || 1;
//   limit = parseInt(req.query.limit) || limit;
//   Category.paginate({
//     where: { user_id: req.params.userID },
//     page: page,
//     paginate: limit,
//     order: ["name"],
//   })
//     .then((categories) => {
//       res.status(200).json(categories);
//     })
//     .catch((err) => next(err));
// });

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ success: 0, errors: errors.array() });
      }

      let userIds = await common.firmUsers(req.body.user_Id);
      let suppliercat = null;
      if (req.body.user_Id != "") {
        suppliercat = await Category.findOne({
          where: { name: req.body.name, user_Id: userIds },
        });
      }

      if (suppliercat == null) {
        const NewCategory = { name: req.body.name, user_Id: req.body.user_Id };
        let result = await Category.create(NewCategory);
        res.status(201).json({
          success: 1,
          message: "Supplier Category Created",
          data: result,
        });
      } else {
        res.status(200).json({
          success: 0,
          data: {},
          message: "This Category Already Created",
        });
      }
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get("/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let id = [req.params.userID];

    let user = await Category.findOne({
      where: { user_Id: req.params.userID },
    });
    if (user !== null) {
      let categories = await Category.findAndCountAll({
        where: { user_Id: id },
        order: ["name"],
      });
      return res.status(200).json(categories);
    } else {
      return res.status(404).json("No category found for this user");
    }
  } catch (err) {
    next(err);
  }
});

router.get("/id/:id", middleware.auth, async (req, res, next) => {
  try {
    let category = await Category.findOne({ where: { id: req.params.id } });

    if (category != null) {
      res.status(200).json(category);
    } else {
      res.status(404).json({
        error: "Category is not found",
      });
    }
  } catch (err) {
    next(err);
  }
});

router.patch("/:id", middleware.auth, async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    let id_Value = await Category.findOne({
      where: { id: req.params.id },
    });

    if (id_Value !== null) {
      const check = await Category.findAll({
        where: {
          user_Id: id_Value.user_Id,
          name: req.body.name,
          isactive: 1,
        },
      });

      console.log(check);
      if (check.length === 0) {
        let cat = await Category.update(
          { name: req.body.name },
          { where: { id: req.params.id } }
        );

        res.status(200).json(cat);
      } else {
        res.status(400).json({
          error: "Category is already created with same name and its active",
        });
      }
    } else {
      res.status(404).json({
        error: "Category is not found",
      });
    }
  } catch (err) {
    next(err);
  }
});

router.patch("/:id/status", middleware.auth, async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }
    let check2 = await Category.findOne({
      where: { id: req.params.id },
    });

    if (check2 !== null) {
      if (req.body.status == 1) {
        let cat1 = await Category.update(
          { isactive: 1 },
          { where: { id: req.params.id } }
        );
        res.status(200).json({
          success: 1,
          data: {},
          message: "category is active now",
        });
      }
      if (req.body.status == 0) {
        let cat1 = await Category.update(
          { isactive: 0 },
          { where: { id: req.params.id } }
        );
        res.status(200).json({
          success: 1,
          data: {},
          message: "category is inactive now",
        });
      }
    } else {
      res.status(400).json({
        error: "category is not found",
      });
    }
  } catch (err) {
    next(err);
  }
});

router.delete("/:id", middleware.auth, async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }
    const del = await Category.findOne({
      where: { id: req.params.id },
    });

    if (del != null) {
      del.destroy();
      res.status(204).json({});
    } else {
      res.status(404).json({
        error: "Category is not found",
      });
    }
  } catch (err) {
    next(err);
  }
});
module.exports = router;
