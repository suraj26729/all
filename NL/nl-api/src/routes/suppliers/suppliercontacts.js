const express = require("express");
const router = express.Router();
const multer = require("multer");
const Supplier = require("../../models/Supplier");
const SupplierContact = require("../../models/SupplierContacts");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const Op = sequelize.Op;

const middleware = {
  validations: [check("firstname").isString(), check("user_id").isInt()],
  auth: checkToken,
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    // let contact = null;
    // if (req.body.email != "") {
    //     contact = await SupplierContact.findOne({
    //         where: { supplier_id: req.body.supplier_id, email: req.body.email }
    //     });
    // }
    // if (contact == null) {
    let dob = req.body.dob;
    if (typeof dob === "object" && dob !== null) {
      dob =
        dob.year +
        "-" +
        (dob.month < 10 ? "0" + dob.month : dob.month) +
        "-" +
        (dob.day < 10 ? "0" + dob.day : dob.day);
    }
    const NewContact = {
      supplier_id: req.body.supplier_id || "",
      firstname: req.body.firstname || "",
      surname: req.body.surname || "",
      email: req.body.email || "",
      creditlimits: req.body.creditlimits || "",
      contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
      contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
      mobile: req.body.mobile_code + "-" + req.body.mobile || "",
      dob: dob || "",
      bankdetails: req.body.bankdetails || "",
      personaldetails: req.body.personaldetails || "",
      address1: req.body.address1 || "",
      address2: req.body.address2 || "",
      city: req.body.city || "",
      zipcode: req.body.zipcode || "",
      country: req.body.country || "",
      province: req.body.province || "",
      type: req.body.type || "c",
      isactive: 1,
      user_id: req.body.user_id || "",
    };
    let result = await SupplierContact.create(NewContact);
    res.status(201).json({
      success: 1,
      message: "Supplier contact Created",
      data: result,
    });
    // } else {
    //     res.status(400).json({
    //         message: "Supplier contact or third party is already found with the given new email address"
    //     });
    // }
  }
);

router.get("/contacts/:supplierId", middleware.auth, async (req, res, next) => {
  try {
    const contacts = await SupplierContact.findAndCountAll({
      where: {
        supplier_id: req.params.supplierId,
        type: "c",
      },
    });

    res.status(200).json({
      success: 1,
      data: contacts,
      message: "List of supplier contacts for a particular supplier",
    });
  } catch (err) {
    next(err);
  }
});

router.get(
  "/thirdparties/:supplierId",
  middleware.auth,
  async (req, res, next) => {
    try {
      const thirdparty = await SupplierContact.findAndCountAll({
        where: {
          supplier_id: req.params.supplierId,
          type: "t",
        },
      });

      res.status(200).json({
        success: 1,
        data: thirdparty,
        message: "List of supplier third parties for a particular supplier",
      });
    } catch (err) {
      next(err);
    }
  }
);

router.put(
  "/:id",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const pcontacts = await SupplierContact.findOne({
        where: { id: req.params.id },
      });

      if (pcontacts != null) {
        let dob = req.body.dob;
        if (typeof dob === "object" && dob != null) {
          dob =
            dob.year +
            "-" +
            (dob.month < 10 ? "0" + dob.month : dob.month) +
            "-" +
            (dob.day < 10 ? "0" + dob.day : dob.day);
        }
        if (pcontacts.email == req.body.email) {
          const UpdatedContact = {
            firstname: req.body.firstname || "",
            surname: req.body.surname || "",
            email: req.body.email || "",
            creditlimits: req.body.creditlimits || "",
            contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
            contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
            mobile: req.body.mobile_code + "-" + req.body.mobile || "",
            dob: dob || "",
            bankdetails: req.body.bankdetails || "",
            personaldetails: req.body.personaldetails || "",
            address1: req.body.address1 || "",
            address2: req.body.address2 || "",
            city: req.body.city || "",
            zipcode: req.body.zipcode || "",
            country: req.body.country || "",
            province: req.body.province || "",
          };
          pcontacts.update(UpdatedContact);
          res.status(200).json({
            success: 1,
            data: pcontacts,
            message: "Supplier Contact Info is updated",
          });
        } else {
          const p2contact = await SupplierContact.findOne({
            where: { email: req.body.email },
          });

          if (p2contact == null) {
            const UpdatedContact = {
              firstname: req.body.firstname || "",
              surname: req.body.surname || "",
              email: req.body.email || "",
              creditlimits: req.body.creditlimits || "",
              contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
              contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
              mobile: req.body.mobile_code + "-" + req.body.mobile || "",
              dob: dob || "",
              bankdetails: req.body.bankdetails || "",
              personaldetails: req.body.personaldetails || "",
              address1: req.body.address1 || "",
              address2: req.body.address2 || "",
              city: req.body.city || "",
              zipcode: req.body.zipcode || "",
              country: req.body.country || "",
              province: req.body.province || "",
            };
            pcontacts.update(UpdatedContact);
            res.status(200).json({
              success: 1,
              data: pcontacts,
              message: "Supplier Contact Info is updated",
            });
          } else {
            res.status(200).json({
              success: 0,
              data: {},
              message:
                "Supplier Contact is already found with the given new email address",
            });
          }
        }
      } else {
        res.status(200).json({
          success: 0,
          data: {},
          message: "Supplier Contact is not found",
        });
      }
    } catch (err) {
      next(err);
    }
  }
);

router.delete("/:id", middleware.auth, async (req, res, next) => {
  try {
    if (req.params.id) {
      const delcontact = await SupplierContact.findOne({
        where: { id: req.params.id },
      });

      if (delcontact != null) {
        delcontact.update({ isactive: 0 });
        delcontact.destroy();
        res.status(200).json({
          success: 1,
          data: delcontact,
          message: "Supplier Contact is deleted",
        });
      } else {
        res.status(200).json({
          success: 0,
          data: {},
          message: "Supplier Contact is not found",
        });
      }
    } else {
      res.status(500).json({
        success: 0,
        data: {},
        message: "Supplier Contact Id is not found in the request",
      });
    }
  } catch (err) {
    next(err);
  }
});

let limit = 999;

router.get(
  "/list/supplier/:supplierId",
  middleware.auth,
  async (req, res, next) => {
    try {
      let offset = 0;
      const getcout = await SupplierContact.count({
        where: {
          supplier_id: req.params.supplierId,
          type: "c",
        },
      });
      limit = req.query.limit ? parseInt(req.query.limit) : limit;
      limit = 999999;
      let page = req.query.page ? req.query.page : 1; // page number
      let pages = Math.ceil(getcout / limit);
      offset = limit * (page - 1);
      const getcon = await SupplierContact.findAndCountAll({
        where: {
          supplier_id: req.params.supplierId,
          type: "c",
        },
        order: [["created_at", "DESC"]],
        limit: limit,
        offset: offset,
      });

      getcon["count"] = getcout;
      res.status(200).json({
        pages: pages,
        data: getcon,
      });
    } catch (err) {
      next(err);
    }
  }
);

router.get(
  "/thirdparties/list/supplier/:supplierId",
  middleware.auth,
  async (req, res, next) => {
    try {
      let offset = 0;
      const geetcout = await SupplierContact.count({
        where: {
          supplier_id: req.params.supplierId,
          type: "t",
        },
      });
      limit = req.query.limit ? parseInt(req.query.limit) : limit;
      limit = 999999;
      let page = req.query.page ? req.query.page : 1; // page number
      let pages = Math.ceil(geetcout / limit);
      offset = limit * (page - 1);
      const thirdparties = await SupplierContact.findAndCountAll({
        where: {
          supplier_id: req.params.supplierId,
          type: "t",
        },
        order: [["created_at", "DESC"]],
        limit: limit,
        offset: offset,
      });

      thirdparties["count"] = geetcout;
      res.status(200).json({
        pages: pages,
        data: thirdparties,
      });
    } catch (err) {
      next(err);
    }
  }
);

router.get("/thirdparty/id/:id", middleware.auth, async (req, res, next) => {
  try {
    const thirdpartyid = await SupplierContact.findByPk(req.params.id);

    res.status(200).json(thirdpartyid);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
