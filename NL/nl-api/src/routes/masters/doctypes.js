const express = require("express");
const router = express.Router();
const DocType = require("../../models/masters/DocType");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");

const middleware = {
  validations: [check("name").isLength({ min: 1 }), check("user_id").isInt()],
  auth: checkToken
};
const Common = require("../../helpers/common");
let limit = 10;

router.get("/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let userIDS = await Common.firmUsers(req.params.userID);
    let doctypes = await DocType.findAndCountAll({
      where: { user_id: userIDS, isactive: 1 },
      order: ["name"]
    });
    res.status(200).json(doctypes);
  } catch (error) {
    next(error);
  }
});

router.get("/list/user/:userID", middleware.auth, async (req, res, next) => {
  let userIDS = await Common.firmUsers(req.params.userID);
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  DocType.paginate({
    where: { user_id: userIDS },
    page: page,
    paginate: limit,
    order: ["name"]
  })
    .then(doctypes => {
      res.status(200).json(doctypes);
    })
    .catch(err => next(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewDocType = { name: req.body.name, user_id: req.body.user_id };

  DocType.findOne({
    where: NewDocType
  }).then(doctype => {
    if (doctype == null) {
      DocType.create(NewDocType)
        .then(doctype => {
          res.status(201).json(doctype);
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(400).json({
        error: "DocType is already created with same name"
      });
    }
  });
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
  DocType.findOne({ where: { id: req.params.id } })
    .then(doctype => {
      if (doctype != null) {
        res.status(200).json(doctype);
      } else {
        res.status(404).json({
          error: "DocType is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch(
  "/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    DocType.findOne({
      where: { id: req.params.id }
    })
      .then(doctype => {
        if (doctype != null) {
          DocType.findOne({
            where: {
              name: req.body.name,
              user_id: doctype.user_id,
              $not: { id: req.params.id }
            }
          })
            .then(fdoctype => {
              if (fdoctype == null) {
                doctype.update({ name: req.body.name });
                res.status(200).json(doctype);
              } else {
                res.status(400).json({
                  error: "DocType is already created with same name"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "DocType is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch("/:id/status", middleware.auth, (req, res, next) => {
  DocType.findOne({
    where: { id: req.params.id }
  })
    .then(doctype => {
      if (doctype != null) {
        doctype.update({ isactive: doctype.isactive ? 0 : 1 });
        res.status(200).json(doctype);
      } else {
        res.status(404).json({
          error: "DocType is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.delete("/:id", middleware.auth, (req, res, next) => {
  DocType.findOne({
    where: { id: req.params.id }
  })
    .then(doctype => {
      if (doctype != null) {
        doctype.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "DocType is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
