const express = require("express");
const router = express.Router();
const location = require("country-state-city");
const sequelize = require("../../config/database");

router.get("/", (req, res, next) => {
    let countries = location.getAllCountries();
    return res.status(200).json(countries);
    // sequelize
    //   .query("SELECT id, nicename as name from martincrm2.countries", {
    //     type: sequelize.QueryTypes.SELECT
    //   })
    //   .then(countries => {
    //     res.status(200).json(countries);
    //   })
    //   .catch(err => {
    //     next(err);
    //   });
});

router.get("/:id/states", (req, res, next) => {
    if (req.params.id == 202) {
        sequelize
            .query(
                "SELECT id, province as name from provinces where country_id=" +
                Number(197), {
                    type: sequelize.QueryTypes.SELECT
                }
            )
            .then(states => {
                res.status(200).json(states);
            })
            .catch(err => next(err));
    } else {
        let states = location.getStatesOfCountry(req.params.id);
        return res.status(200).json(states);
    }
});

router.get("/state/:id/cities", (req, res, next) => {
    // let cities = location.getCitiesOfState(req.params.id);
    // return res.status(200).json(cities);
    sequelize
        .query(
            "SELECT id, name from cities where province_id=" +
            req.params.id, {
                type: sequelize.QueryTypes.SELECT
            }
        )
        .then(cities => {
            res.status(200).json(cities);
        })
        .catch(err => next(err));
});

module.exports = router;