const express = require("express");
const router = express.Router();
const Court = require("../../models/masters/Court");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");

const middleware = {
  validations: [check("name").isLength({ min: 1 }), check("user_id").isInt()],
  auth: checkToken
};

let limit = 10;

router.get("/user/:userID", middleware.auth, async (req, res) => {
  let userIds = await common.firmUsers(req.params.userID);
  Court.findAndCountAll({
    where: { user_id: userIds, isactive: 1 },
    order: ["name"]
  })
    .then(courts => {
      res.status(200).json(courts);
    })
    .catch(err => res.status(500).json(err));
});

router.get("/list/user/:userID", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.userID);
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  Court.paginate({
    where: { user_id: userIds },
    page: page,
    paginate: limit,
    order: ["name"]
  })
    .then(courts => {
      res.status(200).json(courts);
    })
    .catch(err => next(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewCourt = { name: req.body.name, user_id: req.body.user_id };

  Court.findOne({
    where: NewCourt
  }).then(court => {
    if (court == null) {
      Court.create(NewCourt)
        .then(court => {
          res.status(201).json(court);
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(400).json({
        error: "Court is already created with same name"
      });
    }
  });
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Court.findOne({ where: { id: req.params.id } })
    .then(court => {
      if (court != null) {
        res.status(200).json(court);
      } else {
        res.status(404).json({
          error: "Court is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch(
  "/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Court.findOne({
      where: { id: req.params.id }
    })
      .then(court => {
        if (court != null) {
          Court.findOne({
            where: {
              name: req.body.name,
              user_id: req.body.user_id,
              $not: { id: req.params.id }
            }
          })
            .then(fcourt => {
              if (fcourt == null) {
                court.update({ name: req.body.name });
                res.status(200).json(court);
              } else {
                res.status(400).json({
                  error: "Court is already created with same name"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "Court is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch("/:id/status", middleware.auth, (req, res, next) => {
  Court.findOne({
    where: { id: req.params.id }
  })
    .then(court => {
      if (court != null) {
        court.update({ isactive: court.isactive ? 0 : 1 });
        res.status(200).json(court);
      } else {
        res.status(404).json({
          error: "Court is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.delete("/:id", middleware.auth, (req, res, next) => {
  Court.findOne({
    where: { id: req.params.id }
  })
    .then(court => {
      if (court != null) {
        court.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Court is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
