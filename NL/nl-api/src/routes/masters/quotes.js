const express = require("express");
const router = express.Router();
const Quote = require("../../models/masters/Quote");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");
const Sequelize = require("sequelize");

const middleware = {
  validations: [
    check("title").isLength({ min: 1 }),
    check("desc").isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

let limit = 10;

router.get("/user/:userID", middleware.auth, async (req, res) => {
  let userIds = await common.firmUsers(req.params.userID);
  Quote.findAndCountAll({
    where: { user_id: userIds, isactive: 1 },
    order: ["title"]
  })
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => res.status(500).json(err));
});

router.get("/list/user/:userID", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.userID);
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  Quote.paginate({
    where: { user_id: userIds },
    page: page,
    paginate: limit,
    order: ["title"]
  })
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => next(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewQuote = {
    title: req.body.title,
    desc: req.body.desc,
    user_id: req.body.user_id
  };

  Quote.findOne({
    where: NewQuote
  }).then(quote => {
    if (quote == null) {
      Quote.create(NewQuote)
        .then(quote => {
          res.status(201).json(quote);
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(400).json({
        error: "Quote is already created with same title"
      });
    }
  });
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Quote.findOne({ where: { id: req.params.id } })
    .then(quote => {
      if (quote != null) {
        res.status(200).json(quote);
      } else {
        res.status(404).json({
          error: "Quote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch(
  "/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Quote.findOne({
      where: { id: req.params.id }
    })
      .then(quote => {
        if (quote != null) {
          Quote.findOne({
            where: {
              title: req.body.title,
              desc: req.body.desc,
              user_id: quote.user_id,
              $not: { id: req.params.id }
            }
          })
            .then(fquote => {
              if (fquote == null) {
                quote.update({
                  title: req.body.title,
                  desc: req.body.desc
                });
                res.status(200).json(quote);
              } else {
                res.status(400).json({
                  error: "Quote is already created with same content"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "Quote is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch("/:id/status", middleware.auth, (req, res, next) => {
  Quote.findOne({
    where: { id: req.params.id }
  })
    .then(quote => {
      if (quote != null) {
        quote.update({ isactive: quote.isactive ? 0 : 1 });
        res.status(200).json(quote);
      } else {
        res.status(404).json({
          error: "Quote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.delete("/:id", middleware.auth, (req, res, next) => {
  Quote.findOne({
    where: { id: req.params.id }
  })
    .then(quote => {
      if (quote != null) {
        quote.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Quote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.get("/random/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.userID);
    let quote = await Quote.findOne({
      where: { user_id: userIds },
      order: Sequelize.literal("rand()")
    });
    return res.status(200).json(quote);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
