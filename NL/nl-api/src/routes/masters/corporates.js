const express = require("express");
const router = express.Router();
const Corporate = require("../../models/masters/Corporate");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");

const middleware = {
  validations: [check("name").isLength({ min: 1 }), check("user_id").isInt()],
  auth: checkToken
};

let limit = 10;

router.get("/user/:userID", middleware.auth, (req, res) => {
  Corporate.findAndCountAll({
    where: { user_id: req.params.userID, isactive: 1 },
    order: ["name"]
  })
    .then(corporates => {
      res.status(200).json(corporates);
    })
    .catch(err => res.status(500).json(err));
});

router.get("/list/user/:userID", middleware.auth, (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  Corporate.paginate({
    where: { user_id: req.params.userID },
    page: page,
    paginate: limit,
    order: ["name"]
  })
    .then(corporates => {
      res.status(200).json(corporates);
    })
    .catch(err => next(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewCorporate = { name: req.body.name, user_id: req.body.user_id };

  Corporate.findOne({
    where: NewCorporate
  }).then(corporate => {
    if (corporate == null) {
      Corporate.create(NewCorporate)
        .then(corporate => {
          res.status(201).json(corporate);
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(400).json({
        error: "Corporate is already created with same name"
      });
    }
  });
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Corporate.findOne({ where: { id: req.params.id } })
    .then(corporate => {
      if (corporate != null) {
        res.status(200).json(corporate);
      } else {
        res.status(404).json({
          error: "Corporate is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch(
  "/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Corporate.findOne({
      where: { id: req.params.id }
    })
      .then(corporate => {
        if (corporate != null) {
          Corporate.findOne({
            where: { name: req.body.name, $not: { id: req.params.id } }
          })
            .then(fcorporate => {
              if (fcorporate == null) {
                corporate.update({ name: req.body.name });
                res.status(200).json(corporate);
              } else {
                res.status(400).json({
                  error: "Corporate is already created with same name"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "Corporate is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch("/:id/status", middleware.auth, (req, res, next) => {
  Corporate.findOne({
    where: { id: req.params.id }
  })
    .then(corporate => {
      if (corporate != null) {
        corporate.update({ isactive: corporate.isactive ? 0 : 1 });
        res.status(200).json(corporate);
      } else {
        res.status(404).json({
          error: "Corporate is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.delete("/:id", middleware.auth, (req, res, next) => {
  Corporate.findOne({
    where: { id: req.params.id }
  })
    .then(corporate => {
      if (corporate != null) {
        corporate.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Corporate is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
