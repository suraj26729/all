const express = require("express");
const router = express.Router();
const currencycodes = require("../../../currencysymbol.json");

router.get("/", (req, res, next) => {
    let result = currencycodes.map(item => {
        return item
    });
    return res.json(result);
});

module.exports = router;