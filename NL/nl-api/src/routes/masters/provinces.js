const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const Country = require("../../models/masters/Country");
const Province = require("../../models/masters/Province");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const fs = require("fs");

const middleware = {
  validations: [
    check("name").isLength({ min: 1 }),
    check("country_id").isInt(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.get("/", middleware.auth, (req, res) => {
  Province.findAll({
    attributes: ["id", "name"]
  })
    .then(provinces => {
      res.status(200).json({
        success: 1,
        data: provinces,
        message: "List of provinces"
      });
    })
    .catch(err => res.status(500).json(err));
});

router.get("/country/:countryId", middleware.auth, (req, res) => {
  Province.findAll({
    attributes: ["id", "name"],
    where: { country_id: req.params.countryId }
  })
    .then(provinces => {
      res.status(200).json({
        success: 1,
        data: provinces,
        message: "List of provinces for particular country"
      });
    })
    .catch(err => res.status(500).json(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewProvince = {
    name: req.body.name,
    country_id: req.body.country_id,
    user_id: req.body.user_id
  };

  Province.findOne({
    where: {
      [Sequelize.Op.and]: {
        name: req.body.name,
        country_id: req.body.country_id
      }
    }
  }).then(province => {
    if (province == null) {
      Province.create(NewProvince)
        .then(province => {
          res.status(201).json({
            success: 1,
            data: province,
            message: "Province Created Successfully"
          });
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(200).json({
        success: 0,
        data: {},
        message: "Province is already created with same name"
      });
    }
  });
});

router.get("/:id", middleware.auth, (req, res) => {
  if (req.params.id) {
    Province.findOne({
      where: { id: req.params.id }
    })
      .then(province => {
        if (province != null) {
          res.status(200).json({
            success: 1,
            data: province,
            message: "Province is found"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Province is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Province Id is not found in the request"
    });
  }
});

router.patch("/:id", [middleware.auth, middleware.validations], (req, res) => {
  if (req.params.id) {
    Province.findOne({
      where: { id: req.params.id }
    })
      .then(province => {
        if (province != null) {
          Province.update({
            name: req.body.name,
            country_id: req.body.country_id
          });
          res.status(200).json({
            success: 1,
            data: province,
            message: "Province is updated"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Province is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Province Id is not found in the request"
    });
  }
});

router.delete("/:id", middleware.auth, (req, res) => {
  if (req.params.id) {
    Province.findOne({
      where: { id: req.params.id }
    })
      .then(province => {
        if (province != null) {
          Province.destroy();
          res.status(200).json({
            success: 1,
            data: province,
            message: "Province is deleted"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Province is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Province Id is not found in the request"
    });
  }
});

// router.post("/import", (req, res) => {
//   let rawdata = fs.readFileSync("assets/country-states.json");
//   let data = JSON.parse(rawdata);
//   let countries = data.countries;
//   for (country of countries) {
//     let states = country.states;
//     Country.findOne({
//       where: {
//         name: country.country
//       }
//     })
//       .then(country => {
//         if (country != null) {
//           const DBSTATES = [];
//           for (state of states) {
//             DBSTATES.push({ name: state, country_id: country.id });
//           }
//           Province.bulkCreate(DBSTATES)
//             .then(result => {
//               res.status(201).json({
//                 success: 1,
//                 data: null,
//                 message: "Successfully imported"
//               });
//             })
//             .catch(err => res.status(500).json(err));
//         }
//       })
//       .catch(err => res.status(500).json(err));
//   }
// });

module.exports = router;
