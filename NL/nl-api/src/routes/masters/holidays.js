const express = require("express");
const router = express.Router();
const Holiday = require("../../models/masters/Holiday");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");

const middleware = {
  validations: [
    check("content").isLength({ min: 1 }),
    check("start_date").isLength({ min: 1 }),
    check("end_date").isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

let limit = 10;

router.get("/user/:userID", middleware.auth, async (req, res) => {
  let userIDS = await common.firmUsers(req.params.userID);
  Holiday.findAndCountAll({
    where: { user_id: userIDS, isactive: 1 },
    order: ["content"]
  })
    .then(holidays => {
      res.status(200).json(holidays);
    })
    .catch(err => res.status(500).json(err));
});

router.get("/list/user/:userID", middleware.auth, async (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  let userIDS = await common.firmUsers(req.params.userID);
  Holiday.paginate({
    where: { user_id: userIDS },
    page: page,
    paginate: limit,
    order: ["content"]
  })
    .then(holidays => {
      res.status(200).json(holidays);
    })
    .catch(err => next(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewHoliday = {
    content: req.body.content,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    user_id: req.body.user_id
  };

  Holiday.findOne({
    where: NewHoliday
  }).then(holiday => {
    if (holiday == null) {
      Holiday.create(NewHoliday)
        .then(holiday => {
          res.status(201).json(holiday);
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(400).json({
        error: "Holiday is already created with same content"
      });
    }
  });
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Holiday.findOne({ where: { id: req.params.id } })
    .then(holiday => {
      if (holiday != null) {
        res.status(200).json(holiday);
      } else {
        res.status(404).json({
          error: "Holiday is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch(
  "/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Holiday.findOne({
      where: { id: req.params.id }
    })
      .then(holiday => {
        if (holiday != null) {
          Holiday.findOne({
            where: {
              content: req.body.content,
              start_date: req.body.start_date,
              end_date: req.body.end_date,
              user_id: holiday.user_id,
              $not: { id: req.params.id }
            }
          })
            .then(fholiday => {
              if (fholiday == null) {
                holiday.update({
                  content: req.body.content,
                  start_date: req.body.start_date,
                  end_date: req.body.end_date
                });
                res.status(200).json(holiday);
              } else {
                res.status(400).json({
                  error: "Holiday is already created with same content"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "Holiday is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch("/:id/status", middleware.auth, (req, res, next) => {
  Holiday.findOne({
    where: { id: req.params.id }
  })
    .then(holiday => {
      if (holiday != null) {
        holiday.update({ isactive: holiday.isactive ? 0 : 1 });
        res.status(200).json(holiday);
      } else {
        res.status(404).json({
          error: "Holiday is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.delete("/:id", middleware.auth, (req, res, next) => {
  Holiday.findOne({
    where: { id: req.params.id }
  })
    .then(holiday => {
      if (holiday != null) {
        holiday.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Holiday is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
