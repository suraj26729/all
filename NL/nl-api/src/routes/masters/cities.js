const express = require("express");
const router = express.Router();
const Sequelize = require('sequelize');
const City = require('../../models/masters/City');
const checkToken = require('../../middlewares/checkToken');
const { check, validationResult } = require('express-validator/check');

const middleware = {
    validations: [
        check('name').isLength({ min: 1 }),
        check('province_id').isInt(),
        check('user_id').isInt()
    ],
    auth: checkToken
}

router.get('/', middleware.auth,(req, res) => {
    City.findAndCountAll().then(cities => {
        res.status(200).json({
            success: 1,
            data: cities,
            message: 'List of cities'
        });
    }).catch(err => res.status(500).json(err));
});

router.post('/', [middleware.validations, middleware.auth], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ success: 0, errors: errors.array() });
    }

    const NewCity = { name: req.body.name, province_id: req.body.province_id, user_id: req.body.user_id };

    City.findOne({
        where: NewCity
    }).then(city => {
        if(city == null){
            City.create(NewCity).then(city => {
                res.status(201).json({
                    success: 1,
                    data: city,
                    message: 'City Created Successfully'
                });
            }).catch(err => res.status(500).json(err));
        } else {
            res.status(200).json({
                success: 0,
                data: {},
                message: 'City is already created with same name'
            })
        }
    });
});

router.get('/:id',middleware.auth, (req, res) => {
    if(req.params.id){
        City.findOne({
            where: {  id: req.params.id }
        }).then(city => {
            if(city != null){
                res.status(200).json({
                    success: 1,
                    data: city,
                    message: 'City is found'
                });
            } else {
                res.status(404).json({
                    success: 0,
                    data: {},
                    message: 'City is not found'
                });
            }
        }).catch(err => res.status(500).json(err));
    } else {
        res.status(500).json({
            success: 0,
            data: {},
            message: 'City Id is not found in the request'
        });
    }
});

router.patch('/:id', [middleware.auth, middleware.validations],(req, res) => {
    if(req.params.id){
        City.findOne({
            where: { id: req.params.id }
        }).then(city => {
            if(city!=null){
                city.update({ name: req.body.name, province_id: req.body.province_id });
                res.status(200).json({
                    success: 1,
                    data: city,
                    message: 'City is updated'
                });
            } else {
                res.status(404).json({
                    success: 0,
                    data: {},
                    message: 'City is not found'
                });
            }
        }).catch(err => res.status(500).json(err));
    } else {
        res.status(500).json({
            success: 0,
            data: {},
            message: 'City Id is not found in the request'
        });
    }
});

router.delete('/:id', middleware.auth, (req, res) => {
    if(req.params.id){
        City.findOne({
            where: { id: req.params.id }
        }).then(city => {
            if(city!=null){
                city.destroy();
                res.status(200).json({
                    success: 1,
                    data: city,
                    message: 'City is deleted'
                });
            } else {
                res.status(404).json({
                    success: 0,
                    data: {},
                    message: 'City is not found'
                });
            }
        }).catch(err => res.status(500).json(err));
    } else {
        res.status(500).json({
            success: 0,
            data: {},
            message: 'City Id is not found in the request'
        });
    }
});

module.exports = router;