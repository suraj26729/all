const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const Country = require("../../models/masters/Country");
const Province = require("../../models/masters/Province");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const fs = require("fs");

const middleware = {
  validations: [
    check("name").isLength({ min: 1 }),
    check("code").isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.get("/", middleware.auth, (req, res) => {
  Country.findAll({
    attributes: ["id", "name"]
  })
    .then(countries => {
      res.status(200).json({
        success: 1,
        data: countries,
        message: "List of countries"
      });
    })
    .catch(err => res.status(500).json(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewCountry = {
    name: req.body.name,
    code: req.body.code,
    user_id: req.body.user_id
  };

  Country.findOne({
    where: {
      code: req.body.code
    }
  }).then(country => {
    if (country == null) {
      Country.create(NewCountry)
        .then(city => {
          res.status(201).json({
            success: 1,
            data: country,
            message: "Country Created Successfully"
          });
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(200).json({
        success: 0,
        data: {},
        message: "Country is already created with same name"
      });
    }
  });
});

router.get("/:id", middleware.auth, (req, res) => {
  if (req.params.id) {
    Country.findOne({
      where: { id: req.params.id }
    })
      .then(country => {
        if (country != null) {
          res.status(200).json({
            success: 1,
            data: country,
            message: "Country is found"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Country is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Country Id is not found in the request"
    });
  }
});

router.patch("/:id", [middleware.auth, middleware.validations], (req, res) => {
  if (req.params.id) {
    Country.findOne({
      where: { id: req.params.id }
    })
      .then(country => {
        if (country != null) {
          Country.update({ name: req.body.name, code: req.body.code });
          res.status(200).json({
            success: 1,
            data: country,
            message: "Country is updated"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Country is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Country Id is not found in the request"
    });
  }
});

router.delete("/:id", middleware.auth, (req, res) => {
  if (req.params.id) {
    Country.findOne({
      where: { id: req.params.id }
    })
      .then(country => {
        if (country != null) {
          Country.destroy();
          res.status(200).json({
            success: 1,
            data: country,
            message: "Country is deleted"
          });
        } else {
          res.status(404).json({
            success: 0,
            data: {},
            message: "Country is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Country Id is not found in the request"
    });
  }
});

// router.post("/import", (req, res) => {
//   let rawdata = fs.readFileSync("assets/country-states.json");
//   let data = JSON.parse(rawdata);
//   let countries = data.countries;
//   for (country of countries) {
//     Country.findOne({
//       where: {
//         name: country.country
//       }
//     }).then(isfound => {
//       if (isfound == null) {
//         Country.create({
//           name: country.country
//         })
//           .then(db_country => {
//             let country_id = db_country.id;
//           })
//           .catch(err => res.status(500).json(err));
//       }
//     });
//   }
//   return res.json("done");
// });

module.exports = router;
