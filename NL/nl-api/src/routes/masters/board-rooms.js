const express = require("express");
const router = express.Router();
const BoardRoom = require("../../models/masters/BoardRoom");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");

const middleware = {
  validations: [check("name").isLength({ min: 1 }), check("user_id").isInt()],
  auth: checkToken
};

let limit = 10;

router.get("/user/:userID", middleware.auth, async (req, res) => {
  let userIds = await common.firmUsers(req.params.userID);
  BoardRoom.findAll({
    where: { user_id: userIds, isactive: 1 },
    order: ["name"]
  })
    .then(locations => {
      res.status(200).json(locations);
    })
    .catch(err => res.status(500).json(err));
});

router.get("/list/user/:userID", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.userID);
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  BoardRoom.paginate({
    where: { user_id: userIds },
    page: page,
    paginate: limit,
    order: ["name"]
  })
    .then(locations => {
      res.status(200).json(locations);
    })
    .catch(err => next(err));
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  const NewBoardRoom = {
    name: req.body.name,
    user_id: req.body.user_id
  };

  BoardRoom.findOne({
    where: NewBoardRoom
  }).then(location => {
    if (location == null) {
      BoardRoom.create(NewBoardRoom)
        .then(location => {
          res.status(201).json(location);
        })
        .catch(err => res.status(500).json(err));
    } else {
      res.status(400).json({
        error: "Board Room is already created with same name"
      });
    }
  });
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
  BoardRoom.findOne({ where: { id: req.params.id } })
    .then(location => {
      if (location != null) {
        res.status(200).json(location);
      } else {
        res.status(404).json({
          error: "Board Room is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch(
  "/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    BoardRoom.findOne({
      where: { id: req.params.id }
    })
      .then(location => {
        if (location != null) {
          BoardRoom.findOne({
            where: {
              name: req.body.name,
              user_id: location.user_id,
              $not: { id: req.params.id }
            }
          })
            .then(flocation => {
              if (flocation == null) {
                location.update({ name: req.body.name });
                res.status(200).json(location);
              } else {
                res.status(400).json({
                  error: "Board Room is already created with same name"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "Board Room is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch("/:id/status", middleware.auth, (req, res, next) => {
  BoardRoom.findOne({
    where: { id: req.params.id }
  })
    .then(location => {
      if (location != null) {
        location.update({ isactive: location.isactive ? 0 : 1 });
        res.status(200).json(location);
      } else {
        res.status(404).json({
          error: "Board Room is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.delete("/:id", middleware.auth, (req, res, next) => {
  BoardRoom.findOne({
    where: { id: req.params.id }
  })
    .then(location => {
      if (location != null) {
        location.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Board Room is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
