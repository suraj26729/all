const express = require("express");
const router = express.Router();
const Template = require("../../models/masters/Template");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");

const middleware = {
  validations: [
    check("name").isLength({ min: 1 }),
    check("content").isLength({ min: 1 })
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    Template.create(req.body)
      .then(template => res.status(201).json(template))
      .catch(err => next(err));
  }
);

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Template.findByPk(req.params.id)
    .then(template => res.status(200).json(template))
    .catch(next);
});

module.exports = router;
