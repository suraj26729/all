const express = require("express");
const router = express.Router();
const Category = require("../../models/masters/Category");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const User = require("../../models/User");
const middleware = {
  validations: [check("name").isLength({ min: 1 }), check("user_id").isInt()],
  auth: checkToken
};

let limit = 10;

// router.get("/user/:userID", middleware.auth, (req, res, next) => {
//   Category.findAndCountAll({
//     where: { user_id: req.params.userID, isactive: 1 },
//     order: ["name"]
//   })
//     .then(categories => {
//       res.status(200).json(categories);
//     })
//     .catch(err => next(err));
// });

router.get("/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let id = [req.params.userID];
    //let id = [];
    let user = await User.findOne({
      where: { id: req.params.userID }
    });
    if (user.parent_user_id) {
      id.push(user.parent_user_id);
    }
    let categories = await Category.findAndCountAll({
      where: { user_id: id, isactive: 1 },
      order: ["name"]
    });
    res.status(200).json(categories);
  } catch (err) {
    next(err);
  }
});

router.get("/list/user/:userID", middleware.auth, (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  Category.paginate({
    where: { user_id: req.params.userID },
    page: page,
    paginate: limit,
    order: ["name"]
  })
    .then(categories => {
      res.status(200).json(categories);
    })
    .catch(err => next(err));
});

router.post(
  "/",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    const NewCategory = { name: req.body.name, user_id: req.body.user_id };

    Category.findOne({
      where: NewCategory
    }).then(category => {
      if (category == null) {
        Category.create(NewCategory)
          .then(category => {
            res.status(201).json(category);
          })
          .catch(err => next(err));
      } else {
        res.status(400).json({
          error: "Category is already created with same name"
        });
      }
    });
  }
);

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Category.findOne({ where: { id: req.params.id } })
    .then(category => {
      if (category != null) {
        res.status(200).json(category);
      } else {
        res.status(404).json({
          error: "Category is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch(
  "/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Category.findOne({
      where: { id: req.params.id }
    })
      .then(category => {
        if (category != null) {
          Category.findOne({
            where: {
              name: req.body.name,
              user_id: category.user_id,
              $not: { id: req.params.id }
            }
          })
            .then(fcategory => {
              if (fcategory == null) {
                category.update({ name: req.body.name });
                res.status(200).json(category);
              } else {
                res.status(400).json({
                  error: "Category is already created with same name"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "Category is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch(
  "/:id/status",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    Category.findOne({
      where: { id: req.params.id }
    })
      .then(category => {
        if (category != null) {
          category.update({ isactive: category.isactive ? 0 : 1 });
          res.status(200).json(category);
        } else {
          res.status(404).json({
            error: "Category is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.delete("/:id", middleware.auth, (req, res, next) => {
  Category.findOne({
    where: { id: req.params.id }
  })
    .then(category => {
      if (category != null) {
        category.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Category is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
