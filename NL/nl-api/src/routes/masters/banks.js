const express = require("express");
const router = express.Router();
const Bank = require('../../models/masters/Bank');
const checkToken = require('../../middlewares/checkToken');
const { check, validationResult } = require('express-validator/check');

const middleware = {
    validations: [
        check('content').isLength({ min: 1 }),
        check('user_id').isInt()
    ],
    auth: checkToken
}

router.get('/', middleware.auth,(req, res) => {
    Bank.findAndCountAll().then(banks => {
        res.status(200).json({
            success: 1,
            data: banks,
            message: 'List of banks'
        });
    }).catch(err => res.status(500).json(err));
});

router.post('/', [middleware.validations, middleware.auth], (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ success: 0, errors: errors.array() });
    }

    const NewBank = { content: req.body.content, user_id: req.body.user_id };

    Bank.findOne({
        where: NewBank,
    }).then(bank => {
        if(bank == null){
            Bank.create(NewBank).then(bank => {
                res.status(201).json({
                    success: 1,
                    data: bank,
                    message: 'Bank Created Successfully'
                });
            }).catch(err => res.status(500).json(err));
        } else {
            res.status(200).json({
                success: 0,
                data: {},
                message: 'Bank is already created with same name'
            })
        }
    });
});

router.get('/:id',middleware.auth, (req, res) => {
    if(req.params.id){
        Bank.findOne({
            where: {  id: req.params.id }
        }).then(bank => {
            if(bank != null){
                res.status(200).json({
                    success: 1,
                    data: bank,
                    message: 'Bank is found'
                });
            } else {
                res.status(404).json({
                    success: 0,
                    data: {},
                    message: 'Bank is not found'
                });
            }
        }).catch(err => res.status(500).json(err));
    } else {
        res.status(500).json({
            success: 0,
            data: {},
            message: 'Bank Id is not found in the request'
        });
    }
});

router.patch('/:id', [middleware.auth, middleware.validations],(req, res) => {
    if(req.params.id){
        Bank.findOne({
            where: { id: req.params.id }
        }).then(bank => {
            if(bank!=null){
                bank.update({ content: req.body.content });
                res.status(200).json({
                    success: 1,
                    data: bank,
                    message: 'Bank is updated'
                });
            } else {
                res.status(404).json({
                    success: 0,
                    data: {},
                    message: 'Bank is not found'
                });
            }
        }).catch(err => res.status(500).json(err));
    } else {
        res.status(500).json({
            success: 0,
            data: {},
            message: 'Bank Id is not found in the request'
        });
    }
});

router.delete('/:id', middleware.auth, (req, res) => {
    if(req.params.id){
        Bank.findOne({
            where: { id: req.params.id }
        }).then(bank => {
            if(bank!=null){
                bank.destroy();
                res.status(200).json({
                    success: 1,
                    data: bank,
                    message: 'Bank is deleted'
                });
            } else {
                res.status(404).json({
                    success: 0,
                    data: {},
                    message: 'Bank is not found'
                });
            }
        }).catch(err => res.status(500).json(err));
    } else {
        res.status(500).json({
            success: 0,
            data: {},
            message: 'Bank Id is not found in the request'
        });
    }
});

module.exports = router;