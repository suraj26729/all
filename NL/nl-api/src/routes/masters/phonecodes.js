const express = require("express");
const router = express.Router();
const phonecodes = require("../../../phonecodes.json");

router.get("/", (req, res, next) => {
  let result = phonecodes.map(item => {
    return { country: item.ISO, name: item.Name, code: item.Code };
  });
  return res.json(result);
});

module.exports = router;
