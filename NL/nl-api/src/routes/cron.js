const express = require("express");
const router = express.Router();
const User = require("../models/User");
const bcrypt = require("bcryptjs");
const DateDiff = require("date-diff");
const fs = require("fs");
const mail = require("../config/mail");
const cron = require("node-cron");
const Moment = require("moment");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
cron.schedule("00 00 * * *", async () => {
  // let content = fs.readFileSync("./src/email-templates/three-day-notification-emil.html", "utf8");
  // // content = content.replace("**username**", req.body.username);
  // let sentMail = mail.send(
  //     "keegayu5662@gmail.com",
  //     "After 3 days",
  //     content,
  // );
  try {
    var users = await User.findAll({
      include: [{ association: "userdetail" }],
      where: {
        role_id: {
          [Op.notIn]: [1]
        }
      }
    });

    for (var i = 0; i < users.length; i++) {
      // console.log(users[i]);
      var today = Moment(new Date()).format("DD-MM-YYYY");
      var threedays = Moment(users[i].created_at)
        .add(3, "days")
        .format("DD-MM-YYYY");
      var sixdays = Moment(users[i].created_at)
        .add(6, "days")
        .format("DD-MM-YYYY");
      var ninedays = Moment(users[i].created_at)
        .add(9, "days")
        .format("DD-MM-YYYY");
      var twelvedays = Moment(users[i].created_at)
        .add(12, "days")
        .format("DD-MM-YYYY");
      var fourteendays = Moment(users[i].created_at)
        .add(14, "days")
        .format("DD-MM-YYYY");

      let username =
        users[i].userdetail.firstname + " " + users[i].userdetail.lastname;

      if (today == threedays) {
        let content = fs.readFileSync(
          "./src/email-templates/three-day-notification-emil.html",
          "utf8"
        );
        content = content.replace("**username**", username);
        let sentMail = mail.send(users[i].email, "After 3 days", content);
      } else if (today == sixdays) {
        let content = fs.readFileSync(
          "./src/email-templates/six-day-notification-email.html",
          "utf8"
        );
        content = content.replace("**username**", username);
        let sentMail = mail.send(users[i].email, "After 6 days", content);
      } else if (today == ninedays) {
        let content = fs.readFileSync(
          "./src/email-templates/nine-days-notification-email.html",
          "utf8"
        );
        content = content.replace("**username**", username);
        let sentMail = mail.send(users[i].email, "After 9 days", content);
      } else if (today == twelvedays) {
        let content = fs.readFileSync(
          "./src/email-templates/twele-days-notification-email.html",
          "utf8"
        );
        content = content.replace("**username**", username);
        let sentMail = mail.send(users[i].email, "After 12 days", content);
      } else if (today == fourteendays) {
        let content = fs.readFileSync(
          "./src/email-templates/fourteen-day-notification-email.html",
          "utf8"
        );
        content = content.replace("**username**", username);
        let sentMail = mail.send(users[i].email, "After 12 days", content);
      } else {
        console.log("Email send to no one");
      }

      //console.log({ today: today, threedays: threedays, sixdays: sixdays, ninedays: ninedays, twelvedays: twelvedays, fourteendays: fourteendays });
    }
  } catch (err) {
    console.log(err);
  }
});
