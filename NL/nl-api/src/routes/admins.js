const adminRoutes = require("../routes/admins/admins");
const clientHandlingRoutes = require("./admins/clienthandlings");
const messagingRoutes = require("./admins/messages");
const logRoutes = require("./admins/loginlogs");

exports.routes = app => {
  app.use("/api/admins", adminRoutes);
  app.use("/api/admins/client-handlings", clientHandlingRoutes);
  app.use("/api/admins/messages", messagingRoutes);
  app.use("/api/admins/logs", logRoutes);
};
