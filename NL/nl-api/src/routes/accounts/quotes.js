const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const Quote = require("../../models/accounts/Quote");
const QuoteItem = require("../../models/accounts/QuoteItems");
const Invoice = require("../../models/accounts/Invoice");
const User = require("../../models/User");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const helper = require("../../helpers/pdf");
const Op = Sequelize.Op;
let limit = 999;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const ClientContact = require("../../models/ClientContact");
const common = require("../../helpers/common");
const Email = require("../../models/Email");
const {
  getCountries,
  getCurrencySymbol,
  getCurrencySymbolFromIso2
} = require("country-from-iso2");
const Transaction = require("../../models/accounts/Transaction");

const middleware = {
  validations: [
    check("client_id").isUUID(),
    //check("case_id").isUUID(),
    //check("thirdparty_id").isUUID(),
    check("items")
      .isArray()
      .isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

const trustBalance = async case_id => {
  let deposits = await Transaction.findAll({
    include: [
      {
        association: "trust",
        where: { case_id: case_id },
        attributes: ["amount"]
      }
    ],
    where: { type: "deposit" },
    attributes: []
  });
  deposits = deposits.map(row => row.trust.amount);

  let deposit_sum = 0;
  if (deposits.length) {
    deposit_sum = deposits.reduce((a, b) => {
      return a + b;
    }, 0);
  }

  return deposit_sum;
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      var user = [];
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).json({ error: errors.array() });
      }

      if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;

      let quote = await Quote.create(req.body, {
        include: [{ association: "items" }]
      });

      quote = await Quote.findOne({
        where: { id: quote.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let firmusers = await common.firmUsers(quote.user_id);
      let quotes_count = await Quote.findAll({
        attributes: ["id"],
        where: { user_id: firmusers },
        paranoid: false
      });
      quote.update({ quote_number: quotes_count.length });

      for (let fu of firmusers) {
        let u = await User.findOne({
          where: { id: fu, parent_user_id: null },
          include: [{ association: "userdetail" }]
        });
        if (u !== null) {
          user.push(u);
        }
      }

      var currency = user[0].userdetail.currency;

      var cs = await getCurrencySymbol(currency);

      var currencySymbol = "";
      if (cs) {
        currencySymbol = cs;
      } else {
        currencySymbol = "R";
      }

      let balance = await trustBalance(quote.case_id);

      let url = await helper.generateQuotePDF({
        currency: currency,
        quote: quote,
        user_id: quote.user_id,
        balance: balance
      });

      console.log(url);

      if (url)
        await Quote.update({ attachment: url }, { where: { id: quote.id } });

      // var created_date = moment(quote.created_at).format("DD-MM-YYYY");
      // var items = [];
      // var total = [];
      // for (item of quote.items) {
      //     var itemdate = moment(item.created_at).format("DD-MM-YYYY");
      //     console.log(item.description);
      //     console.log(item.costtype.name);
      //     console.log(item.total);
      //     items.push(
      //         "<tr><td>" +
      //         item.description +
      //         "</td><td>" +
      //         item.costtype.name +
      //         "</td><td>" +
      //         item.total +
      //         "</td><td>" +
      //         item.quantity +
      //         "</td><td>" +
      //         item.price +
      //         "</td></tr>"
      //     );
      //     total.push(
      //         '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">' + currencySymbol + " " +
      //         item.price +
      //         '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">' + currencySymbol + " " +
      //         Math.abs(item.price - item.total) +
      //         '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">' + currencySymbol + " " +
      //         item.total +
      //         "</td></tr>"
      //     );
      // }

      // let content = fs.readFileSync(
      //     "./src/email-templates/crm/quote_template.html",
      //     "utf8"
      // );
      // content = content.replace("**bank_details**", quote.client.bankdetails);
      // content = content.replace("**notes**", quote.message);
      // content = content.replace("**balance**", req.body.trust_balance);
      // content = content.replace("**phone_number**", quote.client.contact1);
      // content = content.replace("**email**", quote.client.email);
      // content = content.replace("**number**", quote.quote_number);
      // content = content.replace("**quote_date**", created_date);
      // content = content.replace("**description**", quote.message);
      // content = content.replace("**client_reference**", "");
      // if (req.body.thirdparty_id) {
      //     content = content.replace(
      //         "**third_parties**",
      //         quote.thirdparty.firstname + " " + quote.thirdparty.surname
      //     );
      // } else {
      //     content = content.replace("**third_parties**", "");
      // }
      // content = content.replace(
      //     "**name**",
      //     quote.client.firstname + " " + quote.client.surname
      // );
      // content = content.replace("**items**", items);
      // content = content.replace("**total**", total);

      // if ((quote.client.type = "i")) {
      //     let sentMail = await mail.send(
      //         quote.client.email,
      //         "Quote",
      //         content,
      //         next
      //     );
      // } else if ((quote.client.type = "c" && quote.contact)) {
      //     let clientc = await ClientContact.findByPk(quote.contact);
      //     if (clientc != null) {
      //         let sentMail = await mail.send(clientc.email, "Quote", content, next);
      //     }
      // }

      res.status(200).json(quote);
    } catch (err) {
      console.log(err);
    }
  }
);

router.get("/paginate/user/:id", middleware.auth, async (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  limit = 9999999;
  let userIds = await common.firmUsers(req.params.id);
  Quote.paginate({
    where: { user_id: userIds, status: [0, 1] },
    page: page,
    paginate: limit,
    order: [["quote_number", "DESC"]],
    attributes: [
      "id",
      "uuid",
      "client_id",
      "quote_number",
      "payment_date",
      "total",
      "status",
      "attachment"
    ],
    include: [
      { association: "client" },
      { association: "casecontact" },
      { association: "thirdparty", attributes: ["firstname", "surname"] },
      { association: "matter", attributes: ["prefix", "casenumber"] }
    ]
  })
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => next(err));
});

router.get("/list/user/:id", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.id);
  Quote.findAll({
    where: { user_id: userIds, status: { $not: 2 } },
    attributes: ["id", "quote_number"]
  })
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => next(err));
});

router.get("/id/:uuid", middleware.auth, (req, res, next) => {
  Quote.findOne({
    where: { uuid: req.params.uuid },
    include: [
      {
        association: "items",
        include: [
          { association: "costtype", attributes: ["id", "name", "code"] }
        ]
      },
      { association: "client" },
      { association: "matter" },
      { association: "thirdparty" }
    ]
  })
    .then(quote => res.status(200).json(quote))
    .catch(err => next(err));
});

router.put(
  "/id/:uuid",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let quote = await Quote.findOne({
        where: { uuid: req.params.uuid },
        include: [{ association: "items" }, { association: "client" }]
      });

      if (quote == null)
        return res.status(404).json({ error: "Quote is not found" });

      let result = await QuoteItem.destroy({
        where: { quote_id: quote.id },
        force: true
      });

      if (result == 0)
        return res.status(400).json("Error while updating the items of quotes");

      req.body.payment_date = moment(
        req.body.payment_date,
        "DD-MM-YYYY"
      ).format("YYYY-MM-DD");

      quote.update(req.body);

      for (let item of req.body.items) {
        await QuoteItem.upsert(item, { where: { id: item.id } });
      }

      quote = await Quote.findOne({
        where: { id: quote.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let firmUsers = await common.firmUsers(quote.user_id);
      let user = await User.findOne({
        where: { id: firmUsers, parent_user_id: null, role_id: 2 }
      });

      let balance = await trustBalance(quote.case_id);
      let url = await helper.generateQuotePDF({
        quote: quote,
        user_id: quote.user_id,
        balance: balance
      });

      console.log(url);

      if (url) {
        quote = await Quote.update(
          { attachment: url },
          { where: { id: quote.id } }
        );
      }

      return res.status(200).json(quote);
    } catch (err) {
      next(err);
    }
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  Quote.findOne({
    where: { id: req.params.id }
  })
    .then(quote => {
      if (quote != null) {
        quote.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Quote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch("/id/:id/restore", middleware.auth, (req, res, next) => {
  Quote.findOne({
    where: { id: req.params.id },
    paranoid: false
  })
    .then(quote => {
      if (quote != null) {
        quote.setDataValue("deleted_at", null);
        quote.update({ deleted_at: null });
        res.status(200).json(quote);
      } else {
        res.status(404).json({
          error: "Quote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.get("/list/client/:id", middleware.auth, (req, res, next) => {
  Quote.findAll({ where: { client_id: req.params.id, status: { $not: 2 } } })
    .then(quotes => res.status(200).json(quotes))
    .catch(next);
});

router.patch("/convert/id/:id", middleware.auth, async (req, res, next) => {
  try {
    let invoice = await Invoice.findOne({ where: { quote_id: req.params.id } });
    if (invoice != null)
      return res.status(400).json("Invoice already created for this quote");

    let quote = await Quote.findByPk(req.params.id, {
      include: [{ association: "items" }]
    });

    if (quote == null) return res.status(404).json("Quote is not found");

    let firmusers = await common.firmUsers(quote.user_id);
    let max_invoice_num = await Invoice.findOne({
      attributes: [
        [Sequelize.fn("max", Sequelize.col("invoice_number")), "max"]
      ],
      where: { user_id: firmusers },
      raw: true,
      paranoid: false
    });

    let items = quote.items.map(item => {
      return {
        cost_id: item.cost_id,
        description: item.description,
        quantity: item.quantity,
        price: item.price,
        tax_id: item.tax_id,
        discount: item.discount,
        total: item.total,
        user_id: item.user_id
      };
    });

    // return res.json(max_invoice_num.max);

    invoice = await Invoice.create(
      {
        invoice_number: parseInt(max_invoice_num.max) + 1 || 1,
        client_id: quote.client_id,
        quote_id: quote.id,
        case_id: quote.case_id,
        thirdparty_id: quote.thirdparty_id,
        contact: quote.contact,
        payment_date: quote.payment_date,
        vat_ref: quote.vat_ref,
        message: quote.message,
        total: quote.total,
        status: 0,
        user_id: quote.user_id,
        items: items
      },
      { include: [{ association: "items" }] }
    );

    if (invoice != null) {
      quote.update({ status: 2 });

      var user = [];
      for (let fu of firmusers) {
        let u = await User.findOne({
          where: { id: fu, parent_user_id: null },
          include: [{ association: "userdetail" }]
        });
        if (u !== null) {
          user.push(u);
        }
      }

      var currency = user[0].userdetail.currency;
      var cs = await getCurrencySymbol(currency);

      var currencySymbol = "";
      if (cs) {
        currencySymbol = cs;
      } else {
        currencySymbol = "R";
      }

      invoice = await Invoice.findOne({
        where: { id: invoice.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          // { association: "quote" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let balance = await trustBalance(invoice.case_id);
      let url = await helper.generateInvoicePDF({
        currency: currency,
        invoice: invoice,
        user_id: invoice.user_id,
        balance: balance
      });

      await Invoice.update({ attachment: url }, { where: { id: invoice.id } });

      return res.status(201).json(invoice);
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/view/:id", middleware.auth, async (req, res, next) => {
  try {
    let quote = await Quote.findOne({
      where: { id: req.params.id },
      include: [
        { association: "items", include: [{ association: "costtype" }] },
        { association: "client" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });
    if (quote == null) return res.json("Quote not found");

    let result = await helper.generateQuotePDF({
      quote: quote,
      user_id: quote.user_id
    });
    res.json(result);
  } catch (err) {
    next(err);
  }
});

router.post("/sentBulk", async (req, res, next) => {
  try {
    var quotes = [];
    var user = [];
    quotes = await Quote.findAll({
      where: { id: req.body },
      include: [
        { association: "items", include: [{ association: "costtype" }] },
        { association: "client" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });

    for (let quote of quotes) {
      let firmusers = await common.firmUsers(quote.user_id);

      for (let fu of firmusers) {
        let u = await User.findOne({
          where: { id: fu, parent_user_id: null },
          include: [{ association: "userdetail" }]
        });
        if (u !== null) {
          user.push(u);
        }
      }

      var currency = user[0].userdetail.currency;

      var cs = await getCurrencySymbol(currency);

      var currencySymbol = "";
      if (cs) {
        currencySymbol = cs;
      } else {
        currencySymbol = "R";
      }

      var created_date = moment(quote.created_at).format("DD-MM-YYYY");
      var items = [];
      var total = [];
      for (item of quote.items) {
        var itemdate = moment(item.created_at).format("DD-MM-YYYY");
        items.push(
          "<tr><td>" +
            item.description +
            "</td><td>" +
            item.costtype.name +
            "</td><td>" +
            item.total +
            "</td><td>" +
            item.quantity +
            "</td><td>" +
            item.price +
            "</td></tr>"
        );
        total.push(
          '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.price +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            Math.abs(item.price - item.total) +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.total +
            "</td></tr>"
        );
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/quote_template.html",
        "utf8"
      );

      if (quote.client) {
        content = content.replace("**bank_details**", quote.client.bankdetails);
        content = content.replace("**phone_number**", quote.client.contact1);
        content = content.replace("**email**", quote.client.email);
        content = content.replace(
          "**name**",
          quote.client.firstname + " " + quote.client.surname
        );
      } else {
        content = content.replace("**bank_details**", "-");
        content = content.replace("**phone_number**", "-");
        content = content.replace("**email**", "-");
        content = content.replace("**name**", "-" + " " + "-");
      }

      if (quote.thirdparty) {
        content = content.replace(
          "**third_parties**",
          quote.thirdparty.firstname + " " + quote.thirdparty.surname
        );
      } else {
        content = content.replace("**third_parties**", "-" + " " + "-");
      }
      if (user[0].userdetail.logo) {
        content = content.replace(
          "**logo**",
          "https://www.naartjielegal.com/api/" + user[0].userdetail.logo
        );
      } else {
        content = content.replace(
          "**logo**",
          "https://www.naartjielegal.com/assets/images/logo.png"
        );
      }
      var company_title = "";
      // if (quote.client.type == 'i') {
      //     company_title = quote.client.firstname + " " + quote.client.surname;
      // } else {
      //     company_title = quote.client.companyname
      // }
      if (user[0].userdetail.name_of_firm) {
        company_title = user[0].userdetail.name_of_firm;
      }
      content = content.replace("**company_title**", company_title);
      // content = content.replace(
      //   "**company_title**",
      //   user[0].userdetail.firstname + " " + user[0].userdetail.lastname
      // );
      content = content.replace(
        "**owner**",
        user[0].userdetail.firstname + " " + user[0].userdetail.lastname
      );
      content = content.replace("**owneraddress**", user[0].userdetail.address);
      content = content.replace("**owneremail**", user[0].email);
      content = content.replace("**phone**", user[0].userdetail.contact1);
      content = content.replace("**zip**", "");

      content = content.replace("**notes**", quote.message);
      content = content.replace("**number**", quote.invoice_number);
      content = content.replace("**quote_date**", created_date);
      content = content.replace("**description**", quote.message);
      content = content.replace("**client_reference**", "");

      content = content.replace("**item**", items);
      content = content.replace("**total**", total);
      content = content.replace(
        "**name**",
        quote.client.firstname + " " + quote.client.surname
      );

      let u_quote = await Quote.findByPk(quote.id);
      if (u_quote != null) u_quote.update({ status: 1 });

      if ((quote.client.type = "i")) {
        if (quote.client.email) {
          let email = await Email.create({
            parent_id: quote.id,
            client_id: quote.client_id,
            matter_id: quote.case_id,
            subject: "Quote",
            type: "quote",
            user_id: quote.user_id
          });
          let sentMail = await mail.sendWithAttachment(
            quote.client.email,
            //"satheesh@softalliancetech.com",
            "Quote",
            content,
            quote.attachment,
            next
          );
        }
      } else if ((quote.client.type = "c" && quote.contact)) {
        let clientc = await ClientContact.findByPk(quote.contact);
        if (clientc != null) {
          let email = await Email.create({
            parent_id: quote.id,
            client_id: clientc.id,
            matter_id: quote.case_id,
            subject: "Quote",
            type: "quote",
            user_id: quote.user_id
          });
          let sentMail = await mail.sendWithAttachment(
            clientc.email,
            //"satheesh@softalliancetech.com",
            "Quote",
            content,
            quote.attachment,
            next
          );
        }
      }
      res.status(200).end();
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.put("/sample-pdf", async (req, res, next) => {
  try {
    let url = await helper.generateSamplePDF();
    return res.json(url);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
