const express = require("express");
const router = express.Router();
const SMS = require("../../models/accounts/SMS");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sms = require("../../helpers/sms");
let limit = 10;

const middleware = {
  validations: [
    check("phone").isMobilePhone("any"),
    check("message").isString(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    let {
      phone,
      message,
      user_id,
      client_id,
      case_id,
      recipient_id
    } = req.body;

    let result = await sms.send(phone, message);

    if (result.status >= 200 && result.status < 300) {
      let db_sms = await SMS.create(req.body);
      return res.status(201).json(db_sms);
    } else {
      return res.status(400).json(result);
    }
  }
);

router.get("/paginate/user/:id", middleware.auth, (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  SMS.paginate({
    where: { user_id: req.params.id },
    page: page,
    paginate: limit,
    order: ["updated_at"],
    attributes: [
      "id",
      "recipient_id",
      "phone",
      "message",
      "updated_at",
      "created_at"
    ],
    include: [
      { association: "client", attributes: ["firstname", "surname"] },
      { association: "matter", attributes: ["prefix", "casenumber"] }
      // { association: "recipient_client", attributes: ["firstname", "surname"] },
      // {
      //   association: "recipient_client_contact",
      //   attributes: ["firstname", "surname"]
      // }
    ]
  })
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => next(err));
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
  SMS.findByPk(req.params.id, {
    include: [
      { association: "client" },
      { association: "matter" },
      { association: "recipient_client" },
      {
        association: "recipient_client_contact"
      }
    ]
  })
    .then(sms => res.status(200).json(sms))
    .catch(err => next(err));
});

router.put(
  "/id/:id",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    let db_sms = await SMS.findByPk(req.params.id);

    if (db_sms == null)
      return res.status(404).json({ error: "SMS is not found" });

    let result = await sms.send(db_sms.phone, db_sms.message);

    if (result.status >= 200 && result.status < 300) {
      db_sms.update(req.body);
      return res.status(200).json(db_sms);
    } else {
      return res.status(400).json(result);
    }
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  SMS.findOne({
    where: { id: req.params.id }
  })
    .then(sms => {
      if (sms != null) {
        sms.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "SMS is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.post("/bulk-delete", middleware.auth, async (req, res, next) => {
  try {
    let { ids } = req.body;

    if (ids != null && ids != undefined && Array.isArray(ids)) {
      let result = await SMS.destroy({ where: { id: ids } });
      return res.status(204).json({});
    } else {
      return res.status(400).json("Please collection of ids as array");
    }
  } catch (error) {
    next(error);
  }
});

module.exports = router;
