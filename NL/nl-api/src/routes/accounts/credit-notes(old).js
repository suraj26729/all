const express = require("express");
const router = express.Router();
const CreditNote = require("../../models/accounts/CreditNote");
const CreditNoteItem = require("../../models/accounts/CreditNoteItems");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const helper = require("../../helpers/pdf");
let limit = 10;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const ClientContact = require("../../models/ClientContact");
const common = require("../../helpers/common");

const middleware = {
  validations: [
    check("client_id").isUUID(),
    check("invoice_id").isInt(),
    check("case_id").isUUID(),
    //check("thirdparty_id").isUUID(),
    check("items")
      .isArray()
      .isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let creditnote = await CreditNote.create(req.body, {
        include: [{ association: "items" }]
      });

      creditnote = await CreditNote.findOne({
        where: { id: creditnote.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let url = await helper.generateCreditNotePDF({
        creditnote: creditnote,
        user_id: creditnote.user_id,
        balance: req.body.trust_balance
      });

      await CreditNote.update(
        { attachment: url },
        { where: { id: creditnote.id } }
      );

      var created_date = moment(creditnote.created_at).format("DD-MM-YYYY");
      var items1 = [];
      var total = [];
      for (item of creditnote.items) {
        var itemdate = moment(item.created_at).format("DD-MM-YYYY");
        //items1.push('<tr><td>d</td><td>d</td><td>d</td><td>d</td><td>d</td></tr>');
        items1.push(
          "<tr><td>" +
            itemdate +
            "</td><td>" +
            item.costtype.name +
            "</td><td>" +
            item.description +
            "</td><td>" +
            item.quantity +
            "</td><td>" +
            item.price +
            "</td></tr>"
        );
        total.push(
          '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">R ' +
            item.price +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">R ' +
            Math.abs(item.price - item.total) +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">R ' +
            item.total +
            "</td></tr>"
        );
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/credit_note.html",
        "utf8"
      );
      content = content.replace(
        "**bank_details**",
        creditnote.client.bankdetails
      );
      content = content.replace("**notes**", creditnote.message);
      content = content.replace("**balance**", req.body.trust_balance);
      content = content.replace("**phone_number**", creditnote.client.contact1);
      content = content.replace("**email**", creditnote.client.email);
      content = content.replace("**number**", creditnote.credit_note_number);
      content = content.replace("**quote_date**", created_date);
      content = content.replace("**description**", creditnote.message);
      content = content.replace("**client_reference**", "");
      if (req.body.thirdparty_id) {
        content = content.replace(
          "**third_parties**",
          creditnote.thirdparty.firstname + " " + creditnote.thirdparty.surname
        );
      } else {
        content = content.replace("**third_parties**", "");
      }
      content = content.replace(
        "**name**",
        creditnote.client.firstname + " " + creditnote.client.surname
      );
      content = content.replace("**item**", items1);
      content = content.replace("**total**", total);

      // var firstname = req.body.contact.replace(/ .*/, "");
      // var client_id = req.body.client_id;

      // let clientc = await ClientContact.findOne({
      //   where: { client_id: client_id, firstname: firstname }
      // });

      if ((creditnote.client.type = "i")) {
        let sentMail = await mail.send(
          creditnote.client.email,
          "CREDIT NOTE",
          content,
          next
        );
      } else if ((creditnote.client.type = "c" && creditnote.contact)) {
        let clientc = await ClientContact.findByPk(creditnote.contact);
        if (clientc != null) {
          let sentMail = await mail.send(
            clientc.email,
            "CREDIT NOTE",
            content,
            next
          );
        }
      }

      return res.status(200).json(creditnote);
    } catch (err) {
      next(err);
    }
  }
);

router.get("/paginate/user/:id", middleware.auth, async (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  let userIds = await common.firmUsers(req.params.id);
  CreditNote.paginate({
    where: { user_id: userIds },
    page: page,
    paginate: limit,
    order: ["credit_note_number"],
    attributes: [
      "id",
      "uuid",
      "client_id",
      "credit_note_number",
      "payment_date",
      "total",
      "status",
      "attachment"
    ],
    include: [
      { association: "client", attributes: ["firstname", "surname"] },
      { association: "thirdparty", attributes: ["firstname", "surname"] },
      { association: "matter", attributes: ["prefix", "casenumber"] }
    ]
  })
    .then(creditnotes => {
      res.status(200).json(creditnotes);
    })
    .catch(err => next(err));
});

router.get("/list/user/:id", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.id);
  CreditNote.findAll({
    where: { user_id: userIds },
    attributes: ["id", "credit_note_number"]
  })
    .then(creditnotes => {
      res.status(200).json(creditnotes);
    })
    .catch(err => next(err));
});

router.get("/id/:uuid", middleware.auth, (req, res, next) => {
  CreditNote.findOne({
    where: { uuid: req.params.uuid },
    include: [
      { association: "items" },
      { association: "client" },
      { association: "invoice" },
      { association: "matter" },
      { association: "thirdparty" }
    ]
  })
    .then(creditnote => res.status(200).json(creditnote))
    .catch(err => next(err));
});

router.put(
  "/id/:uuid",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }
      let creditnote = await CreditNote.findOne({
        where: { uuid: req.params.uuid },
        include: [{ association: "items" }]
      });
      if (creditnote == null)
        return res.status(404).json({ error: "CreditNote is not found" });
      let result = await CreditNoteItem.destroy({
        where: { credit_note_id: creditnote.id },
        force: true
      });
      if (result == 0)
        return res
          .status(400)
          .json("Error while updating the items of credit note");
      creditnote.update(req.body);
      for (let item of req.body.items) {
        await CreditNoteItem.upsert(item, { where: { id: item.id } });
      }
      creditnote = await CreditNote.findOne({
        where: { id: creditnote.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          // { association: "quote" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let url = await helper.generateCreditNotePDF({
        creditnote: creditnote,
        user_id: creditnote.user_id,
        balance: req.body.trust_balance
      });

      await CreditNote.update(
        { attachment: url },
        { where: { id: creditnote.id } }
      );

      return res.status(200).json(creditnote);
    } catch (err) {
      next(err);
    }
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  CreditNote.findOne({
    where: { id: req.params.id }
  })
    .then(creditnote => {
      if (creditnote != null) {
        creditnote.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "CreditNote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch("/id/:id/restore", middleware.auth, (req, res, next) => {
  CreditNote.findOne({
    where: { id: req.params.id },
    paranoid: false
  })
    .then(creditnote => {
      if (creditnote != null) {
        creditnote.setDataValue("deleted_at", null);
        creditnote.update({ deleted_at: null });
        res.status(200).json(creditnote);
      } else {
        res.status(404).json({
          error: "CreditNote is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
