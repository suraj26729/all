const router = require("express").Router();
const Transaction = require("../../models/accounts/Transaction");
const common = require("../../helpers/common");
const checkToken = require("../../middlewares/checkToken");

router.get("/paginate/user/:id", checkToken, async (req, res, next) => {
  try {
    let page = req.query.page || 1;
    let limit = parseInt(req.query.limit) || 20;
    let userIds = await common.firmUsers(req.params.id);
    let transactions = await Transaction.paginate({
      where: { user_id: userIds, type: ["deposit", "credit"] },
      include: [
        {
          association: "matter",
          attributes: ["id", "prefix", "casenumber", "status"]
        },
        {
          association: "trust",
          attributes: ["id", "amount", "description"]
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"]
        }
      ],
      order: [["created_at", "ASC"]],
      page: page,
      paginate: limit
    });
    return res.status(200).json(transactions);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
