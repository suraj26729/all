const express = require("express");
const router = express.Router();
const Tax = require("../../models/accounts/Tax");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");

let limit = 10;

const middleware = {
  validations: [
    check("name").isString(),
    check("percent").isFloat(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.get("/user/:userID", middleware.auth, async (req, res) => {
  let userIds = await common.firmUsers(req.params.userID);
  Tax.findAndCountAll({
    where: { user_id: userIds, isactive: 1 },
    order: ["name"]
  })
    .then(taxes => {
      res.status(200).json(taxes);
    })
    .catch(err => next(err));
});

router.get(
  "/paginate/user/:userID",
  middleware.auth,
  async (req, res, next) => {
    let page = req.query.page || 1;
    limit = parseInt(req.query.limit) || limit;
    let userIds = await common.firmUsers(req.params.userID);
    Tax.paginate({
      where: { user_id: userIds },
      page: page,
      paginate: limit,
      order: ["name"]
    })
      .then(taxes => {
        res.status(200).json(taxes);
      })
      .catch(err => next(err));
  }
);

router.post(
  "/",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    const NewTax = {
      name: req.body.name,
      percent: req.body.percent,
      user_id: req.body.user_id
    };

    Tax.findOne({
      where: NewTax
    }).then(tax => {
      if (tax == null) {
        Tax.create(NewTax)
          .then(newtax => {
            res.status(201).json(newtax);
          })
          .catch(err => next(err));
      } else {
        res.status(400).json({
          error: "Tax is already created with same name"
        });
      }
    });
  }
);

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Tax.findOne({ where: { id: req.params.id } })
    .then(tax => {
      if (tax != null) {
        res.status(200).json(tax);
      } else {
        res.status(404).json({
          error: "Tax is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.put(
  "/id/:id",
  [middleware.auth, [check("name").isString(), check("percent").isFloat()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Tax.findOne({
      where: { id: req.params.id }
    })
      .then(tax => {
        if (tax != null) {
          Tax.findOne({
            where: { name: req.body.name, $not: { id: req.params.id } }
          })
            .then(ftax => {
              if (ftax == null) {
                tax.update({ name: req.body.name, percent: req.body.percent });
                res.status(200).json(tax);
              } else {
                res.status(400).json({
                  error: "Tax is already created with same name"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "Tax is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch(
  "/id/:id/status",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    Tax.findOne({
      where: { id: req.params.id }
    })
      .then(tax => {
        if (tax != null) {
          tax.update({ isactive: tax.isactive ? 0 : 1 });
          res.status(200).json(tax);
        } else {
          res.status(404).json({
            error: "Tax is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  Tax.findOne({
    where: { id: req.params.id }
  })
    .then(tax => {
      if (tax != null) {
        tax.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Tax is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch("/id/:id/restore", middleware.auth, (req, res, next) => {
  Tax.findOne({
    where: { id: req.params.id },
    paranoid: false
  })
    .then(tax => {
      if (tax != null) {
        tax.setDataValue("deleted_at", null);
        tax.update({ deleted_at: null });
        res.status(200).json(tax);
      } else {
        res.status(404).json({
          error: "Tax is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
