const router = require("express").Router();
const Invoice = require("../../models/accounts/Invoice");
const { TimesheetInvoice } = require("../../models/accounts/TimesheetInvoice");
const checkToken = require("../../middlewares/checkToken");
const common = require("../../helpers/common");

router.get("/paginate/user/:id", checkToken, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.id);
    let invoices = [];
    invoices = await Invoice.findAll({
      where: { user_id: userIds, status: 2 },
      order: [["invoice_number", "DESC"]],
      attributes: [
        "id",
        "uuid",
        "client_id",
        "invoice_number",
        "payment_date",
        "total",
        "status",
        "attachment"
      ],
      include: [
        {
          association: "client"
          //attributes: ["firstname", "surname", "companyname", "type"]
        },
        { association: "casecontact" },
        { association: "thirdparty", attributes: ["firstname", "surname"] },
        { association: "matter", attributes: ["prefix", "casenumber"] }
      ]
    });
    let timesheet_invoices = [];
    timesheet_invoices = await TimesheetInvoice.findAll({
      where: { user_id: userIds, status: 2 },
      order: [["invoice_number", "DESC"]],
      attributes: [
        "id",
        "uuid",
        "client_id",
        "invoice_number",
        "payment_date",
        "total",
        "status",
        "attachment"
      ],
      include: [
        {
          association: "client"
          //attributes: ["firstname", "surname", "companyname", "type"]
        },
        { association: "casecontact" },
        { association: "thirdparty", attributes: ["firstname", "surname"] },
        { association: "matter", attributes: ["prefix", "casenumber"] }
      ]
    });
    return res.status(200).json({
      invoices: invoices,
      timesheet_invoices: timesheet_invoices
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
