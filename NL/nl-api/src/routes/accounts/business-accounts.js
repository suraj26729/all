const router = require("express").Router();
const Invoice = require("../../models/accounts/Invoice");
const Transaction = require("../../models/accounts/Transaction");
const Expense = require("../../models/accounts/Expense");
const common = require("../../helpers/common");
const checkToken = require("../../middlewares/checkToken");
const moment = require("moment");

const date_cond = (from_date, to_date) => {
  if (from_date != "null" && to_date != "null") {
    from_date = moment(from_date, "YYYY-MM-DD")
      .startOf("day")
      .format("YYYY-MM-DD HH:mm:ss");
    to_date = moment(to_date, "YYYY-MM-DD")
      .endOf("day")
      .format("YYYY-MM-DD HH:mm:ss");
    return { created_at: { $between: [from_date, to_date] } };
  }
  return false;
};

router.get("/all/user/:id", checkToken, async (req, res, next) => {
  try {
    let { type, from_date, to_date } = req.query;
    let userIds = await common.firmUsers(req.params.id);

    let invoices = [];
    if (type == "all" || (type != "all" && type == "assets")) {
      let conditions = [];
      let _date_cond = date_cond(from_date, to_date);
      if (_date_cond) conditions.push(_date_cond);
      conditions.push({ user_id: userIds });
      conditions.push({ status: 2 });
      conditions = conditions.reduce((a, b) => Object.assign(a, b), {});

      invoices = await Invoice.findAll({
        where: conditions,
        attributes: [
          "id",
          "uuid",
          "client_id",
          "invoice_number",
          "payment_date",
          "total",
          "status",
          "attachment",
          "created_at"
        ],
        include: [
          {
            association: "client"
            //attributes: ["firstname", "surname", "companyname", "type"]
          },
          { association: "matter", attributes: ["prefix", "casenumber"] }
        ],
        order: [["created_at", "ASC"]]
      });
    }
    let transactions = [];
    if (type == "all" || (type != "all" && type == "liabilities")) {
      let conditions = [];
      let _date_cond = date_cond(from_date, to_date);
      if (_date_cond) conditions.push(_date_cond);
      conditions.push({ user_id: userIds });
      conditions.push({ type: ["deposit", "credit"] });
      conditions = conditions.reduce((a, b) => Object.assign(a, b), {});

      transactions = await Transaction.findAll({
        where: conditions,
        include: [
          {
            association: "matter",
            attributes: ["id", "prefix", "casenumber", "status"]
          },
          {
            association: "trust",
            attributes: ["id", "amount", "description"]
          },
          {
            association: "allocation",
            attributes: ["id", "amount", "description"]
          }
        ],
        order: [["created_at", "ASC"]]
      });
    }

    let expenses = [];
    if (type == "all" || (type != "all" && type == "expenses")) {
      let conditions = [];
      let _date_cond = date_cond(from_date, to_date);
      if (_date_cond) conditions.push(_date_cond);
      conditions.push({ user_id: userIds });
      conditions = conditions.reduce((a, b) => Object.assign(a, b), {});

      expenses = await Expense.findAll({
        where: conditions,
        order: [["created_at", "DESC"]]
      });
    }

    let _invoices = [];
    if (invoices.length) {
      for (let item of invoices) {
        _invoices.push({
          title: `INV#${item.invoice_number} - ${item.matter.prefix}${item.matter.casenumber}`,
          type: "debit",
          amount: item.total.toFixed(2),
          date: item.created_at
        });
      }
    }

    let _transactions = [];
    if (transactions.length) {
      for (let item of transactions) {
        let amount = 0.0;
        let type = "credit";
        if (item.trust_id != null) {
          amount = item.trust.amount.toFixed(2);
          type = "credit";
        } else {
          type = "debit";
          amount = item.allocation.amount.toFixed(2);
        }
        _transactions.push({
          title: `${item.type} - ${item.matter.prefix}${item.matter.casenumber}`,
          type: type,
          amount: amount,
          date: item.created_at
        });
      }
    }

    let _expenses = [];
    if (expenses.length) {
      for (let item of expenses) {
        _expenses.push({
          title: `${item.name}`,
          type: item.type,
          amount: item.amount.toFixed(2),
          date: item.created_at
        });
      }
    }

    return res.status(200).json({
      assets: _invoices,
      liabilities: _transactions,
      expenses: _expenses
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
