const express = require("express");
const router = express.Router();
const Quote = require("../../models/accounts/Quote");
const Invoice = require("../../models/accounts/Invoice");
const InvoiceItem = require("../../models/accounts/InvoiceItems");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const helper = require("../../helpers/pdf");
let limit = 10;

const middleware = {
  validations: [
    check("client_id").isUUID(),
    check("case_id").isUUID(),
    check("thirdparty_id").isUUID(),
    check("items")
      .isArray()
      .isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let quote = await Quote.findByPk(req.body.quote_id);
      if (quote != null) quote.update({ status: 2 });

      let invoice = await Invoice.create(req.body, {
        include: [{ association: "items" }]
      });
      invoice = await Invoice.findOne({
        where: { id: invoice.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          // { association: "quote" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let url = await helper.generateInvoicePDF({
        invoice: invoice,
        user_id: invoice.user_id,
        balance: req.body.trust_balance
      });

      await Invoice.update({ attachment: url }, { where: { id: invoice.id } });

      return res.status(200).json(invoice);
    } catch (err) {
      next(err);
    }
  }
);

router.get("/paginate/user/:id", middleware.auth, (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  Invoice.paginate({
    where: { user_id: req.params.id },
    page: page,
    paginate: limit,
    order: ["invoice_number"],
    attributes: [
      "id",
      "uuid",
      "invoice_number",
      "payment_date",
      "total",
      "status",
      "attachment"
    ],
    include: [
      { association: "client", attributes: ["firstname", "surname"] },
      { association: "thirdparty", attributes: ["firstname", "surname"] },
      { association: "matter", attributes: ["prefix", "casenumber"] }
    ]
  })
    .then(invoices => {
      res.status(200).json(invoices);
    })
    .catch(err => next(err));
});

router.get("/list/user/:id", middleware.auth, (req, res, next) => {
  Invoice.findAll({
    where: { user_id: req.params.id },
    attributes: ["id", "invoice_number"]
  })
    .then(invoices => {
      res.status(200).json(invoices);
    })
    .catch(err => next(err));
});

router.get("/id/:uuid", middleware.auth, (req, res, next) => {
  Invoice.findOne({
    where: { uuid: req.params.uuid },
    include: [
      { association: "items" },
      { association: "client" },
      // { association: "quote" },
      { association: "matter" },
      { association: "thirdparty" }
    ]
  })
    .then(invoice => res.status(200).json(invoice))
    .catch(err => next(err));
});

router.put(
  "/id/:uuid",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }
      let invoice = await Invoice.findOne({
        where: { uuid: req.params.uuid },
        include: [{ association: "items" }]
      });
      if (invoice == null)
        return res.status(404).json({ error: "Invoice is not found" });
      let result = await InvoiceItem.destroy({
        where: { invoice_id: invoice.id },
        force: true
      });
      if (result == 0)
        return res
          .status(400)
          .json("Error while updating the items of invoice");
      invoice.update(req.body);
      for (let item of req.body.items) {
        await InvoiceItem.upsert(item, { where: { id: item.id } });
      }
      invoice = await Invoice.findOne({
        where: { id: invoice.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          // { association: "quote" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let url = await helper.generateInvoicePDF({
        invoice: invoice,
        user_id: invoice.user_id,
        balance: req.body.trust_balance
      });

      await Invoice.update({ attachment: url }, { where: { id: invoice.id } });

      return res.status(200).json(invoice);
    } catch (err) {
      next(err);
    }
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  Invoice.findOne({
    where: { id: req.params.id }
  })
    .then(invoice => {
      if (invoice != null) {
        invoice.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Invoice is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch("/id/:id/restore", middleware.auth, (req, res, next) => {
  Invoice.findOne({
    where: { id: req.params.id },
    paranoid: false
  })
    .then(invoice => {
      if (invoice != null) {
        invoice.setDataValue("deleted_at", null);
        invoice.update({ deleted_at: null });
        res.status(200).json(invoice);
      } else {
        res.status(404).json({
          error: "Invoice is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.get("/list/client/:id", middleware.auth, (req, res, next) => {
  Invoice.findAll({
    where: { client_id: req.params.id },
    attributes: ["id", "invoice_number"]
  })
    .then(invoices => {
      res.status(200).json(invoices);
    })
    .catch(next);
});

router.get("/list/matter/:id", middleware.auth, (req, res, next) => {
  Invoice.findAll({
    where: { case_id: req.params.id, status: [0, 1] },
    attributes: ["id", "invoice_number", "total"]
  })
    .then(invoices => {
      res.status(200).json(invoices);
    })
    .catch(next);
});

router.get("/view/:id", middleware.auth, async (req, res, next) => {
  try {
    let invoice = await Invoice.findOne({
      where: { id: req.params.id },
      include: [
        { association: "items", include: [{ association: "costtype" }] },
        { association: "client" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });
    if (invoice == null) return res.json("Invoice not found");

    let result = await helper.generateInvoicePDF({
      invoice: invoice,
      user_id: invoice.user_id
    });
    res.json(result);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
