const express = require("express");
const router = express.Router();
const sequelize = require("sequelize");
const Transaction = require("../../models/accounts/Transaction");
const MoneyTrust = require("../../models/accounts/MoneyTrust");
const MoneyAllocation = require("../../models/accounts/MoneyAllocation");
const Invoice = require("../../models/accounts/Invoice");
const checkToken = require("../../middlewares/checkToken");
const Matter = require("../../models/CaseDetails");
const { check, validationResult } = require("express-validator/check");
const ClientContact = require("../../models/ClientContact");
const CaseDetails = require("../../models/CaseDetails");
const ClientDetails = require("../../models/Client");
let limit = 10;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");

const middleware = {
    trust_validations: [
        check("client_id").isUUID(),
        check("case_id").isUUID(),
        check("amount").isFloat(),
        check("user_id").isInt()
    ],
    allocation_validations: [
        check("client_id").isUUID(),
        check("case_id").isUUID(),
        check("invoice_id").isInt(),
        check("amount").isFloat(),
        check("user_id").isInt()
    ],
    auth: checkToken
};

router.post(
    "/deposit", [middleware.auth, middleware.trust_validations],
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ error: errors.array() });
        }

        MoneyTrust.create({
                client_id: req.body.client_id,
                case_id: req.body.case_id,
                amount: req.body.amount,
                description: req.body.description,
                status: req.body.status,
                user_id: req.body.user_id
            })
            .then(trust => {
                Transaction.create({
                        trust_id: trust.id,
                        case_id: req.body.case_id,
                        type: "deposit",
                        status: 1,
                        user_id: req.body.user_id
                    })
                    .then(transaction => {
                        res.status(200).json({ trust, transaction });
                    })
                    .catch(err => next(err));
            })
            .catch(err => next(err));
    }
);

router.post(
    "/allocate", [middleware.auth, middleware.allocation_validations],
    async(req, res, next) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ error: errors.array() });
            }
            let invoice = await Invoice.findByPk(req.body.invoice_id);
            if (invoice == null) return res.status(404).json("Invoice not found");
            let total = invoice.total;
            let allocationTotal = 0.0;
            let allocations = await MoneyAllocation.findOne({
                where: { invoice_id: req.body.invoice_id },
                attributes: [
                    [sequelize.fn("sum", sequelize.col("amount")), "amount"]
                ],
                group: ["invoice_id"]
            });
            if (allocations != null)
                allocationTotal = parseFloat(allocations.amount).toFixed(2);

            let NewTotalAllocation =
                parseFloat(allocationTotal) + parseFloat(req.body.amount);

            if (NewTotalAllocation > total)
                return res.status(400).json("Amount exceeded with Invoice total");
            let NewAllocation = await MoneyAllocation.create({
                client_id: req.body.client_id,
                case_id: req.body.case_id,
                invoice_id: req.body.invoice_id,
                amount: req.body.amount,
                description: req.body.description,
                status: req.body.status,
                user_id: req.body.user_id
            });
            if (NewAllocation) {
                let transaction = await Transaction.create({
                    allocate_id: NewAllocation.id,
                    case_id: req.body.case_id,
                    type: "credit",
                    status: 1,
                    user_id: req.body.user_id
                });
                if (NewTotalAllocation == total) {
                    invoice.update({ status: 2 });
                }
                res.status(200).json({ NewAllocation, transaction });
            }
        } catch (err) {
            next(err);
        }
    }
);

router.get(
    "/trust-balance/matter/:id",
    middleware.auth,
    async(req, res, next) => {
        try {
            let deposits = await Transaction.findAll({
                include: [{
                    association: "trust",
                    where: { case_id: req.params.id },
                    attributes: ["amount"]
                }],
                where: { type: "deposit" },
                attributes: []
            });
            deposits = deposits.map(row => row.trust.amount);
            let credits = await Transaction.findAll({
                include: [{
                    association: "allocation",
                    where: { case_id: req.params.id },
                    attributes: ["amount"]
                }],
                where: { type: "credit" },
                attributes: []
            });
            credits = credits.map(row => row.allocation.amount);
            let deposit_sum = 0;
            let allocation_sum = 0;
            if (deposits.length) {
                deposit_sum = deposits.reduce((a, b) => {
                    return a + b;
                }, 0);
            }
            if (credits.length) {
                allocation_sum = credits.reduce((a, b) => {
                    return a + b;
                }, 0);
            }
            res.status(200).json({
                total_deposit: deposit_sum,
                total_alloc: allocation_sum,
                balance: deposit_sum - allocation_sum
            });
        } catch (err) {
            next(err);
        }
    }
);

router.get(
    "/allocated-money/matter/:id",
    middleware.auth,
    async(req, res, next) => {
        let credits = await Transaction.findAll({
            include: [{
                association: "allocation",
                where: { case_id: req.params.id },
                attributes: ["amount"]
            }],
            where: { type: "credit" },
            attributes: []
        });
        credits = credits.map(row => row.allocation.amount);
        let allocation_sum = 0;
        if (credits.length) {
            allocation_sum = credits.reduce((a, b) => {
                return a + b;
            }, 0);
        }
        res.status(200).json(allocation_sum);
    }
);

router.get(
    "/money-trusts/paginate/user/:id",
    middleware.auth,
    async(req, res, next) => {
        MoneyTrust.count({
                where: { user_id: req.params.id },
                group: ["client_id"],
                attributes: [sequelize.fn("sum", sequelize.col("amount"))]
            })
            .then(countData => {
                limit = parseInt(req.query.limit) || limit;
                let page = req.query.page || 1;
                let pages = Math.ceil(countData.length / limit);
                offset = limit * (page - 1);
                MoneyTrust.findAll({
                        where: { user_id: req.params.id },
                        group: ["client_id"],
                        attributes: [
                            "client_id", [sequelize.fn("sum", sequelize.col("amount")), "amount"],
                            "created_at"
                        ],
                        include: [{
                            association: "client",
                            attributes: ["id", "firstname", "surname"]
                        }],
                        limit: limit,
                        offset: offset
                    })
                    .then(results => {
                        res.status(200).json({
                            pages: pages,
                            total: countData.length,
                            docs: results
                        });
                    })
                    .catch(err => next(err));
            })
            .catch(next);
    }
);

router.delete("/deposit/client/:id", middleware.auth, (req, res, next) => {
    MoneyTrust.findAll({ where: { client_id: req.params.id } })
        .then(deposit => {
            if (deposit == null) return res.status(404).json("Deposit is not found");
            deposit.destroy();
            res.status(204).json({});
        })
        .catch(next);
});

router.get(
    "/money-allocations/paginate/user/:id",
    middleware.auth,
    (req, res, next) => {
        let page = req.query.page || 1;
        limit = parseInt(req.query.limit) || limit;
        MoneyAllocation.paginate({
                where: { user_id: req.params.userID },
                page: page,
                paginate: limit,
                include: [{ association: "client" }, { association: "matter" }]
            })
            .then(results => {
                res.status(200).json(results);
            })
            .catch(err => next(err));
    }
);

router.get("/statement/matter/:id", middleware.auth, (req, res, next) => {
    Transaction.findAll({
            attributes: ["id", "type", "status", "created_at"],
            include: [
                { association: "matter", attributes: ["id", "prefix", "casenumber"] },
                {
                    association: "trust",
                    attributes: ["id", "amount", "description"]
                },
                {
                    association: "allocation",
                    attributes: ["id", "amount", "description"]
                }
            ],
            where: { case_id: req.params.id },
            order: [
                ["created_at", "ASC"]
            ]
        })
        .then(transactions => res.status(200).json(transactions))
        .catch(err => next(err));
});

router.get("/deposits/client/:id", middleware.auth, (req, res, next) => {
    MoneyTrust.findAll({
            where: { client_id: req.params.id },
            attributes: ["id", "amount", "description"],
            include: [
                { association: "matter", attributes: ["id", "prefix", "casenumber"] }
            ]
        })
        .then(deposits => res.status(200).json(deposits))
        .catch(next);
});

router.get("/allocation/invoice/id/:id", middleware.auth, (req, res, next) => {
    MoneyAllocation.findOne({
            where: { invoice_id: req.params.id },
            attributes: [
                [sequelize.fn("sum", sequelize.col("amount")), "amount"]
            ],
            group: ["invoice_id"]
        })
        .then(invoice => res.status(200).json(invoice))
        .catch(next);
});

router.get(
    "/allocation-label/matter/:id",
    middleware.auth,
    async(req, res, next) => {
        let matter = await Matter.findByPk(req.params.id, {
            attributes: ["id", "prefix", "casenumber"],
            include: [{
                association: "clientdetail",
                attributes: ["id", "firstname", "surname"]
            }]
        });
        if (matter == null) return res.status(404).json("Matter is not found");

        let deposits = await Transaction.findAll({
            include: [{
                association: "trust",
                where: { case_id: req.params.id },
                attributes: ["amount"]
            }],
            where: { type: "deposit" },
            attributes: []
        });
        deposits = deposits.map(row => row.trust.amount);
        let credits = await Transaction.findAll({
            include: [{
                association: "allocation",
                where: { case_id: req.params.id },
                attributes: ["amount"]
            }],
            where: { type: "credit" },
            attributes: []
        });
        credits = credits.map(row => row.allocation.amount);
        let deposit_sum = 0;
        let allocation_sum = 0;
        if (deposits.length) {
            deposit_sum = deposits.reduce((a, b) => {
                return a + b;
            }, 0);
        }
        if (credits.length) {
            allocation_sum = credits.reduce((a, b) => {
                return a + b;
            }, 0);
        }

        res.status(200).json({
            client: matter.clientdetail.companyname ||
                matter.clientdetail.firstname + " " + matter.clientdetail.surname,
            matter: matter.prefix + matter.casenumber,
            trust: deposit_sum,
            avail: deposit_sum - allocation_sum
        });
    }
);



router.post("/sentBulkTrust", async(req, res, next) => {
    try {
        var t = [];
        for (let tid of req.body) {
            let transaction = await MoneyTrust.findAll({
                where: { client_id: tid },
            });
            t.push(transaction);
        }

        for (let trust of t[0]) {
            //console.log(t.length);

            let clientcontact = await ClientContact.findOne({
                where: { client_id: trust.client_id },
            });

            let casedetails = await CaseDetails.findOne({
                where: { client_id: trust.client_id },
            });

            let clientdetails = await ClientDetails.findOne({
                where: { id: trust.client_id },
            });

            var created_date = moment(trust.created_at).format("DD-MM-YYYY");
            let content = fs.readFileSync("./src/email-templates/crm/money_transaction.html", "utf8");
            content = content.replace("**name**", clientdetails.firstname);
            content = content.replace("**case**", casedetails.prefix + casedetails.casenumber);
            content = content.replace("**amount**", trust.amount);
            content = content.replace("**description**", trust.description);
            content = content.replace("**date**", created_date);
            if (clientdetails.type = "i") {
                let sentMail = await mail.send(
                    clientdetails.email,
                    "Transaction Details",
                    content,
                    next
                );
            } else if (clientdetails.type = "c") {
                let sentMail = await mail.send(
                    clientcontact.email,
                    "Transaction Details",
                    content,
                    next
                );
            }

        }

        res.status(200).json({ code: 200, message: "Mail sent successfully", t: t });

    } catch (error) {
        next(error);
    }
});


module.exports = router;