const express = require("express");
const router = express.Router();
const BankDetail = require("../../models/accounts/BankDetails");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");
const User = require("../../models/User");

const middleware = {
  validations: [check("message").isString(), check("user_id").isInt()],
  auth: checkToken
};

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let userIds = await common.firmUsers(req.body.user_id);
      let rootUser = await User.findOne({
        where: { id: userIds, role_id: 2, parent_user_id: null }
      });

      req.body.user_id = rootUser.id;
      let message = await BankDetail.findOne({
        where: { user_id: rootUser.id }
      });
      if (message == null) {
        let message = await BankDetail.create(req.body);
        return res.status(201).json(message);
      } else {
        let message = await BankDetail.update(req.body, {
          where: { user_id: rootUser.id }
        });
        return res.status(200).json(message);
      }
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get("/user/:id", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.id);
  let rootUser = await User.findOne({
    where: { id: userIds, role_id: 2, parent_user_id: null }
  });
  BankDetail.findOne({ where: { user_id: rootUser.id } })
    .then(message => res.status(200).json(message))
    .catch(err => next(err));
});

module.exports = router;
