const express = require("express");
const router = express.Router();
const sequelize = require("sequelize");
const Transaction = require("../../models/accounts/Transaction");
const MoneyTrust = require("../../models/accounts/MoneyTrust");
const MoneyAllocation = require("../../models/accounts/MoneyAllocation");
const Invoice = require("../../models/accounts/Invoice");
const { TimesheetInvoice } = require("../../models/accounts/TimesheetInvoice");
const CreditNote = require("../../models/accounts/CreditNote");
const checkToken = require("../../middlewares/checkToken");
const Matter = require("../../models/CaseDetails");
const { check, validationResult } = require("express-validator/check");
const ClientContact = require("../../models/ClientContact");
const CaseDetails = require("../../models/CaseDetails");
const ClientDetails = require("../../models/Client");
let limit = 10;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const common = require("../../helpers/common");
const Email = require("../../models/Email");

const middleware = {
  trust_validations: [
    check("client_id").isUUID(),
    check("case_id").isUUID(),
    check("amount").isFloat(),
    check("user_id").isInt()
  ],
  allocation_validations: [
    check("client_id").isUUID(),
    check("case_id").isUUID(),
    check("invoice_id").isInt(),
    check("amount").isFloat(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/deposit",
  [middleware.auth, middleware.trust_validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    MoneyTrust.create({
      client_id: req.body.client_id,
      case_id: req.body.case_id,
      amount: req.body.amount,
      description: req.body.description,
      status: req.body.status,
      user_id: req.body.user_id
    })
      .then(trust => {
        Transaction.create({
          trust_id: trust.id,
          case_id: req.body.case_id,
          type: "deposit",
          status: 1,
          user_id: req.body.user_id
        })
          .then(transaction => {
            res.status(200).json({ trust, transaction });
          })
          .catch(err => next(err));
      })
      .catch(err => next(err));
  }
);

router.post(
  "/allocate",
  [middleware.auth, middleware.allocation_validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let invoice = {};
      if (req.body.invoice_type == "t")
        invoice = await TimesheetInvoice.findByPk(req.body.invoice_id);
      else invoice = await Invoice.findByPk(req.body.invoice_id);
      if (invoice == null) return res.status(404).json("Invoice not found");
      let total = invoice.total;
      let allocationTotal = 0.0;
      let allocations = await MoneyAllocation.findOne({
        where: {
          invoice_id: req.body.invoice_id,
          invoice_type: req.body.invoice_type
        },
        attributes: [[sequelize.fn("sum", sequelize.col("amount")), "amount"]],
        group: ["invoice_id"]
      });
      if (allocations != null)
        allocationTotal = parseFloat(allocations.amount).toFixed(2);

      let NewTotalAllocation =
        parseFloat(allocationTotal) + parseFloat(req.body.amount);

      if (NewTotalAllocation > total)
        return res.status(400).json("Amount exceeded with Invoice total");

      let NewAllocation = await MoneyAllocation.create({
        client_id: req.body.client_id,
        case_id: req.body.case_id,
        invoice_id: req.body.invoice_id,
        invoice_type: req.body.invoice_type,
        amount: req.body.amount,
        description: req.body.description,
        status: req.body.status,
        user_id: req.body.user_id
      });
      if (NewAllocation != null) {
        let transaction = await Transaction.create({
          allocate_id: NewAllocation.id,
          case_id: req.body.case_id,
          type: "credit",
          status: 1,
          user_id: req.body.user_id
        });
        if (NewTotalAllocation == total) {
          invoice.update({ status: 2 });
        }
        res.status(200).json({ NewAllocation, transaction });
      }
    } catch (err) {
      next(err);
    }
  }
);

const trustBalance = async case_id => {
  let deposits = await Transaction.findAll({
    include: [
      {
        association: "trust",
        where: { case_id: case_id },
        attributes: ["amount"]
      }
    ],
    where: { type: "deposit" },
    attributes: []
  });
  deposits = deposits.map(row => row.trust.amount);
  let credits = await Transaction.findAll({
    include: [
      {
        association: "allocation",
        where: { case_id: case_id },
        attributes: ["amount"]
      }
    ],
    where: { type: "credit" },
    attributes: []
  });
  credits = credits.map(row => row.allocation.amount);
  let deposit_sum = 0;
  let allocation_sum = 0;
  if (deposits.length) {
    deposit_sum = deposits.reduce((a, b) => {
      return a + b;
    }, 0);
  }
  if (credits.length) {
    allocation_sum = credits.reduce((a, b) => {
      return a + b;
    }, 0);
  }

  return {
    deposit_sum,
    allocation_sum
  };
};

router.get(
  "/trust-balance/matter/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let { deposit_sum, allocation_sum } = await trustBalance(req.params.id);
      res.status(200).json({
        total_deposit: deposit_sum,
        total_alloc: allocation_sum,
        balance: deposit_sum - allocation_sum
      });
    } catch (err) {
      next(err);
    }
  }
);

router.get(
  "/trust-balance/client/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      var case_id = [];
      let cases = await CaseDetails.findAll({
        where: { client_id: req.params.id }
      });

      for (c of cases) {
        case_id.push(c.id);
      }

      let { deposit_sum, allocation_sum } = await trustBalance(case_id);
      res.status(200).json({
        total_deposit: deposit_sum,
        total_alloc: allocation_sum,
        balance: deposit_sum - allocation_sum
      });

      //res.status(200).json({ case_id: case_id, cases: cases });
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/allocated-money/matter/:id",
  middleware.auth,
  async (req, res, next) => {
    let credits = await Transaction.findAll({
      include: [
        {
          association: "allocation",
          where: { case_id: req.params.id },
          attributes: ["amount"]
        }
      ],
      where: { type: "credit" },
      attributes: []
    });
    credits = credits.map(row => row.allocation.amount);
    let allocation_sum = 0;
    if (credits.length) {
      allocation_sum = credits.reduce((a, b) => {
        return a + b;
      }, 0);
    }
    res.status(200).json(allocation_sum);
  }
);

router.get(
  "/money-trusts/paginate/user/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let userIds = await common.firmUsers(req.params.id);
      let countData = await MoneyTrust.count({
        where: { user_id: userIds },
        group: ["client_id"],
        attributes: [sequelize.fn("sum", sequelize.col("amount"))]
      });
      limit = parseInt(req.query.limit) || limit;
      let page = req.query.page || 1;
      let pages = Math.ceil(countData.length / limit);
      offset = limit * (page - 1);
      const { from, to } = req.query;

      let cond = {};
      if (from && to) {
        cond = {
          user_id: userIds,
          created_at: { $between: [from, to] }
        };
      } else {
        cond = { user_id: userIds };
      }

      let current_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
      let current_month = moment(current_date)
        .startOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let current_end_date = moment(current_month)
        .endOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let previous_two_date = moment(current_month)
        .subtract(2, "months")
        .startOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let previous_two_end_date = moment(previous_two_date)
        .endOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let previous_date = moment(current_month)
        .subtract(1, "months")
        .startOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let previous_end_date = moment(previous_date)
        .endOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      var date = {
        current_date: current_month,
        current_end_date: current_end_date,
        previous_two_date: previous_two_date,
        previous_two_end_date: previous_two_end_date,
        previous_date: previous_date,
        previous_end_date: previous_end_date
      };

      let results = await MoneyTrust.findAll({
        where: cond,
        group: ["client_id"],
        attributes: [
          "client_id",
          [sequelize.fn("sum", sequelize.col("amount")), "amount"]
          //"created_at"
        ],
        include: [
          {
            association: "client"
            //attributes: ["id", "firstname", "surname"]
          }
        ],
        limit: limit,
        offset: offset
      });

      var newresult = [];
      for (result of results) {
        let currentAmount = await MoneyTrust.findAll({
          where: {
            client_id: result.client_id,
            created_at: {
              $between: [current_month, current_end_date]
            }
          },
          group: ["client_id"],
          attributes: [
            "client_id",
            [sequelize.fn("sum", sequelize.col("amount")), "amount"]
            // "created_at"
          ]
        });

        let previousAmount = await MoneyTrust.findAll({
          where: {
            client_id: result.client_id,
            created_at: {
              $between: [previous_date, previous_end_date]
            }
          },
          group: ["client_id"],
          attributes: [
            "client_id",
            [sequelize.fn("sum", sequelize.col("amount")), "amount"]
            // "created_at"
          ]
        });

        let previous2Amount = await MoneyTrust.findAll({
          where: {
            client_id: result.client_id,
            created_at: {
              $between: [previous_two_date, previous_two_end_date]
            }
          },
          group: ["client_id"],
          attributes: [
            "client_id",
            [sequelize.fn("sum", sequelize.col("amount")), "amount"]
            // "created_at"
          ]
        });

        let allocations = await MoneyAllocation.findAll({
          where: {
            client_id: result.client_id
          },
          attributes: ["client_id", "amount"]
        });

        let credit_amount = 0;
        for (let alloc of allocations) credit_amount += alloc.amount;

        let trust_balance = (result.amount || 0) - (credit_amount || 0);

        let filebalances = await MoneyTrust.findAll({
          where: {
            client_id: result.client_id
          },
          attributes: [
            "case_id",
            [sequelize.fn("sum", sequelize.col("amount")), "amount"]
          ],
          group: ["case_id"],
          include: [
            { association: "matter", attributes: ["prefix", "casenumber"] }
          ]
        });

        newresult.push({
          amount: result.amount ? result.amount : 0,
          client: result.client,
          client_id: result.client_id,
          monthwise: {
            current: currentAmount[0],

            previous: previousAmount[0],

            beforepreviousMonth: previous2Amount[0]
          },
          trust_balance: trust_balance,
          matters: filebalances
        });
      }
      res.status(200).json({
        pages: pages,
        total: countData.length,
        docs: newresult,
        cdate: current_date,
        pdate: previous_date,
        bpdate: previous_two_date
        // amounts: newresult,
      });
    } catch (err) {
      next(err);
    }
  }
);

router.delete(
  "/deposit/client/:id",
  middleware.auth,
  async (req, res, next) => {
    MoneyTrust.findAll({ where: { client_id: req.params.id } })
      .then(deposit => {
        if (deposit == null)
          return res.status(404).json("Deposit is not found");
        deposit.destroy();
        res.status(204).json({});
      })
      .catch(next);
  }
);

router.get(
  "/deposits_balance/client/:id",
  middleware.auth,
  async (req, res, next) => {
    let deposits = await MoneyTrust.findAll({
      where: { client_id: req.params.id },
      attributes: ["id", "amount", "description"],
      include: [
        { association: "matter", attributes: ["id", "prefix", "casenumber"] }
      ]
    });
    var case_id = [];

    for (cid of deposits) {
      case_id.push(cid.matter.id);
    }
    let transaction = await Transaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "matter",
          attributes: ["id", "prefix", "casenumber", "status"]
        },
        {
          association: "trust",
          attributes: ["id", "amount", "description"]
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"]
        }
      ],
      where: { case_id: case_id },
      order: [["created_at", "ASC"]]
    });
    let credit_notes = await CreditNote.findAll({
      where: { client_id: req.params.id },
      include: [{ association: "matter" }]
    });
    if (credit_notes.length) {
      let credit_note_trans = [];
      for (let item of credit_notes) {
        credit_note_trans.push({
          id: item.id,
          type: "deposit",
          status: item.status,
          created_at: item.created_at,
          matter: item.matter,
          trust: {
            amount: item.total,
            description: "Credit Note #" + item.id
          }
        });
      }
      transaction = transaction.concat(credit_note_trans);
    }
    transaction = transaction.sort((a, b) => {
      return new Date(a.created_at) - new Date(b.created_at);
    });
    res.status(200).json({ deposits: deposits, transaction: transaction });
  }
);

router.get(
  "/money-allocations/paginate/user/:id",
  middleware.auth,
  async (req, res, next) => {
    let page = req.query.page || 1;
    limit = parseInt(req.query.limit) || limit;
    let userIds = await common.firmUsers(req.params.id);
    MoneyAllocation.paginate({
      where: { user_id: userIds },
      page: page,
      paginate: limit,
      include: [{ association: "client" }, { association: "matter" }]
    })
      .then(results => {
        res.status(200).json(results);
      })
      .catch(err => next(err));
  }
);

router.post(
  "/statement/matter/:id",
  middleware.auth,
  async (req, res, next) => {
    var whereStatement = {};
    if (req.body.fromDate) {
      let fromDate = moment(req.body.fromDate, "YYYY-MM-DD")
        .startOf("day")
        .format("YYYY-MM-DD HH:mm:ss");
      let toDate = moment(req.body.toDate, "YYYY-MM-DD")
        .endOf("day")
        .format("YYYY-MM-DD HH:mm:ss");
      (whereStatement.created_at = {
        $between: [fromDate, toDate]
      }),
        (whereStatement.case_id = req.params.id);
    } else {
      whereStatement.case_id = req.params.id;
    }

    //res.status(200).json({ body: req.body, where: whereStatement });
    let transactions = await Transaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "matter",
          attributes: ["id", "prefix", "casenumber", "status"]
        },
        {
          association: "trust",
          attributes: ["id", "amount", "description"]
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"]
        }
      ],
      where: whereStatement,
      order: [["created_at", "ASC"]]
    });
    let credit_notes = await CreditNote.findAll({
      where: { case_id: req.params.id },
      include: [{ association: "matter" }]
    });
    if (credit_notes.length) {
      let credit_note_trans = [];
      for (let item of credit_notes) {
        credit_note_trans.push({
          id: item.id,
          type: "deposit",
          status: item.status,
          created_at: item.created_at,
          matter: item.matter,
          trust: {
            amount: item.total,
            description: "CR #" + item.id
          }
        });
      }
      transactions = transactions.concat(credit_note_trans);
    }
    transactions = transactions.sort((a, b) => {
      return new Date(a.created_at) - new Date(b.created_at);
    });
    return res.status(200).json(transactions);
  }
);

router.get("/deposits/client/:id", middleware.auth, (req, res, next) => {
  MoneyTrust.findAll({
    where: { client_id: req.params.id },
    attributes: ["id", "amount", "description"],
    include: [
      { association: "matter", attributes: ["id", "prefix", "casenumber"] }
    ]
  })
    .then(deposits => res.status(200).json(deposits))
    .catch(next);
});

router.get(
  "/allocation/invoice/type/:type/id/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let { id, type } = req.params;

      let invoice = {};
      if (type == "t") invoice = await TimesheetInvoice.findByPk(id);
      else invoice = await Invoice.findByPk(id);

      let allocation = await MoneyAllocation.findOne({
        where: { invoice_id: id, invoice_type: type },
        attributes: [[sequelize.fn("sum", sequelize.col("amount")), "amount"]],
        group: ["invoice_id"]
      });

      if (
        invoice != null &&
        allocation != null &&
        allocation.amount != undefined
      ) {
        invoice.total -= allocation.amount;
      }

      return res.status(200).json({ allocation, invoice });
    } catch (err) {
      next(err);
    }
  }
);

router.get("/totalAmount/:userId", middleware.auth, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.userId);
    let TrustTotal = await MoneyTrust.findAll({
      where: { user_id: userIds },
      attributes: [
        "amount",
        [sequelize.fn("sum", sequelize.col("amount")), "TTotal"]
      ],
      //group: ['MoneyTrust.amount'],
      raw: true
    });

    let MoneyTotal = await MoneyAllocation.findAll({
      where: { user_id: userIds },
      attributes: [
        "amount",
        [sequelize.fn("sum", sequelize.col("amount")), "MTotal"]
      ],
      raw: true
    });

    let amount = TrustTotal[0].TTotal - MoneyTotal[0].MTotal;
    res.status(200).json({
      TrustTotal: TrustTotal,
      MoneyTotal: MoneyTotal,
      TotalAmount: amount
    });
  } catch (err) {
    next(err);
  }
});

router.get(
  "/allocation-label/matter/:id",
  middleware.auth,
  async (req, res, next) => {
    let matter = await Matter.findByPk(req.params.id, {
      attributes: ["id", "prefix", "casenumber"],
      include: [
        {
          association: "clientdetail",
          attributes: ["id", "firstname", "surname"]
        }
      ]
    });
    if (matter == null) return res.status(404).json("Matter is not found");

    let deposits = await Transaction.findAll({
      include: [
        {
          association: "trust",
          where: { case_id: req.params.id },
          attributes: ["amount"]
        }
      ],
      where: { type: "deposit" },
      attributes: []
    });
    deposits = deposits.map(row => row.trust.amount);
    let credits = await Transaction.findAll({
      include: [
        {
          association: "allocation",
          where: { case_id: req.params.id },
          attributes: ["amount"]
        }
      ],
      where: { type: "credit" },
      attributes: []
    });
    credits = credits.map(row => row.allocation.amount);
    let deposit_sum = 0;
    let allocation_sum = 0;
    if (deposits.length) {
      deposit_sum = deposits.reduce((a, b) => {
        return a + b;
      }, 0);
    }
    if (credits.length) {
      allocation_sum = credits.reduce((a, b) => {
        return a + b;
      }, 0);
    }

    res.status(200).json({
      client:
        matter.clientdetail.companyname ||
        matter.clientdetail.firstname + " " + matter.clientdetail.surname,
      matter: matter.prefix + matter.casenumber,
      trust: deposit_sum,
      avail: deposit_sum - allocation_sum
    });
  }
);

router.post("/sentBulkTrust", async (req, res, next) => {
  try {
    var t = [];
    for (let tid of req.body) {
      let transaction = await MoneyTrust.findAll({
        where: { client_id: tid }
      });
      t.push(transaction);
    }

    for (let trust of t[0]) {
      //console.log(t.length);

      let clientcontact = await ClientContact.findOne({
        where: { client_id: trust.client_id }
      });

      let casedetails = await CaseDetails.findOne({
        where: { client_id: trust.client_id }
      });

      let clientdetails = await ClientDetails.findOne({
        where: { id: trust.client_id }
      });

      var created_date = moment(trust.created_at).format("DD-MM-YYYY");
      let content = fs.readFileSync(
        "./src/email-templates/crm/money_transaction.html",
        "utf8"
      );
      content = content.replace(
        "**name**",
        clientdetails.firstname + " " + clientdetails.surname
      );
      content = content.replace(
        "**case**",
        casedetails.prefix + casedetails.casenumber
      );
      content = content.replace("**amount**", trust.amount);
      content = content.replace("**description**", trust.description);
      content = content.replace("**date**", created_date);
      if ((clientdetails.type = "i")) {
        let email = await Email.create({
          parent_id: trust.id,
          client_id: clientdetails.id,
          matter_id: casedetails.id,
          subject: "Transaction Details",
          type: "money_trust",
          user_id: trust.user_id
        });
        let sentMail = await mail.send(
          clientdetails.email,
          "Transaction Details",
          content,
          next
        );
      } else if ((clientdetails.type = "c")) {
        let email = await Email.create({
          parent_id: trust.id,
          client_id: clientcontact.id,
          matter_id: casedetails.id,
          subject: "Transaction Details",
          type: "money_trust",
          user_id: trust.user_id
        });
        let sentMail = await mail.send(
          clientcontact.email,
          "Transaction Details",
          content,
          next
        );
      }
    }

    res
      .status(200)
      .json({ code: 200, message: "Mail sent successfully", t: t });
  } catch (error) {
    next(error);
  }
});

router.post("/send-mail", middleware.auth, async (req, res, next) => {
  if (req.body.type == "all") {
    let deposits = await MoneyTrust.findAll({
      where: { client_id: req.body.client_id },
      attributes: ["id", "amount", "description"],
      include: [
        { association: "matter", attributes: ["id", "prefix", "casenumber"] }
      ]
    });
    var case_id = [];

    for (cid of deposits) {
      case_id.push(cid.matter.id);
    }
    let transaction = await Transaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "matter",
          attributes: ["id", "prefix", "casenumber", "status"]
        },
        {
          association: "trust",
          attributes: ["id", "amount", "description"]
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"]
        }
      ],
      where: { case_id: case_id },
      order: [["created_at", "ASC"]]
    });

    let clientdetails = await ClientDetails.findByPk(req.body.client_id);
    let content = fs.readFileSync(
      "./src/email-templates/crm/transactions.html",
      "utf8"
    );
    content = content.replace(
      "**name**",
      clientdetails.firstname + " " + clientdetails.surname
    );

    let transactionsHtml = "";
    let index = 1;
    for (let t of transaction) {
      transactionsHtml += "<tr>";
      transactionsHtml += `<td>${index}</td>`;
      transactionsHtml += `<td>${t.matter.prefix + t.matter.casenumber}</td>`;
      transactionsHtml += `<td>${moment(t.created_at).format(
        "DD-MM-YYYYY"
      )}</td>`;
      transactionsHtml += `<td>${(t.trust ? t.trust.description : "") ||
        (t.allocation ? t.allocation.description : "")}</td>`;
      transactionsHtml += `<td>${t.matter.status}</td>`;
      transactionsHtml += `<td>${t.trust ? t.trust.amount : 0.0}</td>`;
      transactionsHtml += `<td>${
        t.allocation ? t.allocation.amount : 0.0
      }</td>`;
      transactionsHtml += `<td>${t.balance}</td>`;
      transactionsHtml += "</tr>";
      index += 1;
    }

    content = content.replace("**transactions**", transactionsHtml);

    let email = await Email.create({
      client_id: clientdetails.id,
      subject: "Transactions",
      type: "all_transactions",
      user_id: clientdetails.user_id
    });

    let sentMail = await mail.send(
      clientdetails.email,
      "Transactions",
      content,
      next
    );

    res
      .status(200)
      .json({ code: 200, message: "Mail sent successfully", t: transaction });
  } else {
    let transaction = await Transaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "matter",
          attributes: ["id", "prefix", "casenumber", "status"]
        },
        {
          association: "trust",
          attributes: ["id", "amount", "description"]
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"]
        }
      ],
      where: { case_id: req.body.case_id },
      order: [["created_at", "ASC"]]
    });
    let matter = await Matter.findByPk(req.body.case_id);
    let clientdetails = await ClientDetails.findByPk(matter.client_id);
    let content = fs.readFileSync(
      "./src/email-templates/crm/transactions.html",
      "utf8"
    );
    content = content.replace(
      "**name**",
      clientdetails.firstname + " " + clientdetails.surname
    );

    let transactionsHtml = "";
    let index = 1;
    let bal = 0.0;
    for (let t of transaction) {
      transactionsHtml += "<tr>";
      transactionsHtml += `<td>${index}</td>`;
      transactionsHtml += `<td>${t.matter.prefix + t.matter.casenumber}</td>`;
      transactionsHtml += `<td>${moment(t.created_at).format(
        "DD-MM-YYYYY"
      )}</td>`;
      transactionsHtml += `<td>${(t.trust ? t.trust.description : "") ||
        (t.allocation ? t.allocation.description : "")}</td>`;
      transactionsHtml += `<td>${t.matter.status}</td>`;
      transactionsHtml += `<td>${t.trust ? t.trust.amount : 0.0}</td>`;
      transactionsHtml += `<td>${
        t.allocation ? t.allocation.amount : 0.0
      }</td>`;
      if (t.type == "deposit") {
        if (t.trust) {
          bal += t.trust.amount;
        }
      } else {
        if (t.trust) {
          bal -= t.allocation.amount;
        }
      }
      transactionsHtml += `<td>0.0</td>`;
      transactionsHtml += "</tr>";
      index += 1;
    }

    content = content.replace("**transactions**", transactionsHtml);

    let email = await Email.create({
      client_id: clientdetails.id,
      matter_id: req.body.case_id,
      subject: "Transactions",
      type: "case_transactions",
      user_id: clientdetails.user_id
    });

    let sentMail = await mail.send(
      clientdetails.email,
      "Transactions",
      content,
      next
    );
    res
      .status(200)
      .json({ code: 200, message: "Mail sent successfully", t: transaction });
  }
});

module.exports = router;
