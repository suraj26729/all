const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const Quote = require("../../models/accounts/Quote");
const QuoteItem = require("../../models/accounts/QuoteItems");
const Invoice = require("../../models/accounts/Invoice");
const User = require("../../models/User");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const helper = require("../../helpers/pdf");
const Op = Sequelize.Op;
let limit = 10;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const ClientContact = require("../../models/ClientContact");
const common = require("../../helpers/common");

const middleware = {
  validations: [
    check("client_id").isUUID(),
    check("case_id").isUUID(),
    //check("thirdparty_id").isUUID(),
    check("items")
      .isArray()
      .isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let quote = await Quote.create(req.body, {
        include: [{ association: "items" }]
      });

      quote = await Quote.findOne({
        where: { id: quote.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let url = await helper.generateQuotePDF({
        quote: quote,
        user_id: quote.user_id,
        balance: req.body.trust_balance
      });

      await Quote.update({ attachment: url }, { where: { id: quote.id } });

      var created_date = moment(quote.created_at).format("DD-MM-YYYY");
      var items1 = [];
      var total = [];
      for (item of quote.items) {
        var itemdate = moment(item.created_at).format("DD-MM-YYYY");
        //items1.push('<tr><td>d</td><td>d</td><td>d</td><td>d</td><td>d</td></tr>');
        items1.push(
          "<tr><td>" +
            itemdate +
            "</td><td>" +
            item.costtype.name +
            "</td><td>" +
            item.description +
            "</td><td>" +
            item.quantity +
            "</td><td>" +
            item.price +
            "</td></tr>"
        );
        total.push(
          '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">R ' +
            item.price +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">R ' +
            Math.abs(item.price - item.total) +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">R ' +
            item.total +
            "</td></tr>"
        );
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/quote_template.html",
        "utf8"
      );
      content = content.replace("**bank_details**", quote.client.bankdetails);
      content = content.replace("**notes**", quote.message);
      content = content.replace("**balance**", req.body.trust_balance);
      content = content.replace("**phone_number**", quote.client.contact1);
      content = content.replace("**email**", quote.client.email);
      content = content.replace("**number**", quote.quote_number);
      content = content.replace("**quote_date**", created_date);
      content = content.replace("**description**", quote.message);
      content = content.replace("**client_reference**", "");
      if (req.body.thirdparty_id) {
        content = content.replace(
          "**third_parties**",
          quote.thirdparty.firstname + " " + quote.thirdparty.surname
        );
      } else {
        content = content.replace("**third_parties**", "");
      }
      content = content.replace(
        "**name**",
        quote.client.firstname + " " + quote.client.surname
      );
      content = content.replace("**item**", items1);
      content = content.replace("**total**", total);

      // var firstname = req.body.contact.replace(/ .*/, "");
      // var client_id = req.body.client_id;

      // let clientc = await ClientContact.findOne({
      //   where: { client_id: client_id, firstname: firstname }
      // });

      if ((quote.client.type = "i")) {
        let sentMail = await mail.send(
          quote.client.email,
          "Quote",
          content,
          next
        );
      } else if ((quote.client.type = "c")) {
        let clientc = await ClientContact.findByPk(quote.contact);
        if (clientc != null) {
          let sentMail = await mail.send(clientc.email, "Quote", content, next);
        }
      }

      res.status(200).json(quote);
    } catch (err) {
      next(err);
    }
  }
);

router.get("/paginate/user/:id", middleware.auth, async (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  let userIds = await common.firmUsers(req.params.id);
  Quote.paginate({
    where: { user_id: userIds, status: [0, 1] },
    page: page,
    paginate: limit,
    order: ["quote_number"],
    attributes: [
      "id",
      "uuid",
      "client_id",
      "quote_number",
      "payment_date",
      "total",
      "status",
      "attachment"
    ],
    include: [
      { association: "client", attributes: ["firstname", "surname"] },
      { association: "matter", attributes: ["prefix", "casenumber"] }
    ]
  })
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => next(err));
});

router.get("/list/user/:id", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.id);
  Quote.findAll({
    where: { user_id: userIds },
    attributes: ["id", "quote_number"]
  })
    .then(quotes => {
      res.status(200).json(quotes);
    })
    .catch(err => next(err));
});

router.get("/id/:uuid", middleware.auth, (req, res, next) => {
  Quote.findOne({
    where: { uuid: req.params.uuid },
    include: [
      { association: "items", include: [{ association: "costtype" }] },
      { association: "client" },
      { association: "matter" },
      { association: "thirdparty" }
    ]
  })
    .then(quote => res.status(200).json(quote))
    .catch(err => next(err));
});

router.put(
  "/id/:uuid",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let quote = await Quote.findOne({
        where: { uuid: req.params.uuid },
        include: [{ association: "items" }]
      });

      if (quote == null)
        return res.status(404).json({ error: "Quote is not found" });

      let result = await QuoteItem.destroy({ where: { quote_id: quote.id } });

      if (result == 0)
        return res.status(400).json("Error while updating the items of quotes");

      quote.update(req.body);

      for (let item of req.body.items) {
        await QuoteItem.upsert(item, { where: { id: item.id } });
      }

      quote = await Quote.findOne({
        where: { id: quote.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let url = await helper.generateQuotePDF({
        quote: quote,
        user_id: quote.user_id,
        balance: req.body.trust_balance
      });

      quote = await Quote.update(
        { attachment: url },
        { where: { id: quote.id } }
      );

      return res.status(200).json(quote);
    } catch (err) {
      next(err);
    }
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  Quote.findOne({
    where: { id: req.params.id }
  })
    .then(quote => {
      if (quote != null) {
        quote.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Quote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch("/id/:id/restore", middleware.auth, (req, res, next) => {
  Quote.findOne({
    where: { id: req.params.id },
    paranoid: false
  })
    .then(quote => {
      if (quote != null) {
        quote.setDataValue("deleted_at", null);
        quote.update({ deleted_at: null });
        res.status(200).json(quote);
      } else {
        res.status(404).json({
          error: "Quote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.get("/list/client/:id", middleware.auth, (req, res, next) => {
  Quote.findAll({ where: { client_id: req.params.id } })
    .then(quotes => res.status(200).json(quotes))
    .catch(next);
});

router.patch("/convert/id/:id", middleware.auth, (req, res, next) => {
  Invoice.findOne({ where: { quote_id: req.params.id } })
    .then(invoice => {
      if (invoice != null)
        return res.status(400).json("Invoice already created for this quote");

      Quote.findByPk(req.params.id, { include: [{ association: "items" }] })
        .then(quote => {
          if (quote == null) return res.status(400).json("Quote is not found");
          Invoice.create(
            {
              client_id: quote.client_id,
              quote_id: quote.id,
              case_id: quote.case_id,
              thirdparty_id: quote.thirdparty_id,
              contact: quote.contact,
              payment_date: quote.payment_date,
              vat_ref: quote.vat_ref,
              message: quote.message,
              total: quote.total,
              status: 0,
              user_id: quote.user_id,
              items: quote.items
            },
            { include: [{ association: "items" }] }
          )
            .then(invoice => {
              quote.update({ status: 2 });
              res.status(201).json(invoice);
            })
            .catch(next);
        })
        .catch(next);
    })
    .catch(next);
});

router.get("/view/:id", middleware.auth, async (req, res, next) => {
  try {
    let quote = await Quote.findOne({
      where: { id: req.params.id },
      include: [
        { association: "items", include: [{ association: "costtype" }] },
        { association: "client" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });
    if (quote == null) return res.json("Quote not found");

    let result = await helper.generateQuotePDF({
      quote: quote,
      user_id: quote.user_id
    });
    res.json(result);
  } catch (err) {
    next(err);
  }
});

module.exports = router;
