const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const UserTemplate = require("../../models/accounts/UserTemplate");
const MasterTemplate = require("../../models/masters/Template");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const DefaultMessage = require("../../models/accounts/DefaultMessage");
const BankDetail = require("../../models/accounts/BankDetails");

router.get("/user/:id", checkToken, async (req, res, next) => {
  let user = await User.findByPk(req.params.id, {
    include: [{ association: "userdetail" }]
  });
  if (user == null) return res.status(404).json("User not found!");

  if (user.parent_user_id != null) {
    user = await User.findByPk(user.parent_user_id, {
      include: [{ association: "userdetail" }]
    });
    if (user == null) return res.status(404).json("Parent user not found!");
  }

  let userTemplate = await UserTemplate.findOne({
    where: { user_id: user.id }
  });
  if (userTemplate == null) {
    userTemplate = await MasterTemplate.findByPk(1);
  }

  let defaultMessage = await DefaultMessage.findOne({
    where: { user_id: req.params.id }
  });
  let bankDetail = await BankDetail.findOne({
    where: { user_id: req.params.id }
  });

  return res
    .status(200)
    .json({ user, userTemplate, defaultMessage, bankDetail });
});

module.exports = router;
