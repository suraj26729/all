const router = require("express").Router();
const Expense = require("../../models/accounts/Expense");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");

const middleware = {
  validations: [
    check("name").isString(),
    check("amount").isDecimal(),
    check("category").isString(),
    check("type").isString(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let expense = await Expense.create(req.body);
      return res.status(201).json(expense);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get("/paginate/user/:id", middleware.auth, async (req, res, next) => {
  try {
    let page = req.query.page || 1;
    let limit = parseInt(req.query.limit) || 20;
    let userIds = await common.firmUsers(req.params.id);
    let expenses = await Expense.paginate({
      where: { user_id: userIds },
      page: page,
      paginate: limit,
      order: [["created_at", "DESC"]]
    });
    return res.status(200).json(expenses);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/id/:id", middleware.auth, async (req, res, next) => {
  try {
    let expense = await Expense.findByPk(req.params.id);
    return res.status(200).json(expense);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.put(
  "/id/:id",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      let expense = await Expense.update(req.body, {
        where: { id: req.params.id }
      });
      return res.status(200).json(expense);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.delete("/id/:id", middleware.auth, async (req, res, next) => {
  try {
    let expense = await Expense.destroy({ where: { id: req.params.id } });
    return res.status(204).json(expense);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
