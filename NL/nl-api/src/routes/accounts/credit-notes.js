const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const CreditNote = require("../../models/accounts/CreditNote");
const CreditNoteItem = require("../../models/accounts/CreditNoteItems");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const helper = require("../../helpers/pdf");
let limit = 10;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const ClientContact = require("../../models/ClientContact");
const common = require("../../helpers/common");
const User = require("../../models/User");
const Email = require("../../models/Email");
const {
  getCountries,
  getCurrencySymbol,
  getCurrencySymbolFromIso2
} = require("country-from-iso2");
const Transaction = require("../../models/accounts/Transaction");

const middleware = {
  validations: [
    check("client_id").isUUID(),
    check("invoice_id").isInt(),
    check("case_id").isUUID(),
    //check("thirdparty_id").isUUID(),
    check("items")
      .isArray()
      .isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

const trustBalance = async case_id => {
  let deposits = await Transaction.findAll({
    include: [
      {
        association: "trust",
        where: { case_id: case_id },
        attributes: ["amount"]
      }
    ],
    where: { type: "deposit" },
    attributes: []
  });
  deposits = deposits.map(row => row.trust.amount);

  let deposit_sum = 0;
  if (deposits.length) {
    deposit_sum = deposits.reduce((a, b) => {
      return a + b;
    }, 0);
  }

  return deposit_sum;
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      var user = [];
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;

      let creditnote = await CreditNote.create(req.body, {
        include: [{ association: "items" }]
      });

      creditnote = await CreditNote.findOne({
        where: { id: creditnote.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let firmusers = await common.firmUsers(creditnote.user_id);
      let max_invoice_num = await CreditNote.findOne({
        attributes: [
          [Sequelize.fn("max", Sequelize.col("credit_note_number")), "max"]
        ],
        where: { user_id: firmusers },
        paranoid: false,
        raw: true
      });

      creditnote.update({ credit_note_number: max_invoice_num.max + 1 });

      for (let fu of firmusers) {
        let u = await User.findOne({
          where: { id: fu, parent_user_id: null },
          include: [{ association: "userdetail" }]
        });
        if (u !== null) {
          user.push(u);
        }
      }

      var currency = user[0].userdetail.currency;

      var cs = await getCurrencySymbol(currency);

      var currencySymbol = "";
      if (cs) {
        currencySymbol = cs;
      } else {
        currencySymbol = "R";
      }

      let balance = await trustBalance(creditnote.case_id);

      let url = await helper.generateCreditNotePDF({
        currency: currency,
        creditnote: creditnote,
        user_id: creditnote.user_id,
        balance: balance
      });

      await CreditNote.update(
        { attachment: url },
        { where: { id: creditnote.id } }
      );

      // var created_date = moment(creditnote.created_at).format("DD-MM-YYYY");
      // var items1 = [];
      // var total = [];
      // for (item of creditnote.items) {
      //     var itemdate = moment(item.created_at).format("DD-MM-YYYY");
      //     items1.push(
      //         "<tr><td>" +
      //         item.description +
      //         "</td><td>" +
      //         item.costtype.name +
      //         "</td><td>" +
      //         item.total +
      //         "</td><td>" +
      //         item.quantity +
      //         "</td><td>" +
      //         item.price +
      //         "</td></tr>"
      //     );
      //     total.push(
      //         '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">' + currencySymbol + " " +
      //         item.price +
      //         '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">' + currencySymbol + " " +
      //         Math.abs(item.price - item.total) +
      //         '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">' + currencySymbol + " " +
      //         item.total +
      //         "</td></tr>"
      //     );
      // }

      // let content = fs.readFileSync(
      //     "./src/email-templates/crm/credit_note.html",
      //     "utf8"
      // );
      // content = content.replace(
      //     "**bank_details**",
      //     creditnote.client.bankdetails
      // );
      // content = content.replace("**notes**", creditnote.message);
      // content = content.replace("**balance**", req.body.trust_balance);
      // content = content.replace("**phone_number**", creditnote.client.contact1);
      // content = content.replace("**email**", creditnote.client.email);
      // content = content.replace("**number**", creditnote.credit_note_number);
      // content = content.replace("**quote_date**", created_date);
      // content = content.replace("**description**", creditnote.message);
      // content = content.replace("**client_reference**", "");
      // if (req.body.thirdparty_id) {
      //     content = content.replace(
      //         "**third_parties**",
      //         creditnote.thirdparty.firstname + " " + creditnote.thirdparty.surname
      //     );
      // } else {
      //     content = content.replace("**third_parties**", "");
      // }
      // content = content.replace(
      //     "**name**",
      //     creditnote.client.firstname + " " + creditnote.client.surname
      // );
      // content = content.replace("**item**", items1);
      // content = content.replace("**total**", total);

      // var firstname = req.body.contact.replace(/ .*/, "");
      // var client_id = req.body.client_id;

      // let clientc = await ClientContact.findOne({
      //   where: { client_id: client_id, firstname: firstname }
      // });

      // if ((creditnote.client.type = "i")) {
      //     let sentMail = await mail.send(
      //         creditnote.client.email,
      //         "CREDIT NOTE",
      //         content,
      //         next
      //     );
      // } else if ((creditnote.client.type = "c" && creditnote.contact)) {
      //     let clientc = await ClientContact.findByPk(creditnote.contact);
      //     if (clientc != null) {
      //         let sentMail = await mail.send(
      //             clientc.email,
      //             "CREDIT NOTE",
      //             content,
      //             next
      //         );
      //     }
      // }

      res.status(200).json(creditnote);
    } catch (err) {
      console.log(err);
    }
  }
);

router.get("/paginate/user/:id", middleware.auth, async (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  limit = 9999999;
  let userIds = await common.firmUsers(req.params.id);
  CreditNote.paginate({
    where: { user_id: userIds },
    page: page,
    paginate: limit,
    order: [["credit_note_number", "DESC"]],
    attributes: [
      "id",
      "uuid",
      "client_id",
      "credit_note_number",
      "payment_date",
      "total",
      "status",
      "attachment"
    ],
    include: [
      { association: "client" },
      { association: "casecontact" },
      { association: "thirdparty" },
      { association: "matter" }
    ]
  })
    .then(creditnotes => {
      res.status(200).json(creditnotes);
    })
    .catch(err => next(err));
});

router.get("/list/user/:id", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.id);
  CreditNote.findAll({
    where: { user_id: userIds },
    attributes: ["id", "credit_note_number"]
  })
    .then(creditnotes => {
      res.status(200).json(creditnotes);
    })
    .catch(err => next(err));
});

router.get("/id/:uuid", middleware.auth, (req, res, next) => {
  CreditNote.findOne({
    where: { uuid: req.params.uuid },
    include: [
      {
        association: "items",
        include: [
          {
            association: "costtype",
            attributes: ["id", "name", "code"]
          }
        ]
      },
      { association: "client" },
      { association: "invoice" },
      { association: "matter" },
      { association: "thirdparty" }
    ]
  })
    .then(creditnote => res.status(200).json(creditnote))
    .catch(err => next(err));
});

router.put(
  "/id/:uuid",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      req.body.payment_date = moment(
        req.body.payment_date,
        "YYYY-MM-DD"
      ).format("YYYY-MM-DD");

      let creditnote = await CreditNote.findOne({
        where: { uuid: req.params.uuid },
        include: [{ association: "items" }, { association: "client" }]
      });
      if (creditnote == null)
        return res.status(404).json({ error: "CreditNote is not found" });

      let result = await CreditNoteItem.destroy({
        where: { credit_note_id: creditnote.id },
        force: true
      });
      if (result == 0)
        return res
          .status(400)
          .json("Error while updating the items of credit note");
      creditnote.update(req.body);
      for (let item of req.body.items) {
        await CreditNoteItem.upsert(item, { where: { id: item.id } });
      }
      creditnote = await CreditNote.findOne({
        where: { id: creditnote.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          // { association: "quote" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let balance = await trustBalance(creditnote.case_id);
      let url = await helper.generateCreditNotePDF({
        creditnote: creditnote,
        user_id: creditnote.user_id,
        balance: balance
      });

      await CreditNote.update(
        { attachment: url },
        { where: { id: creditnote.id } }
      );

      return res.status(200).json(creditnote);
    } catch (err) {
      next(err);
    }
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  CreditNote.findOne({
    where: { id: req.params.id }
  })
    .then(creditnote => {
      if (creditnote != null) {
        creditnote.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "CreditNote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch("/id/:id/restore", middleware.auth, (req, res, next) => {
  CreditNote.findOne({
    where: { id: req.params.id },
    paranoid: false
  })
    .then(creditnote => {
      if (creditnote != null) {
        creditnote.setDataValue("deleted_at", null);
        creditnote.update({ deleted_at: null });
        res.status(200).json(creditnote);
      } else {
        res.status(404).json({
          error: "CreditNote is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.post("/sentBulk", async (req, res, next) => {
  try {
    var creditnotes = [];
    var user = [];

    creditnotes = await CreditNote.findAll({
      where: { id: req.body },
      include: [
        { association: "items", include: [{ association: "costtype" }] },
        { association: "client" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });

    for (let cn of creditnotes) {
      let firmusers = await common.firmUsers(cn.user_id);

      for (let fu of firmusers) {
        let u = await User.findOne({
          where: { id: fu, parent_user_id: null },
          include: [{ association: "userdetail" }]
        });
        if (u !== null) {
          user.push(u);
        }
      }

      var currency = user[0].userdetail.currency;

      var cs = await getCurrencySymbol(currency);

      var currencySymbol = "";
      if (cs) {
        currencySymbol = cs;
      } else {
        currencySymbol = "R";
      }

      var created_date = moment(cn.created_at).format("DD-MM-YYYY");
      var items = [];
      var total = [];
      for (item of cn.items) {
        var itemdate = moment(item.created_at).format("DD-MM-YYYY");
        //items1.push('<tr><td>d</td><td>d</td><td>d</td><td>d</td><td>d</td></tr>');
        // console.log(item.description);
        // console.log(item.costtype.name);
        // console.log(item.total);
        items.push(
          "<tr><td>" +
            item.description +
            "</td><td>" +
            item.costtype.name +
            "</td><td>" +
            item.total +
            "</td><td>" +
            item.quantity +
            "</td><td>" +
            item.price +
            "</td></tr>"
        );
        total.push(
          '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.price +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            Math.abs(item.price - item.total) +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.total +
            "</td></tr>"
        );
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/credit_note.html",
        "utf8"
      );

      if (cn.client) {
        content = content.replace("**bank_details**", cn.client.bankdetails);
        content = content.replace("**phone_number**", cn.client.contact1);
        content = content.replace("**email**", cn.client.email);
        content = content.replace(
          "**name**",
          cn.client.firstname + " " + cn.client.surname
        );
      } else {
        content = content.replace("**bank_details**", "-");
        content = content.replace("**phone_number**", "-");
        content = content.replace("**email**", "-");
        content = content.replace("**name**", "-" + " " + "-");
      }

      if (cn.thirdparty) {
        content = content.replace(
          "**third_parties**",
          cn.thirdparty.firstname + " " + cn.thirdparty.surname
        );
      } else {
        content = content.replace("**third_parties**", "-" + " " + "-");
      }
      if (user[0].userdetail.logo) {
        content = content.replace(
          "**logo**",
          "https://www.naartjielegal.com/api/" + user[0].userdetail.logo
        );
      } else {
        content = content.replace(
          "**logo**",
          "https://www.naartjielegal.com/assets/images/logo.png"
        );
      }

      var company_title = "";
      // if (cn.client.type == 'i') {
      //     company_title = cn.client.firstname + " " + cn.client.surname;
      // } else {
      //     company_title = cn.client.companyname
      // }
      if (user[0].userdetail.name_of_firm) {
        company_title = user[0].userdetail.name_of_firm;
      }
      content = content.replace("**company_title**", company_title);

      // content = content.replace(
      //   "**company_title**",
      //   user[0].userdetail.firstname + " " + user[0].userdetail.lastname
      // );
      content = content.replace(
        "**owner**",
        user[0].userdetail.firstname + " " + user[0].userdetail.lastname
      );
      content = content.replace("**owneraddress**", user[0].userdetail.address);
      content = content.replace("**owneremail**", user[0].email);
      content = content.replace("**phone**", user[0].userdetail.contact1);
      content = content.replace("**zip**", "");

      content = content.replace("**notes**", cn.message);
      content = content.replace("**number**", cn.invoice_number);
      content = content.replace("**quote_date**", created_date);
      content = content.replace("**description**", cn.message);
      content = content.replace("**client_reference**", "");

      content = content.replace("**item**", items);
      content = content.replace("**total**", total);
      if (cn.client != null) {
        content = content.replace(
          "**name**",
          cn.client.firstname + " " + cn.client.surname
        );
      } else {
        content = content.replace("**name**", " ");
      }

      let u_credit_note = await CreditNote.findByPk(cn.id);
      if (u_credit_note != null) u_credit_note.update({ status: 1 });

      if ((cn.client.type = "i")) {
        if (cn.client.email) {
          let email = await Email.create({
            parent_id: cn.id,
            client_id: cn.client_id,
            matter_id: cn.case_id,
            subject: "Credit Note",
            type: "credit_note",
            user_id: cn.user_id
          });
          let sentMail = await mail.sendWithAttachment(
            cn.client.email,
            //"keegayu5662@gmail.com",
            "Credit Note",
            content,
            cn.attachment,
            next
          );
        }
      } else if ((cn.client.type = "c" && cn.contact)) {
        let clientc = await ClientContact.findByPk(cn.contact);
        if (clientc != null) {
          let email = await Email.create({
            parent_id: cn.id,
            client_id: clientc.id,
            matter_id: cn.case_id,
            subject: "Credit Note",
            type: "credit_note",
            user_id: cn.user_id
          });
          let sentMail = await mail.sendWithAttachment(
            clientc.email,
            //"keegayu5662@gmail.com",
            "Credit Note",
            content,
            cn.attachment,
            next
          );
        }
      }
      res.status(200).end();
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
