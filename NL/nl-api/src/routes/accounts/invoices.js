const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const Quote = require("../../models/accounts/Quote");
const Invoice = require("../../models/accounts/Invoice");
const InvoiceItem = require("../../models/accounts/InvoiceItems");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const helper = require("../../helpers/pdf");
let limit = 999;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const ClientContact = require("../../models/ClientContact");
const common = require("../../helpers/common");
const sms = require("../../helpers/sms");
const iplocation = require("iplocation").default;
const {
  getCountries,
  getCurrencySymbol,
  getCurrencySymbolFromIso2
} = require("country-from-iso2");
const User = require("../../models/User");
const Email = require("../../models/Email");

const middleware = {
  validations: [
    check("client_id").isUUID(),
    check("case_id").isUUID(),
    //check("thirdparty_id").isUUID(),
    check("items")
      .isArray()
      .isLength({ min: 1 }),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      var user = [];
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      if (req.body.quote_id != "") {
        let quote = await Quote.findByPk(req.body.quote_id);
        if (quote != null) quote.update({ status: 2 });
      } else {
        delete req.body.quote_id;
      }

      if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;

      let invoice = await Invoice.create(req.body, {
        include: [{ association: "items" }]
      });
      invoice = await Invoice.findOne({
        where: { id: invoice.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          // { association: "quote" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      let firmusers = await common.firmUsers(invoice.user_id);
      let max_invoice_num = await Invoice.findOne({
        attributes: [
          [Sequelize.fn("max", Sequelize.col("invoice_number")), "max"]
        ],
        where: { user_id: firmusers },
        paranoid: false,
        raw: true
      });

      invoice.update({ invoice_number: max_invoice_num.max + 1 });

      ///CURRENCY SYMBOL////

      for (let fu of firmusers) {
        let u = await User.findOne({
          where: { id: fu, parent_user_id: null },
          include: [{ association: "userdetail" }]
        });
        if (u !== null) {
          user.push(u);
        }
      }

      var currency = user[0].userdetail.currency;

      var cs = await getCurrencySymbol(currency);

      // var ip = req.headers['x-forwarded-for'] ||
      //     req.connection.remoteAddress ||
      //     req.socket.remoteAddress ||
      //     (req.connection.socket ? req.connection.socket.remoteAddress : null);
      // let location = await iplocation(ip);
      // let cc = await getCurrencySymbolFromIso2(location.countryCode);

      var currencySymbol = "";
      if (cs) {
        currencySymbol = cs;
      } else {
        currencySymbol = "R";
      }
      ///CURRENCY SYMBOL ENDS////
      console.log(currencySymbol);

      let url = await helper.generateInvoicePDF({
        currency: currency,
        invoice: invoice,
        user_id: invoice.user_id,
        balance: req.body.trust_balance
      });

      await Invoice.update({ attachment: url }, { where: { id: invoice.id } });

      var created_date = moment(invoice.created_at).format("DD-MM-YYYY");
      var items1 = [];
      var total = [];
      for (item of invoice.items) {
        var itemdate = moment(item.created_at).format("DD-MM-YYYY");
        //items1.push('<tr><td>d</td><td>d</td><td>d</td><td>d</td><td>d</td></tr>');
        items1.push(
          "<tr><td>" +
            itemdate +
            "</td><td>" +
            item.costtype.name +
            "</td><td>" +
            item.description +
            "</td><td>" +
            item.quantity +
            "</td><td>" +
            item.price +
            "</td></tr>"
        );

        total.push(
          '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.price +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            Math.abs(item.price - item.total) +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.total +
            "</td></tr>"
        );
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/invoice_template.html",
        "utf8"
      );
      var vatR = "";
      if (invoice.vat_ref) {
        vatR = invoice.vat_ref;
      }

      content = content.replace("**bank_details**", invoice.client.bankdetails);
      content = content.replace("**vatrefernce**", vatR);
      content = content.replace("**notes**", invoice.message);
      content = content.replace("**balance**", req.body.trust_balance);
      content = content.replace("**phone_number**", invoice.client.contact1);
      content = content.replace("**email**", invoice.client.email);
      content = content.replace("**number**", invoice.invoice_number);
      content = content.replace("**quote_date**", created_date);
      content = content.replace("**description**", invoice.message);
      content = content.replace("**client_reference**", "");
      if (req.body.thirdparty_id) {
        content = content.replace(
          "**third_parties**",
          invoice.thirdparty.firstname + " " + invoice.thirdparty.surname
        );
      } else {
        content = content.replace("**third_parties**", "");
      }
      content = content.replace(
        "**name**",
        invoice.client.firstname + " " + invoice.client.surname
      );
      content = content.replace("**item**", items1);
      content = content.replace("**total**", total);

      // var firstname = req.body.contact.replace(/ .*/, "");
      // var client_id = req.body.client_id;

      // let clientc = await ClientContact.findOne({
      //   where: { client_id: client_id, firstname: firstname }
      // });

      //await Invoice.update({ status: 1 }, { where: { id: invoice.id } });

      // if (invoice.client) {
      //     if (invoice.client.type = "i") {
      //         if (invoice.client.email) {
      //             let sentMail = await mail.sendWithAttachment(
      //                 invoice.client.email,
      //                 //"Dhivakarmm@gmail.com",
      //                 "TAX INVOICE",
      //                 content,
      //                 invoice.attachment,
      //                 next
      //             );
      //         }

      //     } else if ((invoice.client.type = "c" && invoice.contact)) {
      //         //var firstname = invoice.contact.replace(/ .*/, "");
      //         //var client_id = invoice.client_id;

      //         // let clientc = await ClientContact.findOne({
      //         //   where: { client_id: client_id, firstname: firstname }
      //         // });
      //         let clientc = await ClientContact.findByPk(invoice.contact);
      //         if (clientc) {
      //             let sentMail = await mail.sendWithAttachment(
      //                 clientc.email,
      //                 //"Dhivakarmm@gmail.com",
      //                 "TAX INVOICE",
      //                 content,
      //                 invoice.attachment,
      //                 next
      //             );
      //         }
      //     }
      // }

      let result = "";
      if (req.body.notify_sms) {
        let to =
          invoice.client != undefined && invoice.client != null
            ? invoice.client.mobile
            : null;
        if (to != "" && to != undefined && to != null) {
          let name = "Sir / Madam";
          if (invoice.client.type == "i")
            name = invoice.client.firstname + " " + invoice.client.surname;
          else name = invoice.client.companyname;

          result = await sms.sendInvoice(
            name,
            invoice.invoice_number,
            to,
            invoice.user_id
          );
        }
      }

      return res.status(200).json({ invoice, sms: result });
    } catch (err) {
      next(err);
      console.log(err);
    }
  }
);

// router.post(
//     "/", [middleware.validations, middleware.auth],
//     async(req, res, next) => {
//         try {
//             const errors = validationResult(req);
//             if (!errors.isEmpty()) {
//                 return res.status(422).json({ error: errors.array() });
//             }

//             if (req.body.quote_id != "") {
//                 let quote = await Quote.findByPk(req.body.quote_id);
//                 if (quote != null) quote.update({ status: 2 });
//             } else {
//                 delete req.body.quote_id;
//             }

//             if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;

//             let invoice = await Invoice.create(req.body, {
//                 include: [{ association: "items" }]
//             });
//             invoice = await Invoice.findOne({
//                 where: { id: invoice.id },
//                 include: [
//                     { association: "items", include: [{ association: "costtype" }] },
//                     { association: "client" },
//                     // { association: "quote" },
//                     { association: "matter" },
//                     { association: "thirdparty" }
//                 ]
//             });

//             ///CURRENCY SYMBOL////
//             var ip =
//                 req.headers["x-forwarded-for"] ||
//                 req.connection.remoteAddress ||
//                 req.socket.remoteAddress ||
//                 (req.connection.socket ? req.connection.socket.remoteAddress : null);
//             let location = await iplocation(ip);
//             let cc = await getCurrencySymbolFromIso2(location.countryCode);
//             var currencySymbol = "";
//             if (cc) {
//                 currencySymbol = cc;
//             } else {
//                 currencySymbol = "R";
//             }
//             ///CURRENCY SYMBOL ENDS////

//             let url = await helper.generateInvoicePDF({
//                 ip: ip,
//                 invoice: invoice,
//                 user_id: invoice.user_id,
//                 balance: req.body.trust_balance
//             });

//             await Invoice.update({ attachment: url }, { where: { id: invoice.id } });

//             var created_date = moment(invoice.created_at).format("DD-MM-YYYY");
//             var items1 = [];
//             var total = [];
//             for (item of invoice.items) {
//                 var itemdate = moment(item.created_at).format("DD-MM-YYYY");
//                 //items1.push('<tr><td>d</td><td>d</td><td>d</td><td>d</td><td>d</td></tr>');
//                 items1.push(
//                     "<tr><td>" +
//                     itemdate +
//                     "</td><td>" +
//                     item.costtype.name +
//                     "</td><td>" +
//                     item.description +
//                     "</td><td>" +
//                     item.quantity +
//                     "</td><td>" +
//                     item.price +
//                     "</td></tr>"
//                 );

//                 total.push(
//                     '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">' +
//                     currencySymbol +
//                     " " +
//                     item.price +
//                     '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">' +
//                     currencySymbol +
//                     " " +
//                     Math.abs(item.price - item.total) +
//                     '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">' +
//                     currencySymbol +
//                     " " +
//                     item.total +
//                     "</td></tr>"
//                 );
//             }

//             let content = fs.readFileSync(
//                 "./src/email-templates/crm/invoice_template.html",
//                 "utf8"
//             );
//             content = content.replace("**bank_details**", invoice.client.bankdetails);
//             content = content.replace("**notes**", invoice.message);
//             content = content.replace("**balance**", req.body.trust_balance);
//             content = content.replace("**phone_number**", invoice.client.contact1);
//             content = content.replace("**email**", invoice.client.email);
//             content = content.replace("**number**", invoice.invoice_number);
//             content = content.replace("**quote_date**", created_date);
//             content = content.replace("**description**", invoice.message);
//             content = content.replace("**client_reference**", "");
//             if (req.body.thirdparty_id) {
//                 content = content.replace(
//                     "**third_parties**",
//                     invoice.thirdparty.firstname + " " + invoice.thirdparty.surname
//                 );
//             } else {
//                 content = content.replace("**third_parties**", "");
//             }
//             content = content.replace(
//                 "**name**",
//                 invoice.client.firstname + " " + invoice.client.surname
//             );
//             content = content.replace("**item**", items1);
//             content = content.replace("**total**", total);

//             // var firstname = req.body.contact.replace(/ .*/, "");
//             // var client_id = req.body.client_id;

//             // let clientc = await ClientContact.findOne({
//             //   where: { client_id: client_id, firstname: firstname }
//             // });

//             //await Invoice.update({ status: 1 }, { where: { id: invoice.id } });

//             // if (invoice.client) {
//             //     if (invoice.client.type = "i") {
//             //         if (invoice.client.email) {
//             //             let sentMail = await mail.sendWithAttachment(
//             //                 invoice.client.email,
//             //                 //"Dhivakarmm@gmail.com",
//             //                 "TAX INVOICE",
//             //                 content,
//             //                 invoice.attachment,
//             //                 next
//             //             );
//             //         }

//             //     } else if ((invoice.client.type = "c" && invoice.contact)) {
//             //         //var firstname = invoice.contact.replace(/ .*/, "");
//             //         //var client_id = invoice.client_id;

//             //         // let clientc = await ClientContact.findOne({
//             //         //   where: { client_id: client_id, firstname: firstname }
//             //         // });
//             //         let clientc = await ClientContact.findByPk(invoice.contact);
//             //         if (clientc) {
//             //             let sentMail = await mail.sendWithAttachment(
//             //                 clientc.email,
//             //                 //"Dhivakarmm@gmail.com",
//             //                 "TAX INVOICE",
//             //                 content,
//             //                 invoice.attachment,
//             //                 next
//             //             );
//             //         }
//             //     }
//             // }
//             return res.status(200).json(invoice);
//         } catch (err) {
//             next(err);
//         }
//     }
// );

router.get("/paginate/user/:id", middleware.auth, async (req, res, next) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  limit = 9999999;
  let userIds = await common.firmUsers(req.params.id);
  Invoice.paginate({
    where: { user_id: userIds },
    page: page,
    paginate: limit,
    order: [["invoice_number", "DESC"]],
    attributes: [
      "id",
      "uuid",
      "client_id",
      "invoice_number",
      "payment_date",
      "total",
      "status",
      "attachment"
    ],
    include: [
      {
        association: "client"
        //attributes: ["firstname", "surname", "companyname", "type"]
      },
      { association: "casecontact" },
      { association: "thirdparty", attributes: ["firstname", "surname"] },
      { association: "matter", attributes: ["prefix", "casenumber"] }
    ]
  })
    .then(invoices => {
      res.status(200).json(invoices);
    })
    .catch(err => next(err));
});

router.get("/list/user/:id", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.id);
  Invoice.findAll({
    where: { user_id: userIds },
    attributes: ["id", "invoice_number"]
  })
    .then(invoices => {
      res.status(200).json(invoices);
    })
    .catch(err => next(err));
});

router.get("/id/:uuid", middleware.auth, async (req, res, next) => {
  try {
    let invoice = await Invoice.findOne({
      where: { uuid: req.params.uuid },
      include: [
        {
          association: "items",
          include: [
            {
              association: "costtype",
              attributes: ["id", "name", "code"]
            }
          ]
        },
        { association: "client" },
        // { association: "quote" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });
    let old_invoices = [];
    if (invoice != null) {
      let firmusers = await common.firmUsers(invoice.user_id);
      old_invoices = await Invoice.findAll({
        where: {
          user_id: firmusers,
          invoice_number: invoice.invoice_number,
          parent_id: { $not: null }
        },
        include: [
          {
            association: "items",
            include: [
              {
                association: "costtype",
                attributes: ["id", "name", "code"]
              }
            ]
          },
          { association: "client" },
          // { association: "quote" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });
    }
    return res.status(200).json({ invoice, old_invoices });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.put(
  "/id/:uuid",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      req.body.payment_date = moment(
        req.body.payment_date,
        "YYYY-MM-DD"
      ).format("YYYY-MM-DD");

      let c_invoice = await Invoice.findOne({
        where: { uuid: req.params.uuid },
        include: [{ association: "items" }, { association: "client" }]
      });
      if (c_invoice == null)
        return res.status(404).json({ error: "Invoice is not found" });

      let firmusers = await common.firmUsers(c_invoice.user_id);

      let parent_invoice = await Invoice.findOne({
        where: {
          invoice_number: c_invoice.invoice_number,
          user_id: firmusers,
          parent_id: { $not: null }
        },
        order: [["id", "ASC"]]
      });

      let parent_id = parent_invoice != null ? parent_invoice.id : c_invoice.id;
      let invoice_number = c_invoice.invoice_number;

      c_invoice.update({ parent_id: parent_id });

      req.body.status = c_invoice.status;

      req.body.items = req.body.items.map(item => {
        delete item.invoice_id;
        return item;
      });

      delete req.body.id;
      delete req.body.uuid;

      // return res.json(req.body);

      let new_invoice = await Invoice.create(req.body, {
        include: [{ association: "items" }]
      });

      // let result = await InvoiceItem.destroy({
      //   where: { invoice_id: invoice.id },
      //   force: true
      // });
      // if (result == 0)
      //   return res
      //     .status(400)
      //     .json("Error while updating the items of invoice");

      // invoice.update(req.body);

      // for (let item of req.body.items) {
      //   await InvoiceItem.upsert(item, { where: { id: item.id } });
      // }

      let max_invoice_num = await Invoice.findOne({
        attributes: [
          [Sequelize.fn("max", Sequelize.col("invoice_number")), "max"]
        ],
        where: { user_id: firmusers },
        paranoid: false,
        raw: true
      });

      invoice_number =
        invoice_number == null
          ? parseInt(max_invoice_num.max) + 1
          : invoice_number;

      await Invoice.update(
        { invoice_number: invoice_number },
        { where: { id: new_invoice.id } }
      );

      invoice = await Invoice.findOne({
        where: { id: new_invoice.id },
        include: [
          { association: "items", include: [{ association: "costtype" }] },
          { association: "client" },
          // { association: "quote" },
          { association: "matter" },
          { association: "thirdparty" }
        ]
      });

      ///CURRENCY SYMBOL////
      var ip =
        req.headers["x-forwarded-for"] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        (req.connection.socket ? req.connection.socket.remoteAddress : null);
      let location = await iplocation(ip);
      let cc = await getCurrencySymbolFromIso2(location.countryCode);
      var currencySymbol = "";
      if (cc) {
        currencySymbol = cc;
      } else {
        currencySymbol = "R";
      }
      ///CURRENCY SYMBOL ENDS////

      let url = await helper.generateInvoicePDF({
        ip: ip,
        invoice: invoice,
        user_id: invoice.user_id,
        balance: req.body.trust_balance
      });

      await Invoice.update({ attachment: url }, { where: { id: invoice.id } });

      let _result = "";
      if (req.body.notify_sms) {
        let to =
          invoice.client != undefined && invoice.client != null
            ? invoice.client.mobile
            : null;
        if (to != "" && to != undefined && to != null) {
          let name = "Sir / Madam";
          if (invoice.client.type == "i")
            name = invoice.client.firstname + " " + invoice.client.surname;
          else name = invoice.client.companyname;
          //console.log(name, invoice.invoice_number, to);

          // to = "+918248583545";

          _result = await sms.sendInvoice(
            name,
            invoice.invoice_number,
            to,
            invoice.user_id
          );
        }
      }

      return res.status(200).json({ invoice, sms: _result });
    } catch (err) {
      next(err);
    }
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  Invoice.findOne({
    where: { id: req.params.id }
  })
    .then(invoice => {
      if (invoice != null) {
        invoice.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Invoice is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch("/id/:id/restore", middleware.auth, (req, res, next) => {
  Invoice.findOne({
    where: { id: req.params.id },
    paranoid: false
  })
    .then(invoice => {
      if (invoice != null) {
        invoice.setDataValue("deleted_at", null);
        invoice.update({ deleted_at: null });
        res.status(200).json(invoice);
      } else {
        res.status(404).json({
          error: "Invoice is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.get("/list/client/:id", middleware.auth, (req, res, next) => {
  Invoice.findAll({
    where: { client_id: req.params.id },
    attributes: ["id", "invoice_number"]
  })
    .then(invoices => {
      res.status(200).json(invoices);
    })
    .catch(next);
});

router.get("/list/matter/:id", middleware.auth, (req, res, next) => {
  Invoice.findAll({
    where: { case_id: req.params.id, status: [0, 1] },
    attributes: ["id", "invoice_number", "total"]
  })
    .then(invoices => {
      res.status(200).json(invoices);
    })
    .catch(next);
});

router.get("/view/:id", middleware.auth, async (req, res, next) => {
  try {
    let invoice = await Invoice.findOne({
      where: { id: req.params.id },
      include: [
        { association: "items", include: [{ association: "costtype" }] },
        { association: "client" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });
    if (invoice == null) return res.json("Invoice not found");

    let result = await helper.generateInvoicePDF({
      invoice: invoice,
      user_id: invoice.user_id
    });
    res.json(result);
  } catch (err) {
    next(err);
  }
});

// router.post("/sentBulk", async (req, res, next) => {
//   try {
//     var user = [];
//     var invoices = [];
//     invoices = await Invoice.findAll({
//       where: { id: req.body },
//       include: [
//         { association: "items", include: [{ association: "costtype" }] },
//         { association: "client" },
//         // { association: "quote" },
//         { association: "matter" },
//         { association: "thirdparty" }
//       ]
//     });
//     for (let invoice of invoices) {
//       var created_date = moment(invoice.created_at).format("DD-MM-YYYY");
//       var items1 = [];
//       var total = [];
//       for (item of invoice.items) {
//         var itemdate = moment(item.created_at).format("DD-MM-YYYY");
//         //items1.push('<tr><td>d</td><td>d</td><td>d</td><td>d</td><td>d</td></tr>');
//         items1.push(
//           "<tr><td>" +
//             itemdate +
//             "</td><td>" +
//             item.costtype.name +
//             "</td><td>" +
//             item.description +
//             "</td><td>" +
//             item.quantity +
//             "</td><td>" +
//             item.price +
//             "</td></tr>"
//         );
//         total.push(
//           '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">R ' +
//             item.price +
//             '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">R ' +
//             Math.abs(item.price - item.total) +
//             '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">R ' +
//             item.total +
//             "</td></tr>"
//         );
//       }

//       let content = fs.readFileSync(
//         "./src/email-templates/crm/invoice_templatenew.html",
//         "utf8"
//       );
//       if (invoice.client) {
//         content = content.replace(
//           "**bank_details**",
//           invoice.client.bankdetails
//         );
//         content = content.replace("**phone_number**", invoice.client.contact1);
//         content = content.replace("**email**", invoice.client.email);
//         content = content.replace(
//           "**name**",
//           invoice.client.firstname + " " + invoice.client.surname
//         );
//       } else {
//         content = content.replace("**bank_details**", "-");
//         content = content.replace("**phone_number**", "-");
//         content = content.replace("**email**", "-");
//         content = content.replace("**name**", "-" + " " + "-");
//       }

//       if (invoice.thirdparty) {
//         content = content.replace(
//           "**third_parties**",
//           invoice.thirdparty.firstname + " " + invoice.thirdparty.surname
//         );
//       } else {
//         content = content.replace("**third_parties**", "-" + " " + "-");
//       }

//       content = content.replace("**notes**", invoice.message);
//       content = content.replace("**balance**", req.body.trust_balance);

//       content = content.replace("**number**", invoice.invoice_number);
//       content = content.replace("**quote_date**", created_date);
//       content = content.replace("**description**", invoice.message);
//       content = content.replace("**client_reference**", "");

//       content = content.replace("**item**", items1);
//       content = content.replace("**total**", total);

//       let u_invoice = await Invoice.findByPk(invoice.id);
//       if (u_invoice != null) u_invoice.update({ status: 1 });
//       //await Invoice.update({ status: 1 }, { where: { id: invoice.id } });

//       if (invoice.client) {
//         if ((invoice.client.type = "i")) {
//           let sentMail = await mail.sendWithAttachment(
//             invoice.client.email,
//             //"Dhivakarmm@gmail.com",
//             "TAX INVOICE",
//             content,
//             invoice.attachment,
//             next
//           );
//         } else if ((invoice.client.type = "c" && invoice.contact)) {
//           //var firstname = invoice.contact.replace(/ .*/, "");
//           //var client_id = invoice.client_id;

//           // let clientc = await ClientContact.findOne({
//           //   where: { client_id: client_id, firstname: firstname }
//           // });
//           let clientc = await ClientContact.findByPk(invoice.contact);
//           if (clientc) {
//             let sentMail = await mail.sendWithAttachment(
//               clientc.email,
//               //"Dhivakarmm@gmail.com",
//               "TAX INVOICE",
//               content,
//               invoice.attachment,
//               next
//             );
//           }
//         }
//       }

//       // console.log(invoice[i].client.type);
//       res.status(200).end();
//     }
//     //res.status(400).json({ invoices: invoices, invoiceID: req.body });
//     //res.status(200).json({ code: 200, message: "Invoice Sent Successfully" }).end();
//   } catch (error) {
//     next(error);
//   }
// });

router.post("/sentBulk", async (req, res, next) => {
  try {
    var user = [];
    var invoices = [];
    invoices = await Invoice.findAll({
      where: { id: req.body },
      include: [
        { association: "items", include: [{ association: "costtype" }] },
        { association: "client" },
        // { association: "quote" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });

    for (let invoice of invoices) {
      let firmusers = await common.firmUsers(invoice.user_id);

      for (let fu of firmusers) {
        let u = await User.findOne({
          where: { id: fu, parent_user_id: null },
          include: [{ association: "userdetail" }]
        });
        if (u !== null) {
          user.push(u);
        }
      }

      var currency = user[0].userdetail.currency;

      var cs = await getCurrencySymbol(currency);

      var currencySymbol = "";
      if (cs) {
        currencySymbol = cs;
      } else {
        currencySymbol = "R";
      }

      //res.status(200).json({ user: user, invoice: invoice });
      var created_date = moment(invoice.created_at).format("DD-MM-YYYY");
      var items1 = [];
      var total = [];
      for (item of invoice.items) {
        var itemdate = moment(item.created_at).format("DD-MM-YYYY");
        //items1.push('<tr><td>d</td><td>d</td><td>d</td><td>d</td><td>d</td></tr>');
        items1.push(
          "<tr><td>" +
            itemdate +
            "</td><td>" +
            item.costtype.name +
            "</td><td>" +
            item.description +
            "</td><td>" +
            item.quantity +
            "</td><td>" +
            item.price +
            "</td></tr>"
        );
        total.push(
          '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.price +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            Math.abs(item.price - item.total) +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.total +
            "</td></tr>"
        );
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/invoice_templatenew.html",
        "utf8"
      );
      if (invoice.client) {
        content = content.replace(
          "**bank_details**",
          invoice.client.bankdetails
        );
        content = content.replace("**phone_number**", invoice.client.contact1);
        content = content.replace("**email**", invoice.client.email);
        content = content.replace(
          "**name**",
          invoice.client.firstname + " " + invoice.client.surname
        );
      } else {
        content = content.replace("**bank_details**", "-");
        content = content.replace("**phone_number**", "-");
        content = content.replace("**email**", "-");
        content = content.replace("**name**", "-" + " " + "-");
      }

      if (invoice.thirdparty) {
        content = content.replace(
          "**third_parties**",
          invoice.thirdparty.firstname + " " + invoice.thirdparty.surname
        );
      } else {
        content = content.replace("**third_parties**", "-" + " " + "-");
      }
      if (user[0].userdetail.logo) {
        content = content.replace(
          "**logo**",
          "https://www.naartjielegal.com/api/" + user[0].userdetail.logo
        );
      } else {
        content = content.replace(
          "**logo**",
          "https://www.naartjielegal.com/assets/images/logo.png"
        );
      }

      var company_title = "";
      // if (invoice.client.type == 'i') {
      //     company_title = invoice.client.firstname + " " + invoice.client.surname;
      // } else {
      //     company_title = invoice.client.companyname
      // }

      if (user[0].userdetail.name_of_firm) {
        company_title = user[0].userdetail.name_of_firm;
      }
      content = content.replace("**company_title**", company_title);

      // content = content.replace(
      //   "**company_title**",
      //   user[0].userdetail.firstname + " " + user[0].userdetail.lastname
      // );
      content = content.replace(
        "**owner**",
        user[0].userdetail.firstname + " " + user[0].userdetail.lastname
      );
      content = content.replace("**owneraddress**", user[0].userdetail.address);
      content = content.replace("**owneremail**", user[0].email);
      content = content.replace("**phone**", user[0].userdetail.contact1);
      content = content.replace("**zip**", "");

      content = content.replace("**notes**", invoice.message);
      content = content.replace("**balance**", req.body.trust_balance);

      content = content.replace("**number**", invoice.invoice_number);
      content = content.replace("**quote_date**", created_date);
      content = content.replace("**description**", invoice.message);
      content = content.replace("**client_reference**", "");

      content = content.replace("**item**", items1);
      content = content.replace("**total**", total);

      let u_invoice = await Invoice.findByPk(invoice.id);
      if (u_invoice != null) u_invoice.update({ status: 1 });
      //await Invoice.update({ status: 1 }, { where: { id: invoice.id } });

      if (invoice.client) {
        if ((invoice.client.type = "i")) {
          let email = await Email.create({
            parent_id: invoice.id,
            client_id: invoice.client_id,
            matter_id: invoice.case_id,
            subject: "TAX Invoice",
            type: "invoice",
            user_id: invoice.user_id
          });
          let sentMail = await mail.sendWithAttachment(
            invoice.client.email,
            //"Dhivakarmm@gmail.com",
            "TAX INVOICE",
            content,
            invoice.attachment,
            next
          );
          console.log(sentMail);
        } else if ((invoice.client.type = "c" && invoice.contact)) {
          //var firstname = invoice.contact.replace(/ .*/, "");
          //var client_id = invoice.client_id;

          // let clientc = await ClientContact.findOne({
          //   where: { client_id: client_id, firstname: firstname }
          // });
          let clientc = await ClientContact.findByPk(invoice.contact);
          if (clientc) {
            let email = await Email.create({
              parent_id: invoice.id,
              client_id: clientc.id,
              matter_id: invoice.case_id,
              subject: "TAX Invoice",
              type: "invoice",
              user_id: invoice.user_id
            });
            let sentMail = await mail.sendWithAttachment(
              clientc.email,
              //"Dhivakarmm@gmail.com",
              "TAX INVOICE",
              content,
              invoice.attachment,
              next
            );
          }
        }
      }

      // console.log(invoice[i].client.type);
      res.status(200).end();
    }
    //res.status(400).json({ invoices: invoices, invoiceID: req.body });
    //res.status(200).json({ code: 200, message: "Invoice Sent Successfully" }).end();
  } catch (error) {
    next(error);
  }
});

router.get("/mycurrency", async (req, res, next) => {
  try {
    var ip =
      req.headers["x-forwarded-for"] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket ? req.connection.socket.remoteAddress : null);
    let location = await iplocation(ip);
    let cc = await getCurrencySymbolFromIso2(location.countryCode);
    res.status(200).json({ ip: ip, location: location, cc: cc });
  } catch (error) {
    next(error);
  }
});

router.post("/send-single", async (req, res, next) => {
  try {
    let invoice = await Invoice.findOne({
      where: { id: req.body.id },
      include: [
        { association: "items", include: [{ association: "costtype" }] },
        { association: "client" },
        // { association: "quote" },
        { association: "matter" },
        { association: "thirdparty" }
      ]
    });

    if (invoice != null) {
      let user = [];
      let firmusers = await common.firmUsers(invoice.user_id);

      for (let fu of firmusers) {
        let u = await User.findOne({
          where: { id: fu, parent_user_id: null },
          include: [{ association: "userdetail" }]
        });
        if (u !== null) {
          user.push(u);
        }
      }

      var currency = user[0].userdetail.currency;

      var cs = await getCurrencySymbol(currency);

      var currencySymbol = "";
      if (cs) {
        currencySymbol = cs;
      } else {
        currencySymbol = "R";
      }

      var created_date = moment(invoice.created_at).format("DD-MM-YYYY");
      var items1 = [];
      var total = [];
      for (item of invoice.items) {
        var itemdate = moment(item.created_at).format("DD-MM-YYYY");
        //items1.push('<tr><td>d</td><td>d</td><td>d</td><td>d</td><td>d</td></tr>');
        items1.push(
          "<tr><td>" +
            itemdate +
            "</td><td>" +
            item.costtype.name +
            "</td><td>" +
            item.description +
            "</td><td>" +
            item.quantity +
            "</td><td>" +
            item.price +
            "</td></tr>"
        );
        total.push(
          '<tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total Exclusive:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.price +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Total VAT:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            Math.abs(item.price - item.total) +
            '</td></tr><tr><td colspan="3">&nbsp;</td><td style="text-align: left; border: 1px solid gray;">Sub Total:</td><td style="text-align: left; border: 1px solid gray;">' +
            currencySymbol +
            " " +
            item.total +
            "</td></tr>"
        );
      }

      let content = fs.readFileSync(
        "./src/email-templates/crm/invoice_templatenew.html",
        "utf8"
      );
      if (invoice.client) {
        content = content.replace(
          "**bank_details**",
          invoice.client.bankdetails
        );
        content = content.replace("**phone_number**", invoice.client.contact1);
        content = content.replace("**email**", invoice.client.email);
        content = content.replace(
          "**name**",
          invoice.client.firstname + " " + invoice.client.surname
        );
      } else {
        content = content.replace("**bank_details**", "-");
        content = content.replace("**phone_number**", "-");
        content = content.replace("**email**", "-");
        content = content.replace("**name**", "-" + " " + "-");
      }

      if (invoice.thirdparty) {
        content = content.replace(
          "**third_parties**",
          invoice.thirdparty.firstname + " " + invoice.thirdparty.surname
        );
      } else {
        content = content.replace("**third_parties**", "-" + " " + "-");
      }
      if (user[0].userdetail.logo) {
        content = content.replace(
          "**logo**",
          "https://www.naartjielegal.com/api/" + user[0].userdetail.logo
        );
      } else {
        content = content.replace(
          "**logo**",
          "https://www.naartjielegal.com/assets/images/logo.png"
        );
      }

      var company_title = "";
      // if (invoice.client.type == 'i') {
      //     company_title = invoice.client.firstname + " " + invoice.client.surname;
      // } else {
      //     company_title = invoice.client.companyname
      // }

      if (user[0].userdetail.name_of_firm) {
        company_title = user[0].userdetail.name_of_firm;
      }
      content = content.replace("**company_title**", company_title);

      // content = content.replace(
      //   "**company_title**",
      //   user[0].userdetail.firstname + " " + user[0].userdetail.lastname
      // );
      content = content.replace(
        "**owner**",
        user[0].userdetail.firstname + " " + user[0].userdetail.lastname
      );
      content = content.replace("**owneraddress**", user[0].userdetail.address);
      content = content.replace("**owneremail**", user[0].email);
      content = content.replace("**phone**", user[0].userdetail.contact1);
      content = content.replace("**zip**", "");

      content = content.replace("**notes**", invoice.message);
      content = content.replace("**balance**", req.body.trust_balance);

      content = content.replace("**number**", invoice.invoice_number);
      content = content.replace("**quote_date**", created_date);
      content = content.replace("**description**", invoice.message);
      content = content.replace("**client_reference**", "");

      content = content.replace("**item**", items1);
      content = content.replace("**total**", total);

      // let u_invoice = await Invoice.findByPk(invoice.id);
      // if (u_invoice != null) u_invoice.update({ status: 1 });
      //await Invoice.update({ status: 1 }, { where: { id: invoice.id } });

      if (invoice.client) {
        if ((invoice.client.type = "i")) {
          let email = await Email.create({
            parent_id: invoice.id,
            client_id: invoice.client_id,
            matter_id: invoice.case_id,
            subject: "TAX Invoice",
            type: "invoice",
            user_id: invoice.user_id
          });
          let sentMail = await mail.sendWithAttachment(
            invoice.client.email,
            //"Dhivakarmm@gmail.com",
            "TAX INVOICE",
            content,
            invoice.attachment,
            next
          );
          console.log(sentMail);
        } else if ((invoice.client.type = "c" && invoice.contact)) {
          //var firstname = invoice.contact.replace(/ .*/, "");
          //var client_id = invoice.client_id;

          // let clientc = await ClientContact.findOne({
          //   where: { client_id: client_id, firstname: firstname }
          // });
          let clientc = await ClientContact.findByPk(invoice.contact);
          if (clientc) {
            let email = await Email.create({
              parent_id: invoice.id,
              client_id: clientc.id,
              matter_id: invoice.case_id,
              subject: "TAX Invoice",
              type: "invoice",
              user_id: invoice.user_id
            });
            let sentMail = await mail.sendWithAttachment(
              clientc.email,
              //"Dhivakarmm@gmail.com",
              "TAX INVOICE",
              content,
              invoice.attachment,
              next
            );
          }
        }
      }
      invoice.update({ status: 1 });
      return res.status(200).json({ invoice });
    }

    return res.status(404).json("Invoice not found");
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
