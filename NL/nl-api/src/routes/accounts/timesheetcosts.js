const express = require("express");
const router = express.Router();
const TimesheetCost = require("../../models/accounts/AttorneyTimesheetCost");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
let limit = 10;

const middleware = {
  validations: [
    check("attorney_id").isInt(),
    check("cost").isFloat(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.get("/attorney/id/:id", middleware.auth, (req, res, next) => {
  TimesheetCost.findOne({
    where: { attorney_id: req.params.id },
    include: [
      { association: "attorney", include: [{ association: "userdetail" }] }
    ]
  })
    .then(cost => {
      res.status(200).json(cost);
    })
    .catch(err => next(err));
});

router.post(
  "/attorney/id/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    TimesheetCost.findOne({ where: { attorney_id: req.params.id } })
      .then(cost => {
        if (cost == null) {
          TimesheetCost.create(req.body)
            .then(newcost => {
              res.status(201).json(newcost);
            })
            .catch(err => next(err));
        } else {
          cost.update({ cost: req.body.cost, code: req.body.code });
          res.status(200).json(cost);
        }
      })
      .catch(err => next(err));
  }
);

router.get("/list/user/:id", middleware.auth, (req, res, next) => {
  TimesheetCost.findAll({
    where: { user_id: req.params.id },
    attributes: ["id", "attorney_id", "cost"],
    include: [
      {
        association: "attorney",
        include: [
          {
            association: "userdetail",
            attributes: ["id", "firstname", "lastname"]
          }
        ],
        attributes: ["id"]
      }
    ]
  })
    .then(costs => {
      res.status(200).json(costs);
    })
    .catch(err => next(err));
});

module.exports = router;
