const express = require("express");
const router = express.Router();
const CostType = require("../../models/accounts/CostType");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const common = require("../../helpers/common");

let limit = 10;

const middleware = {
  validations: [
    check("name").isString(),
    check("code").isString(),
    check("cost").isFloat(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.get("/user/:userID", middleware.auth, async (req, res) => {
  let userIds = await common.firmUsers(req.params.userID);
  CostType.findAndCountAll({
    where: { user_id: userIds, isactive: 1 },
    order: ["name"]
  })
    .then(costtypes => {
      res.status(200).json(costtypes);
    })
    .catch(err => next(err));
});

router.get(
  "/paginate/user/:userID",
  middleware.auth,
  async (req, res, next) => {
    let page = req.query.page || 1;
    limit = parseInt(req.query.limit) || limit;
    let userIds = await common.firmUsers(req.params.userID);
    CostType.paginate({
      where: { user_id: userIds },
      page: page,
      paginate: limit,
      order: ["name"]
    })
      .then(costtypes => {
        res.status(200).json(costtypes);
      })
      .catch(err => next(err));
  }
);

router.post(
  "/",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    const NewCostType = {
      name: req.body.name,
      code: req.body.code,
      description: req.body.description,
      cost: req.body.cost,
      user_id: req.body.user_id
    };

    CostType.findOne({
      where: NewCostType
    }).then(costtype => {
      if (costtype == null) {
        CostType.create(NewCostType)
          .then(newcosttype => {
            res.status(201).json(newcosttype);
          })
          .catch(err => next(err));
      } else {
        res.status(400).json({
          error: "Cost Type is already created with same name"
        });
      }
    });
  }
);

router.get("/id/:id", middleware.auth, (req, res, next) => {
  CostType.findOne({ where: { id: req.params.id } })
    .then(costtype => {
      if (costtype != null) {
        res.status(200).json(costtype);
      } else {
        res.status(404).json({
          error: "Cost Type is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.put(
  "/id/:id",
  [
    middleware.auth,
    [
      check("name").isString(),
      check("code").isString(),
      check("cost").isFloat()
    ]
  ],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    CostType.findOne({
      where: { id: req.params.id }
    })
      .then(costtype => {
        if (costtype != null) {
          CostType.findOne({
            where: { code: req.body.code, $not: { id: req.params.id } }
          })
            .then(fcosttype => {
              if (fcosttype == null) {
                costtype.update({
                  name: req.body.name,
                  code: req.body.code,
                  description: req.body.description,
                  cost: req.body.cost
                });
                res.status(200).json(costtype);
              } else {
                res.status(400).json({
                  error: "Cost Type is already created with same name"
                });
              }
            })
            .catch(error => next(error));
        } else {
          res.status(404).json({
            error: "Cost Type is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch(
  "/id/:id/status",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    CostType.findOne({
      where: { id: req.params.id }
    })
      .then(costtype => {
        if (costtype != null) {
          costtype.update({ isactive: costtype.isactive ? 0 : 1 });
          res.status(200).json(costtype);
        } else {
          res.status(404).json({
            error: "Cost Type is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  CostType.findOne({
    where: { id: req.params.id }
  })
    .then(costtype => {
      if (costtype != null) {
        costtype.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Cost Type is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.patch("/id/:id/restore", middleware.auth, (req, res, next) => {
  CostType.findOne({
    where: { id: req.params.id },
    paranoid: false
  })
    .then(costtype => {
      if (costtype != null) {
        costtype.setDataValue("deleted_at", null);
        costtype.update({ deleted_at: null });
        res.status(200).json(costtype);
      } else {
        res.status(404).json({
          error: "Cost Type is not found"
        });
      }
    })
    .catch(err => next(err));
});

module.exports = router;
