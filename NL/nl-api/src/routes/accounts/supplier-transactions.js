const express = require("express");
const router = express.Router();
const sequelize = require("sequelize");
const SupTransaction = require("../../models/accounts/SupTransaction");
const SupMoneyTrust = require("../../models/accounts/SupMoneyTrust");
const SupMoneyAllocation = require("../../models/accounts/SupMoneyAllocation");
const SupInvoice = require("../../models/accounts/SupInvoice");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const SupplierContact = require("../../models/SupplierContacts");
const SupplierDetails = require("../../models/Supplier");
let limit = 10;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const common = require("../../helpers/common");
const Email = require("../../models/Email");

const middleware = {
  trust_validations: [
    check("supplier_id").isUUID(),
    check("amount").isFloat(),
    check("user_id").isInt(),
  ],
  allocation_validations: [
    check("supplier_id").isUUID(),
    check("invoice_id").isInt(),
    check("amount").isFloat(),
    check("user_id").isInt(),
  ],
  auth: checkToken,
};

router.post(
  "/deposit",
  [middleware.auth, middleware.trust_validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let supmontru = await SupMoneyTrust.create({
        supplier_id: req.body.supplier_id,
        amount: req.body.amount,
        description: req.body.description,
        status: req.body.status,
        user_id: req.body.user_id,
      });

      let suptran = await SupTransaction.create({
        trust_id: supmontru.id,
        supplier_id: req.body.supplier_id,
        type: "deposit",
        status: 1,
        user_id: req.body.user_id,
      });

      res.status(200).json({ supmontru, suptran });
    } catch (err) {
      next(err);
      console.log(err);
    }
  }
);
router.post(
  "/allocate",
  [middleware.auth, middleware.allocation_validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ error: errors.array() });
      }

      let invoice = {};

      invoice = await SupInvoice.findByPk(req.body.invoice_id);
      if (invoice == null) return res.status(404).json("Invoice not found");
      let total = invoice.total;
      let allocationTotal = 0.0;
      let allocations = await SupMoneyAllocation.findOne({
        where: {
          invoice_id: req.body.invoice_id,
          invoice_type: req.body.invoice_type,
        },
        attributes: [[sequelize.fn("sum", sequelize.col("amount")), "amount"]],
        group: ["invoice_id"],
      });
      if (allocations != null)
        allocationTotal = parseFloat(allocations.amount).toFixed(2);

      let NewTotalAllocation =
        parseFloat(allocationTotal) + parseFloat(req.body.amount);

      if (NewTotalAllocation > total)
        return res.status(400).json("Amount exceeded with Invoice total");

      let NewAllocation = await SupMoneyAllocation.create({
        supplier_id: req.body.supplier_id,
        invoice_id: req.body.invoice_id,
        invoice_type: req.body.invoice_type,
        amount: req.body.amount,
        description: req.body.description,
        status: req.body.status,
        user_id: req.body.user_id,
      });
      if (NewAllocation != null) {
        let transaction = await SupTransaction.create({
          supplier_id: req.body.supplier_id,
          allocate_id: NewAllocation.id,
          type: "credit",
          status: 1,
          user_id: req.body.user_id,
        });
        if (NewTotalAllocation == total) {
          invoice.update({ status: 2 });
        }
        res.status(200).json({ NewAllocation, transaction });
      }
    } catch (err) {
      next(err);
    }
  }
);

const trustBalance = async (supplier_id) => {
  let deposits = await SupTransaction.findAll({
    include: [
      {
        association: "trust",
        where: { supplier_id: supplier_id },
        attributes: ["amount"],
      },
    ],
    where: { type: "deposit" },
    attributes: [],
  });
  deposits = deposits.map((row) => row.trust.amount);
  let credits = await SupTransaction.findAll({
    include: [
      {
        association: "allocation",
        where: { supplier_id: supplier_id },
        attributes: ["amount"],
      },
    ],
    where: { type: "credit" },
    attributes: [],
  });
  credits = credits.map((row) => row.allocation.amount);
  let deposit_sum = 0;
  let allocation_sum = 0;
  if (deposits.length) {
    deposit_sum = deposits.reduce((a, b) => {
      return a + b;
    }, 0);
  }
  if (credits.length) {
    allocation_sum = credits.reduce((a, b) => {
      return a + b;
    }, 0);
  }

  return {
    deposit_sum,
    allocation_sum,
  };
};

router.get(
  "/trust-balance/supplier/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let { deposit_sum, allocation_sum } = await trustBalance(req.params.id);
      res.status(200).json({
        total_deposit: deposit_sum,
        total_alloc: allocation_sum,
        balance: deposit_sum - allocation_sum,
      });

      //res.status(200).json({ case_id: case_id, cases: cases });
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/allocated-money/supplier/:id",
  middleware.auth,
  async (req, res, next) => {
    let credits = await SupTransaction.findAll({
      include: [
        {
          association: "allocation",
          where: { supplier_id: req.params.id },
          attributes: ["amount"],
        },
      ],
      where: { type: "credit" },
      attributes: [],
    });
    credits = credits.map((row) => row.allocation.amount);
    let allocation_sum = 0;
    if (credits.length) {
      allocation_sum = credits.reduce((a, b) => {
        return a + b;
      }, 0);
    }
    res.status(200).json(allocation_sum);
  }
);

router.get(
  "/money-trusts/paginate/user/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let userIds = await common.firmUsers(req.params.id);
      let countData = await SupMoneyTrust.count({
        where: { user_id: userIds },
        group: ["supplier_id"],
        attributes: [sequelize.fn("sum", sequelize.col("amount"))],
      });
      limit = parseInt(req.query.limit) || limit;
      let page = req.query.page || 1;
      let pages = Math.ceil(countData.length / limit);
      offset = limit * (page - 1);
      const { from, to } = req.query;

      let cond = {};
      if (from && to) {
        cond = {
          user_id: userIds,
          created_at: { $between: [from, to] },
        };
      } else {
        cond = { user_id: userIds };
      }

      let current_date = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
      let current_month = moment(current_date)
        .startOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let current_end_date = moment(current_month)
        .endOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let previous_two_date = moment(current_month)
        .subtract(2, "months")
        .startOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let previous_two_end_date = moment(previous_two_date)
        .endOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let previous_date = moment(current_month)
        .subtract(1, "months")
        .startOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      let previous_end_date = moment(previous_date)
        .endOf("month")
        .format("YYYY-MM-DD HH:mm:ss");
      var date = {
        current_date: current_month,
        current_end_date: current_end_date,
        previous_two_date: previous_two_date,
        previous_two_end_date: previous_two_end_date,
        previous_date: previous_date,
        previous_end_date: previous_end_date,
      };

      let results = await SupMoneyTrust.findAll({
        where: cond,
        group: ["supplier_id"],
        attributes: [
          "supplier_id",
          [sequelize.fn("sum", sequelize.col("amount")), "amount"],
          //"created_at"
        ],
        include: [
          {
            association: "supplier",
            //attributes: ["id", "firstname", "surname"]
          },
        ],
        limit: limit,
        offset: offset,
      });

      var newresult = [];
      for (result of results) {
        let currentAmount = await SupMoneyTrust.findAll({
          where: {
            supplier_id: result.supplier_id,
            created_at: {
              $between: [current_month, current_end_date],
            },
          },
          group: ["supplier_id"],
          attributes: [
            "supplier_id",
            [sequelize.fn("sum", sequelize.col("amount")), "amount"],
            // "created_at"
          ],
        });

        let previousAmount = await SupMoneyTrust.findAll({
          where: {
            supplier_id: result.supplier_id,
            created_at: {
              $between: [previous_date, previous_end_date],
            },
          },
          group: ["supplier_id"],
          attributes: [
            "supplier_id",
            [sequelize.fn("sum", sequelize.col("amount")), "amount"],
            // "created_at"
          ],
        });

        let previous2Amount = await SupMoneyTrust.findAll({
          where: {
            supplier_id: result.supplier_id,
            created_at: {
              $between: [previous_two_date, previous_two_end_date],
            },
          },
          group: ["supplier_id"],
          attributes: [
            "supplier_id",
            [sequelize.fn("sum", sequelize.col("amount")), "amount"],
            // "created_at"
          ],
        });

        let allocations = await SupMoneyAllocation.findAll({
          where: {
            supplier_id: result.supplier_id,
          },
          attributes: ["supplier_id", "amount"],
        });

        let credit_amount = 0;
        for (let alloc of allocations) credit_amount += alloc.amount;

        let trust_balance = (result.amount || 0) - (credit_amount || 0);

        let filebalances = await SupMoneyTrust.findAll({
          where: {
            supplier_id: result.supplier_id,
          },
          attributes: [
            [sequelize.fn("sum", sequelize.col("amount")), "amount"],
          ],
        });

        newresult.push({
          amount: result.amount ? result.amount : 0,
          supplier: result.supplier,
          supplier_id: result.supplier_id,
          monthwise: {
            current: currentAmount[0],

            previous: previousAmount[0],

            beforepreviousMonth: previous2Amount[0],
          },
          trust_balance: trust_balance,
          matters: filebalances,
        });
      }
      res.status(200).json({
        pages: pages,
        total: countData.length,
        docs: newresult,
        cdate: current_date,
        pdate: previous_date,
        bpdate: previous_two_date,
        // amounts: newresult,
      });
    } catch (err) {
      next(err);
    }
  }
);

router.delete(
  "/deposit/supplier/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      const fdegdrsg = await SupMoneyTrust.findOne({
        where: { supplier_id: req.params.id },
      });

      if (fdegdrsg == null) return res.status(404).json("Deposit is not found");
      fdegdrsg.destroy();
      res.status(204).json({});
    } catch (err) {
      next(err);
    }
  }
);

router.get(
  "/deposits_balance/supplier/:id",
  middleware.auth,
  async (req, res, next) => {
    let deposits = await SupMoneyTrust.findAll({
      where: { supplier_id: req.params.id },
      attributes: ["id", "amount", "description"],
    });

    let transaction = await SupTransaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "trust",
          attributes: ["id", "amount", "description"],
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"],
        },
      ],
      where: { supplier_id: req.params.id },
      order: [["created_at", "ASC"]],
    });

    transaction = transaction.sort((a, b) => {
      return new Date(a.created_at) - new Date(b.created_at);
    });
    return res
      .status(200)
      .json({ deposits: deposits, transaction: transaction });
  }
);

router.get(
  "/money-allocations/paginate/user/:id",
  middleware.auth,
  async (req, res, next) => {
    let page = req.query.page || 1;
    limit = parseInt(req.query.limit) || limit;
    let userIds = await common.firmUsers(req.params.id);
    SupMoneyAllocation.paginate({
      where: { user_id: userIds },
      page: page,
      paginate: limit,
      include: [{ association: "supplier" }],
    })
      .then((results) => {
        res.status(200).json(results);
      })
      .catch((err) => next(err));
  }
);

router.post(
  "/statement/supplier/:id",
  middleware.auth,
  async (req, res, next) => {
    var whereStatement = {};
    if (req.body.fromDate) {
      let fromDate = moment(req.body.fromDate, "YYYY-MM-DD")
        .startOf("day")
        .format("YYYY-MM-DD HH:mm:ss");
      let toDate = moment(req.body.toDate, "YYYY-MM-DD")
        .endOf("day")
        .format("YYYY-MM-DD HH:mm:ss");
      (whereStatement.created_at = {
        [sequelize.Op.between]: [fromDate, toDate],
      }),
        (whereStatement.supplier_id = req.params.id);
    } else {
      whereStatement.supplier_id = req.params.id;
    }
    console.log(whereStatement);
    //res.status(200).json({ body: req.body, where: whereStatement });
    let transactions = await SupTransaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "trust",
          attributes: ["id", "amount", "description"],
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"],
        },
      ],
      where: whereStatement,
      order: [["created_at", "ASC"]],
    });

    // transactions = transactions.sort((a, b) => {
    //   return new Date(a.created_at) - new Date(b.created_at);
    // });
    return res.status(200).json(transactions);
  }
);

router.get("/deposits/supplier/:id", middleware.auth, (req, res, next) => {
  SupMoneyTrust.findAll({
    where: { supplier_id: req.params.id },
    attributes: ["id", "amount", "description"],
  })
    .then((deposits) => res.status(200).json(deposits))
    .catch(next);
});

router.get(
  "/allocation/invoice/id/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let { id } = req.params;

      let invoice = await SupInvoice.findByPk(id);

      let allocation = await SupMoneyAllocation.findOne({
        where: { invoice_id: id },
        attributes: [[sequelize.fn("sum", sequelize.col("amount")), "amount"]],
        group: ["invoice_id"],
      });

      if (
        invoice != null &&
        allocation != null &&
        allocation.amount != undefined
      ) {
        invoice.total -= allocation.amount;
      }

      return res.status(200).json({ allocation, invoice });
    } catch (err) {
      next(err);
    }
  }
);

router.get("/totalAmount/:userId", middleware.auth, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.userId);
    let TrustTotal = await SupMoneyTrust.findAll({
      where: { user_id: userIds },
      attributes: [
        "amount",
        [sequelize.fn("sum", sequelize.col("amount")), "TTotal"],
      ],
      //group: ['MoneyTrust.amount'],
      raw: true,
    });

    let MoneyTotal = await SupMoneyAllocation.findAll({
      where: { user_id: userIds },
      attributes: [
        "amount",
        [sequelize.fn("sum", sequelize.col("amount")), "MTotal"],
      ],
      raw: true,
    });

    let amount = TrustTotal[0].TTotal - MoneyTotal[0].MTotal;
    res.status(200).json({
      TrustTotal: TrustTotal,
      MoneyTotal: MoneyTotal,
      TotalAmount: amount,
    });
  } catch (err) {
    next(err);
  }
});

router.get(
  "/allocation-label/supplier/:id",
  middleware.auth,
  async (req, res, next) => {
    let supplierDetails = await SupplierDetails.findByPk(req.params.id, {
      attributes: ["id", "firstname", "surname"],
    });
    if (supplierDetails == null)
      return res.status(404).json("supplier is not found");

    let deposits = await SupTransaction.findAll({
      include: [
        {
          association: "trust",
          where: { supplier_id: req.params.id },
          attributes: ["amount"],
        },
      ],
      where: { type: "deposit" },
      attributes: [],
    });
    deposits = deposits.map((row) => row.trust.amount);
    let credits = await SupTransaction.findAll({
      include: [
        {
          association: "allocation",
          where: { supplier_id: req.params.id },
          attributes: ["amount"],
        },
      ],
      where: { type: "credit" },
      attributes: [],
    });
    credits = credits.map((row) => row.allocation.amount);
    let deposit_sum = 0;
    let allocation_sum = 0;
    if (deposits.length) {
      deposit_sum = deposits.reduce((a, b) => {
        return a + b;
      }, 0);
    }

    if (credits.length) {
      allocation_sum = credits.reduce((a, b) => {
        return a + b;
      }, 0);
    }

    res.status(200).json({
      supplier:
        supplierDetails.companyname ||
        supplierDetails.firstname + " " + supplierDetails.surname,
      trust: deposit_sum,
      avail: deposit_sum - allocation_sum,
    });
  }
);

router.post("/sentBulkTrust", async (req, res, next) => {
  try {
    var t = [];
    for (let tid of req.body) {
      let transaction = await SupMoneyTrust.findAll({
        where: { supplier_id: tid },
      });
      t.push(transaction);
    }

    for (let trust of t[0]) {
      //console.log(t.length);

      let suppliercontact = await SupplierContact.findOne({
        where: { supplier_id: trust.supplier_id },
      });

      let supplierdetails = await SupplierDetails.findOne({
        where: { id: trust.supplier_id },
      });

      var created_date = moment(trust.created_at).format("DD-MM-YYYY");
      let content = fs.readFileSync(
        "./src/email-templates/crm/supplier_money_transaction.html",
        "utf8"
      );
      content = content.replace(
        "**name**",
        supplierdetails.firstname + " " + supplierdetails.surname
      );

      content = content.replace("**amount**", trust.amount);
      content = content.replace("**description**", trust.description);
      content = content.replace("**date**", created_date);
      if ((supplierdetails.type = "i")) {
        // let email = await Email.create({
        //   parent_id: trust.id,
        //   client_id: supplierdetails.id,
        //   matter_id: casedetails.id,
        //   subject: "Transaction Details",
        //   type: "money_trust",
        //   user_id: trust.user_id,
        // });
        let sentMail = await mail.send(
          supplierdetails.email,
          "Transaction Details",
          content,
          next
        );
      } else if ((supplierdetails.type = "c")) {
        // let email = await Email.create({
        //   parent_id: trust.id,
        //   client_id: clientcontact.id,
        //   matter_id: casedetails.id,
        //   subject: "Transaction Details",
        //   type: "money_trust",
        //   user_id: trust.user_id,
        // });

        let sentMail = await mail.send(
          suppliercontact.email,
          "Transaction Details",
          content,
          next
        );
      }
    }

    res
      .status(200)
      .json({ code: 200, message: "Mail sent successfully", t: t });
  } catch (error) {
    next(error);
  }
});

router.post("/send-mail", middleware.auth, async (req, res, next) => {
  if (req.body.type == "all") {
    let deposits = await SupMoneyTrust.findAll({
      where: { supplier_id: req.body.supplier_id },
      attributes: ["id", "amount", "description"],
    });

    let transaction = await SupTransaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "trust",
          attributes: ["id", "amount", "description"],
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"],
        },
      ],
      where: { supplier_id: req.body.supplier_id },
      order: [["created_at", "ASC"]],
    });

    let supplierdetails = await SupplierDetails.findByPk(req.body.supplier_id);

    let content = fs.readFileSync(
      "./src/email-templates/crm/supplier_transactions.html",
      "utf8"
    );
    content = content.replace(
      "**name**",
      supplierdetails.firstname + " " + supplierdetails.surname
    );

    let transactionsHtml = "";
    let index = 1;
    let bal = 0.0;
    for (let t of transaction) {
      transactionsHtml += "<tr>";
      transactionsHtml += `<td>${index}</td>`;
      transactionsHtml += `<td>${moment(t.created_at).format(
        "DD-MM-YYYYY"
      )}</td>`;
      transactionsHtml += `<td>${
        (t.trust ? t.trust.description : "") ||
        (t.allocation ? t.allocation.description : "")
      }</td>`;
      transactionsHtml += `<td>${t.trust != null ? t.trust.amount : 0.0}</td>`;
      transactionsHtml += `<td>${
        t.allocation != null ? t.allocation.amount : 0.0
      }</td>`;
      if (t.type == "deposit") {
        console.log("yes");
        if (t.trust) {
          bal += t.trust.amount;
        }
      } else {
        if (t.allocation) {
          bal -= t.allocation.amount;
        }
      }
      transactionsHtml += `<td>${bal}</td>`;
      transactionsHtml += "</tr>";
      index += 1;
    }

    content = content.replace("**transactions**", transactionsHtml);

    // let email = await Email.create({
    //   client_id: clientdetails.id,
    //   subject: "Transactions",
    //   type: "all_transactions",
    //   user_id: clientdetails.user_id,
    // });

    let sentMail = await mail.send(
      supplierdetails.email,
      "Transactions",
      content,
      next
    );

    res
      .status(200)
      .json({ code: 200, message: "Mail sent successfully", t: transaction });
  } else {
    let transaction = await SupTransaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "trust",
          attributes: ["id", "amount", "description"],
        },
        {
          association: "allocation",
          attributes: ["id", "amount", "description"],
        },
      ],
      where: { supplier_id: req.body.supplier_id },
      order: [["created_at", "ASC"]],
    });

    let supplierDetails = await SupplierDetails.findByPk(req.body.supplier_id);
    let content = fs.readFileSync(
      "./src/email-templates/crm/supplier_transactions.html",
      "utf8"
    );
    content = content.replace(
      "**name**",
      supplierDetails.firstname + " " + supplierDetails.surname
    );

    let transactionsHtml = "";
    let index = 1;
    let bal = 0.0;
    for (let t of transaction) {
      transactionsHtml += "<tr>";
      transactionsHtml += `<td>${index}</td>`;
      transactionsHtml += `<td>${moment(t.created_at).format(
        "DD-MM-YYYYY"
      )}</td>`;
      transactionsHtml += `<td>${
        (t.trust ? t.trust.description : "") ||
        (t.allocation ? t.allocation.description : "")
      }</td>`;
      transactionsHtml += `<td>${t.trust ? t.trust.amount : 0.0}</td>`;
      transactionsHtml += `<td>${
        t.allocation ? t.allocation.amount : 0.0
      }</td>`;
      if (t.type == "deposit") {
        console.log("yes");
        if (t.trust) {
          bal += t.trust.amount;
        }
      } else {
        if (t.allocation) {
          bal -= t.allocation.amount;
        }
      }
      transactionsHtml += `<td>${bal}</td>`;
      transactionsHtml += "</tr>";
      index += 1;
    }

    content = content.replace("**transactions**", transactionsHtml);

    // let email = await Email.create({
    //   client_id: clientdetails.id,
    //   matter_id: req.body.case_id,
    //   subject: "Transactions",
    //   type: "case_transactions",
    //   user_id: clientdetails.user_id,
    // });

    let sentMail = await mail.send(
      supplierDetails.email,
      "Transactions",
      content,
      next
    );
    res
      .status(200)
      .json({ code: 200, message: "Mail sent successfully", t: transaction });
  }
});

module.exports = router;
