const express = require("express");
const router = express.Router();

const User = require("../../models/User");
const Blog = require("../../models/Blog");
const Payment = require("../../models/Payment");
const checkToken = require("../../middlewares/checkToken");

router.get("/totals", checkToken, async (req, res, next) => {
  try {
    let users = await User.findAll({
      where: { role_id: 2, parent_user_id: null },
    });
    let blogs = await Blog.findAll();
    let payments = await Payment.findAll({ where: { status: "completed" } });
    let total_cost = 0.0;

    if (payments.length) {
      for (let item of payments) {
        total_cost += item.amount;
      }
    }

    return res.status(200).json({
      total_users: users.length,
      total_blogs: blogs.length,
      total_cost: total_cost,
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
