const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const UserDetail = require("../../models/UserDetail");
const checkToken = require("../../middlewares/checkToken");

router.get("/paginate", checkToken, async (req, res, next) => {
  let users = await User.findAll({
    // include: [{
    //     model: Country,
    //     as: "country",
    // }],
    where: {
      role_id: 2,
      parent_user_id: null,
    },
    include: [
      {
        association: "userdetail",
        include: [{ association: "country" }],
      },
    ],
    order: [["created_at", "DESC"]],
  });

  return res.status(200).json({
    page: 1,
    data: users,
    total: users.length,
  });
});

router.post("/id/:id", async (req, res, next) => {
  User.findOne({
    where: { id: req.params.id },
  })
    .then((data) => {
      data.destroy();
      res.status(200).json({ code: 200, message: "User Deleted Successfully" });
    })
    .catch((err) => {
      res.status(500).json(err);
    });
});

router.post("/:id", async (req, res, next) => {
  User.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "userdetail",
      },
    ],
  })
    .then((data) => {
      res.status(200).json({
        code: 200,
        message: "User Details",
        data: data,
      });
    })
    .catch((err) => {
      res.status(500).json(err);
    });
});

module.exports = router;
