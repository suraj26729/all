const express = require("express");
const router = express.Router();
const multer = require("multer");
const Lead = require("../models/Leads");
const Client = require("../models/Client");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Helper = require("../helpers/dateTime");
const Notes = require("../models/Notes");
const Document = require("../models/LeadDocument");
const fs = require("fs");
const sequelize = require("sequelize");
const User = require("../models/User");
const moment = require("moment");
const mail = require("../config/mail");
const Category = require("../models/masters/Category");
const common = require("../helpers/common");
const Email = require("../models/Email");

// const ClientContact = require("../../models/ClientContact");
// const ClientDetails = require("../../models/Client");
// const CaseDetails = require("../../models/CaseDetails");

const middleware = {
  validations: [
    //check("type").isLength({ min: 1 }),
    check("lead_type").isString(),
    check("firstname").isString(),
    // check("category_id").isInt(),
    // check("consult_date").isString(),
    // check("consult_time").isString(),
    //check("assignee_id").isInt(),
    check("user_id").isInt(),
  ],
  document_validation: [
    check("lead_id").isInt(),
    check("name").isLength({ min: 1 }),
    check("type_id").isInt(),
    check("description").isLength({ min: 1 }),
    check("url").isLength({ min: 1 }),
  ],
  auth: checkToken,
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/leads/documents/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 4 }, // 4 MB
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "application/pdf" ||
      file.mimetype == "application/vnd.ms-excel" ||
      file.mimetype ==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
      file.mimetype == "application/msword" ||
      file.mimetype ==
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ) {
      cb(null, true);
    } else {
      cb(new Error("Only jpeg, jpg, png, Excel format files allowed!"), false);
    }
  },
});

const uploadDoc = upload.single("doc");

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      req.body.contact1 = req.body.contact1_code + "-" + req.body.contact1;
      req.body.contact2 = req.body.contact2_code + "-" + req.body.contact2;
      req.body.mobile = req.body.mobile_code + "-" + req.body.mobile;
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      if (req.body.consult_time != "" && req.body.consult_time != null) {
        req.body.consult_datetime = new Date(
          req.body.consult_date +
            " " +
            Helper.convertTime(req.body.consult_time) +
            ":00Z"
        );
      }

      let lead = await Lead.create(req.body);
      var consult_date = moment(lead.consult_date).format("DD-MM-YYYY");
      var consult_time = moment(lead.consult_datetime).format("HH:mm");
      let content = fs.readFileSync(
        "./src/email-templates/crm/lead.html",
        "utf8"
      );
      content = content.replace(
        "**name**",
        lead.firstname + " " + lead.surname
      );
      content = content.replace("**date**", consult_date);
      content = content.replace("**book**", lead.consultation_booked);
      content = content.replace("**time**", consult_time);

      let email = await Email.create({
        parent_id: lead.id,
        subject: "Confirmation Details",
        type: "new_lead",
        user_id: lead.user_id,
      });

      mail.send(lead.email, "Confirmation Details", content, next);

      res.status(201).json(lead);
    } catch (err) {
      console.log(err);
      next(err);
    }
  }
);

router.get(
  "/paginate/user/:userId",
  middleware.auth,
  async (req, res, next) => {
    let usersIds = await common.firmUsers(req.params.userId);
    let offset = 0;
    let limit = 999999;
    Lead.count({
      where: {
        user_id: usersIds,
      },
    }).then((count) => {
      limit = req.query.limit ? parseInt(req.query.limit) : limit;
      limit = 999999;
      let page = req.query.page ? req.query.page : 1; // page number
      let pages = Math.ceil(count / limit);
      offset = limit * (page - 1);
      let { from, to } = req.query;
      let cond = {};
      if (from && to) {
        to = moment(new Date(to)).add(1, "day").format("YYYY-MM-DD");
        cond = {
          user_id: usersIds,
          created_at: { $between: [from, to] },
        };
      } else {
        cond = { user_id: usersIds };
      }
      Lead.findAndCountAll({
        attributes: [
          "id",
          "uuid",
          "created_at",
          "consult_date",
          "firstname",
          "surname",
          "email",
          "address1",
          "location",
          "consultation_booked",
          "client_arrive",
          "converted",
          "status",
          "category_id",
          "contact1",
          "contact2",
          "assignee_id",
        ],
        include: [
          {
            association: "category",
          },
          {
            association: "tasks",
          },
          {
            association: "assignee",
            include: [{ association: "userdetail" }],
          },
        ],
        where: cond,
        order: [["created_at", "DESC"]],
        limit: limit,
        offset: offset,
      })
        .then((leads) => {
          leads["count"] = count;
          res.status(200).json({
            pages: pages,
            data: leads,
          });
        })
        .catch((err) => next(err));
    });
  }
);

router.get("/id/:id", middleware.auth, (req, res, next) => {
  Lead.findOne({
    where: { $or: [{ id: req.params.id }, { uuid: req.params.id }] },
    include: [{ association: "tasks", include: [{ association: "matter" }] }],
  })
    .then((lead) => {
      res.status(200).json(lead);
    })
    .catch((err) => next(err));
});

router.put(
  "/id/:id",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    req.body.contact1 = req.body.contact1_code + "-" + req.body.contact1;
    req.body.contact2 = req.body.contact2_code + "-" + req.body.contact2;
    req.body.mobile = req.body.mobile_code + "-" + req.body.mobile;

    Lead.findOne({ where: { id: req.params.id } })
      .then((lead) => {
        lead.update(req.body);
        lead.update({ consult_date: req.body.consult_date });
        res.status(200).json(lead);
      })
      .catch((err) => next(err));
  }
);

router.patch(
  "/id/:id/status",
  [middleware.auth, [check("status").isString()]],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    Lead.findOne({ where: { id: req.params.id } })
      .then((lead) => {
        lead.update({ status: req.body.status });
        res.status(200).json(lead);
      })
      .catch((err) => next(err));
  }
);

router.patch(
  "/id/:id/consultation-book",
  [middleware.auth, [check("consultation_booked").isString()]],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    Lead.findOne({ where: { id: req.params.id } })
      .then((lead) => {
        lead.update({ consultation_booked: req.body.consultation_booked });
        res.status(200).json(lead);
      })
      .catch((err) => next(err));
  }
);

router.patch(
  "/id/:id/client-arrive",
  [middleware.auth, [check("client_arrive").isString()]],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    Lead.findOne({ where: { id: req.params.id } })
      .then((lead) => {
        lead.update({ client_arrive: req.body.client_arrive });
        res.status(200).json(lead);
      })
      .catch((err) => next(err));
  }
);

router.patch(
  "/id/:id/conversion",
  [middleware.auth],
  async (req, res, next) => {
    Lead.findOne({ where: { id: req.params.id } })
      .then((lead) => {
        const NewClient = {
          companyname: lead.companyname || "",
          companyreg: lead.companyreg || "",
          companyvat: lead.companyvat || "",
          firstname: lead.firstname || "",
          surname: lead.surname || "",
          email: lead.email || "",
          creditlimits: "",
          contact1: lead.contact1 || "",
          contact2: lead.contact2 || "",
          mobile: lead.mobile || "",
          dob: lead.dob || "",
          bankdetails: "",
          personaldetails: "",
          address1: lead.address1 || "",
          address2: lead.address2 || "",
          city: lead.city || "",
          zipcode: lead.zipcode || "",
          country: lead.country || "",
          province: lead.province || "",
          type: lead.type || "",
          picturepath: "",
          isactive: 1,
          user_id: lead.user_id || "",
        };
        Client.create(NewClient)
          .then((client) => {
            lead.update({ converted: "yes", status: "converted" });
            res.status(200).json(client);
          })
          .catch((err) => next(err));
      })
      .catch((err) => next(err));
  }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
  Lead.findOne({ where: { id: req.params.id } })
    .then((lead) => {
      if (lead != null) {
        lead.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "Lead is not found",
        });
      }
    })
    .catch((err) => next(err));
});

//Notes
router.get("/notes/:parentID", middleware.auth, async (req, res, next) => {
  // let lead = await Lead.findOne({
  //   where: { uuid: req.params.parentID },
  //   attributes: ["user_id"]
  // });
  // let userIds = await common.firmUsers(lead.user_id);
  // let leads = await Lead.findAll({
  //   where: { user_id: userIds },
  //   attributes: ["id"]
  // });
  // let leadIds = leads.map(item => item.id);
  // let notes = [];
  // if (leadIds.length) {
  notes = await Notes.findAll({
    where: { parent_id: req.params.parentID, type: "lead" },
    order: [["created_at", "DESC"]],
  });
  // }
  res.status(200).json(notes);
});

//Documents
router.post("/document/upload", uploadDoc, middleware.auth, (req, res) => {
  uploadDoc(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(200).json({ error: err });
    } else {
      return res.status(201).json(req.file);
    }
  });
});

router.post(
  "/document",
  [middleware.document_validation, middleware.auth],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Document.create(req.body)
      .then((document) => {
        res.status(201).json(document);
      })
      .catch((err) => res.status(500).json({ error: err }));
  }
);

router.get(
  "/documents/paginate/lead/:id",
  middleware.auth,
  async (req, res, next) => {
    let limit = parseInt(req.query.limit) || 15;
    let page = parseInt(req.query.page) || 1;
    Lead.findOne({ where: { uuid: req.params.id } })
      .then((lead) => {
        if (lead == null) return res.status(404).json("Lead not found!");
        Document.paginate({
          paginate: limit,
          page: page,
          where: { lead_id: lead.id },
          include: [{ association: "type" }, { association: "lead" }],
        })
          .then((doctypes) => {
            res.status(200).json(doctypes);
          })
          .catch((err) => next(err));
      })
      .catch(next);
  }
);

router.delete("/document/id/:id", middleware.auth, (req, res, next) => {
  Document.findOne({ where: { id: req.params.id } })
    .then((doc) => {
      fs.unlink(doc.url);
      doc.destroy();
      res.status(204).json({});
    })
    .catch((error) => next(error));
});

//Consult Report
router.post(
  "/consult-report/user/:id",
  middleware.auth,
  async (req, res, next) => {
    let assignees_ids = req.body.assignees || [];
    let start_date = req.body.start_date || null;
    let end_date = req.body.end_date || null;

    if (start_date != null) {
      start_date = moment(start_date, "YYYY-MM-DD")
        .startOf("day")
        .format("YYYY-MM-DD H:mm:ss");
    }

    if (end_date != null) {
      let _end_date = new Date(end_date);
      //_end_date.setDate(_end_date.getDate() + 1);
      end_date = moment(end_date, "YYYY-MM-DD")
        .endOf("day")
        .format("YYYY-MM-DD H:mm:ss");
    }

    let userId = await common.firmUsers(req.params.id);

    let params = [{ user_id: userId }];

    if (assignees_ids.length) params.push({ assignee_id: assignees_ids });
    if (start_date != null && end_date != null)
      params.push({ created_at: { $between: [start_date, end_date] } });
    if (start_date != null && end_date == null)
      params.push({ created_at: { $gte: start_date } });
    if (start_date == null && end_date != null)
      params.push({ created_at: { $lte: end_date } });

    var conditions = params.reduce((a, b) => Object.assign(a, b), {});

    let locations = await Lead.findAll({
      where: conditions,
      attributes: [
        [sequelize.fn("DISTINCT", sequelize.col("location")), "location"],
      ],
    });
    locations = locations.filter((row) => row.location != "");
    locations = locations.map((row) => row.location);

    let assignees = await Lead.findAll({
      where: conditions,
      attributes: [
        [sequelize.fn("DISTINCT", sequelize.col("assignee_id")), "assignee_id"],
      ],
    });
    assignees = assignees.filter((row) => row.assignee_id != "");
    assignees = assignees.map((row) => row.assignee_id);

    let data = [];
    let assigneeData = [];
    let count = 0;
    for (let assignee_id of assignees) {
      data = [];
      let assigneeDB = await User.findByPk(assignee_id, {
        include: [{ association: "userdetail" }],
      });
      for (let location of locations) {
        let results = await Lead.findAll({
          where: {
            user_id: userId,
            location: location,
            assignee_id: assignee_id,
          },
          attributes: ["id"],
          include: [
            {
              association: "assignee",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"],
                },
              ],
            },
          ],
        });
        data.push(results.length);
      }
      if (assigneeDB) {
        assigneeData.push({
          data: data,
          label:
            assigneeDB.userdetail.firstname +
            " " +
            assigneeDB.userdetail.lastname,
        });
      }
    }

    res.status(200).json({ labels: locations, data: assigneeData });
  }
);

router.get(
  "/consult-report/user/:id/assignee-count/:assigneeId",
  middleware.auth,
  async (req, res, next) => {
    let userIds = await common.firmUsers(req.params.id);
    let leads = await Lead.findAll({
      where: { user_id: userIds, assignee_id: req.params.assigneeId },
      attributes: ["id"],
      include: [
        { association: "assignee", include: [{ association: "userdetail" }] },
      ],
    });
    let assignee = {};
    assignee = leads.length ? leads[0].assignee.userdetail : {};
    return res.status(200).json({ count: leads.length, assignee: assignee });
  }
);

router.post(
  "/converted-report/user/:id",
  middleware.auth,
  async (req, res, next) => {
    let assignees_ids = req.body.assignees || [];
    let start_date = req.body.start_date || null;
    let end_date = req.body.end_date || null;
    let userIds = await common.firmUsers(req.params.id);

    if (start_date != null) {
      start_date = moment(start_date, "YYYY-MM-DD")
        .startOf("day")
        .format("YYYY-MM-DD H:mm:ss");
    }

    if (end_date != null) {
      let _end_date = new Date(end_date);
      //_end_date.setDate(_end_date.getDate() + 1);
      end_date = moment(end_date, "YYYY-MM-DD")
        .endOf("day")
        .format("YYYY-MM-DD H:mm:ss");
    }

    let params = [{ user_id: userIds, status: "converted" }];

    if (assignees_ids.length) params.push({ assignee_id: assignees_ids });
    if (start_date != null && end_date != null)
      params.push({ created_at: { $between: [start_date, end_date] } });
    if (start_date != null && end_date == null)
      params.push({ created_at: { $gte: start_date } });
    if (start_date == null && end_date != null)
      params.push({ created_at: { $lte: end_date } });

    var conditions = params.reduce((a, b) => Object.assign(a, b), {});

    let category_ids = await Lead.findAll({
      where: conditions,
      attributes: ["category_id"],
      include: [{ association: "category", attributes: ["name"] }],
    });
    let category_names = category_ids.map((row) => row.category.name);
    category_names = ["", ...category_names];
    category_ids = category_ids.filter((row) => row.category_id != "");
    category_ids = category_ids.map((row) => row.category_id);
    category_ids = category_ids.filter(
      (item, pos) => category_ids.indexOf(item) == pos
    );

    let data = [];
    for (let category_id of category_ids) {
      let count = await Lead.count({
        where: {
          user_id: userIds,
          status: "converted",
          category_id: category_id,
        },
      });
      data.push(count);
    }

    data = ["", ...data];

    res
      .status(200)
      .json({ labels: category_names, data: [{ data: data, label: "Total" }] });
  }
);

router.get(
  "/created-report/user/:id",
  middleware.auth,
  async (req, res, next) => {
    let from_date = req.query.from_date;
    let to_date = req.query.to_date;

    let assignees = req.query.assignees || [];
    if (assignees.length) assignees = assignees.split(",");

    let userIds = await common.firmUsers(req.params.id);
    // let user = await User.findByPk(req.params.id);
    // if (user == null) return res.status(404).json("User is not found");
    // let parent_user_id = user.id;
    // let attorneys = await User.findAll({
    //   where: { parent_user_id: parent_user_id, role_id: 3 },
    //   attributes: ["id"]
    // });
    // userIds = attorneys.map(item => item.id);
    // userIds.push(user.id);

    let startDate = moment(from_date || new Date()).startOf("month");
    if (!from_date) {
      from_date = moment(new Date())
        .subtract(5, "months")
        .startOf("month")
        .format("YYYY-MM-DD H:mm:ss");
      startDate = moment(new Date()).subtract(5, "months").startOf("month");
    }

    if (!to_date) {
      to_date = moment(new Date()).endOf("month").format("YYYY-MM-DD H:mm:ss");
    } else {
      to_date = moment(to_date).endOf("month").format("YYYY-MM-DD H:mm:ss");
    }

    let labels = [];
    while (startDate.isBefore(to_date)) {
      labels.push(startDate.format("MMMM, YYYY"));
      startDate = startDate.add(1, "month");
    }

    let data = [];
    for (let label of labels) {
      from_date = moment(label).startOf("month").format("YYYY-MM-DD H:mm:ss");
      to_date = moment(label).endOf("month").format("YYYY-MM-DD H:mm:ss");

      let params = [
        {
          user_id: userIds,
          created_at: {
            $between: [from_date, to_date],
          },
        },
      ];

      if (assignees.length) {
        params.push({ assignee_id: assignees });
      }

      var conditions = params.reduce((a, b) => Object.assign(a, b), {});
      let count = await Lead.count({
        where: conditions,
      });
      data.push(count);
    }

    labels = ["", ...labels];
    data = ["", ...data];

    res
      .status(200)
      .json({ labels: labels, data: [{ data: data, label: "Total" }] });
  }
);

router.get(
  "/matter-report/user/:id",
  middleware.auth,
  async (req, res, next) => {
    let from_date = req.query.from_date;
    let to_date = req.query.to_date;

    let assignees = req.query.assignees || [];
    if (assignees.length) assignees = assignees.split(",");

    let userIds = await common.firmUsers(req.params.id);
    // let user = await User.findByPk(req.params.id);
    // if (user == null) return res.status(404).json("User is not found");
    // let parent_user_id = user.id;
    // let attorneys = await User.findAll({
    //   where: { parent_user_id: parent_user_id, role_id: 3 },
    //   attributes: ["id"]
    // });
    // userIds = attorneys.map(item => item.id);
    // userIds.push(user.id);

    if (!from_date) {
      from_date = moment(new Date())
        .subtract(5, "months")
        .startOf("month")
        .format("YYYY-MM-DD H:mm:ss");
    }

    if (!to_date) {
      to_date = moment(new Date()).endOf("month").format("YYYY-MM-DD H:mm:ss");
    }

    let params = [
      {
        user_id: userIds,
        category_id: { $ne: null },
        created_at: { $between: [from_date, to_date] },
      },
    ];

    if (assignees.length) {
      params.push({ assignee_id: assignees });
    }

    var conditions = params.reduce((a, b) => Object.assign(a, b), {});

    let result = await Lead.findAll({
      where: conditions,
      attributes: [
        "category_id",
        [sequelize.fn("COUNT", sequelize.col("id")), "count"],
      ],
      group: ["category_id"],
      raw: true,
    });

    let category_ids = result.map((item) => item.category_id);
    let labels = [];
    if (category_ids.length) {
      labels = await Category.findAll({
        where: { id: category_ids },
        attributes: ["name"],
      });
      labels = labels.map((item) => item.name);
    }

    let data = [];
    data = result.map((item) => item.count);

    labels = ["", ...labels];
    data = ["", ...data];

    res
      .status(200)
      .json({ labels: labels, data: [{ data: data, label: "Total" }] });
  }
);

router.get(
  "/matter-converted-report/user/:id",
  middleware.auth,
  async (req, res, next) => {
    let from_date = req.query.from_date;
    let to_date = req.query.to_date;

    let assignees = req.query.assignees || [];
    if (assignees.length) assignees = assignees.split(",");

    let userIds = await common.firmUsers(req.params.id);
    // let user = await User.findByPk(req.params.id);
    // if (user == null) return res.status(404).json("User is not found");
    // let parent_user_id = user.id;
    // let attorneys = await User.findAll({
    //   where: { parent_user_id: parent_user_id, role_id: 3 },
    //   attributes: ["id"]
    // });
    // userIds = attorneys.map(item => item.id);
    // userIds.push(user.id);

    if (!from_date) {
      from_date = moment(from_date)
        .subtract(5, "months")
        .startOf("month")
        .format("YYYY-MM-DD H:mm:ss");
    }

    if (!to_date) {
      to_date = moment(to_date).endOf("month").format("YYYY-MM-DD H:mm:ss");
    }

    let params = [
      {
        user_id: userIds,
        category_id: { $ne: null },
        created_at: { $between: [from_date, to_date] },
        status: "converted",
      },
    ];

    if (assignees.length) {
      params.push({ assignee_id: assignees });
    }

    var conditions = params.reduce((a, b) => Object.assign(a, b), {});

    let result = await Lead.findAll({
      where: conditions,
      attributes: [
        "category_id",
        [sequelize.fn("COUNT", sequelize.col("id")), "count"],
      ],
      group: ["category_id"],
      raw: true,
    });

    let category_ids = result.map((item) => item.category_id);
    let labels = [];
    if (category_ids.length) {
      labels = await Category.findAll({
        where: { id: category_ids },
        attributes: ["name"],
      });
      labels = labels.map((item) => item.name);
    }

    let data = [];
    data = result.map((item) => item.count);

    labels = ["", ...labels];
    data = ["", ...data];

    res
      .status(200)
      .json({ labels: labels, data: [{ data: data, label: "Total" }] });
  }
);

module.exports = router;
