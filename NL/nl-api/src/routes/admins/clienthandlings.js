const express = require("express");
const router = express.Router();
const Matter = require("../../models/CaseDetails");
const Client = require("../../models/Client");
const checkToken = require("../../middlewares/checkToken");

const middleware = {
  auth: checkToken
};

let limit = 10;

router.get("/:adminID", middleware.auth, async (req, res) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;

  Client.paginate({
    where: { user_id: req.params.adminID },
    attributes: ["id", "firstname", "surname", "email", "address1", "isactive"],
    page: page,
    paginate: limit
  })
    .then(clients => {
      res.status(200).json(clients);
    })
    .catch(err => res.status(500).json({ error: err }));
});

module.exports = router;
