const express = require("express");
const router = express.Router();
const multer = require("multer");
const User = require("../../models/User");
const UserDetail = require("../../models/UserDetail");
const bcrypt = require("bcryptjs");
const env = require("../../../env");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const Op = sequelize.Op;
const Common = require("../../helpers/common");
const UserPermission = require("../../models/UserPermission");
const mail = require("../../config/mail");
const fs = require("fs");

const middleware = {
  validations: [
    check("firstname").isString(),
    check("username").isString(),
    check("password").isString(),
    check("email").isEmail(),
    //check("contact1").isMobilePhone("any"),
    //check("address").isString(),
    //check("yearofpractice").isInt(),
    check("user_id").isInt(),
  ],
  auth: checkToken,
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/admins/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 4 }, // 4 MB
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    ) {
      cb(null, true);
    } else {
      cb(new Error("Only jpeg, jpg, png, Excel format files allowed!"), false);
    }
  },
});

const uploadImage = upload.single("profile_img");

router.post("/uploadImage", uploadImage, middleware.auth, (req, res, next) => {
  uploadImage(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return next(err);
    } else {
      return res.status(201).json(req.file);
    }
  });
});

const storage2 = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/admins/logos");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload2 = multer({
  storage: storage2,
  limits: { fileSize: 500000 },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    )
      cb(null, true);
    else {
      cb(new Error("Only jpeg, jpg, png format images allowed!"), false);
    }
  },
});

const uploadLogo = upload2.single("logo");

router.post("/uploadLogo", uploadLogo, middleware.auth, (req, res, next) => {
  uploadLogo(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return next(err);
    } else {
      return res.status(201).json(req.file);
    }
  });
});

const storage3 = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/admins/signs");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload3 = multer({
  storage: storage3,
  limits: { fileSize: 4194304 },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    )
      cb(null, true);
    else {
      cb(new Error("Only jpeg, jpg, png format images allowed!"), false);
    }
  },
});

const uploadSign = upload3.single("sign");

router.post("/uploadSign", uploadSign, middleware.auth, (req, res, next) => {
  uploadSign(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return next(err);
    } else {
      return res.status(201).json(req.file);
    }
  });
});

let limit = 15;

router.get("/", middleware.auth, (req, res, next) => {
  let offset = 0;
  User.count({
    where: {
      role_id: 2,
      isactive: 1,
    },
  }).then((count) => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    User.findAndCountAll({
      include: [
        {
          association: "userdetail",
        },
      ],
      where: {
        role_id: 2,
        isactive: 1,
      },
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
    })
      .then((admins) => {
        admins["count"] = count;
        res.status(200).json({
          pages: pages,
          data: admins,
        });
      })
      .catch((err) => next(err));
  });
});

router.get("/user/:userId", middleware.auth, (req, res, next) => {
  let offset = 0;
  User.count({
    where: {
      role_id: [1, 2],
      parent_user_id: req.params.userId,
      isactive: 1,
    },
  }).then((count) => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    User.findAndCountAll({
      include: [
        {
          association: "userdetail",
        },
      ],
      where: {
        role_id: [1, 2],
        parent_user_id: req.params.userId,
        isactive: 1,
      },
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
    })
      .then((admins) => {
        User.findAndCountAll({
          include: [
            {
              association: "userdetail",
            },
          ],
          where: {
            id: req.params.userId,
            isactive: 1,
            parent_user_id: null,
            isadmin: 1,
          },
          order: [["created_at", "DESC"]],
        })
          .then((currentUser) => {
            //admins.rows.push(currentUser.rows[0]);
            admins["count"] = count;

            res.status(200).json({
              pages: pages,
              data: admins,
              currentUser: currentUser,
            });
          })
          .catch((err) => res.status(500).json(err));
      })
      .catch((err) => next(err));
  });
});

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      let userIds = await Common.firmUsers(req.body.user_id);

      let user = await User.findOne({
        where: { username: req.body.username, id: userIds },
      });
      if (user != null) {
        return res.status(400).json({
          error: "User is already registered with given username",
        });
      }
      user = await User.findOne({
        where: { email: req.body.email, id: userIds },
      });
      if (user != null) {
        return res.status(400).json({
          error: "User is already registered with given email address",
        });
      }
      let hash = await bcrypt.hash(req.body.password, env.SALTROUNDS);
      const NewUser = {
        username: req.body.username,
        password: hash,
        email: req.body.email,
        role_id: 2,
        parent_user_id: req.body.user_id,
        isactive: 1,
        userdetail: {
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          contact1: req.body.contact1_code + "-" + req.body.contact1,
          contact2: req.body.contact2_code + "-" + req.body.contact2,
          address: req.body.address,
          yearofpractice: req.body.yearofpractice,
          picturepath: req.body.picturepath ? req.body.picturepath : "",
        },
      };
      let result = await User.create(NewUser, {
        include: [{ model: UserDetail, as: "userdetail" }],
      });
      if (result) {
        let content = fs.readFileSync(
          "./src/email-templates/crm/new-admin.html",
          "utf8"
        );
        content = content.replace(
          "**name**",
          req.body.firstname + " " + req.body.lastname
        );
        content = content.replace("**firstname**", req.body.firstname);
        content = content.replace("**lastname**", req.body.lastname);
        content = content.replace("**username**", req.body.username);
        content = content.replace("**email**", req.body.email);
        content = content.replace("**password**", req.body.password);
        let maild = await mail.send(
          req.body.email,
          "Account Created",
          content,
          next
        );
      }
      return res.status(201).json(result);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get("/archive/user/:userId", middleware.auth, (req, res, next) => {
  let offset = 0;
  User.count({
    where: {
      role_id: 2,
      parent_user_id: req.params.userId,
      //deleted_at: { [Op.not]: "" }
      isactive: 0,
    },
  }).then((count) => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    User.findAndCountAll({
      include: [
        {
          association: "userdetail",
        },
      ],
      where: {
        role_id: 2,
        parent_user_id: req.params.userId,
        //deleted_at: { [Op.not]: "" }
        isactive: 0,
      },
      limit: limit,
      offset: offset,
    })
      .then((admins) => {
        res.status(200).json({
          pages: pages,
          data: admins,
        });
      })
      .catch((err) => next(err));
  });
});

router.get("/:id", middleware.auth, async (req, res, next) => {
  let user = await User.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "userdetail",
      },
    ],
  });
  if (user.parent_user_id != null) {
    let firmUserIds = await Common.firmUsers(user.id);
    let admin = await User.findOne({
      where: { id: firmUserIds, role_id: 2, parent_user_id: null },
      include: [
        {
          association: "userdetail",
        },
      ],
    });
    user["userdetail"]["logo"] = admin["userdetail"]["logo"];
  }
  res.status(200).json(user);
});

router.patch(
  "/:id/status",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    User.findOne({
      where: { id: req.params.id },
      paranoid: false,
    })
      .then((user) => {
        if (user != null) {
          user.update({ isactive: req.body.status, archive: !req.body.status });
          if (req.body.status == 1) user.restore();
          user.setDataValue("deleted_at", null);
          user.update({ archive_date: null });
          if (req.body.status == 0) user.update({ archive_date: new Date() });
          res.status(200).json(user);
        } else {
          res.status(404).json({
            error: "User is not found",
          });
        }
      })
      .catch((err) => next(err));
  }
);

router.put("/:id", middleware.auth, (req, res, next) => {
  User.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "userdetail",
      },
    ],
  })
    .then((user) => {
      if (user != null) {
        if (user.email.toLowerCase() == req.body.email.toLowerCase()) {
          if (req.body.password != null && req.body.password != "") {
            bcrypt.hash(req.body.password, env.SALTROUNDS, (err, hash) => {
              if (err) {
                next(err);
              } else {
                const UpdatedAttorney = {
                  password: hash,
                  username: req.body.username,
                  userdetail: {
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    contact1: req.body.contact1_code + "-" + req.body.contact1,
                    contact2: req.body.contact2_code + "-" + req.body.contact2,
                    address: req.body.address,
                    country_code: req.body.country_code || null,
                    currency: req.body.currency || null,
                    yearofpractice: req.body.yearofpractice,
                    picturepath: req.body.picturepath
                      ? req.body.picturepath
                      : user.picturepath,
                    logo: req.body.logo ? req.body.logo : user.logo,
                    signature: req.body.sign ? req.body.sign : user.sign,
                    name_of_firm: req.body.name_of_firm || null,
                  },
                };
                req.body.isadmin = req.body.isadmin == true ? 1 : 0;
                req.body.isattorney = req.body.isattorney == true ? 1 : 0;

                user.update({
                  password: UpdatedAttorney.password,
                  isadmin: req.body.isadmin,
                  isattorney: req.body.isattorney,
                });
                user.userdetail.update(UpdatedAttorney.userdetail);
                res.status(200).json({
                  user: user,
                  isadmin: req.body.isadmin,
                  isattorney: req.body.isattorney,
                });
              }
            });
          } else {
            const UpdatedAttorney = {
              userdetail: {
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                contact1: req.body.contact1_code + "-" + req.body.contact1,
                contact2: req.body.contact2_code + "-" + req.body.contact2,
                address: req.body.address,
                country_code: req.body.country_code || null,
                currency: req.body.currency || null,
                yearofpractice: req.body.yearofpractice,
                picturepath: req.body.picturepath
                  ? req.body.picturepath
                  : user.picturepath,
                logo: req.body.logo ? req.body.logo : user.logo,
                signature: req.body.sign ? req.body.sign : user.sign,
                name_of_firm: req.body.name_of_firm || null,
              },
            };
            req.body.isadmin = req.body.isadmin == true ? 1 : 0;
            req.body.isattorney = req.body.isattorney == true ? 1 : 0;

            user.update({
              username: req.body.username,
              isadmin: req.body.isadmin,
              isattorney: req.body.isattorney,
            });
            user.userdetail.update(UpdatedAttorney.userdetail);
            res.status(200).json({
              user: user,
              isadmin: req.body.isadmin,
              isattorney: req.body.isattorney,
            });
          }
        } else {
          User.findOne({
            where: { email: req.body.email },
            include: [
              {
                association: "userdetail",
              },
            ],
          })
            .then((user) => {
              if (user == null) {
                if (req.body.password != null && req.body.password != "") {
                  bcrypt.hash(
                    req.body.password,
                    env.SALTROUNDS,
                    (err, hash) => {
                      if (err) {
                        next(err);
                      } else {
                        const UpdatedAttorney = {
                          password: hash,
                          email: req.body.email,
                          userdetail: {
                            firstname: req.body.firstname,
                            lastname: req.body.lastname,
                            contact1:
                              req.body.contact1_code + "-" + req.body.contact1,
                            contact2:
                              req.body.contact2_code + "-" + req.body.contact2,
                            address: req.body.address,
                            country_code: req.body.country_code || null,
                            currency: req.body.currency || null,
                            yearofpractice: req.body.yearofpractice,
                            picturepath: req.body.picturepath
                              ? req.body.picturepath
                              : user.picturepath,
                            logo: req.body.logo ? req.body.logo : user.logo,
                            signature: req.body.sign
                              ? req.body.sign
                              : user.sign,
                            name_of_firm: req.body.name_of_firm || null,
                          },
                        };
                        user.update({
                          password: UpdatedAttorney.password,
                          email: UpdatedAttorney.email,
                        });
                        user.userdetail.update(UpdatedAttorney.userdetail);
                        res.status(200).json(user);
                      }
                    }
                  );
                } else {
                  const UpdatedAttorney = {
                    username: req.body.username,
                    email: req.body.email,
                    userdetail: {
                      firstname: req.body.firstname,
                      lastname: req.body.lastname,
                      contact1:
                        req.body.contact1_code + "-" + req.body.contact1,
                      contact2:
                        req.body.contact2_code + "-" + req.body.contact2,
                      address: req.body.address,
                      country_code: req.body.country_code || null,
                      currency: req.body.currency || null,
                      yearofpractice: req.body.yearofpractice,
                      picturepath: req.body.picturepath
                        ? req.body.picturepath
                        : user.picturepath,
                      logo: req.body.logo ? req.body.logo : user.logo,
                      signature: req.body.sign ? req.body.sign : user.sign,
                      name_of_firm: req.body.name_of_firm || null,
                    },
                  };
                  user.update({
                    email: UpdatedAttorney.email,
                  });
                  user.userdetail.update(UpdatedAttorney.userdetail);
                  res.status(200).json(user);
                }
              } else {
                res.status(400).json({
                  error:
                    "User is already found with the given new email address",
                });
              }
            })
            .catch((err) => next(err));
        }
      } else {
        res.status(400).json({
          message: "User is not found",
        });
      }
    })
    .catch((err) => next(err));
});

router.delete("/:id", middleware.auth, (req, res, next) => {
  User.findOne({
    where: { id: req.params.id },
  })
    .then((user) => {
      if (user != null) {
        user.update({ isactive: 0, archive: 1, archive_date: new Date() });
        // user.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "User is not found",
        });
      }
    })
    .catch((err) => next(err));
});

// router.get("/list/user/:userId", middleware.auth, (req, res, next) => {
//     User.findAll({
//             include: [{
//                 association: "userdetail"
//             }],
//             where: {
//                 role_id: 2,
//                 parent_user_id: req.params.userId,
//                 isactive: 1
//             },
//             order: [
//                 ["username", "ASC"]
//             ]
//         })
//         .then(admins => {
//             res.status(200).json(admins);
//         })
//         .catch(err => next(err));
// });

router.get("/list/user/:userId", middleware.auth, async (req, res, next) => {
  try {
    let adminIDS = await Common.Admin(req.params.userId);
    let admins = await User.findAll({
      include: [
        {
          association: "userdetail",
        },
      ],
      where: {
        id: adminIDS,
      },
      order: [["username", "ASC"]],
    });
    res.status(200).json(admins);
  } catch (error) {
    next(error);
  }
});

router.delete(
  "/bulk/delete",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    User.findOne({
      where: {
        id: {
          [Op.in]: req.body.ids,
        },
      },
    })
      .then((user) => {
        if (user != null) {
          user.update({ isactive: 0, archive: 1 });
          user.destroy();
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Users are not found",
          });
        }
      })
      .catch((err) => next(err));
  }
);

router.patch(
  "/bulk/active",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    User.findOne({
      where: {
        id: {
          [Op.in]: req.body.ids,
        },
      },
    })
      .then((user) => {
        if (user != null) {
          user.update({ isactive: 1, archive: 0 });
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Users are not found",
          });
        }
      })
      .catch((err) => next(err));
  }
);

router.patch(
  "/bulk/inactive",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    User.findOne({
      where: {
        id: {
          [Op.in]: req.body.ids,
        },
      },
    })
      .then((user) => {
        if (user != null) {
          user.update({ isactive: 0, archive: 1 });
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Users are not found",
          });
        }
      })
      .catch((err) => next(err));
  }
);

router.get(
  "/list/diaries/user/:userId",
  middleware.auth,
  async (req, res, next) => {
    let user = await User.findByPk(req.params.userId);
    let adminIds = [];
    if (user.role_id != 1 && user.parent_user_id != null) {
      let userPermissions = await UserPermission.findOne({
        where: { user_id: req.params.userId, permission_id: 6 },
        include: [{ association: "calendarAttorneys" }],
      });
      if (user.role_id == 2) adminIds = [parseInt(req.params.userId)];
      if (userPermissions != null && userPermissions.calendarAttorneys.length) {
        for (let item of userPermissions.calendarAttorneys) {
          let attorney = await User.findByPk(item.attorney_id);
          if (
            attorney != null &&
            (attorney.role_id == 2 || attorney.isadmin == 1)
          ) {
            adminIds.push(item.attorney_id);
          }
        }
      }
    } else {
      adminIds = await Common.firmAdmins(req.params.userId);
      // adminIds = await Common.Admin(req.params.userId);
    }
    let admins = await User.findAll({
      where: { id: adminIds },
      include: [{ association: "userdetail" }],
      order: [["username", "ASC"]],
    });
    return res.json(admins);
  }
);

module.exports = router;
