const express = require("express");
const router = express.Router();
const Message = require("../../models/Message");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Sequelize = require("sequelize");

const middleware = {
  validations: [
    check("attorney_id").isInt(),
    check("content").isString(),
    // check("end_date").isString(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

let limit = 15;

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ error: errors.array() });
  }

  Message.create(req.body)
    .then(message => {
      res.status(201).json(message);
    })
    .catch(error => res.status(500).json({ error: error }));
});

router.get("/attorney/:attorneyID", middleware.auth, (req, res) => {
  Message.findAll({
    where: { attorney_id: req.params.attorneyID, is_first: 1 },
    attributes: ["content"]
  })
    .then(messages => {
      res.status(200).json(messages);
    })
    .catch(error => res.status(500).json({ error: error }));
});

router.get("/receive/:attorneyID", middleware.auth, async (req, res) => {
  let ids = [];
  ids = await Message.findAll({
    where: { attorney_id: req.params.attorneyID },
    group: ["thread_id"],
    order: [["id", "DESC"]],
    attributes: [[Sequelize.fn("max", Sequelize.col("id")), "id"]]
  });
  ids = ids.map(item => item.id);

  if (ids.length) {
    let page = req.query.page || 1;
    limit = parseInt(req.query.limit) || limit;
    let messages = await Message.findAll({
      where: { id: ids },
      include: [
        { association: "user", include: [{ association: "userdetail" }] }
      ],
      order: [["created_at", "DESC"]],
      group: ["thread_id", "user_id"]
    });
    res.status(200).json(messages);
  } else {
    res.status(200).json([]);
  }
});

router.get("/sent/:userID", middleware.auth, (req, res) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  Message.findAll({
    where: { user_id: req.params.userID, is_first: 1 },
    include: [
      { association: "attorney", include: [{ association: "userdetail" }] }
    ]
  })
    .then(messages => {
      res.status(200).json(messages);
    })
    .catch(err => res.status(500).json({ error: err }));
});

router.get("/thread/:id", middleware.auth, async (req, res, next) => {
  try {
    let thread_message = await Message.findOne({
      where: { thread_id: req.params.id, is_first: 1 },
      include: [
        { association: "attorney", include: [{ association: "userdetail" }] },
        { association: "user", include: [{ association: "userdetail" }] }
      ]
    });
    let messages = await Message.findAll({
      where: { thread_id: req.params.id, is_first: { $ne: 1 } },
      include: [
        { association: "user", include: [{ association: "userdetail" }] }
      ],
      order: [["created_at", "ASC"]]
    });
    return res.status(200).json({ thread_message, messages });
  } catch (err) {
    next(err);
  }
});

router.delete("/thread/:id", middleware.auth, async (req, res, next) => {
  try {
    let messages = await Message.destroy({
      where: { thread_id: req.params.id }
    });
    return res.status(204).json({});
  } catch (err) {
    next(err);
  }
});

router.delete(
  "/thread/message/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let messages = await Message.destroy({
        where: { id: req.params.id }
      });
      return res.status(204).json({});
    } catch (err) {
      next(err);
    }
  }
);

module.exports = router;
