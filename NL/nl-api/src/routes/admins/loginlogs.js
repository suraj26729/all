const express = require("express");
const router = express.Router();
const LoginLog = require("../../models/LoginLog");
const checkToken = require("../../middlewares/checkToken");

const middleware = {
  auth: checkToken
};

let limit = 15;

router.get("/:adminID", middleware.auth, (req, res) => {
  let page = req.query.page || 1;
  limit = parseInt(req.query.limit) || limit;
  LoginLog.paginate({
    page: page,
    paginate: limit,
    where: { attorney_id: req.params.adminID },
    include: [
      {
        association: "attorney"
      }
    ],
    order: [["created_at", "DESC"]]
  })
    .then(logs => {
      res.status(200).json(logs);
    })
    .catch(err => res.status(500).json({ error: err }));
});

module.exports = router;
