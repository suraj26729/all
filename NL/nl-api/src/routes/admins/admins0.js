const express = require("express");
const router = express.Router();
const multer = require("multer");
const User = require("../../models/User");
const UserDetail = require("../../models/UserDetail");
const bcrypt = require("bcryptjs");
const env = require("../../../env");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const Op = sequelize.Op;

const middleware = {
  validations: [
    check("firstname").isString(),
    check("username").isString(),
    check("password").isString(),
    check("email").isEmail(),
    check("contact1").isMobilePhone("any"),
    check("address").isString(),
    check("yearofpractice").isInt(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/admins/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  }
});
const upload = multer({
  storage: storage,
  limits: { fileSize: 4194304 },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    )
      cb(null, true);
    else {
      cb(new Error("Only jpeg, jpg, png format images allowed!"), false);
    }
  }
});

const uploadImage = upload.single("profile_img");

router.post("/uploadImage", uploadImage, middleware.auth, (req, res, next) => {
  uploadImage(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      return next(err);
    } else {
      return res.status(201).json(req.file);
    }
  });
});

const storage2 = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/admins/logos");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  }
});
const upload2 = multer({
  storage: storage2,
  limits: { fileSize: 4194304 },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    )
      cb(null, true);
    else {
      cb(new Error("Only jpeg, jpg, png format images allowed!"), false);
    }
  }
});

const uploadLogo = upload2.single("logo");

router.post("/uploadLogo", uploadLogo, middleware.auth, (req, res, next) => {
  uploadLogo(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      return next(err);
    } else {
      return res.status(201).json(req.file);
    }
  });
});

const storage3 = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/admins/signs");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  }
});
const upload3 = multer({
  storage: storage3,
  limits: { fileSize: 4194304 },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    )
      cb(null, true);
    else {
      cb(new Error("Only jpeg, jpg, png format images allowed!"), false);
    }
  }
});

const uploadSign = upload3.single("sign");

router.post("/uploadSign", uploadSign, middleware.auth, (req, res, next) => {
  uploadSign(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      return next(err);
    } else {
      return res.status(201).json(req.file);
    }
  });
});

let limit = 15;

router.get("/", middleware.auth, (req, res, next) => {
  let offset = 0;
  User.count({
    where: {
      role_id: 2,
      isactive: 1
    }
  }).then(count => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    User.findAndCountAll({
      include: [
        {
          association: "userdetail"
        }
      ],
      where: {
        role_id: 2,
        isactive: 1
      },
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset
    })
      .then(admins => {
        admins["count"] = count;
        res.status(200).json({
          pages: pages,
          data: admins
        });
      })
      .catch(err => next(err));
  });
});

router.get("/user/:userId", middleware.auth, (req, res, next) => {
  let offset = 0;
  User.count({
    where: {
      role_id: 2,
      parent_user_id: req.params.userId,
      isactive: 1
    }
  }).then(count => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    User.findAndCountAll({
      include: [
        {
          association: "userdetail"
        }
      ],
      where: {
        role_id: 2,
        parent_user_id: req.params.userId,
        isactive: 1
      },
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset
    })
      .then(admins => {
        admins["count"] = count;
        res.status(200).json({
          pages: pages,
          data: admins
        });
      })
      .catch(err => next(err));
  });
});

router.post(
  "/",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
    User.findOne({
      where: { username: req.body.username }
    })
      .then(user => {
        if (user == null) {
          User.findOne({
            where: { email: req.body.email }
          })
            .then(user => {
              if (user == null) {
                bcrypt.hash(req.body.password, env.SALTROUNDS, (err, hash) => {
                  if (err) {
                    next(err);
                  } else {
                    const NewUser = {
                      username: req.body.username,
                      password: hash,
                      email: req.body.email,
                      role_id: 2,
                      parent_user_id: req.body.user_id,
                      isactive: 1,
                      userdetail: {
                        firstname: req.body.firstname,
                        lastname: req.body.lastname,
                        contact1: req.body.contact1,
                        contact2: req.body.contact2,
                        address: req.body.address,
                        yearofpractice: req.body.yearofpractice,
                        picturepath: req.body.picturepath
                          ? req.body.picturepath
                          : ""
                      }
                    };

                    User.create(NewUser, {
                      include: [{ model: UserDetail, as: "userdetail" }]
                    })
                      .then(result => {
                        res.status(201).json(result);
                      })
                      .catch(err => {
                        next(err);
                      });
                  }
                });
              } else {
                res.status(400).json({
                  error: "User is already registered with given email address"
                });
              }
            })
            .catch(err => {
              next(err);
            });
        } else {
          res.status(400).json({
            error: "User is already registered with given username"
          });
        }
      })
      .catch(err => {
        next(err);
      });
  }
);

router.get("/archive/user/:userId", middleware.auth, (req, res, next) => {
  let offset = 0;
  User.count({
    where: {
      role_id: 2,
      parent_user_id: req.params.userId,
      //deleted_at: { [Op.not]: "" }
      isactive: 0
    }
  }).then(count => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    User.findAndCountAll({
      include: [
        {
          association: "userdetail"
        }
      ],
      where: {
        role_id: 2,
        parent_user_id: req.params.userId,
        //deleted_at: { [Op.not]: "" }
        isactive: 0
      },
      limit: limit,
      offset: offset
    })
      .then(admins => {
        res.status(200).json({
          pages: pages,
          data: admins
        });
      })
      .catch(err => next(err));
  });
});

router.get("/:id", middleware.auth, (req, res, next) => {
  User.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "userdetail"
      }
    ]
  })
    .then(user => {
      res.status(200).json(user);
    })
    .catch(err => next(err));
});

router.patch(
  "/:id/status",
  [middleware.auth, middleware.validations],
  (req, res, next) => {
    User.findOne({
      where: { id: req.params.id },
      paranoid: false
    })
      .then(user => {
        if (user != null) {
          user.update({ isactive: req.body.status });
          if (req.body.status == 1) user.restore();
          user.setDataValue("deleted_at", null);
          res.status(200).json(user);
        } else {
          res.status(404).json({
            error: "User is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.put("/:id", middleware.auth, (req, res, next) => {
  User.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "userdetail"
      }
    ]
  })
    .then(user => {
      if (user != null) {
        if (user.email == req.body.email) {
          if (req.body.password != null && req.body.password != "") {
            bcrypt.hash(req.body.password, env.SALTROUNDS, (err, hash) => {
              if (err) {
                next(err);
              } else {
                const UpdatedAttorney = {
                  password: hash,
                  userdetail: {
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    contact1: req.body.contact1,
                    contact2: req.body.contact2,
                    address: req.body.address,
                    country_code: req.body.country_code || null,
                    yearofpractice: req.body.yearofpractice,
                    picturepath: req.body.picturepath
                      ? req.body.picturepath
                      : user.picturepath,
                    logo: req.body.logo ? req.body.logo : user.logo,
                    signature: req.body.sign ? req.body.sign : user.sign
                  }
                };
                user.update({ password: UpdatedAttorney.password });
                user.userdetail.update(UpdatedAttorney.userdetail);
                res.status(200).json(user);
              }
            });
          } else {
            const UpdatedAttorney = {
              userdetail: {
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                contact1: req.body.contact1,
                contact2: req.body.contact2,
                address: req.body.address,
                country_code: req.body.country_code || null,
                yearofpractice: req.body.yearofpractice,
                picturepath: req.body.picturepath
                  ? req.body.picturepath
                  : user.picturepath,
                logo: req.body.logo ? req.body.logo : user.logo,
                signature: req.body.sign ? req.body.sign : user.sign
              }
            };
            user.userdetail.update(UpdatedAttorney.userdetail);
            return res.status(200).json(user);
          }
        } else {
          User.findOne({
            where: { email: req.body.email },
            include: [
              {
                association: "userdetail"
              }
            ]
          })
            .then(user => {
              if (user == null) {
                if (req.body.password != null && req.body.password != "") {
                  bcrypt.hash(
                    req.body.password,
                    env.SALTROUNDS,
                    (err, hash) => {
                      if (err) {
                        next(err);
                      } else {
                        const UpdatedAttorney = {
                          password: hash,
                          email: req.body.email,
                          userdetail: {
                            firstname: req.body.firstname,
                            lastname: req.body.lastname,
                            contact1: req.body.contact1,
                            contact2: req.body.contact2,
                            address: req.body.address,
                            country_code: req.body.country_code || null,
                            yearofpractice: req.body.yearofpractice,
                            picturepath: req.body.picturepath
                              ? req.body.picturepath
                              : user.picturepath,
                            logo: req.body.logo ? req.body.logo : user.logo,
                            signature: req.body.sign ? req.body.sign : user.sign
                          }
                        };
                        user.update({
                          password: UpdatedAttorney.password,
                          email: UpdatedAttorney.email
                        });
                        user.userdetail.update(UpdatedAttorney.userdetail);
                        res.status(200).json(user);
                      }
                    }
                  );
                } else {
                  const UpdatedAttorney = {
                    email: req.body.email,
                    userdetail: {
                      firstname: req.body.firstname,
                      lastname: req.body.lastname,
                      contact1: req.body.contact1,
                      contact2: req.body.contact2,
                      address: req.body.address,
                      country_code: req.body.country_code || null,
                      yearofpractice: req.body.yearofpractice,
                      picturepath: req.body.picturepath
                        ? req.body.picturepath
                        : user.picturepath,
                      logo: req.body.logo ? req.body.logo : user.logo,
                      signature: req.body.sign ? req.body.sign : user.sign
                    }
                  };
                  user.update({
                    email: UpdatedAttorney.email
                  });
                  user.userdetail.update(UpdatedAttorney.userdetail);
                  res.status(200).json(user);
                }
              } else {
                res.status(400).json({
                  error:
                    "User is already found with the given new email address"
                });
              }
            })
            .catch(err => next(err));
        }
      } else {
        res.status(400).json({
          message: "User is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.delete("/:id", middleware.auth, (req, res, next) => {
  User.findOne({
    where: { id: req.params.id }
  })
    .then(user => {
      if (user != null) {
        user.update({ isactive: 0 });
        user.destroy();
        res.status(204).json({});
      } else {
        res.status(404).json({
          error: "User is not found"
        });
      }
    })
    .catch(err => next(err));
});

router.get("/list/user/:userId", middleware.auth, (req, res, next) => {
  User.findAll({
    include: [
      {
        association: "userdetail"
      }
    ],
    where: {
      role_id: 2,
      parent_user_id: req.params.userId,
      isactive: 1
    },
    order: [["username", "ASC"]]
  })
    .then(admins => {
      res.status(200).json(admins);
    })
    .catch(err => next(err));
});

router.delete(
  "/bulk/delete",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    User.findOne({
      where: { id: { [Op.in]: req.body.ids } }
    })
      .then(user => {
        if (user != null) {
          user.update({ isactive: 0 });
          user.destroy();
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Users are not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch(
  "/bulk/active",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    User.findOne({
      where: { id: { [Op.in]: req.body.ids } }
    })
      .then(user => {
        if (user != null) {
          user.update({ isactive: 1 });
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Users are not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.patch(
  "/bulk/inactive",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    User.findOne({
      where: { id: { [Op.in]: req.body.ids } }
    })
      .then(user => {
        if (user != null) {
          user.update({ isactive: 0 });
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Users are not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

module.exports = router;
