const router = require("express").Router();
const checkToken = require("../middlewares/checkToken");
const Notification = require("../models/Notification");
const { check, validationResult } = require("express-validator/check");
const Hearing = require("../models/Hearing");
const Meeting = require("../models/dashboard/Meeting");
const Task = require("../models/dashboard/Task");

const middleware = {
  validations: [
    check("link").isString(),
    check("content").isString(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      let notification = await Notification.create(req.body);
      return res.status(201).json(notification);
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get("/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let notifications = await Notification.findAll({
      where: { user_id: req.params.userID, ref_id: { $not: null } },
      order: [["created_at", "DESC"]]
    });
    return res.status(200).json(notifications);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.patch("/id/:id", middleware.auth, async (req, res, next) => {
  try {
    let notification = await Notification.findByPk(req.params.id);
    if (notification == null) return res.status(404).json("Not Found");
    notification.update({ read: true });
    return res.status(200).json(notification);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.delete("/id/:id", middleware.auth, async (req, res, next) => {
  try {
    let notification = await Notification.findByPk(req.params.id);
    if (notification == null) return res.status(404).json("Not Found");
    notification.destroy();
    return res.status(204).json({});
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.patch("/bulk-read/user/:id", middleware.auth, async (req, res, next) => {
  try {
    let result = await Notification.update(
      { read: true },
      { where: { user_id: req.params.id } }
    );
    return res.status(200).json(result);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.patch("/mark-ref", async (req, res, next) => {
  try {
    let notifications = await Notification.findAll();
    let titles = [];

    for (let item of notifications) {
      titles.push({
        id: item.id,
        title: item.content.split("|")[0].trim(),
        notification_title: item.title.toLowerCase()
      });
    }

    for (let item of titles) {
      let notification = await Notification.findByPk(item.id);
      let hearing = null;
      let task = null;
      let meeting = null;

      if (
        item.notification_title.includes("hearing") ||
        item.notification_title.includes("hearning")
      ) {
        hearing = await Hearing.findOne({
          where: { notes: item.title },
          paranoid: false
        });
      }
      if (item.notification_title.includes("task")) {
        task = await Task.findOne({
          where: { subject: item.title },
          paranoid: false
        });
      }
      if (item.notification_title.includes("meeting")) {
        meeting = await Meeting.findOne({
          where: { subject: item.title },
          paranoid: false
        });
      }

      if (hearing != null) {
        notification.update({ ref_id: hearing.id, ref_module: "hearing" });
      }
      if (task != null) {
        notification.update({ ref_id: task.id, ref_module: "task" });
      }
      if (meeting != null) {
        notification.update({ ref_id: meeting.id, ref_module: "meeting" });
      }
    }
    return res.json(titles.length);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get("/details/id/:id", middleware.auth, async (req, res, next) => {
  try {
    let notification = await Notification.findByPk(req.params.id);
    if (notification == null) return res.status(404).json("Not found error");

    let result = null;
    if (notification.ref_module == "hearing") {
      result = await Hearing.findByPk(notification.ref_id, {
        include: [
          {
            association: "attorneys",
            include: [
              {
                association: "attorney",
                attributes: ["id"],
                include: [
                  {
                    association: "userdetail",
                    attributes: ["firstname", "lastname"]
                  }
                ]
              }
            ]
          }
        ]
      });
      return res.status(200).json(result);
    }
    if (notification.ref_module == "meeting") {
      result = await Meeting.findByPk(notification.ref_id, {
        include: [
          {
            association: "matter",
            attributes: ["id", "prefix", "casenumber"]
          },
          {
            association: "contact",
            attributes: ["id", "firstname", "surname"]
          },
          {
            association: "thirdparty",
            attributes: ["id", "firstname", "surname"]
          },
          {
            association: "assignees",
            include: [
              {
                association: "assignee",
                include: [{ association: "userdetail" }]
              }
            ]
          },
          {
            association: "user",
            attributes: ["id"],
            include: [
              {
                association: "userdetail",
                attributes: ["firstname", "lastname"]
              }
            ]
          }
        ]
      });
      return res.status(200).json(result);
    }
    if (notification.ref_module == "task") {
      result = await Task.findByPk(notification.ref_id, {
        include: [
          {
            association: "clientdetail",
            attributes: ["id", "firstname", "surname", "companyname", "type"]
          },
          {
            association: "matter",
            attributes: ["id", "prefix", "casenumber"]
          },
          {
            association: "contact",
            attributes: ["id", "firstname", "surname"]
          },
          {
            association: "thirdparty",
            attributes: ["id", "firstname", "surname"]
          },
          {
            association: "assignees",
            include: [
              {
                association: "assignee",
                include: [{ association: "userdetail" }]
              }
            ]
          },
          {
            association: "user",
            attributes: ["id"],
            include: [
              {
                association: "userdetail",
                attributes: ["firstname", "lastname"]
              }
            ]
          }
        ]
      });
      return res.status(200).json(result);
    }

    return res
      .status(404)
      .json("Notification reference not found. It may be deleted");
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
