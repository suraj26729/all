const express = require("express");
const router = express.Router();
const multer = require("multer");
const Lead = require("../models/Leads");
const Client = require("../models/Client");
const checkToken = require("../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Helper = require("../helpers/dateTime");
const Notes = require("../models/Notes");
const Document = require("../models/LeadDocument");
const fs = require("fs");
const sequelize = require("sequelize");
const User = require("../models/User");
const moment = require("moment");

const middleware = {
    validations: [
        check("type").isLength({ min: 1 }),
        check("lead_type").isString(),
        check("firstname").isString(),
        // check("category_id").isInt(),
        // check("consult_date").isString(),
        // check("consult_time").isString(),
        check("assignee_id").isInt(),
        check("user_id").isInt()
    ],
    document_validation: [
        check("lead_id").isInt(),
        check("name").isLength({ min: 1 }),
        check("type_id").isInt(),
        check("description").isLength({ min: 1 }),
        check("url").isLength({ min: 1 })
    ],
    auth: checkToken
};

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./uploads/leads/documents/");
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "-" + file.originalname);
    }
});

const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        console.log(file.mimetype);
        if (file.mimetype == "application/pdf") cb(null, true);
        else {
            cb(new Error("Only pdf files allowed!"), false);
        }
    }
});

const uploadDoc = upload.single("doc");

router.post(
    "/", [middleware.validations, middleware.auth],
    (req, res, next) => {
        req.body.contact1 = req.body.contact1_code + "-" + req.body.contact1;
        req.body.contact2 = req.body.contact2_code + "-" + req.body.contact2;
        req.body.mobile = req.body.mobile_code + "-" + req.body.mobile;
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        if (req.body.consult_time != "" && req.body.consult_time != null) {
            req.body.consult_datetime = new Date(
                req.body.consult_date +
                " " +
                Helper.convertTime(req.body.consult_time) +
                ":00Z"
            );
        }

        Lead.create(req.body)
            .then(lead => {
                res.status(201).json(lead);
            })
            .catch(err => {
                console.log(err);
                next(err);
            });
    }
);

router.get("/paginate/user/:userId", middleware.auth, (req, res, next) => {
    let offset = 0;
    let limit = 10;
    Lead.count({
        where: {
            user_id: req.params.userId
        }
    }).then(count => {
        limit = req.query.limit ? parseInt(req.query.limit) : limit;
        let page = req.query.page ? req.query.page : 1; // page number
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        const { from, to } = req.query;
        let cond = {};
        if (from && to) {
            cond = {
                user_id: req.params.userId,
                created_at: { $between: [from, to] }
            };
        } else {
            cond = { user_id: req.params.userId };
        }
        Lead.findAndCountAll({
                attributes: [
                    "id",
                    "uuid",
                    "created_at",
                    "consult_date",
                    "firstname",
                    "surname",
                    "email",
                    "address1",
                    "location",
                    "consultation_booked",
                    "client_arrive",
                    "converted",
                    "status"
                ],
                include: [{
                    association: "category"
                }],
                where: cond,
                order: [
                    ["created_at", "DESC"]
                ],
                limit: limit,
                offset: offset
            })
            .then(leads => {
                leads["count"] = count;
                res.status(200).json({
                    pages: pages,
                    data: leads
                });
            })
            .catch(err => next(err));
    });
});

router.get("/id/:id", middleware.auth, (req, res, next) => {
    Lead.findOne({
            where: { $or: [{ id: req.params.id }, { uuid: req.params.id }] }
        })
        .then(lead => {
            res.status(200).json(lead);
        })
        .catch(err => next(err));
});

router.put(
    "/id/:id", [middleware.auth, middleware.validations],
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        Lead.findOne({ where: { id: req.params.id } })
            .then(lead => {
                lead.update(req.body);
                res.status(200).json(lead);
            })
            .catch(err => next(err));
    }
);

router.patch(
    "/id/:id/status", [middleware.auth, [check("status").isString()]],
    async(req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        Lead.findOne({ where: { id: req.params.id } })
            .then(lead => {
                lead.update({ status: req.body.status });
                res.status(200).json(lead);
            })
            .catch(err => next(err));
    }
);

router.patch(
    "/id/:id/consultation-book", [middleware.auth, [check("consultation_booked").isString()]],
    async(req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        Lead.findOne({ where: { id: req.params.id } })
            .then(lead => {
                lead.update({ consultation_booked: req.body.consultation_booked });
                res.status(200).json(lead);
            })
            .catch(err => next(err));
    }
);

router.patch(
    "/id/:id/client-arrive", [middleware.auth, [check("client_arrive").isString()]],
    async(req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        Lead.findOne({ where: { id: req.params.id } })
            .then(lead => {
                lead.update({ client_arrive: req.body.client_arrive });
                res.status(200).json(lead);
            })
            .catch(err => next(err));
    }
);

router.patch(
    "/id/:id/conversion", [middleware.auth],
    async(req, res, next) => {
        Lead.findOne({ where: { id: req.params.id } })
            .then(lead => {
                const NewClient = {
                    companyname: lead.companyname || "",
                    companyreg: lead.companyreg || "",
                    companyvat: lead.companyvat || "",
                    firstname: lead.firstname || "",
                    surname: lead.surname || "",
                    email: lead.email || "",
                    creditlimits: "",
                    contact1: lead.contact1_code + "-" + lead.contact1 || "",
                    contact2: lead.contact1_code + "-" + lead.contact2 || "",
                    mobile: lead.mobile_code + "-" + lead.mobile || "",
                    dob: lead.dob || "",
                    bankdetails: "",
                    personaldetails: "",
                    address1: lead.address1 || "",
                    address2: lead.address2 || "",
                    city: lead.city || "",
                    zipcode: lead.zipcode || "",
                    country: lead.country || "",
                    province: lead.province || "",
                    type: lead.type || "",
                    picturepath: "",
                    isactive: 1,
                    user_id: lead.user_id || ""
                };
                Client.create(NewClient)
                    .then(client => {
                        lead.update({ converted: "yes", status: "converted" });
                        res.status(200).json(client);
                    })
                    .catch(err => next(err));
            })
            .catch(err => next(err));
    }
);

router.delete("/id/:id", middleware.auth, (req, res, next) => {
    Lead.findOne({ where: { id: req.params.id } })
        .then(lead => {
            if (lead != null) {
                lead.destroy();
                res.status(204).json({});
            } else {
                res.status(404).json({
                    error: "Lead is not found"
                });
            }
        })
        .catch(err => next(err));
});

//Notes
router.get("/notes/:parentID", middleware.auth, async(req, res, next) => {
    Notes.findAll({
            where: { parent_id: req.params.parentID, type: "lead" },
            order: [
                ["created_at", "DESC"]
            ]
        })
        .then(notes => {
            res.status(200).json(notes);
        })
        .catch(error => next(error));
});

//Documents
router.post("/document/upload", uploadDoc, middleware.auth, (req, res) => {
    uploadDoc(req, res, function(err) {
        if (err instanceof multer.MulterError) {
            return res.status(200).json({ error: err });
        } else {
            return res.status(201).json(req.file);
        }
    });
});

router.post(
    "/document", [middleware.document_validation, middleware.auth],
    (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ error: errors.array() });
        }

        Document.create(req.body)
            .then(document => {
                res.status(201).json(document);
            })
            .catch(err => res.status(500).json({ error: err }));
    }
);

router.get(
    "/documents/paginate/lead/:id",
    middleware.auth,
    async(req, res, next) => {
        let limit = parseInt(req.query.limit) || 15;
        let page = parseInt(req.query.page) || 1;
        Lead.findOne({ where: { uuid: req.params.id } })
            .then(lead => {
                if (lead == null) return res.status(404).json("Lead not found!");
                Document.paginate({
                        paginate: limit,
                        page: page,
                        where: { lead_id: lead.id },
                        include: [{ association: "type" }, { association: "lead" }]
                    })
                    .then(doctypes => {
                        res.status(200).json(doctypes);
                    })
                    .catch(err => next(err));
            })
            .catch(next);
    }
);

router.delete("/document/id/:id", middleware.auth, (req, res, next) => {
    Document.findOne({ where: { id: req.params.id } })
        .then(doc => {
            fs.unlink(doc.url);
            doc.destroy();
            res.status(204).json({});
        })
        .catch(error => next(error));
});

//Consult Report
router.post(
    "/consult-report/user/:id",
    middleware.auth,
    async(req, res, next) => {
        let assignees_ids = req.body.assignees || [];
        let start_date = req.body.start_date || null;
        let end_date = req.body.end_date || null;

        if (end_date != null) {
            let _end_date = new Date(end_date);
            _end_date.setDate(_end_date.getDate() + 1);
            end_date = moment(_end_date).format("YYYY-MM-DD");
        }

        let params = [{ user_id: req.params.id }];

        if (assignees_ids.length) params.push({ assignee_id: assignees_ids });
        if (start_date != null && end_date != null)
            params.push({ created_at: { $between: [start_date, end_date] } });
        if (start_date != null && end_date == null)
            params.push({ created_at: { $gte: start_date } });
        if (start_date == null && end_date != null)
            params.push({ created_at: { $lte: end_date } });

        var conditions = params.reduce((a, b) => Object.assign(a, b), {});

        let locations = await Lead.findAll({
            where: conditions,
            attributes: [
                [sequelize.fn("DISTINCT", sequelize.col("location")), "location"]
            ]
        });
        locations = locations.filter(row => row.location != "");
        locations = locations.map(row => row.location);

        let assignees = await Lead.findAll({
            where: conditions,
            attributes: ["assignee_id"]
        });
        assignees = assignees.filter(row => row.assignee_id != "");
        assignees = assignees.map(row => row.assignee_id);

        let data = [];
        let assigneeData = [];
        let count = 0;
        for (let assignee_id of assignees) {
            data = [];
            let assigneeDB = await User.findByPk(assignee_id, {
                include: [{ association: "userdetail" }]
            });
            for (let location of locations) {
                let results = await Lead.findAll({
                    where: {
                        user_id: req.params.id,
                        location: location,
                        assignee_id: assignee_id
                    },
                    attributes: ["id"],
                    include: [{
                        association: "assignee",
                        attributes: ["id"],
                        include: [{
                            association: "userdetail",
                            attributes: ["firstname", "lastname"]
                        }]
                    }]
                });
                data.push(results.length);
            }
            assigneeData.push({
                data: data,
                label: assigneeDB.userdetail.firstname + " " + assigneeDB.userdetail.lastname
            });
        }

        res.status(200).json({ labels: locations, data: assigneeData });
    }
);

router.get(
    "/consult-report/user/:id/assignee-count/:assigneeId",
    middleware.auth,
    (req, res, next) => {
        Lead.findAll({
            where: { user_id: req.params.id, assignee_id: req.params.assigneeId },
            attributes: ["id"],
            include: [
                { association: "assignee", include: [{ association: "userdetail" }] }
            ]
        }).then(
            leads => {
                let assignee = {};
                assignee = leads.length ? leads[0].assignee.userdetail : {};
                res.status(200).json({ count: leads.length, assignee: assignee });
            },
            err => next(err)
        );
    }
);

router.post(
    "/converted-report/user/:id",
    middleware.auth,
    async(req, res, next) => {
        let assignees_ids = req.body.assignees || [];
        let start_date = req.body.start_date || null;
        let end_date = req.body.end_date || null;

        if (end_date != null) {
            let _end_date = new Date(end_date);
            _end_date.setDate(_end_date.getDate() + 1);
            end_date = moment(_end_date).format("YYYY-MM-DD");
        }

        let params = [{ user_id: req.params.id, status: "converted" }];

        if (assignees_ids.length) params.push({ assignee_id: assignees_ids });
        if (start_date != null && end_date != null)
            params.push({ created_at: { $between: [start_date, end_date] } });
        if (start_date != null && end_date == null)
            params.push({ created_at: { $gte: start_date } });
        if (start_date == null && end_date != null)
            params.push({ created_at: { $lte: end_date } });

        var conditions = params.reduce((a, b) => Object.assign(a, b), {});

        let category_ids = await Lead.findAll({
            where: conditions,
            attributes: ["category_id"],
            include: [{ association: "category", attributes: ["name"] }]
        });
        let category_names = category_ids.map(row => row.category.name);
        category_names = ["", ...category_names];
        category_ids = category_ids.filter(row => row.category_id != "");
        category_ids = category_ids.map(row => row.category_id);
        category_ids = category_ids.filter(
            (item, pos) => category_ids.indexOf(item) == pos
        );

        let data = [];
        for (let category_id of category_ids) {
            let count = await Lead.count({
                where: {
                    user_id: req.params.id,
                    status: "converted",
                    category_id: category_id
                }
            });
            data.push(count);
        }

        data = ["", ...data];

        res
            .status(200)
            .json({ labels: category_names, data: [{ data: data, label: "Total" }] });
    }
);

module.exports = router;