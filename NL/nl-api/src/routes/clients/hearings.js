const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Sequelize = require("sequelize");
const moment = require("moment");
const Matter = require("../../models/CaseDetails");
const Hearing = require("../../models/Hearing");
const HearingAttorney = require("../../models/HearingAttorneys");
const User = require("../../models/User");
const UserDetail = require("../../models/UserDetail");
const CaseAssistAttorneys = require("../../models/CaseAssistAttorneys");
const CaseThirdparty = require("../../models/CaseThirdparty");
const CaseContacts = require("../../models/CaseContacts");
const CaseDetails = require("../../models/CaseDetails");
const ClientContact = require("../../models/ClientContact");
const Client = require("../../models/Client");
const Court = require("../../models/masters/Court");
const Helper = require("../../helpers/dateTime");
const mail = require("../../config/mail");
const fs = require("fs");
const common = require("../../helpers/common");
const Email = require("../../models/Email");
const Notification = require("../../models/Notification");

const middleware = {
  validations: [
    //check("client_id").isUUID(),
    //check("matter_id").isUUID(),
    // check("thirdparty_id").isUUID(),
    // check("contact_id").isUUID(),
    //check("court_id").isInt(),
    check("all_day_event").isInt(),
    check("notes").isString(),
    check("from_date").isString(),
    check("to_date").isString()
  ],
  auth: checkToken
};

let limit = 15;

router.get("/:clientID/matters", middleware.auth, (req, res) => {
  Matter.findAll({
    where: { client_id: req.params.clientID, status: { $not: [4] } },
    attributes: ["id", "prefix", "casenumber"]
  })
    .then(matters => {
      if (matters != null) {
        res.json({
          success: 1,
          data: matters,
          message: "List of matters for a particular client"
        });
      } else {
        res.json({
          success: 1,
          data: null,
          message: "Files were closed"
        });
      }
    })
    .catch(err => res.json(err));
});

router.get("/:matterID/attorneys", middleware.auth, async (req, res) => {
  let principleAttorney = await Matter.findOne({
    where: { id: req.params.matterID },
    attributes: ["priattorney"],
    include: [
      {
        model: User,
        as: "principleAttorney",
        include: [{ model: UserDetail, as: "userdetail" }]
      }
    ]
  });
  let associateAttorneys = await CaseAssistAttorneys.findAll({
    where: { case_id: req.params.matterID },
    include: [
      {
        model: User,
        as: "attorney",
        include: [{ model: UserDetail, as: "userdetail" }]
      }
    ]
  });

  let result = [];
  if (principleAttorney.principleAttorney != null) {
    result.push({
      id: principleAttorney.priattorney,
      name:
        principleAttorney.principleAttorney.userdetail.firstname +
        " " +
        principleAttorney.principleAttorney.userdetail.lastname
    });
  }
  if (associateAttorneys.length) {
    for (attorney of associateAttorneys) {
      result.push({
        id: attorney.attorney_id,
        name:
          attorney.attorney.userdetail.firstname +
          " " +
          attorney.attorney.userdetail.lastname
      });
    }
  }
  result = Array.from(new Set(result.map(JSON.stringify))).map(JSON.parse);
  res.json({
    success: 1,
    data: result,
    message: "Attorney Details"
  });
});

router.get("/:matterID/thirdparties", middleware.auth, async (req, res) => {
  let thirdparties = await CaseContacts.findAll({
    where: { case_id: req.params.matterID, c_type: "t" },
    include: [{ association: "clientcontact" }]
  });
  let result = [];
  if (thirdparties != null) {
    for (row of thirdparties) {
      let names = [];
      if (
        row.clientcontact.firstname != undefined &&
        row.clientcontact.firstname != null &&
        row.clientcontact.firstname != ""
      )
        names.push(row.clientcontact.firstname);
      if (
        row.clientcontact.surname != undefined &&
        row.clientcontact.surname != null &&
        row.clientcontact.surname != ""
      )
        names.push(row.clientcontact.surname);

      let name = "";
      if (names.length) {
        result.push({
          id: row.c_id,
          name: names.join(" ", names) || "",
          phone: row.clientcontact.mobile
        });
      }
    }
  }

  const dedupThings = Array.from(
    result.reduce((m, t) => m.set(t.id, t), new Map()).values()
  );

  res.json({
    success: 1,
    data: dedupThings,
    message: "List of third parties for a particular matter"
  });
});

router.get("/:matterID/contacts", middleware.auth, async (req, res) => {
  let contacts = await CaseContacts.findAll({
    where: { case_id: req.params.matterID, c_type: "c" },
    include: [{ association: "clientcontact" }]
  });
  let result = [];
  if (contacts != null) {
    for (row of contacts) {
      result.push({
        id: row.c_id,
        name: row.clientcontact.firstname + " " + row.clientcontact.surname,
        phone: row.clientcontact.mobile
      });
    }
  }
  result = Array.from(new Set(result.map(JSON.stringify))).map(JSON.parse);
  res.json({
    success: 1,
    data: result,
    message: "List of contacts for a particular matter"
  });
});

router.get(
  "/:matterID/claim-handlers/id/:id",
  middleware.auth,
  async (req, res, next) => {
    try {
      let contact = await CaseContacts.findOne({
        where: { case_id: req.params.matterID, c_id: req.params.id },
        include: [{ association: "clientcontact" }]
      });
      if (contact != null) {
        return res.status(200).json(contact);
      }
      return res.status(404).json("Not found");
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ success: 0, errors: errors.array() });
      }

      if (req.body.from_time != "") {
        req.body.from_date = new Date(
          req.body.from_date +
            " " +
            Helper.convertTime(req.body.from_time) +
            ":00Z"
        );
      }
      if (req.body.to_time != "") {
        req.body.to_date = new Date(
          req.body.to_date + " " + Helper.convertTime(req.body.to_time) + ":00Z"
        );
      }

      if (req.body.client_id == "") delete req.body.client_id;
      if (req.body.matter_id == "") delete req.body.matter_id;
      if (req.body.contact_id == "") delete req.body.contact_id;
      if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;
      if (req.body.court_id == "") delete req.body.court_id;

      let hearing = "";
      if (req.body.attorneys != null) {
        hearing = await Hearing.create(req.body, {
          include: [{ model: HearingAttorney, as: "attorneys" }]
        });
      } else {
        hearing = await Hearing.create(req.body);
      }

      if (req.body.matter_id) {
        let casedetail = await CaseDetails.findOne({
          where: { id: hearing.matter_id }
        });

        let user = await User.findOne({
          [Sequelize.Op.or]: {
            parent_user_id: casedetail.user_id,
            id: casedetail.user_id
          }
        });

        let clientdetail = await Client.findOne({
          where: { id: hearing.client_id }
        });

        // return res.status(200).json(hearing);

        let court = await Court.findOne({
          where: { id: hearing.court_id }
        });

        let lawyer = await User.findOne({
          where: { id: hearing.attorneys.attorney_id }
        });

        var name = [];
        var email = [];
        let ids = [];
        // let att=USers.findAll

        for (attorney of hearing.attorneys) {
          let attorneys = await User.findOne({
            include: [{ association: "userdetail" }],
            where: { id: attorney.attorney_id }
          });
          email.push(attorneys.email);
          name.push(
            attorneys.userdetail.firstname + " " + attorneys.userdetail.lastname
          );
          ids.push(attorney.attorney_id);
        }
        var assigned = name.join(", ");

        var sdate = moment(hearing.from_date).format("DD-MM-YYYY");
        var edate = moment(hearing.to_date).format("DD-MM-YYYY");

        for (let i = 0; i < email.length; i++) {
          let content = fs.readFileSync(
            "./src/email-templates/crm/new_hearing.html",
            "utf8"
          );

          content = content.replace(
            "**cname**",
            clientdetail.firstname + " " + clientdetail.surname
          );
          content = content.replace(
            "**file_no**",
            casedetail.prefix + casedetail.casenumber
          );
          content = content.replace("**desc**", hearing.notes);
          content = content.replace("**duration**", sdate + "-" + edate);
          if (req.body.court_id)
            content = content.replace("**court**", court.name);
          else content = content.replace("**court**", "Not Specified");
          content = content.replace("**assigned_by**", assigned);

          content = content.replace("**name**", name[i]);

          await Email.create({
            parent_id: hearing.id,
            client_id: clientdetail.id,
            matter_id: casedetail.id,
            subject: "New Hearing",
            type: "new_hearing",
            user_id: ids[i]
          });

          let createdBy = await User.findByPk(hearing.user_id, {
            include: [{ association: "userdetail" }]
          });
          createdBy =
            createdBy != null
              ? createdBy.userdetail.firstname +
                " " +
                createdBy.userdetail.lastname
              : "Unknown";
          let notification = await Notification.create({
            title: "New Hearing Created",
            content: hearing.notes + "|By:" + createdBy,
            ref_id: hearing.id,
            ref_module: "hearing",
            user_id: ids[i]
          });

          let sentMail = await mail.send(
            email[i],
            "New Hearing",
            content,
            next
          );
          //console.log(email[i]);
        }
      }

      //res.status(200).json({ name: name, email: email, assigned: assigned });
      res.status(201).json({
        success: 1,
        data: hearing,
        message: "New hearing created"
      });
    } catch (err) {
      console.log(err);
      next(err);
    }
  }
);

router.get("/:clientID", middleware.auth, (req, res) => {
  let offset = 0;
  Hearing.count({
    where: {
      client_id: req.params.clientID
    }
  }).then(count => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    limit = 999999;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    Hearing.findAndCountAll({
      where: {
        client_id: req.params.clientID
      },
      include: [
        {
          association: "matter"
        },
        {
          association: "attorneys",
          include: [
            {
              association: "attorney",
              include: [
                {
                  association: "userdetail"
                }
              ]
            }
          ]
        },
        {
          association: "court"
        }
      ],
      order: [["created_at", "DESC"]],
      limit: limit,
      distinct: true,
      offset: offset
    })
      .then(hearings => {
        res.status(200).json({
          success: 1,
          pages: pages,
          data: hearings,
          message: "List of hearings for a particular client"
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json(err);
      });
  });
});

router.patch(
  "/:hearingID/status",
  [middleware.auth, [check("status").isLength({ min: 1 })]],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Hearing.findOne({
      where: { id: req.params.hearingID }
    })
      .then(hearing => {
        if (hearing == null) {
          res.status(404).json({
            error: "Hearing not found"
          });
        } else {
          hearing.update({
            status: req.body.status
          });
          res.status(200).json(hearing);
        }
      })
      .catch(err => res.json(err));
  }
);

router.get("/recents/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.userID);
    let assigneeID = await common.permittedUsers(req.params.userID);
    let clients = await Client.findAll({
      where: { user_id: userIds },
      attributes: ["id"]
    });

    let client_ids = [];
    for (let client of clients) {
      client_ids.push(client.id);
    }

    let hearings = await Hearing.findAll({
      where: {
        client_id: client_ids,
        status: "Open",
        from_date: {
          $gte: new Date()
        }
      },
      attributes: ["id", "client_id", "notes", "from_date", "from_time"],
      include: [
        {
          association: "clientdetail",
          attributes: ["id", "firstname", "surname", "picturepath"]
        },
        {
          association: "attorneys",
          where: { attorney_id: assigneeID }
        }
      ],
      order: [["from_date", "ASC"]]
    });
    return res.status(200).json(hearings);
  } catch (error) {
    next(error);
  }
});

router.get("/id/:id", middleware.auth, async (req, res, next) => {
  let hearing = await Hearing.findByPk(req.params.id, {
    include: [{ association: "attorneys" }]
  });

  return res.status(200).json(hearing);
});

router.put(
  "/id/:id",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    let hearing = await Hearing.findByPk(req.params.id);
    if (hearing != null) {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ success: 0, errors: errors.array() });
      }

      if (req.body.from_time != "") {
        req.body.from_date = new Date(
          req.body.from_date +
            " " +
            Helper.convertTime(req.body.from_time) +
            ":00Z"
        );
      }
      if (req.body.to_time != "") {
        req.body.to_date = new Date(
          req.body.to_date + " " + Helper.convertTime(req.body.to_time) + ":00Z"
        );
      }

      if (req.body.client_id == "") delete req.body.client_id;
      if (req.body.matter_id == "") delete req.body.matter_id;
      if (req.body.contact_id == "") delete req.body.contact_id;
      if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;
      if (req.body.court_id == "") delete req.body.court_id;

      hearing.update(req.body);

      if (req.body.attorneys.length) {
        let attorneys = await HearingAttorney.destroy({
          where: { hearing_id: hearing.id }
        });
        let args = req.body.attorneys.map(item => {
          return { hearing_id: hearing.id, attorney_id: item.attorney_id };
        });
        let new_attorneys = await HearingAttorney.bulkCreate(args);
      }

      res.status(200).json(hearing);
    } else return res.status(404).json("Hearing is not found!");
  }
);

router.delete("/id/:id", middleware.auth, async (req, res, next) => {
  let hearing = await Hearing.findByPk(req.params.id);
  if (hearing != null) {
    hearing.destroy();
    return res.status(204).json({});
  } else return res.status(404).json("Hearing is not found!");
});

module.exports = router;
