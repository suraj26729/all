const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Notes = require("../../models/Notes");

const middleware = {
  validations: [check("content").isLength({ min: 1 })],
  auth: checkToken
};

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Notes.create(req.body)
      .then(NewNote => {
        res.status(201).json(NewNote);
      })
      .catch(error => res.status(500).json({ error: error }));
  }
);

router.get("/client/:parentID", middleware.auth, async (req, res) => {
  Notes.findAll({
    where: { parent_id: req.params.parentID, type: "client" },
    order: [["created_at", "DESC"]]
  })
    .then(notes => {
      res.status(200).json(notes);
    })
    .catch(error => res.status(500).json({ error: error }));
});

router.get("/matter/:parentID", middleware.auth, async (req, res) => {
  Notes.findAll({
    where: { parent_id: req.params.parentID, type: "matter" },
    order: [["created_at", "DESC"]]
  })
    .then(notes => {
      res.status(200).json(notes);
    })
    .catch(error => res.status(500).json({ error: error }));
});

router.delete("/:id", middleware.auth, (req, res) => {
  Notes.findOne({
    where: { id: req.params.id }
  })
    .then(note => {
      if (note != null) {
        note.destroy();
        res.status(200).json(note);
      } else {
        res.status(404).json({});
      }
    })
    .catch(error => res.status(500).json({ error: error }));
});

module.exports = router;
