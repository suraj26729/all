const express = require("express");
const router = express.Router();
const multer = require("multer");
const Client = require("../../models/Client");
const ClientContact = require("../../models/ClientContact");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const Op = sequelize.Op;

const middleware = {
  validations: [check("firstname").isString(), check("user_id").isInt()],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    ClientContact.findOne({
      where: { client_id: req.body.client_id, email: req.body.email }
    })
      .then(contact => {
        if (contact == null) {
          let dob = req.body.dob;
          if (typeof dob === "object" && dob !== null) {
            dob =
              dob.year +
              "-" +
              (dob.month < 10 ? "0" + dob.month : dob.month) +
              "-" +
              (dob.day < 10 ? "0" + dob.day : dob.day);
          }
          const NewContact = {
            client_id: req.body.client_id || "",
            firstname: req.body.firstname || "",
            surname: req.body.surname || "",
            email: req.body.email || "",
            creditlimits: req.body.creditlimits || "",
            contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
            contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
            mobile: req.body.mobile || "",
            dob: dob || "",
            bankdetails: req.body.bankdetails || "",
            personaldetails: req.body.personaldetails || "",
            address1: req.body.address1 || "",
            address2: req.body.address2 || "",
            city: req.body.city || "",
            zipcode: req.body.zipcode || "",
            country: req.body.country || "",
            province: req.body.province || "",
            type: req.body.type || "c",
            isactive: 1,
            user_id: req.body.user_id || ""
          };
          ClientContact.create(NewContact)
            .then(result => {
              res.status(201).json({
                success: 1,
                message: "Client contact Created",
                data: result
              });
            })
            .catch(err => {
              next(err);
            });
        } else {
          res.status(400).json({
            message:
              "Client contact or third party is already found with the given new email address"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.get("/contacts/:clientId", middleware.auth, (req, res) => {
  ClientContact.findAndCountAll({
    where: {
      client_id: req.params.clientId,
      type: "c"
    }
  })
    .then(contacts => {
      res.status(200).json({
        success: 1,
        data: contacts,
        message: "List of client contacts for a particular client"
      });
    })
    .catch(err => res.status(500).json(err));
});

router.get("/thirdparties/:clientId", middleware.auth, (req, res) => {
  ClientContact.findAndCountAll({
    where: {
      client_id: req.params.clientId,
      type: "t"
    }
  })
    .then(thirdparties => {
      res.status(200).json({
        success: 1,
        data: thirdparties,
        message: "List of client third parties for a particular client"
      });
    })
    .catch(err => res.status(500).json(err));
});

router.put(
  "/:id",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    ClientContact.findOne({
      where: { id: req.params.id }
    })
      .then(contact => {
        if (contact != null) {
          let dob = req.body.dob;
          if (typeof dob === "object" && dob != null) {
            dob =
              dob.year +
              "-" +
              (dob.month < 10 ? "0" + dob.month : dob.month) +
              "-" +
              (dob.day < 10 ? "0" + dob.day : dob.day);
          }
          if (contact.email == req.body.email) {
            const UpdatedContact = {
              firstname: req.body.firstname || "",
              surname: req.body.surname || "",
              email: req.body.email || "",
              creditlimits: req.body.creditlimits || "",
              contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
              contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
              mobile: req.body.mobile || "",
              dob: dob || "",
              bankdetails: req.body.bankdetails || "",
              personaldetails: req.body.personaldetails || "",
              address1: req.body.address1 || "",
              address2: req.body.address2 || "",
              city: req.body.city || "",
              zipcode: req.body.zipcode || "",
              country: req.body.country || "",
              province: req.body.province || ""
            };
            contact.update(UpdatedContact);
            res.status(200).json({
              success: 1,
              data: contact,
              message: "Client Contact Info is updated"
            });
          } else {
            ClientContact.findOne({
              where: { email: req.body.email }
            })
              .then(contact2 => {
                if (contact2 == null) {
                  const UpdatedContact = {
                    firstname: req.body.firstname || "",
                    surname: req.body.surname || "",
                    email: req.body.email || "",
                    creditlimits: req.body.creditlimits || "",
                    contact1:
                      req.body.contact1_code + "-" + req.body.contact1 || "",
                    contact2:
                      req.body.contact2_code + "-" + req.body.contact2 || "",
                    mobile: req.body.mobile || "",
                    dob: dob || "",
                    bankdetails: req.body.bankdetails || "",
                    personaldetails: req.body.personaldetails || "",
                    address1: req.body.address1 || "",
                    address2: req.body.address2 || "",
                    city: req.body.city || "",
                    zipcode: req.body.zipcode || "",
                    country: req.body.country || "",
                    province: req.body.province || ""
                  };
                  contact.update(UpdatedContact);
                  res.status(200).json({
                    success: 1,
                    data: contact,
                    message: "Client Contact Info is updated"
                  });
                } else {
                  res.status(200).json({
                    success: 0,
                    data: {},
                    message:
                      "Client Contact is already found with the given new email address"
                  });
                }
              })
              .catch(err => next(err));
          }
        } else {
          res.status(200).json({
            success: 0,
            data: {},
            message: "Client Contact is not found"
          });
        }
      })
      .catch(err => next(err));
  }
);

router.delete("/:id", middleware.auth, (req, res) => {
  if (req.params.id) {
    ClientContact.findOne({
      where: { id: req.params.id }
    })
      .then(contact => {
        if (contact != null) {
          contact.update({ isactive: 0 });
          contact.destroy();
          res.status(200).json({
            success: 1,
            data: contact,
            message: "Client Contact is deleted"
          });
        } else {
          res.status(200).json({
            success: 0,
            data: {},
            message: "Client Contact is not found"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Client Contact Id is not found in the request"
    });
  }
});

let limit = 15;

router.get("/list/client/:clientId", middleware.auth, (req, res, next) => {
  let offset = 0;
  ClientContact.count({
    where: {
      client_id: req.params.clientId,
      type: "c"
    }
  }).then(count => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    ClientContact.findAndCountAll({
      where: {
        client_id: req.params.clientId,
        type: "c"
      },
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset
    })
      .then(contacts => {
        contacts["count"] = count;
        res.status(200).json({
          pages: pages,
          data: contacts
        });
      })
      .catch(err => next(err));
  });
});

router.get(
  "/thirdparties/list/client/:clientId",
  middleware.auth,
  (req, res, next) => {
    let offset = 0;
    ClientContact.count({
      where: {
        client_id: req.params.clientId,
        type: "t"
      }
    }).then(count => {
      limit = req.query.limit ? parseInt(req.query.limit) : limit;
      let page = req.query.page ? req.query.page : 1; // page number
      let pages = Math.ceil(count / limit);
      offset = limit * (page - 1);
      ClientContact.findAndCountAll({
        where: {
          client_id: req.params.clientId,
          type: "t"
        },
        order: [["created_at", "DESC"]],
        limit: limit,
        offset: offset
      })
        .then(thirdparties => {
          thirdparties["count"] = count;
          res.status(200).json({
            pages: pages,
            data: thirdparties
          });
        })
        .catch(err => next(err));
    });
  }
);

router.get("/thirdparty/id/:id", middleware.auth, (req, res) => {
  ClientContact.findByPk(req.params.id)
    .then(thirdparty => {
      res.status(200).json(thirdparty);
    })
    .catch(err => next(err));
});

module.exports = router;
