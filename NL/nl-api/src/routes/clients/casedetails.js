const express = require("express");
const router = express.Router();
const CaseDetails = require("../../models/CaseDetails");
const Document = require("../../models/Document");
const CaseThirdparty = require("../../models/CaseThirdparty");
const CaseAssistAttorneys = require("../../models/CaseAssistAttorneys");
const CaseContacts = require("../../models/CaseContacts");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const Op = sequelize.Op;
const User = require("../../models/User");
const common = require("../../helpers/common");

const middleware = {
  validations: [check("casenumber").isInt(), check("uid").isInt()],
  auth: checkToken
};

let limit = 15;

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    let ids = await common.firmUsers(req.body.uid);
    CaseDetails.findOne({
      where: {
        user_id: ids,
        casenumber: req.body.casenumber
      }
    })
      .then(casedetails => {
        if (casedetails == null) {
          const NewCaseDetails = {
            client_id: req.body.client_id,
            prefix: req.body.prefix,
            casenumber: req.body.casenumber,
            description: req.body.description || "",
            category_id: req.body.category_id,
            client_no: req.body.client_no,
            user_id: req.body.uid,
            priattorney: req.body.priattorney,
            assistattorney: req.body.asstattorney
            //contacts: req.body.contacts
            //casethirdparty: req.body.thirdparty
          };

          CaseDetails.create(NewCaseDetails, {
            include: [
              // { model: CaseThirdparty, as: "casethirdparty" },
              { model: CaseAssistAttorneys, as: "assistattorney" }
              //{ model: CaseContacts, as: "contacts" }
            ]
          })
            .then(result => {
              if (req.body.contacts != null && req.body.contacts.length) {
                let data = [];
                for (let item of req.body.contacts) {
                  data.push({
                    case_id: result.id,
                    c_id: item.c_id,
                    c_type: item.c_type
                  });
                }
                CaseContacts.bulkCreate(data)
                  .then(result2 => {
                    res.status(201).json({
                      success: 1,
                      message: "Case Details Created",
                      data: result
                    });
                  })
                  .catch(next);
              } else {
                res.status(201).json({
                  success: 1,
                  message: "Case Details Created",
                  data: result
                });
              }
            })
            .catch(err => {
              console.log(err);
              next(err);
            });
        } else {
          res.status(200).json({
            success: 0,
            data: {},
            message: "This casenumber already exists"
          });
        }
      })
      .catch(err => {
        next(err);
        console.log(err);
      });
  }
);

router.get("/next-number/:userID", middleware.auth, async (req, res) => {
  let userIds = await common.firmUsers(req.params.userID);
  CaseDetails.max("casenumber", {
    where: { user_id: userIds }
  })
    .then(caseno => {
      if (caseno == null) {
        res.json(1000);
      } else {
        res.json(parseInt(caseno) + 1);
      }
    })
    .catch(err => res.status(500).json(err));
});

router.post("/check-number/:userID", middleware.auth, async (req, res) => {
  let ids = await common.firmUsers(req.params.userID);
  CaseDetails.findOne({
    where: {
      user_id: ids,
      casenumber: req.body.casenumber
    }
  })
    .then(caseno => {
      if (caseno == null) {
        res.json({ exists: 0 });
      } else {
        res.json({ exists: 1 });
      }
    })
    .catch(err => res.status(500).json(err));
});

router.get("/:clientID", middleware.auth, (req, res) => {
  let offset = 0;
  CaseDetails.count({
    where: {
      client_id: req.params.clientID
    }
  }).then(count => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    limit = 999999;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    CaseDetails.findAndCountAll({
      where: {
        status: { $not: [4] },
        client_id: req.params.clientID
      },
      include: [
        {
          association: "clientdetail"
        },
        {
          association: "contacts"
        },
        {
          association: "principleAttorney",
          include: [{ association: "userdetail" }]
        },
        {
          association: "assistattorney",
          include: [
            {
              association: "attorney",
              as: "assistattorney",
              include: [{ association: "userdetail" }]
            }
          ]
        }
      ],
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset
    })
      .then(cases => {
        cases["count"] = count;
        res.status(200).json({
          success: 1,
          pages: pages,
          data: cases,
          message: "List of cases for a particular client"
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json(err);
      });
  });
});

router.get("/single/:id", middleware.auth, async (req, res) => {
  let caseDetail = await CaseDetails.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "clientdetail"
      },
      {
        association: "contacts"
      },
      {
        association: "principleAttorney",
        include: [{ association: "userdetail" }]
      },
      {
        association: "assistattorney",
        include: [
          {
            association: "attorney",
            as: "assistattorney",
            include: [{ association: "userdetail" }]
          }
        ]
      }
    ]
  });
  res.status(200).json({
    success: 1,
    data: caseDetail,
    message: ""
  });
});

router.put("/:id", [middleware.auth], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ success: 0, errors: errors.array() });
  }

  let caseDetail = await CaseDetails.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "assistattorney",
        attributes: ["attorney_id"]
      }
    ]
  });

  if (caseDetail != null) {
    if (req.body.contacts != null) {
      let oldContacts = await CaseContacts.destroy({
        where: { case_id: req.params.id }
      });
      let data = [];
      for (let item of req.body.contacts) {
        data.push({
          case_id: req.params.id,
          c_id: item.c_id,
          c_type: item.c_type
        });
      }
      let result = await CaseContacts.bulkCreate(data);
    }
    caseDetail.update({
      client_no: req.body.client_no,
      category_id: req.body.category_id,
      priattorney: req.body.priattorney
    });
    //Update Assistant Attorneys
    let assistattorney = req.body.asstattorney;
    if (caseDetail.assistattorney != null) {
      assistattorneys = await CaseAssistAttorneys.destroy({
        where: { case_id: req.params.id }
      });
    }
    let new_assist_attorney = [];
    for (let row of assistattorney) {
      new_assist_attorney = await CaseAssistAttorneys.create({
        case_id: req.params.id,
        attorney_id: row.attorney_id
      });
    }

    res.json({
      success: 1,
      data: caseDetail,
      message: "Matter is updated"
    });
  } else {
    res.json({
      success: 0,
      data: null,
      message: "Matter is not found"
    });
  }
});

router.post("/thirdparty/:caseID", [middleware.auth], async (req, res) => {
  CaseThirdparty.findOne({
    where: {
      $and: [
        { tp_id: req.body.tp_id },
        { ch_id: req.body.ch_id },
        { refno: req.body.refno }
      ]
    }
  })
    .then(result => {
      if (result != null) {
        result.destroy();
        res.status(200).json({
          success: 1,
          data: result,
          message: "Record is deleted"
        });
      } else {
        res.status(200).json({
          success: 0,
          data: {},
          message: "Record is not found"
        });
      }
    })
    .catch(err => res.status(500).json(err));
});

router.patch(
  "/:matterID/change/status/close",
  [
    middleware.auth,
    [
      check("status").isLength({ min: 1 }),
      check("closuredate").isLength({ min: 1 }),
      check("closuredetails").isLength({ min: 1 }),
      check("closureby").isLength({ min: 1 })
    ]
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    CaseDetails.findOne({
      where: { id: req.params.matterID }
    })
      .then(matter => {
        if (matter == null) return res.status(404).json({});
        else {
          matter.update({
            status: req.body.status,
            closuredate: req.body.closuredate,
            closuredetails: req.body.closuredetails,
            closureby: req.body.closureby
          });
          res.status(200).json(matter);
        }
      })
      .catch(err => res.status(500).json({ error: err }));
  }
);

router.patch(
  "/:matterID/change/status",
  [middleware.auth, [check("status").isLength({ min: 1 })]],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    CaseDetails.findOne({
      where: { id: req.params.matterID }
    })
      .then(matter => {
        if (matter == null) return res.status(404).json({});
        else {
          matter.update({
            status: req.body.status
          });
          res.status(200).json(matter);
        }
      })
      .catch(err => res.status(500).json({ error: err }));
  }
);

router.get("/all/client/:id", middleware.auth, (req, res, next) => {
  CaseDetails.findAll({ where: { client_id: req.params.id } })
    .then(matters => {
      res.status(200).json(matters);
    })
    .catch(err => next(err));
});

// router.get(
//     "/id/:id/thirdparty/:tid",
//     middleware.auth,
//     async(req, res, next) => {
//         CaseContacts.findOne({
//                 where: { case_id: req.params.id, c_id: req.params.tid, c_type: "t" },
//                 include: [{ association: "clientcontact" }]
//             })
//             .then(thirdparties => res.status(200).json(thirdparties))
//             .catch(err => next(err));
//     }
// );

router.get(
  "/id/:id/thirdparty/:tid",
  middleware.auth,
  async (req, res, next) => {
    let thirdparties = await CaseContacts.findOne({
      where: { case_id: req.params.id, c_id: req.params.tid, c_type: "t" },
      include: [{ association: "clientcontact" }]
    });

    let casedetails = await CaseDetails.findOne({
      where: { id: req.params.id }
    });
    res
      .status(200)
      .json({ refno: casedetails.client_no, clientcontact: thirdparties });
    // res.status(200).json({ thirdparties: thirdparties, casedetails: casedetails });
  }
);

router.post("/transfer-contacts", async (req, res, next) => {
  try {
    let items = await CaseThirdparty.findAll();
    let data = [];
    for (let item of items) {
      data.push({ case_id: item.case_id, c_id: item.tp_id, c_type: "t" });
      data.push({ case_id: item.case_id, c_id: item.ch_id, c_type: "c" });
    }
    let result = await CaseContacts.bulkCreate(data);
    return res.status(200).json(result);
  } catch (err) {
    console.log(err);
    next(err);
  }
});

router.get("/id/:id/documents", middleware.auth, async (req, res, next) => {
  try {
    let documents = await Document.findAll({
      where: { case_id: req.params.id },
      order: [["created_at", "DESC"]],
      include: [{ association: "type" }]
    });
    return res.status(200).json(documents);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = router;
