const express = require("express");
const router = express.Router();
const multer = require("multer");
const Client = require("../../models/Client");
const ClientContact = require("../../models/ClientContact");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const Op = sequelize.Op;
const User = require("../../models/User");
const common = require("../../helpers/common");

const middleware = {
  validations: [check("firstname").isString(), check("user_id").isInt()],
  auth: checkToken,
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/clients/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});
const upload = multer({
  storage: storage,
  // limits: { fileSize: 4194304 },
  limits: { fileSize: 1024 * 1024 * 4 }, // 4 MB
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg"
    ) {
      cb(null, true);
    } else {
      cb(new Error("Only jpeg, jpg, png, Excel format files allowed!"), false);
    }
  },
});

const uploadImage = upload.single("profile_img");
let limit = 15;

router.post("/uploadImage", uploadImage, middleware.auth, (req, res) => {
  uploadImage(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(400).json(err);
    } else {
      return res.status(201).json({
        success: 1,
        data: req.file,
        message: "Image uploaded successfully",
      });
    }
  });
});

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    let userIds = await common.firmUsers(req.body.user_id);
    let client = null;
    if (req.body.email != "") {
      client = await Client.findOne({
        where: { email: req.body.email, user_id: userIds },
      });
    }

    if (client == null) {
      const NewClient = {
        companyname: req.body.companyname || "",
        companyreg: req.body.companyreg || "",
        companyvat: req.body.companyvat || "",
        firstname: req.body.firstname || "",
        surname: req.body.surname || "",
        email: req.body.email || "",
        creditlimits: req.body.creditlimits || "",
        contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
        contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
        mobile: req.body.mobile_code + "-" + req.body.mobile || "",
        dob: req.body.dob || "",
        bankdetails: req.body.bankdetails || "",
        personaldetails: req.body.personaldetails || "",
        address1: req.body.address1 || "",
        address2: req.body.address2 || "",
        city: req.body.city || "",
        zipcode: req.body.zipcode || "",
        country: req.body.country || "",
        province: req.body.province || "",
        type: req.body.type || "",
        picturepath: req.body.picturepath || "",
        isactive: 1,
        user_id: req.body.user_id || "",
        clientcontact: req.body.contacts,
      };
      let result = await Client.create(NewClient, {
        include: [{ model: ClientContact, as: "clientcontact" }],
      });
      if (result) {
        const NewContact = {
          client_id: result.id || "",
          firstname: req.body.firstname || "",
          surname: req.body.surname || "",
          email: req.body.email || "",
          creditlimits: req.body.creditlimits || "",
          contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
          contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
          mobile: req.body.mobile_code + "-" + req.body.mobile || "",
          dob: req.body.dob || "",
          bankdetails: req.body.bankdetails || "",
          personaldetails: req.body.personaldetails || "",
          address1: req.body.address1 || "",
          address2: req.body.address2 || "",
          city: req.body.city || "",
          zipcode: req.body.zipcode || "",
          country: req.body.country || "",
          province: req.body.province || "",
          type: "c",
          isactive: 1,
          user_id: req.body.user_id || "",
        };
        let result2 = await ClientContact.create(NewContact);
        res.status(201).json({
          success: 1,
          message: "Client Created",
          data: result,
          contact: result2,
        });
      }
    } else {
      res.status(200).json({
        success: 0,
        data: {},
        message: "Client is already found with the given new email address",
      });
    }
  }
);

router.get("/", middleware.auth, (req, res) => {
  let offset = 0;
  Client.count().then((count) => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    Client.findAndCountAll({
      include: [
        {
          association: "clientcontact",
        },
      ],
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
    })
      .then((clients) => {
        res.status(200).json({
          success: 1,
          pages: pages,
          data: clients,
          message: "List of clients",
        });
      })
      .catch((err) => res.status(500).json(err));
  });
});

router.get("/user/:userId", middleware.auth, async (req, res) => {
  let search = req.query.search
    ? req.query.search
    : undefined
    ? req.query.search
    : "";
  let offset = 0;
  let userIds = await common.firmUsers(req.params.userId);
  let cond = {};
  if (search !== "") {
    cond = {
      $or: [
        {
          email: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          companyname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          firstname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          surname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          mobile: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          address1: {
            [Op.like]: "%" + search + "%",
          },
        },
      ],
      user_id: userIds,
    };
  } else {
    cond = {
      user_id: userIds,
    };
  }

  //console.log(cond);

  Client.count({
    where: cond,
    //user_id: userIds
  }).then((count) => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    Client.findAndCountAll({
      include: [
        {
          association: "clientcontact",
        },
      ],
      where: cond,
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
    })
      .then((clients) => {
        clients["count"] = count;
        res.status(200).json({
          success: 1,
          pages: pages,
          data: clients,
          search: cond,
          user: userIds,
          message: "List of clients for a particular user",
        });
      })
      .catch((err) => res.status(500).json(err));
  });
});

router.get("/:id", middleware.auth, (req, res) => {
  Client.findOne({
    where: { id: req.params.id },
    include: [
      {
        association: "clientcontact",
      },
    ],
    paranoid: false,
  })
    .then((client) => {
      res.status(200).json({
        success: 1,
        data: client,
        message: "Client is found",
      });
    })
    .catch((err) => res.status(500).json(err));
});

router.patch(
  "/:id/status",
  [middleware.auth, middleware.validations],
  (req, res) => {
    Client.findOne({
      where: { id: req.params.id },
      paranoid: false,
    })
      .then((client) => {
        if (client != null) {
          client.update({ isactive: req.body.status });
          if (req.body.status == 1) client.restore();
          client.setDataValue("deleted_at", null);
          res.status(200).json({
            success: 1,
            data: client,
            message: "Client Status is updated",
          });
        } else {
          res.status(200).json({
            success: 0,
            data: {},
            message: "Client is not found",
          });
        }
      })
      .catch((err) => res.status(500).json(err));
  }
);

router.put(
  "/:id",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    let client = await Client.findOne({
      where: { id: req.params.id },
      include: [
        {
          association: "clientcontact",
        },
      ],
    });
    if (client != null) {
      let userIds = await common.firmUsers(client.user_id);
      let dob = req.body.dob;
      if (typeof dob === "object" && dob != null) {
        dob =
          dob.year +
          "-" +
          (dob.month < 10 ? "0" + dob.month : dob.month) +
          "-" +
          (dob.day < 10 ? "0" + dob.day : dob.day);
      }
      const UpdatedClient = {
        companyname: req.body.companyname || "",
        companyreg: req.body.companyreg || "",
        companyvat: req.body.companyvat || "",
        firstname: req.body.firstname || "",
        surname: req.body.surname || "",
        email: req.body.email || "",
        creditlimits: req.body.creditlimits || "",
        contact1: req.body.contact1_code + "-" + req.body.contact1 || "",
        contact2: req.body.contact2_code + "-" + req.body.contact2 || "",
        mobile: req.body.mobile_code + "-" + req.body.mobile || "",
        dob: dob || "",
        bankdetails: req.body.bankdetails || "",
        personaldetails: req.body.personaldetails || "",
        address1: req.body.address1 || "",
        address2: req.body.address2 || "",
        city: req.body.city || "",
        zipcode: req.body.zipcode || "",
        country: req.body.country || "",
        province: req.body.province || "",
        picturepath: req.body.picturepath || "",
      };
      if (client.email == req.body.email) {
        client.update(UpdatedClient);

        res.status(200).json({
          success: 1,
          data: client,
          message: "Client Info is updated",
        });
      } else if (req.body.email != "") {
        let fclient = await Client.findOne({
          where: { email: req.body.email, user_id: userIds },
          include: [
            {
              association: "clientcontact",
            },
          ],
        });
        if (fclient == null) {
          client.update(UpdatedClient);
          res.status(200).json({
            success: 1,
            data: client,
            message: "Client Info is updated",
          });
        } else {
          res.status(200).json({
            success: 0,
            data: {},
            message: "Client is already found with the given new email address",
          });
        }
      } else {
        client.update(UpdatedClient);
        res.status(200).json({
          success: 1,
          data: client,
          message: "Client Info is updated",
        });
      }
    } else {
      res.status(200).json({
        success: 0,
        data: {},
        message: "Client is not found",
      });
    }
  }
);

router.delete("/:id", middleware.auth, (req, res) => {
  if (req.params.id) {
    Client.findOne({
      where: { id: req.params.id },
    })
      .then((client) => {
        if (client != null) {
          client.update({ isactive: 0 });
          client.destroy();
          res.status(200).json({
            success: 1,
            data: client,
            message: "Client is deleted",
          });
        } else {
          res.status(200).json({
            success: 0,
            data: {},
            message: "Client is not found",
          });
        }
      })
      .catch((err) => res.status(500).json(err));
  } else {
    res.status(500).json({
      success: 0,
      data: {},
      message: "Client Id is not found in the request",
    });
  }
});

router.delete(
  "/bulk/delete",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Client.findOne({
      where: {
        id: {
          [Op.in]: req.body.ids,
        },
      },
    })
      .then((client) => {
        if (client != null) {
          client.update({ isactive: 0 });
          client.destroy();
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Client are not found",
          });
        }
      })
      .catch((err) => next(err));
  }
);

router.patch(
  "/bulk/active",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Client.findOne({
      where: {
        id: {
          [Op.in]: req.body.ids,
        },
      },
    })
      .then((client) => {
        if (client != null) {
          client.update({ isactive: 1 });
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Client are not found",
          });
        }
      })
      .catch((err) => next(err));
  }
);

router.patch(
  "/bulk/inactive",
  [middleware.auth, [check("ids").isArray()]],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Client.findOne({
      where: {
        id: {
          [Op.in]: req.body.ids,
        },
      },
    })
      .then((client) => {
        if (client != null) {
          client.update({ isactive: 0 });
          res.status(204).json({});
        } else {
          res.status(404).json({
            error: "Users are not found",
          });
        }
      })
      .catch((err) => next(err));
  }
);

router.get("/all/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.userID);
    let clients = await Client.findAll({
      where: { user_id: userIds, isactive: 1 },
      order: [["firstname", "ASC"]],
    });
    let results = [];
    for (let client of clients) {
      results.push({
        address1: client.address1,
        address2: client.address2,
        bankdetails: client.bankdetails,
        city: client.city,
        companyname: client.companyname,
        companyreg: client.companyreg,
        companyvat: client.companyvat,
        contact1: client.contact1,
        contact2: client.contact2,
        country: client.country,
        created_at: client.created_at,
        creditlimits: client.creditlimits,
        deleted_at: client.deleted_at,
        dob: client.dob,
        email: client.email,
        firstname: client.type == "c" ? client.companyname : client.firstname,
        id: client.id,
        isactive: client.isactive,
        mobile: client.mobile,
        personaldetails: client.personaldetails,
        picturepath: client.picturepath,
        province: client.province,
        surname: client.type == "c" ? "" : client.surname,
        type: client.type,
        updated_at: client.updated_at,
        user_id: client.user_id,
        zipcode: client.zipcode,
      });
    }
    res.status(200).json(results);
  } catch (err) {
    next(err);
  }
});

router.post(
  "/all/timer/user/:userID",
  middleware.auth,
  async (req, res, next) => {
    try {
      // var ids = [req.params.userID];
      // let user = await User.findOne({ where: { id: req.params.userID } });
      // if (user.parent_user_id == null) {
      //   let allusers = await User.findAll({
      //     where: { parent_user_id: req.params.userID }
      //   });
      //   for (au of allusers) {
      //     ids.push(au.id);
      //   }
      // }
      // let mainids = req.body;
      // if (mainids.length) {
      //   for (let i of mainids) {
      //     ids.push(i);
      //   }
      // }
      let ids = await common.firmUsers(req.params.userID);
      let clients = await Client.findAll({
        where: { user_id: ids, isactive: 1 },
      });
      res.status(200).json(clients);
    } catch (err) {
      next(err);
    }
  }
);

router.get("/archive/user/:userId", middleware.auth, async (req, res) => {
  let search = req.query.search
    ? req.query.search
    : undefined
    ? req.query.search
    : "";
  let offset = 0;
  let userIds = await common.firmUsers(req.params.userId);
  let cond = {};
  if (search !== "") {
    cond = {
      $or: [
        {
          email: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          companyname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          firstname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          surname: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          mobile: {
            [Op.like]: "%" + search + "%",
          },
        },
        {
          address1: {
            [Op.like]: "%" + search + "%",
          },
        },
      ],
      user_id: userIds,
      deleted_at: { $not: null },
    };
  } else {
    cond = {
      user_id: userIds,
      deleted_at: { $not: null },
    };
  }

  //console.log(cond);

  Client.count({
    where: cond,
    paranoid: false,
    //user_id: userIds
  }).then((count) => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    limit = 9999999;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    Client.findAndCountAll({
      include: [
        {
          association: "clientcontact",
        },
      ],
      where: cond,
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset,
      paranoid: false,
    })
      .then((clients) => {
        clients["count"] = count;
        res.status(200).json({
          success: 1,
          pages: pages,
          data: clients,
          search: cond,
          user: userIds,
          message: "List of archive clients for a particular user",
        });
      })
      .catch((err) => res.status(500).json(err));
  });
});

module.exports = router;
