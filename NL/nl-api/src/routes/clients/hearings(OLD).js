const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");
const moment = require("moment");
const Matter = require("../../models/CaseDetails");
const Hearing = require("../../models/Hearing");
const HearingAttorney = require("../../models/HearingAttorneys");
const User = require("../../models/User");
const UserDetail = require("../../models/UserDetail");
const CaseAssistAttorneys = require("../../models/CaseAssistAttorneys");
const CaseThirdparty = require("../../models/CaseThirdparty");
const ClientContact = require("../../models/ClientContact");
const Client = require("../../models/Client");
const Helper = require("../../helpers/dateTime");

const middleware = {
  validations: [
    check("client_id").isUUID(),
    check("matter_id").isUUID(),
    check("thirdparty_id").isUUID(),
    check("contact_id").isUUID(),
    check("court_id").isInt(),
    check("all_day_event").isInt(),
    check("notes").isString(),
    check("from_date").isString(),
    check("to_date").isString()
  ],
  auth: checkToken
};

let limit = 15;

router.get("/:clientID/matters", middleware.auth, (req, res) => {
  Matter.findAll({
    where: { client_id: req.params.clientID },
    attributes: ["id", "prefix", "casenumber"]
  })
    .then(matters => {
      if (matters != null) {
        res.json({
          success: 1,
          data: matters,
          message: "List of matters for a particular client"
        });
      } else {
        res.json({
          success: 1,
          data: null,
          message: "Files were closed"
        });
      }
    })
    .catch(err => res.json(err));
});

router.get("/:matterID/attorneys", middleware.auth, async (req, res) => {
  let principleAttorney = await Matter.findOne({
    where: { id: req.params.matterID },
    attributes: ["priattorney"],
    include: [
      {
        model: User,
        as: "principleAttorney",
        include: [{ model: UserDetail, as: "userdetail" }]
      }
    ]
  });
  let associateAttorneys = await CaseAssistAttorneys.findAll({
    where: { case_id: req.params.matterID },
    include: [
      {
        model: User,
        as: "attorney",
        include: [{ model: UserDetail, as: "userdetail" }]
      }
    ]
  });

  let result = [];
  result.push({
    id: principleAttorney.priattorney,
    name:
      principleAttorney.principleAttorney.userdetail.firstname +
      " " +
      principleAttorney.principleAttorney.userdetail.lastname
  });
  for (attorney of associateAttorneys) {
    result.push({
      id: attorney.attorney_id,
      name:
        attorney.attorney.userdetail.firstname +
        " " +
        attorney.attorney.userdetail.lastname
    });
  }
  res.json({
    success: 1,
    data: result,
    message: "Attorney Details"
  });
});

router.get("/:matterID/thirdparties", middleware.auth, async (req, res) => {
  let thirdparties = await CaseThirdparty.findAll({
    where: { case_id: req.params.matterID },
    include: [{ model: ClientContact, as: "clientthirdparty" }]
  });
  let result = [];
  if (thirdparties != null) {
    for (row of thirdparties) {
      result.push({
        id: row.tp_id,
        name:
          row.clientthirdparty.firstname ||
          "" + " " + row.clientthirdparty.surname ||
          ""
      });
    }
  }

  const dedupThings = Array.from(
    result.reduce((m, t) => m.set(t.id, t), new Map()).values()
  );

  res.json({
    success: 1,
    data: dedupThings,
    message: "List of third parties for a particular matter"
  });
});

router.get("/:matterID/contacts", middleware.auth, async (req, res) => {
  let contacts = await CaseThirdparty.findAll({
    where: { case_id: req.params.matterID },
    include: [{ model: ClientContact, as: "clientcontact" }]
  });
  let result = [];
  if (contacts != null) {
    for (row of contacts) {
      result.push({
        id: row.ch_id,
        name: row.clientcontact.firstname + " " + row.clientcontact.surname
      });
    }
  }
  res.json({
    success: 1,
    data: result,
    message: "List of contacts for a particular matter"
  });
});

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }

    if (req.body.from_time != "") {
      req.body.from_date = new Date(
        req.body.from_date +
          " " +
          Helper.convertTime(req.body.from_time) +
          ":00Z"
      );
    }
    if (req.body.to_time != "") {
      req.body.to_date = new Date(
        req.body.to_date + " " + Helper.convertTime(req.body.to_time) + ":00Z"
      );
    }

    //return res.json(req.body);

    Hearing.create(req.body, {
      include: [{ model: HearingAttorney, as: "attorneys" }]
    })
      .then(hearing => {
        res.status(201).json({
          success: 1,
          data: hearing,
          message: "New hearing created"
        });
      })
      .catch(err => res.json(err));
  }
);

router.get("/:clientID", middleware.auth, (req, res) => {
  let offset = 0;
  Hearing.count({
    where: {
      client_id: req.params.clientID
    }
  }).then(count => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    Hearing.findAndCountAll({
      where: {
        client_id: req.params.clientID
      },
      include: [
        {
          association: "matter"
        },
        {
          association: "attorneys",
          include: [
            {
              association: "attorney",
              include: [
                {
                  association: "userdetail"
                }
              ]
            }
          ]
        },
        {
          association: "court"
        }
      ],
      order: [["created_at", "DESC"]],
      limit: limit,
      distinct: true,
      offset: offset
    })
      .then(hearings => {
        res.status(200).json({
          success: 1,
          pages: pages,
          data: hearings,
          message: "List of hearings for a particular client"
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json(err);
      });
  });
});

router.patch(
  "/:hearingID/status",
  [middleware.auth, [check("status").isLength({ min: 1 })]],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ error: errors.array() });
    }

    Hearing.findOne({
      where: { id: req.params.hearingID }
    })
      .then(hearing => {
        if (hearing == null) {
          res.status(404).json({
            error: "Hearing not found"
          });
        } else {
          hearing.update({
            status: req.body.status
          });
          res.status(200).json(hearing);
        }
      })
      .catch(err => res.json(err));
  }
);

router.get("/recents/user/:userID", middleware.auth, async (req, res, next) => {
  try {
    let clients = await Client.findAll({
      where: { user_id: req.params.userID },
      attributes: ["id"]
    });

    let client_ids = [];
    for (let client of clients) {
      client_ids.push(client.id);
    }

    let hearings = await Hearing.findAll({
      where: {
        client_id: client_ids,
        from_date: {
          $gte: new Date()
        }
      },
      attributes: ["id", "client_id", "notes", "from_date", "from_time"],
      include: [
        {
          association: "clientdetail",
          attributes: ["id", "firstname", "surname", "picturepath"]
        }
      ]
    });
    return res.status(200).json(hearings);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
