const express = require("express");
const router = express.Router();
const multer = require("multer");
const Document = require("../../models/Document");
const Matter = require("../../models/CaseDetails");
const DocType = require("../../models/masters/DocType");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const sequelize = require("sequelize");

const middleware = {
  validations: [
    check("client_id").isUUID(),
    check("name").isLength({ min: 1 }),
    check("case_id").isUUID(),
    check("type_id").isInt(),
    check("description").isLength({ min: 1 }),
    check("url").isLength({ min: 1 }),
  ],
  auth: checkToken,
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads/clients/documents/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 4 }, // 4 MB
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/png" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "application/pdf" ||
      file.mimetype == "application/vnd.ms-excel" ||
      file.mimetype ==
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
      file.mimetype == "application/msword" ||
      file.mimetype ==
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    ) {
      cb(null, true);
    } else {
      cb(new Error("Only jpeg, jpg, png, Excel format files allowed!"), false);
    }
  },
});

const uploadDoc = upload.single("doc");
let limit = 999;

router.post("/upload", uploadDoc, middleware.auth, (req, res) => {
  uploadDoc(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      return res.status(200).json({ error: err });
    } else {
      return res.status(201).json(req.file);
    }
  });
});

router.post("/", [middleware.validations, middleware.auth], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ error: errors.array() });
  }

  Document.create(req.body)
    .then((document) => {
      res.status(201).json(document);
    })
    .catch((err) => res.status(500).json({ error: err }));
});

router.get("/:clientID", middleware.auth, async (req, res) => {
  limit = req.query.limit || limit;
  limit = 999999;
  let page = req.query.page || 1;
  Document.paginate({
    paginate: limit,
    page: page,
    where: { client_id: req.params.clientID },
    include: [
      { association: "matter" },
      { association: "type" },
      { association: "client" },
    ],
  })
    .then((doctypes) => {
      res.status(200).json(doctypes);
    })
    .catch((err) => res.status(500).json({ error: err }));
});

router.delete("/:id", middleware.auth, (req, res) => {
  Document.findOne({ where: { id: req.params.id } })
    .then((doc) => {
      doc.destroy();
      res.status(200).json({});
    })
    .catch((error) => res.status(500).json({ error: error }));
});

module.exports = router;
