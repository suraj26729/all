const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Notes = require("../../models/Notes");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const middleware = {
    validations: [check("content").isLength({ min: 1 })],
    auth: checkToken
};
const Hearing = require("../../models/Hearing");
const Meeting = require("../../models/dashboard/Meeting");
const Task = require("../../models/dashboard/Task");
const Timer = require("../../models/Timesheet");

router.post(
    "/", [middleware.auth, middleware.validations],
    async(req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ error: errors.array() });
        }

        Notes.create(req.body)
            .then(NewNote => {
                res.status(201).json(NewNote);
            })
            .catch(error => res.status(500).json({ error: error }));
    }
);

router.get("/client/:parentID", middleware.auth, async(req, res) => {

    Notes.findAll({
            where: { parent_id: req.params.parentID, type: "client" },
            order: [
                ["created_at", "DESC"]
            ]
        })
        .then(notes => {
            res.status(200).json(notes);
        })
        .catch(error => res.status(500).json({ error: error }));
});

router.get("/matter/:parentID", middleware.auth, async(req, res) => {
    Notes.findAndCountAll({
            where: { parent_id: req.params.parentID, type: "matter" },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        })
        .then(notes => {

            res.status(200).json(notes);
        })
        .catch(error => res.status(500).json({ error: error }));
});


router.get("/hearing/:matterId", middleware.auth, async(req, res, next) => {
    try {
        let hearingNotes = await Hearing.findAndCountAll({
            where: { matter_id: req.params.matterId },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(hearingNotes);
    } catch (err) {
        next(err);
    }
});

router.get("/meeting/:matterId", middleware.auth, async(req, res, next) => {
    try {
        let meetingNotes = await Meeting.findAndCountAll({
            where: { case_id: req.params.matterId },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(meetingNotes);
    } catch (err) {
        next(err);
    }
});

router.get("/task/:matterId", middleware.auth, async(req, res, next) => {
    try {
        let taskNotes = await Task.findAndCountAll({
            where: { case_id: req.params.matterId },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(taskNotes);
    } catch (err) {
        next(err);
    }
});

router.get("/timer/:matterId", middleware.auth, async(req, res, next) => {
    try {
        let timerNotes = await Timer.findAndCountAll({
            where: { case_id: req.params.matterId },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(timerNotes);
    } catch (err) {
        next(err);
    }
});

router.post("/MoreTimerNotes/:matterId", middleware.auth, async(req, res, next) => {
    try {
        let timerNotes = await Timer.findAndCountAll({
            where: {
                case_id: req.params.matterId,
                id: {
                    [Op.notIn]: req.body
                }
            },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(timerNotes);
    } catch (err) {
        next(err);
    }
});

router.post("/MoreTaskNotes/:matterId", middleware.auth, async(req, res, next) => {
    try {
        let taskNotes = await Task.findAndCountAll({
            where: {
                case_id: req.params.matterId,
                id: {
                    [Op.notIn]: req.body
                }
            },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(taskNotes);
    } catch (error) {
        next(error);
    }
});

router.post("/MorefileNotes/:parentID", middleware.auth, async(req, res, next) => {
    try {
        let notes = await Notes.findAndCountAll({
            where: {
                parent_id: req.params.parentID,
                id: {
                    [Op.notIn]: req.body
                }
            },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(notes);
    } catch (error) {
        next(error);
    }
});


router.post("/MoreMeetingNotes/:caseID", middleware.auth, async(req, res, next) => {
    try {
        let meetingNotes = await Meeting.findAndCountAll({
            where: {
                case_id: req.params.caseID,
                id: {
                    [Op.notIn]: req.body
                }
            },
            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(meetingNotes);
    } catch (err) {
        next(err);
    }
});

router.post("/MoreHearingNotes/:matterID", middleware.auth, async(req, res, next) => {
    //res.status(200).json(req.body);
    try {
        let hearingNotes = await Hearing.findAndCountAll({
            where: {
                matter_id: req.params.matterID,
                id: {
                    [Op.notIn]: req.body
                }
            },

            order: [
                ["created_at", "DESC"]
            ],
            limit: 2,
        });
        res.status(200).json(hearingNotes);
    } catch (err) {
        next(err);
    }
});

router.delete("/:id", middleware.auth, (req, res) => {
    Notes.findOne({
            where: { id: req.params.id }
        })
        .then(note => {
            if (note != null) {
                note.destroy();
                res.status(200).json(note);
            } else {
                res.status(404).json({});
            }
        })
        .catch(error => res.status(500).json({ error: error }));
});

module.exports = router;