const usersRoutes = require("./backend/users");
const homeRoutes = require("./backend/home");

exports.routes = (app) => {
  app.use("/api/backend/users", usersRoutes);
  app.use("/api/backend/home", homeRoutes);
};
