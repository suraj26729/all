const express = require("express");
const router = express.Router();
const Meeting = require("../../models/dashboard/Meeting");
const Matter = require("../../models/CaseDetails");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Helper = require("../../helpers/dateTime");

const middleware = {
  validations: [
    check("subject").isString(),
    check("case_id").isUUID(),
    check("contact_id").isUUID(),
    check("thirdparty_id").isUUID(),
    check("start_date").isString(),
    check("end_date").isString(),
    check("start_time").isString(),
    check("end_time").isString(),
    check("description").isString()
  ],
  auth: checkToken
};

router.get("/matters/user/:id", middleware.auth, (req, res, next) => {
  Matter.findAll({ where: { user_id: req.params.id } })
    .then(matters => {
      res.status(200).json(matters);
    })
    .catch(err => next(err));
});

router.post(
  "/",
  [middleware.validations, middleware.auth],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    if (req.body.start_time != "") {
      req.body.start_date = new Date(
        req.body.start_date +
          " " +
          Helper.convertTime(req.body.start_time) +
          ":00Z"
      );
    }
    if (req.body.end_time != "") {
      req.body.end_date = new Date(
        req.body.end_date + " " + Helper.convertTime(req.body.end_time) + ":00Z"
      );
    }

    Meeting.create(req.body, {
      include: [{ association: "assignees" }]
    })
      .then(meeting => {
        res.status(201).json(meeting);
      })
      .catch(err => next(err));
  }
);

module.exports = router;
