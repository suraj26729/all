const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const checkToken = require("../../middlewares/checkToken");
const Client = require("../../models/Client");
const Hearing = require("../../models/Hearing");
const Task = require("../../models/dashboard/Task");
const Meeting = require("../../models/dashboard/Meeting");
const Op = Sequelize.Op;
const Helper = require("../../helpers/dateTime");
const User = require("../../models/User");
const Lead = require("../../models/Leads");
const Leave = require("../../models/dashboard/Leave");
const Matter = require("../../models/CaseDetails");
const Holiday = require("../../models/masters/Holiday");
const common = require("../../helpers/common");
const moment = require("moment");

router.get("/:date/user/:userID", checkToken, async (req, res, next) => {
  let currentDate = req.params.date || "";
  let filter = req.query.filter || "all";
  let assigneeID = req.query.assigneeID || "";
  let assigneeType = req.query.assigneeType || "";
  let range = Helper.weekStartDates(currentDate);
  let start = new Date(range.start);
  let end = new Date(range.end);
  let datesArr = Helper.getDateArray(start, end);
  let timesArr = Helper.getTime24Array();
  let j = 0;
  let dateTimesArr = timesArr.map((time, i) => {
    j = j + 1;
    return { from: timesArr[i], to: timesArr[j] };
  });
  dateTimesArr.splice(-1, 1);

  let user = await User.findByPk(req.params.userID);

  if (assigneeID == "") {
    assigneeID = [req.params.userID];
    if (user.role_id == 2 && user.parent_user_id == null) {
      assigneeID = await common.firmUsers(req.params.userID);
    } else if (user.role_id == 2 && user.parent_user_id != null) {
      //Other diary admins
      assigneeID = await common.otherDiaryUserIds(req.params.userID);

      //All attorneys
      let ids = await common.firmUsers(req.params.userID);
      let firm_users = await User.findAll({ where: { id: ids, role_id: 3 } });
      for (let item of firm_users) assigneeID.push(item.id);
    } else if (user.role_id == 3 && user.parent_user_id != null) {
      //Other diary attorneys
      assigneeID = await common.otherDiaryUserIds(req.params.userID);
    } else {
      assigneeID = await common.firmUsers(req.params.userID);
    }
  }

  // let user_ids = [];
  // let admins = [];
  // let user = await User.findByPk(req.params.userID);
  // user_ids.push(user.id);
  // if (user.parent_user_id != null) {
  //   user_ids.push(user.parent_user_id);
  //   admins = await User.findAll({
  //     where: { parent_user_id: user.parent_user_id },
  //     attributes: ["id"]
  //   });
  // } else {
  //   admins = await User.findAll({
  //     where: { parent_user_id: user.id },
  //     attributes: ["id"]
  //   });
  // }
  // for (let admin of admins) {
  //   user_ids.push(admin.id);
  // }

  // user_ids = user_ids.filter(function(item, pos) {
  //   return user_ids.indexOf(item) == pos;
  // });

  let user_ids = await common.firmUsers(req.params.userID);

  //get the clients created by particular user
  let client_ids = await Client.findAll({
    where: {
      user_id: user_ids
    },
    attributes: ["id"]
  });
  client_ids = client_ids.map(row => {
    return row.id;
  });

  // let matter_clients = [];
  // let asst_matter_clients = [];

  // if (req.query.assigneeID != "all") {
  //   matter_clients = await Matter.findAll({
  //     where: {
  //       priattorney: req.params.userID
  //     },
  //     attributes: ["client_id"]
  //   });
  //   matter_clients = matter_clients.map(row => {
  //     return row.client_id;
  //   });

  //   asst_matter_clients = await Matter.findAll({
  //     attributes: ["client_id"],
  //     include: [
  //       {
  //         association: "assistattorney",
  //         where: { attorney_id: req.params.userID }
  //       }
  //     ]
  //   });
  //   asst_matter_clients = asst_matter_clients.map(row => {
  //     return row.client_id;
  //   });
  // }

  //client_ids = client_ids.concat(matter_clients);
  //client_ids = client_ids.concat(asst_matter_clients);

  client_ids = client_ids.filter(function(item, pos) {
    return client_ids.indexOf(item) == pos;
  });

  let result = [];
  let birthdays = [];
  let hearings = [];
  let tasks = [];
  let meetings = [];
  let leads = [];
  let leaves = [];

  for (date of datesArr) {
    let fdate = moment(new Date(date)).format("YYYY-MM-DD");
    // fdate =
    //   fdate.getFullYear() +
    //   "-" +
    //   (fdate.getMonth() + 1 < 10
    //     ? "0" + (fdate.getMonth() + 1)
    //     : fdate.getMonth() + 1) +
    //   "-" +
    //   (fdate.getDate() < 10 ? "0" + fdate.getDate() : fdate.getDate());
    let data = [];
    if (filter == "all" || filter == "birthday") {
      birthdays = await Client.findAll({
        where: {
          id: client_ids,
          dob: {
            [Op.like]: "%-" + moment(new Date(date)).format("MM-DD")
          }
        },
        attributes: ["id", "firstname", "surname", "email", "mobile"]
      });
    }

    for (let time of dateTimesArr) {
      let fdatetime = fdate + " " + time.from + ":00Z";
      let tdatetime = fdate + " " + time.to + ":00Z";
      if (filter == "all" || filter == "hearing") {
        let include_params = [
          {
            association: "clientdetail",
            attributes: ["id", "firstname", "surname"]
          },
          {
            association: "matter",
            attributes: ["id", "prefix", "casenumber"]
          },
          { association: "court", attributes: ["id", "name"] }
        ];
        if (user.role_id == 3) {
          //if (assigneeType == "attorney") {
          include_params.push({
            association: "attorneys",
            include: [
              {
                association: "attorney",
                attributes: ["id"],
                include: [
                  {
                    association: "userdetail",
                    attributes: ["firstname", "lastname"]
                  }
                ]
              }
            ],
            where: { attorney_id: assigneeID || user.id }
          });
          //}
          hearings = await Hearing.findAll({
            where: {
              status: "Open",
              [Op.or]: [
                { from_date: { $between: [fdatetime, tdatetime] } },
                { to_date: { $between: [fdatetime, tdatetime] } }
              ]
            },
            attributes: [
              "id",
              "notes",
              "from_date",
              "to_date",
              "from_time",
              "to_time",
              "status",
              "created_at"
            ],
            include: include_params
          });
        } else {
          //if (assigneeID && assigneeType == "attorney") {
          include_params.push({
            association: "attorneys",
            include: [
              {
                association: "attorney",
                attributes: ["id"],
                include: [
                  {
                    association: "userdetail",
                    attributes: ["firstname", "lastname"]
                  }
                ]
              }
            ],
            where: { attorney_id: assigneeID }
          });
          //}
          hearings = await Hearing.findAll({
            where: {
              client_id: client_ids,
              status: "Open",
              [Op.or]: [
                { from_date: { $between: [fdatetime, tdatetime] } },
                { to_date: { $between: [fdatetime, tdatetime] } }
              ]
            },
            attributes: [
              "id",
              "notes",
              "from_date",
              "to_date",
              "from_time",
              "to_time",
              "status",
              "created_at"
            ],
            include: include_params
          });
        }
      }
      if (filter == "all" || filter == "task") {
        if (user.role_id == 3) {
          tasks = await Task.findAll({
            where: {
              [Op.or]: [
                { start_date: { $between: [fdatetime, tdatetime] } },
                { end_date: { $between: [fdatetime, tdatetime] } }
              ]
            },
            attributes: [
              "id",
              "subject",
              "description",
              "location",
              "start_date",
              "end_date",
              "start_time",
              "end_time",
              "created_at"
            ],
            include: [
              {
                association: "clientdetail",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "matter",
                attributes: ["id", "prefix", "casenumber"]
              },
              {
                association: "contact",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "thirdparty",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "assignees",
                include: [
                  {
                    association: "assignee",
                    include: [{ association: "userdetail" }]
                  }
                ],
                where: { user_id: assigneeID || user.id }
              },
              {
                association: "user",
                attributes: ["id"],
                include: [
                  {
                    association: "userdetail",
                    attributes: ["firstname", "lastname"]
                  }
                ]
              }
            ]
          });
        } else {
          tasks = await Task.findAll({
            where: {
              [Op.or]: [
                { start_date: { $between: [fdatetime, tdatetime] } },
                { end_date: { $between: [fdatetime, tdatetime] } }
              ]
            },
            attributes: [
              "id",
              "subject",
              "description",
              "location",
              "start_date",
              "end_date",
              "start_time",
              "end_time",
              "created_at"
            ],
            include: [
              {
                association: "clientdetail",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "matter",
                attributes: ["id", "prefix", "casenumber"]
              },
              {
                association: "contact",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "thirdparty",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "assignees",
                include: [
                  {
                    association: "assignee",
                    include: [{ association: "userdetail" }]
                  }
                ],
                where: { user_id: assigneeID || user_ids }
              },
              {
                association: "user",
                attributes: ["id"],
                include: [
                  {
                    association: "userdetail",
                    attributes: ["firstname", "lastname"]
                  }
                ]
              }
            ]
          });
        }
      }
      if (filter == "all" || filter == "meeting") {
        if (user.role_id == 3) {
          meetings = await Meeting.findAll({
            where: {
              [Op.or]: [
                { start_date: { $between: [fdatetime, tdatetime] } },
                { end_date: { $between: [fdatetime, tdatetime] } }
              ]
            },
            attributes: [
              "id",
              "subject",
              "description",
              "location",
              "start_date",
              "end_date",
              "start_time",
              "end_time",
              "created_at"
            ],
            include: [
              {
                association: "matter",
                attributes: ["id", "prefix", "casenumber"]
              },
              {
                association: "contact",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "thirdparty",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "assignees",
                include: [
                  {
                    association: "assignee",
                    include: [{ association: "userdetail" }]
                  }
                ],
                where: { user_id: assigneeID || user.id }
              },
              {
                association: "user",
                attributes: ["id"],
                include: [
                  {
                    association: "userdetail",
                    attributes: ["firstname", "lastname"]
                  }
                ]
              }
            ]
          });
        } else {
          meetings = await Meeting.findAll({
            where: {
              [Op.or]: [
                { start_date: { $between: [fdatetime, tdatetime] } },
                { end_date: { $between: [fdatetime, tdatetime] } }
              ]
            },
            attributes: [
              "id",
              "subject",
              "description",
              "location",
              "start_date",
              "end_date",
              "start_time",
              "end_time",
              "created_at"
            ],
            include: [
              {
                association: "matter",
                attributes: ["id", "prefix", "casenumber"]
              },
              {
                association: "contact",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "thirdparty",
                attributes: ["id", "firstname", "surname"]
              },
              {
                association: "assignees",
                include: [
                  {
                    association: "assignee",
                    include: [{ association: "userdetail" }]
                  }
                ],
                where: { user_id: assigneeID || user_ids }
              },
              {
                association: "user",
                attributes: ["id"],
                include: [
                  {
                    association: "userdetail",
                    attributes: ["firstname", "lastname"]
                  }
                ]
              }
            ]
          });
        }
      }
      if (filter == "all" || filter == "lead") {
        if (user.role_id == 3) {
          let cond = {};
          if (assigneeID && assigneeType == "attorney") {
            cond = {
              assignee_id: assigneeID,
              consult_date: { $between: [fdatetime, tdatetime] }
            };
          } else if (assigneeID && assigneeType == "admin") {
            cond = {
              user_id: assigneeID,
              consult_date: { $between: [fdatetime, tdatetime] }
            };
          } else {
            cond = {
              assignee_id: user.id,
              consult_date: { $between: [fdatetime, tdatetime] }
            };
          }
          leads = await Lead.findAll({
            where: cond,
            attributes: [
              "id",
              "uuid",
              "created_at",
              "consult_date",
              "firstname",
              "surname",
              "email",
              "address1",
              "location",
              "consultation_booked",
              "client_arrive",
              "converted",
              "status"
            ],
            include: [
              {
                association: "category"
              }
            ]
          });
        } else {
          let cond = {};
          if (assigneeID && assigneeType == "attorney") {
            cond = {
              assignee_id: assigneeID,
              consult_date: { $between: [fdatetime, tdatetime] }
            };
          } else if (assigneeID && assigneeType == "admin") {
            cond = {
              user_id: assigneeID,
              consult_date: { $between: [fdatetime, tdatetime] }
            };
          } else {
            cond = {
              user_id: user_ids,
              consult_date: { $between: [fdatetime, tdatetime] }
            };
          }

          leads = await Lead.findAll({
            where: cond,
            attributes: [
              "id",
              "uuid",
              "created_at",
              "consult_date",
              "firstname",
              "surname",
              "email",
              "address1",
              "location",
              "consultation_booked",
              "client_arrive",
              "converted",
              "status"
            ],
            include: [
              {
                association: "category"
              }
            ]
          });
        }
      }

      if (filter == "all" || filter == "leave") {
        let userIds = [];
        if (user.role_id == 3) {
          let matter = await Matter.findAll({
            where: { priattorney: user.id },
            include: [{ association: "assistattorney" }]
          });
          if (matter) {
            if (matter.assistattorney) {
              userIds = matter.assistattorney.map(item => item.attorney_id);
            }
            userIds.push(matter.user_id);
            userIds.push(user.id);
          }
          leaves = await Leave.findAll({
            where: {
              [Op.or]: [
                { from_date: { $between: [fdatetime, tdatetime] } },
                { to_date: { $between: [fdatetime, tdatetime] } }
              ],
              user_id: userIds
            },
            include: [
              {
                association: "user",
                include: [{ association: "userdetail" }]
              },
              {
                association: "author",
                include: [{ association: "userdetail" }]
              }
            ]
          });
        } else {
          if (user.parent_user_id != null) {
            userIds.push(user.parent_user_id);
            let subadmins = await User.findAll({
              where: { parent_user_id: user.parent_user_id },
              role_id: 3
            });
            for (let item of subadmins) userIds.push(item.id);
            let attorneys = await User.findAll({
              where: {
                parent_user_id: [user.id, user.parent_user_id],
                role_id: 3
              }
            });
            for (let item of attorneys) userIds.push(item.id);
          } else {
            userIds.push(user.id);
            let subadmins = await User.findAll({
              where: { parent_user_id: user.id },
              role_id: 3
            });
            for (let item of subadmins) userIds.push(item.id);
            let attorneys = await User.findAll({
              where: {
                parent_user_id: user.id,
                role_id: 3
              }
            });
            for (let item of attorneys) userIds.push(item.id);
          }
          leaves = await Leave.findAll({
            where: {
              user_id: userIds,
              [Op.or]: [
                { from_date: { $between: [fdatetime, tdatetime] } },
                { to_date: { $between: [fdatetime, tdatetime] } }
              ]
            },
            include: [
              {
                association: "user",
                include: [{ association: "userdetail" }]
              },
              {
                association: "author",
                include: [{ association: "userdetail" }]
              }
            ]
          });
        }
      }

      let userIds = [];
      if (user.parent_user_id != null) {
        userIds.push(user.parent_user_id);
        let subadmins = await User.findAll({
          where: { parent_user_id: user.parent_user_id },
          role_id: 3
        });
        for (let item of subadmins) userIds.push(item.id);
        let attorneys = await User.findAll({
          where: {
            parent_user_id: [user.id, user.parent_user_id],
            role_id: 3
          }
        });
        for (let item of attorneys) userIds.push(item.id);
      } else {
        userIds.push(user.id);
        let subadmins = await User.findAll({
          where: { parent_user_id: user.id },
          role_id: 3
        });
        for (let item of subadmins) userIds.push(item.id);
        let attorneys = await User.findAll({
          where: {
            parent_user_id: user.id,
            role_id: 3
          }
        });
        for (let item of attorneys) userIds.push(item.id);
      }

      let holidays = await Holiday.findAll({
        where: {
          user_id: userIds,
          [Op.or]: [
            { start_date: { $between: [fdatetime, tdatetime] } },
            { end_date: { $between: [fdatetime, tdatetime] } }
          ]
        }
      });

      if (time.from == "10:00" && time.to == "11:00") {
        if (
          hearings.length ||
          tasks.length ||
          meetings.length ||
          birthdays.length ||
          leads.length ||
          leads.length ||
          leaves.length ||
          holidays.length
        ) {
          data.push({
            time: time.from + " - " + time.to,
            hearings: hearings,
            tasks: tasks,
            meetings: meetings,
            birthdays: birthdays,
            leads: leads,
            leaves: leaves,
            holidays: holidays
          });
        }
      } else {
        if (
          hearings.length ||
          tasks.length ||
          meetings.length ||
          leads.length ||
          leads.length ||
          leaves.length ||
          holidays.length
        ) {
          data.push({
            time: time.from + " - " + time.to,
            hearings: hearings,
            tasks: tasks,
            meetings: meetings,
            leads: leads,
            leaves: leaves,
            holidays: holidays
          });
        }
      }
    }
    if (data.length) result.push({ date: date, data: data });
  }

  res.json(result);
});

module.exports = router;
