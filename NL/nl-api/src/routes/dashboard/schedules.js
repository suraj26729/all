const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const User = require("../../models/User");
const Hearing = require("../../models/Hearing");
const Task = require("../../models/dashboard/Task");
const Meeting = require("../../models/dashboard/Meeting");
const moment = require("moment");
const common = require("../../helpers/common");
const checkToken = require("../../middlewares/checkToken");

router.get("/user/:userId", checkToken, async (req, res, next) => {
  let today = moment(new Date())
    .startOf("day")
    .format("YYYY-MM-DD H:mm:ss");
  let tomorrow = moment(new Date())
    .add(1, "day")
    .endOf("day")
    .format("YYYY-MM-DD H:mm:ss");

  let user = await User.findByPk(req.params.userId, {
    include: [{ association: "userdetail" }]
  });

  let userIds = [req.params.userId];
  if (user.role_id == 2 && user.parent_user_id == null) {
    userIds = await common.firmUsers(req.params.userId);
  } else if (user.role_id == 2 && user.parent_user_id != null) {
    //Other diary admins
    userIds = await common.otherDiaryUserIds(req.params.userId);

    //All attorneys
    let ids = await common.firmUsers(req.params.userId);
    let firm_users = await User.findAll({ where: { id: ids, role_id: 3 } });
    for (let item of firm_users) userIds.push(item.id);
  } else if (user.role_id == 3 && user.parent_user_id != null) {
    //Other diary attorneys
    userIds = await common.otherDiaryUserIds(req.params.userId);
  } else {
    userIds = await common.firmUsers(req.params.userId);
  }

  let attorneys = await User.findAll({
    attributes: ["id"],
    where: { id: userIds, role_id: 3, isactive: 1 },
    include: [
      { association: "userdetail", attributes: ["firstname", "lastname"] }
    ]
  });

  attorneys = attorneys.map(item => {
    return {
      id: item.id,
      name: item.userdetail.firstname + " " + item.userdetail.lastname
    };
  });

  let admins = await User.findAll({
    attributes: ["id"],
    where: {
      id: userIds,
      role_id: 2,
      parent_user_id: { $not: null },
      isactive: 1
    },
    include: [
      { association: "userdetail", attributes: ["firstname", "lastname"] }
    ]
  });

  admins = admins.map(item => {
    return {
      id: item.id,
      name: item.userdetail.firstname + " " + item.userdetail.lastname
    };
  });

  let assignee_id = req.query.assignee || req.params.userId;

  let schedules_hearings = await Hearing.findAll({
    where: {
      //user_id: req.params.userId,
      $or: [
        { from_date: { $between: [today, tomorrow] } },
        { to_date: { $between: [today, tomorrow] } }
      ]
    },
    include: [
      {
        association: "clientdetail",
        attributes: ["id", "firstname", "surname", "type", "companyname"]
      },
      {
        association: "matter",
        attributes: ["id", "prefix", "casenumber"]
      },
      { association: "court", attributes: ["id", "name"] },
      {
        association: "attorneys",
        where: { attorney_id: [assignee_id] }
      }
    ]
  });
  let schedules_tasks = await Task.findAll({
    where: {
      //user_id: req.params.userId,
      $or: [
        { start_date: { $between: [today, tomorrow] } },
        { end_date: { $between: [today, tomorrow] } }
      ]
    },
    include: [
      {
        association: "assignees",
        where: { user_id: [assignee_id] }
      }
    ]
  });
  let schedules_meetings = await Meeting.findAll({
    where: {
      //user_id: req.params.userId,
      $or: [
        { start_date: { $between: [today, tomorrow] } },
        { end_date: { $between: [today, tomorrow] } }
      ]
    },
    include: [
      {
        association: "assignees",
        where: { user_id: [assignee_id] }
      }
    ]
  });

  return res.status(200).json({
    dates: [today, tomorrow],
    hearings: schedules_hearings,
    tasks: schedules_tasks,
    meetings: schedules_meetings,
    attorneys: attorneys,
    admins: admins,
    user: user
  });
});

module.exports = router;
