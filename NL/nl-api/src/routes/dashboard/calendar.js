const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const moment = require("moment");
const checkToken = require("../../middlewares/checkToken");
const Client = require("../../models/Client");
const Hearing = require("../../models/Hearing");
const Task = require("../../models/dashboard/Task");
const TaskAssignee = require("../../models/dashboard/TaskAssignee");
const TaskPendingAssignee = require("../../models/dashboard/TaskPendingAssignee");
const Meeting = require("../../models/dashboard/Meeting");
const MeetingAssignee = require("../../models/dashboard/MeetingAssignee");
const Op = Sequelize.Op;
const Helper = require("../../helpers/dateTime");
const User = require("../../models/User");
const Lead = require("../../models/Leads");
const Leave = require("../../models/dashboard/Leave");
const Matter = require("../../models/CaseDetails");
const Holiday = require("../../models/masters/Holiday");
const common = require("../../helpers/common");

const populateDates = (startDate, endDate) => {
  var dateArray = new Array();
  var currentDate = startDate;
  while (currentDate <= endDate) {
    dateArray.push(moment(currentDate).format("YYYY-MM-DD"));
    currentDate.setDate(currentDate.getDate() + 1);
  }
  return dateArray;
};

router.get(
  "/:date/view/month/user/:userID",
  checkToken,
  async (req, res, next) => {
    let currentDate = req.params.date || "";
    let filter = req.query.filter || "all";
    let assigneeID = req.query.assigneeID || "";
    let assigneeType = req.query.assigneeType || "";
    let startDate = moment(new Date(currentDate))
      .startOf("month")
      .startOf("week")
      .format("YYYY-MM-DD H:mm:ss");
    let endDate = moment(new Date(currentDate))
      .endOf("month")
      .endOf("week")
      .format("YYYY-MM-DD H:mm:ss");

    let user = await User.findByPk(req.params.userID);

    if (assigneeID == "") {
      assigneeID = [req.params.userID];
      if (user.role_id == 2 && user.parent_user_id == null) {
        assigneeID = await common.firmUsers(req.params.userID);
      } else if (user.role_id == 2 && user.parent_user_id != null) {
        //Other diary admins
        assigneeID = await common.otherDiaryUserIds(req.params.userID);

        //All attorneys
        let ids = await common.firmUsers(req.params.userID);
        let firm_users = await User.findAll({ where: { id: ids, role_id: 3 } });
        for (let item of firm_users) assigneeID.push(item.id);
      } else if (user.role_id == 3 && user.parent_user_id != null) {
        //Other diary attorneys
        assigneeID = await common.otherDiaryUserIds(req.params.userID);
      } else {
        assigneeID = await common.firmUsers(req.params.userID);
      }
    }

    // let user_ids = [];
    // let admins = [];
    // let user = await User.findByPk(req.params.userID);
    // user_ids.push(user.id);
    // if (user.parent_user_id != null) {
    //   user_ids.push(user.parent_user_id);
    //   admins = await User.findAll({
    //     where: { parent_user_id: user.parent_user_id },
    //     attributes: ["id"]
    //   });
    // } else {
    //   admins = await User.findAll({
    //     where: { parent_user_id: user.id },
    //     attributes: ["id"]
    //   });
    // }
    // for (let admin of admins) {
    //   user_ids.push(admin.id);
    // }

    // user_ids = user_ids.filter(function(item, pos) {
    //   return user_ids.indexOf(item) == pos;
    // });

    let user_ids = await common.firmUsers(req.params.userID);

    //get the clients created by particular user
    let client_ids = await Client.findAll({
      where: {
        user_id: user_ids
      },
      attributes: ["id"]
    });
    client_ids = client_ids.map(row => {
      return row.id;
    });
    client_ids = client_ids.filter(function(item, pos) {
      return client_ids.indexOf(item) == pos;
    });

    let birthdays = [];
    let hearings = [];
    let tasks = [];
    let meetings = [];
    let leads = [];
    let leaves = [];
    let wfms = [];
    let holidays = [];

    if (filter == "all" || filter == "birthday") {
      birthdays = await Client.findAll({
        where: {
          id: client_ids,
          dob: { $between: [startDate, endDate] }
        },
        attributes: ["id", "firstname", "surname", "dob", "email", "mobile"]
      });
    }
    if (filter == "all" || filter == "hearing") {
      let include_params = [
        {
          association: "clientdetail",
          attributes: ["id", "firstname", "surname", "companyname", "type"]
        },
        {
          association: "matter",
          attributes: ["id", "prefix", "casenumber"]
        },
        { association: "court", attributes: ["id", "name"] },
        {
          association: "user",
          attributes: ["id"],
          include: [
            { association: "userdetail", attributes: ["firstname", "lastname"] }
          ]
        },
        {
          association: "contact"
        },
        {
          association: "thirdparty"
        }
      ];
      if (user.role_id == 3) {
        //if (assigneeType == "attorney") {
        include_params.push({
          association: "attorneys",
          include: [
            {
              association: "attorney",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"]
                }
              ]
            }
          ],
          where: { attorney_id: assigneeID || user.id }
        });
        //}
        hearings = await Hearing.findAll({
          where: {
            status: "Open",
            [Op.or]: [
              { from_date: { $between: [startDate, endDate] } },
              { to_date: { $between: [startDate, endDate] } }
            ]
          },
          attributes: [
            "id",
            "notes",
            "from_date",
            "to_date",
            "from_time",
            "to_time",
            "status",
            "created_at"
          ],
          include: include_params
        });
      } else {
        //if (assigneeID && assigneeType == "attorney") {
        include_params.push({
          association: "attorneys",
          include: [
            {
              association: "attorney",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"]
                }
              ]
            }
          ],
          where: { attorney_id: assigneeID }
        });
        //}
        hearings = await Hearing.findAll({
          where: {
            client_id: client_ids,
            status: "Open",
            [Op.or]: [
              { from_date: { $between: [startDate, endDate] } },
              { to_date: { $between: [startDate, endDate] } }
            ]
          },
          attributes: [
            "id",
            "notes",
            "from_date",
            "to_date",
            "from_time",
            "to_time",
            "status",
            "created_at"
          ],
          include: include_params
        });
      }
    }
    if (filter == "all" || filter == "task") {
      if (user.role_id == 3) {
        tasks = await Task.findAll({
          where: {
            [Op.or]: [
              { start_date: { $between: [startDate, endDate] } },
              { end_date: { $between: [startDate, endDate] } }
            ]
          },
          attributes: [
            "id",
            "subject",
            "description",
            "location",
            "board_room",
            "start_date",
            "end_date",
            "start_time",
            "end_time",
            "created_at"
          ],
          include: [
            {
              association: "clientdetail",
              attributes: ["id", "firstname", "surname", "companyname", "type"]
            },
            {
              association: "matter",
              attributes: ["id", "prefix", "casenumber"]
            },
            {
              association: "contact",
              attributes: ["id", "firstname", "surname"]
            },
            {
              association: "thirdparty",
              attributes: ["id", "firstname", "surname"]
            },
            {
              association: "assignees",
              include: [
                {
                  association: "assignee",
                  include: [{ association: "userdetail" }]
                }
              ],
              where: { status: { $not: 1 }, user_id: assigneeID || user.id }
            },
            {
              association: "user",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"]
                }
              ]
            }
          ]
        });
      } else {
        tasks = await Task.findAll({
          where: {
            [Op.or]: [
              { start_date: { $between: [startDate, endDate] } },
              { end_date: { $between: [startDate, endDate] } }
            ]
          },
          attributes: [
            "id",
            "subject",
            "description",
            "location",
            "board_room",
            "start_date",
            "end_date",
            "start_time",
            "end_time",
            "created_at"
          ],
          include: [
            {
              association: "clientdetail",
              attributes: ["id", "firstname", "surname", "companyname", "type"]
            },
            {
              association: "matter",
              attributes: ["id", "prefix", "casenumber"]
            },
            {
              association: "contact",
              attributes: ["id", "firstname", "surname"]
            },
            {
              association: "thirdparty",
              attributes: ["id", "firstname", "surname"]
            },
            {
              association: "assignees",
              include: [
                {
                  association: "assignee",
                  include: [{ association: "userdetail" }]
                }
              ],
              where: { status: { $not: 1 }, user_id: assigneeID || user_ids }
            },
            {
              association: "user",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"]
                }
              ]
            }
          ]
        });
      }
    }
    if (filter == "all" || filter == "meeting") {
      if (user.role_id == 3) {
        meetings = await Meeting.findAll({
          where: {
            [Op.or]: [
              { start_date: { $between: [startDate, endDate] } },
              { end_date: { $between: [startDate, endDate] } }
            ]
          },
          attributes: [
            "id",
            "subject",
            "description",
            "location",
            "board_room",
            "start_date",
            "end_date",
            "start_time",
            "end_time",
            "created_at"
          ],
          include: [
            {
              association: "matter",
              attributes: ["id", "prefix", "casenumber"]
            },
            {
              association: "contact",
              attributes: ["id", "firstname", "surname"]
            },
            {
              association: "thirdparty",
              attributes: ["id", "firstname", "surname"]
            },
            {
              association: "assignees",
              include: [
                {
                  association: "assignee",
                  include: [{ association: "userdetail" }]
                }
              ],
              where: { user_id: assigneeID || user.id }
            },
            {
              association: "user",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"]
                }
              ]
            }
          ]
        });
      } else {
        meetings = await Meeting.findAll({
          where: {
            [Op.or]: [
              { start_date: { $between: [startDate, endDate] } },
              { end_date: { $between: [startDate, endDate] } }
            ]
          },
          attributes: [
            "id",
            "subject",
            "description",
            "location",
            "board_room",
            "start_date",
            "end_date",
            "start_time",
            "end_time",
            "created_at"
          ],
          include: [
            {
              association: "matter",
              attributes: ["id", "prefix", "casenumber"]
            },
            {
              association: "contact",
              attributes: ["id", "firstname", "surname"]
            },
            {
              association: "thirdparty",
              attributes: ["id", "firstname", "surname"]
            },
            {
              association: "assignees",
              include: [
                {
                  association: "assignee",
                  include: [{ association: "userdetail" }]
                }
              ],
              where: { user_id: assigneeID || user_ids }
            },
            {
              association: "user",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"]
                }
              ]
            }
          ]
        });
      }
    }

    if (filter == "all" || filter == "lead") {
      if (user.role_id == 3) {
        let cond = {};
        if (assigneeID && assigneeType == "attorney") {
          cond = {
            assignee_id: assigneeID,
            consult_date: { $between: [startDate, endDate] }
          };
        } else if (assigneeID && assigneeType == "admin") {
          cond = {
            user_id: assigneeID,
            consult_date: { $between: [startDate, endDate] }
          };
        } else {
          cond = {
            assignee_id: user.id,
            consult_date: { $between: [startDate, endDate] }
          };
        }
        leads = await Lead.findAll({
          where: cond,
          attributes: [
            "id",
            "uuid",
            "lead_type",
            "mobile",
            "created_at",
            "consult_date",
            "firstname",
            "surname",
            "email",
            "contact1",
            "contact2",
            "address1",
            "location",
            "consultation_booked",
            "client_arrive",
            "converted",
            "status"
          ],
          include: [
            {
              association: "category"
            },
            {
              association: "assignee",
              include: [{ association: "userdetail" }]
            },
            {
              association: "user",
              include: [{ association: "userdetail" }]
            }
          ]
        });
      } else {
        let cond = {};
        if (assigneeID && assigneeType == "attorney") {
          cond = {
            assignee_id: assigneeID,
            consult_date: { $between: [startDate, endDate] }
          };
        } else if (assigneeID && assigneeType == "admin") {
          cond = {
            user_id: assigneeID,
            consult_date: { $between: [startDate, endDate] }
          };
        } else {
          cond = {
            user_id: user_ids,
            consult_date: { $between: [startDate, endDate] }
          };
        }
        leads = await Lead.findAll({
          where: cond,
          attributes: [
            "id",
            "uuid",
            "lead_type",
            "mobile",
            "created_at",
            "consult_date",
            "firstname",
            "surname",
            "email",
            "contact1",
            "contact2",
            "address1",
            "location",
            "consultation_booked",
            "client_arrive",
            "converted",
            "status"
          ],
          include: [
            {
              association: "category"
            },
            {
              association: "assignee",
              include: [{ association: "userdetail" }]
            },
            {
              association: "user",
              include: [{ association: "userdetail" }]
            }
          ]
        });
      }
    }

    if (filter == "all" || filter == "leave" || filter == "wfm") {
      let userIds = [];
      if (user.role_id == 3) {
        let matters = await Matter.findAll({
          where: { priattorney: user.id },
          include: [{ association: "assistattorney" }]
        });
        if (matters.length) {
          for (let matter of matters) {
            if (
              matter.assistattorney != null &&
              matter.assistattorney != undefined &&
              matter.assistattorney.length
            ) {
              userIds = matter.assistattorney.map(item => item.attorney_id);
            }
            userIds.push(matter.user_id);
          }
          userIds.push(user.id);
        }

        let permissionIds = await common.otherDiaryUserIds(user.id);
        userIds = userIds.concat(permissionIds);

        if ((filter == "all" || filter == "leave") && userIds.length) {
          leaves = await Leave.findAll({
            where: {
              type: [1, 3],
              [Op.or]: [
                { from_date: { $between: [startDate, endDate] } },
                { to_date: { $between: [startDate, endDate] } }
              ],
              user_id: userIds
            },
            include: [
              {
                association: "user",
                include: [{ association: "userdetail" }]
              },
              {
                association: "author",
                include: [{ association: "userdetail" }]
              }
            ]
          });
        }
        if ((filter == "all" || filter == "wfm") && userIds.length) {
          wfms = await Leave.findAll({
            where: {
              type: 2,
              [Op.or]: [
                { from_date: { $between: [startDate, endDate] } },
                { to_date: { $between: [startDate, endDate] } }
              ],
              user_id: userIds
            },
            include: [
              {
                association: "user",
                include: [{ association: "userdetail" }]
              },
              {
                association: "author",
                include: [{ association: "userdetail" }]
              }
            ]
          });
        }
      } else {
        if (user.parent_user_id != null) {
          userIds.push(user.parent_user_id);
          let subadmins = await User.findAll({
            where: { parent_user_id: user.parent_user_id },
            role_id: 3
          });
          for (let item of subadmins) userIds.push(item.id);
          let attorneys = await User.findAll({
            where: {
              parent_user_id: [user.id, user.parent_user_id],
              role_id: 3
            }
          });
          for (let item of attorneys) userIds.push(item.id);
        } else {
          userIds.push(user.id);
          let subadmins = await User.findAll({
            where: { parent_user_id: user.id },
            role_id: 3
          });
          for (let item of subadmins) userIds.push(item.id);
          let attorneys = await User.findAll({
            where: {
              parent_user_id: user.id,
              role_id: 3
            }
          });
          for (let item of attorneys) userIds.push(item.id);
        }
        if (filter == "all" || filter == "leave") {
          leaves = await Leave.findAll({
            where: {
              type: [1, 3],
              user_id: userIds,
              [Op.or]: [
                { from_date: { $between: [startDate, endDate] } },
                { to_date: { $between: [startDate, endDate] } }
              ]
            },
            include: [
              {
                association: "user",
                include: [{ association: "userdetail" }]
              },
              {
                association: "author",
                include: [{ association: "userdetail" }]
              }
            ]
          });
        }
        if (filter == "all" || filter == "wfm") {
          wfms = await Leave.findAll({
            where: {
              type: 2,
              user_id: userIds,
              [Op.or]: [
                { from_date: { $between: [startDate, endDate] } },
                { to_date: { $between: [startDate, endDate] } }
              ]
            },
            include: [
              {
                association: "user",
                include: [{ association: "userdetail" }]
              },
              {
                association: "author",
                include: [{ association: "userdetail" }]
              }
            ]
          });
        }
      }
    }

    let userIds = [];
    if (user.parent_user_id != null) {
      userIds.push(user.parent_user_id);
      let subadmins = await User.findAll({
        where: { parent_user_id: user.parent_user_id },
        role_id: 3
      });
      for (let item of subadmins) userIds.push(item.id);
      let attorneys = await User.findAll({
        where: {
          parent_user_id: [user.id, user.parent_user_id],
          role_id: 3
        }
      });
      for (let item of attorneys) userIds.push(item.id);
    } else {
      userIds.push(user.id);
      let subadmins = await User.findAll({
        where: { parent_user_id: user.id },
        role_id: 3
      });
      for (let item of subadmins) userIds.push(item.id);
      let attorneys = await User.findAll({
        where: {
          parent_user_id: user.id,
          role_id: 3
        }
      });
      for (let item of attorneys) userIds.push(item.id);
    }

    holidays = await Holiday.findAll({
      where: {
        user_id: userIds,
        isactive: 1,
        [Op.or]: [
          { start_date: { $between: [startDate, endDate] } },
          { end_date: { $between: [startDate, endDate] } }
        ]
      }
    });

    let tasks2 = [];
    if (tasks.length) {
      let i = 0;
      for (let item of tasks) {
        let assignees = await TaskAssignee.findAll({
          where: { task_id: item.id },
          include: [
            {
              association: "assignee",
              include: [{ association: "userdetail" }]
            }
          ]
        });
        let pending_assignees = await TaskPendingAssignee.findAll({
          where: { task_id: item.id },
          include: [
            {
              association: "assignee",
              include: [{ association: "userdetail" }]
            }
          ]
        });
        assignees = assignees.concat(pending_assignees);
        tasks2.push({
          id: item.id,
          subject: item.subject,
          description: item.description,
          location: item.location,
          board_room: item.board_room,
          start_date: item.start_date,
          end_date: item.end_date,
          start_time: item.start_time,
          end_time: item.end_time,
          created_at: item.created_at,
          clientdetail: item.clientdetail,
          matter: item.matter,
          contact: item.contact,
          thirdparty: item.thirdparty,
          assignees: assignees,
          user: item.user
        });
      }
      tasks = tasks2;
    }

    let meetings2 = [];
    if (meetings.length) {
      let i = 0;
      for (let item of meetings) {
        let assignees = await MeetingAssignee.findAll({
          where: { meeting_id: item.id },
          include: [
            {
              association: "assignee",
              include: [{ association: "userdetail" }]
            }
          ]
        });
        meetings2.push({
          id: item.id,
          subject: item.subject,
          description: item.description,
          location: item.location,
          board_room: item.board_room,
          start_date: item.start_date,
          end_date: item.end_date,
          start_time: item.start_time,
          end_time: item.end_time,
          created_at: item.created_at,
          matter: item.matter,
          contact: item.contact,
          thirdparty: item.thirdparty,
          assignees: assignees,
          user: item.user
        });
      }
      meetings = meetings2;
    }

    res.status(200).json({
      birthdays,
      hearings,
      tasks,
      meetings,
      leads,
      leaves,
      wfms,
      holidays
    });
  }
);

module.exports = router;
