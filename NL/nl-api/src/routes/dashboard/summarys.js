const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const checkToken = require("../../middlewares/checkToken");
const Message = require("../../models/Message");
const Client = require("../../models/Client");
const Hearing = require("../../models/Hearing");
const Task = require("../../models/dashboard/Task");
const Meeting = require("../../models/dashboard/Meeting");
const Op = Sequelize.Op;
const moment = require("moment");
const common = require("../../helpers/common");
const User = require("../../models/User");

const weekStartDates = () => {
  let weekMap = [6, 0, 1, 2, 3, 4, 5];
  var now = new Date();
  now.setHours(0, 0, 0, 0);
  var monday = new Date(now);
  monday.setDate(monday.getDate() - weekMap[monday.getDay()] - 1);
  var sunday = new Date(now);
  sunday.setDate(sunday.getDate() - weekMap[sunday.getDay()] + 5);
  sunday.setHours(23, 59, 59, 999);

  monday =
    monday.getFullYear() +
    "-" +
    (monday.getMonth() + 1 < 10
      ? "0" + (monday.getMonth() + 1)
      : monday.getMonth() + 1) +
    "-" +
    (monday.getDate() < 10 ? "0" + monday.getDate() : monday.getDate());
  sunday =
    sunday.getFullYear() +
    "-" +
    (sunday.getMonth() + 1 < 10
      ? "0" + (sunday.getMonth() + 1)
      : sunday.getMonth() + 1) +
    "-" +
    (sunday.getDate() < 10 ? "0" + sunday.getDate() : sunday.getDate());
  return [monday, sunday];
};

router.get("/counts/user/:userID", checkToken, async (req, res, next) => {
  //let range_dates = weekStartDates();
  let weekDate = moment(new Date());
  if (req.query.date != "" && req.query.date != undefined)
    weekDate = moment(new Date(req.query.date));
  let range_dates = [
    weekDate.startOf("week").format("YYYY-MM-DD H:mm:ss"),
    weekDate
      .add(1, "day")
      .endOf("week")
      .format("YYYY-MM-DD H:mm:ss")
  ];

  let user = await User.findByPk(req.params.userID);

  let userIds = [req.params.userID];
  if (user.role_id == 2 && user.parent_user_id == null) {
    userIds = await common.firmUsers(req.params.userID);
  } else if (user.role_id == 2 && user.parent_user_id != null) {
    //Other diary admins
    userIds = await common.otherDiaryUserIds(req.params.userID);

    //All attorneys
    let ids = await common.firmUsers(req.params.userID);
    let firm_users = await User.findAll({ where: { id: ids, role_id: 3 } });
    for (let item of firm_users) userIds.push(item.id);
  } else if (user.role_id == 3 && user.parent_user_id != null) {
    //Other diary attorneys
    userIds = await common.otherDiaryUserIds(req.params.userID);
  } else {
    userIds = await common.firmUsers(req.params.userID);
  }

  let messages = await Message.findAll({
    where: {
      attorney_id: req.params.userID,
      created_at: { $between: range_dates }
    }
  });

  let client_ids = await Client.findAll({
    where: {
      user_id: userIds
    },
    attributes: ["id"]
  });
  client_ids = client_ids.map(row => {
    return row.id;
  });
  client_ids = client_ids.filter(function(item, pos) {
    return client_ids.indexOf(item) == pos;
  });

  let assigneeID = req.query.assigneeID || userIds;

  let hearings = await Hearing.findAll({
    where: {
      [Op.or]: [
        { from_date: { $between: range_dates } },
        { to_date: { $between: range_dates } }
      ]
    },
    include: [{ association: "attorneys", where: { attorney_id: assigneeID } }]
  });

  let tasks = await Task.findAll({
    where: {
      //user_id: common.firmAdmins(),
      [Op.or]: [
        { start_date: { $between: range_dates } },
        { end_date: { $between: range_dates } }
      ]
    },
    include: [
      {
        association: "assignees",
        where: { user_id: assigneeID }
      }
    ]
  });

  let meetings = await Meeting.findAll({
    where: {
      //user_id: req.params.userID,
      [Op.or]: [
        { start_date: { $between: range_dates } },
        { end_date: { $between: range_dates } }
      ]
    },
    include: [
      {
        association: "assignees",
        where: { user_id: assigneeID }
      }
    ]
  });

  let today = new Date();
  let tomorrow = new Date();
  let dayAfterTomorrow = new Date();

  // tomorrow.setDate(today.getDate() + 1);
  // dayAfterTomorrow.setDate(today.getDate() + 2);

  // today =
  //   today.getFullYear() +
  //   "-" +
  //   (today.getMonth() + 1 < 10
  //     ? "0" + (today.getMonth() + 1)
  //     : today.getMonth() + 1) +
  //   "-" +
  //   (today.getDate() < 10 ? "0" + today.getDate() : today.getDate());

  // tomorrow =
  //   tomorrow.getFullYear() +
  //   "-" +
  //   (tomorrow.getMonth() + 1 < 10
  //     ? "0" + (tomorrow.getMonth() + 1)
  //     : tomorrow.getMonth() + 1) +
  //   "-" +
  //   (tomorrow.getDate() < 10 ? "0" + tomorrow.getDate() : tomorrow.getDate());

  // dayAfterTomorrow =
  //   dayAfterTomorrow.getFullYear() +
  //   "-" +
  //   (dayAfterTomorrow.getMonth() + 1 < 10
  //     ? "0" + (dayAfterTomorrow.getMonth() + 1)
  //     : dayAfterTomorrow.getMonth() + 1) +
  //   "-" +
  //   (dayAfterTomorrow.getDate() < 10
  //     ? "0" + dayAfterTomorrow.getDate()
  //     : dayAfterTomorrow.getDate());

  today = moment(new Date())
    .startOf("day")
    .format("YYYY-MM-DD H:mm:ss");
  tomorrow = moment(new Date())
    .add(1, "day")
    .endOf("day")
    .format("YYYY-MM-DD H:mm:ss");
  dayAfterTomorrow = moment(new Date())
    .add(2, "days")
    .format("YYYY-MM-DD H:mm:ss");

  let userId = req.query.assigneeID || req.params.userID;

  let schedules_hearings = await Hearing.findAll({
    attributes: ["id"],
    where: {
      //user_id: req.params.userID,
      [Op.or]: [
        { from_date: { $between: [today, tomorrow] } },
        { to_date: { $between: [today, tomorrow] } }
      ]
    },
    include: [
      {
        association: "attorneys",
        where: { attorney_id: [userId] }
      }
    ]
  });
  let schedules_tasks = await Task.findAll({
    attributes: ["id"],
    where: {
      //user_id: userId,
      [Op.or]: [
        { start_date: { $between: [today, tomorrow] } },
        { end_date: { $between: [today, tomorrow] } }
      ]
    },
    include: [
      {
        association: "assignees",
        where: { user_id: [userId] }
      }
    ]
  });
  let schedules_meetings = await Meeting.findAll({
    attributes: ["id"],
    where: {
      //user_id: userId,
      [Op.or]: [
        { start_date: { $between: [today, tomorrow] } },
        { end_date: { $between: [today, tomorrow] } }
      ]
    },
    include: [
      {
        association: "assignees",
        where: { user_id: [userId] }
      }
    ]
  });

  let schedules_count =
    schedules_hearings.length +
    schedules_tasks.length +
    schedules_meetings.length;

  return res.status(200).json({
    dates: range_dates,
    date2: [today, tomorrow, dayAfterTomorrow],
    counts: {
      messages: messages.length,
      hearings: hearings.length,
      tasks: tasks.length,
      meetings: meetings.length,
      schedules: schedules_count
    },
    data: {
      hearings
    }
  });
});

module.exports = router;
