const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const Task = require("../../models/dashboard/Task");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Helper = require("../../helpers/dateTime");
const User = require("../../models/User");
const Op = Sequelize.Op;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const ClientContact = require("../../models/ClientContact");
const ClientDetails = require("../../models/Client");
const middleware = {
  validations: [
    check("subject").isString(),
    check("client_id").isUUID(),
    check("case_id").isUUID(),
    check("contact_id").isUUID(),
    check("thirdparty_id").isUUID(),
    check("start_date").isString(),
    check("end_date").isString(),
    check("start_time").isString(),
    check("end_time").isString(),
    check("description").isString()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      if (req.body.start_time != "") {
        req.body.start_time = new Date(
          req.body.start_date +
            " " +
            Helper.convertTime(req.body.start_time) +
            ":00Z"
        );
      }

      console.log(req.body.start_time);

      if (req.body.end_time != "") {
        req.body.end_time = new Date(
          req.body.end_date +
            " " +
            Helper.convertTime(req.body.end_time) +
            ":00Z"
        );
      }

      let task = await Task.create(req.body, {
        include: [{ association: "assignees" }]
      });

      let client = await ClientDetails.findOne({
        where: { id: task.client_id }
      });
      var name = [];
      var email = [];

      for (attorney of task.assignees) {
        let attorneys = await User.findOne({
          where: { id: attorney.user_id }
        });
        email.push(attorneys.email);
        name.push(attorneys.username);
      }
      var assigned = name.join(", ");

      var start_date = moment(task.start_date).format("DD-MM-YYYY");
      var end_date = moment(task.end_date).format("DD-MM-YYYY");
      var duration = start_date + "-" + end_date;
      let content = fs.readFileSync(
        "./src/email-templates/crm/Task_created.html",
        "utf8"
      );
      content = content.replace("**title**", task.subject);
      content = content.replace("**description**", task.description);
      content = content.replace("**description**", task.description);
      content = content.replace("**duration**", duration);
      content = content.replace("**assignedby**", assigned);
      content = content.replace("**name**", client.firstname);
      let sentmail = await mail.send(
        client.email,
        "New Task Details",
        content,
        next
      );
      res.status(201).json(task);
    } catch (err) {
      next(err);
    }
  }
);

router.get(
  "/user/:userID/list/group",
  middleware.auth,
  async (req, res, next) => {
    try {
      let userID = req.params.userID;
      let user = await User.findByPk(userID);
      let mytask = {};
      if (user.parent_user_id != null) {
        user = await User.findByPk(user.parent_user_id);
        userID = user.parent_user_id;
      }

      let userids = [];
      userids.push(userID);

      let all = await Task.count({
        where: {
          user_id: userids
        }
      });

      let tasks = await Task.count({
        include: [{ association: "assignees", where: { user_id: userID } }]
      });
      mytask = {
        id: parseInt(userID),
        name: "My Tasks",
        taskCount: all
      };

      let result_attorneys = [];
      let attorneys = await User.findAll({
        where: { parent_user_id: userID, role_id: 3 },
        include: [{ association: "userdetail" }]
      });

      for (let attorney of attorneys) {
        tasks = await Task.count({
          include: [
            { association: "assignees", where: { user_id: attorney.id } }
          ]
        });
        userids.push(attorney.id);
        result_attorneys.push({
          id: attorney.id,
          name:
            attorney.userdetail.firstname + " " + attorney.userdetail.lastname,
          taskCount: tasks
        });
      }

      let result_admins = [];
      let admins = await User.findAll({
        where: { parent_user_id: userID, role_id: 2 },
        include: [{ association: "userdetail" }]
      });

      for (let admin of admins) {
        tasks = await Task.count({
          include: [{ association: "assignees", where: { user_id: admin.id } }]
        });
        userids.push(admin.id);
        result_admins.push({
          id: admin.id,
          name: admin.userdetail.firstname + " " + admin.userdetail.lastname,
          taskCount: tasks
        });
      }

      let completed = await Task.count({
        where: {
          user_id: userids,
          status: "completed"
          // end_date: {
          //     [Op.lt]: moment(new Date()).format("YYYY-MM-DD")
          // }
        }
      });

      return res.status(200).json({
        mytask: mytask,
        attorneys: result_attorneys,
        admins: result_admins,
        completed: completed,
        all: all
      });
    } catch (err) {
      next(err);
    }
  }
);

router.get("/paginate/user/:userID", middleware.auth, (req, res, next) => {
  Task.count({
    include: [
      {
        association: "assignees",
        where: {
          user_id: req.params.userID
        }
      }
    ]
  })
    .then(count => {
      let limit = req.query.limit ? parseInt(req.query.limit) : 10;
      let offset = 0;
      let page = req.query.page ? req.query.page : 1;
      let pages = Math.ceil(count / limit);
      offset = limit * (page - 1);
      Task.findAndCountAll({
        include: [
          {
            association: "assignees",
            where: {
              user_id: req.params.userID
            }
          },
          { association: "matter" }
        ],
        limit: limit,
        offset: offset
      })
        .then(data => {
          data["count"] = count;
          res.status(200).json({
            success: 1,
            page: pages,
            data: data,
            pageno: parseInt(page),
            testpage: req.query.page,
            message: "All Task Content"
          });
        })
        .catch(err => {
          console.log(err);
        });
    })
    .catch(err => next(err));
});

router.get(
  "/paginate/user/:userID/completedTask",
  middleware.auth,
  (req, res, next) => {
    Task.count({
      where: {
        end_date: {
          [Op.lt]: moment(new Date()).format("YYYY-MM-DD")
        }
      }
    })
      .then(count => {
        let limit = req.query.limit ? parseInt(req.query.limit) : 10;
        let offset = 0;
        let page = req.query.page ? req.query.page : 1;
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        Task.findAndCountAll({
          where: {
            end_date: {
              [Op.lt]: moment(new Date()).format("YYYY-MM-DD")
            }
          },
          include: [
            {
              association: "assignees",
              where: {
                user_id: req.params.userID
              }
            },
            { association: "matter" }
          ],
          limit: limit,
          offset: offset
        })
          .then(data => {
            data["count"] = count;
            res.status(200).json({
              success: 1,
              page: pages,
              data: data,
              pageno: parseInt(page),
              testpage: req.query.page,
              message: "All Completed Task Content"
            });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .catch(err => next(err));

    // let limit = req.query.limit || 10;
    // let page = req.query.page || 1;
    // Task.paginate({
    //         paginate: limit,
    //         page: page,
    //        where: {
    //             end_date: {
    //                 [Op.lt]: moment(new Date()).format("YYYY-MM-DD")
    //             }
    //         },
    //         include: [{
    //                 association: "assignees",
    //                 where: {
    //                     user_id: req.params.userID,

    //                 }
    //             },
    //             { association: "matter" },

    //         ],

    //     })
    //     .then(tasks => {
    //         res.status(200).json(tasks);
    //     })
    //     .catch(err => next(err));
  }
);

router.post("/updatestatus", async (req, res, next) => {
  if (req.body.status) {
    let result = await Task.findOne({
      where: { id: req.body.id }
    });
    if (result) {
      result.update({
        status: req.body.status
      });
    }
    // res.status(200).json(result);
    res.status(200).json(result);
  } else {
    res.status(401).json("cannot update staṭus");
  }
});

module.exports = router;
