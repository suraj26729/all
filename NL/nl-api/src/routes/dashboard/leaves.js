const express = require("express");
const router = express.Router();
const Leave = require("../../models/dashboard/Leave");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Helper = require("../../helpers/dateTime");

const middleware = {
  validations: [
    check("from_date").isString(),
    check("from_time").isString(),
    check("to_date").isString(),
    check("to_time").isString(),
    check("notes").isString(),
    check("user_id").isInt()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.auth, middleware.validations],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      let from_time = req.body.from_time;
      let to_time = req.body.to_time;

      if (req.body.from_time != "") {
        req.body.from_time = Helper.convertTime(from_time);
      }
      if (req.body.to_time != "") {
        req.body.to_time = Helper.convertTime(to_time);
      }

      let leave = await Leave.create(req.body);
      return res.status(201).json(leave);
    } catch (err) {
      next(err);
    }
  }
);

router.delete("/id/:id", middleware.auth, async (req, res, next) => {
  try {
    let leave = await Leave.findByPk(req.params.id);
    if (leave != null) {
      leave.destroy();
      return res.status(204).json({});
    } else {
      return res.status(404).json("Leave is not found!");
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
