const express = require("express");
const router = express.Router();
const Meeting = require("../../models/dashboard/Meeting");
const Matter = require("../../models/CaseDetails");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Helper = require("../../helpers/dateTime");
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const ClientContact = require("../../models/ClientContact");
const ClientDetails = require("../../models/Client");
const CaseDetails = require("../../models/CaseDetails");
const MeetingAssignee = require("../../models/dashboard/MeetingAssignee");
const User = require("../../models/User");
const common = require("../../helpers/common");
const Email = require("../../models/Email");
const Notification = require("../../models/Notification");

const middleware = {
  validations: [
    check("subject").isString(),
    // check("case_id").isUUID(),
    // check("contact_id").isUUID(),
    // check("thirdparty_id").isUUID(),
    check("start_date").isString(),
    check("end_date").isString(),
    check("start_time").isString(),
    check("end_time").isString(),
    check("description").isString()
  ],
  auth: checkToken
};

router.get("/matters/user/:id", middleware.auth, async (req, res, next) => {
  let userIds = await common.firmUsers(req.params.id);
  let clients = await ClientDetails.findAll({
    attributes: ["id"],
    where: { user_id: userIds, isactive: 1 }
  });
  if (clients.length) {
    let clientIds = clients.map(item => item.id);
    let matters = await Matter.findAll({
      where: { client_id: clientIds, status: { $not: [4] } }
    });
    return res.status(200).json(matters);
  } else {
    return res.status(200).json([]);
  }
});

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      // res.status(200).json(req.body);
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      let start_date = req.body.start_date;
      let end_date = req.body.end_date;
      let start_time = req.body.start_time;
      let end_time = req.body.end_time;

      if (req.body.start_time != "") {
        req.body.start_time = new Date(
          start_date + " " + Helper.convertTime(start_time) + ":00Z"
        );
      }
      if (req.body.end_time != "") {
        req.body.end_time = new Date(
          end_date + " " + Helper.convertTime(end_time) + ":00Z"
        );
      }

      req.body.start_date =
        moment(new Date(start_date)).format("YYYY-MM-DD") +
        " " +
        Helper.convertTime(start_time) +
        "Z";
      req.body.end_date =
        moment(new Date(end_date)).format("YYYY-MM-DD") +
        " " +
        Helper.convertTime(end_time) +
        "Z";

      if (req.body.contact_id == "") {
        delete req.body.contact_id;
      }
      if (req.body.thirdparty_id == "") {
        delete req.body.thirdparty_id;
      }
      if (req.body.case_id == "") {
        delete req.body.case_id;
      }

      // return res.status(200).json(req.body);

      let userIds = await common.firmUsers(req.body.user_id);
      let check_meeting = await Meeting.findAll({
        where: {
          location: req.body.location,
          board_room: req.body.board_room,
          user_id: userIds
        },
        order: ["start_time"]
      });

      if (check_meeting) {
        for (meetings of check_meeting) {
          if (
            meetings.start_date <= req.body.start_time &&
            meetings.end_date >= req.body.start_time
          ) {
            return res.status(400).json({
              seccess: 0,
              data: "",
              message: "Same location and board room is already Booked"
            });
          }
          if (
            meetings.start_date >= req.body.start_time &&
            meetings.end_date <= req.body.end_time
          ) {
            return res.status(400).json({
              seccess: 0,
              data: "",
              message: "Same location and board room is already Booked"
            });
          }
        }
      }

      let meeting = await Meeting.create(req.body);

      let assign = [];

      for (ra of req.body.assignees) {
        assign.push({
          meeting_id: meeting.id,
          user_id: ra.user_id
        });
      }

      let meeting_assignees = await MeetingAssignee.bulkCreate(assign);

      let createdBy = await User.findOne({
        where: { id: meeting.user_id },
        include: [{ association: "userdetail" }]
      });

      var cse = "";
      if (meeting.case_id) {
        cse = await CaseDetails.findOne({
          where: { id: meeting.case_id }
        });
      }

      var name = [];
      var email = [];
      var ids = [];

      let assigned = "None";
      if (req.body.assignees.length) {
        for (attorney of meeting_assignees) {
          let attorneys = await User.findOne({
            include: [{ association: "userdetail" }],
            where: { id: attorney.user_id }
          });
          email.push(attorneys.email);
          name.push(
            attorneys.userdetail.firstname + " " + attorneys.userdetail.lastname
          );
          ids.push(attorney.user_id);
        }
        assigned = name.join(", ");
      }

      var start_date1 = moment(meeting.start_date).format("DD-MM-YYYY");
      var end_date1 = moment(meeting.end_date).format("DD-MM-YYYY");
      var duration = start_date1 + "-" + end_date1;

      let i = 0;
      for (let item of email) {
        let content = fs.readFileSync(
          "./src/email-templates/crm/meeting.html",
          "utf8"
        );
        content = content.replace("**title**", meeting.subject);
        content = content.replace("**description**", meeting.description);
        content = content.replace("**duration**", duration);
        //content = content.replace("**name**", client.username);
        if (cse) {
          content = content.replace("**case**", cse.prefix + cse.casenumber);
        } else {
          content = content.replace("**case**", "-");
        }

        let created_by = "";
        content = content.replace("**name**", name[i]);

        let email = await Email.create({
          parent_id: meeting.id,
          matter_id: meeting.case_id,
          subject: "New Meeting Details",
          type: "new_meeting",
          user_id: ids[i]
        });

        if (createdBy != null)
          created_by =
            createdBy.userdetail.firstname +
            " " +
            createdBy.userdetail.lastname;
        else created_by = "Unknown";
        let notification = await Notification.create({
          title: "New Meeting Created",
          content: meeting.subject + "|By:" + created_by,
          ref_id: meeting.id,
          ref_module: "meeting",
          user_id: ids[i]
        });

        let sentmail = await mail.send(
          item,
          "New Meeting Details",
          content,
          next
        );
        i++;
      }

      res.status(201).json({ meeting, meeting_assignees });
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
);

router.get("/id/:id", middleware.auth, async (req, res, next) => {
  let meeting = await Meeting.findByPk(req.params.id);

  let assignees = await MeetingAssignee.findAll({
    where: { meeting_id: meeting.id }
  });

  let admins = [];
  let attorneys = [];
  if (assignees.length) {
    for (let item of assignees) {
      let user = await User.findByPk(item.user_id);
      if (user != null) {
        if (user.role_id == 2) admins.push(user.id);
        if (user.role_id == 3) attorneys.push(user.id);
      }
    }
  }

  return res.status(200).json({ meeting, assignees, admins, attorneys });
});

router.put(
  "/id/:id",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    let start_date = req.body.start_date;
    let end_date = req.body.end_date;
    let start_time = req.body.start_time;
    let end_time = req.body.end_time;

    if (req.body.start_time != "") {
      req.body.start_time = new Date(
        start_date + " " + Helper.convertTime(start_time) + ":00Z"
      );
    }
    if (req.body.end_time != "") {
      req.body.end_time = new Date(
        end_date + " " + Helper.convertTime(end_time) + ":00Z"
      );
    }

    req.body.start_date =
      moment(new Date(start_date)).format("YYYY-MM-DD") +
      " " +
      Helper.convertTime(start_time) +
      "Z";
    req.body.end_date =
      moment(new Date(end_date)).format("YYYY-MM-DD") +
      " " +
      Helper.convertTime(end_time) +
      "Z";

    if (req.body.case_id == "") delete req.body.case_id;
    if (req.body.contact_id == "") delete req.body.contact_id;
    if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;

    let meeting = await Meeting.findByPk(req.params.id);
    if (meeting != null) {
      meeting.update(req.body);
      if (req.body.assigness.length) {
        let assignees = await MeetingAssignee.destroy({
          where: { meeting_id: meeting.id }
        });
        let args = req.body.assigness.map(item => {
          return { meeting_id: meeting.id, user_id: item.user_id };
        });
        let new_assignees = await MeetingAssignee.bulkCreate(args);
      }
      res.status(200).json(meeting);
    } else return res.status(404).json("Meeting is not found!");
  }
);

router.delete("/id/:id", middleware.auth, async (req, res, next) => {
  let meeting = await Meeting.findByPk(req.params.id);
  if (meeting != null) {
    meeting.destroy();
    return res.status(204).json({});
  } else return res.status(404).json("Meeting is not found!");
});

module.exports = router;
