const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const Task = require("../../models/dashboard/Task");
const TaskAssignee = require("../../models/dashboard/TaskAssignee");
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const Helper = require("../../helpers/dateTime");
const User = require("../../models/User");
const Op = Sequelize.Op;
const mail = require("../../config/mail");
const fs = require("fs");
const moment = require("moment");
const ClientContact = require("../../models/ClientContact");
const ClientDetails = require("../../models/Client");
const Hearing = require("../../models/Hearing");
const Client = require("../../models/Client");
const Meeting = require("../../models/dashboard/Meeting");
const common = require("../../helpers/common");
const TaskPendingAssignee = require("../../models/dashboard/TaskPendingAssignee");
const Email = require("../../models/Email");
const Notification = require("../../models/Notification");

const middleware = {
  validations: [
    //check("subject").isString(),
    //check("client_id").isUUID(),
    //check("case_id").isUUID(),
    // check("contact_id").isUUID(),
    // check("thirdparty_id").isUUID(),
    check("start_date").isString(),
    check("end_date").isString(),
    check("start_time").isString(),
    check("end_time").isString()
    // check("description").isString()
  ],
  auth: checkToken
};

router.post(
  "/",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }

      let start_date = req.body.start_date;
      let end_date = req.body.end_date;
      let start_time = req.body.start_time;
      let end_time = req.body.end_time;

      if (req.body.start_time != "") {
        req.body.start_time = new Date(
          start_date + " " + Helper.convertTime(start_time) + ":00Z"
        );
      }
      if (req.body.end_time != "") {
        req.body.end_time = new Date(
          end_date + " " + Helper.convertTime(end_time) + ":00Z"
        );
      }

      req.body.start_date =
        moment(new Date(start_date)).format("YYYY-MM-DD") +
        " " +
        Helper.convertTime(start_time) +
        "Z";
      req.body.end_date =
        moment(new Date(end_date)).format("YYYY-MM-DD") +
        " " +
        Helper.convertTime(end_time) +
        "Z";

      req.body.status = req.body.status || "open";

      if (req.body.client_id == "") delete req.body.client_id;
      if (req.body.case_id == "") delete req.body.case_id;
      if (req.body.contact_id == "") delete req.body.contact_id;
      if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;

      //BEFORE PENDING TASK ASSIGNEE
      //let task = "";
      // if (req.body.assignees.length) {
      //     task = await Task.create(req.body, {
      //         include: [{ association: "assignees" }]
      //     });
      // } else {
      //     task = await Task.create(req.body);
      // }
      let userIds = await common.firmUsers(req.body.user_id);
      let check_tasks = await Task.findAll({
        where: {
          location: req.body.location,
          board_room: req.body.board_room,
          user_id: userIds
        },
        order: ["start_time"]
      });

      if (check_tasks) {
        for (tasks of check_tasks) {
          if (
            tasks.start_date <= req.body.start_time &&
            tasks.end_date >= req.body.start_time
          ) {
            return res.status(400).json({
              seccess: 0,
              data: "",
              message: "Same location and board room is already Booked"
            });
          }
          if (
            tasks.start_date >= req.body.start_time &&
            tasks.end_date <= req.body.end_time
          ) {
            return res.status(400).json({
              seccess: 0,
              data: "",
              message: "Same location and board room is already Booked"
            });
          }
        }
      }

      let task = "";
      let pendingAssignee = "";
      if (req.body.assignees.length) {
        task = await Task.create(req.body);
        for (assignee of req.body.assignees) {
          if (assignee.user_id != req.body.user_id) {
            pendingAssignee = await TaskPendingAssignee.create({
              task_id: task.id,
              user_id: assignee.user_id
            });
          } else {
            await TaskAssignee.create({
              task_id: task.id,
              user_id: assignee.user_id,
              status: 2
            });
          }
        }
      } else {
        task = await Task.create(req.body);
      }

      if (req.body.client_id) {
        let client = await ClientDetails.findOne({
          where: { id: task.client_id }
        });
        var name = [];
        var email = [];
        var ids = [];
        let assigned = "None";

        if (req.body.assignees.length) {
          for (attorney of req.body.assignees) {
            let attorneys = await User.findOne({
              include: [{ association: "userdetail" }],
              where: { id: attorney.user_id }
            });
            email.push(attorneys.email);
            name.push(
              attorneys.userdetail.firstname +
                " " +
                attorneys.userdetail.lastname
            );
            ids.push(attorney.user_id);
          }
          assigned = name.join(", ");
        }

        start_date = moment(task.start_date).format("DD-MM-YYYY");
        end_date = moment(task.end_date).format("DD-MM-YYYY");
        var duration = start_date + "-" + end_date;

        // content = content.replace("**name**", client.firstname);

        let i = 0;
        for (let item of email) {
          let content = fs.readFileSync(
            "./src/email-templates/crm/Task_created.html",
            "utf8"
          );
          content = content.replace("**title**", task.subject);
          content = content.replace("**description**", task.description);
          content = content.replace("**description**", task.description);
          content = content.replace("**location**", task.location);
          content = content.replace("**board_room**", task.board_room);
          content = content.replace("**duration**", duration);
          content = content.replace("**assignedby**", assigned);
          content = content.replace("**name**", name[i]);
          let email = await Email.create({
            parent_id: task.id,
            client_id: task.client_id,
            matter_id: task.case_id,
            subject: "New Task Details",
            type: "new_task",
            user_id: ids[i]
          });

          let createdBy = await User.findByPk(task.user_id, {
            include: [{ association: "userdetail" }]
          });
          createdBy =
            createdBy != null
              ? createdBy.userdetail.firstname +
                " " +
                createdBy.userdetail.lastname
              : "Unknown";

          let notification_title =
            "New Task Created. Waiting for your approval";
          if (req.body.user_id == ids[i]) {
            notification_title = "New Task is created";
          }
          let notification = await Notification.create({
            title: notification_title,
            content: task.subject + "|By:" + createdBy,
            ref_id: task.id,
            ref_module: "task",
            user_id: ids[i]
          });

          let sentmail = await mail.send(
            item,
            "New Task Details",
            content,
            next
          );
          i++;
        }
      }
      res.status(201).json(task);
    } catch (err) {
      console.log(err);
      next(err);
    }
  }
);

router.get(
  "/user/:userID/list/group",
  middleware.auth,
  async (req, res, next) => {
    try {
      let user = await User.findByPk(req.params.userID);
      let mytask = {};
      let userids = [];

      let all = await Task.findAll({
        where: {
          status: {
            [Op.notIn]: ["approved", "completed", "canceled"]
          }
        },
        include: [
          {
            association: "assignees",
            where: { status: { $not: 1 }, user_id: req.params.userID }
          }
        ]
      });

      mytask = {
        id: parseInt(req.params.userID),
        name: "My Tasks",
        taskCount: all.length
      };

      if (user.parent_user_id != null) {
        userids = await common.otherDiaryUserIds(req.params.userID);
      } else {
        userids = [];
        userids.push(req.params.userID);
        userids = await common.firmUsers(req.params.userID);
      }

      let result_attorneys = [];
      let attorneys = await User.findAll({
        where: {
          id: userids,
          isactive: 1,
          $or: [{ role_id: 3 }, { isattorney: 1 }]
        },
        include: [{ association: "userdetail" }]
      });

      for (let attorney of attorneys) {
        tasks = await Task.findAll({
          where: {
            status: {
              [Op.notIn]: ["approved", "completed", "canceled"]
            }
          },
          include: [
            {
              association: "assignees",
              where: { status: { $not: 1 }, user_id: attorney.id }
            }
          ]
        });
        userids.push(attorney.id);
        result_attorneys.push({
          id: attorney.id,
          name:
            attorney.userdetail.firstname + " " + attorney.userdetail.lastname,
          taskCount: tasks.length
        });
      }

      let result_admins = [];
      let admins = await User.findAll({
        where: {
          id: userids,
          isactive: 1,
          $or: [{ role_id: 2 }, { isadmin: 1 }]
        },
        include: [{ association: "userdetail" }]
      });

      for (let admin of admins) {
        tasks = await Task.findAll({
          where: {
            status: {
              [Op.notIn]: ["approved", "completed", "canceled"]
            }
          },
          include: [
            {
              association: "assignees",
              where: { status: { $not: 1 }, user_id: admin.id }
            }
          ]
        });
        userids.push(admin.id);
        result_admins.push({
          id: admin.id,
          name: admin.userdetail.firstname + " " + admin.userdetail.lastname,
          taskCount: tasks.length
        });
      }

      let completed = await Task.count({
        where: {
          user_id: userids,
          status: "completed"
          // end_date: {
          //     [Op.lt]: moment(new Date()).format("YYYY-MM-DD")
          // }
        }
      });

      return res.status(200).json({
        mytask: mytask,
        attorneys: result_attorneys,
        admins: result_admins,
        completed: completed,
        all: all.length
      });
    } catch (err) {
      next(err);
    }
  }
);

// router.get("/paginate/user/:userID", middleware.auth, (req, res, next) => {
//   Task.count({
//     include: [
//       {
//         association: "assignees",
//         where: {
//           user_id: req.params.userID
//         }
//       }
//     ]
//   })
//     .then(count => {
//       let limit = req.query.limit ? parseInt(req.query.limit) : 10;
//       let offset = 0;
//       let page = req.query.page ? req.query.page : 1;
//       let pages = Math.ceil(count / limit);
//       offset = limit * (page - 1);
//       Task.findAndCountAll({
//         include: [
//           {
//             association: "assignees",
//             where: {
//               user_id: req.params.userID
//             }
//           },
//           { association: "matter" }
//         ],
//         limit: limit,
//         offset: offset
//       })
//         .then(data => {
//           data["count"] = count;
//           res.status(200).json({
//             success: 1,
//             page: pages,
//             data: data,
//             pageno: parseInt(page),
//             testpage: req.query.page,
//             message: "All Task Content"
//           });
//         })
//         .catch(err => {
//           console.log(err);
//         });
//     })
//     .catch(err => next(err));
// });

router.get(
  "/paginate/user/:userID",
  middleware.auth,
  async (req, res, next) => {
    try {
      //let userids = await common.firmUsers(req.params.userID);
      let count = await Task.count({
        where: {
          status: {
            [Op.notIn]: ["approved", "completed", "canceled"]
          }
        },
        include: [
          {
            association: "assignees",
            where: {
              status: { $not: 1 },
              user_id: req.params.userID
            }
          }
        ]
      });

      let limit = req.query.limit ? parseInt(req.query.limit) : 10;
      let offset = 0;
      let page = req.query.page ? req.query.page : 1;
      let pages = Math.ceil(count / limit);
      offset = limit * (page - 1);
      let data = await Task.findAndCountAll({
        where: {
          status: {
            [Op.notIn]: ["approved", "completed", "canceled"]
          }
        },
        include: [
          {
            association: "assignees",
            include: [
              {
                association: "assignee",
                include: [{ association: "userdetail" }]
              }
            ],
            where: {
              status: { $not: 1 },
              user_id: req.params.userID
            }
          },
          {
            association: "clientdetail",
            attributes: ["id", "firstname", "surname", "companyname", "type"]
          },
          {
            association: "matter",
            attributes: ["id", "prefix", "casenumber"]
          },
          {
            association: "contact",
            attributes: ["id", "firstname", "surname"]
          },
          {
            association: "thirdparty",
            attributes: ["id", "firstname", "surname"]
          },
          {
            association: "user",
            attributes: ["id"],
            include: [
              {
                association: "userdetail",
                attributes: ["firstname", "lastname"]
              }
            ]
          }
        ],
        order: [["start_date", "DESC"]],
        limit: limit,
        offset: offset
      });
      data["count"] = count;
      res.status(200).json({
        success: 1,
        page: pages,
        data: data,
        pageno: parseInt(page),
        testpage: req.query.page,
        message: "All Task Content"
      });
    } catch (error) {
      next(error);
    }
  }
);

router.get(
  "/paginate/dailyScheduled/user/:userID",
  middleware.auth,
  async (req, res, next) => {
    var after2date = moment(new Date())
      .add(2, "days")
      .format("YYYY-MM-DD HH:mm");
    var afterdate = moment(new Date())
      .add(1, "days")
      .format("YYYY-MM-DD HH:mm");
    let userIDS = [req.params.userID];
    let role = "";
    let mainuser = await User.findAll({
      where: {
        id: req.params.userID
      }
    });

    if (mainuser.length) {
      for (mu of mainuser) {
        role = mu.role_id;
      }
      if (role == 1 || role == 2) {
        let users = await User.findAll({
          where: {
            parent_user_id: req.params.userID
          }
        });

        if (users.length) {
          for (ud of users) {
            userIDS.push(ud.id);
          }
        }
      }
    }

    let count = await Task.count({
      where: {
        status: {
          [Op.notIn]: ["approved", "completed", "canceled"]
        },
        start_date: {
          $between: [afterdate, after2date]
        }
      },
      include: [
        {
          association: "assignees",
          where: {
            status: { $not: 1 },
            user_id: userIDS
          }
        }
      ]
    });
    let limit = req.query.limit ? parseInt(req.query.limit) : 10;
    let offset = 0;
    let page = req.query.page ? req.query.page : 1;
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    let data = await Task.findAndCountAll({
      where: {
        status: {
          [Op.notIn]: ["approved", "completed", "canceled"]
        },
        start_date: {
          $between: [afterdate, after2date]
        }
      },
      include: [
        {
          association: "assignees",
          where: {
            status: { $not: 1 },
            user_id: userIDS
          }
        },
        { association: "matter" }
      ],
      order: [["start_date", "DESC"]],
      limit: limit,
      offset: offset
    });

    let clients = await Client.findAll({
      where: { user_id: req.params.userID },
      attributes: ["id"]
    });

    let client_ids = [];
    for (let client of clients) {
      client_ids.push(client.id);
    }

    let hearings = await Hearing.findAll({
      where: {
        client_id: client_ids,
        from_date: {
          $between: [afterdate, after2date]
        }
      },
      attributes: [
        "id",
        "client_id",
        "notes",
        "from_date",
        "from_time",
        "status",
        "updated_at"
      ],
      include: [
        { association: "matter", attributes: ["prefix", "casenumber"] },
        { association: "court", attributes: ["name"] },
        {
          association: "clientdetail",
          attributes: ["firstname", "surname", "mobile", "email"]
        },
        {
          association: "attorneys",
          include: [
            {
              association: "attorney",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"]
                }
              ]
            }
          ]
        }
      ]
    });

    let meetings = await Meeting.findAll({
      where: {
        [Op.or]: [
          { start_date: { $between: [afterdate, after2date] } },
          { end_date: { $between: [afterdate, after2date] } }
        ]
      },
      attributes: [
        "id",
        "subject",
        "description",
        "location",
        "start_date",
        "end_date",
        "start_time",
        "end_time",
        "created_at",
        "updated_at"
      ],
      include: [
        {
          association: "matter",
          attributes: ["id", "prefix", "casenumber"]
        },
        {
          association: "contact",
          attributes: ["id", "firstname", "surname"]
        },
        {
          association: "thirdparty",
          attributes: ["id", "firstname", "surname"]
        },
        {
          association: "assignees",
          where: { user_id: userIDS }
        }
      ]
    });

    var task = [];

    for (d of data.rows) {
      task.push({
        subject: d.subject,
        description: d.description,
        updated_at: d.updated_at,
        status: d.status
      });
    }
    for (h of hearings) {
      task.push({
        subject:
          h.clientdetail.firstname +
          " " +
          h.clientdetail.surname +
          " " +
          "hearings",
        description: h.notes,
        updated_at: h.updated_at,
        status: h.status
      });
    }

    for (m of meetings) {
      task.push({
        subject: m.subject,
        description: m.description,
        updated_at: m.updated_at,
        status: ""
      });
    }

    data["count"] = count;
    res.status(200).json({
      success: 1,
      page: pages,
      data: task,
      pageno: parseInt(page),
      testpage: req.query.page,
      message: "All Task Content",
      after2date: after2date,
      afterdate: afterdate,
      userIDS: userIDS,
      role: role,
      user_id: req.params.userID,
      hearing: hearings,
      meeting: meetings,
      task: data
    });
  }
);

router.get(
  "/paginate/user/:userID/completedTask",
  middleware.auth,
  (req, res, next) => {
    Task.count({
      where: {
        status: "completed"
      }
    })
      .then(count => {
        let limit = req.query.limit ? parseInt(req.query.limit) : 10;
        let offset = 0;
        let page = req.query.page ? req.query.page : 1;
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        Task.findAndCountAll({
          where: {
            status: "completed"
          },
          include: [
            {
              association: "assignees",
              where: {
                status: { $not: 1 },
                user_id: req.params.userID
              }
            },
            { association: "matter" }
          ],
          order: [["start_date", "DESC"]],
          limit: limit,
          offset: offset
        })
          .then(data => {
            data["count"] = count;
            res.status(200).json({
              success: 1,
              page: pages,
              data: data,
              pageno: parseInt(page),
              testpage: req.query.page,
              message: "All Completed Task Content"
            });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .catch(err => next(err));

    // let limit = req.query.limit || 10;
    // let page = req.query.page || 1;
    // Task.paginate({
    //         paginate: limit,
    //         page: page,
    //        where: {
    //             end_date: {
    //                 [Op.lt]: moment(new Date()).format("YYYY-MM-DD")
    //             }
    //         },
    //         include: [{
    //                 association: "assignees",
    //                 where: {
    //                     user_id: req.params.userID,

    //                 }
    //             },
    //             { association: "matter" },

    //         ],

    //     })
    //     .then(tasks => {
    //         res.status(200).json(tasks);
    //     })
    //     .catch(err => next(err));
  }
);

router.post("/updatestatus", async (req, res, next) => {
  if (req.body.status) {
    let result = await Task.findOne({
      where: { id: req.body.id },
      include: [{ association: "assignees" }]
    });
    if (result) {
      result.update(req.body);

      if (result.assignees.length) {
        for (attorney of result.assignees) {
          let createdBy = await User.findByPk(req.body.created_by, {
            include: [{ association: "userdetail" }]
          });
          createdBy =
            createdBy != null
              ? createdBy.userdetail.firstname +
                " " +
                createdBy.userdetail.lastname
              : "Unknown";
          let notification = await Notification.create({
            title: `Task status changed`,
            content: `${
              result.subject
            } is ${req.body.status.toUpperCase()}|By:${createdBy}`,
            ref_id: result.id,
            ref_module: "task",
            user_id: attorney.user_id
          });
        }
      }
    }
    // res.status(200).json(result);
    res.status(200).json(result);
  } else {
    res.status(401).json("cannot update staṭus");
  }
});

router.get(
  "/paginate/user/:userID/pastTasks",
  middleware.auth,
  async (req, res, next) => {
    Task.count({
      where: {
        status: {
          [Op.notIn]: ["approved", "completed", "canceled"]
        },
        end_date: {
          [Op.lt]: new Date()
        }
      },
      include: [
        {
          association: "assignees",
          where: {
            status: { $not: 1 },
            user_id: req.params.userID
          }
        }
      ]
    })
      .then(count => {
        let limit = req.query.limit ? parseInt(req.query.limit) : 10;
        let offset = 0;
        let page = req.query.page ? req.query.page : 1;
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        Task.findAndCountAll({
          where: {
            status: {
              [Op.notIn]: ["approved", "completed", "canceled"]
            },
            end_date: {
              [Op.lt]: new Date()
            }
          },
          include: [
            {
              association: "assignees",
              where: {
                user_id: req.params.userID
              }
            },
            { association: "matter" }
          ],
          order: [["end_date", "ASC"]],
          limit: limit,
          offset: offset
        })
          .then(data => {
            data["count"] = count;
            res.status(200).json({
              success: 1,
              page: pages,
              data: data,
              pageno: parseInt(page),
              testpage: req.query.page,
              message: "All Past Task Content"
            });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .catch(err => next(err));
  }
);

router.get("/id/:id", middleware.auth, async (req, res, next) => {
  let task = await Task.findByPk(req.params.id);

  let assignees = await TaskAssignee.findAll({ where: { task_id: task.id } });
  let pending_assignees = await TaskPendingAssignee.findAll({
    where: { task_id: task.id }
  });

  let admins = [];
  let attorneys = [];
  if (assignees.length) {
    for (let item of assignees) {
      let user = await User.findByPk(item.user_id);
      if (user != null) {
        if (
          user.role_id == 2 &&
          user.parent_user_id == null &&
          user.isattorney == 1
        )
          attorneys.push(user.id);
        if (
          user.role_id == 2 &&
          user.parent_user_id == null &&
          user.isadmin == 1
        )
          admins.push(user.id);
        if (user.role_id == 2 && user.parent_user_id != null)
          admins.push(user.id);
        if (user.role_id == 3) attorneys.push(user.id);
      }
    }
  }

  if (pending_assignees.length) {
    for (let item of pending_assignees) {
      let user = await User.findByPk(item.user_id);
      if (user != null) {
        if (
          user.role_id == 2 &&
          user.parent_user_id == null &&
          user.isattorney == 1
        )
          attorneys.push(user.id);
        if (
          user.role_id == 2 &&
          user.parent_user_id == null &&
          user.isadmin == 1
        )
          admins.push(user.id);
        if (user.role_id == 2 && user.parent_user_id != null)
          admins.push(user.id);
        if (user.role_id == 3) attorneys.push(user.id);
      }
    }
  }

  admins = admins.filter(function(item, pos) {
    return admins.indexOf(item) == pos;
  });

  attorneys = attorneys.filter(function(item, pos) {
    return attorneys.indexOf(item) == pos;
  });

  return res.status(200).json({ task, assignees, admins, attorneys });
});

router.put(
  "/id/:id",
  [middleware.validations, middleware.auth],
  async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    let start_date = req.body.start_date;
    let end_date = req.body.end_date;
    let start_time = req.body.start_time;
    let end_time = req.body.end_time;

    if (req.body.start_time != "") {
      req.body.start_time = new Date(
        start_date + " " + Helper.convertTime(start_time) + ":00Z"
      );
    }
    if (req.body.end_time != "") {
      req.body.end_time = new Date(
        end_date + " " + Helper.convertTime(end_time) + ":00Z"
      );
    }

    req.body.start_date =
      moment(new Date(start_date)).format("YYYY-MM-DD") +
      " " +
      Helper.convertTime(start_time) +
      "Z";
    req.body.end_date =
      moment(new Date(end_date)).format("YYYY-MM-DD") +
      " " +
      Helper.convertTime(end_time) +
      "Z";

    req.body.status = req.body.status || "open";

    if (req.body.client_id == "") delete req.body.client_id;
    if (req.body.case_id == "") delete req.body.case_id;
    if (req.body.contact_id == "") delete req.body.contact_id;
    if (req.body.thirdparty_id == "") delete req.body.thirdparty_id;

    let task = await Task.findByPk(req.params.id);
    if (task != null) {
      task.update(req.body);
      if (req.body.assignees.length) {
        let arr_assignees = req.body.assignees.map(item => item.user_id);
        arr_assignees = [...new Set(arr_assignees)];
        let exists_assignees = await TaskAssignee.findAll({
          where: { task_id: task.id }
        });
        exists_assignees = exists_assignees.length
          ? exists_assignees.map(item => item.user_id)
          : [];

        let args = [];
        let pending_args = [];
        for (let user_id of arr_assignees) {
          if (
            exists_assignees.length &&
            exists_assignees.indexOf(user_id) != -1
          ) {
            args.push({ task_id: task.id, user_id: user_id, status: 2 });
          } else {
            pending_args.push({ task_id: task.id, user_id: user_id });
          }
        }

        await TaskAssignee.destroy({
          where: { task_id: task.id }
        });

        await TaskPendingAssignee.destroy({
          where: { task_id: task.id }
        });

        let new_assignees = await TaskAssignee.bulkCreate(args);
        let pending_new_assignees = await TaskPendingAssignee.bulkCreate(
          pending_args
        );
      }
      res.status(200).json(task);
    } else return res.status(404).json("Task is not found!");
  }
);

router.delete("/id/:id", middleware.auth, async (req, res, next) => {
  let task = await Task.findByPk(req.params.id);
  if (task != null) {
    task.destroy();
    return res.status(204).json({});
  } else return res.status(404).json("Task is not found!");
});

router.get("/paginate/user/:userID/pendingTasks", async (req, res, next) => {
  try {
    TaskPendingAssignee.count({
      where: {
        user_id: req.params.userID
      }
    })
      .then(count => {
        TaskPendingAssignee.findAll({
          where: {
            user_id: req.params.userID
          }
        })
          .then(data => {
            var taskID = [];
            var userid = [];
            for (d of data) {
              taskID.push(d.task_id);
              userid.push(d.user_id);
            }
            console.log(userid);
            let limit = req.query.limit ? parseInt(req.query.limit) : 10;
            let offset = 0;
            let page = req.query.page ? req.query.page : 1;
            let pages = Math.ceil(count / limit);
            offset = limit * (page - 1);
            Task.findAndCountAll({
              where: {
                id: taskID
                //user_id: userid
              },
              include: [{ association: "matter" }],
              order: [["start_date", "DESC"]],
              limit: limit,
              offset: offset
            })
              .then(pendingTask => {
                res.status(200).json({
                  data: pendingTask,
                  task: data,
                  id: userid,
                  taskID: taskID
                });
              })
              .catch(error => {
                console.log(error);
              });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .catch(error => {
        console.log(error);
      });
  } catch (error) {
    next(error);
  }
});

router.post("/acceptTask", middleware.auth, async (req, res, next) => {
  let taskassignee = "";
  let pendingTask = await TaskPendingAssignee.findOne({
    where: { task_id: req.body.id, user_id: req.body.user_id }
  });
  if (pendingTask != null) {
    taskassignee = await TaskAssignee.create({
      task_id: req.body.id,
      user_id: req.body.user_id,
      status: 2
    });
  }
  if (taskassignee != null) {
    let destroyed = await pendingTask.destroy();
  }
  res.status(200).json({ code: 200, taskassignee: taskassignee });
});

router.post("/declineTask", middleware.auth, async (req, res, next) => {
  let taskassignee = "";
  let pendingTask = await TaskPendingAssignee.findOne({
    where: { task_id: req.body.id, user_id: req.body.user_id }
  });
  if (pendingTask != null) {
    taskassignee = await TaskAssignee.create({
      task_id: req.body.id,
      user_id: req.body.user_id,
      status: 1
    });
  }
  if (taskassignee != null) {
    let destroyed = await pendingTask.destroy();

    let user = await User.findByPk(req.body.user_id, {
      include: [{ association: "userdetail" }]
    });
    if (user != null) {
      let task = await Task.findByPk(req.body.id);
      if (task != null) {
        let createdBy =
          user.userdetail.firstname + " " + user.userdetail.lastname;
        let notification = await Notification.create({
          title: "Task declined",
          content: task.subject + "|By:" + createdBy,
          ref_id: task.id,
          ref_module: "task",
          user_id: task.user_id
        });
      }
    }
  }
  res.status(200).json({ code: 200, taskassignee: taskassignee });
});

module.exports = router;
