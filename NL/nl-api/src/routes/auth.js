const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const env = require("../../env");
const { check, validationResult } = require("express-validator/check");
const User = require("../models/User");
const Sequelize = require("sequelize");
const LoginLog = require("../models/LoginLog");
const Task = require("../models/dashboard/Task");
const common = require("../helpers/common");
const Billing = require("../models/Billing");
const Payment = require("../models/Payment");


router.post(
  "/",
  [
    check("username").isLength({ min: 1 }),
    check("password").isLength({ min: 1 })
  ],
  async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ success: 0, errors: errors.array() });
      }
      let usernameOrEmail = req.body.username;
      let password = req.body.password;

      let user = await User.findOne({
        where: {
          [Sequelize.Op.or]: {
            username: usernameOrEmail,
            email: usernameOrEmail
          },
          isactive: 1,
          role_id: [2, 3]
        },
        include: [
          { association: "userdetail" },
          {
            association: "permissions",
            attributes: [
              "permission_id",
              "view",
              "add",
              "edit",
              "delete",
              "task_assign_admin",
              "task_assign_attorney"
            ],
            include: [
              { association: "calendarAttorneys" },
              { association: "timerAttorneys" },
              { association: "permission" }
            ]
          }
        ]
      });
      if (user) {
        let result = await bcrypt.compare(password, user.password);
        if (result) {
          const token = jwt.sign(
            { username: usernameOrEmail, password: password },
            env.jwtSecret,
            { expiresIn: "36500d" }
          );
          user.update({ last_login: Date.now() });
          let log = await LoginLog.create({
            ip: req.headers["x-real-ip"] || "0.0.0.0",
            attorney_id: user.id
          });
          let logsCount = await LoginLog.findAll({
            where: {
              attorney_id: user.id
            }
          });
          let tasks = await Task.findAll({
            attributes: [
              "id",
              "subject",
              "location",
              "start_date",
              "end_date",
              "start_time",
              "end_time",
              "all_day_event",
              "description",
              "status"
            ],
            where: {
              status: { [Sequelize.Op.notIn]: ["completed", "canceled"] },
              end_date: { $lt: new Date() }
            },
            include: [{ association: "assignees", where: { user_id: user.id } }]
          });

          //collect root user expiry date
          let userIds = await common.firmUsers(user.id);
          let rootUser = await User.findOne({
            where: { id: userIds, role_id: 2, parent_user_id: null }
          });
          let plan_expire_date = "";
          if (rootUser != null) plan_expire_date = rootUser.expiry_date;

          let billing = await Billing.findOne({
            where: { billing_useremail: rootUser.email }
          });
          let billing_date = billing != null ? billing.created_at : null;

          let payments = await Payment.findAll({
            attributes: ["id", "created_at"],
            where: { user_id: rootUser.id, status: "completed" },
            order: [["created_at", "DESC"]]
          });

          let plan = payments.length ? 2 : 1;

          billing_date = payments.length ? payments[0].created_at : null;

          res.status(201).json({
            success: 1,
            data: {
              access_token: "Bearer " + token,
              user: user,
              ip: req.connection.remoteAddress,
              pastTasks: tasks.length,
              plan_expire_date: plan_expire_date,
              billing_date: billing_date,
              type: plan
            },
            message: "Login Success",
            user_id: user.id,
            logincount: logsCount.length
          });
        } else {
          res.status(404).json({
            success: 0,
            error: 404,
            data: {},
            message: "Username or Password is not found in our records"
          });
        }
      } else {
        res.status(404).json({
          success: 0,
          error: 404,
          data: {},
          message: "Username or Password is not found in our records"
        });
      }
    } catch (err) {
      console.log(err);
      next(err);
    }
  }
);

router.post(
  "/backend",
  [
    check("username").isLength({ min: 1 }),
    check("password").isLength({ min: 1 })
  ],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ success: 0, errors: errors.array() });
    }
    let usernameOrEmail = req.body.username;
    let password = req.body.password;

    User.findOne({
      where: {
        [Sequelize.Op.or]: {
          username: usernameOrEmail,
          email: usernameOrEmail,
          role_id: 1
        }
      },
      include: [
        { association: "userdetail" },
        {
          association: "permissions",
          attributes: [
            "permission_id",
            "view",
            "add",
            "edit",
            "delete",
            "task_assign_admin",
            "task_assign_attorney"
          ],
          include: [
            { association: "calendarAttorneys" },
            { association: "timerAttorneys" },
            { association: "permission" }
          ]
        }
      ]
    })
      .then(user => {
        if (user != null) {
          bcrypt.compare(password, user.password, (err, result) => {
            if (err) next(err);

            if (result) {
              const token = jwt.sign(
                { username: usernameOrEmail, password: password },
                env.jwtSecret,
                { expiresIn: "36500d" }
              );
              user.update({ last_login: Date.now() });
              LoginLog.create({
                ip: req.headers["x-real-ip"] || "0.0.0.0",
                attorney_id: user.id
              });

              LoginLog.findAll({
                where: {
                  attorney_id: user.id
                }
              })
                .then(count => {
                  res.status(201).json({
                    success: 1,
                    data: {
                      access_token: "Bearer " + token,
                      user: user,
                      ip: req.connection.remoteAddress
                    },
                    message: "Login Success",
                    logincount: count.length
                  });
                })
                .catch(err => {
                  res.status(500).json(err);
                });
            } else {
              res.status(404).json({
                success: 0,
                error: 404,
                data: {},
                message: "Username or Password is not found in our records"
              });
            }
          });
        } else {
          res.status(404).json({
            success: 0,
            error: 404,
            data: {},
            message: "Username or Password is not found in our records"
          });
        }
      })
      .catch(err => res.status(500).json(err));
  }
);

module.exports = router;
