const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const { check, validationResult } = require("express-validator/check");
const CaseDetails = require("../../models/CaseDetails");
const Attorney = require("../../models/User");
let limit = 999;
const Common = require("../../helpers/common");
const moment = require("moment");
const { Op } = require("sequelize");

// router.get("/user/:userID", checkToken, async(req, res, next) => {
//     let offset = 0;
//     let userIds = await Common.firmUsers(req.params.userID);
//     CaseDetails.count({
//         where: {
//             user_id: userIds
//         }
//     }).then(count => {
//         limit = req.query.limit ? parseInt(req.query.limit) : limit;
//         let page = req.query.page ? req.query.page : 1; // page number
//         let pages = Math.ceil(count / limit);
//         offset = limit * (page - 1);
//         let primaryAttorneyID = "";
//         let assistAttorneyID = "";
//         let status = [1, 2, 3, 4];

//         if (
//             (req.query.attorney_id != "" && req.query.attorney_type != "") ||
//             (req.query.status != "" && req.query.status != undefined)
//         ) {
//             if (req.query.attorney_type == "primary") {
//                 if (req.query.attorney_id == 'all') {
//                     primaryAttorneyID = "";
//                 } else {
//                     primaryAttorneyID = { id: req.query.attorney_id };
//                 }

//             } else if (req.query.attorney_type == "asst") {
//               if (req.query.attorney_id == 'all') {
//                 assistAttorneyID = "";
//             } else {
//               assistAttorneyID = { id: req.query.attorney_id };
//             }
//             }

//             status =
//                 req.query.status != "" && req.query.status != undefined ?
//                 req.query.status : [1, 2, 3, 4];
//         }
//         CaseDetails.findAndCountAll({
//                 where: {
//                     user_id: userIds,
//                     status: status
//                 },
//                 include: [{
//                         association: "clientdetail"
//                     },
//                     {
//                         association: "casethirdparty",
//                         include: [
//                             { association: "clientcontact", as: "contact" },
//                             { association: "clientthirdparty", as: "thirdparty" }
//                         ]
//                     },
//                     {
//                         association: "principleAttorney",
//                         include: [{ association: "userdetail" }],
//                         where: primaryAttorneyID
//                     },
//                     {
//                         association: "assistattorney",
//                         include: [{
//                             association: "attorney",
//                             as: "assistattorney",
//                             include: [{ association: "userdetail" }],
//                             where: assistAttorneyID
//                         }]
//                     },
//                     { association: "category" },
//                     {
//                         association: "tasks",
//                         limit: 5
//                     }
//                 ],
//                 order: [
//                     ["created_at", "DESC"]
//                 ],
//                 limit: limit,
//                 offset: offset
//             })
//             .then(cases => {
//                 cases["count"] = count;
//                 res.status(200).json({
//                     pages: pages,
//                     data: cases,
//                     attorney_id: req.query.attorney_id,
//                     alluser: userIds,
//                     caseid: cases.rows.length ? cases.rows[0].id : ""
//                 });
//             })
//             .catch(err => {
//                 next(err);
//                 console.log(err);
//             });
//     });
// });

router.get("/user/:userID", checkToken, async (req, res, next) => {
  let offset = 0;
  let userIds = await Common.firmUsers(req.params.userID);
  CaseDetails.count({
    where: {
      user_id: userIds
    }
  }).then(count => {
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    let primaryAttorneyID = "";
    let assistAttorneyID = "";
    let status = [1, 2, 3, 4];

    if (
      (req.query.attorney_id != "" && req.query.attorney_type != "") ||
      (req.query.status != "" && req.query.status != undefined)
    ) {
      if (req.query.attorney_type == "primary") {
        if (req.query.attorney_id == "all") {
          primaryAttorneyID = "";
        } else {
          primaryAttorneyID = { id: req.query.attorney_id };
        }
      } else if (req.query.attorney_type == "asst") {
        if (req.query.attorney_id == "all") {
          assistAttorneyID = "";
        } else {
          assistAttorneyID = { id: req.query.attorney_id };
        }
      }

      status =
        req.query.status != "" && req.query.status != undefined
          ? req.query.status
          : [1, 2, 3, 4];
    }
    CaseDetails.findAndCountAll({
      where: {
        user_id: userIds,
        status: status
      },
      include: [
        {
          association: "clientdetail"
        },
        {
          association: "contacts"
        },
        // {
        //   association: "casethirdparty",
        //   include: [
        //     { association: "clientcontact", as: "contact" },
        //     { association: "clientthirdparty", as: "thirdparty" }
        //   ]
        // },
        {
          association: "principleAttorney",
          include: [{ association: "userdetail" }],
          where: primaryAttorneyID
        },
        {
          association: "assistattorney",
          include: [
            {
              association: "attorney",
              as: "assistattorney",
              include: [{ association: "userdetail" }],
              where: assistAttorneyID
            }
          ]
        },
        { association: "category" },
        {
          association: "tasks",
          where: {
            start_date: {
              [Op.gte]: Date.parse(moment(new Date()).format("YYYY-MM-DD"))
            }
          },
          limit: 5,
          required: false
        }
      ],
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset
    })
      .then(cases => {
        cases["count"] = count;
        res.status(200).json({
          pages: pages,
          data: cases,
          attorney_id: req.query.attorney_id,
          alluser: userIds,
          caseid: cases.rows.length ? cases.rows[0].id : ""
        });
      })
      .catch(err => {
        next(err);
        console.log(err);
      });
  });
});

router.get("/all/user/:userID", checkToken, async (req, res, next) => {
  let userIds = await Common.firmUsers(req.params.userID);
  let primaryAttorneyID = "";
  let assistAttorneyID = "";
  let status = [1, 2, 3, 4];

  if (
    (req.query.attorney_id != "" && req.query.attorney_type != "") ||
    (req.query.status != "" && req.query.status != undefined)
  ) {
    if (req.query.attorney_type == "primary") {
      if (req.query.attorney_id == "all") {
        primaryAttorneyID = "";
      } else {
        primaryAttorneyID = { id: req.query.attorney_id };
      }
    } else if (req.query.attorney_type == "asst") {
      if (req.query.attorney_id == "all") {
        assistAttorneyID = "";
      } else {
        assistAttorneyID = { id: req.query.attorney_id };
      }
    }

    status =
      req.query.status != "" && req.query.status != undefined
        ? req.query.status
        : [1, 2, 3, 4];
  }
  CaseDetails.findAll({
    where: {
      user_id: userIds,
      status: status
    },
    include: [
      {
        association: "clientdetail"
      },
      {
        association: "casethirdparty",
        include: [
          { association: "clientcontact", as: "contact" },
          { association: "clientthirdparty", as: "thirdparty" }
        ]
      },
      {
        association: "principleAttorney",
        include: [{ association: "userdetail" }],
        where: primaryAttorneyID
      },
      {
        association: "assistattorney",
        include: [
          {
            association: "attorney",
            as: "assistattorney",
            include: [{ association: "userdetail" }],
            where: assistAttorneyID
          }
        ]
      },
      { association: "category" }
    ],
    order: [["created_at", "DESC"]]
  })
    .then(cases => {
      res.status(200).json({
        data: cases
      });
    })
    .catch(err => {
      next(err);
    });
});

router.get("/attorneys/user/:userID", checkToken, async (req, res, next) => {
  try {
    let IDS = await Common.firmUsers(req.params.userID);
    let attorneys = await Attorney.findAll({
      where: { id: IDS, isactive: 1 },
      include: [{ association: "userdetail" }]
    });
    res.status(200).json(attorneys);
  } catch (error) {
    next(error);
  }
});

router.get("/specific/user/:userID", checkToken, (req, res, next) => {
  CaseDetails.findAll({
    where: {
      client_id: req.params.userID
    },
    order: [["created_at", "DESC"]]
  })
    .then(cases => {
      res.status(200).json({
        data: cases
      });
    })
    .catch(err => {
      next(err);
    });
});

router.get("/user/:userID/caseNumber", checkToken, async (req, res, next) => {
  let userIds = await Common.firmUsers(req.params.userID);
  CaseDetails.findAll({
    where: {
      user_id: userIds
    },
    include: [
      {
        association: "clientdetail"
      },
      {
        association: "casethirdparty",
        include: [
          { association: "clientcontact", as: "contact" },
          { association: "clientthirdparty", as: "thirdparty" }
        ]
      },
      {
        association: "principleAttorney",
        include: [{ association: "userdetail" }]
      },
      {
        association: "assistattorney",
        include: [
          {
            association: "attorney",
            as: "assistattorney",
            include: [{ association: "userdetail" }]
          }
        ]
      },
      { association: "category" }
    ],
    order: [["created_at", "DESC"]]
  })
    .then(cases => {
      res.status(200).json({
        data: cases
      });
    })
    .catch(err => {
      next(err);
    });
});

module.exports = router;
