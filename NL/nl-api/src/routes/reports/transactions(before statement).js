const express = require("express");
const sq = require("sequelize");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const Allocations = require("../../models/accounts/MoneyAllocation");
const CreditNote = require("../../models/accounts/CreditNote");
const Client = require("../../models/Client");
const CaseDetails = require("../../models/CaseDetails");
const moment = require("moment");
const common = require("../../helpers/common");
const Transaction = require("../../models/accounts/Transaction");
const Invoice = require("../../models/accounts/Invoice");
router.get("/client/:id", checkToken, async(req, res, next) => {
    let from_date = req.query.from_date || null;
    let to_date = req.query.to_date || null;
    let case_id = req.query.case_number || null;
    let params = [{ client_id: req.params.id, case_id: case_id }];

    if (from_date != null && to_date != null)
        params.push({ created_at: { $between: [from_date, to_date] } });
    if (from_date != null && to_date == null)
        params.push({ created_at: { $gte: from_date } });
    if (from_date == null && to_date != null)
        params.push({ created_at: { $lte: to_date } });

    var conditions = params.reduce((a, b) => Object.assign(a, b), {});
    let allocations = await Allocations.findAll({
        where: conditions,
        attributes: ["id", "created_at", "invoice_id", "description", "amount"]
    });

    var whereStatement = {};
    if (req.body.fromDate) {
        (whereStatement.created_at = {
            $between: [from_date, to_date]
        }),
        (whereStatement.case_id = case_id);
    } else {
        whereStatement.case_id = case_id;
    }


    let creditnotes = await CreditNote.findAll({
        where: whereStatement,
        attributes: [
            "id",
            "created_at",
            "invoice_id",
            "message",
            "total",
            "case_id"
        ]
    });


    let transaction = await Transaction.findAll({
        attributes: ["id", "type", "status", "created_at"],
        include: [{
                association: "matter",
                // attributes: ["id", "prefix", "casenumber", "status"]
            },
            {
                association: "trust",
                // attributes: ["id", "amount", "description"]
            },
            {
                association: "allocation",
                //attributes: ["id", "amount", "description"]
            },

        ],
        where: whereStatement,
        order: [
            ["created_at", "ASC"]
        ]
    });

    let client = await Client.findByPk(req.params.id, {
        attributes: ["firstname", "surname", "companyname", "type"]
    });

    // var caseid = [];
    // for (crd of creditnotes) {
    //     caseid.push(crd.case_id);
    // }

    let caseDetails = await CaseDetails.findAll({
        where: { id: case_id },
        attributes: ["prefix", "casenumber"]
    });

    let invoice = await Invoice.findAll({
        where: conditions,
    })

    res
        .status(200)
        .json({ client, allocations, creditnotes, caseDetails, conditions, invoice, transaction });
});


module.exports = router;