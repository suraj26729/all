const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const CaseThirdParty = require("../../models/CaseThirdparty");
const CaseContacts = require("../../models/CaseContacts");
const CaseDetails = require("../../models/CaseDetails");
const Common = require("../../helpers/common");
let limit = 999;

router.get("/user/:userID", checkToken, async(req, res, next) => {
    let UserIDS = await Common.firmUsers(req.params.userID);
    let offset = 0;
    let primaryAttorneyID = {};
    let assistAttorneyID = {};
    let status = [1, 2, 3, 4];

    if (
        (req.query.attorney_id && req.query.attorney_type) ||
        (req.query.status != "" && req.query.status != undefined)
    ) {
        if (req.query.attorney_type == "primary") {
            if (req.query.attorney_id == 'all') {
                primaryAttorneyID = "";
            } else {
                primaryAttorneyID = { id: req.query.attorney_id };
            }
        } else if (req.query.attorney_type == "asst") {
            if (req.query.attorney_id == 'all') {
                assistAttorneyID = "";
            } else {
                assistAttorneyID = { attorney_id: req.query.attorney_id };
            }
        }
        status =
            req.query.status != "" && req.query.status != undefined ?
            req.query.status : [1, 2, 3, 4];
    }

    let caseIds = await CaseDetails.findAll({
        where: { user_id: UserIDS, status: status },
        attributes: ["id"]
    });
    caseIds = caseIds.map(row => row.id);
    let count = await CaseThirdParty.count({
        where: { case_id: caseIds }
    });
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);

    let thirdparties = await CaseThirdParty.findAndCountAll({
        where: { case_id: caseIds },
        include: [{
            association: "clientthirdparty",
            attributes: [
                "firstname",
                "surname",
                "email",
                "contact1",
                "contact2",
                "mobile"
            ]
        }],
        limit: limit,
        offset: offset
    });

    let _thirdparties = [];
    count = thirdparties.count;
    for (let thirdparty of thirdparties.rows) {
        let matter = await CaseDetails.findByPk(thirdparty.case_id, {
            include: [{
                    association: "clientdetail",
                    //attributes: ["firstname", "surname", "email"]
                },
                { association: "category", attributes: ["name"] },
                {
                    association: "principleAttorney",
                    include: [{ association: "userdetail" }],
                    where: primaryAttorneyID
                },
                {
                    association: "assistattorney",
                    include: [{
                        association: "attorney",
                        as: "assistattorney",
                        include: [{ association: "userdetail" }]
                    }],
                    where: assistAttorneyID
                }
            ],
            attributes: ["prefix", "casenumber", "status"]
        });
        if (matter != null) {
            _thirdparties.push({
                matter: matter,
                thirdparty: thirdparty.clientthirdparty
            });
        }
    }

    res.status(200).json({
        pages: pages,
        data: _thirdparties,
        total: count
            //userids: UserIDS
    });
});
router.get("/attorneys/user/:userID", checkToken, async(req, res, next) => {
    let userIds = await Common.firmUsers(req.params.userID);
    Attorney.findAll({
            where: { id: userIds, role_id: 3 },
            include: [{ association: "userdetail" }]
        })
        .then(attorneys => {
            res.status(200).json(attorneys);
        })
        .catch(err => next(err));
});

router.get("/all/user/:userID", checkToken, async(req, res, next) => {
    let userIds = await Common.firmUsers(req.params.userID);
    let primaryAttorneyID = {};
    let assistAttorneyID = {};
    let status = [1, 2, 3, 4];

    if (req.query.attorney_id && req.query.attorney_type) {
        if (req.query.attorney_type == "primary") {
            primaryAttorneyID = { id: req.query.attorney_id };
        } else if (req.query.attorney_type == "asst") {
            assistAttorneyID = { id: req.query.attorney_id };
        }
    }

    let caseIds = await CaseDetails.findAll({
        where: { user_id: userIds },
        attributes: ["id"]
    });
    caseIds = caseIds.map(row => row.id);

    let thirdparties = await CaseContacts.findAll({
        where: { case_id: caseIds },
        include: [{
            association: "clientcontact",
            attributes: [
                "firstname",
                "surname",
                "email",
                "contact1",
                "contact2",
                "mobile"
            ],
            where: { type: "t" }
        }]
    });

    let _thirdparties = [];
    for (let thirdparty of thirdparties) {
        let matter = await CaseDetails.findByPk(thirdparty.case_id, {
            include: [{
                    association: "clientdetail",
                    attributes: ["firstname", "surname", "email"]
                },
                { association: "category", attributes: ["name"] },
                {
                    association: "principleAttorney",
                    include: [{ association: "userdetail" }]
                        //where: primaryAttorneyID
                },
                {
                    association: "assistattorney",
                    include: [{
                            association: "attorney",
                            as: "assistattorney",
                            include: [{ association: "userdetail" }]
                        }]
                        //where: assistAttorneyID
                }
            ],
            attributes: ["prefix", "casenumber", "status"]
        });
        _thirdparties.push({
            matter: matter,
            thirdparty: thirdparty.clientcontact
        });
    }

    res.status(200).json({
        data: _thirdparties,
        total: caseIds.length
    });
});

module.exports = router;