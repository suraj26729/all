const express = require("express");
const sq = require("sequelize");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const db = require("../../config/database");
const Invoice = require("../../models/accounts/Invoice");
const common = require("../../helpers/common");

router.get("/user/:user_id", checkToken, async (req, res, next) => {
  try {
    let userIds = await common.firmUsers(req.params.user_id);
    userIds = userIds.join(",");

    let query = `select client_id, firstname, surname, companyname,type,
    sum(rate5) as rate5, sum(rate4) as rate4, sum(rate3) as rate3, sum(rate2) as rate2, sum(rate1) as rate1
    
    from (select invoices.client_id, invoices.id, invoices.invoice_number, client_details.firstname, client_details.surname, client_details.companyname,
    client_details.type,
    
    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 3 MONTH then (ROUND(invoices.total,1)-ifnull(amount,'0')) else 0 end ),2)  as rate5,
    
    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 2 MONTH and invoices.created_at >= NOW() - INTERVAL 3 MONTH 
    then (ROUND(invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate4, 
    
    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 1 MONTH and invoices.created_at >= NOW() - INTERVAL 2 MONTH 
    then (ROUND(invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate3,
    
    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 1 DAY and invoices.created_at >= NOW() - INTERVAL 1 MONTH 
    then (ROUND(invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate2,
    
    TRUNCATE((case when invoices.created_at >= NOW() - INTERVAL 1 DAY then (ROUND(invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate1,
    
    IFNULL(a.amount,0) as paid_amt, invoices.created_at as crdate
    
    FROM invoices
    
    JOIN client_details ON client_details.id=invoices.client_id
    
    LEFT JOIN (select ROUND(sum(amount),2) as amount, invoice_id from money_allocations group by invoice_id ) a on a.invoice_id = invoices.id
    
    WHERE client_details.isactive =  1 AND client_details.user_id IN (${userIds}) AND client_details.deleted_at IS null
    
    ORDER BY invoices.id desc, invoices.created_at DESC) as aaa group by client_id`;

    let invoice = await db.query(query, { type: sq.QueryTypes.SELECT });

    //Timesheet Invoice
    query = `select client_id, firstname, surname, companyname,type,
    sum(rate5) as rate5, sum(rate4) as rate4, sum(rate3) as rate3, sum(rate2) as rate2, sum(rate1) as rate1
    
    from (select timesheet_invoices.client_id, timesheet_invoices.id, timesheet_invoices.invoice_number, client_details.firstname, client_details.surname, client_details.companyname,
    client_details.type,
    
    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 3 MONTH then (ROUND(timesheet_invoices.total,1)-ifnull(amount,'0')) else 0 end ),2)  as rate5,
    
    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 2 MONTH and timesheet_invoices.created_at >= NOW() - INTERVAL 3 MONTH 
    then (ROUND(timesheet_invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate4, 
    
    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 1 MONTH and timesheet_invoices.created_at >= NOW() - INTERVAL 2 MONTH 
    then (ROUND(timesheet_invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate3,
    
    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 1 DAY and timesheet_invoices.created_at >= NOW() - INTERVAL 1 MONTH 
    then (ROUND(timesheet_invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate2,
    
    TRUNCATE((case when timesheet_invoices.created_at >= NOW() - INTERVAL 1 DAY then (ROUND(timesheet_invoices.total,1)-ifnull(amount,'0')) else 0 end),2)  as rate1,
    
    IFNULL(a.amount,0) as paid_amt, timesheet_invoices.created_at as crdate
    
    FROM timesheet_invoices
    
    JOIN client_details ON client_details.id=timesheet_invoices.client_id
    
    LEFT JOIN (select ROUND(sum(amount),2) as amount, invoice_id from money_allocations group by invoice_id ) a on a.invoice_id = timesheet_invoices.id
    
    WHERE client_details.isactive =  1 AND client_details.user_id IN (${userIds}) AND client_details.deleted_at IS null
    
    ORDER BY timesheet_invoices.id desc, timesheet_invoices.created_at DESC) as aaa group by client_id`;

    let timesheet_invoice = await db.query(query, {
      type: sq.QueryTypes.SELECT
    });

    if (timesheet_invoice.length) {
      invoice[0].rate1 += timesheet_invoice[0].rate1;
      invoice[0].rate2 += timesheet_invoice[0].rate2;
      invoice[0].rate3 += timesheet_invoice[0].rate3;
      invoice[0].rate4 += timesheet_invoice[0].rate4;
      invoice[0].rate5 += timesheet_invoice[0].rate5;
    }

    return res.status(200).json(invoice);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.get(
  "/user/:userId/client/:clientId",
  checkToken,
  async (req, res, next) => {
    let userIds = await common.firmUsers(req.params.userId);
    userIds = userIds.join(",");
    let query = `select invoices.client_id, invoices.id, invoices.invoice_number, client_details.firstname, client_details.surname, client_details.companyname,client_details.type,case_details.casenumber, case_details.prefix,

    'i' as type,

    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 3 MONTH then ROUND(invoices.total,1) else 0 end ),2)  as rate5,
    
    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 2 MONTH and invoices.created_at >= NOW() - INTERVAL 3 MONTH 
    then ROUND(invoices.total,1) else 0 end),2)  as rate4, 
    
    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 1 MONTH and invoices.created_at >= NOW() - INTERVAL 2 MONTH 
    then ROUND(invoices.total,1) else 0 end),2)  as rate3,
    
    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 1 DAY and invoices.created_at >= NOW() - INTERVAL 1 MONTH 
    then ROUND(invoices.total,1) else 0 end),2)  as rate2,
    
    TRUNCATE((case when invoices.created_at >= NOW() - INTERVAL 1 DAY then ROUND(invoices.total,1) else 0 end),2)  as rate1,
    
    IFNULL(a.amount,0) as paid_amt, invoices.created_at as crdate
    
    FROM invoices
    
    JOIN client_details ON client_details.id = invoices.client_id

    JOIN case_details ON case_details.id = invoices.case_id
    
    LEFT JOIN (select ROUND(sum(amount),2) as amount, invoice_id from money_allocations group by invoice_id ) a on a.invoice_id = invoices.id
    
    WHERE client_details.isactive =  1 AND client_details.user_id IN (${userIds}) AND client_details.id = '${req.params.clientId}' AND client_details.deleted_at IS null
    
    ORDER BY invoices.id desc, invoices.created_at DESC`;

    let invoice = await db.query(query, { type: sq.QueryTypes.SELECT });

    query = `select timesheet_invoices.client_id, timesheet_invoices.id, timesheet_invoices.invoice_number, client_details.firstname, client_details.surname, client_details.companyname,client_details.type,case_details.casenumber, case_details.prefix,

    't' as type,

    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 3 MONTH then ROUND(timesheet_invoices.total,1) else 0 end ),2)  as rate5,
    
    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 2 MONTH and timesheet_invoices.created_at >= NOW() - INTERVAL 3 MONTH 
    then ROUND(timesheet_invoices.total,1) else 0 end),2)  as rate4, 
    
    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 1 MONTH and timesheet_invoices.created_at >= NOW() - INTERVAL 2 MONTH 
    then ROUND(timesheet_invoices.total,1) else 0 end),2)  as rate3,
    
    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 1 DAY and timesheet_invoices.created_at >= NOW() - INTERVAL 1 MONTH 
    then ROUND(timesheet_invoices.total,1) else 0 end),2)  as rate2,
    
    TRUNCATE((case when timesheet_invoices.created_at >= NOW() - INTERVAL 1 DAY then ROUND(timesheet_invoices.total,1) else 0 end),2)  as rate1,
    
    IFNULL(a.amount,0) as paid_amt, timesheet_invoices.created_at as crdate
    
    FROM timesheet_invoices
    
    JOIN client_details ON client_details.id = timesheet_invoices.client_id

    JOIN case_details ON case_details.id = timesheet_invoices.case_id
    
    LEFT JOIN (select ROUND(sum(amount),2) as amount, invoice_id from money_allocations group by invoice_id ) a on a.invoice_id = timesheet_invoices.id
    
    WHERE client_details.isactive =  1 AND client_details.user_id IN (${userIds}) AND client_details.id = '${req.params.clientId}' AND client_details.deleted_at IS null
    
    ORDER BY timesheet_invoices.id desc, timesheet_invoices.created_at DESC`;

    let timesheet_invoice = await db.query(query, {
      type: sq.QueryTypes.SELECT
    });

    let result = [...invoice, ...timesheet_invoice].sort((a, b) =>
      a.crdate < b.crdate ? 1 : -1
    );

    return res.status(200).json(result);
  }
);

router.get(
  "/user/:userId/client/:clientId/case/:CaseId",
  checkToken,
  async (req, res, next) => {
    let userIds = await common.firmUsers(req.params.userId);
    userIds = userIds.join(",");

    let query = `select invoices.client_id, invoices.id, invoices.invoice_number, client_details.firstname, client_details.surname, client_details.companyname,client_details.type, case_details.casenumber, case_details.prefix,

    'i' as type,

    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 3 MONTH then ROUND(invoices.total,1) else 0 end ),2)  as rate5,

    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 2 MONTH and invoices.created_at >= NOW() - INTERVAL 3 MONTH 
    then ROUND(invoices.total,1) else 0 end),2)  as rate4, 

    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 1 MONTH and invoices.created_at >= NOW() - INTERVAL 2 MONTH 
    then ROUND(invoices.total,1) else 0 end),2)  as rate3,

    TRUNCATE((case when invoices.created_at <= NOW() - INTERVAL 1 DAY and invoices.created_at >= NOW() - INTERVAL 1 MONTH 
    then ROUND(invoices.total,1) else 0 end),2)  as rate2,

    TRUNCATE((case when invoices.created_at >= NOW() - INTERVAL 1 DAY then ROUND(invoices.total,1) else 0 end),2)  as rate1,

    IFNULL(a.amount,0) as paid_amt, invoices.created_at as crdate

    FROM invoices

    JOIN client_details ON client_details.id = invoices.client_id

    JOIN case_details ON case_details.id = invoices.case_id

    LEFT JOIN (select ROUND(sum(amount),2) as amount, invoice_id from money_allocations group by invoice_id ) a on a.invoice_id = invoices.id

    WHERE client_details.isactive =  1 AND client_details.user_id IN (${userIds}) AND client_details.id = '${req.params.clientId}' AND case_details.id = '${req.params.CaseId}' AND client_details.deleted_at IS null

    ORDER BY invoices.id desc, invoices.created_at DESC`;

    let invoice = await db.query(query, { type: sq.QueryTypes.SELECT });

    query = `select timesheet_invoices.client_id, timesheet_invoices.id, timesheet_invoices.invoice_number, client_details.firstname, client_details.surname, client_details.companyname,client_details.type, case_details.casenumber, case_details.prefix,

    't' as type,

    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 3 MONTH then ROUND(timesheet_invoices.total,1) else 0 end ),2)  as rate5,

    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 2 MONTH and timesheet_invoices.created_at >= NOW() - INTERVAL 3 MONTH 
    then ROUND(timesheet_invoices.total,1) else 0 end),2)  as rate4, 

    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 1 MONTH and timesheet_invoices.created_at >= NOW() - INTERVAL 2 MONTH 
    then ROUND(timesheet_invoices.total,1) else 0 end),2)  as rate3,

    TRUNCATE((case when timesheet_invoices.created_at <= NOW() - INTERVAL 1 DAY and timesheet_invoices.created_at >= NOW() - INTERVAL 1 MONTH 
    then ROUND(timesheet_invoices.total,1) else 0 end),2)  as rate2,

    TRUNCATE((case when timesheet_invoices.created_at >= NOW() - INTERVAL 1 DAY then ROUND(timesheet_invoices.total,1) else 0 end),2)  as rate1,

    IFNULL(a.amount,0) as paid_amt, timesheet_invoices.created_at as crdate

    FROM timesheet_invoices

    JOIN client_details ON client_details.id = timesheet_invoices.client_id

    JOIN case_details ON case_details.id = timesheet_invoices.case_id

    LEFT JOIN (select ROUND(sum(amount),2) as amount, invoice_id from money_allocations group by invoice_id ) a on a.invoice_id = timesheet_invoices.id

    WHERE client_details.isactive =  1 AND client_details.user_id IN (${userIds}) AND client_details.id = '${req.params.clientId}' AND case_details.id = '${req.params.CaseId}' AND client_details.deleted_at IS null

    ORDER BY timesheet_invoices.id desc, timesheet_invoices.created_at DESC`;

    let timesheet_invoice = await db.query(query, {
      type: sq.QueryTypes.SELECT
    });

    let result = [...invoice, ...timesheet_invoice].sort((a, b) =>
      a.crdate < b.crdate ? 1 : -1
    );

    return res.status(200).json(result);
  }
);

module.exports = router;
