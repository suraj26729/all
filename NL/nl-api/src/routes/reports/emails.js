const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const Email = require("../../models/Email");
const Common = require("../../helpers/common");
const moment = require("moment");

router.get("/user/:id", checkToken, async (req, res, next) => {
  try {
    let userIDS = await Common.firmUsers(req.params.id);
    let from_date = req.query.from_date || null;
    let to_date = req.query.to_date || null;
    let client_id = req.query.client_id || null;
    let matter_id = req.query.case_id || null;

    if (from_date != null) {
      from_date = moment(from_date, "YYYY-MM-DD")
        .startOf("day")
        .format("YYYY-MM-DD H:mm:ss");
    }
    if (to_date != null) {
      to_date = moment(to_date, "YYYY-MM-DD")
        .endOf("day")
        .format("YYYY-MM-DD H:mm:ss");
    }
    let params = [];
    if (from_date != null && to_date != null)
      params.push({ created_at: { $between: [from_date, to_date] } });
    if (from_date != null && to_date == null)
      params.push({ created_at: { $gte: from_date } });
    if (from_date == null && to_date != null)
      params.push({ created_at: { $lte: to_date } });

    if (client_id != null) params.push({ client_id: client_id });

    if (matter_id != null && matter_id != "all")
      params.push({ matter_id: matter_id });

    var conditions = params.reduce((a, b) => Object.assign(a, b), {});

    let emails = await Email.findAll({
      where: conditions,
      order: [["created_at", "DESC"]],
      include: [{ association: "client" }, { association: "matter" }]
    });

    return res.status(200).json(emails);
  } catch (err) {
    console.log(err);
    next(err);
  }
});

module.exports = router;
