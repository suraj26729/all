const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const Hearing = require("../../models/Hearing");
const Client = require("../../models/Client");
const Common = require("../../helpers/common");
let limit = 999;

router.get("/user/:userId", checkToken, async(req, res, next) => {
    try {
        let userIDS = await Common.firmUsers(req.params.userId);
        let clientIds = await Client.findAll({
            where: { user_id: userIDS, isactive: 1 },
            attributes: ["id"]
        });
        clientIds = clientIds.map(client => client.id);

        let offset = 0;
        let count = await Hearing.count({
            where: {
                client_id: clientIds
            }
        });
        limit = req.query.limit ? parseInt(req.query.limit) : limit;
        let page = req.query.page ? req.query.page : 1; // page number
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);
        let hearings = await Hearing.findAndCountAll({
            attributes: [
                "id",
                "notes",
                "from_date",
                "matter_id",
                "court_id",
                "client_id"
            ],
            where: {
                client_id: clientIds
            },
            order: [
                ["notes", "ASC"]
            ],
            limit: limit,
            offset: offset,
            include: [
                { association: "matter", attributes: ["prefix", "casenumber"] },
                { association: "court", attributes: ["name"] },
                {
                    association: "clientdetail",
                    //attributes: ["firstname", "surname", "mobile", "email"]
                },
                {
                    association: "attorneys",
                    include: [{
                        association: "attorney",
                        attributes: ["id"],
                        include: [{
                            association: "userdetail",
                            attributes: ["firstname", "lastname"]
                        }]
                    }]
                }
            ]
        });
        hearings["count"] = count;
        res.status(200).json({
            pages: pages,
            data: hearings
                //userIDS: userIDS
        });
    } catch (error) {
        next(error);
    }
});

router.get("/all/user/:userId", checkToken, async(req, res, next) => {
    let userIds = await Common.firmUsers(req.params.userId);
    let clientIds = await Client.findAll({
        where: { user_id: userIds, isactive: 1 },
        attributes: ["id"]
    });
    clientIds = clientIds.map(client => client.id);
    Hearing.findAll({
            attributes: [
                "id",
                "notes",
                "from_date",
                "matter_id",
                "court_id",
                "client_id"
            ],
            where: {
                client_id: clientIds
            },
            order: [
                ["notes", "ASC"]
            ],
            include: [
                { association: "matter", attributes: ["prefix", "casenumber"] },
                { association: "court", attributes: ["name"] },
                {
                    association: "clientdetail",
                    attributes: ["firstname", "surname", "mobile", "email"]
                },
                {
                    association: "attorneys",
                    include: [{
                        association: "attorney",
                        attributes: ["id"],
                        include: [{
                            association: "userdetail",
                            attributes: ["firstname", "lastname"]
                        }]
                    }]
                }
            ]
        })
        .then(hearings => {
            res.status(200).json(hearings);
        })
        .catch(err => next(err));
});

module.exports = router;