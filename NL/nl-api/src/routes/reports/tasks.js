const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const Task = require("../../models/dashboard/Task");
const Common = require("../../helpers/common");
let limit = 999;

router.get("/user/:userID", checkToken, async (req, res, next) => {
  try {
    let userIDS = await Common.firmUsers(req.params.userID);
    let offset = 0;
    let count = await Task.findAll({
      include: [{ association: "assignees", where: { user_id: userIDS } }]
    });
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);

    let tasks = await Task.findAndCountAll({
      include: [
        { association: "matter" },
        {
          association: "assignees",
          include: [
            {
              association: "assignee",
              include: [{ association: "userdetail" }]
            }
          ],
          where: { user_id: userIDS }
        }
      ],
      order: [["created_at", "DESC"]],
      limit: limit,
      offset: offset
    });
    tasks["count"] = count.length;
    res.status(200).json({
      pages: pages,
      data: tasks
      // userID: userIDS
    });
  } catch (error) {
    next(error);
  }
});

router.post(
  "/all/client/:client_id/case/:case_id",
  checkToken,
  (req, res, next) => {
    Task.count({
      where: { client_id: req.body.client_id, case_id: req.body.case_id }
    })
      .then(count => {
        let limit = req.query.limit ? parseInt(req.query.limit) : 10;
        let offset = 0;
        let page = req.query.page ? req.query.page : 1;
        let pages = Math.ceil(count / limit);
        offset = limit * (page - 1);

        Task.findAndCountAll({
          where: { client_id: req.body.client_id, case_id: req.body.case_id },
          include: [
            { association: "matter" },
            {
              association: "assignees",
              include: [
                {
                  association: "assignee",
                  include: [{ association: "userdetail" }],
                  where: { role_id: 3 }
                }
              ]
            }
          ],
          order: [["created_at", "DESC"]],
          limit: limit,
          offset: offset
        })
          .then(tasks => {
            tasks["count"] = count;
            res.status(200).json({
              data: tasks,
              page: pages,
              pageno: parseInt(page)
            });
          })
          .catch(err => next(err));
      })
      .catch(err => next(err));
  }
);

module.exports = router;
