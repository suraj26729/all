const express = require("express");
const sq = require("sequelize");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const Allocations = require("../../models/accounts/MoneyAllocation");
const CreditNote = require("../../models/accounts/CreditNote");
const Client = require("../../models/Client");
const CaseDetails = require("../../models/CaseDetails");
const Moment = require("moment");
const common = require("../../helpers/common");
const Transaction = require("../../models/accounts/Transaction");
const Invoice = require("../../models/accounts/Invoice");
const { TimesheetInvoice } = require("../../models/accounts/TimesheetInvoice");
const helper = require("../../helpers/pdf");
const {
  getCountries,
  getCurrencySymbol,
  getCurrencySymbolFromIso2
} = require("country-from-iso2");
const User = require("../../models/User");
const fs = require("fs");
const mail = require("../../config/mail");
const Email = require("../../models/Email");

router.get("/client/:id", checkToken, async (req, res, next) => {
  let from_date = "";
  let to_date = "";
  // if (req.query.bf == 1) {
  //   var returned_endate = Moment(req.query.from_date)
  //     .subtract(1, "years")
  //     .format("DD-MM-YYYY HH:mm");
  //   from_date = returned_endate || null;
  //   to_date = req.query.from_date || null;
  // } else {
  from_date = req.query.from_date || null;
  to_date = req.query.to_date || null;
  // }

  if (from_date != null)
    from_date = Moment(from_date, "YYYY-MM-DD")
      .startOf("day")
      .format("YYYY-MM-DD H:mm:ss");
  if (to_date != null)
    to_date = Moment(to_date, "YYYY-MM-DD")
      .endOf("day")
      .format("YYYY-MM-DD H:mm:ss");

  let case_id = req.query.case_number || null;
  let params = [{ client_id: req.params.id }];

  if (case_id != null && case_id != "all") {
    params.push({ case_id: case_id });
  } else {
    let caseIds = await CaseDetails.findAll({
      where: { client_id: req.params.id },
      attributes: ["id"]
    });
    case_id = caseIds.map(item => item.id);
    params.push({ case_id: case_id });
  }
  if (from_date != null && to_date != null)
    params.push({ created_at: { $between: [from_date, to_date] } });
  if (from_date != null && to_date == null)
    params.push({ created_at: { $gte: from_date } });

  if (from_date == null && to_date != null)
    params.push({ created_at: { $lte: to_date } });

  var conditions = params.reduce((a, b) => Object.assign(a, b), {});

  //console.log(conditions);

  var whereStatement = {};
  var paramsTransaction = [];
  if (req.query.from_date) {
    paramsTransaction.push({
      created_at: {
        $between: [from_date, to_date]
      }
    });
  }
  if (case_id != null && case_id != "all") {
    paramsTransaction.push({ case_id: case_id });
  }

  whereStatement = paramsTransaction.reduce((a, b) => Object.assign(a, b), {});

  //console.log(whereStatement);

  let _allocations = await Allocations.findAll({
    where: conditions
    //attributes: ["id", "created_at", "invoice_id", "description", "amount"]
  });
  let allocations = [];
  for (let item of _allocations) {
    let invoice = await Invoice.findByPk(item.invoice_id);
    if (invoice != null) {
      item.invoice_id = invoice.invoice_number;
      allocations.push(item);
    }
  }

  let transaction = await Transaction.findAll({
    attributes: ["id", "type", "status", "created_at"],
    include: [
      {
        association: "matter"
        // attributes: ["id", "prefix", "casenumber", "status"]
      },
      {
        association: "trust"
        // attributes: ["id", "amount", "description"]
      },
      {
        association: "allocation"
        //attributes: ["id", "amount", "description"]
      }
    ],
    where: whereStatement,
    order: [["created_at", "ASC"]]
  });

  let creditnotes = await CreditNote.findAll({
    where: whereStatement,
    attributes: [
      "id",
      "created_at",
      "invoice_id",
      "message",
      "total",
      "case_id"
    ]
  });

  let client = await Client.findByPk(req.params.id, {
    attributes: ["firstname", "surname", "companyname", "type"]
  });

  // var caseid = [];
  // for (crd of creditnotes) {
  //     caseid.push(crd.case_id);
  // }

  let caseDetails = await CaseDetails.findAll({
    where: { id: case_id },
    attributes: ["id", "prefix", "casenumber"]
  });

  let invoice = await Invoice.findAll({
    where: conditions
  });

  let timesheet_invoice = await TimesheetInvoice.findAll({
    where: conditions
  });

  res.status(200).json({
    client,
    allocations,
    creditnotes,
    caseDetails,
    conditions,
    invoice,
    timesheet_invoice,
    transaction
  });
});

router.get("/currency/user/:id", async (req, res, next) => {
  try {
    var user = [];
    let firmusers = await common.firmUsers(req.params.id);

    for (let fu of firmusers) {
      let u = await User.findOne({
        where: { id: fu, parent_user_id: null },
        include: [{ association: "userdetail" }]
      });
      if (u !== null) {
        user.push(u);
      }
    }
    var currency = user[0].userdetail.currency;

    var cs = await getCurrencySymbol(currency);

    var currencySymbol = "";
    if (cs) {
      currencySymbol = cs;
    } else {
      currencySymbol = "R";
    }
    res.status(200).json(currencySymbol);
  } catch (error) {
    console.log(error);
  }
});

router.post(
  "/client/:id/send-email/user/:userId",
  checkToken,
  async (req, res, next) => {
    let from_date = "";
    let to_date = "";
    if (req.query.bf == 1) {
      var returned_endate = Moment(req.query.from_date)
        .subtract(1, "years")
        .format("DD-MM-YYYY HH:mm");
      // console.log(returned_endate);
      from_date = returned_endate || null;
      to_date = req.query.from_date || null;
    } else {
      from_date = req.query.from_date || null;
      to_date = req.query.to_date || null;
    }

    if (from_date != null)
      from_date = Moment(from_date, "YYYY-MM-DD")
        .startOf("day")
        .format("YYYY-MM-DD HH:mm:ss");

    if (to_date != null)
      to_date = Moment(to_date, "YYYY-MM-DD")
        .endOf("day")
        .format("YYYY-MM-DD HH:mm:ss");

    let case_id = req.query.case_number || null;
    let params = [{ client_id: req.params.id }];

    if (case_id != null && case_id != "all") params.push({ case_id: case_id });
    else {
      let caseIds = await CaseDetails.findAll({
        where: { client_id: req.params.id },
        attributes: ["id"]
      });
      case_id = caseIds.map(item => item.id);
      params.push({ case_id: case_id });
    }
    if (from_date != null && to_date != null)
      params.push({ created_at: { $between: [from_date, to_date] } });
    if (from_date != null && to_date == null)
      params.push({ created_at: { $gte: from_date } });

    if (from_date == null && to_date != null)
      params.push({ created_at: { $lte: to_date } });

    var conditions = params.reduce((a, b) => Object.assign(a, b), {});

    var whereStatement = {};
    var paramsTransaction = [];
    if (from_date != null && to_date != null) {
      paramsTransaction.push({
        created_at: {
          $between: [from_date, to_date]
        }
      });
    }
    if (case_id != null && case_id != "all") {
      paramsTransaction.push({ case_id: case_id });
    }

    whereStatement = paramsTransaction.reduce(
      (a, b) => Object.assign(a, b),
      {}
    );

    let allocations = await Allocations.findAll({
      where: conditions
      //attributes: ["id", "created_at", "invoice_id", "description", "amount"]
    });

    let transaction = await Transaction.findAll({
      attributes: ["id", "type", "status", "created_at"],
      include: [
        {
          association: "matter"
          // attributes: ["id", "prefix", "casenumber", "status"]
        },
        {
          association: "trust"
          // attributes: ["id", "amount", "description"]
        },
        {
          association: "allocation"
          //attributes: ["id", "amount", "description"]
        }
      ],
      where: whereStatement,
      order: [["created_at", "ASC"]]
    });

    let creditnotes = await CreditNote.findAll({
      where: whereStatement,
      attributes: [
        "id",
        "created_at",
        "invoice_id",
        "message",
        "total",
        "case_id"
      ]
    });

    let client = await Client.findByPk(req.params.id);

    // var caseid = [];
    // for (crd of creditnotes) {
    //     caseid.push(crd.case_id);
    // }

    let caseDetails = await CaseDetails.findAll({
      where: { id: case_id },
      attributes: ["id", "prefix", "casenumber"]
    });

    let invoice = await Invoice.findAll({
      where: conditions
    });

    let timesheet_invoice = await TimesheetInvoice.findAll({
      where: conditions
    });

    let result = [];
    let total = 0.0;

    if (allocations.length) {
      for (let alloc of allocations) {
        total = parseFloat(total) - parseFloat(alloc.amount);
        if (caseDetails.length) {
          for (let cn of caseDetails) {
            if (cn.id == alloc.case_id) {
              result.push({
                casenumber: cn.prefix + " " + cn.casenumber,
                date: alloc.created_at,
                invoice_id: "INV#" + alloc.invoice_id,
                description: "Payment",
                debit: 0.0,
                credit: alloc.amount,
                balance: Math.abs(total)
              });
            }
          }
        }
      }
    }

    if (invoice.length) {
      for (let inv of invoice) {
        total = parseFloat(total) + parseFloat(inv.total);
        if (caseDetails.length) {
          for (let cn of caseDetails) {
            if (cn.id == inv.case_id) {
              result.push({
                casenumber: cn.prefix + " " + cn.casenumber,
                date: inv.created_at,
                invoice_id: "INV#" + inv.invoice_number,
                description: "Charges INV#" + inv.invoice_number,
                debit: inv.total,
                credit: 0.0,
                balance: Math.abs(total)
              });
            }
          }
        }
      }
    }

    if (timesheet_invoice.length) {
      for (let inv of timesheet_invoice) {
        total = parseFloat(total) + parseFloat(inv.total);
        if (caseDetails.length) {
          for (let cn of caseDetails) {
            if (cn.id == inv.case_id) {
              result.push({
                casenumber: cn.prefix + " " + cn.casenumber,
                date: inv.created_at,
                invoice_id: "INV#" + inv.invoice_number,
                description: "Charges INV#" + inv.invoice_number,
                debit: inv.total,
                credit: 0.0,
                balance: Math.abs(total)
              });
            }
          }
        }
      }
    }

    if (creditnotes.length) {
      for (let cr of creditnotes) {
        total = parseFloat(total) - parseFloat(cr.total);
        if (caseDetails.length) {
          for (let cn of caseDetails) {
            if (cn.id == cr.case_id) {
              result.push({
                casenumber: cn.prefix + " " + cn.casenumber,
                date: cr.created_at,
                invoice_id: "CR#" + cr.invoice_id,
                description: "CR#" + cr.invoice_id,
                debit: 0.0,
                credit: cr.total,
                balance: Math.abs(total)
              });
            }
          }
        }
      }
    }

    let currentBalance = Math.abs(total);

    let content = fs.readFileSync(
      "./src/email-templates/crm/statement.html",
      "utf8"
    );
    content = content.replace(
      "**date**",
      Moment(new Date()).format("DD-MM-YYYY")
    );
    if (client.type == "c") {
      content = content.replace("**customer_name**", client.company_name);
    } else {
      content = content.replace(
        "**customer_name**",
        client.firstname + " " + client.surname
      );
    }

    let user = await User.findByPk(req.params.userId, {
      include: [{ association: "userdetail" }]
    });
    let userIds = await common.firmUsers(req.params.userId);
    let admin = await User.findOne({
      where: { id: userIds, role_id: 2, parent_user_id: null },
      include: [{ association: "userdetail" }]
    });
    if (admin != null && admin.userdetail.logo) {
      content = content.replace(
        "**logo**",
        "https://www.naartjielegal.com/api/" + admin.userdetail.logo
      );
    } else {
      content = content.replace(
        "**logo**",
        "https://www.naartjielegal.com/assets/images/logo.png"
      );
    }
    content = content.replace("**address**", admin.userdetail.address);
    content = content.replace("**email**", admin.email);
    content = content.replace(
      "**firm_name**",
      (
        admin.userdetail.firstname +
        " " +
        admin.userdetail.lastname
      ).toUpperCase()
    );

    var cs = await getCurrencySymbol(admin.userdetail.currency);

    var currencySymbol = "";
    if (cs) {
      currencySymbol = cs;
    } else {
      currencySymbol = "R";
    }

    let url = await helper.generateTransactionsPDF(
      result,
      currentBalance,
      currencySymbol
    );

    let email = await Email.create({
      client_id: client.id,
      matter_id: Array.isArray(case_id) || case_id == "all" ? null : case_id,
      subject: "Statement",
      type: "statement",
      user_id: req.params.userId
    });

    let sendmail = "";
    sendmail = await mail.sendWithAttachment(
      // "satheesh123raj@gmail.com",
      client.email,
      "Statement",
      content,
      url,
      next
    );
    // if (client.type == "c") {
    //   sendmail = await mail.sendWithAttachment(
    //     //"satheesh123raj@gmail.com",
    //     client.email,
    //     "Statement",
    //     content,
    //     url,
    //     next
    //   );
    // } else {
    //   sendmail = await mail.sendWithAttachment(
    //     //"satheesh123raj@gmail.com",
    //     client.email,
    //     "Statement",
    //     content,
    //     url,
    //     next
    //   );
    // }

    return res.status(200).json({ sendmail, url });
  }
);

module.exports = router;
