const express = require("express");
const sq = require("sequelize");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const InvoiceItem = require("../../models/accounts/InvoiceItems");
const CreditNoteItem = require("../../models/accounts/CreditNoteItems");
const moment = require("moment");
const Common = require("../../helpers/common");
let limit = 10;

router.get("/user/:user_id", checkToken, async (req, res, next) => {
  try {
    let userIDS = await Common.firmUsers(req.params.user_id);

    let from_date = req.query.from_date || null;
    let to_date = req.query.to_date || null;
    let cost_id = req.query.cost_id || "";

    if (to_date != null) {
      let _to_date = new Date(to_date);
      _to_date.setDate(_to_date.getDate() + 1);
      to_date = moment(_to_date).format("YYYY-MM-DD");
    }

    let params = [{ user_id: userIDS }];

    if (from_date != null && to_date != null)
      params.push({ created_at: { $between: [from_date, to_date] } });
    if (from_date != null && to_date == null)
      params.push({ created_at: { $gte: from_date } });
    if (from_date == null && to_date != null)
      params.push({ created_at: { $lte: to_date } });
    if (cost_id != "") params.push({ cost_id });

    var conditions = params.reduce((a, b) => Object.assign(a, b), {});

    let summaryCount = await InvoiceItem.count({ where: conditions });
    let c_summaryCount = await CreditNoteItem.count({ where: conditions });

    let summary = [];
    let c_summary = [];
    let details = [];
    let c_details = [];

    if (summaryCount) {
      summary = await InvoiceItem.findAll({
        attributes: [
          "cost_id",
          //[sq.fn("sum", sq.col("price")), "price"],
          "price",
          [sq.fn("sum", sq.col("quantity")), "quantity"],
          [sq.literal("price*quantity"), "total"],
          [sq.literal("price*quantity*(tax.percent/100)"), "taxprice"],
          [
            sq.literal("(price*quantity)+(price*quantity*(tax.percent/100))"),
            "netTotal"
          ]
        ],
        where: conditions,
        include: [
          { association: "costtype", attributes: ["id", "name"] },
          { association: "tax", attributes: ["id", "name", "percent"] }
        ],
        group: ["cost_id"]
      });
    }
    if (c_summaryCount) {
      c_summary = await CreditNoteItem.findAll({
        attributes: [
          "cost_id",
          //[sq.fn("sum", sq.col("price")), "price"],
          "price",
          [sq.fn("sum", sq.col("quantity")), "quantity"],
          [sq.literal("price*quantity"), "total"],
          [sq.literal("price*quantity*(tax.percent/100)"), "taxprice"],
          [
            sq.literal("(price*quantity)+(price*quantity*(tax.percent/100))"),
            "netTotal"
          ]
        ],
        where: conditions,
        include: [
          { association: "costtype", attributes: ["id", "name"] },
          { association: "tax", attributes: ["id", "name", "percent"] }
        ],
        group: ["cost_id"]
      });
    }

    if (summaryCount) {
      details = await InvoiceItem.findAndCountAll({
        where: conditions,
        attributes: [
          "id",
          "invoice_id",
          "quantity",
          "price",
          "tax_id",
          [sq.literal("price*quantity*(tax.percent/100)"), "taxprice"],
          "discount",
          "total",
          "created_at"
        ],
        include: [
          {
            association: "invoice",
            attributes: ["id", "invoice_number", "payment_date"],
            include: [
              {
                association: "client",
                attributes: ["id", "companyname", "firstname", "surname"]
              },
              {
                association: "matter",
                attributes: ["id", "prefix", "casenumber"]
              },
              {
                association: "thirdparty",
                attributes: ["id", "firstname", "surname"]
              }
            ]
          },
          { association: "tax", attributes: ["id", "name", "percent"] }
        ],
        order: [["created_at", "ASC"]]
      });
    }
    if (c_summaryCount) {
      c_details = await CreditNoteItem.findAndCountAll({
        where: conditions,
        attributes: [
          "id",
          "credit_note_id",
          "quantity",
          "price",
          "tax_id",
          [sq.literal("price*quantity*(tax.percent/100)"), "taxprice"],
          "discount",
          "total",
          "created_at"
        ],
        include: [
          {
            association: "credit_note",
            attributes: ["id", "credit_note_number", "payment_date"],
            include: [
              {
                association: "client",
                attributes: ["id", "companyname", "firstname", "surname"]
              },
              {
                association: "matter",
                attributes: ["id", "prefix", "casenumber"]
              },
              {
                association: "thirdparty",
                attributes: ["id", "firstname", "surname"]
              }
            ]
          },
          { association: "tax", attributes: ["id", "name", "percent"] }
        ],
        order: [["created_at", "ASC"]]
      });
    }

    if (summary.length) {
      summary = summary.map(item => {
        return {
          cost_id: item.cost_id,
          price: item.price,
          quantity: item.quantity,
          total: item.price * item.quantity,
          taxprice: item.price * item.quantity * (item.tax.percent/100),
          netTotal:(item.price*item.quantity)+(item.price*item.quantity*(item.tax.percent/100)),
          costtype: item.costtype,
          tax: item.tax
        };
      });
    }

    if (c_summary.length) {
      c_summary = c_summary.map(item => {
        return {
          cost_id: item.cost_id,
          price: item.price,
          quantity: item.quantity,
          total: item.price * item.quantity,
          taxprice: item.price * item.quantity * (item.tax.percent/100),
          netTotal:(item.price*item.quantity)+(item.price*item.quantity*(item.tax.percent/100)),
          costtype: item.costtype,
          tax: item.tax
        };
      });
    }

    res.status(200).json({ summary, c_summary, details, c_details, userIDS });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
