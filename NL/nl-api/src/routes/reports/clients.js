const express = require("express");
const router = express.Router();
const checkToken = require("../../middlewares/checkToken");
const Client = require("../../models/Client");
const Matter = require("../../models/CaseDetails");
let limit = 999;
const Common = require("../../helpers/common");
router.get("/user/:userId", checkToken, async (req, res, next) => {
  try {
    let userIDS = await Common.firmUsers(req.params.userId);
    let offset = 0;
    let count = await Client.count({
      where: {
        user_id: userIDS,
        isactive: 1
      }
    });
    limit = req.query.limit ? parseInt(req.query.limit) : limit;
    let page = req.query.page ? req.query.page : 1; // page number
    let pages = Math.ceil(count / limit);
    offset = limit * (page - 1);
    let clients = await Client.findAndCountAll({
      attributes: [
        "id",
        "companyname",
        "firstname",
        "surname",
        "email",
        "address1",
        "address2",
        "mobile",
        "type"
      ],
      where: {
        user_id: userIDS,
        isactive: 1
      },
      include: [
        {
          association: "matters",
          attributes: ["prefix", "casenumber"],
          include: [
            {
              association: "principleAttorney",
              attributes: ["id"],
              include: [
                {
                  association: "userdetail",
                  attributes: ["firstname", "lastname"]
                }
              ]
            },
            { association: "category", attributes: ["name"] }
          ]
        }
      ],
      order: [["firstname", "ASC"]],
      limit: limit,
      offset: offset
    });
    clients["count"] = count;

    let results = [];
    for (let item of clients.rows) {
      results.push({
        id: item.id,
        companyname: item.companyname,
        firstname: item.type == "c" ? item.companyname : item.firstname,
        surname: item.type == "c" ? "" : item.surname,
        email: item.email,
        address1: item.address1,
        address2: item.address2,
        mobile: item.mobile,
        matters: item.matters
      });
    }

    results.sort((a, b) =>
      a.firstname > b.firstname ? 1 : b.firstname > a.firstname ? -1 : 0
    );

    clients["rows"] = results;
    res.status(200).json({
      pages: pages,
      data: clients
      //userIDS: userIDS
    });
  } catch (error) {
    next(error);
  }
});

router.get("/all/user/:userId", checkToken, async (req, res, next) => {
  let userIds = await Common.firmUsers(req.params.userId);
  let clients = await Client.findAll({
    attributes: [
      "id",
      "companyname",
      "firstname",
      "surname",
      "email",
      "address1",
      "address2",
      "mobile",
      "type"
    ],
    include: [
      {
        association: "matters",
        attributes: ["prefix", "casenumber"],
        include: [
          {
            association: "principleAttorney",
            attributes: ["id"],
            include: [
              {
                association: "userdetail",
                attributes: ["firstname", "lastname"]
              }
            ]
          },
          { association: "category", attributes: ["name"] }
        ]
      }
    ],
    where: {
      user_id: userIds,
      isactive: 1
    },
    order: [["firstname", "DESC"]]
  });
  let results = [];
  for (let item of clients) {
    results.push({
      id: item.id,
      name:
        item.type == "c"
          ? item.companyname
          : item.firstname + " " + item.surname,
      companyname: item.companyname,
      firstname: item.type == "c" ? item.companyname : item.firstname,
      surname: item.type == "c" ? "" : item.surname,
      email: item.email,
      address1: item.address1,
      address2: item.address2,
      mobile: item.mobile,
      type: item.type
    });
  }
  results = results.sort((a, b) => a.name.localeCompare(b.name));
  res.status(200).json(results);
});

module.exports = router;
