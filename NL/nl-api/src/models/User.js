const Sequelize = require("sequelize");
const db = require("../config/database");
const UserDetail = require("../models/UserDetail");
const Role = require("../models/Role");
const Country = require("../models/masters/Country");

const User = db.define(
  "users",
  {
    parent_user_id: { type: Sequelize.INTEGER, allowNull: true },
    username: Sequelize.STRING,
    password: Sequelize.STRING,
    email: Sequelize.STRING,
    isadmin: Sequelize.INTEGER,
    isattorney: Sequelize.INTEGER,
    archive: Sequelize.INTEGER,
    role_id: { type: Sequelize.INTEGER, allowNull: true },
    isactive: { type: Sequelize.INTEGER, defaultValue: 0 },
    last_login: { type: Sequelize.DATE, allowNull: true },
    expiry_date: Sequelize.DATE,
    remember_token: { type: Sequelize.TEXT, allowNull: true },
    archive_date: { type: Sequelize.DATE, allowNull: true }
  },
  {
    paranoid: true,
    underscored: true
  }
);

User.hasOne(UserDetail, { as: "userdetail", foreignKey: "user_id" });
User.belongsTo(Role, { as: "role", foreignKey: "role_id" });

User.sync();

module.exports = User;
