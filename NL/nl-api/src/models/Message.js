const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("../models/User");

const Message = db.define(
  "attorney_messages",
  {
    thread_id: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1 },
    is_first: { type: Sequelize.STRING, allowNull: true },
    attorney_id: Sequelize.INTEGER,
    content: Sequelize.TEXT,
    //end_date: Sequelize.DATE,
    user_id: Sequelize.INTEGER,
    is_read: { type: Sequelize.INTEGER, defaultValue: 0 }
  },
  {
    underscored: true
  }
);

Message.belongsTo(User, { as: "attorney", foreignKey: "attorney_id" });
Message.belongsTo(User, { as: "user", foreignKey: "user_id" });

paginate.paginate(Message);

Message.sync();

module.exports = Message;
