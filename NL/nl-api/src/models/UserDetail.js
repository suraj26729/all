const Sequelize = require("sequelize");
const db = require("../config/database");
const User = require("../models/User");
const Country = require("../models/masters/Country");
const ISOCountry = require("./ISOCountry");

const UserDetail = db.define(
  "user_details",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    user_id: {
      type: Sequelize.INTEGER,
    },
    firstname: { type: Sequelize.STRING, allowNull: true },
    lastname: { type: Sequelize.STRING, allowNull: true },
    contact1: { type: Sequelize.STRING, allowNull: true },
    contact2: { type: Sequelize.STRING, allowNull: true },
    address: { type: Sequelize.TEXT, allowNull: true },
    country_code: { type: Sequelize.STRING, allowNull: true },
    currency: { type: Sequelize.STRING, allowNull: true },
    yearofpractice: { type: Sequelize.STRING({ length: 4 }), allowNull: true },
    picturepath: { type: Sequelize.TEXT, allowNull: true },
    logo: { type: Sequelize.TEXT, allowNull: true },
    signature: { type: Sequelize.TEXT, allowNull: true },
    name_of_firm: { type: Sequelize.TEXT, allowNull: true },
  },
  { underscored: true }
);
// UserDetail.hasOne(Country, { as: "country", foreignKey: "country_code" });

UserDetail.belongsTo(ISOCountry, {
  as: "country",
  foreignKey: "country_code",
  targetKey: "iso",
});

UserDetail.sync();

module.exports = UserDetail;
