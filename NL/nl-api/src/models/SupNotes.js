const Sequelize = require("sequelize");
const db = require("../config/database");

const SupplierNotes = db.define(
  "supplier_notes",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    content: Sequelize.TEXT,
    type: { type: Sequelize.STRING, allowNull: true },
    parent_id: { type: Sequelize.UUID, allowNull: true },
  },
  {
    underscored: true,
  }
);

SupplierNotes.sync();

module.exports = SupplierNotes;
