const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("../models/User");
const ClientContact = require("../models/ClientContact");
const Matter = require("../models/CaseDetails");

const Client = db.define(
  "client_details",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    companyname: { type: Sequelize.STRING, allowNull: true },
    companyreg: { type: Sequelize.STRING, allowNull: true },
    companyvat: { type: Sequelize.STRING, allowNull: true },
    firstname: Sequelize.STRING,
    surname: { type: Sequelize.STRING, allowNull: true },
    email: { type: Sequelize.STRING, allowNull: true },
    creditlimits: { type: Sequelize.STRING, allowNull: true },
    contact1: { type: Sequelize.STRING, allowNull: true },
    contact2: { type: Sequelize.STRING, allowNull: true },
    mobile: { type: Sequelize.STRING, allowNull: true },
    dob: { type: Sequelize.STRING, allowNull: true },
    bankdetails: { type: Sequelize.STRING, allowNull: true },
    personaldetails: { type: Sequelize.TEXT, allowNull: true },
    address1: { type: Sequelize.TEXT, allowNull: true },
    address2: { type: Sequelize.TEXT, allowNull: true },
    city: { type: Sequelize.STRING, allowNull: true },
    zipcode: { type: Sequelize.STRING, allowNull: true },
    country: { type: Sequelize.INTEGER, allowNull: true },
    province: { type: Sequelize.STRING, allowNull: true },
    picturepath: { type: Sequelize.STRING, allowNull: true },
    type: { type: Sequelize.CHAR, defaultValue: "C" },
    isactive: { type: Sequelize.INTEGER, defaultValue: 0 },
    user_id: { type: Sequelize.INTEGER, allowNull: true }
  },
  {
    paranoid: true,
    underscored: true
  }
);

Client.hasMany(ClientContact, { as: "clientcontact", foreignKey: "client_id" });

Client.hasMany(Matter, { as: "matters", foreignKey: "client_id" });

Matter.belongsTo(Client, {
  as: "clientdetail",
  foreignKey: "client_id"
});

Client.sync();


module.exports = Client;
