const Sequelize = require("sequelize");
const db = require("../config/database");
const User = require("../models/User");

const UserDetail = db.define(
  "user_details",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    user_id: {
      type: Sequelize.INTEGER
    },
    firstname: Sequelize.STRING,
    lastname: { type: Sequelize.STRING, allowNull: true },
    contact1: Sequelize.STRING,
    contact2: { type: Sequelize.STRING, allowNull: true },
    address: Sequelize.TEXT,
    country_code: Sequelize.STRING,
    yearofpractice: Sequelize.INTEGER({ length: 4 }),
    picturepath: { type: Sequelize.TEXT, allowNull: true },
    logo: { type: Sequelize.TEXT, allowNull: true },
    signature: { type: Sequelize.TEXT, allowNull: true }
  },
  { underscored: true }
);

db.sync();

module.exports = UserDetail;
