const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("../models/User");
const ClientContact = require("../models/ClientContact");
const Matter = require("../models/CaseDetails");
const slugify = require("sequelize-slugify");
const Blog = require("../models/Blog");
const Catagory = db.define(
    "catagory", {
        slug: {
            type: Sequelize.STRING,
            unique: true
        },
        name: Sequelize.STRING,

        status: { type: Sequelize.STRING, defaultValue: 'publish' },
    }, {
        paranoid: true,
        underscored: true
    }
);
slugify.slugifyModel(Catagory, {
    source: ['name'],
    slugOptions: { lower: true },
    overwrite: false,
    column: 'slug'
});

Catagory.sync();

module.exports = Catagory;