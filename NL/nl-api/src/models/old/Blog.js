const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("../models/User");
const ClientContact = require("../models/ClientContact");
const Matter = require("../models/CaseDetails");
const slugify = require("sequelize-slugify");
const Catagory = require("../models/Catagory");
const Blog = db.define(
    "blog", {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV1,
            primaryKey: true
        },
        slug: {
            type: Sequelize.STRING,
            unique: true
        },
        title: Sequelize.STRING,
        catagory_id: { type: Sequelize.STRING, allowNull: true },
        featured_image: { type: Sequelize.STRING, allowNull: true },
        content: Sequelize.TEXT,
        status: { type: Sequelize.STRING, defaultValue: 'publish' },
    }, {
        paranoid: true,
        underscored: true
    }
);

slugify.slugifyModel(Blog, {
    source: ['title'],
    slugOptions: { lower: true },
    overwrite: false,
    column: 'slug'
});

//Blog.belongsTo(Catagory, { as: "blog_catagory", foreignKey: "catagory_id" });

Catagory.hasMany(Blog, { as: "blog_catagory", foreignKey: "catagory_id" });

Blog.sync();

module.exports = Blog;