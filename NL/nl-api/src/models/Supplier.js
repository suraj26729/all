const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("../models/User");
const SupplierContact = require("../models/SupplierContacts");
const Matter = require("../models/CaseDetails");

const Supplier = db.define(
  "supplier_details",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    companyname: { type: Sequelize.STRING, allowNull: true },
    companyreg: { type: Sequelize.STRING, allowNull: true },
    companyvat: { type: Sequelize.STRING, allowNull: true },
    firstname: Sequelize.STRING,
    surname: { type: Sequelize.STRING, allowNull: true },
    email: { type: Sequelize.STRING, allowNull: true },
    creditlimits: { type: Sequelize.STRING, allowNull: true },
    contact1: { type: Sequelize.STRING, allowNull: true },
    contact2: { type: Sequelize.STRING, allowNull: true },
    mobile: { type: Sequelize.STRING, allowNull: true },
    dob: { type: Sequelize.STRING, allowNull: true },
    bankdetails: { type: Sequelize.STRING, allowNull: true },
    personaldetails: { type: Sequelize.TEXT, allowNull: true },
    address1: { type: Sequelize.TEXT, allowNull: true },
    address2: { type: Sequelize.TEXT, allowNull: true },
    city: { type: Sequelize.STRING, allowNull: true },
    zipcode: { type: Sequelize.STRING, allowNull: true },
    country: { type: Sequelize.INTEGER, allowNull: true },
    province: { type: Sequelize.STRING, allowNull: true },
    picturepath: { type: Sequelize.STRING, allowNull: true },
    type: { type: Sequelize.CHAR, defaultValue: "c" },
    isactive: { type: Sequelize.INTEGER, defaultValue: 0 },
    user_id: { type: Sequelize.INTEGER, allowNull: true },
  },
  {
    paranoid: true,
    underscored: true,
  }
);

Supplier.hasMany(SupplierContact, {
  as: "suppliercontact",
  foreignKey: "supplier_id",
});

Supplier.sync();

paginate.paginate(Supplier);
module.exports = Supplier;
