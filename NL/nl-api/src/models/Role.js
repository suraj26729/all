const Sequelize = require("sequelize");
const db = require("../config/database");
const User = require('../models/User');

const Role = db.define(
    "roles",
    {
        name: Sequelize.STRING,
        isactive: { type: Sequelize.INTEGER, defaultValue: 1 }
    },
    {
        underscored: true
    }
)

db.sync();

module.exports = Role;