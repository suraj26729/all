const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");

const Category = db.define(
  "supplier_categories",
  {
    name: Sequelize.STRING,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_Id: Sequelize.INTEGER
   
  },
  {
    underscored: true
  }
);



Category.sync();

module.exports = Category;