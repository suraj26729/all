const Sequelize = require("sequelize");
const db = require("../config/database");
const Client = require("../models/Client");
const Matter = require("../models/CaseDetails");
const ClientContact = require("../models/ClientContact");
const HearingAttorney = require("./HearingAttorneys");
const Court = require("./masters/Court");
const User = require("./User");

const Hearing = db.define(
  "hearings",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    client_id: {
      type: Sequelize.UUID
    },
    matter_id: {
      type: Sequelize.UUID
    },
    thirdparty_id: {
      type: Sequelize.UUID
    },
    contact_id: {
      type: Sequelize.UUID
    },
    court_id: Sequelize.INTEGER,
    all_day_event: Sequelize.INTEGER,
    notes: Sequelize.TEXT,
    from_date: Sequelize.DATE,
    from_time: { type: Sequelize.STRING, allowNull: true },
    to_date: Sequelize.DATE,
    to_time: { type: Sequelize.STRING, allowNull: true },
    status: { type: Sequelize.STRING, defaultValue: "Open" },
    user_id: { type: Sequelize.INTEGER, allowNull: true }
  },
  {
    paranoid: true,
    underscored: true
  }
);

Hearing.belongsTo(Client, {
  as: "clientdetail",
  foreignKey: "client_id"
});

Hearing.belongsTo(Matter, {
  as: "matter",
  foreignKey: "matter_id"
});

Hearing.belongsTo(ClientContact, {
  as: "thirdparty",
  foreignKey: "thirdparty_id"
});

Hearing.belongsTo(ClientContact, {
  as: "contact",
  foreignKey: "contact_id"
});

Hearing.belongsTo(Court, {
  as: "court",
  foreignKey: "court_id"
});

Hearing.belongsTo(User, {
  as: "user",
  foreignKey: "user_id"
});

Hearing.hasMany(HearingAttorney, { as: "attorneys", foreignKey: "hearing_id" });

db.sync();

module.exports = Hearing;
