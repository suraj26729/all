const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");

const Category = db.define(
  "master_categories",
  {
    name: Sequelize.STRING,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

paginate.paginate(Category);

db.sync();

module.exports = Category;
