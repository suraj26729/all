const Sequelize = require("sequelize");
const db = require("../../config/database");

const Bank = db.define(
    "master_banks",
    {
        content: Sequelize.STRING,
        user_id: Sequelize.INTEGER
    },
    {
        underscored: true
    }
)

db.sync();

module.exports = Bank;