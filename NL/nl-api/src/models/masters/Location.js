const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");

const MLocation = db.define(
  "master_locations",
  {
    name: Sequelize.STRING,
    accr: Sequelize.STRING,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

paginate.paginate(MLocation);

db.sync();

module.exports = MLocation;
