const Sequelize = require("sequelize");
const db = require("../../config/database");
const Province = require('../masters/Province');

const City = db.define(
    "master_cities",
    {
        name: Sequelize.STRING,
        province_id: Sequelize.INTEGER,
        isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
        user_id: Sequelize.INTEGER
    },
    {
        underscored: true
    }
)

City.belongsTo(Province);

db.sync();

module.exports = City;