const Sequelize = require("sequelize");
const db = require("../../config/database");

const Template = db.define(
  "master_templates",
  {
    name: Sequelize.STRING,
    content: Sequelize.TEXT
  },
  { underscored: true, timestamps: false }
);

Template.sync();

module.exports = Template;
