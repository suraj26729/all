const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");

const Corporate = db.define(
  "master_corporates",
  {
    name: Sequelize.STRING,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

paginate.paginate(Corporate);

db.sync();

module.exports = Corporate;
