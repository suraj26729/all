const Sequelize = require("sequelize");
const db = require("../../config/database");
const Province = require("../masters/Province");

const Country = db.define(
  "master_countries",
  {
    name: Sequelize.STRING,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 }
  },
  {
    underscored: true
  }
);

Country.hasMany(Province, { as: "provinces", foreignKey: "country_id" });

db.sync();

module.exports = Country;
