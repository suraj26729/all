const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");

const Holiday = db.define(
  "master_holidays",
  {
    content: Sequelize.TEXT,
    start_date: Sequelize.DATE,
    end_date: Sequelize.DATE,
    user_id: Sequelize.INTEGER,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 }
  },
  {
    underscored: true
  }
);

paginate.paginate(Holiday);

db.sync();

module.exports = Holiday;
