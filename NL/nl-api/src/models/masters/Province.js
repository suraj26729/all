const Sequelize = require("sequelize");
const db = require("../../config/database");
const Country = require("../masters/Country");

const Province = db.define(
  "master_provinces",
  {
    name: Sequelize.STRING,
    country_id: Sequelize.INTEGER,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 }
  },
  {
    underscored: true
  }
);

db.sync();

module.exports = Province;
