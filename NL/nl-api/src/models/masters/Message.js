const Sequelize = require("sequelize");
const db = require("../config/database");

const Message = db.define(
    "master_messages",
    {
        message: Sequelize.STRING
    },
    {
        underscored: true
    }
)

db.sync();

module.exports = Message;