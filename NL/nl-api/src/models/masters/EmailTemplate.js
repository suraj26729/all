const Sequelize = require("sequelize");
const db = require("../config/database");

const EmailTemplate = db.define(
    "master_email_templates",
    {
        name: Sequelize.STRING,
        subject: Sequelize.STRING,
        from_address: Sequelize.STRING,
        title: Sequelize.STRING,
        content: Sequelize.TEXT,
        type: Sequelize.INTEGER,
        isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
        user_id: Sequelize.INTEGER
    },
    {
        underscored: true
    }
)

db.sync();

module.exports = EmailTemplate;