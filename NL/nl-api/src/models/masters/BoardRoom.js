const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");

const BoardRoom = db.define(
  "master_board_rooms",
  {
    name: Sequelize.STRING,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

paginate.paginate(BoardRoom);

BoardRoom.sync();

module.exports = BoardRoom;
