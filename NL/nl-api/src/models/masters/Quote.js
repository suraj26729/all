const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");

const Quote = db.define(
  "master_quotes",
  {
    title: Sequelize.STRING,
    desc: Sequelize.TEXT,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

paginate.paginate(Quote);

db.sync();

module.exports = Quote;
