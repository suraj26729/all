const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");

const Court = db.define(
  "master_courts",
  {
    name: Sequelize.STRING,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

paginate.paginate(Court);

db.sync();

module.exports = Court;
