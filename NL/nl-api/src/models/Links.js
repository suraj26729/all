const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("./User");

const Link = db.define(
  "links",
  {
    attorney_id: Sequelize.INTEGER,
    type: Sequelize.INTEGER,
    link: Sequelize.TEXT,
    title: Sequelize.STRING,
    notes: { type: Sequelize.TEXT, allowNull: true },
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

Link.belongsTo(User, { as: "attorney", foreignKey: "attorney_id" });
Link.belongsTo(User, { as: "user", foreignKey: "user_id" });

paginate.paginate(Link);

db.sync();

module.exports = Link;
