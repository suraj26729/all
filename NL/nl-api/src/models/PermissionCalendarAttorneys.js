const Sequelize = require("sequelize");
const db = require("../config/database");
const UserPermission = require("./UserPermission");
const User = require("./User");

const PermissionCalendarAttorney = db.define(
  "permission_calendar_attorneys",
  {
    user_permission_id: {
      type: Sequelize.INTEGER,
      references: { model: UserPermission, key: "id" },
      primaryKey: true
    },
    attorney_id: { type: Sequelize.INTEGER, primaryKey: true }
  },
  {
    underscored: true,
    timestamps: false
  }
);

PermissionCalendarAttorney.removeAttribute("id");

PermissionCalendarAttorney.belongsTo(User, {
  as: "attorney",
  foreignKey: "attorney_id",
  targetKey: "id"
});

PermissionCalendarAttorney.sync();

module.exports = PermissionCalendarAttorney;
