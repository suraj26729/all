const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const Lead = require("./Leads");
const DocType = require("./masters/DocType");

const Document = db.define(
  "lead_documents",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    lead_id: Sequelize.INTEGER,
    name: Sequelize.STRING,
    type_id: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    url: Sequelize.TEXT
  },
  {
    underscored: true
  }
);

Document.belongsTo(Lead, {
  as: "lead",
  foreignKey: "lead_id"
});
Document.belongsTo(DocType, { as: "type", foreignKey: "type_id" });

paginate.paginate(Document);

Document.sync();

module.exports = Document;
