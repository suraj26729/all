const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const Client = require("./Client");
const Matter = require("./CaseDetails");
const DocType = require("./masters/DocType");

const Document = db.define(
  "documents",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    client_id: Sequelize.UUID,
    name: Sequelize.STRING,
    case_id: Sequelize.UUID,
    type_id: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    url: Sequelize.TEXT
  },
  {
    underscored: true
  }
);

Document.belongsTo(Client, { as: "client", foreignKey: "client_id" });
Document.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });
Document.belongsTo(DocType, { as: "type", foreignKey: "type_id" });

paginate.paginate(Document);

db.sync();

module.exports = Document;
