const Sequelize = require("sequelize");
const db = require("../config/database");
const CalendarAttorneys = require("./PermissionCalendarAttorneys");
const Permission = require("./Permission");
const User = require("./User");
const TimerAttorneys = require("./PermissionTimerAttorneys");
const UserPermission = db.define(
    "user_permissions", {
        user_id: Sequelize.INTEGER,
        permission_id: Sequelize.INTEGER,
        view: { type: Sequelize.BOOLEAN, defaultValue: false },
        add: { type: Sequelize.BOOLEAN, defaultValue: false },
        edit: { type: Sequelize.BOOLEAN, defaultValue: false },
        delete: { type: Sequelize.BOOLEAN, defaultValue: false },
        assignee: Sequelize.INTEGER,
        task_assign_admin: { type: Sequelize.BOOLEAN, allowNull: true },
        task_assign_attorney: { type: Sequelize.BOOLEAN, allowNull: true },
        isactive: { type: Sequelize.INTEGER, defaultValue: 1 }
    }, { underscored: true }
);

UserPermission.hasMany(CalendarAttorneys, {
    as: "calendarAttorneys",
    foreignKey: "user_permission_id"
});

UserPermission.hasMany(TimerAttorneys, {
    as: "timerAttorneys",
    foreignKey: "user_permission_id"
});


UserPermission.belongsTo(Permission, {
    as: "permission",
    foreignKey: "permission_id"
});

User.hasMany(UserPermission, { as: "permissions", foreignKey: "user_id" });

UserPermission;

UserPermission.sync();

module.exports = UserPermission;