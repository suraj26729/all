const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("../models/User");
const Client = require("../models/Client");
const CaseThirdparty = require("../models/CaseThirdparty");
const CaseAssistAttorneys = require("../models/CaseAssistAttorneys");
const CaseContacts = require("../models/CaseContacts");
const ClientContact = require("../models/ClientContact");
const Category = require("./masters/Category");

const CaseDetails = db.define(
  "case_details",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    client_id: {
      type: Sequelize.UUID
      //references: { model: Client, key: "id" }
    },
    prefix: Sequelize.CHAR,
    casenumber: Sequelize.STRING,
    description: { type: Sequelize.STRING, allowNull: true },
    category_id: { type: Sequelize.INTEGER, allowNull: true },
    client_no: { type: Sequelize.STRING, allowNull: true },
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
    cnflag: { type: Sequelize.INTEGER, defaultValue: 2 },
    priattorney: { type: Sequelize.INTEGER, allowNull: true },
    closuredate: { type: Sequelize.STRING, allowNull: true },
    closuredetails: { type: Sequelize.STRING, allowNull: true },
    closureby: { type: Sequelize.INTEGER, allowNull: true },
    user_id: { type: Sequelize.INTEGER, allowNull: true }
  },
  {
    paranoid: true,
    underscored: true
  }
);

CaseDetails.hasMany(CaseThirdparty, {
  as: "casethirdparty",
  foreignKey: "case_id",
  sourceKey: "id"
});

CaseDetails.hasMany(CaseAssistAttorneys, {
  as: "assistattorney",
  foreignKey: "case_id"
});

CaseDetails.belongsTo(User, {
  as: "principleAttorney",
  foreignKey: "priattorney"
});

CaseDetails.belongsTo(Category, { as: "category", foreignKey: "category_id" });

CaseDetails.belongsToMany(ClientContact, {
  as: "contacts",
  through: { model: CaseContacts },
  foreignKey: "case_id",
  otherKey: "c_id"
});

paginate.paginate(CaseDetails);

db.sync();

module.exports = CaseDetails;
