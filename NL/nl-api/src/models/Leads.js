const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("./User");
const Category = require("./masters/Category");
const Task = require("./dashboard/Task");

const Lead = db.define(
  "leads",
  {
    uuid: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1 },
    type: Sequelize.CHAR,
    companyname: { type: Sequelize.STRING, allowNull: true },
    companyreg: { type: Sequelize.STRING, allowNull: true },
    companyvat: { type: Sequelize.STRING, allowNull: true },
    lead_type: Sequelize.STRING,
    firstname: Sequelize.STRING,
    surname: { type: Sequelize.STRING, allowNull: true },
    email: { type: Sequelize.STRING, allowNull: true },
    contact1: { type: Sequelize.STRING, allowNull: true },
    contact2: { type: Sequelize.STRING, allowNull: true },
    mobile: { type: Sequelize.STRING, allowNull: true },
    dob: { type: Sequelize.DATEONLY, allowNull: true },
    address1: { type: Sequelize.TEXT, allowNull: true },
    address2: { type: Sequelize.TEXT, allowNull: true },
    city: { type: Sequelize.STRING, allowNull: true },
    zipcode: { type: Sequelize.STRING, allowNull: true },
    country: { type: Sequelize.INTEGER, allowNull: true },
    province: { type: Sequelize.STRING, allowNull: true },
    category_id: { type: Sequelize.INTEGER, allowNull: true },
    consult_date: { type: Sequelize.DATEONLY, allowNull: true },
    consult_time: { type: Sequelize.STRING, allowNull: true },
    consult_datetime: { type: Sequelize.DATE, allowNull: true },
    location: { type: Sequelize.STRING, allowNull: true },
    client_name: { type: Sequelize.STRING, allowNull: true },
    file_no: { type: Sequelize.STRING, allowNull: true },
    amount_paid: { type: Sequelize.DECIMAL, allowNull: true },
    deposit_paid: { type: Sequelize.DECIMAL, allowNull: true },
    assignee_id: Sequelize.INTEGER,
    consultation_booked: { type: Sequelize.STRING, defaultValue: "yes" },
    client_arrive: { type: Sequelize.STRING, defaultValue: "yes" },
    converted: { type: Sequelize.STRING, allowNull: true },
    status: { type: Sequelize.STRING, defaultValue: "open" },
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

Lead.belongsTo(User, { as: "user", foreignKey: "user_id" });
Lead.belongsTo(User, { as: "assignee", foreignKey: "assignee_id" });
Lead.belongsTo(Category, { as: "category", foreignKey: "category_id" });
Lead.hasMany(Task, { as: "tasks", foreignKey: "lead_id" });

paginate.paginate(Lead);

db.sync();

module.exports = Lead;
