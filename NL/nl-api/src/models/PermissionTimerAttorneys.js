const Sequelize = require("sequelize");
const db = require("../config/database");
const UserPermission = require("./UserPermission");
const User = require("./User");

const PermissionTimerAttorneys = db.define(
    "permission_timer_attorneys", {
        user_permission_id: {
            type: Sequelize.INTEGER,
            references: { model: UserPermission, key: "id" },
            primaryKey: true
        },
        attorney_id: { type: Sequelize.INTEGER, primaryKey: true }
    }, {
        underscored: true,
        timestamps: false
    }
);

PermissionTimerAttorneys.removeAttribute("id");

PermissionTimerAttorneys.belongsTo(User, {
    as: "attorney",
    foreignKey: "attorney_id",
    targetKey: "id"
});

PermissionTimerAttorneys.sync();

module.exports = PermissionTimerAttorneys;