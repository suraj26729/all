const Sequelize = require("sequelize");
const db = require("../config/database");

const ISOCountry = db.define(
  "countries",
  {
    iso: Sequelize.STRING,
    name: Sequelize.STRING,
    nicename: Sequelize.STRING,
    iso3: Sequelize.STRING,
    numcode: Sequelize.INTEGER,
    phonecode: Sequelize.INTEGER,
  },
  {
    underscored: true,
    timestamps: false,
    freezeTableName: true,
  }
);

ISOCountry.sync();

module.exports = ISOCountry;
