const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");
const QuoteItem = require("./QuoteItems");
const Client = require("../Client");
const Matter = require("../CaseDetails");
const ThirdParty = require("../ClientContact");
const CostType = require("./CostType");
const Tax = require("./Tax");

const Quote = db.define(
  "quotes",
  {
    uuid: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1 },
    quote_number: Sequelize.INTEGER,
    client_id: Sequelize.UUID,
    case_id: { type: Sequelize.UUID, allowNull: true },
    thirdparty_id: { type: Sequelize.UUID, allowNull: true },
    contact: { type: Sequelize.UUID, allowNull: true },
    payment_date: Sequelize.DATEONLY,
    vat_ref: { type: Sequelize.STRING, allowNull: true },
    file_no: { type: Sequelize.STRING, allowNull: true },
    message: { type: Sequelize.TEXT, allowNull: true },
    attachment: { type: Sequelize.TEXT, allowNull: true },
    total: Sequelize.FLOAT,
    status: { type: Sequelize.INTEGER, defaultValue: 0 },
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

// Quote.beforeCreate(async (quote, options) => {
//   let count = await Quote.count({ user_id: quote.user_id });
//   quote.quote_number = count + 1;
//   return quote.quote_number;
// });

Quote.belongsTo(Client, { as: "client", foreignKey: "client_id" });
Quote.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });
Quote.belongsTo(ThirdParty, { as: "thirdparty", foreignKey: "thirdparty_id" });
Quote.belongsTo(ThirdParty, { as: "casecontact", foreignKey: "contact" });

Quote.hasMany(QuoteItem, { as: "items", foreignKey: "quote_id" });
QuoteItem.belongsTo(Quote, { as: "quote", foreignKey: "quote_id" });
QuoteItem.belongsTo(CostType, { as: "costtype", foreignKey: "cost_id" });
QuoteItem.belongsTo(Tax, { as: "tax", foreignKey: "tax_id" });

paginate.paginate(Quote);

Quote.sync();

module.exports = Quote;
