const Sequelize = require("sequelize");
const db = require("../../config/database");

const BankDetail = db.define(
  "bank_details",
  {
    message: Sequelize.TEXT,
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

db.sync();

module.exports = BankDetail;
