const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");
const Client = require("../Client");
const Matter = require("../CaseDetails");

const MoneyAllocation = db.define(
  "money_allocations",
  {
    client_id: Sequelize.UUID,
    case_id: Sequelize.UUID,
    invoice_id: Sequelize.INTEGER,
    invoice_type: { type: Sequelize.CHAR({ length: 1 }), defaultValue: "i" },
    amount: Sequelize.FLOAT,
    description: Sequelize.TEXT,
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

MoneyAllocation.belongsTo(Client, { as: "client", foreignKey: "client_id" });
MoneyAllocation.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });

paginate.paginate(MoneyAllocation);

db.sync();

module.exports = MoneyAllocation;
