const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");
const Quote = require("./Quote");
const CostType = require("./CostType");

const QuoteItem = db.define(
  "quote_item",
  {
    quote_id: Sequelize.INTEGER,
    cost_id: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    quantity: Sequelize.FLOAT,
    price: Sequelize.FLOAT,
    tax_id: Sequelize.INTEGER,
    discount: Sequelize.FLOAT,
    total: Sequelize.FLOAT,
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

db.sync();

module.exports = QuoteItem;
