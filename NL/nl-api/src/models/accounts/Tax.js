const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");

const Tax = db.define(
  "master_taxes",
  {
    name: Sequelize.STRING,
    percent: Sequelize.FLOAT,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

paginate.paginate(Tax);

db.sync();

module.exports = Tax;
