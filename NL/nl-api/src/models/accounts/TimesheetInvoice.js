const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");
const Client = require("../Client");
const Matter = require("../CaseDetails");
const ThirdParty = require("../ClientContact");
const CostType = require("./CostType");
const Tax = require("./Tax");
const User = require("../User");

const TimesheetInvoice = db.define(
  "timesheet_invoices",
  {
    uuid: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1 },
    invoice_number: Sequelize.INTEGER,
    attorney_id: Sequelize.INTEGER,
    client_id: Sequelize.UUID,
    quote_id: { type: Sequelize.INTEGER, allowNull: true },
    case_id: Sequelize.UUID,
    thirdparty_id: Sequelize.UUID,
    contact: Sequelize.STRING,
    payment_date: Sequelize.DATEONLY,
    vat_ref: { type: Sequelize.STRING, allowNull: true },
    file_no: { type: Sequelize.STRING, allowNull: true },
    message: { type: Sequelize.TEXT, allowNull: true },
    attachment: { type: Sequelize.TEXT, allowNull: true },
    total: Sequelize.FLOAT,
    status: { type: Sequelize.INTEGER, defaultValue: 0 },
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

paginate.paginate(TimesheetInvoice);
TimesheetInvoice.sync();

const TimesheetInvoiceItem = db.define(
  "timesheet_invoice_items",
  {
    invoice_id: Sequelize.INTEGER,
    attorney_id: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    hours: Sequelize.TIME,
    price: Sequelize.FLOAT,
    tax_id: Sequelize.INTEGER,
    discount: Sequelize.FLOAT,
    total: Sequelize.FLOAT,
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

TimesheetInvoiceItem.sync();

TimesheetInvoice.beforeCreate(async (invoice, options) => {
  let count = await TimesheetInvoice.count({ user_id: invoice.user_id });
  invoice.invoice_number = count + 1;
  return invoice.invoice_number;
});

TimesheetInvoice.belongsTo(Client, { as: "client", foreignKey: "client_id" });
//TimesheetInvoice.belongsTo(Quote, { as: "quote", foreignKey: "quote_id" });
TimesheetInvoice.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });
TimesheetInvoice.belongsTo(ThirdParty, {
  as: "thirdparty",
  foreignKey: "thirdparty_id"
});

TimesheetInvoice.belongsTo(ThirdParty, {
  as: "casecontact",
  foreignKey: "contact"
});

TimesheetInvoice.hasMany(TimesheetInvoiceItem, {
  as: "items",
  foreignKey: "invoice_id"
});
TimesheetInvoiceItem.belongsTo(TimesheetInvoice, {
  as: "invoice",
  foreignKey: "invoice_id"
});
TimesheetInvoiceItem.belongsTo(User, {
  as: "attorney",
  foreignKey: "attorney_id"
});
TimesheetInvoiceItem.belongsTo(Tax, { as: "tax", foreignKey: "tax_id" });

module.exports = { TimesheetInvoice, TimesheetInvoiceItem };
