const Sequelize = require("sequelize");
const db = require("../../config/database");
const CostType = require("./CostType");

const InvoiceItem = db.define(
  "invoice_item",
  {
    invoice_id: Sequelize.INTEGER,
    cost_id: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    quantity: Sequelize.FLOAT,
    price: Sequelize.FLOAT,
    tax_id: Sequelize.INTEGER,
    discount: Sequelize.FLOAT,
    total: Sequelize.FLOAT,
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

db.sync();

module.exports = InvoiceItem;
