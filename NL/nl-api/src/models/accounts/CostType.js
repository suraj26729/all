const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");

const CostType = db.define(
  "master_cost_types",
  {
    name: Sequelize.STRING,
    code: { type: Sequelize.STRING, allowNull: true },
    description: { type: Sequelize.TEXT, allowNull: true },
    cost: Sequelize.FLOAT,
    isactive: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER,
  },
  {
    paranoid: true,
    underscored: true,
  }
);

paginate.paginate(CostType);

db.sync();

module.exports = CostType;
