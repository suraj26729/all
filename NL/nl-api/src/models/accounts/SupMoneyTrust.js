const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");
const Supplier = require("../Supplier");

const SupMoneyTrust = db.define(
  "Supplier_money_trusts",
  {
    supplier_id: Sequelize.UUID,
    amount: Sequelize.FLOAT,
    description: Sequelize.TEXT,
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER,
  },
  {
    paranoid: true,
    underscored: true,
  }
);

SupMoneyTrust.belongsTo(Supplier, {
  as: "supplier",
  foreignKey: "supplier_id",
});

paginate.paginate(SupMoneyTrust);

SupMoneyTrust.sync();

module.exports = SupMoneyTrust;
