const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");
const Supplier = require("../Supplier");

const SupMoneyAllocation = db.define(
  "supplier_money_allocations",
  {
    supplier_id: Sequelize.UUID,
    invoice_id: Sequelize.INTEGER,
    invoice_type: { type: Sequelize.CHAR({ length: 1 }), defaultValue: "i" },
    amount: Sequelize.FLOAT,
    description: Sequelize.TEXT,
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER,
  },
  {
    paranoid: true,
    underscored: true,
  }
);

SupMoneyAllocation.belongsTo(Supplier, {
  as: "supplier",
  foreignKey: "supplier_id",
});

paginate.paginate(SupMoneyAllocation);

SupMoneyAllocation.sync();

module.exports = SupMoneyAllocation;
