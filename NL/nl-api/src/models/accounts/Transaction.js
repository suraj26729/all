const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");
const MoneyTrust = require("./MoneyTrust");
const MoneyAllocation = require("./MoneyAllocation");
const Matter = require("../CaseDetails");

const Transaction = db.define(
  "transactions",
  {
    case_id: Sequelize.UUID,
    trust_id: { type: Sequelize.INTEGER, allowNull: true },
    allocate_id: { type: Sequelize.INTEGER, allowNull: true },
    type: { type: Sequelize.STRING, defaultValue: "deposit" },
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

Transaction.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });
Transaction.belongsTo(MoneyTrust, { as: "trust", foreignKey: "trust_id" });
Transaction.belongsTo(MoneyAllocation, {
  as: "allocation",
  foreignKey: "allocate_id"
});
MoneyTrust.hasMany(Transaction, {
  as: "transactions",
  foreignKey: "parent_id"
});

paginate.paginate(Transaction);

db.sync();

module.exports = Transaction;
