const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");
const Client = require("../Client");
const Matter = require("../CaseDetails");
const ThirdParty = require("../ClientContact");

const SMS = db.define(
  "sms_messages",
  {
    client_id: { type: Sequelize.UUID, allowNull: true },
    case_id: { type: Sequelize.UUID, allowNull: true },
    recipient_id: { type: Sequelize.UUID, allowNull: true },
    phone: Sequelize.STRING,
    message: Sequelize.TEXT,
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

SMS.belongsTo(Client, { as: "client", foreignKey: "client_id" });
SMS.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });
//SMS.belongsTo(Client, { as: "recipient_client", foreignKey: "recipient_id" });
// SMS.belongsTo(ThirdParty, {
//   as: "recipient_client_contact",
//   foreignKey: "recipient_id"
// });

paginate.paginate(SMS);

SMS.sync();

module.exports = SMS;
