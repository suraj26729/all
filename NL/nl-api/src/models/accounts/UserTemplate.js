const Sequelize = require("sequelize");
const db = require("../../config/database");

const UserTemplate = db.define(
  "user_template",
  {
    user_id: Sequelize.INTEGER,
    template: Sequelize.TEXT
  },
  { underscored: true, timestamps: false, freezeTableName: true }
);

UserTemplate.sync();

module.exports = UserTemplate;
