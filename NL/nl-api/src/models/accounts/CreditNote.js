const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");
const CreditNoteItem = require("./CreditNoteItems");
const Invoice = require("./Invoice");
const Client = require("../Client");
const Matter = require("../CaseDetails");
const ThirdParty = require("../ClientContact");
const CostType = require("./CostType");
const Tax = require("./Tax");

const CreditNote = db.define(
  "credit_notes",
  {
    uuid: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1 },
    credit_note_number: Sequelize.INTEGER,
    client_id: Sequelize.UUID,
    invoice_id: Sequelize.INTEGER,
    case_id: Sequelize.UUID,
    thirdparty_id: Sequelize.UUID,
    file_no: { type: Sequelize.STRING, allowNull: true },
    contact: { type: Sequelize.UUID, allowNull: true },
    payment_date: Sequelize.DATEONLY,
    vat_ref: { type: Sequelize.STRING, allowNull: true },
    message: { type: Sequelize.TEXT, allowNull: true },
    attachment: { type: Sequelize.TEXT, allowNull: true },
    total: Sequelize.FLOAT,
    status: { type: Sequelize.INTEGER, defaultValue: 0 },
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

// CreditNote.beforeCreate(async(creditnote, options) => {
//     let count = await CreditNote.count({ user_id: creditnote.user_id });
//     creditnote.credit_note_number = count + 1;
//     return creditnote.credit_note_number;
// });

CreditNote.belongsTo(Client, { as: "client", foreignKey: "client_id" });
CreditNote.belongsTo(Invoice, { as: "invoice", foreignKey: "invoice_id" });
CreditNote.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });
CreditNote.belongsTo(ThirdParty, {
  as: "thirdparty",
  foreignKey: "thirdparty_id"
});
CreditNote.belongsTo(ThirdParty, {
  as: "casecontact",
  foreignKey: "contact"
});

CreditNote.hasMany(CreditNoteItem, {
  as: "items",
  foreignKey: "credit_note_id"
});
CreditNoteItem.belongsTo(CreditNote, {
  as: "credit_note",
  foreignKey: "credit_note_id"
});
CreditNoteItem.belongsTo(CostType, { as: "costtype", foreignKey: "cost_id" });
CreditNoteItem.belongsTo(Tax, { as: "tax", foreignKey: "tax_id" });

paginate.paginate(CreditNote);

db.sync();

module.exports = CreditNote;
