const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");
const Client = require("../Client");
const Matter = require("../CaseDetails");

const MoneyTrust = db.define(
  "money_trusts",
  {
    client_id: Sequelize.UUID,
    case_id: Sequelize.UUID,
    amount: Sequelize.FLOAT,
    description: Sequelize.TEXT,
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

MoneyTrust.belongsTo(Client, { as: "client", foreignKey: "client_id" });
MoneyTrust.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });

paginate.paginate(MoneyTrust);

db.sync();

module.exports = MoneyTrust;
