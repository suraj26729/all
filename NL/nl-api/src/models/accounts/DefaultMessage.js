const Sequelize = require("sequelize");
const db = require("../../config/database");

const DefaultMessage = db.define(
  "default_messages",
  {
    message: Sequelize.TEXT,
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

db.sync();

module.exports = DefaultMessage;
