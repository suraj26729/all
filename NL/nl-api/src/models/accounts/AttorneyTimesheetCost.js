const Sequelize = require("sequelize");
const db = require("../../config/database");
const User = require("../User");

const TimesheetCost = db.define(
  "attorney_timesheet_costs",
  {
    attorney_id: Sequelize.INTEGER,
    code: { type: Sequelize.STRING, allowNull: true },
    cost: Sequelize.FLOAT,
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

TimesheetCost.belongsTo(User, { as: "attorney", foreignKey: "attorney_id" });
User.hasOne(TimesheetCost, { as: "cost", foreignKey: "attorney_id" });

TimesheetCost.sync();

module.exports = TimesheetCost;
