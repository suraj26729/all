const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");

const Expense = db.define(
  "expenses",
  {
    name: Sequelize.TEXT,
    amount: Sequelize.DOUBLE,
    category: Sequelize.TEXT,
    type: Sequelize.STRING,
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

Expense.sync();

paginate.paginate(Expense);

module.exports = Expense;
