const Sequelize = require("sequelize");
const db = require("../../config/database");
const SupplierInvoiceItem = require("./SupInvoiceItem");
const Supplier = require("../Supplier");
const paginate = require("sequelize-paginate");

const CostType = require("./CostType");
const Tax = require("./Tax");

const SupInvoice = db.define(
  "supplier_invoices",
  {
    uuid: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1 },
    invoice_number: Sequelize.INTEGER,
    supplier_id: Sequelize.UUID,
    contact: { type: Sequelize.UUID, allowNull: true },
    payment_date: Sequelize.DATEONLY,
    vat_ref: { type: Sequelize.STRING, allowNull: true },
    file_no: { type: Sequelize.STRING, allowNull: true },
    message: { type: Sequelize.TEXT, allowNull: true },
    attachment: { type: Sequelize.TEXT, allowNull: true },
    total: Sequelize.FLOAT,
    status: { type: Sequelize.INTEGER, defaultValue: 0 },
    user_id: Sequelize.INTEGER,
    parent_id: { type: Sequelize.INTEGER, allowNull: true },
  },
  {
    paranoid: true,
    underscored: true,
  }
);

SupInvoice.addScope(
  "defaultScope",
  {
    where: { parent_id: null },
  },
  { override: true }
);

// Invoice.beforeCreate(async (invoice, options) => {
//   let count = await Invoice.count({ user_id: invoice.user_id });
//   invoice.invoice_number = count + 1;
//   return invoice.invoice_number;
// });

SupInvoice.belongsTo(Supplier, { as: "supplier", foreignKey: "supplier_id" });

//Invoice.belongsTo(Quote, { as: "quote", foreignKey: "quote_id" });

SupInvoice.hasMany(SupplierInvoiceItem, {
  as: "items",
  foreignKey: "invoice_id",
});
SupplierInvoiceItem.belongsTo(SupInvoice, {
  as: "invoice",
  foreignKey: "invoice_id",
});
SupplierInvoiceItem.belongsTo(CostType, {
  as: "costtype",
  foreignKey: "cost_id",
});

SupplierInvoiceItem.belongsTo(Tax, { as: "tax", foreignKey: "tax_id" });

paginate.paginate(SupInvoice);
SupInvoice.sync();

module.exports = SupInvoice;
