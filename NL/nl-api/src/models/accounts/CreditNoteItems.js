const Sequelize = require("sequelize");
const db = require("../../config/database");

const CreditNoteItem = db.define(
  "credit_note_items",
  {
    credit_note_id: Sequelize.INTEGER,
    cost_id: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    quantity: Sequelize.FLOAT,
    price: Sequelize.FLOAT,
    tax_id: Sequelize.INTEGER,
    discount: Sequelize.FLOAT,
    total: Sequelize.FLOAT,
    user_id: Sequelize.INTEGER
  },
  {
    paranoid: true,
    underscored: true
  }
);

db.sync();

module.exports = CreditNoteItem;
