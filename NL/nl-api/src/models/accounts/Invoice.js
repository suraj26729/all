const Sequelize = require("sequelize");
const db = require("../../config/database");
const paginate = require("sequelize-paginate");
const InvoiceItem = require("./InvoiceItems");
const Quote = require("./Quote");
const Client = require("../Client");
const Matter = require("../CaseDetails");
const ThirdParty = require("../ClientContact");
const CostType = require("./CostType");
const Tax = require("./Tax");

const Invoice = db.define(
  "invoices",
  {
    uuid: { type: Sequelize.UUID, defaultValue: Sequelize.UUIDV1 },
    invoice_number: Sequelize.INTEGER,
    client_id: Sequelize.UUID,
    quote_id: { type: Sequelize.INTEGER, allowNull: true },
    case_id: Sequelize.UUID,
    thirdparty_id: Sequelize.UUID,
    contact: { type: Sequelize.UUID, allowNull: true },
    payment_date: Sequelize.DATEONLY,
    vat_ref: { type: Sequelize.STRING, allowNull: true },
    file_no: { type: Sequelize.STRING, allowNull: true },
    message: { type: Sequelize.TEXT, allowNull: true },
    attachment: { type: Sequelize.TEXT, allowNull: true },
    total: Sequelize.FLOAT,
    status: { type: Sequelize.INTEGER, defaultValue: 0 },
    user_id: Sequelize.INTEGER,
    parent_id: { type: Sequelize.INTEGER, allowNull: true }
  },
  {
    paranoid: true,
    underscored: true
  }
);

Invoice.addScope(
  "defaultScope",
  {
    where: { parent_id: null }
  },
  { override: true }
);

// Invoice.beforeCreate(async (invoice, options) => {
//   let count = await Invoice.count({ user_id: invoice.user_id });
//   invoice.invoice_number = count + 1;
//   return invoice.invoice_number;
// });

Invoice.belongsTo(Client, { as: "client", foreignKey: "client_id" });
//Invoice.belongsTo(Quote, { as: "quote", foreignKey: "quote_id" });
Invoice.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });
Invoice.belongsTo(ThirdParty, {
  as: "thirdparty",
  foreignKey: "thirdparty_id"
});

Invoice.belongsTo(ThirdParty, {
  as: "casecontact",
  foreignKey: "contact"
});

Invoice.hasMany(InvoiceItem, { as: "items", foreignKey: "invoice_id" });
InvoiceItem.belongsTo(Invoice, { as: "invoice", foreignKey: "invoice_id" });
InvoiceItem.belongsTo(CostType, { as: "costtype", foreignKey: "cost_id" });
InvoiceItem.belongsTo(Tax, { as: "tax", foreignKey: "tax_id" });

paginate.paginate(Invoice);

db.sync();

module.exports = Invoice;
