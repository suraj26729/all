const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");
const SupMoneyTrust = require("./SupMoneyTrust");
const SupMoneyAllocation = require("./SupMoneyAllocation");

const SupTransaction = db.define(
  "sup_transactions",
  {
    supplier_id: Sequelize.UUID,
    trust_id: { type: Sequelize.INTEGER, allowNull: true },
    allocate_id: { type: Sequelize.INTEGER, allowNull: true },
    type: { type: Sequelize.STRING, defaultValue: "deposit" },
    status: { type: Sequelize.INTEGER, defaultValue: 1 },
    user_id: Sequelize.INTEGER,
  },
  {
    paranoid: true,
    underscored: true,
  }
);

SupTransaction.belongsTo(SupMoneyTrust, {
  as: "trust",
  foreignKey: "trust_id",
});
SupTransaction.belongsTo(SupMoneyAllocation, {
  as: "allocation",
  foreignKey: "allocate_id",
});
SupMoneyTrust.hasMany(SupTransaction, {
  as: "transactions",
  foreignKey: "parent_id",
});

paginate.paginate(SupTransaction);

SupTransaction.sync();

module.exports = SupTransaction;
