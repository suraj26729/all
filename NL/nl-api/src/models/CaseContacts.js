const Sequelize = require("sequelize");
const db = require("../config/database");
const CaseDetails = require("../models/CaseDetails");
const ClientContact = require("./ClientContact");

const CaseContact = db.define(
  "case_contacts",
  {
    case_id: {
      type: Sequelize.UUID
    },
    c_id: Sequelize.UUID,
    c_type: { type: Sequelize.STRING, defaultValue: "c" }
  },
  {
    timestamps: false,
    underscored: true
  }
);

CaseContact.removeAttribute("id");

CaseContact.belongsTo(ClientContact, {
  as: "clientcontact",
  foreignKey: "c_id",
  targetKey: "id"
});

CaseContact.sync();

module.exports = CaseContact;
