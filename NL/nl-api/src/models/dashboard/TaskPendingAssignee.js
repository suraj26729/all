const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");
const Task = require("./Task");
const User = require("../User");

const TaskPendingAssignee = db.define(
  "task_pending_assignee",
  {
    task_id: {
      type: Sequelize.INTEGER
    },
    user_id: { type: Sequelize.INTEGER }
  },
  {
    underscored: true,
    timestamps: false
  }
);

TaskPendingAssignee.removeAttribute("id");

TaskPendingAssignee.belongsTo(User, {
  as: "assignee",
  foreignKey: "user_id"
});

db.sync();

module.exports = TaskPendingAssignee;
