const Sequelize = require("sequelize");
const db = require("../../config/database");
const Meeting = require("./Meeting");
const User = require("../User");

const MeetingAssignee = db.define(
  "meeting_assignee",
  {
    meeting_id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    user_id: { type: Sequelize.INTEGER, primaryKey: true }
  },
  {
    underscored: true,
    timestamps: false
  }
);

MeetingAssignee.removeAttribute("id");

MeetingAssignee.belongsTo(User, {
  as: "assignee",
  foreignKey: "user_id"
});

db.sync();

module.exports = MeetingAssignee;
