const Sequelize = require("sequelize");
const db = require("../../config/database");
const MeetingAssignee = require("./MeetingAssignee");
const Client = require("../Client");
const Matter = require("../CaseDetails");
const ClientContact = require("../ClientContact");
const User = require("../User");

const DashboardMeeting = db.define(
  "meetings",
  {
    subject: Sequelize.STRING,
    client_id: { type: Sequelize.UUID, allowNull: true },
    case_id: { type: Sequelize.UUID, allowNul: true },
    contact_id: { type: Sequelize.UUID, allowNul: true },
    thirdparty_id: { type: Sequelize.UUID, allowNull: true },
    location: { type: Sequelize.STRING, allowNull: true },
    board_room: { type: Sequelize.STRING, allowNull: true },
    start_date: Sequelize.DATE,
    end_date: Sequelize.DATE,
    start_time: Sequelize.TIME,
    end_time: Sequelize.TIME,
    all_day_event: { type: Sequelize.INTEGER, defaultValue: 0 },
    description: Sequelize.TEXT,
    user_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

DashboardMeeting.belongsTo(Client, {
  as: "clientdetail",
  foreignKey: "client_id"
});

DashboardMeeting.belongsTo(Matter, {
  as: "matter",
  foreignKey: "case_id",
  allowNul: true
});

DashboardMeeting.belongsTo(ClientContact, {
  as: "thirdparty",
  foreignKey: "thirdparty_id",
  allowNul: true
});

DashboardMeeting.belongsTo(ClientContact, {
  as: "contact",
  foreignKey: "contact_id",
  allowNul: true
});

DashboardMeeting.hasMany(MeetingAssignee, {
  as: "assignees",
  foreignKey: "meeting_id"
});

DashboardMeeting.belongsTo(User, {
  as: "user",
  foreignKey: "user_id"
});

db.sync();

module.exports = DashboardMeeting;
