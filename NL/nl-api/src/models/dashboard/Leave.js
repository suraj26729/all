const Sequelize = require("sequelize");
const db = require("../../config/database");
const User = require("../User");

const Leave = db.define(
  "leaves",
  {
    from_date: Sequelize.DATE,
    from_time: Sequelize.TIME,
    to_date: Sequelize.DATE,
    to_time: Sequelize.TIME,
    type: { type: Sequelize.INTEGER, defaultValue: 1 },
    notes: Sequelize.TEXT,
    user_id: Sequelize.INTEGER,
    author_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

Leave.belongsTo(User, { as: "user", foreignKey: "user_id" });
Leave.belongsTo(User, { as: "author", foreignKey: "author_id" });

Leave.sync();

module.exports = Leave;
