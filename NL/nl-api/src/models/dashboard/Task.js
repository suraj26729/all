const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../../config/database");
const TaskAssignee = require("./TaskAssignee");
const Client = require("../Client");
const Matter = require("../CaseDetails");
const ClientContact = require("../ClientContact");
const CaseDetails = require("../CaseDetails");
const User = require("../User");
const moment = require("moment");

const DashboardTasks = db.define(
  "tasks",
  {
    subject: Sequelize.STRING,
    client_id: { type: Sequelize.UUID, allowNull: true },
    case_id: Sequelize.UUID,
    contact_id: { type: Sequelize.UUID, allowNull: true },
    thirdparty_id: { type: Sequelize.UUID, allowNull: true },
    location: { type: Sequelize.STRING, allowNull: true },
    board_room: { type: Sequelize.STRING, allowNull: true },
    start_date: Sequelize.DATE,
    end_date: Sequelize.DATE,
    start_time: Sequelize.TIME,
    end_time: Sequelize.TIME,
    all_day_event: { type: Sequelize.INTEGER, defaultValue: 0 },
    description: Sequelize.TEXT,
    add_notes: { type: Sequelize.INTEGER, defaultValue: 0 },
    user_id: Sequelize.INTEGER,
    status: Sequelize.STRING,
    reason: { type: Sequelize.TEXT, allowNull: true },
    lead_id: { type: Sequelize.INTEGER, allowNull: true }
  },
  {
    underscored: true
  }
);

DashboardTasks.belongsTo(Client, {
  as: "clientdetail",
  foreignKey: "client_id"
});

DashboardTasks.belongsTo(Matter, {
  as: "matter",
  foreignKey: "case_id"
});

DashboardTasks.belongsTo(ClientContact, {
  as: "thirdparty",
  foreignKey: "thirdparty_id",
  allowNull: true
});

DashboardTasks.belongsTo(ClientContact, {
  as: "contact",
  foreignKey: "contact_id",
  allowNull: true
});

DashboardTasks.hasMany(TaskAssignee, {
  as: "assignees",
  foreignKey: "task_id"
});

DashboardTasks.belongsTo(User, {
  as: "user",
  foreignKey: "user_id"
});

CaseDetails.hasMany(DashboardTasks, {
  as: "tasks",
  foreignKey: "case_id"
});

CaseDetails.hasMany(DashboardTasks, {
  as: "future_tasks",
  foreignKey: "case_id",
  where: {
    start_date: {
      $gte: moment(new Date())
        .startOf("day")
        .format("YYYY-MM-DD HH:mm:ss")
    }
  }
});

paginate.paginate(DashboardTasks);

DashboardTasks.sync();

module.exports = DashboardTasks;
