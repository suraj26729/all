const Sequelize = require("sequelize");
const db = require("../../config/database");
const Task = require("./Task");
const User = require("../User");

const TaskAssignee = db.define(
    "task_assignee", {
        task_id: {
            type: Sequelize.INTEGER
        },
        user_id: { type: Sequelize.INTEGER },
        status: { type: Sequelize.INTEGER }
    }, {
        underscored: true,
        timestamps: false
    }
);

TaskAssignee.removeAttribute("id");

TaskAssignee.belongsTo(User, {
    as: "assignee",
    foreignKey: "user_id"
});

db.sync();

module.exports = TaskAssignee;