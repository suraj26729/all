const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const Client = require("./Supplier");
const DocType = require("./masters/DocType");

const SupplierDocument = db.define(
  "supplier_documents",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true,
    },
    supplier_id: Sequelize.UUID,
    name: Sequelize.STRING,
    type_id: Sequelize.INTEGER,
    description: Sequelize.TEXT,
    url: Sequelize.TEXT,
  },
  {
    underscored: true,
  }
);

SupplierDocument.belongsTo(Client, {
  as: "supplier",
  foreignKey: "supplier_id",
});
SupplierDocument.belongsTo(DocType, { as: "type", foreignKey: "type_id" });

paginate.paginate(SupplierDocument);

db.sync();

module.exports = SupplierDocument;
