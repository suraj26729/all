const Sequelize = require("sequelize");
const db = require("../config/database");
const Client = require("../models/Client");
const Matter = require("../models/CaseDetails");
const User = require("./User");

const Email = db.define(
  "emails",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    parent_id: { type: Sequelize.STRING, allowNull: true },
    client_id: {
      type: { type: Sequelize.UUID, allowNull: true }
    },
    matter_id: {
      type: { type: Sequelize.UUID, allowNull: true }
    },
    subject: { type: Sequelize.STRING, allowNull: true },
    type: { type: Sequelize.STRING, defaultValue: "normal" },
    user_id: { type: Sequelize.INTEGER, allowNull: true }
  },
  {
    underscored: true
  }
);

Email.belongsTo(Client, { as: "client", foreignKey: "client_id" });
Email.belongsTo(Matter, { as: "matter", foreignKey: "matter_id" });
Email.belongsTo(User, { as: "user", foreignKey: "user_id" });

Email.sync();

module.exports = Email;
