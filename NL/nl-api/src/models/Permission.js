const Sequelize = require("sequelize");
const db = require("../config/database");

const Permission = db.define(
  "permissions",
  {
    name: Sequelize.STRING,
    url: Sequelize.STRING,
    isactive: { type: Sequelize.INTEGER, defaultValue: 0 }
  },
  { underscored: true }
);

Permission.sync();

module.exports = Permission;
