const Sequelize = require("sequelize");
const db = require("../config/database");

const Order = db.define(
    "orders", {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV1,
            primaryKey: true
        },
        agreement_id: Sequelize.STRING,
        outstanding_balance: { type: Sequelize.UUID, allowNull: true },
        payer_email: Sequelize.STRING,
        payer_firstName: Sequelize.STRING,
        payer_lastName: Sequelize.STRING,
        payer_id: Sequelize.STRING,
        paypal_state: Sequelize.STRING,
        billing_userName: Sequelize.STRING,
        billing_userEmail: Sequelize.STRING,
        regular_amount: Sequelize.STRING
    }, {
        underscored: true
    }
);

db.sync();

module.exports = Order;