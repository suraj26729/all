const Sequelize = require("sequelize");
const db = require("../config/database");

const Notification = db.define(
  "notifications",
  {
    title: Sequelize.STRING,
    content: Sequelize.TEXT,
    ref_id: { type: Sequelize.STRING, allowNull: true },
    ref_module: { type: Sequelize.STRING, allowNull: true },
    user_id: Sequelize.INTEGER,
    read: { type: Sequelize.BOOLEAN, defaultValue: false }
  },
  {
    underscored: true
  }
);

Notification.sync();

module.exports = Notification;
