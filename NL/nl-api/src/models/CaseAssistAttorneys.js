const Sequelize = require("sequelize");
const db = require("../config/database");
const CaseDetails = require("../models/CaseDetails");
const User = require("./User");

const CaseAssistAttorney = db.define(
  "case_assist_attorneys",
  {
    case_id: {
      type: Sequelize.UUID,
      references: { model: CaseDetails, key: "id" },
      primaryKey: true
    },
    attorney_id: { type: Sequelize.INTEGER, primaryKey: true }
  },
  {
    underscored: true,
    timestamps: false
  }
);

CaseAssistAttorney.removeAttribute("id");

// CaseAssistAttorney.belongsTo(CaseDetails, {
//   as: "casedetails",
//   foreignKey: "case_id",
//   targetKey: "id"
// });

CaseAssistAttorney.belongsTo(User, {
  as: "attorney",
  foreignKey: "attorney_id",
  targetKey: "id"
});

db.sync();

module.exports = CaseAssistAttorney;
