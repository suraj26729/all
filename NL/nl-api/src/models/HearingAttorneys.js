const Sequelize = require("sequelize");
const db = require("../config/database");
const Hearing = require("./Hearing");
const User = require("./User");

const HearingAttorney = db.define(
  "hearing_attorney",
  {
    hearing_id: {
      type: Sequelize.UUID,
      references: { model: Hearing, key: "id" },
      primaryKey: true
    },
    attorney_id: { type: Sequelize.INTEGER, primaryKey: true }
  },
  {
    underscored: true,
    timestamps: false
  }
);

HearingAttorney.removeAttribute("id");

HearingAttorney.belongsTo(User, {
  as: "attorney",
  foreignKey: "attorney_id",
  targetKey: "id"
});

db.sync();

module.exports = HearingAttorney;
