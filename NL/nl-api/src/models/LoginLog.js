const Sequelize = require("sequelize");
const paginate = require("sequelize-paginate");
const db = require("../config/database");
const User = require("../models/User");

const LoginLog = db.define(
  "login_logs",
  {
    ip: Sequelize.STRING,
    attorney_id: Sequelize.INTEGER
  },
  {
    underscored: true
  }
);

LoginLog.belongsTo(User, { as: "attorney", foreignKey: "attorney_id" });

paginate.paginate(LoginLog);

db.sync();

module.exports = LoginLog;
