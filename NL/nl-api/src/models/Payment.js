const Sequelize = require("sequelize");
const db = require("../config/database");

const Payment = db.define(
  "payment",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    user_id: Sequelize.INTEGER,
    amount: { type: Sequelize.DOUBLE, allowNull: true },
    payment_id: { type: Sequelize.TEXT, allowNull: true },
    payer_id: { type: Sequelize.STRING, allowNull: true },
    payer_email: { type: Sequelize.STRING, allowNull: true },
    payment_method: { type: Sequelize.STRING, defaultValue: "paypal" },
    status: { type: Sequelize.STRING, defaultValue: "pending" },
    details: { type: Sequelize.TEXT, allowNull: true }
  },
  {
    paranoid: true,
    underscored: true
  }
);

Payment.sync();

module.exports = Payment;
