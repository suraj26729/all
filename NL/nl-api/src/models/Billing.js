const Sequelize = require("sequelize");
const db = require("../config/database");

const Billing = db.define(
  "billing",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    agreement_id: { type: Sequelize.STRING, allowNull: true },
    outstanding_balance: { type: Sequelize.STRING, allowNull: true },
    payer_email: { type: Sequelize.STRING, allowNull: true },
    payer_firstName: { type: Sequelize.STRING, allowNull: true },
    payer_lastName: { type: Sequelize.STRING, allowNull: true },
    payer_id: { type: Sequelize.STRING, allowNull: true },
    paypal_state: { type: Sequelize.STRING, allowNull: true },
    billing_useremail: { type: Sequelize.STRING, allowNull: true },
    expiry_date: { type: Sequelize.DATE, allowNull: true },
    due_paid: { type: Sequelize.STRING, allowNull: true }
  },
  {
    paranoid: true,
    underscored: true
  }
);

Billing.sync();

module.exports = Billing;
