const Sequelize = require("sequelize");
const db = require("../config/database");
const Client = require("./Client");
const Matter = require("./CaseDetails");
const User = require("./User");

const Timesheet = db.define(
  "timesheets",
  {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV1,
      primaryKey: true
    },
    client_id: Sequelize.UUID,
    case_id: Sequelize.UUID,
    notes: { type: Sequelize.TEXT, allowNull: true },
    date: Sequelize.DATEONLY,
    hours: Sequelize.TIME,
    user_id: Sequelize.INTEGER
  },
  { underscored: true }
);

Timesheet.belongsTo(Client, { as: "client", foreignKey: "client_id" });
Timesheet.belongsTo(Matter, { as: "matter", foreignKey: "case_id" });
Timesheet.belongsTo(User, { as: "user", foreignKey: "user_id" });

Timesheet.sync();

module.exports = Timesheet;
