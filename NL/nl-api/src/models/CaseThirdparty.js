const Sequelize = require("sequelize");
const db = require("../config/database");
const CaseDetails = require("../models/CaseDetails");
const ClientContact = require("./ClientContact");

const CaseThirdparty = db.define(
  "case_thirdparty",
  {
    case_id: {
      type: Sequelize.UUID,
      references: { model: CaseDetails, key: "id" },
      primaryKey: true
    },
    tp_id: { type: Sequelize.UUID, primaryKey: true },
    ch_id: { type: Sequelize.UUID, primaryKey: true },
    refno: { type: Sequelize.STRING, allowNull: true }
  },
  {
    timestamps: false,
    underscored: true
  }
);

CaseThirdparty.removeAttribute("id");

CaseThirdparty.belongsTo(ClientContact, {
  as: "clientthirdparty",
  foreignKey: "tp_id",
  targetKey: "id"
});

CaseThirdparty.belongsTo(ClientContact, {
  as: "clientcontact",
  foreignKey: "ch_id",
  targetKey: "id"
});

db.sync();

module.exports = CaseThirdparty;
