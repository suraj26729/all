const express = require("express");
const app = express();
const bodyParser = require("body-parser");
var fs = require("fs");
const morgan = require("morgan");
var path = require("path");
const cors = require("cors");

const userRoutes = require("./src/routes/users");
const authRoutes = require("./src/routes/auth");
const roleRoutes = require("./src/routes/roles");
const masterRoutes = require("./src/routes/masters");
const attorneyRoutes = require("./src/routes/attorneys");
const clientRoutes = require("./src/routes/clients");
const supplierRoutes= require("./src/routes/suppliers")
const adminRoutes = require("./src/routes/admins");
const dashboardRoutes = require("./src/routes/dashboard");
const linkRoutes = require("./src/routes/links");
const leadRoutes = require("./src/routes/leads");
const reportRoutes = require("./src/routes/reports");
const accountsRoutes = require("./src/routes/accounts");
const timerRoutes = require("./src/routes/timesheets");
const permissionRoutes = require("./src/routes/permissions");
const frontRoutes = require("./src/routes/front");
const backendRoutes = require("./src/routes/backend");
const notificationRoutes = require("./src/routes/notifications");
const cron = require("./src/routes/cron");
const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "access.log"),
  { flags: "a" }
);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  morgan("combined", {
    skip: function(req, res) {
      return res.statusCode < 400;
    },
    stream: accessLogStream
  })
);
app.use("/api/assets", express.static(__dirname + "/assets"));
app.use("/api/assets/images", express.static(__dirname + "/assets/images"));
app.use("/api/uploads", express.static(__dirname + "/uploads"));
app.use("/api/blogs", express.static(__dirname + "/uploads/blogs"));

app.use("/api/users", userRoutes);
app.use("/api/auth", authRoutes);
app.use("/api/roles", roleRoutes);
app.use("/api/notifications", notificationRoutes);
masterRoutes.routes(app);
attorneyRoutes.routes(app);
clientRoutes.routes(app);
supplierRoutes.routes(app);
adminRoutes.routes(app);
dashboardRoutes.routes(app);
app.use("/api/links", linkRoutes);
app.use("/api/leads", leadRoutes);
reportRoutes.routes(app);
accountsRoutes.routes(app);
app.use("/api/timesheets", timerRoutes);
app.use("/api/permissions", permissionRoutes);
frontRoutes.routes(app);

backendRoutes.routes(app);

app.use((req, res, next) => {
  const error = new Error("404 Not Found!");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  const status = error.status || 500;
  res.status(status).json({
    success: 0,
    data: {},
    error: {
      status: status,
      message: error.message
    }
  });
});

var port = process.env.PORT || 3000;
app.listen(port);

console.log("API is running at http://localhost:" + port);
