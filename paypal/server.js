const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const env = require("dotenv");

env.config();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


const prefix = "/api/v1";
app.use(prefix + "/auth",require( "./src/routes/paypal"));
app.use((req, res, next) => {
    const error = new Error("404 Not Found!");
    error.status = 404;
    next(error);
  });
  app.use((error, req, res, next) => {
    const status = error.status || 500;
    return res.status(status).json({
      status: status,
      message: error.message
    });
  });
  
  var port = process.env.PORT || 3000;
  app.listen(port);
  
  console.log("API is running at http://localhost:" + port);
  