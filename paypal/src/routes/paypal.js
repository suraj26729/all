const express = require("express");
const router = express.Router();
const paypal = require("paypal-rest-sdk");
paypal.configure({
  mode: "sandbox", //sandbox or live
  client_id: process.env.CLIENT_ID,
  client_secret: process.env.SECRET_ID,
});

router.post("/pay", async (req, res, next) => {
  try {
    const create_payment = {
      intent: "sale",
      payer: {
        payment_method: "paypal",
      },
      redirect_urls: {
        return_url: "http://localhost:3000/api/v1/auth/success",
        cancel_url: "http://localhost:3000/api/v1/auth/cancel",
      },
      transactions: [
        {
          item_list: {
            items: [
              {
                name: "phone",
                price: "1000",
                currency: "USD",
                quantity: 1,
              },
            ],
          },
          amount: {
            currency: "USD",
            total: "1000",
          },
          description: "good phone",
        },
      ],
    };
    paypal.payment.create(create_payment, function (error, payment) {
      if (error) {
        throw error;
      } else {
          
        let links = payment.links;
        console.log(payment);
        for (let link of links) {
          if (link.method == "REDIRECT") {
            
            return res.status(201).json(link.href);
            

          }
        }
      }
    });
  } catch (err) {
    next(err);
  }
});

router.get("/success", async (req, res, next) => {
  try {
    let payer_id = req.query.PayerID;
    let paymentId = req.query.paymentId;
    let total = 1000;

    var execute_payment_json = {
      payer_id: payer_id,
      transactions: [
        {
          amount: {
            currency: "USD",
            total: total,
          },
        },
      ],
    };
    paypal.payment.execute(
      paymentId,
      execute_payment_json,
      async (error, payment) => {
        if (error) {
          console.log(error.response);
          throw error;
        } else {
            console.log(JSON.stringify(payment));
            res.send("Payment success");
          
        }
      }
    );
  } catch (err) {
    next(err);
  }
});

router.get("/cancel", async (req, res, next) => {
   res.send("Payment Cncelled");
});
module.exports = router;
